﻿
1) Install the MySQLServer(version 5.6.10.1 and above)

 i) Get the MySQLServer (.exe) from the IT team or download ,click the mysql-<version>.exe file
    and follow the intructions.Stick to the default options when prompted.
 ii)Run the DDL and DML sql files(located under the GIT path samsung-dayamed\sqlFiles) 
    from mysql-workbench

2) Install RabbitMQ Server( version 3.7.4 and above)(Follow the url: https://www.rabbitmq.com/install-windows.html). Run rabbitmq-server batch file (under the sbin folder of RabbitMQ installation) 
to run the RabbitMQ server.Login to admin UI which is available at http://localhost:15672 with 
default username "guest" and password "guest" to test the installation.

3. To use dayamed cloud config server set the following properties in the bootstrap.properties 
   file under the location src/main/resources
  i)  Set the property spring.cloud.config.enabled to true
  ii) Set the property spring.cloud.config.uri to url of the existing clould config server(eg. http:
  iii)Set the property spring.profiles.active=<active spring profile> 

Docker Steps: 
Containerizing the Dayamed Services (Image Name : dayamed-services-docker)

1.Generate docker image . Execute the below command from the current directory.
Command:  gradlew build docker 

2.Execute to following command from the MySQLServer client to enable  communication with docker-hosted dayamed services application with the database.
CREATE USER '<dbuserName>'@'docker_host_ip' IDENTIFIED BY '<dbpassword>'
GRANT ALL PRIVILEGES ON *.* TO '<dbuserName>'@'docker_host_ip';
FLUSH PRIVILEGES; 

3.To enable communication with docker-hosted dayamed services application with RabbitMQ server
.Create an RabbitMQ user (eg. with username 'test' and password 'test') .Specify this user detials,docker_host_ip in the RabbitMQ configuration section of dayamed-services-<profile_name>.properties config file in the GIT).

4.Run the docker container (Image Name : dayamed-services-docker) 
Command: docker run -e "SPRING_PROFILES_ACTIVE=<spring_active_profile>" -e  "SPRING_CLOUD_CONFIG_URI=http://<docker_host_ip>:8888" -p 8082:8082 -t com.sg.dayamed/dayamed-services-docker
The Dayamed Services Application will be available at http://localhost:8082 in Docker host.




