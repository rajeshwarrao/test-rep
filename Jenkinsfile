pipeline {
    
	agent any
	
	tools {
		maven 'maven_3_6_0'
	}
	environment {
        // This can be nexus3 or nexus2
        NEXUS_VERSION = "nexus3"
        // This can be http or https
        NEXUS_PROTOCOL = "http"
        // Where your Nexus is running
        NEXUS_URL = "192.168.8.133:8081/nexus"
        // Repository where we will upload the artifact
        NEXUS_REPOSITORY = "DayaMed-Server"
        //NEXUS_REPOSITORY = "maven-releases"
        // Jenkins credential id to authenticate to Nexus OSS
        NEXUS_CREDENTIAL_ID = "nexus-credentials"
    }
	stages {
		stage ('Notify') {
			steps {
				//send build start notification email
                emailext mimeType: 'text/html',subject: "STARTED: Job '${env.JOB_NAME} #${env.BUILD_NUMBER}'",
                    body: """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} #${env.BUILD_NUMBER}</a>&QUOT;</p>""",
                    recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]

				// Send slack build start notitification
				slackSend channel: '#workflow-updates',
                    color: '#D4DADF',
                    message: "The pipeline `${currentBuild.fullDisplayName}`: Build Initiated \n${env.BUILD_URL} "

				bitbucketStatusNotify buildState: "INPROGRESS"
			}
		}
		stage ('SonarQube Analysis') {
			when {
				branch 'dayamed-dev';
			}
			steps {
				withSonarQubeEnv('sonar-scanner') {
					//sh 'mvn org.jacoco:jacoco-maven-plugin:prepare-agent install'
					sh 'mvn clean install -P runSonardev sonar:sonar'
				}
			}
			post {
                success {
                    junit 'target/surefire-reports/*.xml'
                }
            }
		}
		stage("Quality Gate") {
			when {
				branch 'dayamed-dev';
			}
            steps {
				script {
					timeout(time: 10, unit: 'MINUTES') {
                    def qg = waitForQualityGate abortPipeline: true
					if (qg.status != 'OK') {
						echo "Status: ${qg.status}"
                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
						}
					}
                }
            }
        }
		stage('Build') {
            steps {
        	    script {
        		    if(env.BRANCH_NAME == 'dayamed-dev') {
        		        sh 'mvn clean install -P vm -DskipTests=true versions:set -DnewVersion=1.0.0.${BUILD_NUMBER}-SNAPSHOT'
                        archiveArtifacts artifacts:'target/*.jar', fingerprint: true
        		    } else {
                        sh 'mvn clean install -P vm'
        		    }
                }
            }
        }
		stage('Publish to Nexus') {
            when {
				branch 'dayamed-dev';
			}
           steps {
                script {
                    // Read POM xml file using 'readMavenPom' step , this step 'readMavenPom' is included in: https://plugins.jenkins.io/pipeline-utility-steps
                    pom = readMavenPom file: "pom.xml";
                    // Find built artifact under target folder
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
                    // Print some info from the artifact found
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    // Extract the path from the File found
                    artifactPath = filesByGlob[0].path;
                    // Assign to a boolean response verifying If the artifact name exists
                    artifactExists = fileExists artifactPath;
                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";
                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                // Artifact generated such as .jar, .ear and .war files.
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],
                                // Lets upload the pom.xml file for additional information for Transitive dependencies
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            	]
                        	);
                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
        }
		stage('Deploy to local VM') {
			when {
				branch 'dayamed-dev';
			}
			steps {
				sh 'scp -i "/var/lib/jenkins/.ssh/id_rsa" target/*.jar jenkins@192.168.1.86:C:/dayamed-app'
			}
		}
	}
	post {
        success {
            emailext mimeType: 'text/html',subject: "SUCCESSFUL: Job '${env.JOB_NAME} #${env.BUILD_NUMBER}'",
                body: """<p>SUCCESSFUL: Job '${env.JOB_NAME} #${env.BUILD_NUMBER}':</p>
                <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} #${env.BUILD_NUMBER}</a>&QUOT;</p>""",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
			
			slackSend channel: '#workflow-updates',
                    color: '#BDFFC3',
                    message: "The pipeline `${currentBuild.fullDisplayName}`: Build Success \n${env.BUILD_URL} "
			
			bitbucketStatusNotify buildState: "SUCCESSFUL"
		}
        failure {
            emailext mimeType: 'text/html', attachLog: true, compressLog: true,subject: "FAILED: Job '${env.JOB_NAME} #${env.BUILD_NUMBER}'",
                body: """<p>FAILED: Job '${env.JOB_NAME} #${env.BUILD_NUMBER}':</p>
                <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} #${env.BUILD_NUMBER}</a>&QUOT;</p>""",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
			
			slackSend channel: '#workflow-updates',
                    color: '#FFFE89',
                    message: "The pipeline `${currentBuild.fullDisplayName}`: Build Failed \n${env.BUILD_URL} "

			bitbucketStatusNotify buildState: "FAILED"
        }
	}
}
