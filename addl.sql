/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.6.42 : Database - dayamedgsdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dayamedgsdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dayamedgsdb`;

/*Table structure for table `DATABASECHANGELOG` */

DROP TABLE IF EXISTS `DATABASECHANGELOG`;

CREATE TABLE `DATABASECHANGELOG` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `DATABASECHANGELOGLOCK` */

DROP TABLE IF EXISTS `DATABASECHANGELOGLOCK`;

CREATE TABLE `DATABASECHANGELOGLOCK` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `adherence` */

DROP TABLE IF EXISTS `adherence`;

CREATE TABLE `adherence` (
  `adherence_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gs_notification_status` bigint(20) DEFAULT NULL,
  `how` tinyblob,
  `location` tinyblob,
  `observedtime` datetime DEFAULT NULL,
  `prescribedtime` datetime DEFAULT NULL,
  `prescriptionid` bigint(20) NOT NULL,
  PRIMARY KEY (`adherence_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4453 DEFAULT CHARSET=latin1;

/*Table structure for table `adherence_consumptionstatus` */

DROP TABLE IF EXISTS `adherence_consumptionstatus`;

CREATE TABLE `adherence_consumptionstatus` (
  `adherence_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FKa9v0ljq7a9rexy5tu5dwk6cir` (`adherence_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `adherence_dosageinfo_map` */

DROP TABLE IF EXISTS `adherence_dosageinfo_map`;

CREATE TABLE `adherence_dosageinfo_map` (
  `adherence_dosageinfo_map_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dosageinfo_id` bigint(20) DEFAULT NULL,
  `adherence_adherence_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`adherence_dosageinfo_map_id`),
  KEY `FKfegcgfqdcubxnxcqi9ilrnm6i` (`adherence_adherence_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5815 DEFAULT CHARSET=latin1;

/*Table structure for table `bp_consumptionstatus` */

DROP TABLE IF EXISTS `bp_consumptionstatus`;

CREATE TABLE `bp_consumptionstatus` (
  `bp_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FK3f818cktlh9xld86amnsxqm4n` (`bp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `bp_monitor` */

DROP TABLE IF EXISTS `bp_monitor`;

CREATE TABLE `bp_monitor` (
  `bp_monitor_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_info_id` varchar(255) DEFAULT NULL,
  `diastolic_pressure_reading_in_mm_hg` varchar(255) DEFAULT NULL,
  `location` tinyblob,
  `observed_time` datetime DEFAULT NULL,
  `prescribed_time` datetime DEFAULT NULL,
  `prescriptionid` bigint(20) DEFAULT NULL,
  `systolic_pressure_reading_in_mm_hg` varchar(255) DEFAULT NULL,
  `patient_patient_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`bp_monitor_id`),
  KEY `FK62gw2ex9egfmbb30vrq4b3ka0` (`patient_patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `caregiver_provider_map` */

DROP TABLE IF EXISTS `caregiver_provider_map`;

CREATE TABLE `caregiver_provider_map` (
  `caregiver_id` bigint(20) NOT NULL,
  `provider_id` bigint(20) NOT NULL,
  PRIMARY KEY (`caregiver_id`,`provider_id`),
  KEY `FKg647i6j9l06tg79gikbar69lu` (`provider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `cart` */

DROP TABLE IF EXISTS `cart`;

CREATE TABLE `cart` (
  `id` bigint(20) NOT NULL,
  `added_date` datetime DEFAULT NULL,
  `created_user_id` bigint(20) DEFAULT NULL,
  `created_userrole` varchar(255) DEFAULT NULL,
  `item_category_type` varchar(255) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `item_prescription_id` bigint(20) DEFAULT NULL,
  `patient_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `config_params` */

DROP TABLE IF EXISTS `config_params`;

CREATE TABLE `config_params` (
  `id` bigint(20) NOT NULL,
  `key_param` varchar(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `value_param` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `consentdocument` */

DROP TABLE IF EXISTS `consentdocument`;

CREATE TABLE `consentdocument` (
  `document_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `docbody` longtext,
  `publicationdate` datetime DEFAULT NULL,
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `consumergoods` */

DROP TABLE IF EXISTS `consumergoods`;

CREATE TABLE `consumergoods` (
  `consumer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `consumer_description` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `consumer_name` varchar(255) DEFAULT NULL,
  `consumer_purpose` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`consumer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `consumergoods_info` */

DROP TABLE IF EXISTS `consumergoods_info`;

CREATE TABLE `consumergoods_info` (
  `consumergoods_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goods_comment` varchar(255) DEFAULT NULL,
  `deleteflag` bit(1) DEFAULT NULL,
  `time` tinyblob,
  `commodity_consumer_id` bigint(20) DEFAULT NULL,
  `prescription_patient_prescription_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`consumergoods_info_id`),
  KEY `FK88tnneiixdrw09b3r7qnuwhim` (`commodity_consumer_id`),
  KEY `FK2ty0cukcc1u80yceecqj9f1wb` (`prescription_patient_prescription_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `consumption_frequency` */

DROP TABLE IF EXISTS `consumption_frequency`;

CREATE TABLE `consumption_frequency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `consumptions_desc` varchar(255) DEFAULT NULL,
  `consumptions_id` bigint(20) DEFAULT NULL,
  `consumptions_key` varchar(255) DEFAULT NULL,
  `consumptions_times` varchar(255) DEFAULT NULL,
  `frequency_id` bigint(20) DEFAULT NULL,
  `frequency_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6mkalx8mgur6wo2g9xv1wimjw` (`consumptions_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `consumption_template` */

DROP TABLE IF EXISTS `consumption_template`;

CREATE TABLE `consumption_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `consumptions_desc` varchar(255) DEFAULT NULL,
  `consumptions_id` bigint(20) DEFAULT NULL,
  `consumptions_key` varchar(255) DEFAULT NULL,
  `consumptions_times` varchar(255) DEFAULT NULL,
  `frequency_id` bigint(20) DEFAULT NULL,
  `frequency_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_janyuewq4fmciy358xheot96m` (`consumptions_key`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `dayamed_caregiver` */

DROP TABLE IF EXISTS `dayamed_caregiver`;

CREATE TABLE `dayamed_caregiver` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adminflag` bit(1) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbbow4pshatlfrty9aiwgkaqxs` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Table structure for table `dayamed_patient` */

DROP TABLE IF EXISTS `dayamed_patient`;

CREATE TABLE `dayamed_patient` (
  `patient_id` bigint(20) NOT NULL,
  `currentadherence` varchar(255) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `emoji` varchar(255) DEFAULT NULL,
  `predictedadherence` varchar(255) DEFAULT NULL,
  `projectedadherence` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  KEY `FK1w4riid0qspb0is18tb9y5ppy` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `dayamed_pharmasist` */

DROP TABLE IF EXISTS `dayamed_pharmasist`;

CREATE TABLE `dayamed_pharmasist` (
  `pharmasist_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pharma_company_name` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pharmasist_id`),
  KEY `FKjj3x5dl66l5ympr9kbu1wp0sw` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `dayamed_provider` */

DROP TABLE IF EXISTS `dayamed_provider`;

CREATE TABLE `dayamed_provider` (
  `provider_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `licence` varchar(255) DEFAULT NULL,
  `provider_specilization_on` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`provider_id`),
  KEY `FKh5bamh20bo3ebrrchbb9305yo` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Table structure for table `device` */

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device` (
  `device_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_description` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `device_info` */

DROP TABLE IF EXISTS `device_info`;

CREATE TABLE `device_info` (
  `device_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_info_comment` varchar(255) DEFAULT NULL,
  `deleteflag` bit(1) DEFAULT NULL,
  `device_duration` int(11) DEFAULT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `device_info_time` tinyblob,
  `device_device_id` bigint(20) DEFAULT NULL,
  `prescription_patient_prescription_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`device_info_id`),
  KEY `FK9jsjhdflu2ae0rj8djfr48k4x` (`device_device_id`),
  KEY `FK8ot6ac92s4dmty9cv4o99budf` (`prescription_patient_prescription_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `deviceinfo_notification` */

DROP TABLE IF EXISTS `deviceinfo_notification`;

CREATE TABLE `deviceinfo_notification` (
  `deviceinfo_notification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `countrycode` varchar(255) DEFAULT NULL,
  `expected_time` varchar(255) DEFAULT NULL,
  `greater_than_reading1` varchar(255) DEFAULT NULL,
  `greater_than_reading2` varchar(255) DEFAULT NULL,
  `less_than_reading1` varchar(255) DEFAULT NULL,
  `less_than_reading2` varchar(255) DEFAULT NULL,
  `notification_type` varchar(255) DEFAULT NULL,
  `notification_value` varchar(255) DEFAULT NULL,
  `device_infoid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`deviceinfo_notification_id`),
  KEY `FKcf0598tt9kqgm1jcbrgi6tmsk` (`device_infoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `devicestatus` */

DROP TABLE IF EXISTS `devicestatus`;

CREATE TABLE `devicestatus` (
  `devicestatus_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devicestatus` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_details_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`devicestatus_id`),
  KEY `FK714fcj1p654bmqo6ygc515hdx` (`user_details_user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `diagnosis` */

DROP TABLE IF EXISTS `diagnosis`;

CREATE TABLE `diagnosis` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=187 DEFAULT CHARSET=latin1;

/*Table structure for table `disease` */

DROP TABLE IF EXISTS `disease`;

CREATE TABLE `disease` (
  `disease_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `disease_description` varchar(3000) DEFAULT NULL,
  `disease_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`disease_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `dosage_device` */

DROP TABLE IF EXISTS `dosage_device`;

CREATE TABLE `dosage_device` (
  `dosage_device_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device` varchar(255) DEFAULT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `prescription_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dosage_device_id`),
  KEY `FKfth7tti11nmdu58vh12fxgs86` (`prescription_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `dosage_notification` */

DROP TABLE IF EXISTS `dosage_notification`;

CREATE TABLE `dosage_notification` (
  `dosage_info_notification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `countrycode` varchar(255) DEFAULT NULL,
  `expected_time` varchar(255) DEFAULT NULL,
  `notification_type` varchar(255) DEFAULT NULL,
  `notification_value` varchar(255) DEFAULT NULL,
  `owner_userid` bigint(20) DEFAULT NULL,
  `dosage_info_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dosage_info_notification_id`),
  KEY `FKrkdwejxct5iiagdc4dhkapr7d` (`dosage_info_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Table structure for table `gluco_consumptionstatus` */

DROP TABLE IF EXISTS `gluco_consumptionstatus`;

CREATE TABLE `gluco_consumptionstatus` (
  `gluco_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FKipdpldgkq2mmoslnf2sy9t7f` (`gluco_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `glucometer` */

DROP TABLE IF EXISTS `glucometer`;

CREATE TABLE `glucometer` (
  `glucometer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_info_id` varchar(255) DEFAULT NULL,
  `location` tinyblob,
  `observed_time` datetime DEFAULT NULL,
  `prescribed_time` datetime DEFAULT NULL,
  `prescriptionid` bigint(20) DEFAULT NULL,
  `glucometer_reading` varchar(255) DEFAULT NULL,
  `patient_patient_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`glucometer_id`),
  KEY `FK5dtqh51546xv5dvbovf6qhb0r` (`patient_patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `gstoken_vuca` */

DROP TABLE IF EXISTS `gstoken_vuca`;

CREATE TABLE `gstoken_vuca` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_on` datetime DEFAULT NULL,
  `ndc_code` varchar(255) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2365 DEFAULT CHARSET=latin1;

/*Table structure for table `heart_rate` */

DROP TABLE IF EXISTS `heart_rate`;

CREATE TABLE `heart_rate` (
  `heart_rate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_info_id` varchar(255) DEFAULT NULL,
  `location` tinyblob,
  `observed_time` datetime DEFAULT NULL,
  `prescribed_time` datetime DEFAULT NULL,
  `prescriptionid` bigint(20) DEFAULT NULL,
  `heart_rate_reading` varchar(255) DEFAULT NULL,
  `patient_patient_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`heart_rate_id`),
  KEY `FKp9clu0fpia3vqhffrcaw4p4n1` (`patient_patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `hibernate_sequence` */

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `hr_consumptionstatus` */

DROP TABLE IF EXISTS `hr_consumptionstatus`;

CREATE TABLE `hr_consumptionstatus` (
  `hr_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FKhi7y2teqpo1juqm0ut60uhbcm` (`hr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `imantic_notification` */

DROP TABLE IF EXISTS `imantic_notification`;

CREATE TABLE `imantic_notification` (
  `imanticnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `belonging_user_details_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`imanticnotification_id`),
  KEY `FKlsc4ix9tpriato7wg118pw8hg` (`belonging_user_details_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `imanticnotification_touser` */

DROP TABLE IF EXISTS `imanticnotification_touser`;

CREATE TABLE `imanticnotification_touser` (
  `imanticnotification_id` bigint(20) NOT NULL,
  `touser` bigint(20) DEFAULT NULL,
  KEY `FKkv05c0b1m29miqk2u89wixfxc` (`imanticnotification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `medicine` */

DROP TABLE IF EXISTS `medicine`;

CREATE TABLE `medicine` (
  `medicine_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `medicine_category` varchar(255) DEFAULT NULL,
  `medicine_description` varchar(255) DEFAULT NULL,
  `gen_code` bigint(20) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `medicine_name` varchar(255) DEFAULT NULL,
  `ndc_code` bigint(20) DEFAULT NULL,
  `ndc_type` varchar(255) DEFAULT NULL,
  `packager` varchar(255) DEFAULT NULL,
  `medicine_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`medicine_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3722 DEFAULT CHARSET=latin1;

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `notification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleteflag` bit(1) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `roomname` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `notification_title` varchar(255) DEFAULT NULL,
  `notification_type` varchar(255) DEFAULT NULL,
  `belongigng_user_details_user_id` bigint(20) DEFAULT NULL,
  `meeting_duration` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `FK6b776v6jcxnwcmu7ai5cwxdvx` (`belongigng_user_details_user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6439 DEFAULT CHARSET=latin1;

/*Table structure for table `notification_received_user` */

DROP TABLE IF EXISTS `notification_received_user`;

CREATE TABLE `notification_received_user` (
  `notification_received_user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_id` bigint(20) DEFAULT NULL,
  `user_details_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`notification_received_user_id`),
  KEY `FKk13e62mk0t5rwhkxft245vs0j` (`notification_id`),
  KEY `FK51yvjb75sdedhbjbk93jvx8ya` (`user_details_user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3278 DEFAULT CHARSET=latin1;

/*Table structure for table `notification_schedule` */

DROP TABLE IF EXISTS `notification_schedule`;

CREATE TABLE `notification_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actual_date` datetime DEFAULT NULL,
  `fire_time` datetime DEFAULT NULL,
  `notification_type` varchar(255) DEFAULT NULL,
  `notificationvalue` varchar(255) DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `deviceinfo_id` bigint(20) DEFAULT NULL,
  `dosageinfo_id` bigint(20) DEFAULT NULL,
  `owner_userid` bigint(20) DEFAULT NULL,
  `prescrption_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKt61mwskdlfq8fcwpvn3ps3v0r` (`deviceinfo_id`),
  KEY `FKes37ghb0gmhxp1obhqsybu0op` (`dosageinfo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6608 DEFAULT CHARSET=latin1;

/*Table structure for table `notificationschedule_consumptionstatus` */

DROP TABLE IF EXISTS `notificationschedule_consumptionstatus`;

CREATE TABLE `notificationschedule_consumptionstatus` (
  `notificationschedule_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FKod7ojct23ukg6sh561e74ogkr` (`notificationschedule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_caregiver_map` */

DROP TABLE IF EXISTS `patient_caregiver_map`;

CREATE TABLE `patient_caregiver_map` (
  `patient_id` bigint(20) NOT NULL,
  `caregiver_id` bigint(20) NOT NULL,
  PRIMARY KEY (`patient_id`,`caregiver_id`),
  KEY `FK6ttajgqn8gvfa3j2dlxfbltbe` (`caregiver_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_disease_info` */

DROP TABLE IF EXISTS `patient_disease_info`;

CREATE TABLE `patient_disease_info` (
  `patient_disease_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleteflag` bit(1) DEFAULT NULL,
  `disease_disease_id` bigint(20) DEFAULT NULL,
  `prescription_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`patient_disease_info_id`),
  KEY `FKnb6p28lupw2lhpw6vokrp32j2` (`disease_disease_id`),
  KEY `FK8jaqf8w22bwlkfx4luyy70vt6` (`prescription_id`)
) ENGINE=MyISAM AUTO_INCREMENT=743 DEFAULT CHARSET=latin1;

/*Table structure for table `patient_dosage_info` */

DROP TABLE IF EXISTS `patient_dosage_info`;

CREATE TABLE `patient_dosage_info` (
  `patient_dosage_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adherencetype` varchar(255) DEFAULT NULL,
  `comnent` varchar(255) DEFAULT NULL,
  `deleteflag` bit(1) DEFAULT NULL,
  `disage_course_duration` int(11) DEFAULT NULL,
  `identitycode` varchar(255) DEFAULT NULL,
  `pharmacistcomment` varchar(255) DEFAULT NULL,
  `dosage_intake_time` tinyblob,
  `medicine_medicine_id` bigint(20) DEFAULT NULL,
  `prescription_id` bigint(20) DEFAULT NULL,
  `consume_duration` int(11) DEFAULT NULL,
  `frequency_json` text,
  `fristconsumption_frequency` varchar(255) DEFAULT NULL,
  `consumption_template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`patient_dosage_info_id`),
  KEY `FKjyyn7bcdhd6mtam28suwql623` (`fristconsumption_frequency`),
  KEY `FKcm8lq7ee6x01yobf4rkv4ru4a` (`consumption_template`),
  KEY `FKlwf7w7hah3h6b55l0bld5o537` (`medicine_medicine_id`),
  KEY `FK3eo9e23p60j0r1ym4jsku56fy` (`prescription_id`)
) ENGINE=MyISAM AUTO_INCREMENT=769 DEFAULT CHARSET=latin1;

/*Table structure for table `patient_pharmasist_map` */

DROP TABLE IF EXISTS `patient_pharmasist_map`;

CREATE TABLE `patient_pharmasist_map` (
  `patient_id` bigint(20) NOT NULL,
  `pharmasist_id` bigint(20) NOT NULL,
  PRIMARY KEY (`patient_id`,`pharmasist_id`),
  KEY `FK3vbr8ooamwc3s5rgaw85tp8dh` (`pharmasist_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `patient_prescription` */

DROP TABLE IF EXISTS `patient_prescription`;

CREATE TABLE `patient_prescription` (
  `patient_prescription_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider_comment` varchar(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `expected_adherence` varchar(255) DEFAULT NULL,
  `patient_illness_description` varchar(5000) DEFAULT NULL,
  `patient_illness_diagnosis` varchar(5000) DEFAULT NULL,
  `patient_patient_id` bigint(20) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  `creted_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `can_patient_modify` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`patient_prescription_id`),
  KEY `FKfev82pmpiarfqrvaeu73kyjay` (`patient_patient_id`),
  KEY `FKn1v1e2njsfrvxiacg25w4sara` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=727 DEFAULT CHARSET=latin1;

/*Table structure for table `patient_provider_map` */

DROP TABLE IF EXISTS `patient_provider_map`;

CREATE TABLE `patient_provider_map` (
  `patient_id` bigint(20) NOT NULL,
  `provider_id` bigint(20) NOT NULL,
  PRIMARY KEY (`patient_id`,`provider_id`),
  KEY `FKh5fkrv4taqhkfgtw5uvv57vos` (`provider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `prescription_diagnosis_map` */

DROP TABLE IF EXISTS `prescription_diagnosis_map`;

CREATE TABLE `prescription_diagnosis_map` (
  `prescription_id` bigint(20) NOT NULL,
  `diagnosis_id` bigint(20) NOT NULL,
  KEY `FKsli73cgt8m3bxhsj0crsncxoj` (`diagnosis_id`),
  KEY `FK3c9nga8kq5hd0vsm7c77lvy9f` (`prescription_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `prescription_schedule` */

DROP TABLE IF EXISTS `prescription_schedule`;

CREATE TABLE `prescription_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fire_time` datetime DEFAULT NULL,
  `actual_date` datetime DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `deviceinfo_id` bigint(20) DEFAULT NULL,
  `dosageinfo_id` bigint(20) DEFAULT NULL,
  `prescrption_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlmi8chn1rsblprb920oadcj8h` (`deviceinfo_id`),
  KEY `FK7ipbehllbi8i2fpsws6s4y7o2` (`dosageinfo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49304 DEFAULT CHARSET=latin1;

/*Table structure for table `prescription_status` */

DROP TABLE IF EXISTS `prescription_status`;

CREATE TABLE `prescription_status` (
  `prescription_status_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`prescription_status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `prescriptionschedule_consumptionstatus` */

DROP TABLE IF EXISTS `prescriptionschedule_consumptionstatus`;

CREATE TABLE `prescriptionschedule_consumptionstatus` (
  `prescriptionschedule_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FKf1ps2473qonx3fq09n2ermbgd` (`prescriptionschedule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `pulse_oximeter` */

DROP TABLE IF EXISTS `pulse_oximeter`;

CREATE TABLE `pulse_oximeter` (
  `pulse_oximeter_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pulse` int(11) DEFAULT NULL,
  `sp02` varchar(255) DEFAULT NULL,
  `taken_time` datetime DEFAULT NULL,
  `patient_patient_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pulse_oximeter_id`),
  KEY `FK196evibjy12vty7oouujt9bvl` (`patient_patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `scale` */

DROP TABLE IF EXISTS `scale`;

CREATE TABLE `scale` (
  `scale_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_info_id` varchar(255) DEFAULT NULL,
  `location` tinyblob,
  `observed_time` datetime DEFAULT NULL,
  `prescribed_time` datetime DEFAULT NULL,
  `prescriptionid` bigint(20) DEFAULT NULL,
  `reading_in_lbs` varchar(255) DEFAULT NULL,
  `patient_patient_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`scale_id`),
  KEY `FKrtef85rwbg8txfypnchvhdrid` (`patient_patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `scale_consumptionstatus` */

DROP TABLE IF EXISTS `scale_consumptionstatus`;

CREATE TABLE `scale_consumptionstatus` (
  `scale_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FKq573vy4rkiyvu76vvmhp4sv1` (`scale_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `steps` */

DROP TABLE IF EXISTS `steps`;

CREATE TABLE `steps` (
  `steps_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_info_id` varchar(255) DEFAULT NULL,
  `location` tinyblob,
  `observed_time` datetime DEFAULT NULL,
  `prescribed_time` datetime DEFAULT NULL,
  `prescriptionid` bigint(20) DEFAULT NULL,
  `step_count` int(11) DEFAULT NULL,
  `patient_patient_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`steps_id`),
  KEY `FKl1grashwhm6181i91yibxa511` (`patient_patient_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `steps_consumptionstatus` */

DROP TABLE IF EXISTS `steps_consumptionstatus`;

CREATE TABLE `steps_consumptionstatus` (
  `steps_id` bigint(20) NOT NULL,
  `consumptionstatus` varchar(255) DEFAULT NULL,
  KEY `FKfdseadqpw7jedgoigupwmsr26` (`steps_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `user_details` */

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `app_version` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `imantic_userid` varchar(255) DEFAULT NULL,
  `last_activetime` datetime DEFAULT NULL,
  `last_logintime` datetime DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `os_version` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pwd_change_token` varchar(255) DEFAULT NULL,
  `pwd_token_expire_date` datetime DEFAULT NULL,
  `race` varchar(255) DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `voiptoken` varchar(200) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `user_zoneid` varchar(255) DEFAULT NULL,
  `emailflag` tinyint(1) DEFAULT '1',
  `smsflag` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UK_c1fsif9ivmojbwdugkighpr1b` (`email_id`),
  KEY `FK8smm9xm25n5js2ia3ghfhrshm` (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

/*Table structure for table `user_options` */

DROP TABLE IF EXISTS `user_options`;

CREATE TABLE `user_options` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gs_pharmacist_enable` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK344wg993q16n4jl57byk98ulb` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `user_pref` */

DROP TABLE IF EXISTS `user_pref`;

CREATE TABLE `user_pref` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `user_setting` text,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1iemd45ajlv05p29lt7kpqcdq` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_role_id` bigint(20) NOT NULL,
  `user_role_discription` varchar(255) DEFAULT NULL,
  `user_role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `userconsent` */

DROP TABLE IF EXISTS `userconsent`;

CREATE TABLE `userconsent` (
  `userconsent_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userconsent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `usho_images` */

DROP TABLE IF EXISTS `usho_images`;

CREATE TABLE `usho_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `medicine_name` varchar(255) DEFAULT NULL,
  `ndccode` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `vidyotoken` */

DROP TABLE IF EXISTS `vidyotoken`;

CREATE TABLE `vidyotoken` (
  `vidyotoken_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vidyotoken_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
