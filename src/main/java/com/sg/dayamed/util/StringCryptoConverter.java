package com.sg.dayamed.util;

import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;

import javax.crypto.SecretKey;
import javax.persistence.AttributeConverter;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/

public class StringCryptoConverter<T> implements AttributeConverter<String, String> {

	PHIDataEncryption phiDataEncryption = null;

	private String keyText = "1234567890ABCD";

	public PHIDataEncryption getPhiDataEncryption() {
		if (this.phiDataEncryption == null) {
			PHIDataEncryption phiDataEncryption = BeanUtil.getBean(PHIDataEncryption.class);
			this.phiDataEncryption = phiDataEncryption;
			return this.phiDataEncryption;
		}
		return this.phiDataEncryption;
	}

	@Override
	public String convertToDatabaseColumn(String attribute) {
		String text = attribute;
		try {
			SecretKey secretKey = getPhiDataEncryption().getSecretEncryptionKey(keyText);
			return phiDataEncryption.encryptText(text, secretKey);
		} catch (Exception e) {

		}
		return text;
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		try {
			SecretKey secretKey = getPhiDataEncryption().getSecretEncryptionKey(keyText);
			return phiDataEncryption.decryptText(dbData, secretKey);
		} catch (Exception e) {

		}
		return dbData;
	}
}
