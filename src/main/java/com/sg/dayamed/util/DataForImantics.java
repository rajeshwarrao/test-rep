package com.sg.dayamed.util;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DataForImantics {

	private String patientName;

	private long patientId;

	private String imanticId;

	private long prescriptionId;

	private Map<Long, String> deviceInfoIds;

	private Map<Long, String> dosageInfoIds;

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getImanticId() {
		return imanticId;
	}

	public void setImanticId(String imanticId) {
		this.imanticId = imanticId;
	}

	public long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public Map<Long, String> getDeviceInfoIds() {
		return deviceInfoIds;
	}

	public void setDeviceInfoIds(Map<Long, String> deviceInfoIds) {
		this.deviceInfoIds = deviceInfoIds;
	}

	public Map<Long, String> getDosageInfoIds() {
		return dosageInfoIds;
	}

	public void setDosageInfoIds(Map<Long, String> dosageInfoIds) {
		this.dosageInfoIds = dosageInfoIds;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

}
