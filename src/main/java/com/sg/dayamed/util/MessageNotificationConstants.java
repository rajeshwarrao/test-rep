package com.sg.dayamed.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.apache.commons.lang3.ArrayUtils;

import javax.annotation.PostConstruct;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class MessageNotificationConstants {

	Locale[] locales = null;

	@Autowired
	MessageSource messageSource;

	@PostConstruct
	public void init() throws Exception {
		this.locales = SimpleDateFormat.getAvailableLocales();

	}

	public String getUserPreferedLangTranslatedMsg(Map<String, String> keyVal, String resourceBundleKey,
	                                               String userLangPreference, Object[] objectParams) {
		String translatedString = null;
		Locale preferedLocale = Arrays.stream(locales)
		                              .filter(locale -> locale.toString()
		                                                      .equals(userLangPreference))
		                              .findFirst()
		                              .orElse(null);
		if (preferedLocale != null) {
			String langTranslatedMsg = messageSource.getMessage(resourceBundleKey, objectParams, preferedLocale);
			VelocityContext context = new VelocityContext();
			context.put("paramMap", keyVal);
			StringWriter writer = new StringWriter();
			Velocity.evaluate(context, writer, UUID.randomUUID()
			                                       .toString(), langTranslatedMsg);
			translatedString = writer.toString();
			if (ArrayUtils.isNotEmpty(objectParams)) {
				translatedString = MessageFormat.format(translatedString, objectParams);
			}
		}
		return translatedString;

	}

	public String prefixAndToList(List<String> listToAppendLangSpecificAnd, String userLangPreference) {
		return CollectionUtils.isNotEmpty(listToAppendLangSpecificAnd) ? listToAppendLangSpecificAnd.stream()
		                                                                                            .collect(
				                                                                                            Collectors.joining(
						                                                                                            ", ")) :
				StringUtils.EMPTY;

	}

	public Map<String, String> buildAdherenceMsgForMessageResource(JSONObject jsonObj, String mapBindKey,
	                                                               String jsonPropKey) throws JSONException {
		Map<String, String> msgResourceReplaceKeyVal = new HashMap<String, String>();
		msgResourceReplaceKeyVal.put(mapBindKey, jsonObj.getString(jsonPropKey));
		msgResourceReplaceKeyVal.put("prescribedTime", jsonObj.getString("prescribedtime")
		                                                      .trim());
		msgResourceReplaceKeyVal.put("observedTime", jsonObj.getString("observedtime")
		                                                    .trim());
		return msgResourceReplaceKeyVal;
	}

	public Map<String, String> buildAdherenceMsgForMessageResource(JSONObject jsonObj, String mapBindKey,
	                                                               String jsonPropKey, List<String> medicineNameList,
	                                                               String userLangPreference)
			throws JSONException {

		Map<String, String> keyVal = new HashMap<String, String>();
		keyVal.put("prescribedTime", jsonObj.getString("prescribedtime")
		                                    .trim());
		keyVal.put("observedTime", jsonObj.getString("observedtime")
		                                  .trim());
		keyVal.put(mapBindKey, jsonObj.getString(jsonPropKey)
		                              .trim());
		String appendMedicineListWithAnd = "";
		appendMedicineListWithAnd = prefixAndToList(medicineNameList, userLangPreference);
		keyVal.put("appendMedicineListWithAnd", appendMedicineListWithAnd);
		return keyVal;

	}

}
