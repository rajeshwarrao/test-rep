

package com.sg.dayamed.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MessageResourceUtility {

   @Autowired
   MessageSource messageSource;

   public String getErrorMessageFromResource(String key, Object[] objects) {
      try {
         return messageSource.getMessage(key, objects, LocaleContextHolder.getLocale());
      } catch (Exception e) {
         return null;
      }
   }
}
