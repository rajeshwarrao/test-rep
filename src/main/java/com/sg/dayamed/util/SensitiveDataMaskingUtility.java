package com.sg.dayamed.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 29/Aug/2019
 **/

public final class SensitiveDataMaskingUtility {

	private SensitiveDataMaskingUtility() {

	}

	public static String maskEmailId(String emailData) {
		if (StringUtils.isBlank(emailData)) {
			return StringUtils.EMPTY;
		}
		String[] data = emailData.split(",");
		return Arrays.stream(data)
		             .map(value -> StringUtils.replace(value, StringUtils.substringBefore(value, "@"), "*******"))
		             .collect(Collectors.joining(", "));
	}

	public static String maskMobileNumber(String mobileData) {
		if (StringUtils.isBlank(mobileData)) {
			return StringUtils.EMPTY;
		}
		String[] data = mobileData.split(",");
		return Arrays.stream(data)
		             .map(value -> StringUtils.replace(value, StringUtils.substring(value, 0, value.length() - 4), "**********"))
		             .collect(Collectors.joining(", "));
	}

}
