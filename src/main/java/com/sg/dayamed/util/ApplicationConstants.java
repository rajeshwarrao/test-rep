package com.sg.dayamed.util;

public final class ApplicationConstants {

	private ApplicationConstants() {
		// this prevents even the native class from
		// calling this ctor as well :
		throw new AssertionError();
	}

	public static final String CAREGIVER = "caregiver";

	public static final String PROVIDER = "provider";

	public static final String PATIENT = "patient";

	public static final String PHARMACIST = "pharmacist";

	public static final String ADMIN = "admin";

	public static final String PRESCRIPTION_ADDED = "Prescription added";

	public static final String PROFILEPICS_LOCAL_PATH = "D:\\sample\\profilepics\\";

	public static final String PROFILEPICS_SERVER_PRODUCTION_PATH =
			"http://205.186.138.55:8081/pharma/images/profilepic/";

	public static final String PROFILEPICS_SERVER_STAGE_PATH = "http://205.186.138.55:8084/pharma/images/profilepic/";

	public static final String PROFILEPICS_SERVER_LOCAL_PATH = "http://192.168.5.73:8082/pharma/images/profilepic/";

	public static final String DEVICE_IMAGE_PRODUCTION_PATH = "http://205.186.138.55:8081/pharma/images/devices/";

	public static final String DEVICE_IMAGE_STAGE_PATH = "http://205.186.138.55:8084/pharma/images/devices/";

	public static final String PRESCRIPTION = "prescription";

	public static final String CALLING = "calling";

	public static final String MedicineNoAction = "medicine_no_action";

	public static final String DeviceNoAction = "device_no_action";

	public static final String INPROCESS = "InProcess";

	public static final String NEW = "New";

	public static final String HOLD = "Hold";

	public static final String DONE = "Done";

	public static final String RECEIVED = "Received";

	public static final String PROCESSED = "Processed";

	public static final String SENT = "Sent";

	public static final String DELIVERED = "Delivered";

	public static final String EMPTY_STRING = "";

	public static final String CALL = "call";

	public static final String EMAIL = "email";

	public static final String SMS = "sms";

	public static final String VIDEO = "video";

	public static final String REFILL_REMAINDER = "Refill Reminder";

	public static final String PRESCRIBEDTIME = "prescribedTime";

	public static final String TAKENTIME = "takenTime";

	public static final String STEPS = "Steps";

	public static final String BP_METER = "BP Meter";

	public static final String GLUCO_METER = "Gluco Meter";

	public static final String PULSE_OXIMETER = "Pulse Oximeter";

	public static final String PT_INR = "PT/INR";

	public static final String SCALE = "Scale";

	public static final String SPIRO_METER = "Spiro Meter";

	public static final String HEART_RATE = "Heart Rate";

	public static final String BIOMETRIC_STATUS = "Biometric Status";

	public static final String No_Action = "No_Action";

	public static final String DEVICE_STATUS_WIT_ = "device_status";

	public static final String INVALID_CURR_CREDENTIAL = "Invalid current password";

	public static final String INVALID_NEW_CREDENTIAL = "New password cannot be null or empty";

	public static final String INVALID_USER = "User does not exist";

	public static final String INVALID_TOKEN = "invalid token";

	public static final String EXPIRED_TOKEN = "expired token";

	public static final String INVALID_EMAILID = "Email id does not exist";

	public static final String INVALID_JSON = "invalid JSON Request";

	public static final String FAILED = "fail";

	public static final String MESSAGE = "message";

	public static final String STATUS = "status";

	public static final String ENCRYPTION_ALGORITHM_TYPE = "AES";

	public static final String ENCRYPTION_BYTE_FORMAT = "UTF-8";

	public static final String IOS = "ios";

	public static final String ANDROID = "android";

	public static final String WEB = "web";

	public static final String NOT_FOUND = "not found";

	public static final String VIDEO_CALL_NOTIIFCATION_MESSAGE_TOKEN_IS_EMPTY = "Token_is_Empty";

	public static final String VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION = "Sent_Notification";

	public static final String RESPONSE_STATUS = "success";

	public static final String SUCCESS = "success";

	public static final String FAIL = "fail";

	public static final String CART_ITEM_CATEGORY_MEDICIEN = "MEDICIEN";

	public static final String CART_ITEM_CATEGORY_CONSUMER = "CONSUMER";

	public static final String UPDATED_PROFILE_IN_DAYAMED_TITLE = "Updated profile in Dayamed";

	public static final String UPDATED_PROFILE_IN_DAYAMED_BODY = "your profile has been updated";

	public static final String UPDATED_PATIET_PROFILE_IN_DAYAMED_TITLE = "updated.profile.in.dayamed";

	public static final String MEDICATION_STATUS = "Medication Status";

	public static final String DEVICE_STATUS = "Device Status";

	public static final String VITAL_TAKEN = "Vital taken";

	public static final String CONSUMPTION_STATUS_SKIPPED = "skipped";

	public static final String CONSUMPTION_STATUS_SECONDALERT_SKIPPED = "second_alert_skipped";

	public static final String CONSUMPTION_STATUS_AUTODELAY = "autodelayed";

	public static final String CONSUMPTION_STATUS_DELAY = "delayed";

	public static final String CONSUMPTION_STATUS_CONSUMED = "consumed";

	public static final String CONSUMPTION_STATUS_NOACTION = "NO_Action";

	public static final String VITAL_TAKEN_WIT = "vital_consumed";

	public static final String VITAL_SKIPPED_WIT = "vital_skipped";

	public static final String VITAL_DELAYED_WIT = "vital_delayed";

	public static final String CONSUMPTION_STATUS = "Consumption status";

	public static final String DAYAMED_PATIENT_ROLE = "patient";

	public static final String PATIENT_MISSED_NOTIIFCATION_TYPE_DAILY = "Daily";

	public static final String PATIENT_MISSED_NOTIIFCATION_TYPE_WEEKLY = "Weekly";

	public static final String DOSAGE_FREQUENCY_MONTHLY = "MONTHLY";

	public static final String DOSAGE_FREQUENCY_DAILY = "Daily";

	public static final String DOSAGE_FREQUENCY_WEEKLY = "WEEKLY";

	public static final String LANG_PREF_DEFAULT = "en";

	public static final String ONCNOON = "ONCNOON";

	public static final String ONCEVE = "ONCEVE";

	public static final String ONCNIG = "ONCNIG";

	public static final String TWIEAR = "TWIEAR";

	public static final String TWIMID = "TWIMID";

	public static final String TWILAT = "TWILAT";

	public static final String THREAR = "THREAR";

	public static final String THRMID = "THRMID";

	public static final String THRLAT = "THRLAT";

	public static final String CUSTOM = "CUSTOM";

	public static final String EAR = "EAR";

	public static final String MID = "MID";

	public static final String LATE = "LATE";

	public static final String NOT_EXIST ="notExist";

	public static final String VUCA_URL_GETTING_NULL = "VUCA Url getting null";

	public static final String INVALID_VUCA_TOKEN = "Invalid Token";
	
	public static final String BAD_REQUEST = "Badrequest";

	public enum CallType {
		VideoCall,
		AudioCall,
		PSTN
	}

}
