package com.sg.dayamed.util;

import com.sg.dayamed.dosage.frequency.FrequencyAbstractFactory;
import com.sg.dayamed.entity.Address;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Adherence_DosageInfoMap;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.ConsumerGoodInfo;
import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.entity.Device;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.DeviceNotification;
import com.sg.dayamed.entity.DeviceStatus;
import com.sg.dayamed.entity.Diagnosis;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.DiseaseInfo;
import com.sg.dayamed.entity.DosageDevice;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.NotificationReceivedUser;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.pojo.AddressModel;
import com.sg.dayamed.pojo.AdherenceDataPointsModel;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.ConsumerGoodInfoModel;
import com.sg.dayamed.pojo.ConsumerGoodsModel;
import com.sg.dayamed.pojo.DeviceInfoModel;
import com.sg.dayamed.pojo.DeviceModel;
import com.sg.dayamed.pojo.DeviceStatusModel;
import com.sg.dayamed.pojo.DiagnosisModel;
import com.sg.dayamed.pojo.DiseaseInfoModel;
import com.sg.dayamed.pojo.DiseaseModel;
import com.sg.dayamed.pojo.DosageDeviceModel;
import com.sg.dayamed.pojo.DosageInfoModel;
import com.sg.dayamed.pojo.DosageNotificationModel;
import com.sg.dayamed.pojo.FirstConsumptionTimeModel;
import com.sg.dayamed.pojo.FrequencyPerDayModel;
import com.sg.dayamed.pojo.FristConsumptionTypeModel;
import com.sg.dayamed.pojo.MedicineModel;
import com.sg.dayamed.pojo.NotificationModel;
import com.sg.dayamed.pojo.NotificationReceivedUserModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.PrescriptionStatusModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.RoleModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.pojo.notification.DeviceNotificationModel;
import com.sg.dayamed.pojo.notification.DeviceSpecificNotificationModel;
import com.sg.dayamed.pojo.notification.GreaterThanNotificationModel;
import com.sg.dayamed.pojo.notification.LessThanNotificationModel;
import com.sg.dayamed.repository.ConsumptionFrequencyRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.ConsumptionTemplateService;
import com.sg.dayamed.service.DiagnosisService;
import com.sg.dayamed.service.DosageInfoService;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.UserPreferenceService;

import lombok.NonNull;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;


@Component
public class Utility {

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	NotificationService notificationService;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	DiseaseRepository diseaseRepository;

	@Autowired
	ImanticService imanticService;

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	@Autowired
	UserPreferenceRepository userPreferenceRepository;

	@Autowired
	ConsumptionFrequencyRepository consumptionFrequencyRepository;

	@Value("${profilepics.server.path}")
	String profilepicsServerPath;

	@Value("${profilepics.folder.path}")
	String profilePicsFolderPath;

	@Autowired
	FrequencyAbstractFactory frequencyAbstractFactory;

	@Autowired
	UserPreferenceService userPreferenceService;

	@Autowired
	ConsumptionTemplateService consumptionTemplateService;

	@Autowired
	DiagnosisService diagnosisService;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	PatientService patientService;

	@Autowired
	DosageInfoService dosageInfoService;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Value("${dosage.medicine.image.default.url}")
	private String medicineDefaultURL;
	
	@Value("${product.owner}")
	String productOwner;


	private static final Logger logger = LoggerFactory.getLogger(Utility.class);
	
	public String getProductOwner(){
		return productOwner;
	}
	public String uploadProfileImage(@NonNull UserDetailsModel userDetailsModel, MultipartFile profileImage) {
		String oldImageName = ApplicationConstants.EMPTY_STRING;
		if (userDetailsModel.getImageUrl() != null) {
			oldImageName = getLastBitFromUrl(userDetailsModel.getImageUrl());
		}
		return uploadProfilepic(profileImage, userDetailsModel.getFirstName(), oldImageName);
	}

	public String updateDosageInfoOfprescriptions(long userId,String patietnPrefTime) {
		Patient patient = patientService.findByUserDetails_id(userId);
		if (patient != null) {
			List<Prescription> prescriptionList = prescriptionService.fetchPrescriptionsByPatientId(patient.getId());
			for (Prescription prescription : prescriptionList) {
				// condition represents, which one have no start date or endDate is
				// more than current date can consider active.
				if (prescription.getEndDate() == null && prescription.isCanPatientModify()) {
					updateDosageInfoOfPrescription(prescription, patietnPrefTime);
				}
			}
		}
		return "success";
	}

	public Prescription updateDosageInfoOfPrescription(Prescription prescription, String patietnPrefTime){

		// this I will do later
		Boolean updateFlag = false;
		for (DosageInfo dosageInfo : prescription.getDosageInfoList()) {// have to update dosageInfo "first consumption type" based
			// on patient user_pref
			// patietnPrefTime = "EAR";
			if (dosageInfo.getConsumptionTemplate() != null) {
				if (patietnPrefTime.equalsIgnoreCase(ApplicationConstants.EAR)) {
					if (ApplicationConstants.TWIMID.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							|| ApplicationConstants.TWILAT.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())) {
						ConsumptionTemplate consumptionTemplateTWIEAR = consumptionTemplateService
								.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.TWIEAR);
						dosageInfo.setConsumptionTemplate(consumptionTemplateTWIEAR);
						updateFlag = true;
					}
					if (ApplicationConstants.THRMID.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							|| ApplicationConstants.THRLAT.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())) {
						ConsumptionTemplate consumptionTemplateTHREAR = consumptionTemplateService
								.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.THREAR);
						dosageInfo.setConsumptionTemplate(consumptionTemplateTHREAR);
						updateFlag = true;
					}
					if (ApplicationConstants.CUSTOM.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							&& dosageInfo.getTime() != null) {
						if (dosageInfo.getTime().size() == 2) {
							ConsumptionTemplate cusConsumptionTemplateTWIEAR = consumptionTemplateService
									.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.TWIEAR);
							dosageInfo.setConsumptionTemplate(cusConsumptionTemplateTWIEAR);
							updateFlag = true;
						}
						if (dosageInfo.getTime().size() == 3) {
							ConsumptionTemplate cusConsumptionTemplateTHREAR = consumptionTemplateService
									.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.THREAR);
							dosageInfo.setConsumptionTemplate(cusConsumptionTemplateTHREAR);
							updateFlag = true;
						}
						if (dosageInfo.getTime().size() > 3) {
							logger.error("dosage intake time for this doasage info more than 3");
						}
					}
				}
				// patietnPrefTime = "MID";
				if (patietnPrefTime.equalsIgnoreCase(ApplicationConstants.MID)) {
					if (ApplicationConstants.TWIEAR.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							|| ApplicationConstants.TWILAT.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())) {
						ConsumptionTemplate consumptionTemplateTWIMID = consumptionTemplateService
								.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.TWIMID);
						dosageInfo.setConsumptionTemplate(consumptionTemplateTWIMID);
						updateFlag = true;
					}
					if (ApplicationConstants.THREAR.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							|| ApplicationConstants.THRLAT.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())) {
						ConsumptionTemplate consumptionTemplateTHRMID = consumptionTemplateService
								.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.THRMID);
						dosageInfo.setConsumptionTemplate(consumptionTemplateTHRMID);
						updateFlag = true;
					}
					if (ApplicationConstants.CUSTOM.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							&& dosageInfo.getTime() != null) {
						if (dosageInfo.getTime().size() == 2) {
							ConsumptionTemplate cusConsumptionTemplateTWIMid = consumptionTemplateService
									.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.TWIMID);
							dosageInfo.setConsumptionTemplate(cusConsumptionTemplateTWIMid);
							updateFlag = true;
						}
						if (dosageInfo.getTime().size() == 3) {
							ConsumptionTemplate cusConsumptionTemplateTHRMID = consumptionTemplateService
									.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.THRMID);
							dosageInfo.setConsumptionTemplate(cusConsumptionTemplateTHRMID);
							updateFlag = true;
						}

						if (dosageInfo.getTime().size() > 3) {
							logger.error("dosage intake time for this doasage info more than 3");
						}
					}
				}
				// patietnPrefTime = "LATE";
				if (patietnPrefTime.equalsIgnoreCase(ApplicationConstants.LATE)) {
					if (ApplicationConstants.TWIEAR.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							|| ApplicationConstants.TWIMID.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())) {
						ConsumptionTemplate consumptionTemplateTWILAT = consumptionTemplateService
								.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.TWILAT);
						dosageInfo.setConsumptionTemplate(consumptionTemplateTWILAT);
						updateFlag = true;
					}
					if (ApplicationConstants.THREAR.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							|| ApplicationConstants.THRMID.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())) {
						ConsumptionTemplate consumptionTemplateTHRLAT = consumptionTemplateService
								.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.THRLAT);
						dosageInfo.setConsumptionTemplate(consumptionTemplateTHRLAT);
						updateFlag = true;
					}
					if (ApplicationConstants.CUSTOM.equals(dosageInfo.getConsumptionTemplate().getConsumptionsKey())
							&& dosageInfo.getTime() != null) {
						if (dosageInfo.getTime().size() == 2) {
							ConsumptionTemplate cusConsumptionTemplateTWIMid = consumptionTemplateService
									.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.TWILAT);
							dosageInfo.setConsumptionTemplate(cusConsumptionTemplateTWIMid);
							updateFlag = true;
						}
						if (dosageInfo.getTime().size() == 3) {
							ConsumptionTemplate cusConsumptionTemplateTHRMID = consumptionTemplateService
									.fetchConsumptionTemplateByConsumptionsKey(ApplicationConstants.THRLAT);
							dosageInfo.setConsumptionTemplate(cusConsumptionTemplateTHRMID);
							updateFlag = true;
						}
						if (dosageInfo.getTime().size() > 3) {
							logger.error("dosage intake time for this doasage info more than 3");
						}
					}
				}
			}
		}
		if (updateFlag) {
			prescription.setUpdatedDate(LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC));
			prescription = prescriptionRepository.save(prescription);
		}
		return prescription;
	}

	public String roundUpTo2Decimal(String strNumber){
		DecimalFormat decimalFormat = new DecimalFormat(".##");
		return decimalFormat.format(Double.parseDouble(strNumber));
	}

	public String loggedUserDetailsString(String modelstr,String role){

		try {
			JSONObject jsonObject = new JSONObject(modelstr);
			JSONObject resJson = new JSONObject();
			if(!role.equalsIgnoreCase("admin")){
				String str = "id,userDetails";
				for(String str1:str.split(",")){
					if(jsonObject.has(str1)){
						resJson.put(str1, jsonObject.get(str1));
					}
				}
			}else{
				resJson.put("userDetails", jsonObject);
			}

			return resJson.toString();
		} catch (JSONException e) {
			logger.error("Exception occurred when getting logged in user details", e);
			return null;
		}
	}

	public Date tempStringDateToDateObjForImantic(String dateStr){
		//dateStr = "Sun Jan 01 07:00:00 UTC 2017";
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		Date date = null;
		try {
			date = (Date)formatter.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.getLocalizedMessage();
		}
		return date;
	}

	public  Date getMeYesterday(){
		return new Date(System.currentTimeMillis()-24*60*60*1000);
	}

	public Date getSixmonthsBackDate(){
		Calendar cal3 = Calendar.getInstance();
		cal3.add(Calendar.DATE, -10);
		Date date = new Date(cal3.getTimeInMillis());
		return date;
	}

	/*public Map<String,String> getCookieValue(HttpServletRequest request) {
		Cookie[] cookies = null;
		String cookievalue = ApplicationConstants.EMPTY_STRING;
		Map<String,String> cookieMap = new HashMap<String,String>();

		// Get an array of Cookies associated with the this domain
		cookies = request.getCookies();
		if (cookies != null) {

			for (int i = 0; i < cookies.length; i++) {
				if ((cookies[i].getName()).compareTo("username") == 0) {
					cookievalue = cookies[i].getValue();
					cookieMap.put(cookies[i].getName(), cookies[i].getValue());
					//return cookievalue;
				}
				if ((cookies[i].getName()).compareTo("role") == 0) {
					cookievalue = cookies[i].getValue();
					cookieMap.put(cookies[i].getName(), cookies[i].getValue());
				}

				if ((cookies[i].getName()).compareTo("providerid") == 0) {
					cookievalue = cookies[i].getValue();
					cookieMap.put(cookies[i].getName(), cookies[i].getValue());
				}
				if ((cookies[i].getName()).compareTo("pharmacistid") == 0) {
					cookievalue = cookies[i].getValue();
					cookieMap.put(cookies[i].getName(), cookies[i].getValue());
				}
				if ((cookies[i].getName()).compareTo("caregiverid") == 0) {
					cookievalue = cookies[i].getValue();
					cookieMap.put(cookies[i].getName(), cookies[i].getValue());
				}
				if ((cookies[i].getName()).compareTo("patientid") == 0) {
					cookievalue = cookies[i].getValue();
					cookieMap.put(cookies[i].getName(), cookies[i].getValue());
				}
				if ((cookies[i].getName()).compareTo("userid") == 0) {
					cookievalue = cookies[i].getValue();
					cookieMap.put(cookies[i].getName(), cookies[i].getValue());
				}
			}
		}
		return cookieMap;
	}
	*/
	/**
	 *  only un-softdeleted dosageinfo and diseaseInfo and consumergoodsinfo
	 *   list of that particular  prescription
	 * @return Prescription
	 */
	public Prescription fetchUnsoftDeletedPrescriptionInfo(Prescription prescription) {
		// to show only unsoftdeleted dosageinfo list of that particular
		// prescription
		if(prescription != null){
			List<DiseaseInfo> diseaseinfolist = new ArrayList<DiseaseInfo>();
			List<DosageInfo> dosageinfolist = new ArrayList<>();
			List<DeviceInfo> deviceinfolist = new ArrayList<DeviceInfo>();
			List<ConsumerGoodInfo> consumerGoodInfolist = new ArrayList<ConsumerGoodInfo>();

			// Disease list
			prescription.getDiseaseInfoList().forEach(diseaseinfo -> {
				if (!diseaseinfo.isDeleteflag()) {
					diseaseinfolist.add(diseaseinfo);
				}
			});
			prescription.setDiseaseInfoList(diseaseinfolist);
			// Dosage list
			for(DosageInfo dosageinfo : prescription.getDosageInfoList()){
				if (!dosageinfo.isDeleteflag()) {
					dosageinfolist.add(dosageinfo);

				}
			}
		/*prescription.getDosageInfoList().forEach(dosageinfo -> {
			if (!dosageinfo.isDeleteflag()) {
				dosageinfolist.add(dosageinfo);

			}
		});*/
			prescription.setDosageInfoList(dosageinfolist);
			// device list
			prescription.getDeviceInfoList().forEach(deviceinfo -> {
				if (!deviceinfo.isDeleteflag()) {
					deviceinfolist.add(deviceinfo);
				}
			});
			prescription.setDeviceInfoList(deviceinfolist);
			// consumergoods
			for(ConsumerGoodInfo commodityinfo: prescription.getCommodityInfoList()){
				if (!commodityinfo.isDeleteflag()) {
					consumerGoodInfolist.add(commodityinfo);
				}
			}
			prescription.setCommodityInfoList(consumerGoodInfolist);
		}
		return prescription;
	}

	public NotificationModel convertNotificationEntityToModel(Notification notification) {
		NotificationModel notificationModel = new NotificationModel();
		List<NotificationReceivedUserModel> notificationReceivedUserModelList = new ArrayList<NotificationReceivedUserModel>();
		if (notification != null) {
			BeanUtils.copyProperties(notification, notificationModel);

			if(notification.getBelongigngUserDetails()!= null){
				UserDetailsModel notificationBelongngUserDetailsModel = convertUserDetailsEntityToModel(notification.getBelongigngUserDetails());
				notificationModel.setBelongigngUserDetails(notificationBelongngUserDetailsModel);
			}
			for(NotificationReceivedUser notificationReceivedUser :notification.getNotificationReceivedUserList()){
				notificationReceivedUserModelList.add(convertNotificationReceivedUserEntityToModel(notificationReceivedUser));
			}
			notificationModel.setNotificationReceivedUserList(notificationReceivedUserModelList);
			if(notification.getVidoeCallSignatureModel() !=null){
				notificationModel.setVidoeCallSignatureModel(notification.getVidoeCallSignatureModel());
			}
		}
		return notificationModel;
	}
	public NotificationReceivedUserModel convertNotificationReceivedUserEntityToModel(NotificationReceivedUser notificationReceivedUser) {
		NotificationReceivedUserModel notificationReceivedUserModel = new NotificationReceivedUserModel();
		List<NotificationReceivedUserModel> notificationReceivedUserModelList = new ArrayList<NotificationReceivedUserModel>();

		if(notificationReceivedUser != null){
			BeanUtils.copyProperties(notificationReceivedUser, notificationReceivedUserModel);
			notificationReceivedUserModel.setUserDetails(convertUserDetailsEntityToModel(notificationReceivedUser.getUserDetails()));

			NotificationModel notificationModel = new NotificationModel();
			Notification notification = notificationReceivedUser.getNotification();
			if (notification != null) {
				BeanUtils.copyProperties(notification, notificationModel);

				if(notification.getBelongigngUserDetails()!= null){
					//UserDetailsModel notificationBelongngUserDetailsModel = convertUserDetailsEntityToModel(notification.getBelongigngUserDetails());
					//notificationModel.setBelongigngUserDetails(notificationBelongngUserDetailsModel);
					notificationModel.setBelongigngUserDetails(null);

					//to stop recursion issue we are not setting NotificationReceivedUserList
					notificationReceivedUserModelList.add(null);
				}
			}
			notificationReceivedUserModel.setNotification(null);
		}
		return notificationReceivedUserModel;
	}

	public AdherenceDataPointsModel convertAdherenceEntityToModel(AdherenceDataPoints adherence) {
		AdherenceDataPointsModel adherenceModel = new AdherenceDataPointsModel();
		List<DosageInfoModel> dosageInfoModelList = new ArrayList<>();
		if (adherence != null) {
			BeanUtils.copyProperties(adherence, adherenceModel);
			adherenceModel.setObservedTime(new DateUtility().convertLocalDateTimeToDateObject(adherence.getObservedTime()));
			adherenceModel.setPrescribedTime(new DateUtility().convertLocalDateTimeToDateObject(adherence.getPrescribedTime()));
			for(Adherence_DosageInfoMap adherence_DosageInfoMap : adherence.getAdherenceDosageInfoMapList()){
				Optional<DosageInfo> optionalDosageInfo = dosageInfoRepository.findById(adherence_DosageInfoMap.getDosageinfoId());
				dosageInfoModelList.add(optionalDosageInfo.isPresent()?convertDosageinfoEntityToModel(optionalDosageInfo.get()): null);
			}
			adherenceModel.setDosageInfoList(dosageInfoModelList);
		}
		return adherenceModel;
	}


	public RoleModel convertRoleEntityToModel(Role role){

		RoleModel roleModel = new RoleModel();
		if(role!= null){
			BeanUtils.copyProperties(role, roleModel);
		}
		return roleModel;
	}

	public Medicine convertMedicineModelToEntity(MedicineModel medicineModel){
		Medicine medicine= new Medicine();
		BeanUtils.copyProperties(medicineModel, medicine);
		return medicine;
	}

	public MedicineModel convertMedicineEntityToModel(Medicine medicine){
		MedicineModel medicineModel= new MedicineModel();
		BeanUtils.copyProperties(medicine, medicineModel);
		medicineModel.setImageURL(StringUtils.isEmpty(medicineModel.getImageURL()) ? medicineDefaultURL : medicineModel.getImageURL());
		return medicineModel;
	}

	public AdherenceDataPoints convertAdherenceModelToEntity(AdherenceDataPointsModel adherenceModel) {
		AdherenceDataPoints adherence = new AdherenceDataPoints();
		/*if (adherenceModel != null) {
			BeanUtils.copyProperties(adherenceModel, adherence);

			if (adherence.getDosageInfo() != null) {
				DosageInfo dosageInfo = new DosageInfo();
				BeanUtils.copyProperties(adherenceModel.getDosageInfo(),dosageInfo);
				if(adherenceModel.getDosageInfo().getMedicine()!= null){
					Medicine medicine = new Medicine();
					BeanUtils.copyProperties(adherenceModel.getDosageInfo().getMedicine(), medicine);
					dosageInfo.setMedicine(medicine);
				}
				adherence.setDosageInfo(dosageInfo);
			}
		}*/
		return adherence;
	}

	public int getMaxDigit(Integer... vals) {
		return new TreeSet<>(Arrays.asList(vals)).last();
	}

	public String uploadProfilepic(MultipartFile profileImage , String imageName,String oldImageName){

		try {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String imageNamewitTimestamp = imageName+Long.toString(timestamp.getTime());
			String realPathtoUploads = profilePicsFolderPath;
			if(! new File( realPathtoUploads).exists())
			{
				new File( realPathtoUploads).mkdir();
			}
			if(!oldImageName.equals(ApplicationConstants.EMPTY_STRING)){
				try
				{
					Files.deleteIfExists(Paths.get(realPathtoUploads+oldImageName));
				}
				catch(NoSuchFileException e)
				{
					logger.error("No such file/directory exists when uploading profile pic", e);
				}
			}

			String[] orgName = profileImage.getOriginalFilename().split("\\.");
			String imageNameWitExctension = imageNamewitTimestamp+"."+orgName[orgName.length-1];
			String imageNameWitPathUrl = profilepicsServerPath+imageNameWitExctension;

			File dest = new File(realPathtoUploads + imageNameWitExctension);
			profileImage.transferTo(dest);
			return imageNameWitPathUrl;

		}catch(Exception e){
			logger.error("error while uploading profilepic");
			e.getLocalizedMessage();
		}
		return null;
	}

	public String getLastBitFromUrl(final String url){
		return url.replaceFirst(".*/([^/?]+).*", "$1");
	}

	public UserDetails convertUserDetailsModelToEntity(UserDetailsModel userDetailsmodel) {
		UserDetails userDetails = new UserDetails();
		try {
			if (userDetailsmodel != null) {
				BeanUtils.copyProperties(userDetailsmodel, userDetails, "age");

				if (userDetailsmodel.getRole() != null) {
					Role role = new Role();
					BeanUtils.copyProperties(userDetailsmodel.getRole(), role);
					userDetails.setRole(role);
				}
				if (userDetailsmodel.getAddress() != null) {
					Address address = new Address();
					BeanUtils.copyProperties(userDetailsmodel.getAddress(), address);
					userDetails.setAddress(address);
				}
				if(userDetailsmodel.getAge() != null && !"".equals(userDetailsmodel.getAge().trim())){
					userDetails.setAge(Integer.parseInt(userDetailsmodel.getAge()));
				}
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return userDetails;
	}

	public  UserDetailsModel convertUserDetailsEntityToModel(UserDetails userDetails) {
		UserDetailsModel userDetailsModel = new UserDetailsModel();
		try {
			if (userDetails != null) {
				BeanUtils.copyProperties(userDetails, userDetailsModel);
				if (userDetails.getRole() != null) {
					RoleModel roleModel = new RoleModel();
					BeanUtils.copyProperties(userDetails.getRole(), roleModel);
					userDetailsModel.setRole(roleModel);
				}
				if (userDetails.getAddress() != null) {
					AddressModel addressModel = new AddressModel();
					BeanUtils.copyProperties(userDetails.getAddress(), addressModel);
					userDetailsModel.setAddress(addressModel);
				}
				userDetailsModel.setAge(String.valueOf(userDetails.getAge()));
				return userDetailsModel;
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}

		return null;
	}

	public  PharmacistModel convertPharmacistEntityToModel(Pharmacist pharmacist) {

		PharmacistModel pharmacistModel = new PharmacistModel();
		try {
			if (pharmacist != null) {
				BeanUtils.copyProperties(pharmacist, pharmacistModel);
				if (pharmacist.getUserDetails() != null) {
					UserDetailsModel pharmacistUserDetailsmodel = new UserDetailsModel();
					BeanUtils.copyProperties(pharmacist.getUserDetails(), pharmacistUserDetailsmodel);
					pharmacistModel.setUserDetails(pharmacistUserDetailsmodel);

					AddressModel pharmacistadressmodel = new AddressModel();
					if (pharmacist.getUserDetails().getAddress() != null) {
						BeanUtils.copyProperties(pharmacist.getUserDetails().getAddress(), pharmacistadressmodel);
						pharmacistModel.getUserDetails().setAddress(pharmacistadressmodel);
					}
				}
				if (pharmacist.getUserDetails().getRole() != null) {
					RoleModel rolemodel = new RoleModel();
					BeanUtils.copyProperties(pharmacist.getUserDetails().getRole(), rolemodel);
					pharmacistModel.getUserDetails().setRole(rolemodel);
				}
				pharmacistModel.getUserDetails().setAge(String.valueOf(pharmacist.getUserDetails().getAge()));
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return pharmacistModel;
	}

	public  CaregiverModel convertCaregiverEntityToModel(Caregiver caregiver) {

		CaregiverModel caregiverModel = new CaregiverModel();
		try {
			if (caregiver != null) {
				BeanUtils.copyProperties(caregiver, caregiverModel);
				if (caregiver.getUserDetails() != null) {
					UserDetailsModel providerUserDetailsmodel = new UserDetailsModel();
					BeanUtils.copyProperties(caregiver.getUserDetails(), providerUserDetailsmodel);
					caregiverModel.setUserDetails(providerUserDetailsmodel);

					AddressModel provideradressmodel = new AddressModel();
					if (caregiver.getUserDetails().getAddress() != null) {
						BeanUtils.copyProperties(caregiver.getUserDetails().getAddress(), provideradressmodel);
						caregiverModel.getUserDetails().setAddress(provideradressmodel);
					}
				}
				if (caregiver.getUserDetails().getRole() != null) {
					RoleModel rolemodel = new RoleModel();
					BeanUtils.copyProperties(caregiver.getUserDetails().getRole(), rolemodel);
					caregiverModel.getUserDetails().setRole(rolemodel);
				}
				caregiverModel.getUserDetails().setAge(String.valueOf(caregiver.getUserDetails().getAge()));
				//New change start
				if (caregiver.getProviders() != null) {
					Set<ProviderModel> providerModelSet = new LinkedHashSet<ProviderModel>();
					caregiver.getProviders().forEach(provider -> {
						ProviderModel providerModel = convertProviderEntityToModel(provider);
						providerModelSet.add(providerModel);
					});
					caregiverModel.setProviders(providerModelSet);
				}
				//New change end
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return caregiverModel;
	}


	public PatientModel convertPatientEntityTopatientModel(Patient persistedpatient) {

		PatientModel patientModel = new PatientModel();
		try {
			if (persistedpatient != null) {
				BeanUtils.copyProperties(persistedpatient, patientModel);

				if (persistedpatient.getUserDetails() != null) {
					UserDetailsModel patientUserDetailsmodel =  convertUserDetailsEntityToModel(persistedpatient.getUserDetails());
					patientModel.setUserDetails(patientUserDetailsmodel);
				}
				if(persistedpatient.getPrescriptions().size()> 0){
					List<PrescriptionModel> PrescriptionModelList = new ArrayList<>();
					persistedpatient.getPrescriptions().forEach(prescription ->{
						PrescriptionModel  prescriptionModel  =convertPrescriptionEntityToModel(prescription);
						prescriptionModel.setPatient(null);//To break recursion
						PrescriptionModelList.add(prescriptionModel);
					});
					patientModel.setPrescriptions(PrescriptionModelList);
				}
				if (persistedpatient.getProviders() != null) {
					Set<ProviderModel> providerModelset = new LinkedHashSet<ProviderModel>();
					persistedpatient.getProviders().forEach(provider -> {
						ProviderModel providermodel = convertProviderEntityToModel(provider);
						providerModelset.add(providermodel);
					});
					patientModel.setProviders(providerModelset);
				}
				if (persistedpatient.getPharmacists() != null) {
					Set<PharmacistModel> pharmacistModelset = new LinkedHashSet<PharmacistModel>();
					persistedpatient.getPharmacists().forEach(Pharmacist -> {
						PharmacistModel pharmacistmodel = convertPharmacistEntityToModel(Pharmacist);
						pharmacistModelset.add(pharmacistmodel);
					});
					patientModel.setPharmacists(pharmacistModelset);
				}
				//multiple caregiver changes start
				if (persistedpatient.getCaregivers() != null) {
					Set<CaregiverModel> caregiverModelSet = new LinkedHashSet<CaregiverModel>();
					persistedpatient.getCaregivers().forEach(caregiver -> {
						CaregiverModel caregivermodel = convertCaregiverEntityToModel(caregiver);
						caregiverModelSet.add(caregivermodel);
					});
					patientModel.setCaregivers(caregiverModelSet);
				}
				//end
				if (persistedpatient.getUserDetails().getRole() != null) {
					RoleModel rolemodel = new RoleModel();
					BeanUtils.copyProperties(persistedpatient.getUserDetails().getRole(), rolemodel);
					patientModel.getUserDetails().setRole(rolemodel);
				}
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return patientModel;
	}
	public  ProviderModel convertProviderEntityToModel(Provider  provider) {
		try{
			ProviderModel providerModel = new ProviderModel();
			BeanUtils.copyProperties(provider, providerModel);
			UserDetailsModel provderUserDetails = convertUserDetailsEntityToModel(provider.getUserDetails());
			providerModel.setUserDetails(provderUserDetails);
			//converting provider's patient entity to patient model
			/*Set<PatientModel> patientModelSet  = new LinkedHashSet<PatientModel>();
			for(Patient patient : provider.getPatients()){
				patientModelSet.add(convertPatientEntityTopatientModel(patient));
			}*/
			providerModel.setPatients(null);
			return providerModel;
		}catch(Exception e){
			e.getLocalizedMessage();
		}
		return null;
	}

	public  Provider convertProviderModelToEntity(ProviderModel  providerModel) {
		try{
			Provider provider = new Provider();
			BeanUtils.copyProperties(providerModel, provider);
			UserDetails provderUserDetails = convertUserDetailsModelToEntity(providerModel.getUserDetails());
			provider.setUserDetails(provderUserDetails);
			return provider;
		}catch(Exception e){
			e.getLocalizedMessage();
		}
		return null;
	}

	public Patient convertPatientModelTopatientEntity(PatientModel patientModel) {
		Patient patient = new Patient();
		try {
			BeanUtils.copyProperties(patientModel, patient);
			UserDetails patientUserDetails = convertUserDetailsModelToEntity(patientModel.getUserDetails());
			patient.setUserDetails(patientUserDetails);

			Set<Provider> providerset = new LinkedHashSet<Provider>();
			if (patientModel.getProviders() != null) {
				patientModel.getProviders().forEach(providermodel -> {
					Provider provider = convertProviderModelToEntity(providermodel);
					providerset.add(provider);
				});
				patient.setProviders(providerset);
			}
			// patient.setProviders(providerset);
			if (patientModel.getPharmacists() != null) {
				Set<Pharmacist> pharmacistset = new LinkedHashSet<Pharmacist>();
				patientModel.getPharmacists().forEach(Pharmasistmodel -> {
					Pharmacist pharmacist = convertPharmasistModelToEntity(Pharmasistmodel);
					pharmacistset.add(pharmacist);
				});
				patient.setPharmacists(pharmacistset);
			}
			if (patientModel.getCaregivers() != null) {
				Set<Caregiver> caregiverset = new LinkedHashSet<Caregiver>();
				patientModel.getCaregivers().forEach(caregivermodel -> {
					Caregiver caregiver = convertCaregiverModelToEntity(caregivermodel);
					caregiverset.add(caregiver);
				});
				patient.setCaregivers(caregiverset);
			}
			if(patientModel.getUserDetails().getAge() != null && !"".equals(patientModel.getUserDetails().getAge().trim())){
				patient.getUserDetails().setAge(Integer.parseInt(patientModel.getUserDetails().getAge()));
			}

		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return patient;
	}

	public Pharmacist convertPharmasistModelToEntity(PharmacistModel PharmasistModel){
		Pharmacist pharmacist = new Pharmacist();
		BeanUtils.copyProperties(PharmasistModel, pharmacist);
		if(PharmasistModel.getUserDetails()!= null){
			UserDetails pharmacistUserDetails = convertUserDetailsModelToEntity(PharmasistModel.getUserDetails());
			pharmacist.setUserDetails(pharmacistUserDetails);
		}
		return pharmacist;
	}
	public String getSimpleDate(LocalDateTime date){
		//return new SimpleDateFormat("dd.MM.yyyy - hh.mm.ss aa").format(date);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy - HH.mm.ss");
		return date.format(formatter);
	}

	public DeviceInfoModel convertDeviceinfoEntityToModel(DeviceInfo deviceInfo){
		// dosageInfo.setPrescription(prescription);
		DeviceInfoModel deviseInfoModel = new DeviceInfoModel();
		BeanUtils.copyProperties(deviceInfo, deviseInfoModel);
		if (deviceInfo.getDevice() != null) {
			DeviceModel devisemodel = new DeviceModel();
			BeanUtils.copyProperties(deviceInfo.getDevice(), devisemodel);
			deviseInfoModel.setDevice(devisemodel);
		}
		// add devicetime
		if (deviceInfo.getTime() != null && deviceInfo.getTime().size() > 0) {
			ArrayList<String> devisetimings = new ArrayList<String>();
			deviceInfo.getTime().forEach(time->{
				devisetimings.add(time.trim());
			});
			deviseInfoModel.setTime(devisetimings);
			if(deviceInfo.getDeviceNotificationList() != null){

				DeviceNotificationModel deviceNotificationModel = new DeviceNotificationModel();
				List<DeviceSpecificNotificationModel> deviceSpecificNotificationModelList = new ArrayList<>();

				deviceInfo.getDeviceNotificationList().forEach(deviceNotification ->{
					GreaterThanNotificationModel greaterThan = new GreaterThanNotificationModel();
					LessThanNotificationModel lessThan = new LessThanNotificationModel();

					if(deviceNotification.getGreaterThanReading1() != null){
						greaterThan.setReading(deviceNotification.getGreaterThanReading1());
						if(deviceNotification.getGreaterThanReading2() != null){
							greaterThan.setReading2(deviceNotification.getGreaterThanReading2());
						}
						deviceNotificationModel.setGreaterThan(greaterThan);
					}

					if(deviceNotification.getLessThanReading1() != null){
						lessThan.setReading(deviceNotification.getLessThanReading1());
						if(deviceNotification.getLessThanReading2() != null){
							lessThan.setReading2(deviceNotification.getLessThanReading2());
						}
						deviceNotificationModel.setLessThan(lessThan);
					}
					DeviceSpecificNotificationModel deviceSpecificNotificationModel= new DeviceSpecificNotificationModel();

					deviceSpecificNotificationModel.setId(deviceNotification.getId());
					deviceSpecificNotificationModel.setExpectedTime(deviceNotification.getExpectedTime());
					if(deviceNotification.getCountryCode()!= null){
						deviceSpecificNotificationModel.setCountryCode(deviceNotification.getCountryCode());
					}
					deviceSpecificNotificationModel.setNotificationType(deviceNotification.getNotificationType());
					deviceSpecificNotificationModel.setNotificationValue(deviceNotification.getNotificationValue());
					deviceSpecificNotificationModelList.add(deviceSpecificNotificationModel);

					deviceNotificationModel.setNotificationList(deviceSpecificNotificationModelList);

				});
				deviseInfoModel.setDeviceNotification(deviceNotificationModel);
			}
		}
		return deviseInfoModel;
	}


	public DeviceStatusModel convertDosageDeviceEntityToModel(DeviceStatus deviceStatus){
		DeviceStatusModel deviceStatusModel = new DeviceStatusModel();
		if(deviceStatus != null){
			BeanUtils.copyProperties(deviceStatus, deviceStatusModel);
			deviceStatusModel.setUserDetails(convertUserDetailsEntityToModel(deviceStatus.getUserDetails()));
		}
		return deviceStatusModel;
	}

	public DosageDeviceModel convertDosageDeviceEntityToModel(DosageDevice dosageDevice){
		DosageDeviceModel dosageDeviceModel = new DosageDeviceModel();
		if(dosageDevice != null){
			BeanUtils.copyProperties(dosageDevice, dosageDeviceModel);
		}
		return dosageDeviceModel;
	}

	public DosageInfoModel convertDosageinfoEntityToModel(DosageInfo dosageInfo){

		// dosageInfo.setPrescription(prescription);
		DosageInfoModel dosageInfoModel = new DosageInfoModel();
		BeanUtils.copyProperties(dosageInfo, dosageInfoModel);
		MedicineModel medicinemodel = new MedicineModel();
		if (dosageInfo.getMedicine() != null) {
			BeanUtils.copyProperties(dosageInfo.getMedicine(), medicinemodel);
			medicinemodel.setImageURL(StringUtils.isEmpty(medicinemodel.getImageURL()) ? medicineDefaultURL : medicinemodel.getImageURL());
			dosageInfoModel.setMedicine(medicinemodel);
		}
		if (dosageInfo.getConsumptionTemplate() != null) {
			try {
				FristConsumptionTypeModel fristConsumptionType = new FristConsumptionTypeModel();
				FrequencyPerDayModel frequencyPerDay= new FrequencyPerDayModel();
				FirstConsumptionTimeModel firstConsumptionTime= new FirstConsumptionTimeModel();
				frequencyPerDay.setName(dosageInfo.getConsumptionTemplate().getFrequencyName());
				frequencyPerDay.setId(dosageInfo.getConsumptionTemplate().getFrequencyId());
				fristConsumptionType.setFrequencyPerDay(frequencyPerDay);

				firstConsumptionTime.setDescription(dosageInfo.getConsumptionTemplate().getConsumptionsDesc());
				firstConsumptionTime.setName(dosageInfo.getConsumptionTemplate().getConsumptionsKey());
				firstConsumptionTime.setId(dosageInfo.getConsumptionTemplate().getConsumptionsId());
				firstConsumptionTime.setTimes(dosageInfo.getConsumptionTemplate().getConsumptionsTimes());
				fristConsumptionType.setFirstConsumptionTime(firstConsumptionTime);

				dosageInfoModel.setFristConsumptionType(fristConsumptionType);
			} catch (Exception e) {
				logger.error("Exception occured in fristConsumptionType in Dosage info...." + e.getMessage());
			}
		}
		// add dosagetime
		// saving time string as "9:00 am in db while fetching time converting this only time to Instant time and return"
		if (dosageInfo.getTime() != null && dosageInfo.getTime().size() > 0) {
			ArrayList<String> dosagetimings = new ArrayList<String>();

			dosageInfo.getTime().forEach(time->{
				dosagetimings.add(time.trim());
			});
			dosageInfoModel.setTime(dosagetimings);

			if(dosageInfo.getDosageNotificationList()!= null){
				List<DosageNotificationModel> dosageNotificationModelList = new ArrayList<>();
				dosageInfo.getDosageNotificationList().forEach(dosageNotification ->{
					DosageNotificationModel dosageNotificationModel= new DosageNotificationModel();
					BeanUtils.copyProperties(dosageNotification, dosageNotificationModel);
					//dosageNotificationModel.setDosageInfo(dosageInfoModel);
					dosageNotificationModelList.add(dosageNotificationModel);
				});
				dosageInfoModel.setDosageNotificationList(dosageNotificationModelList);
			}
		}
		return dosageInfoModel;
	}

	public PrescriptionModel convertPrescriptionEntityToModel(Prescription prescription){
		PrescriptionModel prescriptionModel = new PrescriptionModel();
		try {
			if (prescription != null && prescription.getUpdatedDate() != null) {
				BeanUtils.copyProperties(prescription, prescriptionModel);
				prescriptionModel.setDate(getSimpleDate(prescription.getUpdatedDate()));
				String problem = prescription.getDiagnosisList().stream().map(dia -> dia.getName()).collect(Collectors.joining(","));
				prescriptionModel.setProblem(StringUtils.isBlank(problem) ? StringUtils.EMPTY : problem);
				//prescriptionModel.setDate(prescription.getDate());
				PatientModel patientModel = new PatientModel();
				//Diagnosis convertion
				List<DiagnosisModel> diagnosisModelSet = new ArrayList<DiagnosisModel>();
				if(prescription.getDiagnosisList() != null){
					for(Diagnosis diagnosis :prescription.getDiagnosisList()){
						DiagnosisModel diagnosisModel = new DiagnosisModel();
						BeanUtils.copyProperties(diagnosis, diagnosisModel);
						diagnosisModelSet.add(diagnosisModel);
					}
				}
				prescriptionModel.setDiagnosisList(diagnosisModelSet);
				//patient convertion
				if (prescription.getPatient() != null) {
					Patient patietEntity = prescription.getPatient();
					BeanUtils.copyProperties(patietEntity, patientModel);

					if (patietEntity.getUserDetails() != null) {
						UserDetailsModel patientUserDetailsmodel =  convertUserDetailsEntityToModel(patietEntity.getUserDetails());
						patientModel.setUserDetails(patientUserDetailsmodel);
					}
					if(patietEntity.getPrescriptions().size()> 0){
						patientModel.setPrescriptions(null);// becoz of recursion
					}
					if (patietEntity.getProviders() != null) {
						Set<ProviderModel> providerModelset = new LinkedHashSet<ProviderModel>();
						patietEntity.getProviders().forEach(provider -> {
							ProviderModel providermodel = convertProviderEntityToModel(provider);
							providerModelset.add(providermodel);
						});
						patientModel.setProviders(providerModelset);
					}
					if (patietEntity.getPharmacists() != null) {
						Set<PharmacistModel> pharmacistModelset = new LinkedHashSet<PharmacistModel>();
						patietEntity.getPharmacists().forEach(Pharmacist -> {
							PharmacistModel pharmacistmodel = convertPharmacistEntityToModel(Pharmacist);
							pharmacistModelset.add(pharmacistmodel);
						});
						patientModel.setPharmacists(pharmacistModelset);
					}
					//multiple caregiver changes start
					if (patietEntity.getCaregivers() != null) {
						Set<CaregiverModel> caregiverModelSet = new LinkedHashSet<CaregiverModel>();
						patietEntity.getCaregivers().forEach(caregiver -> {
							CaregiverModel caregivermodel = convertCaregiverEntityToModel(caregiver);
							caregiverModelSet.add(caregivermodel);
						});
						patientModel.setCaregivers(caregiverModelSet);
					}
					//end
					if (patietEntity.getUserDetails().getRole() != null) {
						RoleModel rolemodel = new RoleModel();
						BeanUtils.copyProperties(patietEntity.getUserDetails().getRole(), rolemodel);
						patientModel.getUserDetails().setRole(rolemodel);
					}
					//}
					prescriptionModel.setPatient(patientModel);
				}

				//latest multiple diseases support
				List<DiseaseInfoModel> diseaseInfoModelList = new ArrayList<>();
				prescription.getDiseaseInfoList().forEach(diseaseInfo ->{
					DiseaseInfoModel diseaseInfoModel = new DiseaseInfoModel();
					BeanUtils.copyProperties(diseaseInfo, diseaseInfoModel);
					if(diseaseInfo.getDisease() != null){
						DiseaseModel diseaseModel = new DiseaseModel();
						BeanUtils.copyProperties(diseaseInfo.getDisease(), diseaseModel);
						diseaseInfoModel.setDisease(diseaseModel);
						diseaseInfoModelList.add(diseaseInfoModel);
					}
				});

				PrescriptionStatusModel PrescriptionStatusModel = new PrescriptionStatusModel();
				if (prescription.getPrescriptionStatus() != null) {
					BeanUtils.copyProperties(prescription.getPrescriptionStatus(), PrescriptionStatusModel);
					prescriptionModel.setPrescriptionStatus(PrescriptionStatusModel);
				}
				List<DosageDeviceModel> dosageDeviceModelList = new ArrayList<DosageDeviceModel>();
				prescription.getDosageDevices().forEach(dosageDevice -> {
					DosageDeviceModel dosageDeviceModel = convertDosageDeviceEntityToModel(dosageDevice);
					dosageDeviceModelList.add(dosageDeviceModel);
				});

				List<DosageInfoModel> dosageInfoModelList = new ArrayList<>();
				prescription.getDosageInfoList().forEach(dosageInfo -> {
					DosageInfoModel dosageInfoModel = convertDosageinfoEntityToModel(dosageInfo);
					dosageInfoModelList.add(dosageInfoModel);
				});

				List<DeviceInfoModel> deviceInfoModellList = new ArrayList<>();
				prescription.getDeviceInfoList().forEach(deviceInfo -> {

					DeviceInfoModel deviceInfoModel = convertDeviceinfoEntityToModel(deviceInfo);
					deviceInfoModellList.add(deviceInfoModel);
				});

				List<ConsumerGoodInfoModel> consumerGoodInfoModelList = new ArrayList<>();
				prescription.getCommodityInfoList().forEach(commodityinfo -> {
					// dosageInfo.setPrescription(prescription);
					ConsumerGoodInfoModel consumerGoodInfoModel = new ConsumerGoodInfoModel();
					BeanUtils.copyProperties(commodityinfo, consumerGoodInfoModel);
					ConsumerGoodsModel consumerGoodsModel = new ConsumerGoodsModel();
					if (commodityinfo.getCommodity() != null) {
						BeanUtils.copyProperties(commodityinfo.getCommodity(), consumerGoodsModel);
						consumerGoodInfoModel.setCommodity(consumerGoodsModel);
						consumerGoodInfoModelList.add(consumerGoodInfoModel);
					}
					// add consumergoodtime
					if (commodityinfo.getTime() != null && commodityinfo.getTime().size() > 0) {
						ArrayList<String> consumergoodstimings = new ArrayList<String>();

						commodityinfo.getTime().forEach(time->{
							consumergoodstimings.add(time.trim());
						});
						consumerGoodInfoModel.setTime(consumergoodstimings);
					}
				});

				prescriptionModel.setDosageDevices(dosageDeviceModelList);
				prescriptionModel.setDiseaseInfoList(diseaseInfoModelList);
				prescriptionModel.setDeviceInfoList(deviceInfoModellList);
				prescriptionModel.setDosageInfoList(dosageInfoModelList);
				prescriptionModel.setCommodityInfoList(consumerGoodInfoModelList);
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return prescriptionModel;
	}

	public DosageNotification convertDosageNotificationModelToEntity(DosageNotificationModel dosageNotificationModel) {
		DosageNotification dosageNotification = null;
		if(dosageNotificationModel != null){
			dosageNotification = new DosageNotification();
			BeanUtils.copyProperties(dosageNotificationModel,dosageNotification );
			return dosageNotification;
		}
		return null;
	}

	public DosageNotificationModel convertDosageNotificationEntityToModel(DosageNotification dosageNotification) {
		DosageNotificationModel dosageNotificationModel = null;
		if(dosageNotification != null){
			dosageNotificationModel = new DosageNotificationModel();
			BeanUtils.copyProperties(dosageNotification, dosageNotificationModel);
			return dosageNotificationModel;
		}

		return null;
	}

	public DeviceInfo convertDeviceinfoModelToEntity(DeviceInfoModel deviseInfoModel) {

		if (deviseInfoModel != null) {
			DeviceInfo deviceInfo = new DeviceInfo();
			BeanUtils.copyProperties(deviseInfoModel, deviceInfo);
			Device device = new Device();
			BeanUtils.copyProperties(deviseInfoModel.getDevice(), device);
			deviceInfo.setDevice(device);
			// add devisetime
			if (deviseInfoModel.getTime() != null && deviseInfoModel.getTime().size() > 0) {
				ArrayList<String> devisetimings = new ArrayList<String>();

				for(String dateObj:deviseInfoModel.getTime()){
					try{
						devisetimings.add(dateObj);
					}catch(IllegalArgumentException ex){
						ex.getMessage();
					}
				}
				deviceInfo.setTime(devisetimings);
				if (deviseInfoModel.getDeviceNotification() != null
						&& (deviseInfoModel.getDeviceNotification().getGreaterThan() != null
						|| deviseInfoModel.getDeviceNotification().getLessThan() != null)) {
					List<DeviceNotification> greaterDeviceNotificationList = new ArrayList<>();
					List<DeviceNotification> lesstThanDeviceNotificationList = new ArrayList<>();
					List<DeviceNotification> deviceNotificationList = new ArrayList<>();

					if (deviseInfoModel.getDeviceNotification().getGreaterThan() != null && deviseInfoModel.getDeviceNotification().getNotificationList() != null) {
						deviseInfoModel.getDeviceNotification().getNotificationList()
						               .forEach(notificationModel -> {
							               DeviceNotification deviceNotification = new DeviceNotification();

							               deviceNotification.setGreaterThanReading1(
									               deviseInfoModel.getDeviceNotification().getGreaterThan().getReading());

							               if(deviseInfoModel.getDeviceNotification().getGreaterThan().getReading2() != null){
								               deviceNotification.setGreaterThanReading2(deviseInfoModel.getDeviceNotification().getGreaterThan().getReading2());
							               }

							               if (deviseInfoModel.getDeviceNotification().getLessThan() != null && deviseInfoModel.getDeviceNotification().getNotificationList() != null) {
								               deviseInfoModel.getDeviceNotification().getNotificationList()
								                              .forEach(lessThannotificationModel -> {
									                              deviceNotification.setLessThanReading1(
											                              deviseInfoModel.getDeviceNotification().getLessThan().getReading());

									                              if(deviseInfoModel.getDeviceNotification().getLessThan().getReading2() != null){
										                              deviceNotification.setLessThanReading2(deviseInfoModel.getDeviceNotification().getLessThan().getReading2());
									                              }
								                              });
							               }

							               deviceNotification
									               .setId(notificationModel.getId());

							               if(notificationModel.getCountryCode() != null){
								               deviceNotification.setCountryCode(notificationModel.getCountryCode());
							               }
							               deviceNotification.setExpectedTime(notificationModel.getExpectedTime());
							               deviceNotification.setNotificationType(notificationModel.getNotificationType());
							               deviceNotification.setNotificationValue(notificationModel.getNotificationValue());
							               deviceNotification.setDeviceInfo(deviceInfo);// FK

							               //greaterDeviceNotificationList.add(deviceNotification);
							               deviceNotificationList.add(deviceNotification);
						               });
					}
					deviceInfo.setDeviceNotificationList(deviceNotificationList);
				}
			}
			return deviceInfo;
		}
		return null;
	}
	public DosageDevice convertDosageDevicesModelToEntity(DosageDeviceModel dosageDeviceModel){
		DosageDevice dosageDevice = new DosageDevice();
		if(dosageDeviceModel != null){
			BeanUtils.copyProperties(dosageDeviceModel, dosageDevice);
		}
		return dosageDevice;
	}

	public DosageInfo convertDosageinfoModelToEntity(DosageInfoModel dosageInfoModel){
		if(dosageInfoModel != null){
			DosageInfo dosageInfo = new DosageInfo();
			BeanUtils.copyProperties(dosageInfoModel, dosageInfo);
			Medicine medicine = new Medicine();
			BeanUtils.copyProperties(dosageInfoModel.getMedicine(), medicine);
			dosageInfo.setMedicine(medicine);
			//dosageInfoList.add(dosageInfo);
			//consumption template added to entity
			if(dosageInfoModel.getFristConsumptionType() != null && dosageInfoModel.getFristConsumptionType().getFirstConsumptionTime() != null){
				ConsumptionTemplate fristConsumptionFrequency = consumptionFrequencyRepository
						.findByconsumptionsKey(dosageInfoModel.getFristConsumptionType().getFirstConsumptionTime().getName());
				dosageInfo.setConsumptionTemplate(fristConsumptionFrequency);
			}
			// setting dosagetime

			if (dosageInfoModel.getTime() != null && dosageInfoModel.getTime().size() > 0) {
				ArrayList<String> dosagetimings = new ArrayList<String>();

				for(String dateObj:dosageInfoModel.getTime()){
					try{
						dosagetimings.add(dateObj);
					}catch(IllegalArgumentException ex){
						ex.getMessage();
					}
				}
				dosageInfo.setTime(dosagetimings);
				//setting DosageNotification
				if(dosageInfoModel.getDosageNotificationList()!= null){
					List<DosageNotification> dosageNotificationList = new ArrayList<>();
					dosageInfoModel.getDosageNotificationList().forEach(dosageNotificationModel ->{
						DosageNotification dosageNotification= new DosageNotification();
						BeanUtils.copyProperties(dosageNotificationModel, dosageNotification);
						dosageNotification.setDosageInfo(dosageInfo);//setting dosageinfo FK relation
						dosageNotificationList.add(dosageNotification);
					});
					dosageInfo.setDosageNotificationList(dosageNotificationList);
				}
			}
			return dosageInfo;
		}
		return null;

	}
	public DiagnosisModel convertDiagnosisEntityToModel(Diagnosis diagnosis){
		DiagnosisModel diagnosisModel = null;
		if(diagnosis != null){
			diagnosisModel = new DiagnosisModel();
			BeanUtils.copyProperties(diagnosis, diagnosisModel);
		}
		return diagnosisModel;
	}

	public Diagnosis convertDiagnosisModelToEntity(DiagnosisModel diagnosisModel){
		Diagnosis diagnosis= null;
		if(diagnosisModel != null){
			diagnosis= new Diagnosis();
			BeanUtils.copyProperties(diagnosisModel, diagnosis);
		}
		return diagnosis;
	}

	public  Prescription convertPrescriptionModelToEntity(PrescriptionModel prescriptionModel) {

		Prescription prescription = new Prescription();
		try {
			BeanUtils.copyProperties(prescriptionModel, prescription);
			//Diagnosis conversion
			List<Diagnosis> diagnosisList = new ArrayList<Diagnosis>();
			if(prescriptionModel.getDiagnosisList() != null){
				for(DiagnosisModel diagnosisModel : prescriptionModel.getDiagnosisList()){
					Diagnosis diagnosis= new Diagnosis();
					BeanUtils.copyProperties(diagnosisModel, diagnosis);
					diagnosisList.add(diagnosis);
				}
			}
			prescription.setDiagnosisList(diagnosisList);
			Patient patient = convertPatientModelTopatientEntity(prescriptionModel.getPatient());
			prescription.setPatient(patient);

			//latest multiple diseases support
			List<DiseaseInfo> diseaseInfoList = new ArrayList<DiseaseInfo>();
			if (prescriptionModel.getDiseaseInfoList() != null || (prescriptionModel.getDiseaseInfoList().size() <= 0)) {
				prescriptionModel.getDiseaseInfoList().forEach(diseaseInfoModel ->{
					DiseaseInfo diseaseInfo = new DiseaseInfo();
					BeanUtils.copyProperties(diseaseInfoModel, diseaseInfo);
					if(diseaseInfoModel.getDisease() != null){
						Disease disease = new Disease();
						BeanUtils.copyProperties(diseaseInfoModel.getDisease(), disease);
						diseaseInfo.setDisease(disease);
						diseaseInfoList.add(diseaseInfo);
					}
				});
			}
			if (prescriptionModel.getPrescriptionStatus() != null) {
				PrescriptionStatus PrescriptionStatus = new PrescriptionStatus();
				BeanUtils.copyProperties(prescriptionModel.getPrescriptionStatus(), PrescriptionStatus);
				prescription.setPrescriptionStatus(PrescriptionStatus);
			}
			List<DosageDevice> dosageDevices = new ArrayList<>();
			if (prescriptionModel.getDosageDevices() != null || (prescriptionModel.getDosageDevices().size() <= 0)) {
				for(DosageDeviceModel dosageDeviceModel :prescriptionModel.getDosageDevices()){
					DosageDevice dosageDevice = convertDosageDevicesModelToEntity(dosageDeviceModel);
					dosageDevice.setPrescription(prescription);
					dosageDevices.add(dosageDevice);
				}
			}

			List<DosageInfo> dosageInfoList = new ArrayList<>();
			//need to change here
			if (prescriptionModel.getDosageInfoList() != null || (prescriptionModel.getDosageInfoList().size() <= 0)) {
				for(DosageInfoModel dosageInfoModel :prescriptionModel.getDosageInfoList()){
					DosageInfo dosageInfo = convertDosageinfoModelToEntity(dosageInfoModel);
					dosageInfoList.add(dosageInfo);
				}
			}
			List<DeviceInfo> deviceInfolList = new ArrayList<>();

			for(DeviceInfoModel deviseInfoModel :prescriptionModel.getDeviceInfoList()){
				DeviceInfo deviceInfo = convertDeviceinfoModelToEntity(deviseInfoModel);
				deviceInfolList.add(deviceInfo);
			}

			List<ConsumerGoodInfo> consumerGoodInfoList = new ArrayList<>();
			prescriptionModel.getCommodityInfoList().forEach(commodityinfoModel -> {
				ConsumerGoodInfo consumerGoodInfo = new ConsumerGoodInfo();
				BeanUtils.copyProperties(commodityinfoModel, consumerGoodInfo);
				ConsumerGoods consumerGoods = new ConsumerGoods();
				BeanUtils.copyProperties(commodityinfoModel.getCommodity(), consumerGoods);
				consumerGoodInfo.setCommodity(consumerGoods);
				consumerGoodInfoList.add(consumerGoodInfo);
				// add dosagetime
				if (commodityinfoModel.getTime() != null && commodityinfoModel.getTime().size() > 0) {
					ArrayList<String> commoditytimings = new ArrayList<String>();
					for (String dateObj : commodityinfoModel.getTime()) {
						try {
							commoditytimings.add(dateObj);
						} catch (IllegalArgumentException ex) {
							ex.getMessage();
						}
					}
					consumerGoodInfo.setTime(commoditytimings);
				}

			});
			//dosageDevices
			prescription.setDosageDevices(dosageDevices);
			prescription.setDiseaseInfoList(diseaseInfoList);
			prescription.setDeviceInfoList(deviceInfolList);
			prescription.setDosageInfoList(dosageInfoList);
			prescription.setCommodityInfoList(consumerGoodInfoList);
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return prescription;
	}
	//convert caregiver Model to Entity
	public  Caregiver convertCaregiverModelToEntity(CaregiverModel caregiverModel){

		try{
			if(caregiverModel!= null){
				Caregiver caregiver = new Caregiver();
				BeanUtils.copyProperties(caregiverModel, caregiver);
				UserDetails caregiverUserDetails = convertUserDetailsModelToEntity(caregiverModel.getUserDetails());
				caregiver.setUserDetails(caregiverUserDetails);

				//New change start
				if (caregiverModel.getProviders() != null) {
					Set<Provider> providerSet = new LinkedHashSet<Provider>();
					caregiverModel.getProviders().forEach(providerModel -> {
						Provider provider = convertProviderModelToEntity(providerModel);
						providerSet.add(provider);
					});
					caregiver.setProviders(providerSet);
				}
				//New change end
				return caregiver;
			}
		}catch(Exception e){
			e.getLocalizedMessage();
		}

		return null;
	}

	//convert date to instantDate(UTC)
	public  Instant convertDateTimeToInstant(String time,Date prescriptionDate){
		try {
			SimpleDateFormat sdff = new SimpleDateFormat("hh:mm a");
			Date date = new Date();
			date = sdff.parse(time);
			//setting prescriptioncreated time in instant time
			Calendar prescritioncal = Calendar.getInstance();
			prescritioncal.setTime(prescriptionDate);

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.DATE, prescritioncal.get(Calendar.DATE));
			cal.set(Calendar.MONTH, prescritioncal.get(Calendar.MONTH));
			cal.set(Calendar.YEAR, prescritioncal.get(Calendar.YEAR));


			cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
			cal.set(Calendar.SECOND, cal.get(Calendar.SECOND));
			cal.set(Calendar.MILLISECOND, 0);
			Date startTime = cal.getTime();
			Instant instant = startTime.toInstant();
			logger.info("Time string converted to Instant");
			return instant;
		} catch (ParseException e) {
			logger.error("parse exception while converting time string to Instant");
			e.getLocalizedMessage();
		}
		return null;
	}
	//convert instant(UTC) to locale date
	public  Date convertInstantToDate(Instant instant){
		Date myDate = Date.from(instant);
		logger.info("Instant converted to Date");
		return myDate;
	}

	//add days to current date
	public Date addDaysToCurrentDate(int days){
		LocalDateTime addedDate =  LocalDateTime.now(ZoneOffset.UTC).plusDays(days);
		return Date.from( addedDate.atZone(ZoneOffset.UTC).toInstant());
	}

	public PatientModel setLatestAdherenceValues(PatientModel patientModel){
		Map<String, String> analyticMap = imanticService.fetchAnalyticsMap(patientModel.getUserDetails().getImanticUserid());
		patientModel.setPredictedAdherence(analyticMap.get("predictedAdherence"));
		patientModel.setCurrentAdherence(analyticMap.get("currentAdherence"));
		patientModel.setProjectedAdherence(analyticMap.get("projectedAdherence"));
		return patientModel;
	}

	public String getUserPreferedLang(Long userId) {
		String userPrelang = "en";
		try {
			UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userId);
			if(userPreference != null) {
				String jsonOfUserSetting = userPreference.getUserPrefSetting();
				ObjectMapper userPreferenceModelMapper = new ObjectMapper();
				UserPreferenceModel userPreferenceModel = userPreferenceModelMapper.readValue(jsonOfUserSetting, UserPreferenceModel.class);
				String userLangSetting = userPreferenceModel.getLanguagePreference();
				if(StringUtils.isNotEmpty(userLangSetting)) {
					userPrelang = userLangSetting;
				}
			}
		}catch(Exception e) {

		}

		return userPrelang;

	}

	public UserPreferenceModel getUserPreferenceModel(Long userId) throws Exception{
		UserPreferenceModel userPreferenceModel = null;
		UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userId);
		if(userPreference != null) {
			String jsonOfUserSetting = userPreference.getUserPrefSetting();
			ObjectMapper userPreferenceModelMapper = new ObjectMapper();
			userPreferenceModel = userPreferenceModelMapper.readValue(jsonOfUserSetting, UserPreferenceModel.class);

		}
		return userPreferenceModel;
	}
	
	public int instanceOfPatient(Patient patient, JwtUserDetails jwtUser) {
		if (jwtUser != null && "provider".equalsIgnoreCase(jwtUser.getRole())) {
			Set<Provider> providerSet = patient.getProviders();
			boolean underProvider = false;
			for (Provider provider : providerSet) {
				if (provider.getUserDetails()
				            .getEmailId()
				            .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underProvider = true;
				}
			}
			if (!underProvider) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "caregiver".equalsIgnoreCase(jwtUser.getRole())) {

			Set<Caregiver> caregiverSet = patient.getCaregivers();
			boolean underCaregiver = false;
			for (Caregiver caregiver : caregiverSet) {
				if (caregiver.getUserDetails()
				             .getEmailId()
				             .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underCaregiver = true;
				}
			}
			if (!underCaregiver) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "pharmacist".equalsIgnoreCase(jwtUser.getRole())) {
			Set<Pharmacist> pharmacistSet = patient.getPharmacists();
			boolean underPharmacist = false;
			for (Pharmacist pharmacist : pharmacistSet) {
				if (pharmacist.getUserDetails()
				              .getEmailId()
				              .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underPharmacist = true;
				}
			}
			if (!underPharmacist) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		return HttpStatus.OK.value();
	}
	
	public int instanceOfPrescriptionModel(PrescriptionModel prescriptionModel, JwtUserDetails jwtUser) {

		PatientModel patientModel = prescriptionModel.getPatient();
		if (jwtUser != null && "provider".equalsIgnoreCase(jwtUser.getRole())) {
			Set<ProviderModel> providerModelSet = patientModel.getProviders();
			boolean underProvider = false;
			for (ProviderModel providerModel : providerModelSet) {
				if (providerModel.getUserDetails()
				                 .getEmailId()
				                 .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underProvider = true;
				}
			}
			if (!underProvider) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "caregiver".equalsIgnoreCase(jwtUser.getRole())) {
			Set<CaregiverModel> caregiverModelSet = patientModel.getCaregivers();
			boolean underCaregiver = false;
			for (CaregiverModel caregiverModel : caregiverModelSet) {
				if (caregiverModel.getUserDetails()
				                  .getEmailId()
				                  .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underCaregiver = true;
				}
			}
			if (!underCaregiver) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "pharmacist".equalsIgnoreCase(jwtUser.getRole())) {
			Set<PharmacistModel> PharmacistModelSet = patientModel.getPharmacists();
			boolean underPharmacist = false;
			for (PharmacistModel pharmacistModel : PharmacistModelSet) {
				if (pharmacistModel.getUserDetails()
				                   .getEmailId()
				                   .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underPharmacist = true;
				}
			}
			if (!underPharmacist) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		return HttpStatus.OK.value();
	}

}
