package com.sg.dayamed.util;

import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.email.MessageModel;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.UserDetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class SendResetPwdNotification {

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	PasswordEncryption pwdEncryption;

	@Value("${dayamed.email.subject}")
	String emailSubject;

	@Value("${dayamed.email.body}")
	String emailBody;

	@Value("${dayamed.sms.body}")
	String smsBody;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;

	@Value("${dayamed.download.android.app}")
	String downloadAndroidApp;

	@Value("${dayamed.download.ios.app}")
	String downloadIosApp;

	@Autowired
	Utility utility;

	@Autowired
	EmailService emailService;

	private static final Logger logger = LoggerFactory.getLogger(SendResetPwdNotification.class);

	public void sendPwdResetNotificationWhenRegst(UserDetails userDetails, String userRole) {
		String emailBodyNew = emailBody;
		if (userRole.equalsIgnoreCase("patient") || userRole.equalsIgnoreCase("caregiver")) {
			emailBodyNew = emailBodyNew + downloadAndroidApp + " " + downloadIosApp;
		}
		sendEmailNotification(userDetails, userRole, emailSubject, emailBodyNew);
		sendSMSNotification(userDetails, userRole);
	}

	public void sendEmailNotification(UserDetails userDetails, String userRole, String subject, String body) {
		try {
			Email emailobj = new Email();
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(userDetails.getEmailId());
			emailobj.setRecipientList(recipientList);
			emailobj.setSubject(subject);
			body = (body == null || "".equalsIgnoreCase(body.trim())) ? emailBody : body;
			StringBuffer sbEmailBody = new StringBuffer(body);
			if (userRole != null && userDetails.getPwdChangeToken() != null) {
				sbEmailBody = new StringBuffer(sbEmailBody.toString()
				                                          .replace("$role", userRole)
				                                          .replace("$token",
				                                                   userDetails.getPwdChangeToken()));
			}
			emailobj.setBody(sbEmailBody.toString());
			emailService.sendEmail(emailobj);
		} catch (Exception e) {
			logger.error("Exception occurred while sending mail to {}", userDetails.getEmailId(), e);
		}
	}

	public boolean sendEmailNotificationByRecepients(List<Map<String, String>> recipientMap, String subject,
	                                                 String body) {
		try {
			List<String> recipientList = new ArrayList<>();
			recipientMap.forEach(emailMap -> {
				if (!StringUtils.isEmpty(emailMap.get("emailId"))) {
					recipientList.add(emailMap.get("emailId"));
				}
			});
			logger.info("====recipientList===" + recipientList);
			Email emailobj = new Email();
			emailobj.setRecipientList(recipientList);
			emailobj.setSubject(subject);
			emailobj.setBody(body);
			emailService.sendEmail(emailobj);
			return true;
		} catch (Exception e) {
			logger.error("Exception occurred while sending mail to {} ", recipientMap, e);
			return false;
		}
	}

	public boolean sendSMSNotification(List<Map<String, String>> recipientMap, String body) {
		try {
			recipientMap.forEach(emailMap -> {
				if (!StringUtils.isEmpty(emailMap.get("mobileNumber"))
						&& !StringUtils.isEmpty(emailMap.get("countryCode"))) {
					String mobileNumber = emailMap.get("mobileNumber");
					String countryCode = emailMap.get("countryCode");
					emailAndSmsUtility.sendSms(body, countryCode, mobileNumber);
				}
			});
			return true;
		} catch (Exception e) {
			logger.error("Exception occurred while sending SMS to {}", recipientMap, e);
			return false;
		}
	}

	public void sendSMSNotification(UserDetails userDetails, String userRole) {
		try {
			MessageModel messageModel = new MessageModel();
			StringBuffer sbsmsBody = new StringBuffer(smsBody);
			if (userRole != null && userDetails.getPwdChangeToken() != null) {
				sbsmsBody = new StringBuffer(sbsmsBody.toString()
				                                      .replace("$role", userRole)
				                                      .replace("$token",
				                                               userDetails.getPwdChangeToken()));
			}
			messageModel.setMessage(sbsmsBody.toString());
			messageModel.setTocontact(userDetails.getMobileNumber());
			messageModel.setCountryPhoneCode(userDetails.getCountryCode());
			logger.info("Sending SMS To ===>: " + userDetails.getCountryCode() + userDetails.getMobileNumber());
			emailAndSmsUtility.sendSms(sbsmsBody.toString(),
			                           userDetails.getCountryCode(), userDetails.getMobileNumber());
		} catch (Exception e) {
			logger.error("Exception occurred while sending SMS to the user {} ", userDetails.getEmailId(), e);
		}
	}

}
