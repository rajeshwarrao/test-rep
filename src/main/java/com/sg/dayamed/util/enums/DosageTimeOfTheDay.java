package com.sg.dayamed.util.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created By Gorantla, Eresh on 17/Jul/2019
 **/
@Getter
public enum DosageTimeOfTheDay {

	FIRST(1, "First"), SECOND(2, "Second"), THIRD(3, "Third"), FOURTH(4, "Fourth"),
	FIFTH(5, "Fifth"), SIXTH(6, "Sixth"), SEVENTH(7, "Seventh"), EIGHTH(8, "Eighth"),
	NINTH(9, "Ninth"), TENTH(10, "Tenth");

	private Integer id;

	private String value;

	private DosageTimeOfTheDay(Integer id, String value) {
		this.id = id;
		this.value = value;
	}

	public static DosageTimeOfTheDay getTimeOfTheDay(Integer id) {
		DosageTimeOfTheDay[] timeOfTheDays = DosageTimeOfTheDay.values();
		return Arrays.stream(timeOfTheDays)
		             .filter(day -> day.getId()
		                               .equals(id))
		             .findFirst()
		             .orElse(null);
	}
}
