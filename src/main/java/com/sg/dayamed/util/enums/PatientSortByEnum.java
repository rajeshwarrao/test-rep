package com.sg.dayamed.util.enums;

import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
public enum PatientSortByEnum {

	FIRST_NAME(1, "firstName"), ACTIVE_TIME(2, "lastActiveTime");

	private Integer id;

	private String sortBy;

	private PatientSortByEnum(Integer id, String sortBy) {
		this.id = id;
		this.sortBy = sortBy;
	}

	public static PatientSortByEnum getPatientSortByEnum(String sortBy) {
		PatientSortByEnum[] patientSortByEnums = PatientSortByEnum.values();
		return Arrays.stream(patientSortByEnums)
		             .filter(sort -> StringUtils.equalsIgnoreCase(sort.getSortBy(), sortBy))
		             .findFirst()
		             .orElse(null);
	}
}
