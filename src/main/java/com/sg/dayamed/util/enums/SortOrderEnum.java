package com.sg.dayamed.util.enums;

import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
public enum SortOrderEnum {

	ASC(1, "Asc"), DESC(2, "Desc");

	private Integer id;

	private String type;

	private SortOrderEnum(Integer id, String type) {
		this.id = id;
		this.type = type;
	}

	public static SortOrderEnum getSortTypeEnum(String type) {
		SortOrderEnum[] sortTypeEnums = SortOrderEnum.values();
		return Arrays.stream(sortTypeEnums)
		             .filter(sortType -> StringUtils.equalsIgnoreCase(sortType.getType(), type))
		             .findFirst()
		             .orElse(null);
	}
}
