package com.sg.dayamed.util.enums;

import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/

@Getter
public enum PrescriptionStatusEnum {

	NEW(1, "New"),
	HOLD(2, "Hold"),
	IN_PROCESS(3, "InProcess"),
	RECEIVED(4, "Received"),
	PROCESSED(5, "Processed"),
	SENT(6, "Sent"),
	DELIVERED(7, "Delivered");

	private Integer id;

	private String status;

	private PrescriptionStatusEnum(Integer id, String status) {
		this.id = id;
		this.status = status;
	}

	public PrescriptionStatusEnum getPrescriptionStatus(String status) {
		PrescriptionStatusEnum[] statusEnums = PrescriptionStatusEnum.values();
		return Arrays.stream(statusEnums)
		             .filter(statusEnum -> StringUtils.equalsIgnoreCase(status, statusEnum.getStatus()))
		             .findFirst()
		             .orElse(null);
	}
}
