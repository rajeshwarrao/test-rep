package com.sg.dayamed.util.enums;

import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
public enum UserRoleEnum {

	PROVIDER(1, "Provider"), PATIENT(2, "Patient"), CAREGIVER(3, "Caregiver"), PHARMACIST(4, "Pharmacist"),
	ADMIN(5, "Admin");

	private Integer id;

	private String role;

	private UserRoleEnum(Integer id, String role) {
		this.role = role;
		this.id = id;
	}

	public static UserRoleEnum getUserRoleEnum(String role) {
		UserRoleEnum[] roleEnums = UserRoleEnum.values();
		return Arrays.stream(roleEnums)
		             .filter(roleEnum -> StringUtils.equalsIgnoreCase(role, roleEnum.getRole()))
		             .findFirst()
		             .orElse(null);
	}
}
