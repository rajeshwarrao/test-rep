package com.sg.dayamed.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service("DateUtility")
public class DateUtility {

	private static final Logger logger = LoggerFactory.getLogger(DateUtility.class);

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";

	private static final String NEW_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static Date asDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay()
		                          .atZone(ZoneOffset.UTC)
		                          .toInstant());
	}

	public LocalDateTime convertUTCLocalDateTimeStringToObject(String localDateTime) {
		return LocalDateTime.parse(localDateTime, DateTimeFormatter.ofPattern(DATE_FORMAT));
	}

	// Java.time based code
	/*final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
	String actualDateTime ="2018-02-15 17:30";//24hrs
	String zoneid = "Asia/Kolkata"*/
	//convertLocalTimeToUTCTimeByzoneid
	public LocalDateTime convertLocalTimeToUTCTimeObjectByzoneid(String actualDateTime, String zoneid) {
		try {
			String localDatetimeDateUTCStr = "";
			if (StringUtils.isNotBlank(actualDateTime) && StringUtils.isNotBlank(zoneid)) {
				localDatetimeDateUTCStr = convertLocalTimeToUTCTimeStringByzoneid(actualDateTime, zoneid);
			}
			return LocalDateTime.parse(localDatetimeDateUTCStr, DateTimeFormatter.ofPattern(DATE_FORMAT));
		} catch (Exception e) {
			return null;
		}
	}

	public LocalDateTime convertLocalTimeToUTCWithZoneId(String actualDateTime, String zoneid) {
		try {
			String localDatetimeDateUTCStr = "";
			if (StringUtils.isNotBlank(actualDateTime) && StringUtils.isNotBlank(zoneid)) {
				localDatetimeDateUTCStr = convertLocalTimeForZoneId(actualDateTime, zoneid);
			}
			return LocalDateTime.parse(localDatetimeDateUTCStr, DateTimeFormatter.ofPattern(DATE_FORMAT));
		} catch (Exception e) {
			return null;
		}
	}

	public String convertUTCDateToLocalDateByZoneID(String utcTime, String zoneId, String datePattern) {
		try {
			ZonedDateTime zonedTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(utcTime)
			                                                                  .toInstant()
			                                                                  .atZone(ZoneId.of(zoneId));
			return zonedTime.format(DateTimeFormatter.ofPattern(datePattern));
		} catch (ParseException e) {
			logger.error("Error in DateUtility :: convertUTCDateToLocalDateByZoneID", e);
			return ApplicationConstants.EMPTY_STRING;
		}
	}

	public String convertUTCLocalDateTimeObjectToZonedLocalDateTimeByZoneID(String localDateTimeInUtc, String zoneid,
	                                                                        String datePattern) {

		ZonedDateTime zonedTime;
		try {
			zonedTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(localDateTimeInUtc)
			                                                    .toInstant()
			                                                    .atZone(ZoneId.of(zoneid));
			return zonedTime.format(DateTimeFormatter.ofPattern(datePattern));
		} catch (ParseException e) {
			logger.error("Error in DateUtility :: convertUTCLocalDateTimeObjectToZonedLocalDateTimeByZoneID", e);
			return null;
		}
	}

	/**
	 * @param dateInString "dd-M-yyyy hh:mm:ss"
	 * @param zoneId       "Asia/Kolkata"
	 * @return utcString
	 */
	public String convertLocalTimeToUTCTimeStringByzoneid(String dateInString, String zoneId) {
		try {

			// final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
			LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));
			ZoneId zoneIdObj = ZoneId.of(zoneId);
			ZonedDateTime zonedDateTime = ldt.atZone(zoneIdObj);
			ZoneId utcZoneId = ZoneId.of("UTC");

			ZonedDateTime utcDateTime = zonedDateTime.withZoneSameInstant(utcZoneId);
			DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
			return format.format(utcDateTime);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		}
		return null;
	}

	public String convertLocalTimeForZoneId(String dateInString, String zoneId) {
		try {

			// final String DATE_FORMAT = "yyyy-MM-dd HH:mm";
			LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));
			ZoneId zoneIdObj = ZoneId.of(zoneId);
			ZonedDateTime zonedDateTime = ldt.atZone(zoneIdObj);
			ZoneId utcZoneId = ZoneId.of(zoneId);

			ZonedDateTime utcDateTime = zonedDateTime.withZoneSameInstant(utcZoneId);
			DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
			return format.format(utcDateTime);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
		}
		return null;
	}

	public String getDateTimeBasedOnZoneId(String zoneId) {
		try {
			ZonedDateTime zdt = Instant.now()
			                           .atZone(ZoneId.of(zoneId));
			LocalDateTime currentZoneDateTime = zdt.toLocalDateTime();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
			return currentZoneDateTime.truncatedTo(ChronoUnit.SECONDS)
			                          .format(dtf);
		} catch (Exception e) {
			logger.error("problem while getting datetime based on zoeID", e);
		}
		return ApplicationConstants.EMPTY_STRING;
	}

	public String getFormattedLocalDateTime(LocalDateTime localdatetime) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT);
		return localdatetime.truncatedTo(ChronoUnit.SECONDS)
		                    .format(dtf)
		                    .replace("T", " ");
	}

	public LocalDate addDaysToLocalDate(LocalDate presentDate, int noOfDays) {
		try {
			LocalDate localdate = presentDate.plusDays(noOfDays);
			return localdate;
		} catch (Exception e) {
			logger.error("Error in DateUtility :: addDaysToLocalDate", e);
		}
		return null;
	}

	//String = "1986-04-08 12:30";yyyy-MM-dd HH:mm
	public LocalDateTime convertdatetimeStringToLocalDateTime(String datetimeStr) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
		LocalDateTime dateTime = LocalDateTime.parse(datetimeStr, formatter);
		return dateTime;
	}

	public LocalDateTime convertDateObjectToLocalDateTimeObject(Date date) {
		LocalDateTime localDateTimeObj = date.toInstant()
		                                     .atZone(ZoneOffset.UTC)
		                                     .toLocalDateTime();
		return localDateTimeObj;
	}

	public LocalDateTime addTimestringToLacalDate(String time, LocalDate durationDate) {

		try {
			time = time.replace("am", "AM")
			           .replace("pm", "PM")
			           .trim();
			LocalTime localTime3 = LocalTime.parse(time, DateTimeFormatter.ofPattern("h:mm a"));

			LocalDateTime localDateTime = durationDate.atTime(localTime3);
			return localDateTime;
		} catch (Exception e) {
			logger.error("While converting time string to LocalDateTime object", e);
		}
		return null;
	}

	public Date convertdateStringToDateObject(String sourceDate, String dateFormate) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormate);
		try {
			Date date = formatter.parse(sourceDate);
			return date;
		} catch (ParseException e) {
			logger.error("Error in DateUtility :: convertdateStringToDateObject", e);
		}
		return null;
	}

	public Date convertLocalDateTimeToDateObject(LocalDateTime dateToConvert) {
		if (dateToConvert != null) {
			return java.util.Date
					.from(dateToConvert.atZone(ZoneOffset.UTC)
					                   .toInstant());
		}
		return null;
	}

	public String convertDate24hoursFormatTo12hourForamat(String _24HourTime) throws ParseException {
		SimpleDateFormat _24HourSDF = new SimpleDateFormat("dd.MM.yyyy - HH.mm.ss");
		SimpleDateFormat _12HourSDF = new SimpleDateFormat("dd.MM.yyyy - hh.mm.ss aa");
		Date _24HourDt = _24HourSDF.parse(_24HourTime);
		return _12HourSDF.format(_24HourDt);
	}

}
