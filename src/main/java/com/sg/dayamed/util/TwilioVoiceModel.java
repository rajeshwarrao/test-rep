package com.sg.dayamed.util;

import javax.validation.constraints.NotBlank;

public class TwilioVoiceModel {

	@NotBlank(message = "{identity.is.required}")
	private String identity;

	private String token;

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
