package com.sg.dayamed.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * Created by Eresh Gorantla on 09/May/2019
 **/

@Service
public class BeanUtil implements ApplicationContextAware {

	private static ApplicationContext context;

	public BeanUtil() {
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;
	}

	public static <T> T getBean(Class<T> beanClass) {
		return context.getBean(beanClass);
	}
}
