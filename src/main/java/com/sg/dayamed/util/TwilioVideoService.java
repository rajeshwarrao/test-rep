package com.sg.dayamed.util;

import com.twilio.jwt.accesstoken.AccessToken;
import com.twilio.jwt.accesstoken.VideoGrant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TwilioVideoService {

	@Value("${twilio.account.sid}")
	private String accountSid;

	@Value("${twilio.key.sid}")
	private String keySid;

	@Value("${twilio.secret}")
	private String secret;

	public TwilioVideoModel generateAuthToken(String clientIdentity) {

		TwilioVideoModel twilioVideoModel = new TwilioVideoModel();
		// Create a VideoGrant
		final VideoGrant grant = new VideoGrant();

		// Create an Access Token
		final AccessToken token = new AccessToken.Builder(accountSid, keySid, secret)
				.identity(clientIdentity) // Set the Identity of this token
				.grant(grant) // Grant access to Video
				.build();

		// Serialize the token as a JWT
		final String jwt = token.toJwt();
		twilioVideoModel.setIdentity(clientIdentity);
		twilioVideoModel.setToken(jwt);

		return twilioVideoModel;
	}
}
