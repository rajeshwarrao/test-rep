package com.sg.dayamed.util;

import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.MessageModel;
import com.sg.dayamed.helper.pojo.ResultVO;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.rabbitmq.RabbitMQSender;
import com.sg.dayamed.service.UserDetailsService;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class EmailAndSmsUtility {

	@Autowired
	RabbitMQSender rabbitMQSender;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	@Autowired
	Utility utility;

	@Value("${product.owner}")
	String productOwner;

	public boolean sendEmail(String subject, String bodyMsg, List<String> recipientList) {
		try {
			Email emailobj = new Email();
			emailobj.setSubject(subject);
			emailobj.setBody(bodyMsg);
			List<String> filteredEmails = userDetailsService.findByEmailIdIn(recipientList);
			emailobj.setRecipientList(filteredEmails);
			rabbitMQSender.sendEmail(emailobj);
			return true;
		} catch (Exception e) {
			log.error("Exception occurred when sending email", e);
			return false;
		}
	}

	public boolean sendEmail(String subject, String bodyMsg, List<String> recipientList, Boolean defaultFlag) {
		try {
			Email emailobj = new Email();
			emailobj.setSubject(subject);
			emailobj.setBody(bodyMsg);
			emailobj.setRecipientList(recipientList);
			rabbitMQSender.sendEmail(emailobj);
			return true;
		} catch (Exception e) {
			log.error("Exception occurred when sending email", e);
			return false;
		}
	}

	public boolean sendSms(String bodyMsg, String toContact) {
		try {
			MessageModel messageModel = new MessageModel();
			messageModel.setTocontact(toContact);
			messageModel.setMessage(bodyMsg);
			rabbitMQSender.sendSms(messageModel);
			return true;
		} catch (Exception e) {
			log.error("Exception occurred when sending sms", e);
			return false;
		}
	}

	public boolean sendSms(String bodyMsg, String countryCode, String toContact) {
		try {
			MessageModel messageModel = new MessageModel();
			messageModel.setTocontact(countryCode + toContact);
			messageModel.setMessage(bodyMsg);
			Boolean isFlagSet = userDetailsService.findByPhoneNumber(toContact);
			if (isFlagSet) {
				rabbitMQSender.sendSms(messageModel);
			}
			return true;
		} catch (Exception e) {
			log.error("Exception occurred when sending sms", e);
			return false;
		}
	}

	public boolean sendEmail(String subject, String langKey, Map<String, String> replaceDynamic,
	                         List<String> recipientList) {
		try {
			Map<String, List<String>> langMsg = new HashMap<String, List<String>>();
			List<ResultVO> resultVOs = userDetailsService.findByEmailIdInResultVo(recipientList);
			List<String> finalList = new ArrayList<String>();
			resultVOs.stream()
			         .forEach(result -> {
				         UserPreferenceModel model = result.getModel();
				         if (model != null) {
					         String langPref = model.getLanguagePreference();
					         if (langMsg.get(langPref) != null) {
						         List<String> newList = langMsg.get(langPref);
						         newList.add(result.getEmailId());
						         langMsg.put(langPref, newList);
					         } else {
						         //if lang not present add the new language to the Map
						         List<String> newList = new ArrayList<String>();
						         newList.add(result.getEmailId());
						         langMsg.put(langPref, newList);
					         }

				         } else {
					         finalList.add(result.getEmailId());
				         }
			         });

			langMsg.entrySet()
			       .forEach(entry -> {

				       String bodyMsgLangTranslated = "";
				       String emailSubject = "";
				       try {
					       bodyMsgLangTranslated =
							       messageNotificationConstants.getUserPreferedLangTranslatedMsg(replaceDynamic,
							                                                                     langKey,
							                                                                     entry.getKey(), null);
					       if (StringUtils.equalsIgnoreCase(subject, "updated.profile.in.dayamed") ||
							       StringUtils.equalsIgnoreCase(subject, "appconstants.profile.updated")) {
						       emailSubject =
								       messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, subject,
								                                                                     entry.getKey(),
								                                                                     new Object[]{
										                                                                     utility.getProductOwner()});
					       } else {
						       emailSubject =
								       messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, subject,
								                                                                     entry.getKey(),
								                                                                     null);
					       }

				       } catch (Exception e) {

				       }
				       Email emailobj = new Email();
				       emailobj.setSubject(emailSubject);
				       emailobj.setBody(bodyMsgLangTranslated);
				       emailobj.setRecipientList(entry.getValue());
				       rabbitMQSender.sendEmail(emailobj);

			       });

			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
