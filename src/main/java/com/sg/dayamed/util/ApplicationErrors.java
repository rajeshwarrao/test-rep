package com.sg.dayamed.util;

public final class ApplicationErrors {

	private ApplicationErrors() {
		throw new AssertionError();
	}

	public static final String PATIENT_ID_SHOULD_NOT_NULL = "patient id should not null or empty";

	public static final String PROVIDER_ID_SHOULD_NOT_NULL = "provider id should not null or empty";

	public static final String CAREGIVER_ID_SHOULD_NOT_NULL = "caregiver id should not null or empty";

	public static final String PHARMACIST_ID_SHOULD_NOT_NULL = "pharmacist id should not null or empty";

	public static final String USER_ID_SHOULD_NOT_NULL = "user id should not null or empty";

	public static final String ACCESS_DENIED_PROVIDER = "The user can not acccess this API. User must be Provider";

	public static final String ACCESS_DENIED_PATIENT = "The user can not acccess this API. User must be Patient";

	public static final String ACCESS_DENIED_CAREGIVER = "The user can not acccess this API. User must be Caregiver";

	public static final String ACCESS_DENIED_PHARMACIST = "The user can not acccess this API. User must be Pharmacist";

	public static final String NOT_FOUND_PROVIDER = "provider not found";

	public static final String NOT_FOUND_PATIENT = "patient not found";

	public static final String NOT_FOUND_CARREGIVER = "caregiver not found";

	public static final String NOT_FOUND_PHARMACIST = "pharmacist not found";

	public static final String PROBLEM_FETCHING_PATIETNS_BY_PROVIDER_ID =
			"Problem while fetching patietns by Provider Id";

	public static final String PROBLEM_FETCHING_PATIETNS_BY_PATIENT_ID =
			"Problem while fetching patietns by Patient Id";

	public static final String PROBLEM_FETCHING_PATIETNS_BY_CAREGIVER_ID =
			"Problem while fetching patietns by Caregiver Id";

	public static final String PROBLEM_FETCHING_PATIETNS_BY_PHARMACIST_ID =
			"Problem while fetching patietns by Pharmacist Id";

	public static final String PROBLEM_FETCHING_PATIETNS_BY_USER_ID = "Problem while fetching patietns by User Id";

	public static final String PROBLEM_FETCHING_PRESCIPTIONS_BY_USER_ID =
			"Problem while fetching prescriptions by User Id";

	public static final String PROBLEM_FETCHING_PRESCIPTIONS_BY_PATIENT_ID =
			"Problem while fetching prescriptions by Patient Id";

	public static final String PROBLEM_FETCHING_PRESCIPTIONS_BY_PROVIDER_ID =
			"Problem while fetching prescriptions by Provider Id";

	public static final String PROBLEM_FETCHING_PRESCIPTIONS_BY_CAREGIVER_ID =
			"Problem while fetching prescriptions by Caregiver Id";

	public static final String PROBLEM_FETCHING_PRESCIPTIONS_BY_PHARMACIST_ID =
			"Problem while fetching prescriptions by Pharmacist Id";

	public static final String NOT_FOUND_PROVIDER_IN_DB = "provider not found in database";

	public static final String NOT_FOUND_PATIENT_IN_DB = "patient not found in database";

	public static final String NOT_FOUND_CAREGIVER_IN_DB = "caregiver not found in database";

	public static final String NOT_FOUND_PHARMACIST_IN_DB = "pharmacist not found in database";

	public static final String NOT_FOUND_USER_IN_DB = "user not found in database";

	//204 status code
	public static final String NOT_FOUND_PROVIDER_WITH_ID = "provider not found with provided id";

	public static final String NOT_FOUND_PATIENT_WITH_ID = "patient not found with provided id";

	public static final String NOT_FOUND_CAREGIVER_WITH_ID = "caregiver not found with provided id";

	public static final String NOT_FOUND_PHARMACIST_WITH_ID = "pharmacist not found with provided id";

	public static final String NOT_FOUND_USER_WITH_ID = "user not found with provided id";

	public static final String NOT_FOUND_PRESCRIPTION_WITH_ID = "prescription not found with provided id";

	public static final String EMAIL_ALREADY_EXISTED = "email already existed";

	public static final String DATA_NULL_WHILE_MAPPING = "data is null while mapping input string to model class";

	public static final String NOT_FOUND_USER_DETAILS = "userdetails data not found";

	public static final String MISSING_MANDATORY_DATA = "missing mandatory data";

	public static final String UNRECOGNIZED_FIELD_NAMES_ERROR =
			"Unrecognized field names: Not coming input data as expected json format";

}
