package com.sg.dayamed;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author naresh.kamisetti
 * This services are invoked while bootstrap.
 * here we are purging All Queues while bootstrap
 */

@Component
public class BootstrapService implements ApplicationListener<ApplicationReadyEvent> {

	@Autowired
	private AmqpAdmin admin;

	@Value("${dayamed.rabbitmq.email.queue}")
	String emailQueue;

	@Value("${dayamed.rabbitmq.sms.queue}")
	String smsQueue;

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		admin.purgeQueue(emailQueue, true);
		admin.purgeQueue(smsQueue, true);
		return;
	}

}
