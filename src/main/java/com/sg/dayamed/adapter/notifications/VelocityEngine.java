package com.sg.dayamed.adapter.notifications;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.runtime.RuntimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Writer;

/**
 * Created by Eresh Gorantla on 02/Jun/2019
 **/
@Component
public class VelocityEngine {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final VelocityCustomLogger customLogger = new VelocityCustomLogger();
    /**
     * Initialize Velocity Engine at start up.
     */
    @Autowired
    public void init() {
        logger.info("Initializing Velocity Engine...");
        Velocity.init();
        setVelocityLogger(customLogger);
        logger.info("Velocity Engine initialization completed.");
    }
    /**
     * Wrapper method for Velocity evaluate which merges the input
     * template string with the variables in the context and writes the result
     * to the output writer.
     * @param context
     * @param out
     * @param logTag - name of template
     * @param instring - template as a string
     * @return
     */
    public boolean evaluate(Context context, Writer out, String logTag, String instring ) {
        return Velocity.evaluate(context, out, logTag, instring);
    }

    /**
     * Sets the Velocity runtime logger to the customer logger
     * @param velocityLogger
     */
    public void setVelocityLogger( VelocityCustomLogger velocityLogger ) {
        // CCBO-4526: Set to custom velocity logger in order to create log in log directory and use log4j.
        Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM, velocityLogger);
    }
}
