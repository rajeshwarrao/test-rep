package com.sg.dayamed.adapter.notifications.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DosageDetailsVO {

	public DosageDetailsVO(String patientName, List<MedicineStatusInfoVO> listMedicineStatusInfoVO, String mobileNumber, String deepLink) {
		this.patientName = patientName;
		this.listMedicineStatusInfoVO = listMedicineStatusInfoVO;
		this.mobileNumber = mobileNumber;
		this.deepLink = deepLink;
	}

	private String patientName;
	private List<MedicineStatusInfoVO> listMedicineStatusInfoVO = new ArrayList<>();
	private String mobileNumber;
	private String deepLink;
}
