package com.sg.dayamed.adapter.notifications.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicineStatusInfoVO {

	public MedicineStatusInfoVO(String name, String status) {
		this.name = name;
		this.status = status;
	}

	private String name;
	private String status;

}
