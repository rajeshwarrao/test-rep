package com.sg.dayamed.adapter.notifications;

import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sg.dayamed.adapter.notifications.vo.DosageDetailsVO;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.service.RediseService;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Eresh Gorantla on 02/Jun/2019
 **/
@Component
public class VelocityMessageFormatter {

    @Autowired
    private VelocityEngine velocityEngine;
    
    @Autowired
	RediseService rediseService;

    public String formatCustomDosageNotification( List<DosageDetailsVO> dosageDetailsVOS) {
        VelocityContext context = new VelocityContext();
        context.put("dosageInfoS", dosageDetailsVOS);
        StringWriter writer = new StringWriter();
        String emailMessage = (String)rediseService.getDataInRedisByKey(RedisKeyConstants.VELOCITY_EMAIL_TEMPLATE);
        velocityEngine.evaluate(context, writer, UUID.randomUUID().toString(), emailMessage);
        return writer.toString();
    }

    public String formatMissedMedicationNotification(List<Map<String, String>> patientMedicationData , String patientName, String emailContentMsg) {
         VelocityContext context = new VelocityContext();
         context.put("patientMedicationData", patientMedicationData);
         context.put("emailContentMsg", emailContentMsg);
         context.put("patientName", patientName);
         StringWriter writer = new StringWriter();
         String emailMessage = (String)rediseService.getDataInRedisByKey(RedisKeyConstants.MISSED_MEDICATION_VELOCITY_EMAIL_TEMPLATE);
         velocityEngine.evaluate(context, writer, UUID.randomUUID().toString(), emailMessage);
         return writer.toString();
    }
}
