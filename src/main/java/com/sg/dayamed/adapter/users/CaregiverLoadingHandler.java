package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.repository.CaregiverRepository;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CaregiverLoadingHandler extends UserInfoLoadingHandler {

	@Autowired
	CaregiverLoadingHandler self;

	@Autowired
	CaregiverRepository caregiverRepository;

	@Override
	protected UserInfoLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	protected String[] getUserTypes() {
		return new String[]{UserRoleEnum.CAREGIVER.getRole()};
	}

	protected CaregiverModel getCaregiverModel(Long userId) {
		Caregiver caregiver = caregiverRepository.findByUserDetails_id(userId);
		return utility.convertCaregiverEntityToModel(caregiver);
	}

	@Override
	public UserDetailsWithJwtModel getUserDetailsWithModelResponse(UserDetailsWithJwtModel userDetailsWithJwtModel,
	                                                               LoginRequestModel loginRequestModel)
			throws ApplicationException {
		CaregiverModel caregiverModel = getCaregiverModel(userDetailsWithJwtModel.getUserDetails()
		                                                                         .getId());
		userDetailsWithJwtModel.getUserDetails()
		                       .setCaregiver(caregiverModel);
		userDetailsWithJwtModel.setId(caregiverModel.getId());
		return userDetailsWithJwtModel;
	}

}
