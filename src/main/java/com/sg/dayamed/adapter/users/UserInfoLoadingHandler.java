package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserOptions;
import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.security.JwtGenerator;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Date;

public abstract class UserInfoLoadingHandler {

	@Autowired
	UserLoadingAdapter adapter;

	@Autowired
	Utility utility;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	UserPreferenceRepository userPreferenceRepository;

	@Autowired
	ObjectMapper userPreferenceModelMapper;

	@PostConstruct
	public void init() {
		adapter.registerHandler(getUserTypeHandler(), getUserTypes());
	}

	protected UserDetailsWithJwtModel getUserDetailsWithModel(LoginRequestModel loginRequestModel,
	                                                          JwtGenerator jwtGenerator, UserDetails userDetails)
			throws Exception {
		UserDetailsModel userDetailsModel = null;
		JwtUser jwtUser = new JwtUser();
		try {
			UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
			userDetails.setType(loginRequestModel.getTypeOfDevice());
			userDetails.setOsVersion(loginRequestModel.getOsVersion());
			userDetails.setAppVersion(loginRequestModel.getAppVersion());
			userDetails.setLastLoginTime(new Date());
			userDetails.setLastActiveTime(new Date());
			Role role = userDetails.getRole();
			userDetailsModel = utility.convertUserDetailsEntityToModel(userDetails);
			jwtUser.setUserName(loginRequestModel.getUserName());
			jwtUser.setRole(role.getName()
			                    .toUpperCase());
			jwtUser.setUserId(userDetails.getId());
			jwtUser.setUserEmailId(userDetails.getEmailId());
			jwtUser.setDeviceId(loginRequestModel.getDeviceId());
			jwtUser.setTypeOfDevice(loginRequestModel.getTypeOfDevice());
			if (userDetails.getUserOptions() != null) {
				UserOptions userOptions = userDetails.getUserOptions();
				userDetailsWithJwtModel.setGsPharmacistEnable(userOptions.getGsPharmacistEnable() != null
						                                              && userOptions.getGsPharmacistEnable()
						                                                            .intValue() == 1);
				jwtUser.setGsPharmacistEnable(userDetailsWithJwtModel.isGsPharmacistEnable());
			}
			userDetailsWithJwtModel.setJwtToken(jwtGenerator.generate(jwtUser));
			userDetailsModel.setUserPreferenceModel(fetchUserPreferenceModel(userDetails.getId()));
			userDetailsWithJwtModel.setUserDetails(userDetailsModel);
			userDetailsRepository.save(userDetails);
			return userDetailsWithJwtModel;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private UserPreferenceModel fetchUserPreferenceModel(long userId) throws Exception {
		UserPreferenceModel userPreferenceModel = new UserPreferenceModel();
		UserPreference userPreferenceInDB = userPreferenceRepository.findByUserDetailsId(userId);
		if (userPreferenceInDB != null) {
			userPreferenceModel = userPreferenceModelMapper.readValue(userPreferenceInDB.getUserPrefSetting(),
			                                                          UserPreferenceModel.class);
		}
		return userPreferenceModel;
	}

	protected abstract UserInfoLoadingHandler getUserTypeHandler();

	protected abstract UserDetailsWithJwtModel getUserDetailsWithModelResponse(
			UserDetailsWithJwtModel userDetailsWithJwtModel, LoginRequestModel loginRequestModel)
			throws ApplicationException;

	protected abstract String[] getUserTypes();

}
