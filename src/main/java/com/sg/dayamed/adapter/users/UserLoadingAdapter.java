package com.sg.dayamed.adapter.users;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserLoadingAdapter {

	private Map<String, UserInfoLoadingHandler> handlers = new HashMap<>();

	public UserInfoLoadingHandler getHandler(String userType) {
		return handlers.get(userType);
	}

	public void registerHandler(UserInfoLoadingHandler handler, String... userTypes) {
		if (handler == null) {
			throw new IllegalArgumentException("Handler cannot be null");
		}

		if (ArrayUtils.isEmpty(userTypes)) {
			throw new IllegalArgumentException("import types cannot be null or empty");
		}

		for (String importType : userTypes) {
			handlers.put(importType, handler);
		}
	}
}
