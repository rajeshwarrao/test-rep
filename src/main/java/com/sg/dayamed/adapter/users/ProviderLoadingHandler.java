package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProviderLoadingHandler extends UserInfoLoadingHandler {

	@Autowired
	ProviderLoadingHandler self;

	@Autowired
	ProviderRepository providerRepository;

	@Override
	public UserInfoLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	public String[] getUserTypes() {
		return new String[]{UserRoleEnum.PROVIDER.getRole()};
	}

	@Override
	public UserDetailsWithJwtModel getUserDetailsWithModelResponse(UserDetailsWithJwtModel userDetailsWithJwtModel,
	                                                               LoginRequestModel loginRequestModel)
			throws ApplicationException {
		return userDetailsWithJwtModel;
	}
}
