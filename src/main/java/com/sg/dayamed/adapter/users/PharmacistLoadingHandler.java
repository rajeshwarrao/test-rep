package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PharmacistLoadingHandler extends UserInfoLoadingHandler {

	@Autowired
	PharmacistLoadingHandler self;

	@Autowired
	PharmacistRepository pharmacistRepository;

	@Override
	public UserInfoLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	public String[] getUserTypes() {
		return new String[]{UserRoleEnum.PHARMACIST.getRole()};
	}

	public PharmacistModel getPharmacistModel(Long userId) {
		Pharmacist pharmacist = pharmacistRepository.findByUserDetails_id(userId);
		return utility.convertPharmacistEntityToModel(pharmacist);
	}

	@Override
	public UserDetailsWithJwtModel getUserDetailsWithModelResponse(UserDetailsWithJwtModel userDetailsWithJwtModel,
	                                                               LoginRequestModel loginRequestModel)
			throws ApplicationException {
		PharmacistModel pharmacistModel = getPharmacistModel(userDetailsWithJwtModel.getUserDetails()
		                                                                            .getId());
		userDetailsWithJwtModel.getUserDetails()
		                       .setPharmasist(pharmacistModel);
		userDetailsWithJwtModel.setId(pharmacistModel.getId());
		return userDetailsWithJwtModel;
	}
}
