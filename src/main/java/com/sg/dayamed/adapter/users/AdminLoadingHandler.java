package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminLoadingHandler extends UserInfoLoadingHandler {

	@Autowired
	AdminLoadingHandler self;

	@Override
	protected UserInfoLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	protected UserDetailsWithJwtModel getUserDetailsWithModelResponse(UserDetailsWithJwtModel userDetailsWithJwtModel,
	                                                                  LoginRequestModel loginRequestModel)
			throws ApplicationException {
		return userDetailsWithJwtModel;
	}

	@Override
	protected String[] getUserTypes() {
		return new String[]{UserRoleEnum.ADMIN.getRole()};
	}

}
