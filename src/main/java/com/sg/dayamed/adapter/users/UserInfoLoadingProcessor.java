package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.security.jwtsecurity.security.JwtGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserInfoLoadingProcessor {

	@Autowired
	UserLoadingAdapter adapter;

	public UserDetailsWithJwtModel getUserDetailsWithModel(UserRoleEnum userRoleEnum,
	                                                       LoginRequestModel loginRequestModel,
	                                                       JwtGenerator jwtGenerator, UserDetails userDetails)
			throws Exception {
		try {
			UserInfoLoadingHandler handler = adapter.getHandler(userRoleEnum.getRole());
			UserDetailsWithJwtModel userDetailsWithJwtModel = handler.getUserDetailsWithModel(loginRequestModel,
			                                                                                  jwtGenerator,
			                                                                                  userDetails);
			return handler.getUserDetailsWithModelResponse(userDetailsWithJwtModel, loginRequestModel);
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
