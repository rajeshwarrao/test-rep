package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class PatientLoadingHandler extends UserInfoLoadingHandler {

	@Autowired
	PatientLoadingHandler self;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	PHIDataEncryptionService pHIDataEncryptionService;

	@Override
	protected UserInfoLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	protected String[] getUserTypes() {
		return new String[]{UserRoleEnum.PATIENT.getRole()};
	}

	protected Patient getPatient(Long userId) throws Exception {
		return patientRepository.findByUserDetails_id(userId);
	}

	@Override
	public UserDetailsWithJwtModel getUserDetailsWithModelResponse(UserDetailsWithJwtModel userDetailsWithJwtModel,
	                                                               LoginRequestModel loginRequestModel)
			throws ApplicationException {
		Patient patient = patientRepository.findByUserDetails_id(userDetailsWithJwtModel.getUserDetails()
		                                                                                .getId());
		// if patient created by GS provider
		boolean gsProviderFlag = false;
		for (Provider provider : patient.getProviders()) {
			if (provider.getUserDetails() != null && provider.getUserDetails()
			                                                 .getUserOptions() != null
					&& provider.getUserDetails()
					           .getUserOptions()
					           .getGsPharmacistEnable()
					           .intValue() == 1) {
				gsProviderFlag = true;
			}
		}
		if (gsProviderFlag) {
			patient.setPharmacists(new HashSet<Pharmacist>());
		}
		PatientModel patModel = utility.convertPatientEntityTopatientModel(patient);
		patModel.setUserDetails(patModel.getUserDetails());
		String keyText = pHIDataEncryptionService.getKeyText(userDetailsWithJwtModel.getJwtToken(),
		                                                     loginRequestModel.getTypeOfDevice(),
		                                                     loginRequestModel.getDeviceId());
		PatientModel encryptPatntModel = pHIDataEncryptionService.encryptPatient(patModel, keyText);
		UserPreferenceModel userPrefModel = userDetailsWithJwtModel.getUserDetails()
		                                                           .getUserPreferenceModel();
		encryptPatntModel.getUserDetails().setUserPreferenceModel(userPrefModel);
		userDetailsWithJwtModel.setUserDetails(
				userDetailsWithJwtModel.getUserDetails());
		userDetailsWithJwtModel.getUserDetails()
		                       .setUserPreferenceModel(userPrefModel);
		if (ApplicationConstants.ANDROID.equalsIgnoreCase(loginRequestModel.getTypeOfDevice())) {
			encryptPatntModel.getUserDetails()
			                 .setPatient(null);
			userDetailsWithJwtModel.setUserDetails(encryptPatntModel.getUserDetails());
		}
		if (ApplicationConstants.IOS.equalsIgnoreCase(loginRequestModel.getTypeOfDevice())) {
			userDetailsWithJwtModel.setUserDetails(encryptPatntModel.getUserDetails());
		}
		userDetailsWithJwtModel.setId(encryptPatntModel.getId());
		return userDetailsWithJwtModel;
	}
}
