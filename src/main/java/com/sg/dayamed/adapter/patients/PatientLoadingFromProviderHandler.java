package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientProviderAssociationsV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.specification.PatientProviderSpecification;
import com.sg.dayamed.repository.view.PatientProviderAssociationsVRepository;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Component
public class PatientLoadingFromProviderHandler extends BasePatientInitLoadingHandler {

	@Autowired
	PatientLoadingFromProviderHandler self;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	PatientProviderAssociationsVRepository providerAssociationsVRepository;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	AdherenceService adherenceService;

	@Override
	public BasePatientInitLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	public String[] getUserTypes() {
		return new String[]{UserRoleEnum.PROVIDER.getRole()};
	}

	@Override
	public PatientDetailsResponseVO getPatients(LoadPatientRequestVO requestVO) throws ApplicationException {
		PatientDetailsResponseVO responseVO;
		try {
			String authority = requestVO.getUserDetails()
			                            .getAuthorities()
			                            .stream()
			                            .map(auth -> auth.getAuthority())
			                            .findFirst()
			                            .orElse(null);
			UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(authority);
			userValidator.validateGetPatients(userRoleEnum, requestVO);
			List<PatientAssociationDetailsV> associationDetailsVS =
					getPatientAssociationDetails(userRoleEnum, requestVO);
			if (requestVO.getNonAdherentFlag()) {
				associationDetailsVS = getNonAdherentPatients(associationDetailsVS);
			}
			responseVO = constructPagesForResult(userRoleEnum, requestVO, associationDetailsVS);
			return responseVO;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	@Override
	public List<PatientAssociationDetailsV> getPatientAssociationDetails(UserRoleEnum userRoleEnum,
	                                                                     LoadPatientRequestVO requestVO)
			throws ApplicationException {
		try {
			List<PatientProviderAssociationsV> patientProviderAssociationsVS =
					providerAssociationsVRepository.findAll(new PatientProviderSpecification(userRoleEnum, requestVO));
			patientProviderAssociationsVS = patientProviderAssociationsVS.stream()
			                                                             .filter(distinctByKey(
					                                                             detail -> detail.getPatientId()))
			                                                             .collect(Collectors.toList());
			return patientProviderAssociationsVS.stream()
			                                    .map(src -> mapperFacade.map(src, PatientAssociationDetailsV.class))
			                                    .collect(Collectors.toList());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private List<PatientAssociationDetailsV> getNonAdherentPatients(
			List<PatientAssociationDetailsV> associationDetailsVS) {
		List<Long> patientIds = associationDetailsVS.stream()
		                                            .map(PatientAssociationDetailsV::getPatientId)
		                                            .distinct()
		                                            .collect(Collectors.toList());
		List<PatientAssociationDetailsV> nonAdherentPatients = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(patientIds)) {
			List<Prescription> prescriptions = prescriptionRepository.findByPatient_idIn(patientIds);
			List<Long> prescriptionIds = prescriptions.stream()
			                                          .map(Prescription::getId)
			                                          .collect(Collectors.toList());
			String startTime = " 00:00";
			String endTime = " 23:59";
			LocalDate yesterdayLocalDateInUTC = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC)
			                                                 .toLocalDate()
			                                                 .minusDays(1);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDateTime startLocalDateTime = dateUtility.convertdatetimeStringToLocalDateTime(
					yesterdayLocalDateInUTC.format(formatter) + startTime);
			LocalDateTime endLocalDateTime = dateUtility.convertdatetimeStringToLocalDateTime(
					yesterdayLocalDateInUTC.format(formatter) + endTime);
			List<AdherenceDataPoints> dataPoints =
					adherenceService.findAdherencesBetweenPrescribedTime(startLocalDateTime, endLocalDateTime,
					                                                     prescriptionIds);
			for (Long patientId : patientIds) {
				List<Long> presIds = prescriptions.stream()
				                                  .filter(pres -> pres.getPatient()
				                                                      .getId() == patientId.longValue())
				                                  .map(pres -> pres.getId())
				                                  .distinct()
				                                  .collect(Collectors.toList());
				List<AdherenceDataPoints> adherenceDataPoints = dataPoints.stream()
				                                                          .filter(data -> presIds.contains(
						                                                          data.getPrescriptionID()))
				                                                          .collect(Collectors.toList());
				AdherenceDataPoints points = adherenceDataPoints.stream()
				                                                .filter(data -> data.getConsumptionStatus()
				                                                                    .contains(
						                                                                    ApplicationConstants.CONSUMPTION_STATUS_SKIPPED) ||
						                                                data.getConsumptionStatus()
						                                                    .
								                                                    contains(
										                                                    ApplicationConstants.CONSUMPTION_STATUS_NOACTION))
				                                                .findFirst()
				                                                .orElse(null);
				if (points != null) {
					PatientAssociationDetailsV detailsV = associationDetailsVS.stream()
					                                                          .filter(detail -> detail.getPatientId()
					                                                                                  .equals(patientId))
					                                                          .findAny()
					                                                          .orElse(null);
					if (detailsV != null) {
						nonAdherentPatients.add(detailsV);
					}
				}
			}
		}
		return nonAdherentPatients;
	}
}
