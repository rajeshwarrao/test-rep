package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.specification.PatientCaregiverSpecification;
import com.sg.dayamed.repository.view.PatientCaregiverAssociationsVRepository;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Component
public class PatientLoadingFromCaregiverHandler extends BasePatientInitLoadingHandler {

	@Autowired
	PatientLoadingFromCaregiverHandler self;

	@Autowired
	PatientCaregiverAssociationsVRepository caregiverAssociationsVRepository;

	@Override
	public BasePatientInitLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	public String[] getUserTypes() {
		return new String[]{UserRoleEnum.CAREGIVER.getRole()};
	}

	@Override
	public List<PatientAssociationDetailsV> getPatientAssociationDetails(UserRoleEnum userRoleEnum,
	                                                                     LoadPatientRequestVO requestVO)
			throws ApplicationException {
		try {
			List<PatientCaregiverAssociationsV> caregiverAssociationsVS = caregiverAssociationsVRepository.findAll(
					new PatientCaregiverSpecification(userRoleEnum, requestVO));
			caregiverAssociationsVS = caregiverAssociationsVS.stream()
			                                                 .filter(distinctByKey(detail -> detail.getPatientId()))
			                                                 .collect(Collectors.toList());
			return caregiverAssociationsVS.stream()
			                              .map(src -> mapperFacade.map(src, PatientAssociationDetailsV.class))
			                              .collect(Collectors.toList());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
