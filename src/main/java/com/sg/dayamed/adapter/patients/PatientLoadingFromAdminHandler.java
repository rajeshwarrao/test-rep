package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Component
public class PatientLoadingFromAdminHandler extends BasePatientInitLoadingHandler {

	@Autowired
	PatientLoadingFromAdminHandler self;

	@Override
	public BasePatientInitLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	public String[] getUserTypes() {
		return new String[]{UserRoleEnum.ADMIN.getRole()};
	}
}
