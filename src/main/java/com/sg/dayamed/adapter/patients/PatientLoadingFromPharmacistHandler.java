package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientPharmacistAssociationsV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.specification.PatientPharmacistSpecification;
import com.sg.dayamed.repository.view.PatientPharmacistAssociationsVRepository;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Component
public class PatientLoadingFromPharmacistHandler extends BasePatientInitLoadingHandler {

	@Autowired
	PatientLoadingFromPharmacistHandler self;

	@Autowired
	PatientPharmacistAssociationsVRepository pharmacistAssociationsVRepository;

	@Override
	public BasePatientInitLoadingHandler getUserTypeHandler() {
		return self;
	}

	@Override
	public String[] getUserTypes() {
		return new String[]{UserRoleEnum.PHARMACIST.getRole()};
	}

	@Override
	public List<PatientAssociationDetailsV> getPatientAssociationDetails(UserRoleEnum userRoleEnum,
	                                                                     LoadPatientRequestVO requestVO)
			throws ApplicationException {
		try {
			List<PatientPharmacistAssociationsV> patientPharmacistAssociationsVS =
					pharmacistAssociationsVRepository.findAll(
							new PatientPharmacistSpecification(userRoleEnum, requestVO));
			patientPharmacistAssociationsVS = patientPharmacistAssociationsVS.stream()
			                                                                 .filter(distinctByKey(
					                                                                 detail -> detail.getPatientId()))
			                                                                 .collect(Collectors.toList());
			return patientPharmacistAssociationsVS.stream()
			                                      .map(src -> mapperFacade.map(src, PatientAssociationDetailsV.class))
			                                      .collect(Collectors.toList());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
