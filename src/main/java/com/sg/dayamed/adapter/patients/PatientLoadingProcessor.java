package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Component
public class PatientLoadingProcessor {

	@Autowired
	PatientLoadingAdapter adapter;

	public PatientDetailsResponseVO getPatients(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO)
			throws ApplicationException {
		try {
			BasePatientInitLoadingHandler handler = adapter.getHandler(userRoleEnum.getRole());
			return handler.getPatients(requestVO);
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
