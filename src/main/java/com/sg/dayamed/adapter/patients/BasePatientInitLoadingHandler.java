package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.specification.PatientLoadingSpecification;
import com.sg.dayamed.repository.view.PatientAssociationDetailsVRepository;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import ma.glasnost.orika.MapperFacade;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/

public abstract class BasePatientInitLoadingHandler {

	@Autowired
	PatientLoadingAdapter adapter;

	@Autowired
	UserValidator userValidator;

	@Autowired
	PatientAssociationDetailsVRepository associationDetailsVRepository;

	@Autowired
	MapperFacade mapperFacade;

	@PostConstruct
	public void init() {
		adapter.registerHandler(getUserTypeHandler(), getUserTypes());
	}

	protected abstract BasePatientInitLoadingHandler getUserTypeHandler();

	protected abstract String[] getUserTypes();

	protected PatientDetailsResponseVO getPatients(LoadPatientRequestVO requestVO) throws ApplicationException {
		PatientDetailsResponseVO responseVO;
		try {
			String authority = requestVO.getUserDetails()
			                            .getAuthorities()
			                            .stream()
			                            .map(auth -> auth.getAuthority())
			                            .findFirst()
			                            .orElse(null);
			UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(authority);
			userValidator.validateGetPatients(userRoleEnum, requestVO);
			List<PatientAssociationDetailsV> associationDetailsVS =
					getPatientAssociationDetails(userRoleEnum, requestVO);
			responseVO = constructPagesForResult(userRoleEnum, requestVO, associationDetailsVS);
			return responseVO;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	protected List<PatientAssociationDetailsV> getPatientAssociationDetails(UserRoleEnum userRoleEnum,
	                                                                        LoadPatientRequestVO requestVO)
			throws ApplicationException {
		try {
			List<PatientAssociationDetailsV> associationDetailsVS =
					associationDetailsVRepository.findAll(new PatientLoadingSpecification(userRoleEnum, requestVO));
			associationDetailsVS = associationDetailsVS.stream()
			                                           .filter(distinctByKey(detail -> detail.getPatientId()))
			                                           .collect(Collectors.toList());
			return associationDetailsVS;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private List<PatientAssociationDetailsV> sortPatientAssociationDetailsVS(LoadPatientRequestVO requestVO,
	                                                                         List<PatientAssociationDetailsV> patientAssociationDetailsVS) {
		if (PatientSortByEnum.FIRST_NAME.equals(requestVO.getSortByEnum()) &&
				SortOrderEnum.ASC.equals(requestVO.getSortOrderEnum())) {
			patientAssociationDetailsVS = patientAssociationDetailsVS.stream().filter(e -> (e != null && e.getFirstName() != null))
			                                                         .sorted(Comparator.comparing(
					                                                         PatientAssociationDetailsV::getFirstName,
					                                                         String::compareToIgnoreCase))
			                                                         .collect(Collectors.toList());
		} else if (PatientSortByEnum.FIRST_NAME.equals(requestVO.getSortByEnum()) &&
				SortOrderEnum.DESC.equals(requestVO.getSortOrderEnum())) {
			patientAssociationDetailsVS = patientAssociationDetailsVS.stream()
			                                                         .sorted(Comparator.comparing(
					                                                         PatientAssociationDetailsV::getFirstName,
					                                                         String::compareToIgnoreCase)
			                                                                           .reversed())
			                                                         .collect(Collectors.toList());
		}
		return patientAssociationDetailsVS;
	}

	protected PatientDetailsResponseVO constructPagesForResult(UserRoleEnum userRoleEnum,
	                                                           LoadPatientRequestVO requestVO,
	                                                           List<PatientAssociationDetailsV> associationDetailsVS)
			throws ApplicationException {
		PatientDetailsResponseVO responseVO = new PatientDetailsResponseVO();
		List<PatientAssociationDetailsV> detailsVS = new ArrayList<>();
		try {
			if (CollectionUtils.isNotEmpty(associationDetailsVS)) {
				List<PatientAssociationDetailsV> patientAssociationDetailsVS = associationDetailsVS;
				if (StringUtils.isNotBlank(requestVO.getQuery())) {
					patientAssociationDetailsVS = associationDetailsVS.stream()
					                                                  .filter(detail -> StringUtils.containsIgnoreCase(
							                                                  detail.getFirstName(),
							                                                  requestVO.getQuery()) ||
							                                                  StringUtils.containsIgnoreCase(
									                                                  detail.getLastName(),
									                                                  requestVO.getQuery()))
					                                                  .collect(Collectors.toList());
				}
				patientAssociationDetailsVS = sortPatientAssociationDetailsVS(requestVO, patientAssociationDetailsVS);

				AtomicInteger count = new AtomicInteger();
				Collection<List<PatientAssociationDetailsV>> collections;
				collections = patientAssociationDetailsVS.stream()
				                                         .collect(Collectors.groupingBy(
						                                         it -> count.getAndIncrement() / requestVO.getLimit()))
				                                         .values();
				Integer index = 0;
				for (Collection collection : collections) {
					if (requestVO.getOffset()
					             .equals(index)) {
						detailsVS = new ArrayList<>(collection);
						break;
					}
					index++;
				}

				List<PatientDetailsVO> patientDetailsVOS = detailsVS.stream()
				                                                    .map(details -> mapperFacade.map(details,
				                                                                                     PatientDetailsVO.class))
				                                                    .collect(Collectors.toList());
				responseVO.setPatientDetails(patientDetailsVOS);
				responseVO.setTotalRecords(
						StringUtils.isNotBlank(requestVO.getQuery()) ? patientAssociationDetailsVS.size() :
								associationDetailsVS.size());
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
		return responseVO;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
		Map<Object, String> seen = new ConcurrentHashMap<>();
		return t -> seen.put(keyExtractor.apply(t), "") == null;
	}
}
