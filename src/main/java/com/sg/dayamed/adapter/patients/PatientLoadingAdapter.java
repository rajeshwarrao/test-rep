package com.sg.dayamed.adapter.patients;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Component
public class PatientLoadingAdapter {

	private Map<String, BasePatientInitLoadingHandler> handlers = new HashMap<>();

	public BasePatientInitLoadingHandler getHandler(String userType) {
		return handlers.get(userType);
	}

	public void registerHandler(BasePatientInitLoadingHandler handler, String... userTypes) {
		if (handler == null) {
			throw new IllegalArgumentException("Handler cannot be null");
		}

		if (ArrayUtils.isEmpty(userTypes)) {
			throw new IllegalArgumentException("import types cannot be null or empty");
		}

		for (String importType : userTypes) {
			handlers.put(importType, handler);
		}
	}
}
