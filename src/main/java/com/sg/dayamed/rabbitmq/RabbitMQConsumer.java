package com.sg.dayamed.rabbitmq;

import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.email.MessageModel;
import com.sg.dayamed.email.SmsService;
import com.sg.dayamed.util.SensitiveDataMaskingUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class RabbitMQConsumer {

	@Autowired
	EmailService emailService;

	@Autowired
	SmsService smsService;

	private static final Logger logger = LoggerFactory.getLogger(RabbitMQConsumer.class);

	//@Async
	@RabbitListener(queues = "${dayamed.rabbitmq.email.queue}", containerFactory = "jsaFactory")
	public void emailReciever(Email email) {
		if (email.getRecipientList() != null) {
			emailService.sendEmail(email);
			String recipientList = email.getRecipientList()
			                            .stream()
			                            .collect(Collectors.joining(", "));
			logger.info("emailReciever invoked to send message to " + SensitiveDataMaskingUtility.maskEmailId(recipientList));
		}

	}

	//@Async
	@RabbitListener(queues = "${dayamed.rabbitmq.sms.queue}", containerFactory = "jsaFactory")
	public void smsReciever(MessageModel messageModel) {
		if (messageModel.getTocontact() != null) {
			smsService.sendSMSNotification(messageModel);
			logger.info("smsReciever invoked to send message to " + SensitiveDataMaskingUtility.maskMobileNumber(messageModel.getTocontact()));
		}
	}
}
