package com.sg.dayamed.rabbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQSenderConfig {

	/*@Value("${dayamed.rabbitmq.queue}")
	String queueName;*/

	@Value("${dayamed.rabbitmq.email.queue}")
	String emailQueue;

	@Value("${dayamed.rabbitmq.sms.queue}")
	String smsQueue;

	@Value("${dayamed.rabbitmq.exchange}")
	String exchange;

	@Value("${dayamed.rabbitmq.routingkey}")
	private String routingkey;

	@Bean
	DirectExchange exchange() {
		return new DirectExchange(exchange);
	}

	//Email Queue
	@Bean
	Queue emailQueue() {
		return new Queue(emailQueue, false);
	}

	@Bean
	Binding emailbinding(Queue emailQueue, DirectExchange exchange) {
		return BindingBuilder.bind(emailQueue)
		                     .to(exchange)
		                     .with(routingkey);
	}

	//Sms Queue
	@Bean
	Queue smsQueue() {
		return new Queue(smsQueue, false);
	}

	@Bean
	Binding smsbinding(Queue smsQueue, DirectExchange exchange) {
		return BindingBuilder.bind(smsQueue)
		                     .to(exchange)
		                     .with(routingkey);
	}

	@Bean
	public MessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(jsonMessageConverter());
		return rabbitTemplate;
	}
}
