package com.sg.dayamed.rabbitmq;

import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.MessageModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.service.RediseService;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQSender {

	@Autowired
	private AmqpTemplate rabbitTemplate;

	@Value("${dayamed.rabbitmq.exchange}")
	private String exchange;

	@Value("${dayamed.rabbitmq.routingkey}")
	private String routingkey;

	@Autowired
	RediseService rediseService;

	public void sendEmail(Email email) {
		if (email != null &&
				(CollectionUtils.isNotEmpty(email.getRecipientList()) && StringUtils.isNotBlank(email.getBody()) &&
						StringUtils.isNotBlank(email.getSubject()))) {
			rabbitTemplate.convertAndSend(exchange, routingkey, email);
		}
	}

	public void sendSms(MessageModel messageModel) {
		String isSMSEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.GS_GLOBAL_SMS_ENABLE);
		if (messageModel != null && (StringUtils.isNotBlank(messageModel.getTocontact()) &&
				StringUtils.isNotBlank(messageModel.getMessage())) &&
				"true".equalsIgnoreCase(isSMSEnable)) {
			rabbitTemplate.convertAndSend(exchange, routingkey, messageModel);
		}
	}
}
