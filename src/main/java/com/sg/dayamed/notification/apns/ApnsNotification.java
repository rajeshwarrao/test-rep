package com.sg.dayamed.notification.apns;

import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.service.UserDetailsService;

import javapns.Push;
import javapns.notification.PushNotificationBigPayload;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class ApnsNotification {

	@Value("${dayamed.apns.path}")
	String certPath;

	@Value("${dayamed.voip.apns.path}")
	String voipcertPath;

	String apnsCredentials = "Seneca123$";

	@Autowired
	UserDetailsService userDetailsService;

	private static final Logger logger = LoggerFactory.getLogger(ApnsNotification.class);

	public void pushApnsNotification(String token, String title, String message) {
		try {
			PushNotificationPayload payload = PushNotificationPayload.complex();
			payload.addAlert(message);
			payload.setCharacterEncoding("utf8");
			payload.addBadge(1);
			payload.addSound("default");
			payload.addCustomDictionary("title", title);
			payload.addCustomDictionary("message", message);
			Boolean pushNotification = userDetailsService.findByToken(token);

			List<PushedNotification> notifications = new ArrayList<>();
			/*File file = null;
			if (StringUtils.isNotBlank(certPath)) {
				file = ResourceUtils.getFile(certPath);
			}*/
			if (certPath.contains("Prod")) {
				/*
				 * service = APNS.newService().withCert(certPath, apnsCredentials)
				 * .withProductionDestination().build();
				 */
				if (pushNotification) {
					notifications = Push.payload(payload, certPath, apnsCredentials, true, token);
				}
			} else {
				if (pushNotification) {
					notifications = Push.payload(payload, certPath, apnsCredentials, false, token);
				}
			}
			logPushnotificationStatus(notifications);
		} catch (Exception e) {
			logger.error("Exception occurred when sending Apns push notification ", e);
		}
	}

	private void logPushnotificationStatus(List<PushedNotification> notifications) {
		for (PushedNotification notification : notifications) {
			if (notification.isSuccessful()) {
				// APPLE ACCEPTED THE NOTIFICATION AND SHOULD DELIVER IT
				logger.info("Push notification sent successfully to: " + notification.getDevice()
				                                                                     .getToken());
				// STILL NEED TO QUERY THE FEEDBACK SERVICE REGULARLY
			} else {
				Exception notificationException = notification.getException();
				logger.error("Exception occurred when sending Apns notification", notificationException);
				// IF THE PROBLEM WAS AN ERROR-RESPONSE PACKET RETURNED BY
				// APPLE, GET IT
				ResponsePacket errorResponse = notification.getResponse();
				if (errorResponse != null) {
					logger.error("Notification error response {}", errorResponse.getMessage());
				}
			}
		}
	}

	public void callApnsNotification(String token, String title, String message, String roomname, String callername,
	                                 VidoeCallSignatureModel vidoeCallSignatureModel) {
		try {
			if (StringUtils.isNotBlank(vidoeCallSignatureModel.getMeetingNumber())
					&& StringUtils.isNotBlank(vidoeCallSignatureModel.getCallType())) {
				String meetingnumber = vidoeCallSignatureModel.getMeetingNumber();
				String calltype = vidoeCallSignatureModel.getCallType();
				String  joinUrl = vidoeCallSignatureModel.getJoin_url();
				String zoomPassword = vidoeCallSignatureModel.getPassword();
				PushNotificationPayload payload = PushNotificationBigPayload.complex();
				payload.addAlert(message);
				payload.addBadge(1);
				payload.addSound("default");
				payload.addCustomDictionary("roomname", roomname);
				payload.addCustomDictionary("callername", callername);
				payload.addCustomDictionary("meetingnumber", meetingnumber);
				payload.addCustomDictionary("join_url", joinUrl);
				payload.addCustomDictionary("password", zoomPassword);
				payload.addCustomDictionary("calltype", calltype);
				//payload.addCustomDictionary("zoomResponse", vidoeCallSignatureModel.getZoomMeetingResponse());
				List<PushedNotification> notifications = new ArrayList<>();
				/*File file = null;
				if (StringUtils.isNotBlank(voipcertPath)) {
					file = ResourceUtils.getFile(voipcertPath);
				}*/
				logger.info("====voipcertPath Notification===" + voipcertPath);
				if (certPath.contains("Prod")) {
					notifications = Push.payload(payload, voipcertPath, apnsCredentials, true, token);
				} else {
					notifications = Push.payload(payload, voipcertPath, apnsCredentials, false, token);
				}
				logPushnotificationStatus(notifications);
			}
		} catch (Exception e) {
			logger.error("Exception on Apns call notification", e);
		}
	}

}

