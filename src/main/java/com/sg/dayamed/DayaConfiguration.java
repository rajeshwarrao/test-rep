package com.sg.dayamed;

import com.sg.dayamed.util.DayamedObjectMapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.management.ManagementService;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.management.MBeanServer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.OptionalInt;
import java.util.stream.IntStream;

/**
 * @author naresh.kamisetti
 * This class is for configuring CORS issues and swagger
 */
@Configuration
@EnableSwagger2
@EnableAsync //for Asynchronous methods(Multi Threding)
public class DayaConfiguration implements WebMvcConfigurer {

	@Value("${rest.client.timeout}")
	int restClientTimeout;

	private final MBeanServer mBeanServer;

	private final CacheManager cacheManager;

	public DayaConfiguration(MBeanServer mBeanServer, CacheManager cacheManager) {
		this.mBeanServer = mBeanServer;
		this.cacheManager = cacheManager;
	}

	// To avoid CORS issue
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
		        //.allowedOrigins("http://192.168.5.28:4200","http://localhost:4200","http://192.168.5.28:8080")
		        .allowedOrigins("*")
		        .allowedHeaders("*")
		        .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
		        .allowCredentials(true);

	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build()
				.globalOperationParameters(Arrays.asList(new ParameterBuilder().name("Authorization")
				                                                               .description("Description of header")
				                                                               .modelRef(new ModelRef("string"))
				                                                               .parameterType("header")
				                                                               .required(true)
				                                                               .build()));
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames("classpath:messages/messages");
		// if true, the key of the message will be displayed if the key is not
		// found, instead of throwing a NoSuchMessageException
		messageSource.setUseCodeAsDefaultMessage(true);
		messageSource.setDefaultEncoding("UTF-8");
		// # -1 : never reload, 0 always reload
		messageSource.setCacheSeconds(600);

		return messageSource;
	}

	@Bean
	public LocalValidatorFactoryBean getValidator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		slr.setDefaultLocale(Locale.US);
		return slr;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("lang");
		return localeChangeInterceptor;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

	@Bean(name = "jsonMapper")
	@Primary
	public ObjectMapper jsonMapper() {
		return new DayamedObjectMapper();
	}

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new MappingJackson2HttpMessageConverter(jsonMapper()));
	}

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());

		OptionalInt index = IntStream.range(0, restTemplate.getMessageConverters()
		                                                   .size())
		                             .filter(i -> restTemplate.getMessageConverters()
		                                                      .get(i) instanceof MappingJackson2HttpMessageConverter)
		                             .findFirst();

		if (index.isPresent()) {
			restTemplate.getMessageConverters()
			            .set(index.getAsInt(), new MappingJackson2HttpMessageConverter(jsonMapper()));
		}
		restTemplate.getMessageConverters()
		            .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		return restTemplate;
	}

	@Bean(initMethod = "init", destroyMethod = "dispose")
	public ManagementService ehCacheManagementService() {
		return new ManagementService(cacheManager, mBeanServer, true, true, true, true, true);

	}

	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(restClientTimeout);
		clientHttpRequestFactory.setReadTimeout(restClientTimeout);
		return clientHttpRequestFactory;
	}
}