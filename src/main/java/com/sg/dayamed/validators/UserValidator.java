package com.sg.dayamed.validators;

import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.stereotype.Component;

/**
 * Created by Eresh Gorantla on 19/Jun/2019
 **/
@Component
public class UserValidator {

	public void validateGetPatients(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO)
			throws DataValidationException {
		validateUserRoleEnum(userRoleEnum);
		if (UserRoleEnum.ADMIN.equals(userRoleEnum)) {
			if (requestVO.getProviderId() == null && requestVO.getCaregiverId() == null) {
				throw new DataValidationException(APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
				                                  APIErrorKeys.ERRORS_EITHER_CAREGIVER_ID_OR_PROVIDER_ID_IS_REQUIRED);
			} else if (requestVO.getCaregiverId() != null && requestVO.getProviderId() != null) {
				throw new DataValidationException(APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
				                                  APIErrorKeys.ERRORS_BOTH_CAREGIVER_ID_OR_PROVIDER_ID_IS_NOT_EXPECTED);
			}
		}
	}

	public void validateGetPatientAssociations(UserRoleEnum userRoleEnum, PatientAssociationsRequestVO requestVO)
			throws DataValidationException {
		validateUserRoleEnum(userRoleEnum);
		if (UserRoleEnum.ADMIN.equals(userRoleEnum)) {
			if (requestVO.getProviderId() == null && requestVO.getCaregiverId() == null) {
				throw new DataValidationException(APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
				                                  APIErrorKeys.ERRORS_EITHER_CAREGIVER_ID_OR_PROVIDER_ID_IS_REQUIRED);
			} else if (requestVO.getCaregiverId() != null && requestVO.getProviderId() != null) {
				throw new DataValidationException(APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
				                                  APIErrorKeys.ERRORS_BOTH_CAREGIVER_ID_OR_PROVIDER_ID_IS_NOT_EXPECTED);
			}
		}
	}

	public UserRoleEnum validatePharmacistPatients(JwtUserDetails userDetails) throws DataValidationException {
		String authority = userDetails.getAuthorities()
		                              .stream()
		                              .map(auth -> auth.getAuthority())
		                              .findFirst()
		                              .orElse(null);
		UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(authority);
		validateUserRoleEnum(userRoleEnum);
		if (UserRoleEnum.PROVIDER.equals(userRoleEnum) && !userDetails.isGsPharmacistEnable()) {
			throw new DataValidationException(APIErrorFields.AUTHORIZATION, IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
		} else if (!(UserRoleEnum.PROVIDER.equals(userRoleEnum) || UserRoleEnum.PHARMACIST.equals(userRoleEnum))) {
			throw new DataValidationException(APIErrorFields.AUTHORIZATION, IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
		}
		return userRoleEnum;
	}

	public void validateSearchPatients(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO, String query)
			throws DataValidationException {
		validateUserRoleEnum(userRoleEnum);
		if (UserRoleEnum.ADMIN.equals(userRoleEnum)) {
			if (requestVO.getProviderId() == null && requestVO.getCaregiverId() == null) {
				throw new DataValidationException(APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
				                                  APIErrorKeys.ERRORS_EITHER_CAREGIVER_ID_OR_PROVIDER_ID_IS_REQUIRED);
			} else if (requestVO.getCaregiverId() != null && requestVO.getProviderId() != null) {
				throw new DataValidationException(APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
				                                  APIErrorKeys.ERRORS_BOTH_CAREGIVER_ID_OR_PROVIDER_ID_IS_NOT_EXPECTED);
			}
		}
		if (query.length() < 3) {
			throw new DataValidationException(APIErrorFields.QUERY_STRING,
			                                  APIErrorKeys.ERRORS_INVALID_LENGTH_FOR_QUERY,
			                                  new Object[]{3});
		}
	}

	private void validateUserRoleEnum(UserRoleEnum userRoleEnum) throws DataValidationException {
		if (userRoleEnum == null) {
			throw new DataValidationException(APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_INVALID_ROLE);
		}
	}

	public void validateUserRole(String userType) throws DataValidationException {
		UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(userType);
		validateUserRoleEnum(userRoleEnum);
	}
}
