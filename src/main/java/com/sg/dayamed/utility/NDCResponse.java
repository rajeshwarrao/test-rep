package com.sg.dayamed.utility;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By Gorantla, Eresh on 21/Aug/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class NDCResponse {
	private NDCData replyStatus;
	private List<RXImages> nlmRxImages = new ArrayList<>();
}
