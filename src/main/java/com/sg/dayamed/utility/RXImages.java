package com.sg.dayamed.utility;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 21/Aug/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class RXImages {
	private String ndc11;
	private String imageUrl;
	private String name;
	private String rxcui;
}
