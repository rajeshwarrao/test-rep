package com.sg.dayamed.utility;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 21/Aug/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class NDCData {
	private Boolean success;
	private Integer totalImageCount;
	private String medicineId;
	private String packager;
	private String ndcCode;
	private String imageURL;
}
