package com.sg.dayamed.utility;

import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.repository.MedicineRepository;

import com.opencsv.CSVWriter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 22/Aug/2019
 **/
@Component
public class USHOImageRenderingComponent {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MedicineRepository medicineRepository;

	@Autowired
	RestTemplate restTemplate;

	public void generateUSHOMedicineImages() {
		List<Medicine> medicineList = medicineRepository.findAll();
		List<NDCResponse> ndcResponses = new ArrayList<>();

		medicineList.stream()
		            .forEach(medicine -> {
			            String url = "https://rximage.nlm.nih.gov/api/rximage/1/rxnav?ndc=" + medicine.getNdcCode();
			            ResponseEntity<NDCResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, NDCResponse.class);
			            NDCResponse response = responseEntity.getBody();
			            if (CollectionUtils.isNotEmpty(response.getNlmRxImages())) {
				            System.out.println("Medicine Id => " + medicine.getId());
				            response.getReplyStatus()
				                    .setMedicineId(String.valueOf(medicine.getId()));
				            response.getReplyStatus()
				                    .setNdcCode(medicine.getNdcCode()
				                                        .toString());
				            response.getReplyStatus()
				                    .setPackager(StringUtils.contains(medicine.getPackager(), " ") ? medicine.getPackager()
				                                                                                             .replaceAll(" ", "_") :
						                                 medicine.getPackager());
				            ndcResponses.add(response);
			            }
		            });

		if (CollectionUtils.isNotEmpty(ndcResponses)) {
			try {
				Path path = Paths.get("ndc_images");
				Files.createDirectories(path);
			} catch (Exception e) {
				logger.error("Unable to create ndc_images folder", e);
			}
		}
		for (NDCResponse ndcResponse : ndcResponses) {
			RXImages images = ndcResponse.getNlmRxImages()
			                             .get(0);
			String srcFileName = images.getImageUrl();
			String srcFileExt = StringUtils.substringAfterLast(images.getImageUrl(), ".");
			String fileName = ndcResponse.getReplyStatus()
			                             .getNdcCode() + "_" + ndcResponse.getReplyStatus()
			                                                              .getPackager() + "." + srcFileExt;
			ndcResponse.getReplyStatus()
			           .setImageURL(fileName);
			BufferedImage bufferedImage;
			try {
				bufferedImage = ImageIO.read(new URL(srcFileName));
				ImageIO.write(bufferedImage, srcFileExt, new File("ndc_images/" + fileName));
			} catch (Exception e) {
				logger.error("Error in downloading image", e);
			}
		}
		List<Data> dataS = ndcResponses.stream()
		                               .filter(response -> StringUtils.isNotBlank(response.getReplyStatus()
		                                                                                  .getImageURL()))
		                               .map(response -> new Data(NumberUtils.toLong(response.getReplyStatus()
		                                                                                    .getMedicineId()), response.getReplyStatus()
		                                                                                                               .getImageURL()))
		                               .collect(Collectors.toList());
		//generateCSVFile(dataS);
	}

	/*private void generateCSVFile(List<Data> dataList) {
		String fileName = "medicine_images-prod-1.csv";
		CSVWriter csvWriter;
		Writer writer = null;
		try {
			new File(fileName).createNewFile();
			writer = new FileWriter(new File(fileName));
			csvWriter = new CSVWriter(writer,
			                          CSVWriter.DEFAULT_SEPARATOR,
			                          CSVWriter.NO_QUOTE_CHARACTER,
			                          CSVWriter.DEFAULT_ESCAPE_CHARACTER,
			                          CSVWriter.DEFAULT_LINE_END);
			String[] headerRecord = {"medicine_id", "image_url"};
			csvWriter.writeNext(headerRecord);
			dataList.stream()
			        .forEach(data -> {
				        csvWriter.writeNext(new String[]{data.getMedicineId().toString(), data.getImageName()});
			        });

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}*/

	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	public class Data {

		private Long medicineId;

		private String imageName;
	}
}
