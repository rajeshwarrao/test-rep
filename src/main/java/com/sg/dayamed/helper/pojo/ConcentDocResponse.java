package com.sg.dayamed.helper.pojo;

import org.springframework.stereotype.Component;

@Component
public class ConcentDocResponse {

	public ConcentDocResponse() {
		super();
	}

	public ConcentDocResponse(String response, String version) {
		super();
		this.response = response;
		this.version = version;
	}

	private String response;

	private String version;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
