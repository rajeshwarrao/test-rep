package com.sg.dayamed.helper.pojo;

public class PatientAdherencePojo {

	private String predictedAdherence;

	private String currentAdherence;

	private String projectedAdherence;

	private String emoji;

	public PatientAdherencePojo(String currentAdherence, String predictedAdherence, String projectedAdherence,
	                            String emoji) {
		// NumberFormat decimalFormat = new DecimalFormat("#0.00");

		this.predictedAdherence = predictedAdherence;
		this.currentAdherence = currentAdherence;
		this.projectedAdherence = projectedAdherence;
		this.emoji = emoji;
	}

	public String getPredictedAdherence() {
		return predictedAdherence;
	}

	public void setPredictedAdherence(String predictedAdherence) {
		this.predictedAdherence = predictedAdherence;
	}

	public String getCurrentAdherence() {
		return currentAdherence;
	}

	public void setCurrentAdherence(String currentAdherence) {
		this.currentAdherence = currentAdherence;
	}

	public String getProjectedAdherence() {
		return projectedAdherence;
	}

	public void setProjectedAdherence(String projectedAdherence) {
		this.projectedAdherence = projectedAdherence;
	}

	public String getEmoji() {
		return emoji;
	}

	public void setEmoji(String emoji) {
		this.emoji = emoji;
	}

}
