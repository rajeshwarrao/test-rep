package com.sg.dayamed.helper.pojo;

import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.rest.commons.RestResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetailsWithJwtModel extends RestResponse {

	private long id;

	private String jwtToken;

	private UserDetailsModel userDetails;

	private boolean gsPharmacistEnable;
}
