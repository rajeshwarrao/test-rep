package com.sg.dayamed.helper.pojo;

import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.ProviderModel;

import java.util.List;

public class PatientAssociatedActorsPojo {

	private long patietnID;

	private List<ProviderModel> providerModelList;

	private List<CaregiverModel> caregiverModelList;

	private List<PharmacistModel> pharmacistModelList;

	public long getPatietnID() {
		return patietnID;
	}

	public void setPatietnID(long patietnID) {
		this.patietnID = patietnID;
	}

	public List<ProviderModel> getProviderModelList() {
		return providerModelList;
	}

	public void setProviderModelList(List<ProviderModel> providerModelList) {
		this.providerModelList = providerModelList;
	}

	public List<CaregiverModel> getCaregiverModelList() {
		return caregiverModelList;
	}

	public void setCaregiverModelList(List<CaregiverModel> caregiverModelList) {
		this.caregiverModelList = caregiverModelList;
	}

	public List<PharmacistModel> getPharmacistModelList() {
		return pharmacistModelList;
	}

	public void setPharmacistModelList(List<PharmacistModel> pharmacistModelList) {
		this.pharmacistModelList = pharmacistModelList;
	}

}
