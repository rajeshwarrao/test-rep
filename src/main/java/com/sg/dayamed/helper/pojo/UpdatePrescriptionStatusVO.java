package com.sg.dayamed.helper.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdatePrescriptionStatusVO {
	
	private String prescriptionId;
	private String status;
}