package com.sg.dayamed.helper.pojo;

import java.util.List;

public class LatestGraphSuperJsonModel {

	private List<LatestGraphJsonModel> datasets;

	public List<LatestGraphJsonModel> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<LatestGraphJsonModel> datasets) {
		this.datasets = datasets;
	}
}
