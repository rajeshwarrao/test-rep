package com.sg.dayamed.helper.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericIDAndZoneVO {
	
	@JsonProperty("patientid")
	private String patientId;
	private String zoneId;
}