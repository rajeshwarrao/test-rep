package com.sg.dayamed.helper.pojo;

import com.sg.dayamed.pojo.UserPreferenceModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResultVO {

	private String emailId;

	private UserPreferenceModel model;

}
