package com.sg.dayamed.helper.pojo;

import com.sg.dayamed.pojo.DosageNotificationModel;

import java.util.List;

public class CaregiverDosageNotificationModel {

	private long patientId;

	private long prescriptionId;

	private long dosageInfoID;

	private long owneruserid;

	private List<DosageNotificationModel> DosageNotificationList;

	public List<DosageNotificationModel> getDosageNotificationList() {
		return DosageNotificationList;
	}

	public void setDosageNotificationList(List<DosageNotificationModel> dosageNotificationList) {
		DosageNotificationList = dosageNotificationList;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public long getDosageInfoID() {
		return dosageInfoID;
	}

	public void setDosageInfoID(long dosageInfoID) {
		this.dosageInfoID = dosageInfoID;
	}

	public long getOwneruserid() {
		return owneruserid;
	}

	public void setOwneruserid(long owneruserid) {
		this.owneruserid = owneruserid;
	}
}
