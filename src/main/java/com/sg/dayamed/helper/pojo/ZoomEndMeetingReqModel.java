package com.sg.dayamed.helper.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ZoomEndMeetingReqModel {

	private String event;

	private ZoomEndMeetingPayloadReqModel payload;

}
