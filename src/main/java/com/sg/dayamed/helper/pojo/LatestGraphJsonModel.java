package com.sg.dayamed.helper.pojo;

import java.util.List;
import java.util.Map;

public class LatestGraphJsonModel {

	private String label;

	private List<Map<String, String>> data;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<Map<String, String>> getData() {
		return data;
	}

	public void setData(List<Map<String, String>> data) {
		this.data = data;
	}
}
