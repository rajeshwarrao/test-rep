package com.sg.dayamed.helper.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ZoomEndMeetingPayloadReqModel {

	private String account_id;

	private ZoomEndMeetingObjectReqModel object;
}
