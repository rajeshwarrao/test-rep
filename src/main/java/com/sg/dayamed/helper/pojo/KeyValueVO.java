package com.sg.dayamed.helper.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyValueVO {

	private String key;

	private String value;

	public KeyValueVO(String key, String value) {
		this.key = key;
		this.value = value;
	}
}
