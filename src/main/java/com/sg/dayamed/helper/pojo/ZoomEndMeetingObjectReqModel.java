package com.sg.dayamed.helper.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ZoomEndMeetingObjectReqModel {

	private Long duration;

	private String start_time;

	private String timezone;

	private String topic;

	private String id;

	private Integer type;

	private String uuid;

	private String host_id;

}
