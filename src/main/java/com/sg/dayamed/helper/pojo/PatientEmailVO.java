package com.sg.dayamed.helper.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientEmailVO {
	
	private String patientEmailId;
}