package com.sg.dayamed.helper;

import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Diagnosis;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.DiagnosisModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.repository.ConsumptionFrequencyRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.GlucoMeterRepository;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.DiagnosisService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.Utility;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PrescriptionHelper {

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ProviderRepository providerRepository;

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	@Autowired
	DiseaseRepository diseaseRepository;

	@Autowired
	PatientService patientService;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	HeartRateRepository heartRateRepository;

	@Autowired
	GlucoMeterRepository glucoMeterRepository;

	@Autowired
	BpMonitorRepository bpMonitorRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	PrescriptionStatusRepository prescriptionStatusRepository;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	ProviderRepository provoiderRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	Utility utility;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	RediseService rediseService;

	@Autowired
	ConsumptionFrequencyRepository consumptionFrequencyRepository;

	@Autowired
	DiagnosisService diagnosisService;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	public List<PrescriptionSchedule> fetchPrescriptionSchedulesByUserId(LocalDateTime actualDate, long userId) {
		return prescriptionScheduleRepository.findByPrescribedTimeGreaterThanEqualAndUserId(actualDate, userId);
	}

	public List<PrescriptionSchedule> fetchPrescriptionSchedulesBetweenActaulDatesByUserId(LocalDateTime startDate,
	                                                                                       LocalDateTime endDate,
	                                                                                       long userId) {
		return prescriptionScheduleRepository.findByPrescribedTimeBetweenAndUserId(startDate, endDate, userId);
	}

	public PrescriptionModel addPrescription(PrescriptionModel prescriptionmodel, Patient encypatient, long userid) {

		String msg = ApplicationConstants.EMPTY_STRING;
		String patientmsg = ApplicationConstants.EMPTY_STRING;
		String title = ApplicationConstants.EMPTY_STRING;

		List<DiagnosisModel> diagnosisModelList = new ArrayList<DiagnosisModel>();
		if (CollectionUtils.isNotEmpty(prescriptionmodel.getDiagnosisList())) {
			for (DiagnosisModel diagnosisModel : prescriptionmodel.getDiagnosisList()) {
				if (diagnosisModel != null && diagnosisModel.getId() == null) {
					Diagnosis diagnosis = new Diagnosis();
					diagnosis.setName(diagnosisModel.getName());
					Diagnosis persistedDiagnosis = diagnosisService.addDiagnosis(diagnosis);
					diagnosisModel = utility.convertDiagnosisEntityToModel(persistedDiagnosis);
					diagnosisModelList.add(diagnosisModel);
				} else if (diagnosisModel != null && diagnosisModel.getId() != null) {
					diagnosisModelList.add(diagnosisModel);
				}
			}
		}

		prescriptionmodel.setDiagnosisList(diagnosisModelList);

		Prescription Prescriptionentity = utility.convertPrescriptionModelToEntity(prescriptionmodel);
		//Prescriptionentity.setDate(Date.from(Instant.now()));//
		if (prescriptionmodel.getId() == 0 && Prescriptionentity.getCretedDate() == null) {
			Prescriptionentity.setCretedDate(LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC));
		}
		Prescriptionentity.setUpdatedDate(LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC));
		PrescriptionStatus prescriptionStatus = prescriptionStatusRepository.findByName("New");
		Prescriptionentity.setPrescriptionStatus(prescriptionStatus);
		List<String> recipientList = new ArrayList<String>();

		UserDetails patientdecryUserDetails = encypatient.getUserDetails();
		String msgKey = ApplicationConstants.EMPTY_STRING;
		String patientmsgKey = ApplicationConstants.EMPTY_STRING;
		String titleResource = ApplicationConstants.EMPTY_STRING;
		String patientName = patientdecryUserDetails.getFirstName() + " " + patientdecryUserDetails.getLastName();
		if (Prescriptionentity.getId() > 0) {
			msg = "Prescription updated for" + " " + patientdecryUserDetails.getFirstName() + " "
					+ patientdecryUserDetails.getLastName();
			//patientmsg = "Your Prescription has been updated ";
			//title = "Prescription updated";
			titleResource = "emailsubject.prescriptionupdated";
			msgKey = "prescription.updated.for.patient";
			patientmsgKey = "your.prescription.updated";

		} else {
			msg = "New Prescription added for" + " " + patientdecryUserDetails.getFirstName() + " "
					+ patientdecryUserDetails.getLastName();
			//patientmsg = "New Prescription has been added";
			//title = "Prescription added";
			titleResource = "prescription.added";
			msgKey = "prescription.added.new.patient";
			patientmsgKey = "prescription.added.new";
			//sending mail to reciepents

		}
		//while adding/updating this should be null, only aftr start prescription only it sholud have value.
		Prescriptionentity.setStartDate(null);
		PrescriptionModel prescriptionmodell =
				patientService.addPrescription(Prescriptionentity, encypatient.getId(), userid);

		if (prescriptionmodell != null) {
			Map<String, String> msgResourceReplaceKeyVal = new HashMap<String, String>();
			msgResourceReplaceKeyVal.put("patientName", patientName);
			String userLangPreference = utility.getUserPreferedLang(patientdecryUserDetails.getId());
			String msgNew =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, msgKey,
					                                                              userLangPreference, null);
			patientmsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, patientmsgKey,
			                                                                           userLangPreference, null);
			title = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, titleResource,
			                                                                      userLangPreference, null);
			// send notification to patient 
			//notificationService.sendMobileNotification(patientdecryUserDetails.getType(), msg, title,
			// patientdecryUserDetails.getToken());
			notificationService.sendMobileNotification(patientdecryUserDetails.getType(), msgNew, title,
			                                           patientdecryUserDetails.getToken());
			//For notification history we are sending data to notification table too 
			List<Long> notificationRecieverUseridsList = new ArrayList<>();
			notificationRecieverUseridsList.add(patientdecryUserDetails.getId());
			recipientList.add(patientdecryUserDetails.getEmailId());
			if (encypatient.getUserDetails()
			               .isEmailFlag()) {
				emailAndSmsUtility.sendEmail(title, patientmsg, recipientList, true);
			}
			recipientList.clear();
			if (encypatient.getUserDetails()
			               .isSmsFlag()) {
				emailAndSmsUtility.sendSms(patientmsg, patientdecryUserDetails.getCountryCode() +
						patientdecryUserDetails.getMobileNumber());
			}
			//send notifications to cartegiver
			if ((encypatient.getCaregivers() != null) && CollectionUtils.isNotEmpty(encypatient.getCaregivers())) {

				for (Caregiver caregiver : encypatient.getCaregivers()) {
					if ((caregiver.getUserDetails() != null && caregiver.getUserDetails()
					                                                    .getEmailId() != null)) {
						userLangPreference = utility.getUserPreferedLang(caregiver.getUserDetails()
						                                                          .getId());
						msgNew =
								messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal,
								                                                              msgKey,
								                                                              userLangPreference, null);
						title = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, titleResource,
						                                                                      userLangPreference, null);
						UserDetails caregiverDecryUserDetails = caregiver.getUserDetails();
						if (caregiverDecryUserDetails.getType() != null &&
								(caregiverDecryUserDetails.getType()
								                          .equalsIgnoreCase(ApplicationConstants.ANDROID) ||
										caregiverDecryUserDetails.getType()
										                         .equalsIgnoreCase(ApplicationConstants.IOS))) {
							notificationService.sendMobileNotification(caregiverDecryUserDetails.getType(), msgNew,
							                                           title, caregiverDecryUserDetails.getToken());
						}
						notificationRecieverUseridsList.add(caregiverDecryUserDetails.getId());
						recipientList.add(caregiverDecryUserDetails.getEmailId());
						emailAndSmsUtility.sendSms(msgNew, caregiverDecryUserDetails.getCountryCode(),
						                           caregiverDecryUserDetails.getMobileNumber());
						// added into notification table for caregiver calander.
						notificationService.addWebNotification(caregiverDecryUserDetails.getId(), msgNew, title,
						                                       title);
					}
					emailAndSmsUtility.sendEmail(titleResource, msgKey, msgResourceReplaceKeyVal, recipientList);
					recipientList.clear();
				}
			}
			// send notification to pharmacists
			if ((encypatient.getPharmacists() != null) && (encypatient.getPharmacists()
			                                                          .size() > 0)) {
				encypatient.getPharmacists()
				           .forEach(pharmacist -> {
					           if ((pharmacist.getUserDetails() != null)) {
						           notificationRecieverUseridsList.add(pharmacist.getUserDetails()
						                                                         .getId());
					           }
				           });
			}
			//For showing notification calender
			notificationService.addWebNotification(notificationRecieverUseridsList, encypatient.getUserDetails()
			                                                                                   .getId(), msgNew, title,
			                                       ApplicationConstants.PRESCRIPTION);
		}
		return prescriptionmodell;
	}

}
