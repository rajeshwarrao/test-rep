package com.sg.dayamed.helper;

import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.AdherenceRepository;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.repository.GlucoMeterRepository;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.ScaleRepository;
import com.sg.dayamed.repository.StepsRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.DeviceStatusService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.service.ProviderService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author naresh.kamisetti This helper class is for patient opertaions in
 * patient controller.
 */
@Component
public class PatientHelper {

	@Autowired
	Utility utility;

	@Autowired
	PatientService patientService;

	@Autowired
	ProviderService providerService;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	HeartRateRepository heartRateRepository;

	@Autowired
	StepsRepository stepsRepository;

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	NotificationScheduleRepository notificationScheduleRepository;

	@Autowired
	GlucoMeterRepository glucoMeterRepository;

	@Autowired
	BpMonitorRepository bpMonitorRepository;

	@Autowired
	ScaleRepository scaleRepository;

	@Autowired
	PrescriptionStatusRepository prescriptionStatusRepository;

	@Autowired
	PharmacistService pharmacistService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	AdherenceRepository adherenceRepository;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DeviceStatusService deviceStatusService;

	@Autowired
	ObjectMapper userPreferenceModelMapper;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	UserPreferenceRepository userPreferenceRepository;

	@Autowired
	UserPreferenceService userPreferenceService;

	private static final Logger logger = LoggerFactory.getLogger(PatientHelper.class);

	public PatientModel addPatient(PatientModel patientModel, Authentication authentication) throws DataValidationException {

		Patient patient = utility.convertPatientModelTopatientEntity(patientModel);
		Set<Caregiver> caregiverset = new LinkedHashSet<Caregiver>();
		Set<Provider> providerset = new LinkedHashSet<Provider>();

		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		// if provider not available in cookie should not add patient in db
		if (jwtUser != null && jwtUser.getRole()
		                              .equalsIgnoreCase("provider")) {
			Provider provider = providerService.directFetchProviderByUserId(jwtUser.getUserId());
			if (provider != null) {
				//GS Change
				Provider provider2 = providerService.directFetchProviderById(provider.getId());
				providerset.add(provider2);
				patient.setProviders(providerset);
			} else {
				logger.error("Something went wrong with Authuntication");
				return null;
			}
			Role role = roleRepository.findByName("patient");
			patient.getUserDetails()
			       .setRole(role);
		}

		PatientModel persistedpatientmodel = null;
		//if null adding patient else update patient.
		if (patientModel.getUserDetails()
		                .getImanticUserid() == null) {
			for (CaregiverModel caregivermodel : patientModel.getCaregiverList()) {
				Caregiver caregiver = caregiverService.directFetchCaregiverById(caregivermodel.getId());
				caregiverset.add(caregiver);
			}
			patient.setCaregivers(caregiverset);
			// assigning pharmacist to patient
			Set<Pharmacist> pharmacistset = new LinkedHashSet<Pharmacist>();
			//long pharamUserId = jwtUser.isGsPharmacistEnable() ? jwtUser.getUserId()  : 1L;
			if (jwtUser != null && jwtUser.isGsPharmacistEnable()) {
				Pharmacist pharmacist = pharmacistService.directFetchPharmacistByUserId(jwtUser.getUserId());
				if (pharmacist != null) {
					pharmacistset.add(pharmacist);
				}
			} else {
				Pharmacist pharmacist = pharmacistService.directFetchPharmacistById(1L);
				if (pharmacist != null) {
					pharmacistset.add(pharmacist);
				}
			}
			if (CollectionUtils.isEmpty(pharmacistset)) {
				logger.error("pharmacist not assigned to patient");
				return null;
			}
			patient.setPharmacists(pharmacistset);
			patient.getUserDetails()
			       .setEmailFlag(patientModel.isEmailFlag());
			patient.getUserDetails()
			       .setSmsFlag(patientModel.isSmsFlag());
			persistedpatientmodel = patientService.addPatient(patient);
			//adding userPreferences
			if (persistedpatientmodel != null && persistedpatientmodel.getUserDetails() != null) {
				try {
					userPreferenceService.addUserPreference(persistedpatientmodel.getUserDetails()
					                                                             .getId(), new UserPreferenceModel());
				} catch (Exception e) {
					logger.error("json parsing exception while adding userpref of patient", e);
				}
			}
		} else {
			for (CaregiverModel caregivermodel : patientModel.getCaregiverList()) {
				Caregiver caregiver = caregiverService.fetchCaregiverById(caregivermodel.getId());
				caregiverset.add(caregiver);
			}
			patient.setCaregivers(caregiverset);
			Patient existedPatient = patientService.noDecrypCargiverfetchPatientById(patient.getId());
			patient.getUserDetails()
			       .setToken(existedPatient.getUserDetails()
			                               .getToken());
			patient.getUserDetails()
			       .setVoipToken(existedPatient.getUserDetails()
			                                   .getVoipToken());
			if (existedPatient.getUserDetails()
			                  .getCreateOn() == null) {
				patient.getUserDetails()
				       .setCreateOn(Date.from(Instant.now()));
			} else {
				patient.getUserDetails()
				       .setCreateOn(existedPatient.getUserDetails()
				                                  .getCreateOn());
				patient.getUserDetails()
				       .setUpdateOn(Date.from(Instant.now()));
			}
			UserDetails patientUserdetails = patient.getUserDetails();
			//email & sms flags coming in patient model insted of userdetailsModel obj
			patientUserdetails.setEmailFlag(patientModel.isEmailFlag());
			patientUserdetails.setSmsFlag(patientModel.isSmsFlag());
			existedPatient.setUserDetails(patientUserdetails);
			//existedPatient.getUserDetails().setPassword(patient.getUserDetails().getFirstName());
			existedPatient.setCaregivers(patient.getCaregivers());
			persistedpatientmodel = patientService.updatePatient(existedPatient);
			//updating user pref
			try {
				userPreferenceService.addUserPreference(persistedpatientmodel.getUserDetails()
				                                                             .getId(), new UserPreferenceModel());
			} catch (Exception e) {
				logger.error("json parsing exception while adding userpref of patient", e);
			}
		}
		return persistedpatientmodel;
	}

	public String deletePatient(String id) {
		return patientService.deletePatientByProcedure(Long.parseLong(id));
	}
}
