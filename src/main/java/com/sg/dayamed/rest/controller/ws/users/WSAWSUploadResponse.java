package com.sg.dayamed.rest.controller.ws.users;

import com.sg.dayamed.rest.commons.RestResponse;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 18/Sep/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSAWSUploadResponse extends RestResponse {
	private String imageUrl;
}
