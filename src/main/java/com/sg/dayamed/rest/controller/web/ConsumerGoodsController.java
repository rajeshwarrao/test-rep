package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.ConsumerGoodsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerGoodsController extends BaseRestApi {

	@Autowired
	private ConsumerGoodsService consumerGoodsService;

	@GetMapping(value = "/rest/utility/consumergoods/bytype/{type}", produces = "application/json")
	public ResponseEntity<Object> fetchConsumerGoodsByType(@PathVariable(value = "type") String type)
			throws RestApiException {
		return inboundServiceCallGeneric(type, service -> ResponseEntity.ok(
				consumerGoodsService.getConsumerGoodsByType(type)));
	}

}
