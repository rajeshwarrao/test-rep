package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.pojo.NDCImageModel;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.MedicineService;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedicineController extends BaseRestApi {

	@Autowired
	private MedicineService medicineService;

	@PutMapping(value = "/rest/utility/updateNDCImageURL", produces = "application/json")
	@ApiOperation(value = "updateNDCImageUR", notes = "Update Medicine table with NDC Image URL")
	public ResponseEntity<Object> updateMedicineImageURL(@RequestBody NDCImageModel ndcImageModel)
			throws RestApiException {
		return inboundServiceCallGeneric(ndcImageModel, service -> {
			medicineService.updateImages(ndcImageModel);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		});
	}

	@GetMapping(value = "/rest/medicine", produces = "application/json")
	@ApiOperation(value = "Get Medicines", notes = "Returns all medicines matching the provided filter")
	public ResponseEntity<Object> fetchMedicines(@CurrentUser JwtUserDetails jwtUser,
	                                             @RequestParam(name = "name") String name,
	                                             @RequestParam(name = "offset", defaultValue = "0") Integer offset,
	                                             @RequestParam(name = "limit", defaultValue = "10") Integer limit)
			throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			final Page<Medicine> medicinesContaining = medicineService.getMedicines(name, offset, limit);
			return ResponseEntity.ok(medicinesContaining);
		});
	}
}
