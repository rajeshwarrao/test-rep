package com.sg.dayamed.rest.controller.ws.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang.StringUtils;

import java.time.LocalDateTime;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSUserDetails {

	private WSRole role;

	private Long id;

	private String age;

	private String appVersion;

	private String countryCode;

	private LocalDateTime createdOn;

	private String deviceType;

	private String emailId;

	private Boolean emailFlag = false;

	private String firstName;

	private String gender;

	private WSAddress address;

	private String imageUrl = StringUtils.EMPTY;

	private String imanticUserId;

	private LocalDateTime lastActiveTime;

	private LocalDateTime lastLoginTime;

	private String lastName;

	private String middleName;

	private String mobileNumber;

	private String osVersion;

	private String pwdChangeToken;

	private LocalDateTime pwdTokenExpireDate;

	private String race;

	private Boolean smsFlag = false;

	private String token;

	private LocalDateTime updatedOn;

	private String zoneId;

	private String voipToken;

	private String deviceId;
}
