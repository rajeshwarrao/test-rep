package com.sg.dayamed.rest.controller.ws.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPatientDetails {

	private Long id;

	private String emoji;

	private String currentAdherence;

	private String patientDeviceId;

	private String predictedAdherence;

	private String projectedAdherence;

	private Integer patientStatus;

	private WSUserDetails userDetails;
}
