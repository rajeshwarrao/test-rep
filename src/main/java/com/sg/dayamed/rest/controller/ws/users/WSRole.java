package com.sg.dayamed.rest.controller.ws.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSRole {

	private Long id;

	private String name;

	public WSRole(Long roleId, String role) {
		this.id = roleId;
		this.name = role;
	}
}
