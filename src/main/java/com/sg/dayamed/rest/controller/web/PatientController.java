package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.commons.rest.RestRequest;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSPatientAssociationDetailsResponse;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetails;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetailsResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.v1.PatientAssociationDetailsService;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@RestController(value = PatientController.RESOURCE_NAME + "_" + PatientController.API_VERSION)
@RequestMapping("/rest/" + PatientController.API_VERSION)
public class PatientController extends BaseRestApi {

	public static final String API_VERSION = "w";

	public static final String RESOURCE_NAME = "PatientController";

	@Autowired
	PatientAssociationDetailsService associationDetailsService;

	@Autowired
	MapperFacade mapperFacade;

	@GetMapping("/patients")
	public ResponseEntity<RestResponse> loadPatients(@CurrentUser JwtUserDetails jwtUser,
	                                                 @RequestParam(name = "offset", defaultValue = "0") Integer offset,
	                                                 @RequestParam(name = "limit", defaultValue = "10") Integer limit,
	                                                 @RequestParam(name = "sortBy", defaultValue = "lastActiveTime") String sortBy,
	                                                 @RequestParam(name = "sortOrder", defaultValue = "desc") String sortOrder,
	                                                 @RequestParam(name = "nonAdherentFlag", defaultValue = "false") Boolean nonAdherentFlag,
	                                                 @RequestParam(name = "providerId", required = false) Long providerId,
	                                                 @RequestParam(name = "caregiverId", required = false) Long caregiverId,
	                                                 @RequestParam(name = "query", required = false) String query)
			throws RestApiException {
		return inboundServiceCall(new RestRequest(), service -> {

			                          PatientDetailsResponseVO responseVO = associationDetailsService.getPatients(
					                          new LoadPatientRequestVO(jwtUser, limit, offset, sortBy, sortOrder,
					                                                   nonAdherentFlag, providerId,
					                                                   caregiverId, query));
			                          WSPatientDetailsResponse response = new WSPatientDetailsResponse();
			                          response.setTotalRecords(responseVO.getTotalRecords());
			                          response.setPatients(responseVO.getPatientDetails()
			                                                         .stream()
			                                                         .map(detail -> mapperFacade.map(detail,
			                                                                                         WSPatientDetails.class))
			                                                         .collect(Collectors.toList()));
			                          return new ResponseEntity<>(response, HttpStatus.OK);
		                          }, Stream.of("firstName", "lastName", "mobileNumber", "emailId", "city", "country",
		                                       "location", "state",
		                                       "zipCode", "age")
		                                   .collect(Collectors.toList()), null,
		                          WSPatientDetailsResponse.class, jwtUser);
	}

	@GetMapping("/patients/associations/{id}")
	public ResponseEntity<RestResponse> getPatientAssociations(@CurrentUser JwtUserDetails userDetails,
	                                                           @PathVariable(name = "id") Long patientId,
	                                                           @RequestParam(name = "providerId", required = false) Long providerId,
	                                                           @RequestParam(name = "caregiverId", required = false) Long caregiverId)
			throws RestApiException {
		return inboundServiceCall(new RestRequest(), service -> {
			PatientAssociationsVO associationsVO = associationDetailsService.getPatientAssociations(
					new PatientAssociationsRequestVO(userDetails, patientId, caregiverId, providerId));
			WSPatientAssociationDetailsResponse response = new WSPatientAssociationDetailsResponse();
			response.setPatient(mapperFacade.map(associationsVO.getPatient(), WSPatientDetails.class));
			response.setPatient(encryptUserDetails(response.getPatient(), userDetails,
			                                       Stream.of("firstName", "lastName", "mobileNumber", "emailId",
			                                                 "city",
			                                                 "country",
			                                                 "location", "state", "zipCode", "age")
			                                             .collect(Collectors.toList())));
			response.setCaregivers(associationsVO.getCaregivers()
			                                     .stream()
			                                     .map(vo -> vo.toWsCaregiver())
			                                     .collect(Collectors.toList()));
			response.setPharmacists(associationsVO.getPharmacists()
			                                      .stream()
			                                      .map(vo -> vo.toWsPharmacist())
			                                      .collect(Collectors.toList()));
			response.setProviders(associationsVO.getProviders()
			                                    .stream()
			                                    .map(vo -> vo.toWsProvider())
			                                    .collect(Collectors.toList()));
			return new ResponseEntity<>(response, HttpStatus.OK);
		});
	}

	@GetMapping("/patients/search")
	public ResponseEntity<RestResponse> searchPatients(@CurrentUser JwtUserDetails userDetails,
	                                                   @RequestParam(name = "offset", defaultValue = "0") Integer offset,
	                                                   @RequestParam(name = "limit", defaultValue = "10") Integer limit,
	                                                   @RequestParam(name = "query") String query,
	                                                   @RequestParam(name = "sortBy", defaultValue = "lastActiveTime") String sortBy,
	                                                   @RequestParam(name = "sortOrder", defaultValue = "desc") String sortOrder,
	                                                   @RequestParam(name = "providerId", required = false) Long providerId,
	                                                   @RequestParam(name = "caregiverId", required = false) Long caregiverId)
			throws RestApiException {
		return inboundServiceCall(new RestRequest(), service -> {
			                          PatientDetailsResponseVO responseVO = associationDetailsService.searchPatients(
					                          new LoadPatientRequestVO(userDetails, limit, offset, sortBy, sortOrder,
					                                                   false, providerId,
					                                                   caregiverId, "txt"), query);
			                          WSPatientDetailsResponse response = new WSPatientDetailsResponse();
			                          response.setTotalRecords(responseVO.getTotalRecords());
			                          response.setPatients(responseVO.getPatientDetails()
			                                                         .stream()
			                                                         .map(detail -> mapperFacade.map(detail,
			                                                                                         WSPatientDetails.class))
			                                                         .collect(Collectors.toList()));
			                          return new ResponseEntity<>(response, HttpStatus.OK);
		                          }, Stream.of("firstName", "lastName", "mobileNumber", "emailId", "city", "country",
		                                       "location", "state",
		                                       "zipCode", "age")
		                                   .collect(Collectors.toList()), null,
		                          WSPatientDetailsResponse.class, userDetails);
	}

}
