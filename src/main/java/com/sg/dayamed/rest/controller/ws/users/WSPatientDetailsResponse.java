package com.sg.dayamed.rest.controller.ws.users;

import com.sg.dayamed.rest.commons.RestResponse;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
public class WSPatientDetailsResponse extends RestResponse {

	private List<WSPatientDetails> patients;

	private Integer totalRecords;
}
