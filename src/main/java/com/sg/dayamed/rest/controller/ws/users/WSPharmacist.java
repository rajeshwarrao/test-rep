package com.sg.dayamed.rest.controller.ws.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPharmacist {

	private Long id;

	private String company;

	private WSUserDetails userDetails;
}
