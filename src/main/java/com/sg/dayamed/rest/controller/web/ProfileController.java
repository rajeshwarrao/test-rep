package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.component.AmazonClient;
import com.sg.dayamed.pojo.PasswordChangeReqModel;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSAWSUploadResponse;
import com.sg.dayamed.rest.controller.ws.users.WSPasswordChangeResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.ApplicationConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
public class ProfileController extends BaseRestApi {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	AmazonClient amazonClient;

	@PostMapping(value = "/rest/utility/changepwd", consumes = "application/json", produces = "application/json")
	public ResponseEntity<RestResponse> changePassword(@CurrentUser JwtUserDetails userDetails,
	                                                   @RequestBody @Valid PasswordChangeReqModel passwordChangeReqModel)
			throws RestApiException {
		return inboundServiceCall(passwordChangeReqModel, service -> {
			userDetailsService.changePassword(passwordChangeReqModel, userDetails.getUserId());
			return ResponseEntity.ok(buildChangePasswordResponseModel());
		});
	}

	@PostMapping("/profile/upload")
	public ResponseEntity<RestResponse> uploadFile(@RequestPart(value = "file", required = false) MultipartFile imageFile) throws RestApiException {
		return inboundServiceCall(new RestRequest(), service -> {
			String imageUrl = amazonClient.uploadFile(imageFile);
			WSAWSUploadResponse response = new WSAWSUploadResponse();
			response.setImageUrl(imageUrl);
			return ResponseEntity.ok(response);
		});
	}

	private WSPasswordChangeResponse buildChangePasswordResponseModel() {
		WSPasswordChangeResponse responseModel = new WSPasswordChangeResponse();
		responseModel.setStatus(ApplicationConstants.SUCCESS);
		responseModel.setMessage("Password changed successfully.");
		return responseModel;
	}

}
