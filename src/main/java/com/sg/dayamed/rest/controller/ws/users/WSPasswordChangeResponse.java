package com.sg.dayamed.rest.controller.ws.users;

import com.sg.dayamed.rest.commons.RestResponse;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WSPasswordChangeResponse extends RestResponse {

	private String status;

	private String message;
}
