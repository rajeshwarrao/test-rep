package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSUserDetailsResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsResponseVO;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@RestController(value = AdminController.RESOURCE_NAME + "_" + AdminController.API_VERSION)
@RequestMapping("/rest/" + AdminController.API_VERSION)
public class AdminController extends BaseRestApi {

	public static final String API_VERSION = "w";

	public static final String RESOURCE_NAME = "AdminController";

	@Autowired
	UserPreferencesVService userPreferencesVService;

	@Autowired
	UserValidator userValidator;

	@PreAuthorize("@roleCustomAuthorization.isAdmin(authentication)")
	@GetMapping("/users")
	public ResponseEntity<RestResponse> loadUsers(@CurrentUser JwtUserDetails userDetails,
	                                              @RequestParam(name = "offset", defaultValue = "0") Integer offset,
	                                              @RequestParam(name = "limit", defaultValue = "10") Integer limit,
	                                              @RequestParam(name = "sortBy", defaultValue = "lastActiveTime") String sortBy,
	                                              @RequestParam(name = "sortOrder", defaultValue = "desc") String sortOrder,
	                                              @RequestParam(name = "query", required = false) String query,
	                                              @RequestParam(name = "userType") String userType)
			throws RestApiException {
		return inboundServiceCall(new RestRequest(), service -> {
			LoadPatientRequestVO requestVO = new LoadPatientRequestVO();
			requestVO.setOffset(offset);
			requestVO.setLimit(limit);
			requestVO.setUserDetails(userDetails);
			requestVO.setSortByEnum(PatientSortByEnum.getPatientSortByEnum(sortBy));
			requestVO.setSortOrderEnum(SortOrderEnum.getSortTypeEnum(sortOrder));
			requestVO.setQuery(query);
			userValidator.validateUserRole(userType);
			UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(userType);
			UserDetailsResponseVO responseVO = userPreferencesVService.getUserPreferences(userRoleEnum, requestVO);
			WSUserDetailsResponse response = new WSUserDetailsResponse(responseVO);
			return new ResponseEntity<>(response, HttpStatus.OK);
		});
	}
}
