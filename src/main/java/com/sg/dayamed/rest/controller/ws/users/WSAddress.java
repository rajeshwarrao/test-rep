package com.sg.dayamed.rest.controller.ws.users;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WSAddress {

	private String location;

	private String city;

	private String state;

	private String country;

	private String zipCode;
}
