package com.sg.dayamed.rest.controller.ws.users;

import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.service.v1.vo.users.UserDetailsResponseVO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSUserDetailsResponse extends RestResponse {

	private Integer totalRecords;

	private List<WSProvider> providers = new ArrayList<>();

	private List<WSCaregiver> caregivers = new ArrayList<>();

	private List<WSPharmacist> pharmacists = new ArrayList<>();

	public WSUserDetailsResponse(UserDetailsResponseVO responseVO) {
		this.totalRecords = responseVO.getTotalRecords();
		if (CollectionUtils.isNotEmpty(responseVO.getProviders())) {
			this.providers = responseVO.getProviders()
			                           .stream()
			                           .map(provider -> provider.toWsProvider())
			                           .collect(Collectors.toList());
		}
		if (CollectionUtils.isNotEmpty(responseVO.getCaregivers())) {
			this.caregivers = responseVO.getCaregivers()
			                            .stream()
			                            .map(caregiver -> caregiver.toWsCaregiver())
			                            .collect(Collectors.toList());
		}
		if (CollectionUtils.isNotEmpty(responseVO.getPharmacists())) {
			this.pharmacists = responseVO.getPharmacists()
			                             .stream()
			                             .map(pha -> pha.toWsPharmacist())
			                             .collect(Collectors.toList());
		}
	}
}
