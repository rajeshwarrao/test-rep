package com.sg.dayamed.rest.controller.ws.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSCaregiver {

	private Long id;

	private Boolean adminFlag;

	private WSUserDetails userDetails;
}
