package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.util.TwilioVideoService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class TwilioVideoController extends BaseRestApi {

	@Autowired
	private TwilioVideoService twilioVideoService;

	@PostMapping(value = "/rest/utility/generatetwilioauthtoken", produces = "application/json", consumes =
			"application/json")
	@ApiOperation(value = "Fetch TwilioAuthToken", notes = "Returns TwilioAuthToken")
	public ResponseEntity<Object> fetchTwilioAuthToken(@CurrentUser JwtUserDetails userDetails)
			throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			final String userEmailId = userDetails.getUserEmailId();
			return ResponseEntity.ok(twilioVideoService.generateAuthToken(userEmailId));
		});
	}

}
