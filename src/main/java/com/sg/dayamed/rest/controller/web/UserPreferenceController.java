package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.UserPreferenceService;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserPreferenceController extends BaseRestApi {

	@Autowired
	private UserPreferenceService userPreferenceService;

	@GetMapping(value = "/rest/utility/getUserPreferences", produces = "application/json")
	@ApiOperation(value = "Get User Preference", notes = "Returns User Preference")
	public ResponseEntity<Object> getUserPreferences(@CurrentUser JwtUserDetails userDetails) throws RestApiException {
		return inboundServiceCallGeneric(userDetails.getUserId(), service -> {
			final UserPreferenceModel userSettings =
					userPreferenceService.findUserSettingsByUserId(userDetails.getUserId());
			return ResponseEntity.ok(userSettings);
		});
	}

	@PutMapping(value = "/rest/utility/updateUserPreferences", produces = "application/json")
	@ApiOperation(value = "Update User Preference", notes = "Returns updated user details data ")
	public ResponseEntity<Object> updateUserPreferences(@CurrentUser JwtUserDetails userDetails,
	                                                    @RequestBody UserPreferenceModel settingsUpdateRequest)
			throws RestApiException {
		return inboundServiceCallGeneric(settingsUpdateRequest, service -> {
			final UserPreferenceModel updatedSettings =
					userPreferenceService.updateUserPreference(userDetails.getUserId(), settingsUpdateRequest);
			return ResponseEntity.ok(updatedSettings);
		});
	}

}
