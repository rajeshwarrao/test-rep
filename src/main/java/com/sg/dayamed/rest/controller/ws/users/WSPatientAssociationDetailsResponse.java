package com.sg.dayamed.rest.controller.ws.users;

import com.sg.dayamed.rest.commons.RestResponse;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSPatientAssociationDetailsResponse extends RestResponse {

	private WSPatientDetails patient;

	private List<WSProvider> providers = new ArrayList<>();

	private List<WSCaregiver> caregivers = new ArrayList<>();

	private List<WSPharmacist> pharmacists = new ArrayList<>();

}
