package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetails;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetailsResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.v1.PharmacistService;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@RestController(value = PharmacistController.RESOURCE_NAME + "_" + PharmacistController.API_VERSION)
@RequestMapping("/rest/" + PharmacistController.API_VERSION)
public class PharmacistController extends BaseRestApi {

	public static final String API_VERSION = "w";

	public static final String RESOURCE_NAME = "PharmacistController";

	@Autowired
	@Qualifier("pharmacistServiceV1")
	PharmacistService pharmacistService;

	@Autowired
	MapperFacade mapperFacade;

	@GetMapping("/pharmacist/patients")
	public ResponseEntity<RestResponse> loadPatients(@CurrentUser JwtUserDetails jwtUser,
	                                                 @RequestParam(name = "offset", defaultValue = "0") Integer offset,
	                                                 @RequestParam(name = "limit", defaultValue = "10") Integer limit,
	                                                 @RequestParam(name = "sortBy", defaultValue = "lastActiveTime") String sortBy,
	                                                 @RequestParam(name = "sortOrder", defaultValue = "desc") String sortOrder,
	                                                 @RequestParam(name = "status", defaultValue = "New") String status,
	                                                 @RequestParam(name = "query", required = false) String query)
			throws RestApiException {
		return inboundServiceCall(new RestRequest(), service -> {
			                          LoadPatientRequestVO requestVO = new LoadPatientRequestVO();
			                          requestVO.setLimit(limit);
			                          requestVO.setOffset(offset);
			                          requestVO.setSortByEnum(PatientSortByEnum.getPatientSortByEnum(sortBy));
			                          requestVO.setSortOrderEnum(SortOrderEnum.getSortTypeEnum(sortOrder));
			                          requestVO.setUserDetails(jwtUser);
			                          requestVO.setQuery(query);
			                          PatientDetailsResponseVO responseVO =
					                          pharmacistService.getPatientsForGSEnabled(jwtUser, status, requestVO);
			                          WSPatientDetailsResponse response = new WSPatientDetailsResponse();
			                          response.setTotalRecords(responseVO.getTotalRecords());
			                          response.setPatients(responseVO.getPatientDetails()
			                                                         .stream()
			                                                         .map(detail -> mapperFacade.map(detail,
			                                                                                         WSPatientDetails.class))
			                                                         .collect(Collectors.toList()));
			                          return new ResponseEntity<>(response, HttpStatus.OK);
		                          }, Stream.of("firstName", "lastName", "mobileNumber", "emailId", "city", "country",
		                                       "location", "state",
		                                       "zipCode", "age")
		                                   .collect(Collectors.toList()), null,
		                          WSPatientDetailsResponse.class, jwtUser);
	}
}
