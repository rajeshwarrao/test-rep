package com.sg.dayamed.rest.commons;

import com.sg.dayamed.commons.rest.IServiceMethod;
import com.sg.dayamed.component.CacheDataComponent;
import com.sg.dayamed.dao.UserDetailsCacheVO;
import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetails;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.crypto.SecretKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

public class BaseRestApi {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	PHIDataEncryption phiDataEncryption;

	@Autowired
	CacheDataComponent cacheDataComponent;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	protected <I> ResponseEntity<RestResponse> inboundServiceCall(I request,
	                                                              IServiceMethod<I, ResponseEntity<RestResponse>> service)
			throws RestApiException {
		try {
			validateUserIntegrity();
			ResponseEntity<RestResponse> response = service.execute(request);
			return response;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	protected <I> ResponseEntity<Object> inboundServiceCallObjectResponse(I request,
	                                                                      IServiceMethod<I, ResponseEntity<Object>> service)
			throws RestApiException {
		try {
			validateUserIntegrity();
			ResponseEntity<Object> response = service.execute(request);
			return response;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	protected <I> ResponseEntity<Object> inboundServiceCallGeneric(I request,
	                                                               IServiceMethod<I, ResponseEntity<Object>> service)
			throws RestApiException {
		try {
			validateUserIntegrity();
			ResponseEntity<Object> response = service.execute(request);
			return response;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	protected <I> ResponseEntity<RestResponse> inboundServiceCall(I request,
	                                                              IServiceMethod<I, ResponseEntity<RestResponse>> service,
	                                                              List<String> encryptions, List<String> ids,
	                                                              Class<? extends RestResponse> clazz,
	                                                              JwtUserDetails userDetails) throws RestApiException {
		try {
			validateUserIntegrity();
			ResponseEntity<RestResponse> response = service.execute(request);
			encryptions = CollectionUtils.isEmpty(encryptions) ? new ArrayList<>() : encryptions;
			ids = CollectionUtils.isEmpty(ids) ? new ArrayList<>() : ids;
			String key = getKeyText(userDetails);
			SecretKey secretKey = phiDataEncryption.getSecretEncryptionKey(key);
			if (CollectionUtils.isNotEmpty(encryptions) || CollectionUtils.isNotEmpty(ids)) {
				String responseString = new DefaultInboundJsonSanitizer(objectMapper).doSanitization(
						objectMapper.writeValueAsString(response.getBody()), encryptions, ids, secretKey);
				RestResponse restResponse = objectMapper.readValue(responseString, clazz);
				return new ResponseEntity<>(restResponse, response.getStatusCode());
			}
			return response;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	private void validateUserIntegrity() throws DataValidationException {
		Authentication authentication = SecurityContextHolder.getContext()
		                                                     .getAuthentication();
		if (authentication != null && authentication.getPrincipal() instanceof JwtUserDetails) {
			JwtUserDetails userDetails = (JwtUserDetails) authentication.getPrincipal();
			UserDetailsCacheVO cacheVO = cacheDataComponent.validateUserDetails(userDetails.getUserId());
			if (cacheVO == null) {
				throw new DataValidationException(APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_USER_NO_LONGER_IN_SYSTEM);
			}
		}
	}

	protected WSPatientDetails encryptUserDetails(WSPatientDetails patientDetails, JwtUserDetails jwtUserDetails,
	                                              List<String> encryptionS) throws RestApiException {
		try {
			encryptionS = CollectionUtils.isEmpty(encryptionS) ? new ArrayList<>() : encryptionS;
			String key = getKeyText(jwtUserDetails);
			SecretKey secretKey = phiDataEncryption.getSecretEncryptionKey(key);
			if (CollectionUtils.isNotEmpty(encryptionS)) {
				String responseString = new DefaultInboundJsonSanitizer(objectMapper).doSanitization(
						objectMapper.writeValueAsString(patientDetails), encryptionS, Collections.emptyList(),
						secretKey);
				return objectMapper.readValue(responseString, WSPatientDetails.class);
			}
			return patientDetails;
		} catch (Exception e) {
			logError(e);
			throw processException(e);
		}
	}

	private String getKeyText(JwtUserDetails userDetails) {
		if (ApplicationConstants.IOS.equalsIgnoreCase(userDetails.getTypeOfDevice())
				|| ApplicationConstants.ANDROID.equalsIgnoreCase(userDetails.getTypeOfDevice())) {
			return userDetails.getDeviceId() + userDetails.getToken();
		} else {
			return userDetails.getToken();
		}
	}

	private void logError(Exception e) {
		logger.error("Error processing Rest Request => ", e);
	}

	protected RestApiException processException(Exception e) {
		logger.debug("At: processException()");
		RestApiException restApiException = null;
		Throwable rootCause = ExceptionLogUtil.getRootCause(e);
		if (e instanceof DataValidationException) {
			restApiException = new RestApiException((DataValidationException) e);
		} else if (rootCause instanceof DataValidationException) {
			restApiException = new RestApiException((DataValidationException) rootCause);
		} else if (e instanceof ApplicationException) {
			restApiException = new RestApiException(e.getMessage(), RestApiException.createApplicationExceptionFault(
					(ApplicationException) e));
		}

		if (restApiException == null) {
			restApiException = new RestApiException(e.getMessage(), RestApiException.createUnexpectedFault(e));
		}
		// restApiException.setMethod(method);
		return restApiException;
	}

	public class DefaultInboundJsonSanitizer {

		public DefaultInboundJsonSanitizer(ObjectMapper jsonMapper) {

		}

		protected String doSanitization(String json, List<String> encryptions, List<String> ids, SecretKey secretKey) {
			List<Pattern> cryptoProperties = encryptions.stream()
			                                            .map(value -> generatePattern(value))
			                                            .collect(
					                                            Collectors.toList()); // Get patterns which are fully
			// masked.
			List<Pattern> idCryptoProperties = ids.stream()
			                                      .map(value -> generatePattern(value))
			                                      .collect(Collectors.toList());
			Matcher matcher;
			for (Pattern pattern : cryptoProperties) {
				matcher = pattern.matcher(json);
				while (matcher.find()) {
					json = getEncryptedString(json, matcher, false, secretKey);
				}
			}
			for (Pattern pattern : idCryptoProperties) {
				matcher = pattern.matcher(json);
				while (matcher.find()) {
					json = getEncryptedString(json, matcher, true, secretKey);
				}
			}
			return json;
		}

		private String getEncryptedString(String json, Matcher matcher, Boolean idField, SecretKey secretKey) {
			String[] strings = matcher.group()
			                          .split(":");
			String data = StringUtils.substring(strings[1], 1, strings[1].length() - 1);
			if (idField) {
				//strings[1] = CryptoUtil.encryptIdField(data);
			} else {
				try {
					strings[1] = phiDataEncryption.encryptText(data, secretKey);
				} catch (Exception e) {
					//strings[1] = strings[1];
				}
			}
			String original = strings[0] + ":\"" + strings[1] + "\"";
			String group = matcher.group();
			json = StringUtils.replace(json, group, original);
			return json;
		}

		private Pattern generatePattern(String value) {
			return Pattern.compile("\"(" + value + ")\":\"((\\\\\"|[^\"])*)\"");
		}
	}
}