package com.sg.dayamed.rest.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Component
public class BaseRestOutboundProcessor {

	@Autowired
	RestTemplate restTemplate;

	public <T extends Object> ResponseEntity<T> post(String apiUrl, Object request, Class<T> responseClazz,
	                                                 Map<String, String> headers) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Content-Type", MediaType.APPLICATION_JSON_UTF8.getType());
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		if (headers != null && !headers.isEmpty()) {
			headers.forEach((key, value) ->
					                httpHeaders.set(key, value));
		}
		HttpEntity<Object> entity = new HttpEntity<>(request, httpHeaders);
		ResponseEntity<T> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, responseClazz);
		return responseEntity;
	}
}
