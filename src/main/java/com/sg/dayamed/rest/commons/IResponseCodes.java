package com.sg.dayamed.rest.commons;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

public interface IResponseCodes {

	String SUCCESSFUL = "Successful";
	String DATA_VALIDATION_ERROR = "DataValidationError";
	String UNEXPECTED_SYSTEM_ERROR = "UnexpectedSystemError";
	String SERVICE_UNAVAILABLE = "ServiceUnavailable";
	String AUTHENTICATION_FAILED = "AuthenticationFailed";
	String AUTHORIZATION_FAILED = "AuthorizationFailed";
	String ACCESS_DENIED_EXCEPTION = "AccessDeniedException";
	String CONFLICT = "Conflict";
	String BADREQUEST = "Badrequest";
	String PRECONDITION_FAILED = "PreConditionFailed";
	String SERVICE_CONFLICT = "StatusConflict";
	String GONE = "NotAvailableError";
	String NO_CONTENT ="NoContent";
}
