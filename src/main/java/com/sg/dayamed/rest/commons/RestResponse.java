package com.sg.dayamed.rest.commons;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

@Getter
@Setter
public class RestResponse {

	private String result = IResponseCodes.SUCCESSFUL;
}
