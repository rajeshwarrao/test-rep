package com.sg.dayamed.rest.commons;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

@FunctionalInterface
public interface IServiceMethod<I, O> {

	O execute(I request) throws Exception;
}
