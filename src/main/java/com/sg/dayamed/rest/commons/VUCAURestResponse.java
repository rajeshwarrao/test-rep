package com.sg.dayamed.rest.commons;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VUCAURestResponse extends RestResponse {

	private String status;

	private String vucaurl;

	private String message;
}
