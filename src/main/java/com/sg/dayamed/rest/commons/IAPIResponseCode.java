package com.sg.dayamed.rest.commons;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

public interface IAPIResponseCode {

	Integer SUCCESS = 0;
	Integer AUTHENTICATION_FAILED = 1;
	Integer PARAMETER_VALIDATION_FAILED = 2;
	Integer UNEXPECTED_SYSTEM_ERROR = 3;
	Integer FAILED = 4;
}
