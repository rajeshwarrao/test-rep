package com.sg.dayamed.rest.commons;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.UUID;

/**
 * Created by Eresh Gorantla on 12/Jun/2019
 **/

public class RestApiException extends Exception {

	private RestFault fault;

	private ServiceMethod method;

	public RestApiException() {
		super();
	}

	public RestApiException(DataValidationException dve) {
		this.fault = createFault(dve);
	}

	public RestApiException(String message, RestFault wsFault) {
		super(message);
		Validate.notNull(wsFault, "Fault cannot be null.");
		this.fault = wsFault;
	}

	public RestApiException(String message, Exception e) {
		super(message);
		this.fault = createUnexpectedFault(e);
	}

	public RestFault getFaultInfo() {
		return this.fault;
	}

	public static RestFault createApplicationExceptionFault(ApplicationException ae) {
		RestFault fault = new RestFault();
		fault.setErrorKey(StringUtils.isBlank(ae.getErrorKey()) ? IWSGlobalApiErrorKeys.ERROR_UNEXPECTED_EXCEPTION :
				                  ae.getErrorKey());
		fault.setErrorMessage(ae.getMessage());
		fault.setResult(ae.getResult());
		fault.setErrorParameters(ae.getErrorParams());
		return fault;
	}

	public static RestFault createUnexpectedFault(Exception ex) {
		RestFault fault = new RestFault();
		fault.setResult(IResponseCodes.UNEXPECTED_SYSTEM_ERROR);
		fault.setErrorKey(IWSGlobalApiErrorKeys.ERRORS_GENERAL_ERROR_UNEXPECTED);
		fault.setErrorMessage(ExceptionLogUtil.getRootCause(ex)
		                                      .getMessage());
		fault.setUid(UUID.randomUUID()
		                 .toString());

		return fault;
	}

	public static RestFault createNoPrivilegeFault() {
		RestFault fault = new RestFault();
		fault.setResult(IResponseCodes.AUTHORIZATION_FAILED);
		fault.setErrorKey(IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
		fault.setUid(UUID.randomUUID()
		                 .toString());
		return fault;
	}

	private RestFault createFault(DataValidationException e) {
		RestFault fault = new RestFault();
		fault.setFieldName(e.getFieldName());
		fault.setErrorKey(e.getErrorKey());
		fault.setErrorMessage(e.getMessage());
		fault.setResult(IResponseCodes.DATA_VALIDATION_ERROR);
		fault.setErrorParameters(e.getErrorKeyParameters());
		return fault;
	}

	public ServiceMethod getMethod() {
		return method;
	}

	public void setMethod(ServiceMethod method) {
		this.method = method;
	}
}
