package com.sg.dayamed.rest.commons;

import com.sg.dayamed.exceptions.IGlobalErrorKeys;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.service.util.APIErrorKeys;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eresh Gorantla on 18/May/2019
 **/

public class ErrorKeyToResponseStatusMapper {

	private static final Map<String, ResponseStatus> MAPPER = new HashMap<String, ResponseStatus>();

	private ErrorKeyToResponseStatusMapper() {
		// Prevent instantiation.
	}

	static {
		// 404:Not Found error keys.
		MAPPER.put(IWSGlobalApiErrorKeys.ERRORS_PUBLIC_RECORD_NOT_FOUND, ResponseStatus.NOT_FOUND);

		MAPPER.put(APIErrorKeys.ERROR_LOGIN_USERNAME_NOT_FOUND, ResponseStatus.NOT_FOUND);

		// 401:Unauthorized error keys.
		MAPPER.put(IWSGlobalApiErrorKeys.ERRORS_AUTHENTICATION_FAILED, ResponseStatus.UNAUTHENTICATED);

		// 500:Internal Server Error keys.
		MAPPER.put(IWSGlobalApiErrorKeys.ERRORS_GENERAL_ERROR_UNEXPECTED, ResponseStatus.INTERNAL_SERVER_ERROR);

		// 503: Service Unavailable
		MAPPER.put(IWSGlobalApiErrorKeys.ERRORS_GENERAL_SERVICE_UNAVAILABLE, ResponseStatus.SERVICE_UNAVAILABLE);

		// 401:Unauthorized error keys.
		MAPPER.put(IWSGlobalApiErrorKeys.ERRORS_AUTHORIZATION_FAILED, ResponseStatus.UNAUTHORIZED);

		MAPPER.put(IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES, ResponseStatus.UNAUTHORIZED);

		MAPPER.put(APIErrorKeys.ERROR_LOGIN_CREDENTIALS_INCORRECT, ResponseStatus.UNAUTHORIZED);

		MAPPER.put(APIErrorKeys.ERROR_EMAIL_ALREADY_EXISTED, ResponseStatus.SERVICE_CONFLICT);

		MAPPER.put(APIErrorKeys.ERROR_INVALID_DATA, ResponseStatus.BAD_REQUEST);

		MAPPER.put(APIErrorKeys.ERROR_EMAIL_CONFLICT, ResponseStatus.CONFLICT);

		MAPPER.put(APIErrorKeys.ERROR_PROVIDER_ASSOCIATED_WITH_PATIENTS, ResponseStatus.PRECONDITION_FAILED);

		MAPPER.put(APIErrorKeys.ERROR_PHARMACIST_ASSOCIATED, ResponseStatus.PRECONDITION_FAILED);

		MAPPER.put(APIErrorKeys.ERROR_USER_NOT_FOUND, ResponseStatus.NOT_FOUND);

		MAPPER.put(IGlobalErrorKeys.ERRORS_USER_NO_LONGER_IN_SYSTEM, ResponseStatus.GONE);
		
		MAPPER.put(APIErrorKeys.ERROR_PATIENT_EMAIL_ALREADY_MAPPED, ResponseStatus.NO_CONTENT);
		MAPPER.put(APIErrorKeys.ERROR_EMAIL_NOT_EXIST, ResponseStatus.NO_CONTENT);
	}

	/**
	 * Returns the response status for the passed error key. If no specific
	 * response status is found, returns 400 status code with
	 * DatavalidationError result. if error key is blank or null, returns 500
	 * status code with UnexpectedSystemError result.
	 *
	 * @param errorKey
	 * @return
	 */
	public static ResponseStatus getResponseStatus(String errorKey) {
		if (StringUtils.isBlank(errorKey)) { // If errorKey is blank, return
			// 500: Internal Server Error.
			return ResponseStatus.INTERNAL_SERVER_ERROR;
		}
		return MAPPER.get(errorKey) != null ? MAPPER.get(errorKey) : ResponseStatus.BAD_REQUEST;
	}

	public static final class ResponseStatus {

		public static final ResponseStatus INTERNAL_SERVER_ERROR = new ResponseStatus(
				HttpStatus.SC_INTERNAL_SERVER_ERROR, IResponseCodes.UNEXPECTED_SYSTEM_ERROR);

		public static final ResponseStatus NOT_FOUND = new ResponseStatus(HttpStatus.SC_NOT_FOUND,
		                                                                  IResponseCodes.DATA_VALIDATION_ERROR);

		public static final ResponseStatus BAD_REQUEST = new ResponseStatus(HttpStatus.SC_BAD_REQUEST,
		                                                                    IResponseCodes.DATA_VALIDATION_ERROR);

		public static final ResponseStatus UNAUTHENTICATED = new ResponseStatus(HttpStatus.SC_UNAUTHORIZED,
		                                                                        IResponseCodes.AUTHENTICATION_FAILED);

		public static final ResponseStatus UNAUTHORIZED = new ResponseStatus(HttpStatus.SC_FORBIDDEN,
		                                                                     IResponseCodes.AUTHORIZATION_FAILED);

		public static final ResponseStatus SERVICE_UNAVAILABLE = new ResponseStatus(HttpStatus.SC_SERVICE_UNAVAILABLE,
		                                                                            IResponseCodes.SERVICE_UNAVAILABLE);

		public static final ResponseStatus SERVICE_CONFLICT = new ResponseStatus(HttpStatus.SC_CONFLICT,
		                                                                         IResponseCodes.SERVICE_CONFLICT);

		public static final ResponseStatus CONFLICT = new ResponseStatus(HttpStatus.SC_CONFLICT,
		                                                                 IResponseCodes.CONFLICT);

		public static final ResponseStatus PRECONDITION_FAILED = new ResponseStatus(HttpStatus.SC_PRECONDITION_FAILED,
		                                                                            IResponseCodes.PRECONDITION_FAILED);

		public static final ResponseStatus GONE = new ResponseStatus(HttpStatus.SC_GONE, IResponseCodes.GONE);
		
		public static final ResponseStatus NO_CONTENT = new ResponseStatus(HttpStatus.SC_NO_CONTENT,
                IResponseCodes.NO_CONTENT);

		private int status;

		private String result;

		ResponseStatus(int status, String result) {
			this.status = status;
			this.result = result;
		}

		public int getStatus() {
			return status;
		}

		public String getResult() {
			return result;
		}
	}
}