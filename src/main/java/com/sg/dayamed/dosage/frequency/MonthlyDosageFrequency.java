package com.sg.dayamed.dosage.frequency;

import lombok.NonNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class MonthlyDosageFrequency implements FrequencyFactory {

	private static final Logger logger = LoggerFactory.getLogger(MonthlyDosageFrequency.class);

	public List<String> frequencyCalculation(@NonNull String freqencyStr, List<String> timeList)
			throws JSONException, ParseException {
		List<String> listOfAlarms = null;
		JSONObject freqJson = new JSONObject(freqencyStr);
		int dom = freqJson.has("value") ? freqJson.getInt("value") : 1;
		if (dom != 0) {
			String startDateStr = freqJson.optString("startDate");
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date startDateObj = !StringUtils.isEmpty(startDateStr) ? formatter.parse(startDateStr) : new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDateObj);
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), dom, 0, 0, 0);
			Date prsDate = calendar.getTime();
			// if prsdate 06 April 2019 and start date is 10th April
			// 2019 then add 1 month.
			if (prsDate.before(startDateObj)) {
				Calendar prsCal = Calendar.getInstance();
				prsCal.setTime(prsDate);
				prsCal.add(Calendar.MONTH, +1);
				prsDate = prsCal.getTime();
			}
			listOfAlarms = new ArrayList();
			String patientTimeZone = freqJson.optString("patientTimeZone");
			boolean isPastDatesEnables = freqJson.optBoolean("pastDatesEnable");
			int endAfterOccurrence = Integer
					.parseInt(!StringUtils.isEmpty(freqJson.optString("endAfterOccurrences"))
							          ? freqJson.optString("endAfterOccurrences") : "1");
			int noOfOccurrence = Integer.parseInt(!StringUtils.isEmpty(freqJson.optString("recurrence"))
					                                      ? freqJson.optString("recurrence") : "1");
			Calendar prsCalAddFreq = Calendar.getInstance();
			for (int i = 0; i < endAfterOccurrence; i++) {
				if (i == 0) {
					addTimeToDateObj(listOfAlarms, prsDate, timeList, patientTimeZone, isPastDatesEnables);
				} else {
					prsCalAddFreq.setTime(prsDate);
					prsCalAddFreq.add(Calendar.MONTH, +noOfOccurrence);
					prsDate = prsCalAddFreq.getTime();
					addTimeToDateObj(listOfAlarms, prsDate, timeList, patientTimeZone, isPastDatesEnables);
				}
			}
		} else {
			logger.error("Invlid JSON no DOM");
		}
		return listOfAlarms;
	}

	public boolean addTimeToDateObj(List<String> listOfAlarms, Date prsDate, List<String> timeList,
	                                String patientTimeZone, boolean isPastDatesEnables) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat sdfConvert24hours = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
		if (timeList != null) {
			for (String timeString : timeList) {
				String timeStr = timeString.split(" ")[0];
				Calendar prsCalAddTime = Calendar.getInstance();
				prsCalAddTime.setTime(prsDate);
				prsCalAddTime.set(prsCalAddTime.get(Calendar.YEAR), prsCalAddTime.get(Calendar.MONTH),
				                  prsCalAddTime.get(Calendar.DATE), Integer.parseInt(timeStr.split(":")[0]),
				                  Integer.parseInt(timeStr.split(":")[1]));
				String dateObjstr = formatter.format(prsCalAddTime.getTime());
				dateObjstr = dateObjstr + " " + timeString.split(" ")[1];
				Date presDate = sdfConvert24hours.parse(dateObjstr);
				Date currentDate = new Date();
				if (!StringUtils.isEmpty(patientTimeZone)) {
					LocalDateTime localTime = LocalDateTime.now(Clock.system(ZoneId.of(patientTimeZone)));
					long milliSeconds = Timestamp.valueOf(localTime)
					                             .getTime();
					currentDate = new Date(milliSeconds);
				}
				if (isPastDatesEnables || currentDate.before(presDate)) {
					listOfAlarms.add(outputformat.format(presDate));
				}
			}
		} else {
			Date currentDate = new Date();
			if (isPastDatesEnables || currentDate.before(prsDate)) {
				listOfAlarms.add(outputformat.format(prsDate));
			}
		}
		return false;
	}

}
