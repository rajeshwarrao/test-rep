package com.sg.dayamed.dosage.frequency;

import org.json.JSONException;

import java.text.ParseException;
import java.util.List;

public interface FrequencyFactory {

	public List<String> frequencyCalculation(String freqencyStr, List<String> timeList)
			throws JSONException, ParseException;

}
