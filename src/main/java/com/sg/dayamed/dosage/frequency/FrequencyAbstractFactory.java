package com.sg.dayamed.dosage.frequency;

import com.sg.dayamed.util.ApplicationConstants;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service("FrequencyAbstractFactory")
public class FrequencyAbstractFactory implements ApplicationContextAware {

	private ApplicationContext context;

	public FrequencyFactory getFrequencyFactory(String type) {
		FrequencyFactory frequencyFactory = null;
		if (ApplicationConstants.DOSAGE_FREQUENCY_MONTHLY.equalsIgnoreCase(type)) {
			frequencyFactory = this.context.getBean(MonthlyDosageFrequency.class);
		} else if (ApplicationConstants.DOSAGE_FREQUENCY_DAILY.equalsIgnoreCase(type)) {
			frequencyFactory = this.context.getBean(DailyDosageFrequency.class);
		} else if (ApplicationConstants.DOSAGE_FREQUENCY_WEEKLY.equalsIgnoreCase(type)) {
			frequencyFactory = this.context.getBean(WeeklyDosageFrequency.class);
		}
		return frequencyFactory;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;

	}
}
