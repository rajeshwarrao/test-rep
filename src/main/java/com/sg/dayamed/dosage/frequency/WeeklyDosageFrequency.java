package com.sg.dayamed.dosage.frequency;

import com.sg.dayamed.util.DateUtility;

import lombok.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@Component
public class WeeklyDosageFrequency implements FrequencyFactory {

	@Override
	public List<String> frequencyCalculation(@NonNull String freqencyStr, List<String> timeList) throws JSONException {
		List<String> listOfAlarms = new ArrayList<String>();
		JSONObject freqJson = new JSONObject(freqencyStr);
		JSONArray jsonArray = freqJson.getJSONArray("value");
		int endAfterOccurrence = Integer.parseInt(freqJson.optString("endAfterOccurrences"));
		int recurrence = Integer.parseInt(freqJson.optString("recurrence"));
		String dosageStartDate = freqJson.optString("startDate");
		String patientTimeZone = freqJson.optString("patientTimeZone");
		boolean isPastDatesEnables = freqJson.optBoolean("pastDatesEnable");
		//logger.info("jsonArray obj "+jsonArray + " endAfterOccurrence "+endAfterOccurrence+ " recurrence
		// "+recurrence+ " patientTimeZone "+patientTimeZone);
		long recurrenceWeeklyMultiplier = 7l * recurrence; // after how many days we need to find the day
		for (int i = 0; i < jsonArray.length(); i++) {
			DayOfWeek dayOfWeekObj = getMappedDayOfWeek(jsonArray.getString(i));
			for (int timeListI = 0; timeListI < timeList.size(); timeListI++) {
				//String dosageTimeStr = timeList.get(timeListI).replaceAll("\\s", "").toUpperCase();
				LocalDate matchingDayDate = findFirstMatchingDay(dayOfWeekObj, dosageStartDate);
				DateUtility dateUtility = new DateUtility();
				LocalDateTime dateTimeObj =
						dateUtility.addTimestringToLacalDate(timeList.get(timeListI), matchingDayDate);
				listOfAlarms =
						addRecurrenceAlarms(dateTimeObj, recurrenceWeeklyMultiplier, endAfterOccurrence, listOfAlarms);
			}
		}
		List<String> filteredAlarms = filterAlarmSchd(patientTimeZone, listOfAlarms, isPastDatesEnables);
		return filteredAlarms;
	}

	private List<String> filterAlarmSchd(String patientTimeZone, List<String> alarmSchd, boolean isPastDatesEnables) {
		List<String> filterAlarmSchd = new ArrayList<String>();
		LocalDateTime patientDateTimeNow = LocalDateTime.now(Clock.system(ZoneId.of(patientTimeZone)));
		for (String filterList : alarmSchd) {
			LocalDateTime dosageTime = LocalDateTime.parse(filterList, DateTimeFormatter.ISO_DATE_TIME);
			if (isPastDatesEnables || dosageTime.isAfter(patientDateTimeNow)) {
				String formatByRemovingT = filterList.replace("T", " ");
				filterAlarmSchd.add(formatByRemovingT);
			}
		}

		return filterAlarmSchd;

	}

	private List<String> addRecurrenceAlarms(LocalDateTime currentDosageDate, long frequencyOfOccurrence,
	                                         int endAfterOccurrence, List<String> alaramsDateTimeList) {
		alaramsDateTimeList.add(currentDosageDate.toString());// first record of the matchignDay
		// Keep adding until the occurance end
		for (int i = 2; i <= endAfterOccurrence; i++) {
			currentDosageDate = currentDosageDate.plusDays(frequencyOfOccurrence);
			String matchingDayWithTime = currentDosageDate.toString();
			alaramsDateTimeList.add(matchingDayWithTime);
		}

		return alaramsDateTimeList;

	}

	private LocalDate findFirstMatchingDay(DayOfWeek dayOfWeek, String dosageStartDateStr) {

		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDate dosageStartDate = LocalDate.parse(dosageStartDateStr, formatters);
		LocalDate firstMatchingDay = dosageStartDate.with(TemporalAdjusters.nextOrSame((dayOfWeek)));
		return firstMatchingDay;
	}

	private DayOfWeek getMappedDayOfWeek(String weekDayName) {

		if ("MON".equals(weekDayName)) {
			return DayOfWeek.MONDAY;
		} else if ("TUE".equals(weekDayName)) {
			return DayOfWeek.TUESDAY;
		} else if ("WED".equals(weekDayName)) {
			return DayOfWeek.WEDNESDAY;
		} else if ("THU".equals(weekDayName)) {
			return DayOfWeek.THURSDAY;
		} else if ("FRI".equals(weekDayName)) {
			return DayOfWeek.FRIDAY;
		} else if ("SAT".equals(weekDayName)) {
			return DayOfWeek.SATURDAY;
		} else {
			return DayOfWeek.SUNDAY;
		}
	}

}
