package com.sg.dayamed.dosage.frequency;

import com.sg.dayamed.util.DateUtility;

import lombok.NonNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DailyDosageFrequency implements FrequencyFactory {

	// check if same date condition
	public List<String> frequencyCalculation(@NonNull String frequencyStr, List<String> timeList)
			throws JSONException, ParseException {
		//String zoneid= "Asia/Kolkata";
		//logger.info("daily frequency calc started");
		List<String> listOfAlarms = null;
		DateUtility dateUtility = new DateUtility();
		JSONObject freqJson = new JSONObject(frequencyStr);

		int durationLength = Integer.parseInt(freqJson.optString("value"));
		String zoneid = freqJson.optString("patientTimeZone");
		boolean isPastDatesEnables = freqJson.optBoolean("pastDatesEnable");

		//startDateObj expecting as UTC time
		String startDateStr = freqJson.getString("startDate");
		//String startDateStr = "04/08/2019";
		SimpleDateFormat simpleDateFormater = new SimpleDateFormat("MM/dd/yyyy");
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		//current UTC time
		LocalDateTime currentUTCLocalDateTime = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
		Date uTCCurrentDateObj = null;
		//startDateStr coming from DosageInfo
		if (!StringUtils.isEmpty(startDateStr)) {
			uTCCurrentDateObj = simpleDateFormater.parse(startDateStr);
		} else {
			//else date obj have to check
			uTCCurrentDateObj = dateUtility.convertLocalDateTimeToDateObject(currentUTCLocalDateTime);
		}
		//date - string , String to Localdate
		LocalDate startUTCLocalDate = LocalDate.parse(simpleDateFormater.format(uTCCurrentDateObj), dateTimeFormatter);
		//startLocalDateTime in utc
		listOfAlarms = new ArrayList<String>();
		for (int i = 0; i < durationLength; i++) {
			LocalDate durationLocalDate = dateUtility.addDaysToLocalDate(startUTCLocalDate, i);
			for (String time : timeList) {
				// insted of creating two colmns make it as one
				LocalDateTime durationLocalDateTime = dateUtility.addTimestringToLacalDate(time,
				                                                                           durationLocalDate);
				String strDurationDatewithTimeUTC = dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
						dateUtility.getFormattedLocalDateTime(durationLocalDateTime), zoneid);
				LocalDateTime durationLocalDateTimeUTC =
						dateUtility.convertUTCLocalDateTimeStringToObject(strDurationDatewithTimeUTC);
				//utcDurationtime have to cmapare with Current UTC
				if (!isPastDatesEnables &&
						durationLocalDateTimeUTC.isBefore(LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC))) {
					//durationLocalDateTimeUTC = durationLocalDateTimeUTC.plusDays(durationLength);
				} else {
					ZonedDateTime zdtATUTC = durationLocalDateTimeUTC.atZone(ZoneId.of("UTC"));
					ZonedDateTime zdtAtIST = zdtATUTC
							.withZoneSameInstant(ZoneId.of(zoneid));
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
					String formattedString = zdtAtIST.toLocalDateTime()
					                                 .format(formatter);
					listOfAlarms.add(formattedString);
				}
			}
		}
		return listOfAlarms;
	}
}

