package com.sg.dayamed.repository;

import com.sg.dayamed.entity.ConsumerGoods;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsumerGoodsRepository extends JpaRepository<ConsumerGoods, Long> {

	ConsumerGoods findByName(String name);

	List<ConsumerGoods> findByType(String type);
}
