package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Caregiver;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Long> {

	Caregiver findByUserDetails_id(long userid);

	List<Caregiver> findByAdminflag(boolean True);

	List<Caregiver> findByProviders_id(long providerID);

	List<Caregiver> findByPatients_id(long patientId);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM patient_caregiver_map WHERE caregiver_id =?1", nativeQuery = true)
	void deletepatientsBycaregiverId(long caregiverId);
}
