package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Medicine;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicineRepository extends JpaRepository<Medicine, Long> {

	Medicine findByName(String name);

	List<Medicine> findByNdcCodeIn(List<Long> ndcCodes);

	Page<Medicine> findByNameContainingOrderByName(String name, Pageable pageable);
}
