package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Adherence_DosageInfoMap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Adherence_DosageInfoMapRepository extends JpaRepository<Adherence_DosageInfoMap, Long> {

}
