package com.sg.dayamed.repository.view;

import com.sg.dayamed.entity.view.PrescriptionInfoV;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/

public interface PrescriptionInfoVRepository extends JpaRepository<PrescriptionInfoV, String> {

	List<PrescriptionInfoV> findByStatusIgnoreCaseAndPharmacistIdAndPatientIdIn(String status, Long pharmacistId,
	                                                                            List<Long> patientIds);
}
