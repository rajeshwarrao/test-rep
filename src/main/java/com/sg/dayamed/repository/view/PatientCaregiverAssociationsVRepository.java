package com.sg.dayamed.repository.view;

import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created By Gorantla, Eresh on 24/Jun/2019
 **/

public interface PatientCaregiverAssociationsVRepository extends JpaRepository<PatientCaregiverAssociationsV, String>,
                                                                 JpaSpecificationExecutor<PatientCaregiverAssociationsV> {

}
