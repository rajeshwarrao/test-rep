package com.sg.dayamed.repository.view;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
public interface PatientAssociationDetailsVRepository extends JpaRepository<PatientAssociationDetailsV, String>,
                                                              JpaSpecificationExecutor<PatientAssociationDetailsV> {

	List<PatientAssociationDetailsV> findByPatientId(Long patientId);
}
