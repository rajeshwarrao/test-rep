package com.sg.dayamed.repository.view;

import com.sg.dayamed.entity.view.PatientPharmacistAssociationsV;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created By Gorantla, Eresh on 24/Jun/2019
 **/
public interface PatientPharmacistAssociationsVRepository extends JpaRepository<PatientPharmacistAssociationsV, String>,
                                                                  JpaSpecificationExecutor<PatientPharmacistAssociationsV> {

}
