package com.sg.dayamed.repository.view;

import com.sg.dayamed.entity.view.UserPreferencesV;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
public interface UserPreferencesVRepository
		extends JpaRepository<UserPreferencesV, String>, JpaSpecificationExecutor<UserPreferencesV> {

	List<UserPreferencesV> findByUserIdIn(List<Long> userIds);
}
