package com.sg.dayamed.repository;

import com.sg.dayamed.entity.ConsumptionTemplate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsumptionFrequencyRepository extends JpaRepository<ConsumptionTemplate, Long> {

////	@Transactional
//	@Query("select distinct cf.frequency_id, cf.frequency_name from FristConsumptionFrequency cf")
//	List<Object> findAllDistinctData();

	List<ConsumptionTemplate> findByfrequencyId(long frequencyId);

	ConsumptionTemplate findByconsumptionsKey(String consumptionsKey);
}
