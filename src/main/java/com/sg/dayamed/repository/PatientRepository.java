package com.sg.dayamed.repository;

import com.sg.dayamed.dao.EhCacheConstants;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Long> {

	List<Patient> findByProviders_id(long providerid);

	List<Patient> findByProviders_idAndStatus(long providerid, int status);

	List<Prescription> findPrescriptionsByCaregivers_id(long caregiverid);

	List<Patient> findByCaregivers_id(long caregiverid);

	List<Patient> findByPharmacists_id(long pharmacistid);

	List<Patient> findByPharmacists_idAndStatus(long pharmacistid, int status);

	Patient findByUserDetails_id(long userid);

	Patient findByDeviceId(String deviceId);
	//List<Patient> findByProvider_UserDetails_emailId(String emailId);

	@Query("SELECT  NEW com.sg.dayamed.helper.pojo.PatientAdherencePojo(p.currentAdherence , p.predictedAdherence ,p" +
			       ".projectedAdherence ,p.emoji) FROM Patient p where p.id = :id")
	PatientAdherencePojo findAdherenceById(@Param("id") Long id);

	List<Provider> findProvidersById(long patientId);

	@Transactional
	@Procedure(procedureName = "delete_patient")
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	String deletePatient(@Param("patinetId") long patinetId);
}
