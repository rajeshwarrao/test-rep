package com.sg.dayamed.repository.impl;

import com.sg.dayamed.repository.MyEntityRepositoryCustom;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

@Repository
public class MyEntityRepositoryImpl implements MyEntityRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	private static final Logger logger = LoggerFactory.getLogger(MyEntityRepositoryImpl.class);

	public List<Object> findGSNotifications(String queryStr) {
		List<Object> list = em.createNativeQuery(queryStr)
		                      .getResultList();
		List<Object[]> listObjeArruy = em.createNativeQuery(queryStr)
		                                 .getResultList();
		HashMap<String, String> map = new HashMap();
		for (Object[] result : listObjeArruy) {
			map.put(result[0].toString(), result[1].toString());
		}
		Set set = em.createNativeQuery(queryStr)
		            .getParameters();
		return list;
	}

	@Transactional
	public List<Map<String, String>> getColmNameAndColumValueAsMap(String query) {
		List<Map<String, String>> listMap = new ArrayList<>();
		try {
			//logger.info("==getColmNameAndColumValueAsMap==query==" + query);
			final Session session = em.unwrap(Session.class);
			final AtomicReference<ResultSetMetaData> msRef = new AtomicReference<>();
			session.doWork((c) -> {
				try (PreparedStatement statement = c.prepareStatement(query)) {
					msRef.set(statement.getMetaData());
					ResultSet rs = statement.executeQuery();
					final ResultSetMetaData metaData = msRef.get();
					Map<String, String> columnNameToColumnIndex = null;
					while (rs.next()) {
						columnNameToColumnIndex = new LinkedHashMap();
						for (int t = 1; t <= metaData.getColumnCount(); t++) {
							String columName = metaData.getColumnLabel(t);
							String columValue = rs.getString(columName);
							columnNameToColumnIndex.put(columName, columValue);
						}
						listMap.add(columnNameToColumnIndex);
					}
				}
			});
		} catch (Exception e) {
			logger.error("Exception occurred in the method getColmNameAndColumValueAsMap", e);
		}
		return listMap;
	}

	@Transactional
	public List<Map<String, String>> getColmNameAndColumValueAsMapWithInParams(String query,
	                                                                           Map<Integer, Object> indexMap) {
		List<Map<String, String>> listMap = new ArrayList<>();
		try {
			//logger.info("==getColmNameAndColumValueAsMap==query==" + query);
			final Session session = em.unwrap(Session.class);
			final AtomicReference<ResultSetMetaData> msRef = new AtomicReference<>();
			session.doWork((c) -> {
				try (PreparedStatement statement = c.prepareStatement(query)) {
					indexMap.forEach((k, v) -> {
						try {
							statement.setObject(k, v);
						} catch (Exception ex) {
							logger.error("Exception occurred in the method getColmNameAndColumValueAsMapWithInParams",
							             ex);
						}
					});
					msRef.set(statement.getMetaData());
					ResultSet rs = statement.executeQuery();
					final ResultSetMetaData metaData = msRef.get();
					Map<String, String> columnNameToColumnIndex = null;
					while (rs.next()) {
						columnNameToColumnIndex = new LinkedHashMap();
						for (int t = 1; t <= metaData.getColumnCount(); t++) {
							String columName = metaData.getColumnLabel(t);
							String columValue = rs.getString(columName);
							columnNameToColumnIndex.put(columName, columValue);
						}
						listMap.add(columnNameToColumnIndex);
					}
				}
			});
		} catch (Exception e) {
			logger.error("Exception occurred in the method getColmNameAndColumValueAsMap", e);
		}
		return listMap;
	}

	@Transactional
	public int updateQueryPrepareStamt(String updateQry, Map<Integer, Object> indexMap) {
		try {
			int updateStatus = 0;
			final Session session = em.unwrap(Session.class); // ATTENTION! This
			// is
			// Hibernate-specific!
			session.doWork((connection) -> {
				try (PreparedStatement statement = connection.prepareStatement(updateQry)) {
					if (indexMap != null && indexMap.size() > 0) {
						indexMap.forEach((k, v) -> {
							try {
								statement.setObject(k, v);
							} catch (Exception ex) {
							}
						});
					}
					// statement.setInt(1, gsNotificationStatus);
					// statement.setInt(2, adharenceId);
					statement.executeUpdate();
				}
			});
		} catch (Exception e) {
			logger.error("Exception occurred in the method updateQueryPrepareStamt", e);
		}
		return 0;
	}
}
