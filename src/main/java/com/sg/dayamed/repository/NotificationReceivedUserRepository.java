package com.sg.dayamed.repository;

import com.sg.dayamed.entity.NotificationReceivedUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationReceivedUserRepository extends JpaRepository<NotificationReceivedUser, Long> {

	List<NotificationReceivedUser> findByuserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc(
			long userid, String type, boolean False);

	//fetching one notification which is lessthan 60 sec .
	//NotificationReceivedUser
	// findTop1Notification_timeBetweenAnduserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc
	// (Date start,Date end,long userid,String type,boolean False);
	NotificationReceivedUser findTop1ByuserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc(
			long userid, String type, boolean False);
}
