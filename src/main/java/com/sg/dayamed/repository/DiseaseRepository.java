package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Disease;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiseaseRepository extends CrudRepository<Disease, Long> {

	Disease findByName(String name);
}
