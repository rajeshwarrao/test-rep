package com.sg.dayamed.repository;

import com.sg.dayamed.entity.UserPreference;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserPreferenceRepository extends JpaRepository<UserPreference, Long> {

	UserPreference findByUserDetailsId(long id);

	List<UserPreference> findByUserDetailsIdIn(List<Long> id);

}
