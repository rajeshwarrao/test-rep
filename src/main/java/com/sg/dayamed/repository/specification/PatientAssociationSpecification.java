package com.sg.dayamed.repository.specification;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/

public class PatientAssociationSpecification implements Specification<PatientAssociationDetailsV> {

	private UserRoleEnum roleEnum;

	private PatientAssociationsRequestVO requestVO;

	public PatientAssociationSpecification(UserRoleEnum userRoleEnum, PatientAssociationsRequestVO requestVO) {
		this.roleEnum = userRoleEnum;
		this.requestVO = requestVO;
	}

	@Override
	public Predicate toPredicate(Root<PatientAssociationDetailsV> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		JwtUserDetails userDetails = requestVO.getUserDetails();
		if (UserRoleEnum.PROVIDER.equals(roleEnum)) {
			predicates.add(cb.and(cb.equal(root.get("providerUserId"), userDetails.getUserId()),
			                      cb.equal(root.get("patientStatus"), NumberUtils.INTEGER_ZERO),
			                      cb.equal(root.get("patientId"), requestVO.getPatientId())));
		} else if (UserRoleEnum.CAREGIVER.equals(roleEnum)) {
			predicates.add(cb.and(cb.equal(root.get("caregiverUserId"), userDetails.getUserId()),
			                      cb.equal(root.get("patientStatus"), NumberUtils.INTEGER_ZERO),
			                      cb.equal(root.get("patientId"), requestVO.getPatientId())));
		} else if (UserRoleEnum.PHARMACIST.equals(roleEnum)) {
			predicates.add(cb.and(cb.equal(root.get("pharmacistUserId"), userDetails.getUserId()),
			                      cb.equal(root.get("patientStatus"), NumberUtils.INTEGER_ZERO),
			                      cb.equal(root.get("patientId"), requestVO.getPatientId())));
		} else if (UserRoleEnum.ADMIN.equals(roleEnum)) {
			if (requestVO.getProviderId() != null) {
				predicates.add(cb.and(cb.equal(root.get("providerUserId"), requestVO.getProviderId()),
				                      cb.equal(root.get("patientStatus"), NumberUtils.INTEGER_ZERO),
				                      cb.equal(root.get("patientId"), requestVO.getPatientId())));
			} else if (requestVO.getCaregiverId() != null) {
				predicates.add(cb.and(cb.equal(root.get("caregiverUserId"), requestVO.getCaregiverId()),
				                      cb.equal(root.get("patientStatus"), NumberUtils.INTEGER_ZERO),
				                      cb.equal(root.get("patientId"), requestVO.getPatientId())));
			}
		}
		return cb.and(predicates.toArray(new Predicate[predicates.size()]));
	}
}
