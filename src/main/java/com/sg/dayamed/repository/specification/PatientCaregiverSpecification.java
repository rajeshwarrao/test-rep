package com.sg.dayamed.repository.specification;

import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By Gorantla, Eresh on 24/Jun/2019
 **/
public class PatientCaregiverSpecification implements Specification<PatientCaregiverAssociationsV> {

	private UserRoleEnum roleEnum;

	private LoadPatientRequestVO requestVO;

	public PatientCaregiverSpecification(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO) {
		this.requestVO = requestVO;
		this.roleEnum = userRoleEnum;
	}

	@Override
	public Predicate toPredicate(Root<PatientCaregiverAssociationsV> root, CriteriaQuery<?> query,
	                             CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		JwtUserDetails userDetails = requestVO.getUserDetails();
		predicates.add(cb.and(cb.equal(root.get("caregiverUserId"), userDetails.getUserId()),
		                      cb.equal(root.get("patientStatus"), NumberUtils.INTEGER_ZERO)));
		if (PatientSortByEnum.ACTIVE_TIME.equals(requestVO.getSortByEnum())) {
			if (SortOrderEnum.ASC.equals(requestVO.getSortOrderEnum())) {
				query.orderBy(cb.asc(root.get("lastActiveTime")));
			} else if (SortOrderEnum.DESC.equals(requestVO.getSortOrderEnum())) {
				query.orderBy(cb.desc(root.get("lastActiveTime")));
			}
		}
		return cb.and(predicates.toArray(new Predicate[predicates.size()]));
	}
}
