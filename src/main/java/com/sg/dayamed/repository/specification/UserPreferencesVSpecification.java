package com.sg.dayamed.repository.specification;

import com.sg.dayamed.entity.view.UserPreferencesV;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/

public class UserPreferencesVSpecification implements Specification<UserPreferencesV> {

	private UserRoleEnum userRoleEnum;

	private LoadPatientRequestVO requestVO;

	public UserPreferencesVSpecification(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO) {
		this.userRoleEnum = userRoleEnum;
		this.requestVO = requestVO;
	}

	@Override
	public Predicate toPredicate(Root<UserPreferencesV> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		if (UserRoleEnum.PROVIDER.equals(userRoleEnum)) {
			predicates.add(cb.and(cb.equal(root.get("role"), UserRoleEnum.PROVIDER.getRole()),
			                      cb.isNotNull(root.get("providerId"))));
		} else if (UserRoleEnum.CAREGIVER.equals(userRoleEnum)) {
			predicates.add(cb.and(cb.equal(root.get("role"), UserRoleEnum.CAREGIVER.getRole()),
			                      cb.isNotNull(root.get("caregiverId"))));
		} else if (UserRoleEnum.PHARMACIST.equals(userRoleEnum)) {
			predicates.add(cb.and(cb.equal(root.get("role"), UserRoleEnum.PHARMACIST.getRole()),
			                      cb.isNotNull(root.get("pharmacistId"))));
		} else if (UserRoleEnum.PATIENT.equals(userRoleEnum)) {
			predicates.add(cb.and(cb.equal(root.get("role"), UserRoleEnum.PATIENT.getRole()),
			                      cb.isNotNull(root.get("patientId"))));
		}
		if (PatientSortByEnum.ACTIVE_TIME.equals(requestVO.getSortByEnum())) {
			if (SortOrderEnum.ASC.equals(requestVO.getSortOrderEnum())) {
				query.orderBy(cb.asc(root.get("lastActiveTime")));
			} else if (SortOrderEnum.DESC.equals(requestVO.getSortOrderEnum())) {
				query.orderBy(cb.desc(root.get("lastActiveTime")));
			}
		}
		return cb.and(predicates.toArray(new Predicate[predicates.size()]));
	}
}
