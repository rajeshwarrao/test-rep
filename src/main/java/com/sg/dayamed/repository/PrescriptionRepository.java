package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Prescription;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {

	List<Prescription> findByPatient_id(long patientid);

	Prescription findTop1ByPatient_id(long patientid);

	List<Prescription> findByPrescriptionStatus_nameIn(List<String> status);

	Prescription findTop1ByPatient_idOrderByUpdatedDateDesc(long patientId);

	List<Prescription> findByPatient_idIn(List<Long> patientIds);

	List<Prescription> findByPatient_idAndEndDateGreaterThanOrEndDateIsNull(long patientid, LocalDateTime currentDate);
	//Not working below query
	/* @Query(
			  value = "SELECT * FROM patient_prescription pr LEFT OUTER JOIN dayamed_patient pt ON pr
			  .patient_patient_id=pt.patient_id WHERE patient_id=?1 AND (pr.end_date>?2 OR pr.end_date IS NULL)",
			  nativeQuery = true)
	 AdherenceDataPoints findByPatient_idAndEndDateGreaterThanOrEndDateIsNull(long patientId,LocalDateTime
	 currentDateTime);
	*/

	@Transactional
	@Procedure(procedureName = "delete_prescription")
	String deletePrescription(@Param("presId") long presId);
}
