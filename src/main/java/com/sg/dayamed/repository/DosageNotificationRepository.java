package com.sg.dayamed.repository;

import com.sg.dayamed.entity.DosageNotification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface DosageNotificationRepository extends JpaRepository<DosageNotification, Long> {

	//delete has some issue in spring data. thats why overrided delete function.
	@Modifying
	@Transactional
	@Query("delete from DosageNotification dn where dn.id = ?1")
	void delete(Long entityId);

	List<DosageNotification> findByDosageInfo_idAndUserIdNot(long dosageInfoId, long userid);

	//fetching list based on owner_userId And dosageInfoId
	List<DosageNotification> findByDosageInfo_idAndUserId(long dosageInfoId, long userid);
}
