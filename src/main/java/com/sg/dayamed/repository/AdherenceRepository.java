package com.sg.dayamed.repository;

import com.sg.dayamed.entity.AdherenceDataPoints;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AdherenceRepository extends JpaRepository<AdherenceDataPoints, Long> {

	//List<AdherenceDataPoints> findByDosageInfo_idInOrderByTakentimeDesc(List<Long> dosageinfoId);
	List<AdherenceDataPoints> findByPrescriptionID(long prescriptionID);

	List<AdherenceDataPoints> findByPrescribedTimeBetweenAndPrescriptionID(LocalDateTime start, LocalDateTime end,
	                                                                       long prescriptionID);

	List<AdherenceDataPoints> findByPrescribedTimeBetweenAndPrescriptionIDIn(LocalDateTime start, LocalDateTime end,
	                                                                         List<Long> prescriptionIDs);

	AdherenceDataPoints findByPrescribedTimeAndPrescriptionID(LocalDateTime PrescribedTime, long prescriptionID);

	List<AdherenceDataPoints> findByPrescriptionIDAndPrescribedTime(long prescriptionID, LocalDateTime PrescribedTime);

	List<AdherenceDataPoints> findByPrescriptionIDAndPrescribedTimeIn(long prescriptionID,
	                                                                  List<LocalDateTime> PrescribedTime);

	List<AdherenceDataPoints> findByPrescriptionIDAndAdherenceDosageInfoMapList_DosageinfoId(Long prescriptionId,
	                                                                                         Long dosageId);

	// AdherenceDataPoints findByPrescribedTimeAnd(LocalDateTime PrescribedTime,long prescriptionID);
	@Query(
			value = "SELECT * FROM USERS u WHERE u.status = 1",
			nativeQuery = true)
	AdherenceDataPoints fetchNativeQuery(LocalDateTime localDateTime, long dosageInfoId);

}
