package com.sg.dayamed.repository;

import com.sg.dayamed.entity.GSVUCAToken;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GSVUCATokenRepository extends JpaRepository<GSVUCAToken, Long> {

	List<GSVUCAToken> findByToken(String token);
}
