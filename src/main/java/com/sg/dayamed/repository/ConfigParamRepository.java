package com.sg.dayamed.repository;

import com.sg.dayamed.entity.ConfigParams;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfigParamRepository extends JpaRepository<ConfigParams, Long> {

}
