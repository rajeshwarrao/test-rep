package com.sg.dayamed.repository;

import com.sg.dayamed.entity.PrescriptionSchedule;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PrescriptionScheduleRepository extends JpaRepository<PrescriptionSchedule, Long> {

	List<PrescriptionSchedule> findByDosageInfo_id(long doasageInfoId);

	List<PrescriptionSchedule> findByDeviceInfo_id(long deviceInfoId);

	List<PrescriptionSchedule> findByUserId(long userID);

	List<PrescriptionSchedule> findByUserIdAndPrescriptionId(long userID, Long prescriptionId);

	List<PrescriptionSchedule> findByPrescribedTimeGreaterThanEqualAndUserId(LocalDateTime actualDate, long userId);

	List<PrescriptionSchedule> findByPrescribedTimeLessThanEqual(LocalDateTime actualDate);

	List<PrescriptionSchedule> findByFireTimeLessThanEqualAndDosageInfoNotNull(LocalDateTime fireTime);

	List<PrescriptionSchedule> findByFireTimeLessThanEqualAndDeviceInfoNotNull(LocalDateTime fireTime);

	List<PrescriptionSchedule> findByPrescribedTimeBetweenAndUserId(LocalDateTime start, LocalDateTime end,
	                                                                long userid);

	List<PrescriptionSchedule> findByPrescribedTimeAndDosageInfo_id(LocalDateTime actualdateandtime,
	                                                                long doasageInfoId);

	List<PrescriptionSchedule> findByPrescribedTimeAndDeviceInfo_id(LocalDateTime actualdateandtime,
	                                                                long deviceInfoId);

	@Modifying
	@Transactional
	@Query("delete from PrescriptionSchedule ps where ps.id = ?1")
	void delete(Long entityId);

	List<PrescriptionSchedule> findByDosageInfo_idIn(List<Long> dosageInfoIds);
}
