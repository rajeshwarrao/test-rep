package com.sg.dayamed.repository;

import com.sg.dayamed.entity.ConsentDocument;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsentDocumentRerpository extends JpaRepository<ConsentDocument, Long> {

	ConsentDocument findFirst1ByOrderByPublicationDateDesc();

}
