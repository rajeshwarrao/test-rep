package com.sg.dayamed.repository;

import com.sg.dayamed.entity.ImanticNotification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImanticNotificationRepository extends JpaRepository<ImanticNotification, Long> {

}
