package com.sg.dayamed.repository;

import com.sg.dayamed.dao.EhCacheConstants;
import com.sg.dayamed.entity.Provider;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long> {

	Provider findByUserDetails_id(long userid);

	Provider findByUserDetails_emailId(String emailId);

	List<Provider> findByPatients_id(long patientId);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM patient_provider_map WHERE provider_id =?1", nativeQuery = true)
	void deletepatientsByProviderId(long providerId);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM caregiver_provider_map WHERE provider_id =?1", nativeQuery = true)
	void deleteCaregiverMapByProviderId(long providerId);

	@Transactional
	@Procedure(procedureName = "delete_provider")
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	String deleteProvider(@Param("providerId") long providerId);
}
