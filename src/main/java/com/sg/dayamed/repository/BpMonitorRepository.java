package com.sg.dayamed.repository;

import com.sg.dayamed.entity.BpMonitor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BpMonitorRepository extends JpaRepository<BpMonitor, Long> {

	List<BpMonitor> findByPatient_id(long patientId);

	List<BpMonitor> findByPrescribedTime(LocalDateTime prescribedTime);

	BpMonitor findByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId);

	List<BpMonitor> findByprescriptionID(long prescriptionID);
	//List<BpMonitor> findByDateBetween(Date start, Date end);
	//List<HeartRate>  findByDateBetweenAndPatient_id(Date start, Date end,long patientId);
}
