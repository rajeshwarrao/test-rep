package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Diagnosis;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiagnosisRepository extends JpaRepository<Diagnosis, Long> {

	/*@Query(value="Select d from diagnosis d where d.description like %:description%", nativeQuery = true)
	List<Diagnosis> findByDescriptionIgnoreCaseContaining(String description);*/
	List<Diagnosis> findByNameIgnoreCaseContaining(String name);
}
