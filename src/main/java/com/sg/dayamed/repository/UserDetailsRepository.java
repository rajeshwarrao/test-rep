package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {

	//UserDetails findByEmailId(String emailid);
	UserDetails findByEmailIdIgnoreCase(String emailid);

	List<UserDetails> findByEmailIdIgnoreCaseIn(List<String> emailIds);

	UserDetails findTopByMobileNumber(String mobileNumber);

	UserDetails findByToken(String token);

	UserDetails findByImanticUserid(String imanticid);

	//List<UserDetails> findByFcmtokenIgnoreCase(String fcmToken);
	UserDetails findByPwdChangeToken(String tokeId);

	List<UserDetails> findByTokenIgnoreCase(String token);

	List<UserDetails> findByIdInAndRole(List<Long> ids, Role role);

}
