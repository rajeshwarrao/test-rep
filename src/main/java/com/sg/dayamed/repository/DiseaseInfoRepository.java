package com.sg.dayamed.repository;

import com.sg.dayamed.entity.DiseaseInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiseaseInfoRepository extends JpaRepository<DiseaseInfo, Long> {

	List<DiseaseInfo> findByPrescription_id(long prescriptionId);
}
