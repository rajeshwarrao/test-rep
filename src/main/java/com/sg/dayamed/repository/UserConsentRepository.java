package com.sg.dayamed.repository;

import com.sg.dayamed.entity.UserConsent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserConsentRepository extends JpaRepository<UserConsent, Long> {

	UserConsent findByUserIdAndVersion(long userid, String version);

}
