package com.sg.dayamed.repository;

import com.sg.dayamed.entity.DeviceInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceInfoRepository extends JpaRepository<DeviceInfo, Long> {

	List<DeviceInfo> findByPrescription_id(long prescriptionId);
}
