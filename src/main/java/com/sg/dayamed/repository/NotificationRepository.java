package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Notification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

	List<Notification> findBytype(String type);

	List<Notification> findBybelongigngUserDetails_id(long userid);

	List<Notification> findByTimeBetweenAndBelongigngUserDetails_id(Date start, Date end, long userid);

	Notification findByroomName(String roomName);
	
	/*
	@Modifying
    @Query("UPDATE Notification n SET n.deleteflag = :deleteflag WHERE n.belongigngUserDetails_id = :userID")
    int updateNotificationDeleteFlag(@Param("userID") int userId, @Param("deleteflag") String deleteflag);*/
}
