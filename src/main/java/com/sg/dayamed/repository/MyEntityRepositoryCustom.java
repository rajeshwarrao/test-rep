package com.sg.dayamed.repository;

import java.util.List;
import java.util.Map;

public interface MyEntityRepositoryCustom {

	public List<Object> findGSNotifications(String queryStr);

	public List<Map<String, String>> getColmNameAndColumValueAsMap(String query);

	public int updateQueryPrepareStamt(String updateQry, Map<Integer, Object> indexMap);

	public List<Map<String, String>> getColmNameAndColumValueAsMapWithInParams(String query,
	                                                                           Map<Integer, Object> indexMap);
}
