package com.sg.dayamed.repository;

import com.sg.dayamed.entity.ConsumerGoodInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsumerGoodsInfoRepository extends JpaRepository<ConsumerGoodInfo, Long> {

	List<ConsumerGoodInfo> findByPrescription_id(long prescriptionId);
}
