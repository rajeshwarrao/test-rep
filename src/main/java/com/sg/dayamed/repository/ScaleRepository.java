package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Scale;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ScaleRepository extends JpaRepository<Scale, Long> {

	List<Scale> findByPatient_id(long patientId);

	List<Scale> findByPrescribedTime(LocalDateTime prescribedTime);

	List<Scale> findByprescriptionID(long prescriptionID);

	Scale findByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId);
}
