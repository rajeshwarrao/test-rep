package com.sg.dayamed.repository;

import com.sg.dayamed.entity.HeartRate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface HeartRateRepository extends JpaRepository<HeartRate, Long> {

	List<HeartRate> findByPatient_id(long patientId);

	List<HeartRate> findByprescriptionID(long prescriptionID);

	List<HeartRate> findByPrescribedTime(LocalDateTime prescribedTime);

	//List<HeartRate> findByDateBetween(Date start, Date end);
	//List<HeartRate>  findByDateBetweenAndPatient_id(Date start, Date end,long patientId);
	HeartRate findByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId);
}
