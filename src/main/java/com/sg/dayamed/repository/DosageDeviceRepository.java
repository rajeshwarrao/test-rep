package com.sg.dayamed.repository;

import com.sg.dayamed.entity.DosageDevice;
import com.sg.dayamed.entity.Prescription;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface DosageDeviceRepository extends JpaRepository<DosageDevice, Long> {

	List<DosageDevice> findByPrescription_id(long prescriptionId);

	Prescription findPrescriptionById(long dosageInfoId);

	@Modifying
	@Transactional
	@Query("delete from DosageDevice dd where dd.id = ?1")
	public void delete(Long entityId);
}
