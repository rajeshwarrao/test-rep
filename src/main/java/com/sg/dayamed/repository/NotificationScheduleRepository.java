package com.sg.dayamed.repository;

import com.sg.dayamed.entity.NotificationSchedule;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
@Repository
public interface NotificationScheduleRepository extends JpaRepository<NotificationSchedule, Long> {

	// need to send here LocalDateTime instaed of Date
	List<NotificationSchedule> findByActualDateAndDosageInfo_id(LocalDateTime actualdateandtime, long doasageInfoId);

	List<NotificationSchedule> findByActualDateAndDeviceInfo_id(LocalDateTime actualdateandtime, long deviceInfoId);

	/* @Query("select sj from SchedlerJobs sj where sj.fireTime like %?1")
	List<SchedlerJobs> findByFireTimeLike(LocalDateTime fireTime);*/
	List<NotificationSchedule> findByDosageInfo_id(long doasageInfoId);

	List<NotificationSchedule> findByDeviceInfo_id(long deviceInfoId);

	List<NotificationSchedule> findByUserId(long userId);

	List<NotificationSchedule> findByUserIdAndPrescriptionId(long userId, Long prescriptionId);

	List<NotificationSchedule> findByFireTimeLessThanEqual(LocalDateTime fireTime);

	List<NotificationSchedule> findByActualDateGreaterThanEqualAndUserId(LocalDateTime actualDate, long userId);

	List<NotificationSchedule> findByFireTimeLessThanEqualAndDosageInfoNotNull(LocalDateTime fireTime);

	List<NotificationSchedule> findByFireTimeLessThanEqualAndDeviceInfoNotNull(LocalDateTime fireTime);

	@Modifying
	@Transactional
	@Query("delete from NotificationSchedule ns where ns.id = ?1")
	void delete(Long entityId);
	
	/*@Modifying
	@Transactional
	@Query("delete from NotificationSchedule ns where ns.fire_time = ?1")
	void deleteByFireTime(LocalDateTime fireTime);
	*/

	@Query(value = "SELECT id FROM notification_schedule ns WHERE ns.dosageinfo_id NOT IN (SELECT " +
			"patient_dosage_info_id  FROM patient_dosage_info)", nativeQuery = true)
	List<BigInteger> getNotificationScheduleIdsNotInDosageInfoTable();

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM notification_schedule ns WHERE ns.id IN (:notificationIds)", nativeQuery = true)
	void deleteNotificationSchedulseIdsNotInDosageInfoTable(@Param("notificationIds") List<Long> notificationIds);
	//DELETE FROM table WHERE id IN (1, 4, 6, 7)

	List<NotificationSchedule> findByDosageInfo_idIn(List<Long> dosageInfoIds);

}
