package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Glucometer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface GlucoMeterRepository extends JpaRepository<Glucometer, Long> {

	List<Glucometer> findByPatient_id(long patientId);

	List<Glucometer> findByprescriptionID(long prescriptionID);

	//List<Glucometer> findByDateBetween(Date start, Date end);
	//List<Glucometer>  findByDateBetweenAndPatient_id(Date start, Date end,long patientId);
	Glucometer findByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId);
}
