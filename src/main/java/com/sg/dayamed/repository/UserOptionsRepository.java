package com.sg.dayamed.repository;

import com.sg.dayamed.entity.UserOptions;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserOptionsRepository extends JpaRepository<UserOptions, Long> {

}
