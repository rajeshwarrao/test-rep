package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Cart;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {

	@Transactional
	long deleteByItemIdAndPatientUserIdAndItemCategoryType(Long itemId, Long patientUserId, String itemCategoryType);

	List<Cart> findByPatientUserId(long patientUserId);
}
