package com.sg.dayamed.repository;

import com.sg.dayamed.entity.DeviceNotification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DeviceNotificationRepository extends JpaRepository<DeviceNotification, Long> {

	//delete has some issue in spring data. thats why overrided delete function.
	@Modifying
	@Transactional
	@Query("delete from DeviceNotification dn where dn.id = ?1")
	void delete(Long entityId);
}
