package com.sg.dayamed.repository;

import com.sg.dayamed.entity.PulseOximeter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PulseOximeterRepository extends JpaRepository<PulseOximeter, Long> {

	List<PulseOximeter> findByPatient_id(long patientId);
	//PulseOximeter findByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime,String deviceInfoId);
}
