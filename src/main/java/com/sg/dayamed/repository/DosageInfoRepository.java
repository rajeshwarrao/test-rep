package com.sg.dayamed.repository;

import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.Prescription;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DosageInfoRepository extends JpaRepository<DosageInfo, Long> {

	List<DosageInfo> findByPrescription_id(long prescriptionId);

	Prescription findPrescriptionById(long dosageInfoId);

	//not working
	@Query(value = "SELECT  MAX(d.disage_course_duration) FROM patient_dosage_info d WHERE d.prescription_id = 1",
	       nativeQuery = true)
	int fetchMaxDurationdayByPrescriptionId(long prescriptionId);

	DosageInfo findTop1ByPrescription_idAndDeleteflagOrderByDurationDesc(long prescriptionId, boolean False);
}
