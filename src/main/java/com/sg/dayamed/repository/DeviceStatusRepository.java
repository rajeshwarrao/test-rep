package com.sg.dayamed.repository;

import com.sg.dayamed.entity.DeviceStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceStatusRepository extends JpaRepository<DeviceStatus, Long> {

	DeviceStatus findByUserDetails_id(long userid); //need to change

	List<DeviceStatus> findByUserDetails_idOrderByIdDesc(long userid);

}
