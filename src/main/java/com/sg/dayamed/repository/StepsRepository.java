package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Steps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface StepsRepository extends JpaRepository<Steps, Long> {

	List<Steps> findByPatient_id(long patientId);

	List<Steps> findByprescriptionID(long prescriptionID);

	List<Steps> findByPrescribedTime(LocalDateTime prescribedTime);

	Steps findByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId);
}
