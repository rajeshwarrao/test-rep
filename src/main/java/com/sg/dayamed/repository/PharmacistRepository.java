package com.sg.dayamed.repository;

import com.sg.dayamed.entity.Pharmacist;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface PharmacistRepository extends JpaRepository<Pharmacist, Long> {

	//List<Patient> findByProviders_id(long providerid);
	Pharmacist findByUserDetails_id(long userid);

	List<Pharmacist> findByPatients_id(long patientId);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM patient_pharmasist_map WHERE pharmasist_id =?1", nativeQuery = true)
	void deletepatientsByPharmasistId(long pharmasistId);
}
