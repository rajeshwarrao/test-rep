package com.sg.dayamed.config;

import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/

public interface OrikaMapperFactoryBuilderConfigurer {

	void configure(DefaultMapperFactory.MapperFactoryBuilder<?, ?> orikaMapperFactoryBuilder);
}
