package com.sg.dayamed.config;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;
import com.sg.dayamed.entity.view.PatientPharmacistAssociationsV;
import com.sg.dayamed.entity.view.PatientProviderAssociationsV;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetails;
import com.sg.dayamed.rest.controller.ws.users.WSUserDetails;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsVO;

import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@RequiredArgsConstructor
@ConditionalOnProperty(name = "orika.enabled", matchIfMissing = true)
@EnableConfigurationProperties(OrikaProperties.class)
@Configuration
public class OrikaConfiguration {

	/**
	 * The configuration properties for Orika.
	 */
	private final OrikaProperties orikaProperties;

	/**
	 * The configurers for {@link DefaultMapperFactory.MapperFactoryBuilder}.
	 */
	private final Optional<List<OrikaMapperFactoryBuilderConfigurer>> orikaMapperFactoryBuilderConfigurers;

	/**
	 * The configurers for {@link MapperFactory}.
	 */
	private final Optional<List<OrikaMapperFactoryConfigurer>> orikaMapperFactoryConfigurers;

	/**
	 * Creates a {@link DefaultMapperFactory.MapperFactoryBuilder}.
	 *
	 * @return a {@link DefaultMapperFactory.MapperFactoryBuilder}.
	 */
	@ConditionalOnMissingBean
	@Bean
	public DefaultMapperFactory.MapperFactoryBuilder<?, ?> orikaMapperFactoryBuilder() {
		DefaultMapperFactory.Builder orikaMapperFactoryBuilder = new DefaultMapperFactory.Builder();
		if (orikaProperties.getUseBuiltinConverters() != null) {
			orikaMapperFactoryBuilder.useBuiltinConverters(orikaProperties.getUseBuiltinConverters());
		}
		if (orikaProperties.getUseAutoMapping() != null) {
			orikaMapperFactoryBuilder.useAutoMapping(orikaProperties.getUseAutoMapping());
		}
		if (orikaProperties.getMapNulls() != null) {
			orikaMapperFactoryBuilder.mapNulls(orikaProperties.getMapNulls());
		}
		if (orikaProperties.getDumpStateOnException() != null) {
			orikaMapperFactoryBuilder.dumpStateOnException(orikaProperties.getDumpStateOnException());
		}
		if (orikaProperties.getFavorExtension() != null) {
			orikaMapperFactoryBuilder.favorExtension(orikaProperties.getFavorExtension());
		}
		if (orikaProperties.getCaptureFieldContext() != null) {
			orikaMapperFactoryBuilder.captureFieldContext(orikaProperties.getCaptureFieldContext());
		}
		orikaMapperFactoryBuilderConfigurers
				.orElseGet(Collections::emptyList)
				.forEach(configurer -> configurer.configure(orikaMapperFactoryBuilder));
		//log.debug("Created a MapperFactoryBuilder: [{}]", orikaMapperFactoryBuilder);
		return orikaMapperFactoryBuilder;
	}

	/**
	 * Creates a {@link MapperFactory}.
	 *
	 * @param orikaMapperFactoryBuilder the {@link DefaultMapperFactory.MapperFactoryBuilder}.
	 * @return a {@link MapperFactory}.
	 */
	@ConditionalOnMissingBean
	@Bean
	public MapperFactory orikaMapperFactory(DefaultMapperFactory.MapperFactoryBuilder<?, ?> orikaMapperFactoryBuilder) {
		MapperFactory orikaMapperFactory = orikaMapperFactoryBuilder.build();
		orikaMapperFactoryConfigurers
				.orElseGet(Collections::emptyList)
				.forEach(configurer -> configurer.configure(orikaMapperFactory));
		//log.debug("Created a MapperFactory: [{}]", orikaMapperFactory);
		orikaMapperFactory.classMap(PatientAssociationDetailsV.class, PatientDetailsVO.class)
		                  .field("patientId", "id")
		                  .field("userId", "userDetails.id")
		                  .field("age", "userDetails.age")
		                  .field("appVersion", "userDetails.appVersion")
		                  .field("city", "userDetails.city")
		                  .field("country", "userDetails.country")
		                  .field("countryCode", "userDetails.countryCode")
		                  .field("userCreatedOn", "userDetails.userCreatedOn")
		                  .field("userDeviceType", "userDetails.userDeviceType")
		                  .field("emailId", "userDetails.emailId")
		                  .field("emailFlag", "userDetails.emailFlag")
		                  .field("firstName", "userDetails.firstName")
		                  .field("gender", "userDetails.gender")
		                  .field("userImageUrl", "userDetails.userImageUrl")
		                  .field("imanticUserId", "userDetails.imanticUserId")
		                  .field("lastActiveTime", "userDetails.lastActiveTime")
		                  .field("lastLoginTime", "userDetails.lastLoginTime")
		                  .field("lastName", "userDetails.lastName")
		                  .field("location", "userDetails.location")
		                  .field("middleName", "userDetails.middleName")
		                  .field("mobileNumber", "userDetails.mobileNumber")
		                  .field("osVersion", "userDetails.osVersion")
		                  .field("pwdChangeToken", "userDetails.pwdChangeToken")
		                  .field("pwdTokenExpireDate", "userDetails.pwdTokenExpireDate")
		                  .field("race", "userDetails.race")
		                  .field("smsFlag", "userDetails.smsFlag")
		                  .field("state", "userDetails.state")
		                  .field("token", "userDetails.token")
		                  .field("userUpdatedOn", "userDetails.userUpdatedOn")
		                  .field("userZoneId", "userDetails.userZoneId")
		                  .field("voipToken", "userDetails.voipToken")
		                  .field("zipCode", "userDetails.zipCode")
		                  .field("patientDeviceId", "userDetails.deviceId")
		                  .field("userZoneId", "userDetails.userZoneId")
		                  .byDefault()
		                  .register();
		orikaMapperFactory.classMap(WSPatientDetails.class, PatientDetailsVO.class)
		                  .field("userDetails", "userDetails")
		                  .field("userDetails.createdOn", "userDetails.userCreatedOn")
		                  .field("userDetails.deviceType", "userDetails.userDeviceType")
		                  .field("userDetails.imageUrl", "userDetails.userImageUrl")
		                  .field("userDetails.updatedOn", "userDetails.userUpdatedOn")
		                  .field("userDetails.address.country", "userDetails.country")
		                  .field("userDetails.address.state", "userDetails.state")
		                  .field("userDetails.address.zipCode", "userDetails.zipCode")
		                  .field("userDetails.address.location", "userDetails.location")
		                  .field("userDetails.address.city", "userDetails.city")
		                  .byDefault()
		                  .register();
		orikaMapperFactory.classMap(WSUserDetails.class, UserDetailsVO.class)
		                  .field("address.country", "country")
		                  .field("address.state", "state")
		                  .field("address.zipCode", "zipCode")
		                  .field("address.city", "city")
		                  .field("address.location", "location")
		                  .field("updatedOn", "userUpdatedOn")
		                  .field("createdOn", "userCreatedOn")
		                  .byDefault()
		                  .register();
		orikaMapperFactory.classMap(PatientProviderAssociationsV.class, PatientAssociationDetailsV.class)
		                  .byDefault()
		                  .register();
		orikaMapperFactory.classMap(PatientCaregiverAssociationsV.class, PatientAssociationDetailsV.class)
		                  .byDefault()
		                  .register();
		orikaMapperFactory.classMap(PatientPharmacistAssociationsV.class, PatientAssociationDetailsV.class)
		                  .byDefault()
		                  .register();
		return orikaMapperFactory;
	}

	/**
	 * Creates a {@link MapperFacade}.
	 *
	 * @param orikaMapperFactory the {@link MapperFactory}.
	 * @return a {@link MapperFacade}.
	 */
	@ConditionalOnMissingBean
	@Bean
	public MapperFacade orikaMapperFacade(MapperFactory orikaMapperFactory) {
		MapperFacade orikaMapperFacade = orikaMapperFactory.getMapperFacade();
		//log.debug("Created a MapperFacade: [{}]", orikaMapperFacade);
		return orikaMapperFacade;
	}
}
