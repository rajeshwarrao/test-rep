package com.sg.dayamed.config;

import ma.glasnost.orika.MapperFactory;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/

public interface OrikaMapperFactoryConfigurer {

	void configure(MapperFactory orikaMapperFactory);
}
