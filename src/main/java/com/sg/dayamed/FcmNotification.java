package com.sg.dayamed;

import com.sg.dayamed.rest.commons.BaseRestOutboundProcessor;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.pojo.notification.FCMNotificationDataRequestModel;
import com.sg.dayamed.pojo.notification.FCMNotificationRequestModel;
import com.sg.dayamed.service.UserDetailsService;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author naresh.kamisetti
 * This class is for Sending FCM Notifications to mobiles
 */
@Component
public class FcmNotification {

	@Value("${dayamed.notification.fcm.key.auth}")
	String authKey;

	@Value("${dayamed.notification.fcm.api.url}")
	String fcmCurl;

	@Autowired
	BaseRestOutboundProcessor baseRestOutboundProcessor;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	UserDetailsService userDetailsService;

	public void pushFCMNotification(String userDeviceIdKey, String msg, String title) throws Exception {
		Boolean pushNotification = userDetailsService.findByToken(userDeviceIdKey);
		if (pushNotification) {
			FCMNotificationRequestModel fCMNotificationRequestModel = new FCMNotificationRequestModel();
			fCMNotificationRequestModel.setTo(userDeviceIdKey.trim());
			FCMNotificationDataRequestModel fCMNotificationDataRequestModel = new FCMNotificationDataRequestModel();
			fCMNotificationDataRequestModel.setTitle(convertMsgIntoURLEncode(title));
			fCMNotificationDataRequestModel.setMessage(convertMsgIntoURLEncode(msg));
			fCMNotificationRequestModel.setData(fCMNotificationDataRequestModel);
			String requestBody = objectMapper.writeValueAsString(fCMNotificationRequestModel);
			baseRestOutboundProcessor.post(fcmCurl, requestBody, String.class, generateCommonHeaders());
		}
	}

	public void pushTwilioFCMNotification(String userDeviceIdKey, String msg, String title, String roomName,
	                                      String callerName) throws Exception {
		FCMNotificationRequestModel fCMNotificationRequestModel = new FCMNotificationRequestModel();
		fCMNotificationRequestModel.setTo(userDeviceIdKey.trim());
		FCMNotificationDataRequestModel fCMNotificationDataRequestModel = new FCMNotificationDataRequestModel();
		fCMNotificationDataRequestModel.setTitle(convertMsgIntoURLEncode(title));
		fCMNotificationDataRequestModel.setMessage(convertMsgIntoURLEncode(msg));
		fCMNotificationDataRequestModel.setRoomName(roomName);
		fCMNotificationDataRequestModel.setCallerName(callerName);
		fCMNotificationRequestModel.setData(fCMNotificationDataRequestModel);
		String requestBody = objectMapper.writeValueAsString(fCMNotificationRequestModel);
		baseRestOutboundProcessor.post(fcmCurl, requestBody, String.class, generateCommonHeaders());
	}

	public void pushVideoCallFCMNotification(String userDeviceIdKey, String msg, String title, String roomName,
	                                         String callerName, VidoeCallSignatureModel videoCallSignatureModel)
			throws Exception {

		FCMNotificationRequestModel fCMNotificationRequestModel = new FCMNotificationRequestModel();
		fCMNotificationRequestModel.setTo(userDeviceIdKey.trim());
		FCMNotificationDataRequestModel fCMNotificationDataRequestModel = new FCMNotificationDataRequestModel();
		fCMNotificationDataRequestModel.setTitle(convertMsgIntoURLEncode(title));
		fCMNotificationDataRequestModel.setMessage(convertMsgIntoURLEncode(msg));
		fCMNotificationDataRequestModel.setRoomName(roomName);
		fCMNotificationDataRequestModel.setCallerName(callerName);
		fCMNotificationDataRequestModel.setVidoeCallSignatureModel(videoCallSignatureModel);
		fCMNotificationRequestModel.setData(fCMNotificationDataRequestModel);
		String requestBody = objectMapper.writeValueAsString(fCMNotificationRequestModel);
		baseRestOutboundProcessor.post(fcmCurl, requestBody, String.class, generateCommonHeaders());
	}

	private String convertMsgIntoURLEncode(String message) {
		try {
			return URLEncoder.encode(message, CharEncoding.UTF_8);
		} catch (Exception e) {
			return message;
		}
	}

	private Map<String, String> generateCommonHeaders() {
		Map<String, String> headers = new HashMap<>();
		headers.put("Authorization", "key=" + authKey);
		return headers;
	}
}
