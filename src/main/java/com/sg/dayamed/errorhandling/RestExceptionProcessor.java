package com.sg.dayamed.errorhandling;

import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.ExceptionLogUtil;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.exceptions.UnexpectedException;
import com.sg.dayamed.rest.commons.ErrorKeyToResponseStatusMapper;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestFault;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;


/*
 * This class is a Centralized exception handling component
 */

@ControllerAdvice(basePackages = "com.sg.dayamed")
@RequestMapping(produces = "application/json")
public class RestExceptionProcessor {

	private static final Logger logger = LoggerFactory.getLogger(RestExceptionProcessor.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	ObjectMapper mapper;

	/*
	 * This method processes exceptions in the application and returns appropriate error message
	 */
	@ExceptionHandler(ServiceException.class)
	@ResponseBody
	public ServiceError handleServiceException(final ServiceException ex) {
		final String message = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
		// log the error
		if (ex.getLogger() != null) {
			ex.getLogger()
			  .error(message, ex);
		}
		final HttpStatus httpStatus = ex.getHttpStatus() == null ? HttpStatus.INTERNAL_SERVER_ERROR
				: ex.getHttpStatus();
		return new ServiceError(message, httpStatus.value(), httpStatus.getReasonPhrase());
	}

	@ExceptionHandler(value = {RestApiException.class})
	protected ResponseEntity<RestFault> handleException(RestApiException ex, Locale locale, HttpServletRequest request)
			throws Exception {
		RestApiException restApiException = null;
		Throwable rootCause = ExceptionLogUtil.getRootCause(ex);

		if (rootCause instanceof RestApiException) {
			restApiException = (RestApiException) rootCause;
			if (StringUtils.isBlank(restApiException.getFaultInfo()
			                                        .getErrorMessage())) {
				String message = getErrorMessageFromResource(restApiException, locale);
				restApiException.getFaultInfo()
				                .setErrorMessage(message);
			}
		} /*else if (rootCause instanceof DataValidationException) {
			restApiException = new RestApiException((DataValidationException) rootCause);
		} else if (rootCause instanceof ApplicationException) {
			restApiException = new RestApiException(rootCause.getMessage(), RestApiException
			.createApplicationExceptionFault((ApplicationException) rootCause));
		}*/
		// Exception is unknown.
		if (restApiException == null) {
			restApiException =
					new RestApiException(ex.getMessage(), RestApiException.createUnexpectedFault(new Exception(ex)));
		}
		RestFault fault = restApiException.getFaultInfo();
		if (ArrayUtils.isNotEmpty(fault.getErrorParameters())) {
			fault.setErrorMessage(MessageFormat.format(fault.getErrorMessage(), fault.getErrorParameters()));
			fault.setErrorParameters(null);
		}
		if (StringUtils.isBlank(fault.getErrorMessage())) {
			fault.setErrorMessage("UnExpected System Exception Occurred");
		}

		// Map error key into status.
		int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey())
		                                           .getStatus();
		fault.setUid(UUID.randomUUID()
		                 .toString());

		// Add content type and x-cub-hdr to Http headers.
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
		headers.add("error_hdr", toJson(fault));
		RestApiException apiException = restApiException;

		// Return the response entity with fault response, headers and status.
		return new ResponseEntity<RestFault>(fault, headers, HttpStatus.valueOf(status));
	}

	@ExceptionHandler({ConstraintViolationException.class})
	protected ResponseEntity<RestFault> handleConstraintViolation(ConstraintViolationException ex,
	                                                              HttpServletRequest request) {
		RestApiException restApiException =
				new RestApiException(ex.getMessage(), RestApiException.createUnexpectedFault(new Exception(ex)));
		List<ConstraintViolation> violations = ex.getConstraintViolations()
		                                         .stream()
		                                         .collect(Collectors.toList());
		ConstraintViolation constraintViolation = CollectionUtils.isNotEmpty(violations) ? violations.get(0) : null;
		if (constraintViolation != null) {
			Path path = constraintViolation.getPropertyPath();
			PathImpl node = (PathImpl) path;
			DataValidationException validationException = new DataValidationException(node.getLeafNode()
			                                                                              .asString(), null);
			String errorMessage = constraintViolation.getMessage();
			validationException.setMessage(errorMessage);
			restApiException = new RestApiException(validationException);
		}
		RestFault fault = restApiException.getFaultInfo();
		if (StringUtils.isBlank(fault.getErrorKey())) {
			fault.setErrorKey(ErrorKeyToResponseStatusMapper.ResponseStatus.BAD_REQUEST.getResult());
		}
		int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey())
		                                           .getStatus();
		fault.setUid(UUID.randomUUID()
		                 .toString());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json;charset=UTF-8");
		headers.add("error_hdr", this.toJson(fault));
		RestApiException apiException = restApiException;
		return new ResponseEntity(fault, headers, HttpStatus.valueOf(status));
	}

	@ExceptionHandler({MissingServletRequestParameterException.class})
	protected ResponseEntity<RestFault> handleConflict(MissingServletRequestParameterException ex,
	                                                   HttpServletRequest request) {
		RestApiException restApiException;
		DataValidationException validationException = new DataValidationException(ex.getParameterName(),
		                                                                          IWSGlobalApiErrorKeys.ERRORS_MISSING_REQUEST_PARAMETER_FIELD);
		validationException.setMessage(ex.getMessage());
		restApiException = new RestApiException(validationException);
		/*if (restApiException == null) {
			restApiException = new RestApiException(ex.getMessage(), RestApiException.createUnexpectedFault(new
			Exception(ex)));
		}*/
		RestFault fault = restApiException.getFaultInfo();
		if (StringUtils.isBlank(fault.getErrorKey())) {
			fault.setErrorKey(ErrorKeyToResponseStatusMapper.ResponseStatus.BAD_REQUEST.getResult());
		}
		int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey())
		                                           .getStatus();
		fault.setUid(UUID.randomUUID()
		                 .toString());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json;charset=UTF-8");
		headers.add("error_hdr", this.toJson(fault));
		RestApiException apiException = restApiException;
		return new ResponseEntity(fault, headers, HttpStatus.valueOf(status));
	}

	@ExceptionHandler({MethodArgumentNotValidException.class})
	protected ResponseEntity<RestFault> handleConflict(MethodArgumentNotValidException ex,
	                                                   HttpServletRequest request) {
		RestApiException restApiException = null;
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();
		fieldErrors = fieldErrors.stream()
		                         .sorted(Comparator.comparing(FieldError::getField))
		                         .collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(fieldErrors)) {
			FieldError error = fieldErrors.get(0);
			DataValidationException validationException = new DataValidationException(error.getField(), null);
			String errorMessage;
			if (StringUtils.contains(error.getDefaultMessage(), "{0}")) {
				errorMessage = MessageFormat.format(error.getDefaultMessage(),
				                                    new Object[]{error.getRejectedValue().toString()});
			} else {
				errorMessage = error.getDefaultMessage();
			}
			validationException.setMessage(errorMessage);
			restApiException = new RestApiException(validationException);
		}

		if (restApiException == null) {
			restApiException =
					new RestApiException(ex.getMessage(), RestApiException.createUnexpectedFault(new Exception(ex)));
		}

		RestFault fault = restApiException.getFaultInfo();
		if (StringUtils.isBlank(fault.getErrorKey())) {
			fault.setErrorKey(ErrorKeyToResponseStatusMapper.ResponseStatus.BAD_REQUEST.getResult());
		}
		int status = ErrorKeyToResponseStatusMapper.getResponseStatus(fault.getErrorKey())
		                                           .getStatus();
		fault.setUid(UUID.randomUUID()
		                 .toString());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json;charset=UTF-8");
		headers.add("error_hdr", this.toJson(fault));
		RestApiException apiException = restApiException;
		return new ResponseEntity(fault, headers, HttpStatus.valueOf(status));
	}
	
	/*@ExceptionHandler({AuthenticationException.class})
	@ResponseBody
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ServiceError handleExpiredJwtException(
      Exception ex, WebRequest request) {
		String role = "";
		try {
			Object principal = request.getUserPrincipal();
			ObjectMapper mapper = new ObjectMapper();
			JSONObject jsosnobj = new JSONObject(mapper.writeValueAsString(principal)).getJSONObject("principal");
			if(jsosnobj.has("role")){
				role = jsosnobj.getString("role");
			}
			logger.error("UnAuthorized Access, the "+ role+" can not acccess this API - "+((ServletWebRequest)request)
			.getRequest().getRequestURL().toString(), ex.getMessage() );
		} catch (JsonProcessingException | JSONException e) {
			logger.error("JsonProcessingException while handling AccessDeniedException");
			e.getMessage();
		}
		return new ServiceError("UnAuthorized Access", HttpStatus.FORBIDDEN.value(),"The user can not acccess this
		API");
    }*/

	@ExceptionHandler({AuthenticationException.class})
	@ResponseBody
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ServiceError handleCustomException(
			Exception ex, WebRequest request) {
		String role = "";
		try {
			Object principal = request.getUserPrincipal();
			ObjectMapper mapper = new ObjectMapper();
			JSONObject jsosnobj = new JSONObject(mapper.writeValueAsString(principal)).getJSONObject("principal");
			if (jsosnobj.has("role")) {
				role = jsosnobj.getString("role");
			}
			logger.error("UnAuthorized Access, the " + role + " can not acccess this API - " +
					             ((ServletWebRequest) request).getRequest()
					                                          .getRequestURL()
					                                          .toString(), ex.getMessage());
		} catch (JsonProcessingException | JSONException e) {
			logger.error("JsonProcessingException while handling AccessDeniedException");
			e.getMessage();
		}
		return new ServiceError("UnAuthorized Access", HttpStatus.FORBIDDEN.value(),
		                        "The user can not acccess this API");
	}

	@ExceptionHandler({AccessDeniedException.class})
	@ResponseBody
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ServiceError handleAccessDeniedException(
			Exception ex, WebRequest request) {
		String role = "";
		try {
			Object principal = request.getUserPrincipal();
			ObjectMapper mapper = new ObjectMapper();
			JSONObject jsosnobj = new JSONObject(mapper.writeValueAsString(principal)).getJSONObject("principal");
			if (jsosnobj.has("role")) {
				role = jsosnobj.getString("role");
			}
			logger.error("UnAuthorized Access, the " + role + " can not acccess this API - " +
					             ((ServletWebRequest) request).getRequest()
					                                          .getRequestURL()
					                                          .toString(), ex.getMessage());
		} catch (JsonProcessingException | JSONException e) {
			logger.error("JsonProcessingException while handling AccessDeniedException");
			e.getMessage();
		}
		return new ServiceError("UnAuthorized Access", HttpStatus.FORBIDDEN.value(),
		                        "The user can not acccess this API");
	}

	/**
	 * This handler is added temporarily. It should be removed once all the string request parameters representing JSON
	 * are converted to Java models
	 **/
	@ExceptionHandler({JSONException.class})
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ServiceError handleJSONException(JSONException ex, WebRequest request) {
		final String message = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
		return new ServiceError(message, HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
	}

	/*
	 * This method processes validation errors in the application and returns appropriate error message
	 */

	/*@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ValidationError handleValidationException(final MethodArgumentNotValidException exception) {
		return processFieldErrors(exception.getBindingResult().getFieldErrors());
	}*/

	/*
	 * This method processes validation errors in the application and returns appropriate error message
	 */
	@ExceptionHandler(BindException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ValidationError handleValidationBindException(final BindException exception) {
		return processFieldErrors(exception.getBindingResult()
		                                   .getFieldErrors());
	}

	/*
	 * This method processes unhandled errors in the application and returns appropriate error message
	 */
	@ExceptionHandler({Throwable.class})
	@ResponseBody
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ServiceError unhandledError(Throwable ex) {
		// log the error
		logger.error("Server Error", ex);

		return new ServiceError("Server Error", HttpStatus.INTERNAL_SERVER_ERROR.value(),
		                        "Internal Server Error");
	}

	private ValidationError processFieldErrors(List<FieldError> fieldErrors) {
		List<ValidationFieldError> validationFieldErrors = fieldErrors.stream()
		                                                              .map(fieldError -> new ValidationFieldError(
				                                                              fieldError.getField(),
				                                                              resolveLocalizedErrorMessage(fieldError)))
		                                                              .collect(Collectors.toList());
		return new ValidationError(validationFieldErrors);
	}

	private String resolveLocalizedErrorMessage(FieldError fieldError) {
		Locale currentLocale = LocaleContextHolder.getLocale();
		return messageSource.getMessage(fieldError.getDefaultMessage(), null, currentLocale);
	}

	public String toJson(Object entity) {
		try {
			return mapper.writeValueAsString(entity);
		} catch (JsonProcessingException e) {
			throw new UnexpectedException("Error converting entity to JSON string", e);
		}
	}

	private String getErrorMessageFromResource(RestApiException restApiException, Locale locale) {
		try {
			return messageSource.getMessage(restApiException.getFaultInfo()
			                                                .getErrorKey(), null, locale);
		} catch (Exception e) {
			return null;
		}
	}

}
