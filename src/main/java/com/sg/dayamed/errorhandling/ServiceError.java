package com.sg.dayamed.errorhandling;

import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.Instant;

public class ServiceError implements Serializable {

	private static final long serialVersionUID = -9037755944661782122L;

	private long timestamp;

	private String message;

	private String errorType;

	private int statusCode;

	private HttpStatus httpstatus;

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public long getTimestamp() {
		timestamp = Instant.now()
		                   .toEpochMilli();
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ServiceError(String message) {
		this.message = message;

	}

	public ServiceError(String message, int statusCode, String errorType) {
		this.message = message;
		this.errorType = errorType;
		this.statusCode = statusCode;

	}

	public ServiceError(String message, int statusCode, String errorType, HttpStatus httpstatus) {
		this.message = message;
		this.errorType = errorType;
		this.statusCode = statusCode;
		this.httpstatus = httpstatus;
	}

}
