package com.sg.dayamed.errorhandling;

public class ValidationFieldError {

	private String fieldName;

	private String message;

	public String getFieldName() {
		return fieldName;
	}

	public String getMessage() {
		return message;
	}

	public ValidationFieldError(String fieldName, String message) {
		this.fieldName = fieldName;
		this.message = message;
	}

}
