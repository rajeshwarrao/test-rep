package com.sg.dayamed.errorhandling;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ServiceException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = -9037755944661782123L;

	private HttpStatus httpStatus;

	private Exception wrappedException;

	private Logger logger;

	public Logger getLogger() {
		return logger;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public ServiceException(String errorMessage) {
		super(errorMessage);
	}

	public ServiceException(String errorMessage, Logger logger) {
		super(errorMessage);
		this.logger = logger;

	}

	public ServiceException(String errorMessage, HttpStatus httpStatus) {
		super(errorMessage);
		this.httpStatus = httpStatus;
	}

	public ServiceException(String errorMessage, HttpStatus httpStatus, Logger logger) {
		super(errorMessage);
		this.httpStatus = httpStatus;
		this.logger = logger;
	}

	public ServiceException(String errorMessage, Exception exception) {
		super(exception.getMessage());
		wrappedException = exception;
	}

	public Exception getWrappedException() {
		return wrappedException;
	}

}

 
