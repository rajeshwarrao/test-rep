package com.sg.dayamed.errorhandling;

import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ValidationError implements Serializable {

	private static final long serialVersionUID = -9037775944661782122L;

	private long timestamp;

	private final int statusCode = HttpStatus.BAD_REQUEST.value();

	private final String message = "Bad Request.Validation Errors exist in the Application";

	private final String errorType = "Validation Error";

	private List<ValidationFieldError> validationErrors = new ArrayList<>();

	public ValidationError(List<ValidationFieldError> fieldErrors) {
		this.validationErrors = fieldErrors;
	}

	public long getTimestamp() {
		timestamp = Instant.now()
		                   .toEpochMilli();
		return timestamp;
	}

	public String getErrorType() {
		return errorType;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public List<ValidationFieldError> getValidationErrors() {
		return validationErrors;
	}

	public void addFieldError(String path, String message) {
		ValidationFieldError error = new ValidationFieldError(path, message);
		validationErrors.add(error);
	}

	public String getMessage() {
		return message;
	}

}
