package com.sg.dayamed.service;

import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.Prescription;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

public interface NotificationScheduleService {

	public boolean addSchedulerJob(Prescription prescription, String zoneid);

	public boolean updateSchedulerJobByDeviceInfo(LocalDateTime ldtPrescribedDatetimeInUTC, Long infoId,
	                                              String consumptionStatus);

	public boolean updateNotificationSchedulerJobByDosageInfo(LocalDateTime ldtPrescribedDatetimeInUTC, Long infoId,
	                                                          String consumptionStatus);

	public void deletebulkNotificationSchdule(List<BigInteger> nsIds);

	//public List<NotificationSchedule> fetchJobsByUserId(Long userId);
	public List<NotificationSchedule> fetchJobsByUserIdAndPrescriptionId(Long userId, Long prescriptionId);

	public List<NotificationSchedule> fetchJobsByDosageInfoIds(List<Long> dosageInfoIds);
}
