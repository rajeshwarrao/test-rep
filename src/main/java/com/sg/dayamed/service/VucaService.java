package com.sg.dayamed.service;

import com.sg.dayamed.exceptions.ApplicationException;

public interface VucaService {

	String fetchVucaVideoURL(String ndc) throws ApplicationException;

}
