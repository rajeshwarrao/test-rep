package com.sg.dayamed.service;

import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.ProviderModel;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProviderService {

	public ProviderModel validateAddProvider(ProviderModel providerModel, MultipartFile profileImage)
			throws DataValidationException;

	public ProviderModel addProvider(ProviderModel providerModel);

	public List<ProviderModel> fetchProviders();

	public void deleteProviderById(long providerid) throws DataValidationException;

	public ProviderModel fetchProviderById(long providerId) throws DataValidationException;

	public ProviderModel fetchProviderByUserId(long userId) throws DataValidationException;

	public List<ProviderModel> fetchProvidersByPatientId(long patientid);

	public Provider directFetchProviderById(long providerId);

	public Provider directFetchProviderByUserId(long userId);
}
