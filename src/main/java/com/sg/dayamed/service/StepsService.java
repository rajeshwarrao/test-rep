package com.sg.dayamed.service;

import com.sg.dayamed.entity.Steps;

import java.time.LocalDateTime;
import java.util.List;

public interface StepsService {

	public List<Steps> findByPrescribedTime(LocalDateTime actualDate);
}
