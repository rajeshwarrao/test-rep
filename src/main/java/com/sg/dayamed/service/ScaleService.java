package com.sg.dayamed.service;

import com.sg.dayamed.entity.Scale;

import java.time.LocalDateTime;
import java.util.List;

public interface ScaleService {

	public List<Scale> findByPrescribedTime(LocalDateTime actualDate);
}
