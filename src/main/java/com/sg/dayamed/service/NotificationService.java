package com.sg.dayamed.service;

import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.helper.pojo.CaregiverDosageNotificationModel;
import com.sg.dayamed.pojo.APNSUpdateTokenRequestModel;
import com.sg.dayamed.pojo.CalendarNotificationModel;
import com.sg.dayamed.pojo.CalenderNotificationRequestModel;
import com.sg.dayamed.pojo.DosageNotificationModel;
import com.sg.dayamed.pojo.FcmNotificationRequestModel;
import com.sg.dayamed.pojo.NotificationModel;
import com.sg.dayamed.pojo.NotificationRequestModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.VideoNotificationRequestModel;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;

import java.util.Date;
import java.util.List;

public interface NotificationService {

	public boolean addNotification(List<Long> recievingUseridlist, long belongingUserId, String msg, String title,
	                               String type);

	public Notification addWebNotification(Notification notification);

	public Notification addWebNotification(List<Long> useridlist, long belongingUserID, String msg, String title,
	                                       String type);

	public Notification addWebNotification(long belongingUserId, String msg, String title, String notificationType);

	public String deleteNotificationById(long id);

	public void deleteNotifications(List<Notification> notificationList);

	public String sendTwilioVideoFCMNotification(String userid, String msg, String title, String roomName,
	                                             String callerName);

	public Notification fetchNotificationById(long id);

	public List<Notification> fetchAllNotifications();

	public List<Notification> fetchByNotificationType(String type) ;

	public List<NotificationModel> fetchNotificationModelsByNotificationType(String type) throws  Exception;

	//public List<Notification> fetchNotificationsByUserId(long userid);

	public List<Notification> fetchNotificationsByBelongingUserId(long belongingUserId);

	public Notification fetchNotificationsByUseridAndType(long userid, String type);

	public List<Notification> fetchNotificationsByUseridBetweenDates(Date start, Date end, long belonginguserid);

	public String sendMobileNotification(long userid, String msg, String title);

	public String sendMobileNotification(String deviceType, String msg, String title, String token);

	public String sendMobileVidoeCallNotification(String userid, String msg, String title, String roomName,
	                                              String callerName, VidoeCallSignatureModel vidoeCallSignatureModel);

	public Notification getNotificationByRoomName(String roomName);

	public Notification saveNotification(Notification notification);


	CalendarNotificationModel calenderNotificationsWithOutScheduledByPatientId(Long userId,
	                                                                           CalenderNotificationRequestModel calenderNotificationRequestModel)
			throws Exception;

	CalendarNotificationModel calenderNotificationsByPatientId(Long userId,
	                                                           CalenderNotificationRequestModel calenderNotificationRequestModel)
			throws Exception;

	String sendVideoNotificationByUserId(VideoNotificationRequestModel videoNotificationRequestModel) throws Exception;

	String sendFcmNotification(FcmNotificationRequestModel fcmNotificationRequestModel) throws Exception;

	UserDetailsModel updateTokenInUserDetails(APNSUpdateTokenRequestModel aPNSUpdateTokenRequestModel);

	NotificationModel addNotificationFromController(NotificationRequestModel notificationRequestModel) throws  Exception;
	
	CaregiverDosageNotificationModel addCaregiverNotifications(CaregiverDosageNotificationModel caregiverDosageNotificationModel) throws Exception;

	List<DosageNotificationModel> fetchDosageNotificationsByCaregiverId(Long caregiverUserId, Long dosageInfoId);
}
