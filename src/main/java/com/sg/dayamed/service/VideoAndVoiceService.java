package com.sg.dayamed.service;

import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

public interface VideoAndVoiceService {

	public VidoeCallSignatureModel getZoomVideoCallSignature(int userRole, JwtUserDetails jwtUser);

	public String sendVideoCallNotificationToReciverByUserId(VidoeCallSignatureModel vidoeCallSignatureModel,
	                                                         long callerUserId);
}
