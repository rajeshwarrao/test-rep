package com.sg.dayamed.service;

import com.sg.dayamed.entity.HeartRate;

import java.time.LocalDateTime;
import java.util.List;

public interface HeartRateService {

	public List<HeartRate> findByPrescribedTime(LocalDateTime actualDate);

	public HeartRate fetchHeartRateByPrescribedTimeAndDeviceInfoId(LocalDateTime actualDate, String deviceInfoId);

}
