package com.sg.dayamed.service;

import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import java.util.List;

public interface OrderOrCartService {

	public String addOrUpdateCartItems(long userId, String inputJson, JwtUserDetails jwtUser);

	public String deleteCartItems(long userId, long itemId, String itemCategoryType);

	public List<Object> getCartSummaryByUserId(long userId);
}
