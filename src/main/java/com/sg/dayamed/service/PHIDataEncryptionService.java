package com.sg.dayamed.service;

import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PHIDataEncryptionService {

	List<PatientModel> encryptPatientList(List<PatientModel> listPatinetModel, String keyText);

	PatientModel encryptPatient(PatientModel patientModel, String keyText);

	PatientModel encryptPatient(PatientModel patientModel, String keyText,
	                            String phidataEncryptFieldsArryFelids);

	PatientModel decryptPatient(PatientModel patientModel, String keyText);

	PatientModel decryptPatient(PatientModel patientModel, String keyText,
	                            String phidataEncryptFieldsArryFelids);

	String getKeyText(HttpServletRequest request, Authentication authentication);

	String getKeyText(JwtUserDetails jwtUser);

	String getKeyText(String token, String typeOfDevice, String deviceId);

	UserDetailsModel encryptUserDetails(UserDetailsModel userDetailsModel, String keyText);

	UserDetailsModel encryptUserDetails(UserDetailsModel userDetailsModel, String keyText,
	                                    String phidataEncryptFieldsArryFelids);

	UserDetailsModel decryptPatientUserDetails(UserDetailsModel userDetailsModel, String keyText,
	                                           String phidataEncryptFieldsArryFeilds);

}
