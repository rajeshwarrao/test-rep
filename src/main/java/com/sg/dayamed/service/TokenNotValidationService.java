package com.sg.dayamed.service;

import com.sg.dayamed.controller.ws.ImanticNotificationReqModel;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingReqModel;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.util.DataForImantics;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public interface TokenNotValidationService {

	public Integer endMeeting(ZoomEndMeetingReqModel zoomEndMeetingReqModel, String zoomNotificationTokenFromApi)
			throws Exception;

	public RestResponse getVUCAUrlbyGSToken(String gsToken) throws Exception;

	public RestResponse webErrorLogsSave(String inputJson) throws Exception;

	public List<DataForImantics> fetchPatientAttributes(Optional<String> patientId) throws Exception;

	public String fetchNDCImages(String imgname, HttpServletResponse response) throws Exception;

	public String getDeviceImageOrmedicineImageOrProfileImagetestOrImage(String imgname, HttpServletResponse response)
			throws Exception;

	public String addImanticNotification(ImanticNotificationReqModel notificationModel) throws Exception;

}
