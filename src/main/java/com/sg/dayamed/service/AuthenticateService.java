package com.sg.dayamed.service;

import com.sg.dayamed.pojo.UserDetailsModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthenticateService {

	public UserDetailsModel login(String username, String pwd, String typeofdivice, HttpServletRequest request,
	                              HttpServletResponse response);

	public UserDetailsModel login(String emailid);
}
