package com.sg.dayamed.service;

import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.NDCImageModel;

import org.springframework.data.domain.Page;

public interface MedicineService {

	void updateImages(NDCImageModel imageModel) throws DataValidationException;

	Page<Medicine> getMedicines(String nameFilter, int offset, int limit);
}
