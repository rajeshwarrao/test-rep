package com.sg.dayamed.service;

import com.sg.dayamed.pojo.BpMonitorModel;
import com.sg.dayamed.pojo.GlucometerModel;
import com.sg.dayamed.pojo.HeartRateModel;
import com.sg.dayamed.pojo.PulseOximeterModel;
import com.sg.dayamed.pojo.ScaleModel;
import com.sg.dayamed.pojo.StepsModel;

import java.time.LocalDateTime;
import java.util.List;

public interface BiometricService {

	public HeartRateModel addHeartRate(HeartRateModel heartrate);

	public List<HeartRateModel> fetchHeartRatesByPatientId(long id);

	public GlucometerModel addGlucometer(GlucometerModel glucometerModel);

	public List<GlucometerModel> fetchGlocoReadingsByPatientId(long id);

	public BpMonitorModel addBpReading(BpMonitorModel bpMonitorModel);

	public List<BpMonitorModel> fetchBpeadingsByPatientId(long id);

	public StepsModel addSteps(StepsModel stepsModel);

	public List<StepsModel> fetchStepsByPatientId(long patientid);

	public PulseOximeterModel addPulseOximeter(PulseOximeterModel pulseOximeterModel);

	public List<PulseOximeterModel> fetchPulseOximeterByPatientId(long patientid);

	public ScaleModel addScale(ScaleModel scaleModel);

	public List<ScaleModel> fetchScaleByPatientId(long patientid);

	//
	public GlucometerModel fetchGlucometerByPrescribedTimeAndDeviceInfoId(LocalDateTime actualDate,
	                                                                      String deviceInfoId);

	public BpMonitorModel fetchBpMonitorByPrescribedTimeAndDeviceInfoId(LocalDateTime actualDate, String deviceInfoId);

	public ScaleModel fetchScaleByPrescribedTimeAndDeviceInfoId(LocalDateTime actualDate, String deviceInfoId);

	public StepsModel fetchStepsByPrescribedTimeAndDeviceInfoId(LocalDateTime actualDate, String deviceInfoId);

	public HeartRateModel fetchHeartRateByPrescribedTimeAndDeviceInfoId(LocalDateTime actualDate, String deviceInfoId);
}
