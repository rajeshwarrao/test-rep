package com.sg.dayamed.service;

import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.entity.Device;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PharmacistModel;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PharmaService {

	List<PharmacistModel> fetchPharmasists();

	List<Medicine> fetchMedicines();

	List<ConsumerGoods> fetchConsumergoods();

	List<Device> fetchAllDevices();

	List<Disease> fetchDiseases();

	Pharmacist findByUserDetails_id(long userid);

	PharmacistModel addPharmacist(PharmacistModel pharmacistModel);

	PharmacistModel addPharmacistValidation(PharmacistModel pharmacistModel, MultipartFile profileImage)
			throws Exception;

	String deletePharmacist(long pharmacistId);

	PharmacistModel fetchPharmacistById(Long pharmacistId) throws DataValidationException;

	String deletePharmacistValidation(Long pharmacistId) throws DataValidationException;
}
