package com.sg.dayamed.service;

import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;

import java.util.List;

public interface PatientService {

	//public Patient addPatient();
	//public Patient addPatient(Patient patient);
	public PrescriptionModel addPrescription(Prescription prescription, long pat_id, long userId);

	public PrescriptionModel updatePrescription(Prescription prescription);

	public List<PatientModel> fetchPatientsByProviderId(long id);

	public List<PatientModel> fetchPatientsByCaregiverId(long id);

	public Patient fetchPatientById(long id);

	public Patient directFetchPatientById(long id);

	public List<PrescriptionModel> fetchPrescriptionlistByPatientId(long id);

	public List<Patient> fetchAllPatients();

	public List<Patient> fetchEncryptPatientsByProviderId(long providerid);

	public List<PrescriptionModel> fetchPrescriptionlistByStatus(List<String> status);

	public PatientModel addPatient(Patient patient);

	public PatientModel updatePatient(Patient patient);

	public PatientModel updatePatientAlone(Patient patient);

	public String deletePatient(long id);

	public Patient findByUserDetails_id(long userid);

	public Patient fetchByDeviceId(String deviceId);

	public PatientAdherencePojo fetchAdherenceByPatientId(long id);

	public Patient fetchPatientByUserId(long userID);

	public Patient directFetchPatientByPatientId(long id);

	public Patient noDecrypCargiverfetchPatientById(long id);

	public List<Patient> directFetchPatientsByCaregiverId(long id);

	public Patient decryptPatientProviderfetchPatientById(long id);

	public String deletePatientByProcedure(long patientId);

}
