package com.sg.dayamed.service;

import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.pojo.ConsumptionFrequencyResponseModel;
import com.sg.dayamed.pojo.FirstConsumptionResponseModel;

public interface ConsumptionTemplateService {

	ConsumptionTemplate fetchConsumptionTemplateByConsumptionsKey(String consumptionsKey);

	ConsumptionFrequencyResponseModel getFrequencyList();

	FirstConsumptionResponseModel getFirstConsumption(Long frequencyId);
}
