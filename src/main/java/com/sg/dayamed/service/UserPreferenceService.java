package com.sg.dayamed.service;

import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.pojo.UserPreferenceModel;

import java.io.IOException;

public interface UserPreferenceService {

	public String fetchUserPreferenceTimeByUserId(Long userid);

	public UserPreference addUserPreference(Long userId, UserPreferenceModel userPreferenceModel);

	UserPreferenceModel findUserSettingsByUserId(Long userId) throws IOException;

	UserPreferenceModel updateUserPreference(Long userId, UserPreferenceModel userPreferenceModel) throws Exception;

}
