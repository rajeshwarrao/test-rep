package com.sg.dayamed.service;

import com.sg.dayamed.entity.Diagnosis;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.DiagnosisModel;

import java.util.List;

public interface DiagnosisService {

	public List<Diagnosis> fetchDiagnosisIntelligenceByDescription(String description)throws DataValidationException;
	public List<DiagnosisModel> fetchDiagnosisModelIntelligenceByDescription(String description)throws DataValidationException;

	public Diagnosis addDiagnosis(Diagnosis diagnosis);
}
