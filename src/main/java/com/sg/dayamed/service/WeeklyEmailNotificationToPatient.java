package com.sg.dayamed.service;

import com.sg.dayamed.entity.GSVUCAToken;

public interface WeeklyEmailNotificationToPatient {

	public String sendNotificationToPatient(String notoificationType, String emailId);

	public GSVUCAToken getNdcCodeByGSToken(String gsToken);
}
