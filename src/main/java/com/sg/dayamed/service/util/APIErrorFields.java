package com.sg.dayamed.service.util;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/

public interface APIErrorFields {

	String SORT_BY = "sortBy";
	String SORT_ORDER = "sortOrder";
	String AUTHORIZATION = "authorization";
	String QUERY_STRING = "query";
	String ADMIN_LOAD_PATENT_ERROR_FIELD = "caregiverId/providerId";
	String FIELDNAME = "username";
	String USER_ID = "userId";
	String CURRENT_CREDENTIAL = "currentPassword";
	String IMAGE_PATH = "imagePath";
	String fieldName = "username";
	String FILE_UPLOAD = "file";
}
