package com.sg.dayamed.service.util;

import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/

public interface APIErrorKeys extends IWSGlobalApiErrorKeys {

	String ERRORS_INVALID_ROLE = "errors.invalid.role.in.authorization.header";
	String ERRORS_INVALID_LENGTH_FOR_QUERY = "errors.invalid.length.for.query.string";
	String ERRORS_EITHER_CAREGIVER_ID_OR_PROVIDER_ID_IS_REQUIRED =
			"errors.either.caregiver.id.or.provider.id.is.required";
	String ERRORS_BOTH_CAREGIVER_ID_OR_PROVIDER_ID_IS_NOT_EXPECTED =
			"errors.both.caregiver.id.or.provider.id.is.not.expected";
	String ERRORS_INVALID_USER_ID = "errors.invalid.user.id";
	String ERRORS_INVALID_CURRENT_CREDENTIALS = "errors.invalid.current.password";
	String ERROR_LOGIN_USERNAME_NOT_FOUND = "error.login.username.not.found";

	String ERROR_LOGIN_CREDENTIALS_INCORRECT = "error.login.username.or.password.incorrect";

	String ERROR_EMAIL_CONFLICT = "error.email.conflict.already.exist";
	String ERROR_INPUT_BADREQUEST = "error.input.badrequest";
	String ERROR_INPUT_BADREQUEST_MUST_NOT_NULL = "error.input.badrequest.must.not.null";
	String ERROR_PROVIDER_ASSOCIATED_WITH_PATIENTS = "error.provider.associated.with.patients";
	String ERROR_CAREGIVER_ASSOCIATED_WITH_PATIENTS = "error.caregiver.associated.with.patients";
	String ERROR_PROVIDED_ID_NOT_IN_DB = "error.provided.id.not.in.db";

	String ERROR_EMAIL_ALREADY_EXISTED = "email.already.existed";
	String ERROR_INVALID_DATA = "error.request.invalid.data";

	String ERROR_UNABLE_TO_GET_VUCA_TOKEN = "error.unable.to.get.vuca.token";
	String ERROR_NO_IMAGE_IN_IMAGE_DIRECTORY = "error.no.image.in.image.directory";
	String ERROR_IMAGE_DIRECTORY_NOT_ACCESSIBLE = "error.image.directory.not.accessible";
	String ERROR_PHARMACIST_ASSOCIATED = "error.delete.pharmacist.associated";
	String ERROR_PHARMACIST_ID_NOT_IN_DB = "error.pharmacist.id.not.in.db";
	String ERROR_USER_NOT_FOUND = "error.user.not.found";
	
	String SUCCESS_PRESCRIPTION_DELETED = "success.delete.prescription";
	String ERROR_AWS_UPLOAD_INVALID_FILE_FORMAT = "error.aws.file.upload.invalid.format";
	String ERROR_EMAIL_NOT_EXIST = "error.email.not.exist";
	String ERROR_PATIENT_EMAIL_ALREADY_MAPPED = "error.patient.email.already.mapped";
	
}
