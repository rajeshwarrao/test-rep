package com.sg.dayamed.service;

import com.sg.dayamed.entity.DeviceStatus;
import com.sg.dayamed.pojo.DeviceStatusRequestModel;

public interface DeviceStatusService {

	public DeviceStatus addDeviceStatus(DeviceStatusRequestModel deviceStatusRequestModel) throws Exception;

	public DeviceStatus fetchDeviceStatusById(long deviceStatusId);

	public DeviceStatus fetchDeviceStatusByUserID(long userId);

	public void deleteDeviceStatus(long deviceStatusId);
}
