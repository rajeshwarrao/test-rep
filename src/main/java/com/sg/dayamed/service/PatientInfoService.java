package com.sg.dayamed.service;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.helper.pojo.PatientAssociatedActorsPojo;
import com.sg.dayamed.pojo.PatientModel;

public interface PatientInfoService {

	List<PatientModel> fetchPatientsByProviderId(long id);

	List<PatientModel> fetchPatientsByCaregiverId(long id);

	Patient fetchPatientById(long id);

	Patient directFetchPatientById(long id);

	List<Patient> fetchAllPatients();

	List<Patient> fetchEncryptPatientsByProviderId(long providerid);

	PatientModel addPatient(Patient patient);

	PatientModel updatePatient(Patient patient);

	PatientModel updatePatientAlone(Patient patient);

	Patient findByUserDetails_id(long userid);

	Patient fetchByDeviceId(String deviceId);

	PatientAdherencePojo fetchAdherenceByPatientId(long id);

	Patient fetchPatientByUserId(long userID);

	Patient directFetchPatientByPatientId(long id);

	Patient noDecrypCargiverfetchPatientById(long id);

	List<Patient> directFetchPatientsByCaregiverId(long id);

	Patient decryptPatientProviderfetchPatientById(long id);

	String deletePatientByProcedure(long patientId);

	List<PatientModel> fetchPatietnsByProviderId(HttpServletRequest request, String providerid,
	                                             Authentication authentication) throws Exception;

	List<PatientModel> fetchMissedAdherencePatietnsByProviderId(Authentication authentication,
	                                                            HttpServletRequest request) throws Exception;

	List<PatientModel> fetchPatietnsByProviderIdFromCookie(Authentication authentication,
	                                                       HttpServletRequest request) throws Exception;

	List<PatientModel> fetchAllPatients(Authentication authentication, HttpServletRequest request)
			throws Exception;

	PatientModel fetchPatientById(String id, Authentication authentication, HttpServletRequest request)
			throws Exception;

	PatientModel updatePatient(PatientModel patientModelEncrypt, MultipartFile profileImage,
	                           HttpServletRequest request, Authentication authentication) throws Exception;

	PatientModel addPatient(PatientModel patientModel, MultipartFile profileImage, HttpServletRequest request,
	                        Authentication authentication) throws Exception;

	String deletePatient(String id, HttpServletRequest request, Authentication authentication) throws Exception;

	PatientAssociatedActorsPojo fetchProvidersByPatientId(String patientid) throws Exception;
}
