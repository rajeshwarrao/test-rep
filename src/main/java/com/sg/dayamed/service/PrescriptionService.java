package com.sg.dayamed.service;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.security.core.Authentication;

import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.pojo.DeletePrescriptionVO;
import com.sg.dayamed.helper.pojo.GenericIDAndZoneVO;
import com.sg.dayamed.helper.pojo.UpdatePrescriptionStatusVO;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

public interface PrescriptionService {

	 PrescriptionModel addPrescription(PrescriptionModel prescriptionModel, long pat_id,
			JwtUserDetails jwtUser)throws DataValidationException;

	 PrescriptionModel validateFetchLatestPrescriptionByPatientId(long patientId, Authentication authentication) throws ParseException,
	                                                                                                                                                      DataValidationException;
	 List<PrescriptionModel> fetchActivePrescriptionListByPatientId(GenericIDAndZoneVO genericIDAndZoneVO,Authentication authentication) throws ApplicationException, DataValidationException;
	
	 Prescription fetchLatestPrescriptionByPatientId(long patientId);

	 List<PrescriptionModel> fetchPrescriptionListByPatientId(long id,Authentication authentication)throws DataValidationException;

	 boolean createSchedulerJobsByPrescription(long prescriptionId, String zoneid)throws DataValidationException;
	
	 PrescriptionModel validateFetchPrescriptionByPrescriptionId(long id, Authentication authentication)throws DataValidationException;

	 PrescriptionModel fetchPrescriptionByPrescriptionId(long id, long userid)throws DataValidationException ;

	 Prescription fetchPrescriptionEntityByPrescriptionId(long id);

	 boolean generatePrescriptionSchedule(long prescriptionId, String zoneid)throws DataValidationException;

	 List<Prescription> fetchPrescriptionsByPatientId(long patientId);

	 Prescription addPrescription(Prescription prescription);

	 List<Prescription> fetchActivePrescriptionsByPatientId(long patientId, LocalDateTime currentDate);

	 String deletePrescriptionBySP(long presId);
	 
	 PrescriptionModel updatePrescriptionStatus(PrescriptionModel prescriptionModel, Authentication authentication);
	
	 String updatePrescriptionStatusByStatusString(UpdatePrescriptionStatusVO prescriptionModel);
			
	 DeletePrescriptionVO deletePrescriptionById(long prescriptionId);
} 
