package com.sg.dayamed.service;

public interface GoodShapredAlertProtocal {

	public String sendMissedAdherenceNotification();

}
