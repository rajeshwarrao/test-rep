package com.sg.dayamed.service;

import com.sg.dayamed.pojo.AdherenceReportResModel;

import java.util.List;

public interface AdherenceReportService {

	public AdherenceReportResModel getAdherenceReport(List<Long> providerIds, String recipientEmail);
}
