package com.sg.dayamed.service;

import com.sg.dayamed.entity.DosageInfo;

public interface DosageInfoService {

	public DosageInfo updateDosageInfo(DosageInfo dosageInfo);

	public DosageInfo fetctDosageInfoByID(long dosageInfoId);

	public DosageInfo fetchMaxDurationDosageByPrescription_id(long prescriptionId);
}
