package com.sg.dayamed.service;

import com.sg.dayamed.entity.AdherenceDataPoints;

import java.time.LocalDateTime;
import java.util.List;

public interface AdherenceService {

	public AdherenceDataPoints addAdherence(AdherenceDataPoints adherence, String prescribedTime);

	public AdherenceDataPoints fetchAdherenceById(long id);

	//public List<AdherenceDataPoints> addAdherenceList(List<AdherenceDataPoints> adherence);
	public List<AdherenceDataPoints> findByDosageInfo_idIn(List<Long> dosageinfoidlist);

	public List<AdherenceDataPoints> findByprescriptionID(long prescriptionID);

	public AdherenceDataPoints findByPrescribedTimeAndprescriptionID(LocalDateTime PrescribedTime,
	                                                                 long prescriptionID);

	//findByPrescribedTimeBetweenAndPrescriptionID
	public List<AdherenceDataPoints> findByPrescribedTimeAndBetweenTimes(LocalDateTime startPrescribedTime,
	                                                                     LocalDateTime endPrescribedTime,
	                                                                     long prescriptionID);

	List<AdherenceDataPoints> findAdherencesBetweenPrescribedTime(LocalDateTime startPrescribedTime,
	                                                              LocalDateTime endPrescribedTime,
	                                                              List<Long> prescriptionIDs);
} 
