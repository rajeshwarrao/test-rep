package com.sg.dayamed.service;

import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import java.util.List;

public interface PharmacistService {

	List<PharmacistModel> fetchPharmacistListByPatientId(Long patientId) throws DataValidationException;

	PharmacistModel fetchPharmacistByUserId(Long userId) throws DataValidationException;

	List<PatientModel> fetchPatientsByPharmacistId(long id);

	PharmacistModel addPharmacist(Pharmacist pharmacist);

	Pharmacist fetchPharmacistById(long pharmacistId);

	Pharmacist directFetchPharmacistById(long pharmacistId);

	Pharmacist directFetchPharmacistByUserId(long userId);

	List<PatientModel> getPatientsByPharmacistIdentifier(JwtUserDetails jwtUser) throws DataValidationException;
}
