package com.sg.dayamed.service;

import com.sg.dayamed.pojo.ConsumerGoodsModel;

import java.util.List;

public interface ConsumerGoodsService {

	List<ConsumerGoodsModel> getConsumerGoodsByType(String type);

}
