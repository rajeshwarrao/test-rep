package com.sg.dayamed.service;

import com.sg.dayamed.entity.PrescriptionSchedule;

import java.time.LocalDateTime;
import java.util.List;

public interface PrescriptionScheduleService {

	//public List<PrescriptionSchedule> fetchJobsByUserId(Long userId);
	public List<PrescriptionSchedule> fetchJobsByUserIdAndPrescriptionId(Long userID, Long prescriptionId);

	public boolean updatePrescriptionScheduleByDeviceInfo(LocalDateTime ldtPrescribedDatetimeInUTC, Long infoId,
	                                                      String consumptionStatus);

	public boolean updatePrescriptionScheduleByDosageInfo(LocalDateTime ldtPrescribedDatetimeInUTC, Long infoId,
	                                                      String consumptionStatus);

	public List<PrescriptionSchedule> fetchJobsByDosageInfoIds(List<Long> dosageInfoIds);
}
