package com.sg.dayamed.service;

import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface CaregiverService {

	
	List<CaregiverModel> fetchCaregivers();

	Caregiver fetchCaregiverById(long id)throws DataValidationException ;

	Set<CaregiverModel> addCaregiverToPatient(CaregiverModel caregiverModel, long patientid)
			throws DataValidationException;

	CaregiverModel addCaregiver(CaregiverModel caregiverModel, JwtUserDetails jwtUser);

	List<CaregiverModel> fetchAdminAndProviderCargivers(long providerid);

	String deleteCaregiver(long caregiverId) throws DataValidationException;

	Caregiver fetchCaregiverByUserId(long userId)throws DataValidationException;

	List<CaregiverModel> fetchCaregiversByPatientId(long patientid);

	Caregiver directFetchCaregiverById(long id);

	Set<CaregiverModel> addCaregiverFromApp(CaregiverModel caregiverModel, MultipartFile profileImage) throws Exception;
	
	List<PatientModel> fetchPatietnsByCaregiverIdFromJwtToken(Authentication authentication)throws DataValidationException;

	List<PatientModel> fetchPatietnsAloneByCaregiver(Authentication authentication)throws DataValidationException;

	CaregiverModel addCaregiverFromWebValidate(CaregiverModel caregivermodel, MultipartFile profileImage,
			JwtUserDetails jwtUser) throws DataValidationException;

	String mapPatientToCaegiver(String patientemailid,Long caregiverId)throws DataValidationException ;
	
	CaregiverModel updateCaregiverFromApp(CaregiverModel caregivermodel,MultipartFile profileImage) throws DataValidationException;

	Set<CaregiverModel> createCaregiverAssociatedwithPatient(CaregiverModel caregiverModel,Long patientid, final MultipartFile profileImage) throws DataValidationException;
	
	List<PatientModel> fetchPatientsByCaregiverId(Long cargiverid,Authentication authentication) throws DataValidationException;
	
	List<PrescriptionModel> fetchPrescriptionsByCaregiverId(Long cargiverid);
	
	List<CaregiverModel> fetchAdminAndSpecificProviderCargivers(Long userid, Authentication authentication);
	
	

}
