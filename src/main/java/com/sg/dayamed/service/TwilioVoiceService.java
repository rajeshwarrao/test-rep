package com.sg.dayamed.service;

import com.sg.dayamed.util.TwilioVoiceModel;

import com.twilio.twiml.Client;
import com.twilio.twiml.Dial;
import com.twilio.twiml.TwiMLException;
import com.twilio.twiml.VoiceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("TwilioVoiceService")
public class TwilioVoiceService {

	@Autowired
	TwilioVoiceTokenGenerator tokenGenerator;

	public TwilioVoiceModel generateVoiceToken(String clientIdentity) {
		TwilioVoiceModel voiceModel = new TwilioVoiceModel();
		tokenGenerator.setIdentity(clientIdentity);
		tokenGenerator.setCapabilityBuilder();
		String token = tokenGenerator.generateToken();
		voiceModel.setIdentity(clientIdentity);
		voiceModel.setToken(token);
		return voiceModel;
	}

	public String connectVoiceCall(String clientIdentity) throws TwiMLException {
		Dial.Builder dialBuilder = new Dial.Builder();
		Client client = new Client.Builder(clientIdentity).build();
		dialBuilder = dialBuilder.client(client);
		Dial dial = dialBuilder.build();
		VoiceResponse twimlResponse = new VoiceResponse.Builder().dial(dial)
		                                                         .build();
		return twimlResponse.toXml();
	}
}
