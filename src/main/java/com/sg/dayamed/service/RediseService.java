package com.sg.dayamed.service;

import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;

public interface RediseService {

	public String saveDataInRedis(String jwtTokenKey, JwtUser jwtUser);

	public JwtUser getDataInRedis(String jwtTokenKey, boolean updateSessionExpire);

	public void setExpireTime(String jwtTokenKey, JwtUser jwtUser);

	public boolean removeTokenFromCache(String token);

	public String saveVedioCallRecevierNotifacation(String recevierUserId,
	                                                VidoeCallSignatureModel vidoeCallSignatureModel);

	public VidoeCallSignatureModel getVedioCallRecevierNotifacation(String recevierUserId);

	public Object getDataInRedisByKey(String key);

	public Object getDataInRedisByKeyOrFail(String key);

	public String configParamsUpdateInRedis(String keyObj, Object valueObject);

	public String saveObjectWithExpire(String keyObject, Object valueObj, int expireTime);
}
