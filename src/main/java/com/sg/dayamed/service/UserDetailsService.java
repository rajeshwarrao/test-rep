package com.sg.dayamed.service;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.pojo.ResultVO;
import com.sg.dayamed.pojo.PasswordChangeReqModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;

import java.util.List;

public interface UserDetailsService {

	public UserDetailsModel findByEmailIdIgnoreCase(String emailId);

	public UserDetailsModel updateUserDetails(UserDetailsModel userDetailsModel);

	public UserDetailsModel fetchUserDetailsById(Long userid);

	public List<UserDetails> findUserDetailsByIdInAndRole(List<Long> caregiverIds, String role);

	public List<UserDetailsModel> fetchUserDetailsByToken(String token);

	public String resetPassword(String tokenId, String password);

	public String forgotPassword(String emailId);

	public UserDetails save(UserDetails userDetails);

	public UserDetails fetchDecryptedUserDetailsById(Long userid);

	public UserDetails fetchUserDetailEntitiysById(Long userid);

	public UserPreferenceModel findByEmailId(String emailId);

	public List<String> findByEmailIdIn(List<String> emailIds);

	public Boolean findByPhoneNumber(String phoneNumbers);

	public Boolean findByToken(String token);

	public List<ResultVO> findByEmailIdInResultVo(List<String> emailIds);

	public void changePassword(PasswordChangeReqModel passwordChangeReqModel, Long userId)
			throws DataValidationException;
}
