package com.sg.dayamed.service;

import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;

import com.imantics.akp.sdk.pid.model.PidDataTable;
import com.imantics.dm.api.DmPatientDataApi;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ImanticService {

	public String getImanticId();

	//public Double getPredictedAdherenceByDemographicdataAndImanticID(Patient patient,String imanticId);
	public Map<String, String> createPatientInImantics(Patient patient);

	//public void updateDemographicData(DmPatientDataModel dmPatientDataModel,Patient patient);
	public String createPrescription(Prescription prescription);

	public boolean medicationTaken(AdherenceDataPoints adherence, int flag, String prescribedTime);

	public boolean biometricEvent(Object obj, LocalDateTime prescribedTime, LocalDateTime takenDateTime);

	public boolean deletePatientByImanticId(String imanticId);

	public boolean deletePatientBypatientId(String patientId);

	public List<PidDataTable> fetchBiometricData(String patientId, Date startDate, Date endDate,
	                                             ArrayList<String> biometricNameList);

	public List<PidDataTable> fetchPredictedBiometricData(String patientId, Date startDate, Date endDate,
	                                                      ArrayList<String> biometricNameList);
	//public  Map<String,String>  fetchAnalytics( String patientId,Date oldDate,Date endDate, ArrayList<String>
	// analyticNameList);

	// public  Map<String,Map<String,String>>  fetchAnalyticsMap( String patientId,Date oldDate,Date endDate,
	// ArrayList<String> analyticNameList);
	public Map<String, List<Map<String, String>>> fetchAnalyticsMap(String patientId, Date oldDate, Date endDate,
	                                                                ArrayList<String> analyticNameList);

	public DmPatientDataApi updateDemographicData(Patient persistedpatient);

	//latest api from imantics
	public Map<String, String> fetchAnalyticsMap(String imanticId);
}
