package com.sg.dayamed.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.ConsumerGoodInfo;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.DiseaseInfo;
import com.sg.dayamed.entity.DosageDevice;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.repository.ConsumerGoodsInfoRepository;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.repository.DeviceNotificationRepository;
import com.sg.dayamed.repository.DiseaseInfoRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.DosageDeviceRepository;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.repository.GlucoMeterRepository;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionScheduleService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.Utility;

@Service
public class PatientServiceImpl implements PatientService {

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ProviderRepository providerRepository;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	Utility utility;

	@Autowired
	PrescriptionStatusRepository prescriptionStatusRepository;

	@Autowired
	DiseaseInfoRepository diseaseInfoRepository;

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	@Autowired
	DeviceInfoRepository deviceInfoRepository;

	@Autowired
	ConsumerGoodsInfoRepository consumerGoodsInfoRepository;

	@Autowired
	HeartRateRepository heartRateRepository;

	@Autowired
	GlucoMeterRepository glucoMeterRepository;

	@Autowired
	BpMonitorRepository bpMonitorRepository;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	DiseaseRepository diseaseRepository;

	@Autowired
	ImanticService imanticService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DosageNotificationRepository dosageNotificationRepository;

	@Autowired
	DeviceNotificationRepository deviceNotificationRepository;

	@Autowired
	SendResetPwdNotification sendPwdResetNtf;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	NotificationScheduleService notificationScheduleService;

	@Autowired
	PrescriptionScheduleService prescriptionScheduleService;

	@Autowired
	NotificationScheduleRepository notificationScheduleRepository;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	@Autowired
	PasswordEncryption pwdEncryption;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;

	/*
	 * @Autowired BCryptPasswordEncoder bCryptPasswordEncoder;
	 */

	private static final Logger logger = LoggerFactory.getLogger(PatientServiceImpl.class);

	public String deletePatientByProcedure(long patientId) {
		String restStatus = "success";

		Optional<Patient> optionalPatient = patientRepository.findById(patientId);
		if (optionalPatient.isPresent() && optionalPatient.get()
		                                                  .getUserDetails()
		                                                  .getImanticUserid() != null) {
				logger.info("Patient going to be deleted in local DB");
				try {
					restStatus = patientRepository.deletePatient(patientId);
					if (StringUtils.isNotBlank(optionalPatient.get()
					                                          .getUserDetails()
					                                          .getImanticUserid())) {
						imanticService
								.deletePatientByImanticId(optionalPatient.get()
								                                         .getUserDetails()
								                                         .getImanticUserid());
					}
				} catch (Exception e) {
					logger.error("Exception occurred when deleting patient in local db", e);
				}
				return restStatus;
		}
		return "success";
	}

	@Override
	public String deletePatient(long id) {

		Optional<Patient> optionalPatient = patientRepository.findById(id);
		if (optionalPatient.isPresent() && optionalPatient.get()
		                                                  .getUserDetails()
		                                                  .getImanticUserid() != null) {

			boolean result = imanticService
					.deletePatientByImanticId(optionalPatient.get()
					                                         .getUserDetails()
					                                         .getImanticUserid());
			//patientRepository.delete(id);
			// logger.info("patient got deleted in local DB");
			//patientRepository.deleteById(id);
			//logger.info("patient got deleted in local DB");
			if (result == true) {
				patientRepository.deleteById(id);
				logger.info("patient got deleted in local DB");
			} else {
				logger.info("problem faced while deleting patient in Imantic DB");
				//return "failed";
			}
		}
		return "success";
	}

	@Transactional
	@Override
	public PatientModel addPatient(Patient patient) {

		// creating patient in ImanticDB and getting back imanticId of that
		// particular patient
		Map<String, String> imanticMap = imanticService.createPatientInImantics(patient);
		// Add Imantics ID to Local patient record and use it to update the
		// patient data on Imantics.
		Patient persistedpatient = null;
		if (imanticMap != null && imanticMap.size() > 0) {
			patient.getUserDetails()
			       .setImanticUserid(imanticMap.get("imanticid"));

			Map<String, String> analyticMap = imanticService.fetchAnalyticsMap(patient.getUserDetails()
			                                                                          .getImanticUserid());

			patient.setPredictedAdherence(analyticMap.get("predictedAdherence"));
			patient.setCurrentAdherence(analyticMap.get("currentAdherence"));
			patient.setProjectedAdherence(analyticMap.get("projectedAdherence"));
			if (analyticMap.get("emoji") != null) {
				patient.setEmoji(analyticMap.get("emoji"));
			}
			if (patient.getUserDetails()
			           .getCreateOn() == null) {
				patient.getUserDetails()
				       .setCreateOn(Date.from(Instant.now()));
			} else {
				patient.getUserDetails()
				       .setUpdateOn(Date.from(Instant.now()));
			}
			if (patient.getUserDetails()
			           .getCreateOn() == null) {
				patient.getUserDetails()
				       .setCreateOn(Date.from(Instant.now()));
			} else {
				patient.getUserDetails()
				       .setUpdateOn(Date.from(Instant.now()));
			}
			String pwdChangeToken = pwdEncryption.encrypt(patient.getUserDetails()
			                                                     .getEmailId() + System.currentTimeMillis());
			UserDetails nonEncryptUserDetails = patient.getUserDetails();
			boolean emailFlag = true;
			boolean smsFlag = true;
			if (patient.getUserDetails() != null) {
				emailFlag = patient.getUserDetails()
				                   .isEmailFlag();
				smsFlag = patient.getUserDetails()
				                 .isSmsFlag();
			}
			patient.getUserDetails()
			       .setPwdChangeToken(pwdChangeToken);
			patient.getUserDetails()
			       .setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));

			patient.getUserDetails()
			       .setEmailFlag(emailFlag);
			patient.getUserDetails()
			       .setSmsFlag(smsFlag);
			persistedpatient = patientRepository.save(patient);
			logger.info("Patient got created in local DB");
			nonEncryptUserDetails.setPwdChangeToken(pwdChangeToken);
			sendPwdResetNtf.sendPwdResetNotificationWhenRegst(nonEncryptUserDetails, "Patient");
			patient.setUserDetails(nonEncryptUserDetails);
			notificationService.sendMobileNotification(patient.getUserDetails()
			                                                  .getType(),
			                                           "Your profile has been added into Dayamed as patient",
			                                           "Added profile in Dayamed", patient.getUserDetails()
			                                                                                   .getToken());
			Map<String, String> keyVal = new HashMap<String, String>();
			keyVal.put("patientName", patient.getUserDetails()
			                                 .getFirstName() + " " + patient.getUserDetails()
			                                                                .getLastName());
			String userLangPreferencePat = utility.getUserPreferedLang(patient.getId());
			if (patient.getCaregivers() != null) {
				List<String> recipientList = new ArrayList<String>();
				for (Caregiver caregiver : patient.getCaregivers()) {
					String userLangPreference = utility.getUserPreferedLang(caregiver.getUserDetails()
					                                                                 .getId());
					recipientList.add(caregiver.getUserDetails()
					                           .getEmailId());
					if (caregiver.getUserDetails()
					             .getType() != null &&
							(caregiver.getUserDetails()
							          .getType()
							          .equalsIgnoreCase(ApplicationConstants.ANDROID) || caregiver.getUserDetails()
							                                                                      .getType()
							                                                                      .equalsIgnoreCase(
									                                                                      ApplicationConstants.IOS))) {
						notificationService.sendMobileNotification(caregiver.getUserDetails()
						                                                    .getType(),
						                                           "You are now the caregiver for patient :" +
								                                           patient.getUserDetails()
								                                                  .getFirstName() + " " +
								                                           patient.getUserDetails()
								                                                  .getLastName(), "Patient added",
						                                           caregiver.getUserDetails()
						                                                    .getToken());
					}

					String msgSms = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal,
					                                                                              "profile.patient" +
							                                                                              ".added" +
							                                                                              ".caregiver",
					                                                                              userLangPreference, null);
					emailAndSmsUtility.sendSms(msgSms, caregiver.getUserDetails()
					                                            .getCountryCode(), caregiver.getUserDetails()
					                                                                        .getMobileNumber());
				}
				String msgSms = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal,
				                                                                              "profile.patient.added" +
						                                                                              ".caregiver",
				                                                                              userLangPreferencePat, null);
				String emailSubject =
						messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "emailsubject" +
								                                                              ".patientadded",
						                                                              userLangPreferencePat, null);
				emailAndSmsUtility.sendEmail(emailSubject, msgSms, recipientList);
				//emailAndSmsUtility.sendEmail("Patient added", "You are now the caregiver for patient :"+patient
				// .getUserDetails().getFirstName()+" "+patient.getUserDetails().getLastName(), recipientList);
			}

		} else {
			return null;
		}
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(persistedpatient);
		//decrypting patient data to save in DB
		return patientmodel;

	}

	// @Transactional
	@Override
	public PrescriptionModel addPrescription(Prescription prescription, long patientid, long userid) {

		try {
			Optional<Patient> optionalPatient = patientRepository.findById(patientid);
			if (optionalPatient.isPresent()) {
				prescription.setPatient(optionalPatient.get());
			}

			// should delete updated dosage and device list in updated prescription
			/*
			 * //not working this way List<DosageInfo> olddosageInfoList =
			 * dosageInfoRepository.findByPrescription_id(prescription.getId());
			 * olddosageInfoList.forEach(olddosageInfo ->{
			 * if(!prescription.getDosageInfoList().contains(olddosageInfo)){
			 * deltedosageInfoList.add(olddosageInfo); } )
			 */
			List<Long> newdosageInfoidsList = new ArrayList<Long>();
			List<Long> newdeviceInfoidsList = new ArrayList<Long>();
			List<Long> newconsumergoodsInfoidsList = new ArrayList<Long>();
			List<Long> newdiseaseInfoidsList = new ArrayList<Long>();
			// new
			Map<Long, List<Long>> newdosageNotificationMap = new HashMap<Long, List<Long>>();
			Map<Long, List<Long>> newdeviceNotificationMap = new HashMap<Long, List<Long>>();

			// getting only disease id in dieseaseinfo list. So have to fetch
			// diseaseinfo object based on id then has to set.
			for (DiseaseInfo diseaseInfo : prescription.getDiseaseInfoList()) {
				Optional<Disease> optionalDisease = diseaseRepository.findById(diseaseInfo.getDisease()
				                                                                          .getId());
				if (optionalDisease.isPresent()) {
					diseaseInfo.setDisease(optionalDisease.get());
				}
			}
			// This loop for setting prescription for respective infos
			prescription.getDiseaseInfoList()
			            .forEach(diseaseInfo -> {
				            newdiseaseInfoidsList.add(diseaseInfo.getId());
				            diseaseInfo.setPrescription(prescription);
			            });
			prescription.getDosageInfoList()
			            .forEach(dosageInfo -> {
				            dosageInfo.setDeleteflag(false);
				            newdosageInfoidsList.add(dosageInfo.getId());
				            dosageInfo.setPrescription(prescription);
				            if (dosageInfo.getDuration() <= 0) {
					            dosageInfo.setDuration(1);
				            }

				            if (dosageInfo.getDosageNotificationList() != null) {
					            long dosageInfoId = dosageInfo.getId();
					            List<DosageNotification> dosageNotificationList = dosageNotificationRepository
							            .findByDosageInfo_idAndUserIdNot(dosageInfoId, userid);
					            dosageInfo.getDosageNotificationList()
					                      .addAll(dosageNotificationList);
					            List<Long> newDosageInfoNotificationidsList = new ArrayList<Long>();
					            dosageInfo.getDosageNotificationList()
					                      .forEach(dosageNotification -> {
						                      if (dosageNotification.getUserId() == 0 ||
								                      dosageNotification.getUserId() == userid) {
							                      dosageNotification.setUserId(userid);
						                      }
						                      newDosageInfoNotificationidsList.add(dosageNotification.getId());
					                      });
					            newdosageNotificationMap.put(dosageInfo.getId(), newDosageInfoNotificationidsList);
				            }

			            });
			// prescription.getDeviceInfoList().forEach(deviceInfo -> {
			for (DeviceInfo deviceInfo : prescription.getDeviceInfoList()) {
				deviceInfo.setDeleteflag(false);
				newdeviceInfoidsList.add(deviceInfo.getId());
				deviceInfo.setPrescription(prescription);
				if (deviceInfo.getDuration() <= 0) {
					deviceInfo.setDuration(1);
				}
				if (deviceInfo.getDeviceNotificationList() != null) {
					List<Long> newDeviceNotificationidsList = new ArrayList<Long>();
					deviceInfo.getDeviceNotificationList()
					          .forEach(deviceNotification -> {
						          newDeviceNotificationidsList.add(deviceNotification.getId());
					          });
					newdeviceNotificationMap.put(deviceInfo.getId(), newDeviceNotificationidsList);
				}
			}
			// });
			prescription.getCommodityInfoList()
			            .forEach(commodityInfo -> {
				            commodityInfo.setDeleteflag(false);
				            newconsumergoodsInfoidsList.add(commodityInfo.getId());
				            commodityInfo.setPrescription(prescription);
			            });

			List<DiseaseInfo> olddiseaseInfoList = diseaseInfoRepository.findByPrescription_id(prescription.getId());
			List<DosageInfo> olddosageInfoList = dosageInfoRepository.findByPrescription_id(prescription.getId());
			List<DeviceInfo> olddeviceInfoList = deviceInfoRepository.findByPrescription_id(prescription.getId());
			List<ConsumerGoodInfo> oldconsumerGoodsInfoList = consumerGoodsInfoRepository
					.findByPrescription_id(prescription.getId());

			olddiseaseInfoList.forEach(oldDisease -> {
				if (!newdiseaseInfoidsList.contains(oldDisease.getId())) {
					oldDisease.setDeleteflag(true);
					diseaseInfoRepository.save(oldDisease);// for softdelete
				}
			});
			for (DosageInfo oldDosage : olddosageInfoList) {
				List<Long> oldDosageInfoNotificationIdsList = new ArrayList<Long>();
				if (oldDosage.getDosageNotificationList() != null) {
					oldDosage.getDosageNotificationList()
					         .forEach(oldDosageInfoNotification -> {
						         oldDosageInfoNotificationIdsList.add(oldDosageInfoNotification.getId());
					         });
				}

				if (!newdosageInfoidsList.contains(oldDosage.getId())) {
					for (Long id : oldDosageInfoNotificationIdsList) {
						dosageNotificationRepository.delete(id);// hibernate issue
						// it is not
						// deleting in same
						// transction
					}
					// new end
					oldDosage.setDeleteflag(true);
					dosageInfoRepository.save(oldDosage);// for softdelete
				} else {
					// have to check dosagenotificationilist individually to delete.
					List<Long> newdosageNotificationslist = newdosageNotificationMap.get(oldDosage.getId());
					if (newdosageNotificationslist != null) {
						for (Long olddosageNotificationID : oldDosageInfoNotificationIdsList) {
							if (!newdosageNotificationslist.contains(olddosageNotificationID)) {
								dosageNotificationRepository.delete(olddosageNotificationID);
							}
						}
					}
				}

			} // end for dosageinfo
			// });
			// olddeviceInfoList.forEach(oldDevice -> {
			for (DeviceInfo oldDevice : olddeviceInfoList) {
				List<Long> oldDeviceInfoNotificationIdsList = new ArrayList<Long>();
				if (oldDevice.getDeviceNotificationList() != null) {
					oldDevice.getDeviceNotificationList()
					         .forEach(oldDeviceInfoNotification -> {
						         oldDeviceInfoNotificationIdsList.add(oldDeviceInfoNotification.getId());
					         });
				}
				if (!newdeviceInfoidsList.contains(oldDevice.getId())) {
					for (Long id : oldDeviceInfoNotificationIdsList) {
						deviceNotificationRepository.delete(id);// hibernate issue
						// it is not
						// deleting in same
						// transction
					}
					oldDevice.setDeleteflag(true);
					deviceInfoRepository.save(oldDevice);// for softdelete
				} else {
					// have to check devicenotificationilist individually to delete.
					List<Long> newdeviceNotificationslist = newdeviceNotificationMap.get(oldDevice.getId());
					if (newdeviceNotificationslist != null) {
						for (Long olddeviceNotificationID : oldDeviceInfoNotificationIdsList) {
							if (!newdeviceNotificationslist.contains(olddeviceNotificationID)) {
								deviceNotificationRepository.delete(olddeviceNotificationID);
							}
						}
					}
				}
			}
			// });
			oldconsumerGoodsInfoList.forEach(oldConsumerGood -> {
				if (!newconsumergoodsInfoidsList.contains(oldConsumerGood.getId())) {
					oldConsumerGood.setDeleteflag(true);
					consumerGoodsInfoRepository.save(oldConsumerGood);
					//for softdelete
				}
			});

			//String predictedAdherence = imanticService.createPrescription(prescription);
			String result = imanticService.createPrescription(prescription);
			if (result != null && result.equalsIgnoreCase(ApplicationConstants.SUCCESS) &&
					prescription.getPatient() != null && prescription.getPatient()
					                                                 .getUserDetails() != null) {

				UserDetails userDetails = prescription.getPatient()
				                                      .getUserDetails();
				Patient patient = prescription.getPatient();
				Map<String, String> analyticMap = imanticService.fetchAnalyticsMap(userDetails.getImanticUserid());

				patient.setPredictedAdherence(analyticMap.get("predictedAdherence"));
				patient.setCurrentAdherence(analyticMap.get("currentAdherence"));
				patient.setProjectedAdherence(analyticMap.get("projectedAdherence"));
				if (analyticMap.get("emoji") != null) {
					patient.setEmoji(analyticMap.get("emoji"));
				}
				prescription.setPatient(patient);
				Prescription persistedPrescription = prescriptionRepository.save(prescription);
				if (persistedPrescription != null && prescription.getId() > 0) {
					//we are removing jobs after updating prescription
					//delete all scheduled things based on user id and PrescriptionId, and then need to generate again.
					List<PrescriptionSchedule> existedPrescriptionScheduleList =
							prescriptionScheduleService.fetchJobsByUserIdAndPrescriptionId(prescription.getPatient()
							                                                                           .getUserDetails()
							                                                                           .getId(),
							                                                               prescription.getId());
					if (CollectionUtils.isNotEmpty(existedPrescriptionScheduleList)) {
						prescriptionScheduleRepository.deleteAll(existedPrescriptionScheduleList);
					}
					List<NotificationSchedule> existedSchedulerJobList =
							notificationScheduleService.fetchJobsByUserIdAndPrescriptionId(prescription.getPatient()
							                                                                           .getUserDetails()
							                                                                           .getId(),
							                                                               prescription.getId());
					if (CollectionUtils.isNotEmpty(existedSchedulerJobList)) {
						notificationScheduleRepository.deleteAll(existedSchedulerJobList);
					}

					//deleting jobs by dosageInfo ID commented
						/*List<Long> dosageInfoIds = new ArrayList<Long>();
						persistedPrescription.getDosageInfoList().forEach(dosageInfo ->{
							dosageInfoIds.add(dosageInfo.getId());
						});
						if(CollectionUtils.isNotEmpty(dosageInfoIds)){
							List<PrescriptionSchedule> existedprescriptionScheduleList = prescriptionScheduleService
							.fetchJobsByDosageInfoIds(dosageInfoIds);
							if(CollectionUtils.isNotEmpty(existedprescriptionScheduleList)){
							prescriptionScheduleRepository.deleteAll(existedprescriptionScheduleList);
							}
							List<NotificationSchedule> existedSchedulerJobList = notificationScheduleService
							.fetchJobsByDosageInfoIds(dosageInfoIds);
							if(CollectionUtils.isNotEmpty(existedSchedulerJobList)){
								notificationScheduleRepository.deleteAll(existedSchedulerJobList);
							}
						}*/
				}
				// To serilize and return model data
				PrescriptionModel prescriptionModel = utility.convertPrescriptionEntityToModel(persistedPrescription);
				return prescriptionModel;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Exception occurred when adding prescription", e);
			return null;
		}
	}

	@Autowired
	DosageDeviceRepository dosageDeviceRepository;

	@Override
	public PrescriptionModel updatePrescription(Prescription prescription) {

		Optional<Patient> optionalPatient = patientRepository.findById(prescription.getPatient()
		                                                                           .getId());
		if (optionalPatient.isPresent()) {
			prescription.setPatient(optionalPatient.get());
		}
		prescription.getDiseaseInfoList()
		            .forEach(diseaseInfo -> {
			            diseaseInfo.setPrescription(prescription);
		            });
		prescription.getDosageInfoList()
		            .forEach(dosageInfo -> {
			            dosageInfo.setPrescription(prescription);
		            });
		prescription.getDeviceInfoList()
		            .forEach(deviceInfo -> {
			            deviceInfo.setPrescription(prescription);
		            });
		prescription.getCommodityInfoList()
		            .forEach(commodityInfo -> {
			            commodityInfo.setPrescription(prescription);
		            });

		// deleting deleted updated dosagedevices.
		List<Long> newDosageDeviceList = new ArrayList<Long>();
		prescription.getDosageDevices()
		            .forEach(dosageDevice -> {
			            newDosageDeviceList.add(dosageDevice.getId());
		            });

		List<DosageDevice> oldDosageDeviceList = dosageDeviceRepository.findByPrescription_id(prescription.getId());
		for (DosageDevice olDosageDevice : oldDosageDeviceList) {
			if (!newDosageDeviceList.contains(olDosageDevice.getId())) {
				dosageDeviceRepository.delete(olDosageDevice.getId());
			}
		}
//		Patient patient = prescription.getPatient();
//		prescription.setPatient(patient);
		Prescription prescriptionpersisted = prescriptionRepository.save(prescription);
		logger.info("Prescription updated successfully in local DB");
		List<DeviceInfo> deviceInfoList = prescriptionpersisted.getDeviceInfoList();
		List<ConsumerGoodInfo> commodityInfoList = prescriptionpersisted.getCommodityInfoList();

		PrescriptionModel prescriptionModel = utility.convertPrescriptionEntityToModel(prescriptionpersisted);

		return prescriptionModel;
	}

	@Override
	public List<PatientModel> fetchPatientsByProviderId(long providerid) {

		List<Patient> patientlist = patientRepository.findByProviders_idAndStatus(providerid, 0);
		List<PatientModel> patientmodels = new LinkedList<PatientModel>();
		Collections.sort(patientlist, new SortPatientsbyUpdatedTime());

		for (Patient patient : patientlist) {
			List<PrescriptionModel> PrescriptionModelList = new ArrayList<PrescriptionModel>();
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);

			for (Prescription prescription : patient.getPrescriptions()) {
				PrescriptionModelList.add(utility
						                          .convertPrescriptionEntityToModel(
								                          utility.fetchUnsoftDeletedPrescriptionInfo(prescription)));
			}

			patientModel.setPrescriptions(PrescriptionModelList);
			patientmodels.add(patientModel);
		}
		 
		 /*patientlist.forEach(patient -> {
			List<PrescriptionModel> PrescriptionModelList = new ArrayList<PrescriptionModel>();
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
			patient.getPrescriptions().forEach(prescription -> {
				PrescriptionModelList.add(utility
						.convertPrescriptionEntityToModel(utility.fetchUnsoftDeletedPrescriptionInfo(prescription)));
			});
			patientModel.setPrescriptions(PrescriptionModelList);
			patientModel.setUserDetails(pHIDataDBEncryption.decryptUserDetails(patientModel.getUserDetails(),
			pHIDataDBEncryption.getDBEncryKey()));
			patientmodels.add(patientModel);
			
		});*/
		return patientmodels;
	}

	class SortPatientsbyUpdatedTime implements Comparator<Patient> {

		public int compare(Patient o1, Patient o2) {
			if (o1.getUserDetails()
			      .getUpdateOn() == null || o2.getUserDetails()
			                                  .getUpdateOn() == null) {
				return 0;
			}
			return o2.getUserDetails()
			         .getUpdateOn()
			         .compareTo(o1.getUserDetails()
			                      .getUpdateOn());
		}

	}

	@Override
	public List<PrescriptionModel> fetchPrescriptionlistByPatientId(long patientid) {
		// prescs
		List<PrescriptionModel> prescriptionmodels = new ArrayList<PrescriptionModel>();
		try {
			List<Prescription> prescriptions = prescriptionRepository.findByPatient_id(patientid);
			for (Prescription prescription : prescriptions) {
				Prescription processedPrescription = utility.fetchUnsoftDeletedPrescriptionInfo(prescription);
				PrescriptionModel prescriptionModel = utility.convertPrescriptionEntityToModel(processedPrescription);
				prescriptionmodels.add(prescriptionModel);
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return prescriptionmodels;
	}

	@Override
	public List<PrescriptionModel> fetchPrescriptionlistByStatus(List<String> status) {
		List<Prescription> prescriptions = prescriptionRepository.findByPrescriptionStatus_nameIn(status);
		// ---conveert to models
		List<PrescriptionModel> prescriptionmodels = new ArrayList<PrescriptionModel>();
		for (Prescription prescription : prescriptions) {
			PrescriptionModel prescriptionModel = utility.convertPrescriptionEntityToModel(prescription);
			prescriptionmodels.add(prescriptionModel);
		}
		return prescriptionmodels;
	}

	@Override
	public Patient fetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		if (optionalPatient.isPresent()) {
			Patient patinet = optionalPatient.get();
			boolean emailFlag = patinet.getUserDetails()
			                           .isEmailFlag();
			boolean smsFlag = patinet.getUserDetails()
			                         .isSmsFlag();
			patinet.getUserDetails()
			       .setEmailFlag(emailFlag);
			patinet.getUserDetails()
			       .setSmsFlag(smsFlag);
			return patinet;
		} else {
			return null;
		}
	}

	@Override
	public Patient directFetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		if (optionalPatient.isPresent()) {
			return optionalPatient.get();
		} else {
			return null;
		}
	}

	@Override
	public Patient decryptPatientProviderfetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		return optionalPatient.isPresent() ? optionalPatient.get() : null;
	}

	public Patient noDecrypCargiverfetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		return optionalPatient.isPresent() ? optionalPatient.get() : null;
	}

	public Patient directFetchPatientByPatientId(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		if (optionalPatient.isPresent()) {
			return optionalPatient.get();
		} else {
			return null;
		}

	}

	@Override
	public Patient findByUserDetails_id(long userid) {
		Patient patient = patientRepository.findByUserDetails_id(userid);
		return patient;
	}

	@Override
	public PatientModel updatePatient(Patient patient) {
		//settint Analytics to patient
		imanticService.updateDemographicData(patient);
		Map<String, String> analyticMap = imanticService.fetchAnalyticsMap(patient.getUserDetails()
		                                                                          .getImanticUserid());

		patient.setPredictedAdherence(analyticMap.get("predictedAdherence"));
		patient.setCurrentAdherence(analyticMap.get("currentAdherence"));
		patient.setProjectedAdherence(analyticMap.get("projectedAdherence"));
		if (analyticMap.get("emoji") != null) {
			patient.setEmoji(analyticMap.get("emoji"));
		}

		if (patient.getUserDetails()
		           .getCreateOn() == null) {
			patient.getUserDetails()
			       .setCreateOn(Date.from(Instant.now()));
		} else {
			patient.getUserDetails()
			       .setUpdateOn(Date.from(Instant.now()));
		}
		UserDetails nonEncryptUserDetails = patient.getUserDetails();
		boolean emailFlag = true;
		boolean smsFlag = true;
		if (patient.getUserDetails() != null) {
			emailFlag = patient.getUserDetails()
			                   .isEmailFlag();
			smsFlag = patient.getUserDetails()
			                 .isSmsFlag();
		}
		patient.getUserDetails()
		       .setEmailFlag(emailFlag);
		patient.getUserDetails()
		       .setSmsFlag(smsFlag);
		patient = patientRepository.save(patient);
		logger.info("Patient got created in local DB");
		//sendPwdResetNtf.sendPwdResetNotificationWhenRegst(nonEncryptUserDetails, "Patient");
		patient.setUserDetails(nonEncryptUserDetails);

		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(patient);
		List<String> recipientList = new ArrayList<String>();

		recipientList.add(patient.getUserDetails()
		                         .getEmailId());

		//emailAndSmsUtility.sendEmail(ApplicationConstants.UPDATED_PROFILE_IN_DAYAMED_TITLE, ApplicationConstants
		// .UPDATED_PROFILE_IN_DAYAMED_BODY, recipientList);
		String userLangPreference = utility.getUserPreferedLang(patient.getUserDetails()
		                                                               .getId());
		String msgRes =
				messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "profile.update.patient.you",
				                                                              userLangPreference, null);
		String emailSubject =
				messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "appconstants.profile.updated",
				                                                              userLangPreference, new Object[]{utility.getProductOwner()});
		emailAndSmsUtility.sendEmail(emailSubject, msgRes, recipientList);
		//emailAndSmsUtility.sendEmail(ApplicationConstants.UPDATED_PROFILE_IN_DAYAMED_TITLE, "profile.update.patient
		// .you", null, recipientList);
		recipientList.clear();
		//notificationService.sendMobileNotification(patient.getUserDetails().getType(), ApplicationConstants
		// .UPDATED_PROFILE_IN_DAYAMED_BODY, ApplicationConstants.UPDATED_PROFILE_IN_DAYAMED_TITLE, patient
		// .getUserDetails().getToken());
		notificationService.sendMobileNotification(patient.getUserDetails()
		                                                  .getType(), msgRes, emailSubject, patient.getUserDetails()
		                                                                                           .getToken());
		//if patient has caregiver
		Map<String, String> msgResourceReplaceKeyVal = new HashMap<String, String>();
		String firstName = patient.getUserDetails()
		                          .getFirstName() != null ? patient.getUserDetails()
		                                                           .getFirstName() : "";
		String lastName = patient.getUserDetails()
		                         .getLastName() != null ? patient.getUserDetails()
		                                                         .getLastName() : "";
		String patientName = firstName + " " + lastName;
		msgResourceReplaceKeyVal.put("patientName", patientName);
		if (patient != null && patient.getCaregivers() != null) {
			for (Caregiver caregiver : patient.getCaregivers()) {
				userLangPreference = utility.getUserPreferedLang(caregiver.getUserDetails()
				                                                          .getId());
				recipientList.add(caregiver.getUserDetails()
				                           .getEmailId());
				//sending sms too				
				msgRes = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal,
				                                                                       "profile.update.patient" +
						                                                                       ".profile",
				                                                                       userLangPreference, null);
				emailAndSmsUtility.sendSms(msgRes, caregiver.getUserDetails()
				                                            .getCountryCode(), caregiver.getUserDetails()
				                                                                        .getMobileNumber());
				//emailAndSmsUtility.sendSms("Patient :"+patient.getUserDetails().getFirstName()+" "+patient
				// .getUserDetails().getLastName()+" profile has been updated", caregiver.getUserDetails()
				// .getCountryCode(),caregiver.getUserDetails().getMobileNumber());
				if (caregiver.getUserDetails()
				             .getType() != null &&
						(caregiver.getUserDetails()
						          .getType()
						          .equalsIgnoreCase(ApplicationConstants.ANDROID) || caregiver.getUserDetails()
						                                                                      .getType()
						                                                                      .equalsIgnoreCase(
								                                                                      ApplicationConstants.IOS))) {
					//notificationService.sendMobileNotification(caregiver.getUserDetails().getType(), "Patient
					// :"+patient.getUserDetails().getFirstName()+" "+patient.getUserDetails().getLastName()+" profile
					// has been updated", ApplicationConstants.UPDATED_PATIET_PROFILE_IN_DAYAMED_TITLE, caregiver
					// .getUserDetails().getToken());
					notificationService.sendMobileNotification(caregiver.getUserDetails()
					                                                    .getType(), msgRes,
					                                           messageNotificationConstants.getUserPreferedLangTranslatedMsg(
							                                           null,
							                                           ApplicationConstants.UPDATED_PATIET_PROFILE_IN_DAYAMED_TITLE,
							                                           userLangPreference, null), caregiver.getUserDetails()
					                                                                                       .getToken());
				}
			}
			emailAndSmsUtility.sendEmail("appconstants.profile.updated", "profile.update.patient.profile",
			                             msgResourceReplaceKeyVal, recipientList);
			//emailAndSmsUtility.sendEmail(ApplicationConstants.UPDATED_PATIET_PROFILE_IN_DAYAMED_TITLE, "Patient
			// :"+patient.getUserDetails().getFirstName()+" "+patient.getUserDetails().getLastName()+" profile has
			// been updated", recipientList);
			recipientList.clear();
		}
		emailAndSmsUtility.sendSms(msgRes, patient.getUserDetails()
		                                          .getCountryCode(), patient.getUserDetails()
		                                                                    .getMobileNumber());
		return patientmodel;
	}

	@Override
	public List<PatientModel> fetchPatientsByCaregiverId(long id) {
		List<Patient> patientlist = patientRepository.findByCaregivers_id(id);
		List<PatientModel> patientmodels = new ArrayList<PatientModel>();
		patientlist.forEach(patient -> {
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
			patientmodels.add(patientModel);
		});
		return patientmodels;
	}

	@Override
	public List<Patient> directFetchPatientsByCaregiverId(long id) {
		List<Patient> patientlist = patientRepository.findByCaregivers_id(id);
		return patientlist;
	}

	//with out contact Imantics
	@Override
	public PatientModel updatePatientAlone(Patient patient) {
		if (patient.getUserDetails()
		           .getCreateOn() == null) {
			patient.getUserDetails()
			       .setCreateOn(Date.from(Instant.now()));
		} else {
			patient.getUserDetails()
			       .setUpdateOn(Date.from(Instant.now()));
		}
		patient = patientRepository.save(patient);
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(patient);
		return patientmodel;
	}

	@Override
	public Patient fetchByDeviceId(String deviceId) {
		Patient patient = patientRepository.findByDeviceId(deviceId);
		return patient;
	}

	@Override
	public PatientAdherencePojo fetchAdherenceByPatientId(long id) {
		PatientAdherencePojo patientAdherencePojo = patientRepository.findAdherenceById(id);
		if (patientAdherencePojo == null) {
			return null;
		}
		return patientAdherencePojo;
	}

	@Override
	public Patient fetchPatientByUserId(long userID) {
		Patient patient = patientRepository.findByUserDetails_id(userID);
		if (patient != null) {
			if (patient.getProviders() != null && patient.getProviders()
			                                             .size() > 0) {
				for (Provider provider : patient.getProviders()) {
					if (provider.getUserDetails() != null && provider.getUserDetails()
					                                                 .getUserOptions() != null &&
							provider.getUserDetails()
							        .getUserOptions()
							        .getGsPharmacistEnable()
							        .intValue() == 1) {
						patient.setPharmacists(null);
					}
				}
			}
			return patient;
		}
		return null;
	}

	@Override
	public List<Patient> fetchAllPatients() {
		return patientRepository.findAll();
	}

	@Override
	public List<Patient> fetchEncryptPatientsByProviderId(long providerid) {
		return patientRepository.findByProviders_id(providerid);
	}
}
