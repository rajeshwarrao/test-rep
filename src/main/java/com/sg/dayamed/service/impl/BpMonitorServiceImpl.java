package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.service.BpMonitorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BpMonitorServiceImpl implements BpMonitorService {

	@Autowired
	BpMonitorRepository bpMonitorRepository;

	@Override
	public List<BpMonitor> findByPrescribedTime(LocalDateTime actualDate) {
		return bpMonitorRepository.findByPrescribedTime(actualDate);
	}

}
