package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.pojo.ConsumptionFrequencyResponseModel;
import com.sg.dayamed.pojo.ConsumptionResponseModel;
import com.sg.dayamed.pojo.FirstConsumptionFrequencyResponseModel;
import com.sg.dayamed.pojo.FirstConsumptionResponseModel;
import com.sg.dayamed.repository.ConsumptionFrequencyRepository;
import com.sg.dayamed.service.ConsumptionTemplateService;

import org.apache.commons.collections4.CollectionUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConsumptionTemplateServiceImpl implements ConsumptionTemplateService {

	@Autowired
	ConsumptionFrequencyRepository consumptionFrequencyRepository;

	@Override
	public ConsumptionTemplate fetchConsumptionTemplateByConsumptionsKey(String consumptionsKey) {
		ConsumptionTemplate consumptionTemplate =
				consumptionFrequencyRepository.findByconsumptionsKey(consumptionsKey);
		return consumptionTemplate;
	}

	@Override
	public ConsumptionFrequencyResponseModel getFrequencyList() {
		String prvValue = null;
		List<ConsumptionTemplate> cdataList = consumptionFrequencyRepository.findAll();
		List<FirstConsumptionFrequencyResponseModel> listFirstConsumptionFrequencyResponseModel = new ArrayList<>();
		for (ConsumptionTemplate firstConsumptionFrequency : cdataList) {
			if (firstConsumptionFrequency.getFrequencyName() != null
					&& !firstConsumptionFrequency.getFrequencyName()
					                             .equalsIgnoreCase(prvValue)) {
				prvValue = firstConsumptionFrequency.getFrequencyName();
				FirstConsumptionFrequencyResponseModel firstConsumptionFrequencyResponseModel =
						new FirstConsumptionFrequencyResponseModel();
				firstConsumptionFrequencyResponseModel.setName(firstConsumptionFrequency.getFrequencyName());
				firstConsumptionFrequencyResponseModel.setId(firstConsumptionFrequency.getFrequencyId());
				listFirstConsumptionFrequencyResponseModel.add(firstConsumptionFrequencyResponseModel);
			}
		}
		ConsumptionFrequencyResponseModel ConsumptionFrequencyResponseModel = null;
		if (CollectionUtils.isNotEmpty(listFirstConsumptionFrequencyResponseModel)) {
			ConsumptionFrequencyResponseModel = new ConsumptionFrequencyResponseModel();
			ConsumptionFrequencyResponseModel.setFrequencies(listFirstConsumptionFrequencyResponseModel);
		}
		return ConsumptionFrequencyResponseModel;
	}

	@Override
	public FirstConsumptionResponseModel getFirstConsumption(Long frequencyId) {
		FirstConsumptionResponseModel firstConsumptionResponseModel = null;
		if (frequencyId != null && frequencyId.longValue() > 0) {
			List<ConsumptionTemplate> cdataList = consumptionFrequencyRepository.findByfrequencyId(frequencyId);
			List<ConsumptionResponseModel> listConsumptionResponseModel = new ArrayList<>();
			for (ConsumptionTemplate firstConsumptionFrequency : cdataList) {
				ConsumptionResponseModel consumptionResponseModel = new ConsumptionResponseModel();
				consumptionResponseModel.setDescription(firstConsumptionFrequency.getConsumptionsDesc());
				consumptionResponseModel.setName(firstConsumptionFrequency.getConsumptionsKey());
				consumptionResponseModel.setId(firstConsumptionFrequency.getConsumptionsId());
				listConsumptionResponseModel.add(consumptionResponseModel);
			}
			if (CollectionUtils.isNotEmpty(listConsumptionResponseModel)) {
				firstConsumptionResponseModel = new FirstConsumptionResponseModel();
				firstConsumptionResponseModel.setConsumptions(listConsumptionResponseModel);
			}
		}
		return firstConsumptionResponseModel;
	}
}

