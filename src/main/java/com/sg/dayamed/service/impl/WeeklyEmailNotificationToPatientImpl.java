package com.sg.dayamed.service.impl;

import com.sg.dayamed.adapter.notifications.VelocityMessageFormatter;
import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.entity.GSVUCAToken;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.GSVUCATokenRepository;
import com.sg.dayamed.repository.MyEntityRepositoryCustom;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.WeeklyEmailNotificationToPatient;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.EmailAndSmsUtility;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class WeeklyEmailNotificationToPatientImpl implements WeeklyEmailNotificationToPatient {

	private static final Logger logger = LoggerFactory.getLogger(WeeklyEmailNotificationToPatientImpl.class);

	@Autowired
	RediseService rediseService;

	@Autowired
	MyEntityRepositoryCustom myEntityRepository;

	@Autowired
	GSVUCATokenRepository gsVUCATokenRepository;

	public String sendNotificationToPatient(String notoificationType, String emailId) {
		logger.info("====WeeklyEmailNotificationToPatientImpl===" + notoificationType);
		String userDetailsNotificationQry = (String) rediseService
				.getDataInRedisByKey(RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_USERDETAILS_QRY);
		List<Map<String, String>> listUserDetailswithDosage = userDetailsNotificationQry != null
				? myEntityRepository.getColmNameAndColumValueAsMap(userDetailsNotificationQry) : null;
		if (listUserDetailswithDosage != null && listUserDetailswithDosage.size() > 0) {
			String strNoOfDays = ApplicationConstants.PATIENT_MISSED_NOTIIFCATION_TYPE_WEEKLY
					.equalsIgnoreCase(notoificationType) ? (String) rediseService
							.getDataInRedisByKey(RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_MEDICATION_NOOF_LASTDAYS)
							: "24";
			int noOfDays = !StringUtils.isEmpty(strNoOfDays) && NumberUtils.isNumber(strNoOfDays)
					? Integer.parseInt(strNoOfDays) : 0;
			for (Map<String, String> userDetails : listUserDetailswithDosage) {
				String patientId = userDetails.get("patientId");
				if (!StringUtils.isEmpty(patientId)) {
					String patientMedictiaonQry = ApplicationConstants.PATIENT_MISSED_NOTIIFCATION_TYPE_WEEKLY
							.equalsIgnoreCase(notoificationType) ? (String) rediseService
									.getDataInRedisByKey(RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_MEDICATION_QRY)
									: (String) rediseService.getDataInRedisByKey(
											RedisKeyConstants.DAILY_EMAIL_TO_PATIENT_USERDETAILS_QRY);
					Map<Integer, Object> stmtMap = new HashMap<>();
					stmtMap.put(1, patientId);
					stmtMap.put(2, noOfDays);
					List<Map<String, String>> patientMedicationData = myEntityRepository
							.getColmNameAndColumValueAsMapWithInParams(patientMedictiaonQry, stmtMap);
					// decryptUserDetails(userDetails);
					// sendEmail(patientMedicationData, userDetails,
					// notoificationType);
					if(patientMedicationData != null && patientMedicationData.size() >0)
					sendMissedMedicationVelocityTemplate(patientMedicationData, userDetails, notoificationType, emailId);
				}
			}
		}
		return "";
	}

	@Autowired
	VelocityMessageFormatter velocityMessageFormatter;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	public void sendMissedMedicationVelocityTemplate(List<Map<String, String>> patientMedicationData,
			Map<String, String> userDetailsJson, String notoificationType, String emailId) {
		String emailConent = ApplicationConstants.PATIENT_MISSED_NOTIIFCATION_TYPE_WEEKLY
				.equalsIgnoreCase(notoificationType)
						? (String) getValueFromCacheBasedOnKey(
								RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_MEDICATION_EMAIL_CONTENT)
						: (String) getValueFromCacheBasedOnKey(
								RedisKeyConstants.DAILY_EMAIL_TO_PATIENT_MEDICATION_EMAIL_CONTENT);
		emailConent = emailConent.replace("$patient", userDetailsJson.get("firstName"));
		String recipient = StringUtils.isEmpty(emailId)? userDetailsJson.get("emailId") : emailId;
		String patientName = userDetailsJson.get("firstName");
		String applicationURL = (String) getValueFromCacheBasedOnKey(
				RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_MEDICATION_APPURL);
		for (Map<String, String> medicationMap : patientMedicationData) {
			String token = genrateToken(medicationMap, userDetailsJson);
			String vucaVideoLink = applicationURL + "?token=" + token;
			medicationMap.put("vUCAVidoeLink", vucaVideoLink);
		}

		String bodyTemplate = velocityMessageFormatter.formatMissedMedicationNotification(patientMedicationData,
				patientName, emailConent);
		List<String> recipientList = new ArrayList<String>();
		recipientList.add(recipient);
		//recipientList.add("RAJESHWARRAO.KODATI@SENECAGLOBAL.COM");
		emailAndSmsUtility.sendEmail(notoificationType + " Missed medications", bodyTemplate, recipientList, true);
	}

	public void decryptUserDetails(Map<String, String> userDetailsJson) {
		if (userDetailsJson.containsKey("email")) {
			userDetailsJson.put("Email", userDetailsJson.remove("emailId"));
		}
		if (userDetailsJson.containsKey("firstName")) {
			userDetailsJson.put("FirstName", userDetailsJson.remove("firstName"));
		}
	}

	public Object getValueFromCacheBasedOnKey(String key) {
		return rediseService.getDataInRedisByKey(key);
	}

	@Autowired
	EmailService emailService;

	public void sendEmail(List<Map<String, String>> adhrRs, Map<String, String> userDetailsJson,
			String notoificationType) {
		String recipient = userDetailsJson.get("Email");
		String subject = notoificationType + " Missed medications from GS";
		try {
			String emailConent = ApplicationConstants.PATIENT_MISSED_NOTIIFCATION_TYPE_WEEKLY
					.equalsIgnoreCase(notoificationType)
							? (String) getValueFromCacheBasedOnKey(
									RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_MEDICATION_EMAIL_CONTENT)
							: (String) getValueFromCacheBasedOnKey(
									RedisKeyConstants.DAILY_EMAIL_TO_PATIENT_MEDICATION_EMAIL_CONTENT);
			emailConent = emailConent.replace("$patient", userDetailsJson.get("firstName"));
			emailConent = ApplicationConstants.PATIENT_MISSED_NOTIIFCATION_TYPE_WEEKLY
					.equalsIgnoreCase(notoificationType) ? getEmailContent(adhrRs, emailConent, userDetailsJson)
							: getEmailContentForDaily(adhrRs, emailConent, userDetailsJson);
			if (!StringUtils.isEmpty(emailConent)) {
				Email emailobj = new Email();
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(recipient);
				emailobj.setRecipientList(recipientList);
				emailobj.setSubject(subject);
				emailobj.setBody(emailConent);
				emailService.sendEmail(emailobj);
			}
		} catch (Exception ex) {
			logger.error("Exception occurred when sending email", ex);
		}
	}

	public String getEmailContent(List<Map<String, String>> adhrRs, String emailContent,
			Map<String, String> userDetailsJson) {
		StringBuffer sbContent = null;
		try {
			if (adhrRs != null && adhrRs.size() > 0) {
				String applicationURL = (String) getValueFromCacheBasedOnKey(
						RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_MEDICATION_APPURL);
				sbContent = new StringBuffer(emailContent);
				Map<String, String> headers = adhrRs.get(0);
				sbContent.append("<br/> <br/> <table border='1'><thead><tr>");
				// Using entrySet() to get the entry's of the map
				Set<Map.Entry<String, String>> set = headers.entrySet();
				for (Map.Entry<String, String> it : set) {
					if (!"ndccode".equalsIgnoreCase(it.getKey())) {
						String headerValue = it.getKey();
						if ("Prescribed_Time".equalsIgnoreCase(headerValue)) {
							headerValue = headerValue + " (UTC)";
						}
						if (it.getKey().contains("_")) {
							headerValue = headerValue.replace("_", " ");
						}
						sbContent.append("<th bgcolor='#CDB1AC'>" + headerValue + "</th>");
					}
				}
				sbContent.append("<th bgcolor='#CDB1AC'>VUCA Video</th>");
				sbContent.append("</tr></thead><tbody>");
				for (Map medicationMap : adhrRs) {
					sbContent.append("<tr>");
					Set<Map.Entry<String, String>> medicatinSet = medicationMap.entrySet();
					for (Map.Entry<String, String> it : medicatinSet) {
						if (!"ndccode".equalsIgnoreCase(it.getKey())) {
							String strValue = it.getValue();
							strValue = StringUtils.isEmpty(strValue) || "null".equalsIgnoreCase(strValue) ? "--"
									: strValue;
							strValue = strValue.contains("_") ? strValue.replace("_", " ") : strValue;
							sbContent.append("<td>" + strValue + "</td>");
						}
					}
					String token = genrateToken(medicationMap, userDetailsJson);
					sbContent.append(
							"<td><a href=" + applicationURL + "?token=" + token + ">Click here for Video</a></td>");
					sbContent.append("</tr>");
				}
				sbContent.append("</tbody></table> <br/>");
				return sbContent.toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Exception occurred when getting email content", e);
			return null;
		}
	}

	public String getEmailContentForDaily(List<Map<String, String>> adhrRs, String emailContent,
			Map<String, String> userDetailsJson) {
		StringBuffer sbContent = null;
		try {
			if (adhrRs != null && adhrRs.size() > 0) {
				String applicationURL = (String) getValueFromCacheBasedOnKey(
						RedisKeyConstants.WEEKLY_EMAIL_TO_PATIENT_MEDICATION_APPURL);
				sbContent = new StringBuffer(emailContent);
				Map<String, String> headers = adhrRs.get(0);
				sbContent.append("<br/> <br/> <table border='1'><thead><tr>");
				// Using entrySet() to get the entry's of the map
				Set<Map.Entry<String, String>> set = headers.entrySet();
				for (Map.Entry<String, String> it : set) {
					if (!"ndccode".equalsIgnoreCase(it.getKey())) {
						String headerValue = it.getKey();
						if ("Prescribed_Time".equalsIgnoreCase(headerValue)) {
							headerValue = headerValue + " (UTC)";
						}
						if (it.getKey().contains("_")) {
							headerValue = headerValue.replace("_", " ");
						}
						sbContent.append("<th bgcolor='#CDB1AC'>" + headerValue + "</th>");
					}
				}
				// sbContent.append("<th bgcolor='#CDB1AC'>VUCA Vidoe</th>");
				sbContent.append("</tr></thead><tbody>");
				for (Map medicationMap : adhrRs) {
					sbContent.append("<tr>");
					Set<Map.Entry<String, String>> medicatinSet = medicationMap.entrySet();
					for (Map.Entry<String, String> it : medicatinSet) {
						if (!"ndccode".equalsIgnoreCase(it.getKey())) {
							String strValue = it.getValue();
							strValue = strValue != null && strValue.contains("_") ? strValue.replace("_", " ")
									: strValue;
							sbContent.append("<td>" + strValue + "</td>");
						}
					}
					// String token = genrateToken(medicationMap,
					// userDetailsJson);
					// sbContent.append(
					// "<td><a href=" + applicationURL + "?token=" + token +
					// ">Click here for Video</a></td>");
					sbContent.append("</tr>");
				}
				sbContent.append("</tbody></table> <br/>");
				return sbContent.toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Exception occurred when getting daily email content", e);
			return null;
		}
	}

	public String genrateToken(Map<String, String> medicationMap, Map<String, String> userDetailsJson) {
		try {
			String ndcCode = (String) medicationMap.get("ndccode");
			long currTime = System.currentTimeMillis();
			String userId = userDetailsJson.get("userId");
			logger.debug(ndcCode + "==genrateToken===" + userId);
			String token = ndcCode + currTime + userId;
			logger.debug("{}===========token==={}", currTime, token);
			GSVUCAToken gsVUCAtoken = new GSVUCAToken();
			gsVUCAtoken.setNdcCode(ndcCode);
			gsVUCAtoken.setToken(token);
			gsVUCAtoken.setUserId(!StringUtils.isEmpty(userId) ? Long.parseLong(userId) : 0);
			gsVUCAtoken.setStatus(0l);
			gsVUCAtoken.setCreatedOn(new Date());
			gsVUCATokenRepository.save(gsVUCAtoken);
			return token;
		} catch (Exception e) {
			logger.error("Exception occurred when generating token", e);
		}
		return null;
	}

	public GSVUCAToken getNdcCodeByGSToken(String gsToken) {
		List<GSVUCAToken> listGSVUCAToken = gsVUCATokenRepository.findByToken(gsToken);
		return listGSVUCAToken != null && listGSVUCAToken.size() > 0 ? listGSVUCAToken.get(0) : null;
	}
}
