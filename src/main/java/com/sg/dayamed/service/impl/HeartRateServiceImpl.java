package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.service.HeartRateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class HeartRateServiceImpl implements HeartRateService {

	@Autowired
	HeartRateRepository heartRateRepository;

	@Override
	public List<HeartRate> findByPrescribedTime(LocalDateTime actualDate) {
		return heartRateRepository.findByPrescribedTime(actualDate);
	}

	@Override
	public HeartRate fetchHeartRateByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId) {
		return heartRateRepository.findByPrescribedTimeAndDeviceInfoId(prescribedTime, deviceInfoId);
	}

}
