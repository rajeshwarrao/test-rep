package com.sg.dayamed.service.impl;

import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.util.ApplicationConstants;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class PHIDataEncryptionServiceImpl implements PHIDataEncryptionService {

	@Value("${dayamed.phidata.encrypt.fields}")
	String phidataEncryptFieldsArry;

	@Value("${dayamed.phidata.encrypt.enable}")
	String strAESEnable;

	@Autowired
	PHIDataEncryption pHIDataEncryption;

	private static final Logger logger = LoggerFactory.getLogger(PHIDataEncryptionServiceImpl.class);

	public List<PatientModel> encryptPatientList(List<PatientModel> listPatinetModel, String keyText) {
		keyText = keyText != null ? keyText.trim() : keyText;
		List<PatientModel> encrypPatientList = null;
		if (listPatinetModel != null && listPatinetModel.size() > 0) {
			encrypPatientList = new ArrayList<PatientModel>();
			for (PatientModel patientModel : listPatinetModel) {
				encrypPatientList.add(encryptPatient(patientModel, keyText));
			}
		}
		return encrypPatientList;
	}

	public PatientModel encryptPatient(PatientModel patientModel, String keyText) {
		return encryptPatient(patientModel, keyText, phidataEncryptFieldsArry);
	}

	public PatientModel encryptPatient(PatientModel patientModel, String keyText,
	                                   String phidataEncryptFieldsArryFelids) {
		JSONObject patntJson = null;
		try {
			if (patientModel != null && strAESEnable != null && "true".equalsIgnoreCase(strAESEnable.trim())) {
				keyText = keyText != null ? keyText.trim() : keyText;
				//logger.info("User details encryptPatient for user==" + patientModel.getUserDetails().getId() +
				// "key======" + keyText);
				patientModel.setUserDetails(
						encryptUserDetails(patientModel.getUserDetails(), keyText, phidataEncryptFieldsArryFelids));
				/*
				 * try{ String jsonStr = new
				 * Gson().toJson(patientModel.getProviders());
				 *
				 * }catch(Exception e){ e.printStackTrace(); }
				 */

				String jsonStr = new Gson().toJson(patientModel);
				patntJson = new JSONObject(jsonStr);
				JSONArray jsonPrecArray = patntJson.optJSONArray("prescriptions");
				if (phidataEncryptFieldsArryFelids != null) {
					SecretKey secretKey = pHIDataEncryption.getSecretEncryptionKey(keyText);
					for (String encrField : phidataEncryptFieldsArryFelids.split(",")) {
						if (jsonPrecArray != null && jsonPrecArray.length() > 0) {
							for (int i = 0; i < jsonPrecArray.length(); i++) {
								JSONObject jsonPre = jsonPrecArray.getJSONObject(i);
								String preFieldValue = jsonPre.optString(encrField);
								if (preFieldValue != null && !preFieldValue.trim()
								                                           .equals("")) {
									String encryFiledValue = pHIDataEncryption.encryptText(preFieldValue, secretKey);
									jsonPre.put(encrField, encryFiledValue);
								}
							}
						}
					}
					PatientModel encyPatientModel = new Gson().fromJson(patntJson.toString(), PatientModel.class);
					return encyPatientModel;
				}
			}
		} catch (Exception e) {
			logger.error("Exception while encrypting patient", e);
		}
		return patientModel;
	}

	public UserDetailsModel encryptUserDetails(UserDetailsModel userDetailsModel, String keyText) {
		return encryptUserDetails(userDetailsModel, keyText, phidataEncryptFieldsArry);
	}

	public UserDetailsModel encryptUserDetails(UserDetailsModel userDetailsModel, String keyText,
	                                           String phidataEncryptFieldsArryFeilds) {
		String errorKey = null, errorValue = null;
		try {
			if (userDetailsModel != null) {
				String userDetailsStr = new Gson().toJson(userDetailsModel);
				JSONObject userDetailsJson = new JSONObject(userDetailsStr);
				JSONObject addresssDetaJson = userDetailsJson.getJSONObject("address");
				if (phidataEncryptFieldsArryFeilds != null) {
					keyText = keyText != null ? keyText.trim() : keyText;
					SecretKey secretKey = pHIDataEncryption.getSecretEncryptionKey(keyText);
					for (String encrField : phidataEncryptFieldsArryFeilds.split(",")) {
						errorKey = encrField;
						String fieldValue = userDetailsJson.optString(encrField);
						errorValue = fieldValue;
						if (fieldValue != null && !fieldValue.trim()
						                                     .equals("")) {
							String encryFiledValue = pHIDataEncryption.encryptText(fieldValue, secretKey);
							userDetailsJson.put(encrField, encryFiledValue);
						}
						String addFieldValue = addresssDetaJson.optString(encrField);
						errorValue = addFieldValue;
						if (addFieldValue != null && !addFieldValue.trim()
						                                           .equals("")) {
							String encryaddFiledValue = pHIDataEncryption.encryptText(addFieldValue, secretKey);
							addresssDetaJson.put(encrField, encryaddFiledValue);
						}
					}
					return new Gson().fromJson(userDetailsJson.toString(), UserDetailsModel.class);
				}
			}
		} catch (Exception e) {
			logger.error("Exception while encrypting user details. ErrorKey = {},  errorValue = {}, keyText = {}",
			             errorKey, errorValue, keyText, e);
		}
		return userDetailsModel;
	}

	public PatientModel decryptPatient(PatientModel patientModel, String keyText) {
		return decryptPatient(patientModel, keyText, phidataEncryptFieldsArry);
	}

	public PatientModel decryptPatient(PatientModel patientModel, String keyText,
	                                   String phidataEncryptFieldsArryFeilds) {
		try {
			if (patientModel != null && strAESEnable != null && "true".equalsIgnoreCase(strAESEnable.trim())) {
				//logger.info("User details decryptPatient for user==" + patientModel.getUserDetails().getId()+
				// "key======" + keyText);
				UserDetailsModel userDetailsModel = decryptPatientUserDetails(patientModel.getUserDetails(), keyText,
				                                                              phidataEncryptFieldsArryFeilds);
				if (userDetailsModel != null) {
					patientModel.setUserDetails(userDetailsModel);
					String jsonStr = new Gson().toJson(patientModel);
					JSONObject patntJson = new JSONObject(jsonStr);
					JSONArray jsonPrecArray = patntJson.optJSONArray("prescriptions");
					if (phidataEncryptFieldsArryFeilds != null) {
						keyText = keyText != null ? keyText.trim() : keyText;
						SecretKey secretKey = pHIDataEncryption.getSecretEncryptionKey(keyText);
						for (String encrField : phidataEncryptFieldsArryFeilds.split(",")) {
							for (int i = 0; i < jsonPrecArray.length(); i++) {
								JSONObject jsonPre = jsonPrecArray.getJSONObject(i);
								String preFieldValue = jsonPre.optString(encrField);
								if (preFieldValue != null && !preFieldValue.trim()
								                                           .equals("")) {
									String encryFiledValue = pHIDataEncryption.decryptText(preFieldValue, secretKey);
									jsonPre.put(encrField, encryFiledValue);
								}
							}
						}
						PatientModel encyPatientModel = new Gson().fromJson(patntJson.toString(), PatientModel.class);
						return encyPatientModel;
					}
				} else {
					logger.info("decryptPatient user details failed==keyText=" + keyText);
				}
			}
		} catch (Exception e) {
			logger.error("Exception while decrypting patient #{} and the JWT token {}", patientModel.getId(), keyText,
			             e);
		}
		return null;
	}

	public UserDetailsModel decryptPatientUserDetails(UserDetailsModel userDetailsModel, String keyText) {
		return decryptPatientUserDetails(userDetailsModel, keyText, phidataEncryptFieldsArry);
	}

	public UserDetailsModel decryptPatientUserDetails(UserDetailsModel userDetailsModel, String keyText,
	                                                  String phidataEncryptFieldsArryFeilds) {
		String errorKey = null, errorValue = null;
		try {
			if (userDetailsModel != null) {
				String userDetailsStr = new Gson().toJson(userDetailsModel);
				JSONObject userDetailsJson = new JSONObject(userDetailsStr);
				JSONObject addresssDetaJson = userDetailsJson.optJSONObject("address");
				//logger.info("User details decryptPatientUserDetails for user==" + userDetailsModel.getId() +
				// "key======"+ keyText);
				if (phidataEncryptFieldsArryFeilds != null) {
					keyText = keyText != null ? keyText.trim() : keyText;
					SecretKey secretKey = pHIDataEncryption.getSecretEncryptionKey(keyText);
					for (String encrField : phidataEncryptFieldsArryFeilds.split(",")) {
						errorKey = encrField;
						String fieldValue = userDetailsJson.optString(encrField);
						errorValue = fieldValue;
						if (fieldValue != null && !fieldValue.trim()
						                                     .equals("")) {
							String encryFiledValue = pHIDataEncryption.decryptText(fieldValue, secretKey);
							userDetailsJson.put(encrField, encryFiledValue);
						}
						if (addresssDetaJson != null) {
							String addFieldValue = addresssDetaJson.optString(encrField);
							errorValue = addFieldValue;
							if (addFieldValue != null && !addFieldValue.trim()
							                                           .equals("")) {
								String encryaddFiledValue = pHIDataEncryption.decryptText(addFieldValue, secretKey);
								addresssDetaJson.put(encrField, encryaddFiledValue);
							}
						}
					}
					return new Gson().fromJson(userDetailsJson.toString(), UserDetailsModel.class);
				}
			}
		} catch (Exception e) {
			logger.error("Exception while decrypting user details. ErrorKey = {},  errorValue = {}, keyText = {}",
			             errorKey, errorValue, keyText, e);
		}
		return null;
	}

	public String getKeyText(HttpServletRequest request, Authentication authentication) {
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		if (ApplicationConstants.IOS.equalsIgnoreCase(jwtUser.getTypeOfDevice())
				|| ApplicationConstants.ANDROID.equalsIgnoreCase(jwtUser.getTypeOfDevice())) {
			return jwtUser.getDeviceId() + jwtUser.getToken();
		} else {
			return jwtUser.getToken();
		}
	}

	public String getKeyText(JwtUserDetails jwtUser) {
		if (ApplicationConstants.IOS.equalsIgnoreCase(jwtUser.getTypeOfDevice())
				|| ApplicationConstants.ANDROID.equalsIgnoreCase(jwtUser.getTypeOfDevice())) {
			return jwtUser.getDeviceId() + jwtUser.getToken();
		} else {
			return jwtUser.getToken();
		}
	}

	public String getKeyText(String token, JwtUser jwtUser) {
		if (ApplicationConstants.IOS.equalsIgnoreCase(jwtUser.getTypeOfDevice())
				|| ApplicationConstants.ANDROID.equalsIgnoreCase(jwtUser.getTypeOfDevice())) {
			return jwtUser.getDeviceId() + token;
		} else {
			return token;
		}
	}

	public String getKeyText(String token, String typeOfDevice, String deviceId) {
		if (ApplicationConstants.IOS.equalsIgnoreCase(typeOfDevice)
				|| ApplicationConstants.ANDROID.equalsIgnoreCase(typeOfDevice)) {
			return deviceId + token;
		} else {
			return token;
		}
	}

}
