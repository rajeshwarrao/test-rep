package com.sg.dayamed.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.dao.EhCacheConstants;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Address;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.CaregiverRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.ProviderService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.ApplicationErrors;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

@Service
public class CaregiverServiceImpl implements CaregiverService {

	@Autowired
	CaregiverRepository caregiverRepository;

	@Autowired
	Utility utility;

	@Autowired
	PatientService patientService;

	@Autowired
	ProviderService providerService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	SendResetPwdNotification sendPwdResetNtf;

	@Autowired
	PasswordEncryption pwdEncryption;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;

	@Autowired
	ObjectMapper userPreferenceModelMapper;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	UserPreferenceRepository userPreferenceRepository;

	@Autowired
	UserPreferenceService userPreferenceService;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;
	
	@Autowired
	PHIDataEncryptionService pHIDataEncryptionService;
	
	@Autowired
	PatientRepository patientRepository;
	
	@Autowired
	PrescriptionRepository prescriptionRepository;
	
	@Autowired
	ProviderRepository providerRepository;

	private static final Logger logger = LoggerFactory.getLogger(CaregiverServiceImpl.class);

	@Override
	public List<CaregiverModel> fetchCaregivers() {
		List<Caregiver> caregivers = caregiverRepository.findAll();

		// convert caregiverEntity to Model
		List<CaregiverModel> caregiverModelList = new ArrayList<>();
		caregivers.forEach(caregiver -> {
			caregiverModelList.add(utility.convertCaregiverEntityToModel(caregiver));
		});
		return caregiverModelList;
	}

	@Override
	public Caregiver fetchCaregiverById(long id)throws DataValidationException {
		Optional<Caregiver> optionalCaregiver = caregiverRepository.findById(id);
		if (optionalCaregiver.isPresent()) {
			return optionalCaregiver.get();
		} else {
			logger.error(ApplicationErrors.NOT_FOUND_CAREGIVER_IN_DB);
			throw new DataValidationException(ApplicationConstants.CAREGIVER, APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
	}

	
	@Override
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public Set<CaregiverModel> addCaregiverToPatient(CaregiverModel caregiverModel, long patientid) throws DataValidationException {
		Caregiver caregiver = utility.convertCaregiverModelToEntity(caregiverModel);
		if(caregiver == null){
			throw new DataValidationException(ApplicationConstants.CAREGIVER, APIErrorKeys.ERROR_INPUT_BADREQUEST);
		}
		Set<CaregiverModel> caregiverModelSet = new LinkedHashSet<>();
		if (caregiver != null && caregiver.getUserDetails() != null) {
			Role role = roleRepository.findByName(ApplicationConstants.CAREGIVER);
			caregiver.getUserDetails().setRole(role);
			if (caregiver.getUserDetails().getCreateOn() == null) {
				caregiver.getUserDetails().setCreateOn(Date.from(Instant.now()));
			} else {
				caregiver.getUserDetails().setUpdateOn(Date.from(Instant.now()));
			}
			// if patientid zero means adding caregiver else adding/updating
			// patient with caregiver
			UserDetails nonEncryptCaregiverUserDetails = caregiver.getUserDetails();
			if (patientid == 0) {
				// create
				caregiverModel = createCaregiver(caregiver);
				caregiverModelSet.add(caregiverModel);
			} else {
				// update
					PatientModel persistedPatientModel = updateCaregiver(caregiver, patientid);
					if (persistedPatientModel != null && persistedPatientModel.getCaregivers() != null) {
						//sending mails to respective people
						sendPwdResetNtf.sendPwdResetNotificationWhenRegst(nonEncryptCaregiverUserDetails, "Caregiver");
						// sending email and msg to patient
						List<String> recipientList = new ArrayList<>();
						recipientList.add(persistedPatientModel.getUserDetails().getEmailId());
						Long userId = persistedPatientModel.getUserDetails().getId();
						String userLangPreference = utility.getUserPreferedLang(userId);
						String bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
								"profile" + ".added" + ".caregiver", userLangPreference,null);
						String emailSubject = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
								"emailsubject.addedcaregiver", userLangPreference,null);
						emailAndSmsUtility.sendEmail(emailSubject, bodyMsg, recipientList);
						emailAndSmsUtility.sendSms(bodyMsg, persistedPatientModel.getUserDetails().getCountryCode(),
								persistedPatientModel.getUserDetails().getMobileNumber());
					
						caregiverModelSet.addAll(persistedPatientModel.getCaregivers());
						
					}
			}
		} 
		return caregiverModelSet;
	}
	
	private CaregiverModel createCaregiver(Caregiver caregiver) {
		caregiver.getUserDetails().setRole(roleRepository.findByName("caregiver"));
		UserDetails nonEncryptCaregiverUserDetails = caregiver.getUserDetails();
		String pwdChangeToken = pwdEncryption.encrypt(caregiver.getUserDetails()
                .getEmailId() + System.currentTimeMillis());
		caregiver.getUserDetails().setPwdChangeToken(pwdChangeToken);
		caregiver.getUserDetails().setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));
		if (caregiver.getUserDetails().getCreateOn() == null) {
			caregiver.getUserDetails().setCreateOn(Date.from(Instant.now()));
		} else {
			caregiver.getUserDetails().setUpdateOn(Date.from(Instant.now()));
		}
		CaregiverModel caregiverModel = utility.convertCaregiverEntityToModel(caregiverRepository.save(caregiver));
		if (caregiverModel != null) {
			sendPwdResetNtf.sendPwdResetNotificationWhenRegst(nonEncryptCaregiverUserDetails, "Caregiver");
			//adding user pref
			if (caregiverModel.getUserDetails() != null) {
				userPreferenceService.addUserPreference(caregiverModel.getUserDetails()
				                                                      .getId(), new UserPreferenceModel());
			}
		}
		return caregiverModel;
	}
	private void addUserPreference(Long userId){
		userPreferenceService.addUserPreference(userId,
				new UserPreferenceModel());
	}
	
	private PatientModel updateCaregiver(Caregiver caregiver,long patientid) {
		Patient patient = patientService.fetchPatientById(patientid);
		Set<Caregiver> caregiverSet = patient.getCaregivers();
		caregiver.getUserDetails().setPwdChangeToken(
				pwdEncryption.encrypt(caregiver.getUserDetails().getEmailId() + System.currentTimeMillis()));
		caregiver.getUserDetails().setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));
		// encripting patient data to save in DB
		caregiverSet.add(caregiver);
		
		//updating userpref
		if (caregiver.getUserDetails() != null) {
			addUserPreference(caregiver.getUserDetails().getId());
		}
		
		patient.setCaregivers(caregiverSet);
		PatientModel persistedPatientModel = patientService.updatePatientAlone(patient);
		return persistedPatientModel;
	}

	
		@Override
		@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
		public CaregiverModel addCaregiver(CaregiverModel caregiverModel, JwtUserDetails jwtUser) {
			
			Caregiver caregiver = utility.convertCaregiverModelToEntity(caregiverModel);
			long cargiverId = caregiver.getId();
			String pwdChangeToken = pwdEncryption.encrypt(caregiver.getUserDetails()
			                                                       .getEmailId() + System.currentTimeMillis());
			UserDetails nonEncryptCaregiverUserDetails = caregiver.getUserDetails();
			
			if (jwtUser != null && jwtUser.getRole()
			                              .equalsIgnoreCase("provider")) {
				Provider provider = providerService.directFetchProviderByUserId(jwtUser.getUserId());
				if (provider != null) {
					Set<Provider> providerSet = new HashSet<>();
					providerSet.add(provider);
					caregiver.setProviders(providerSet);
					caregiverModel = createCaregiver(caregiver);
				}
			} else {
				Role role = roleRepository.findByName(ApplicationConstants.CAREGIVER);
				caregiver.getUserDetails()
				       .setRole(role);
				// if login by Admin
				caregiver.setAdminflag(true);
				caregiverModel = utility.convertCaregiverEntityToModel(caregiverRepository.save(caregiver));
			}
			nonEncryptCaregiverUserDetails.setPwdChangeToken(pwdChangeToken);
			if (caregiverModel != null) {
				if (caregiverModel.getUserDetails() != null) {
					addUserPreference(caregiverModel.getUserDetails().getId());
				}
				caregiver.setUserDetails(nonEncryptCaregiverUserDetails);
				List<String> recipientList = new ArrayList<>();
				if (cargiverId == 0) {
					//creating caregiver
					sendPwdResetNtf.sendPwdResetNotificationWhenRegst(nonEncryptCaregiverUserDetails, "Caregiver");
					//adding userPreferences
					if (caregiverModel.getUserDetails() != null) {
						try {
							addUserPreference(caregiverModel.getUserDetails().getId());
						} catch (Exception e) {
							logger.info("json parsing exception while adding userpref of patient");
						}
					}
				} else {
					// sending Email and SMS
					recipientList.add(caregiver.getUserDetails()
					                           .getEmailId());
					Long userId = caregiver.getUserDetails()
					                       .getId();
					String userLangPreference = utility.getUserPreferedLang(userId);
				String bodyMsgCaregiver = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
						msgNotificationCaregiverAdd, userLangPreference, null);
				String bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
						msgNotificationPatientAdd, userLangPreference, null);
				String emailSubject = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
						msgNotificationPatientAdd, userLangPreference, null);
					emailAndSmsUtility.sendEmail(emailSubject, bodyMsgCaregiver, recipientList);
					emailAndSmsUtility.sendSms(bodyMsg, caregiver.getUserDetails()
					                                             .getCountryCode(), caregiver.getUserDetails()
					                                                                         .getMobileNumber());
				}
			}
			return caregiverModel;
		}

		String msgNotificationCaregiverAdd = "profile.update.caregiver.you";
		String msgNotificationPatientAdd= "profile.update.patient.you";
		
	@Override
	public List<CaregiverModel> fetchAdminAndProviderCargivers(long providerid) {

		List<CaregiverModel> adminAndProviderCregiverModelList = new ArrayList<>();
		List<Caregiver> adminAndProviderCregiverList = new ArrayList<>();
		List<Caregiver> adminCaregiverList = caregiverRepository.findByAdminflag(true);
		List<Caregiver> providerCaregiverList = caregiverRepository.findByProviders_id(providerid);
		adminAndProviderCregiverList.addAll(adminCaregiverList);
		adminAndProviderCregiverList.addAll(providerCaregiverList);
		for (Caregiver caregiver : adminAndProviderCregiverList) {
			adminAndProviderCregiverModelList.add(utility.convertCaregiverEntityToModel(caregiver));
		}
		// }
		return adminAndProviderCregiverModelList;
	}

	@Override
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public String deleteCaregiver(long caregiverId) throws DataValidationException {
		List<PatientModel> PatientModelList = patientService.fetchPatientsByCaregiverId(caregiverId);
		if (CollectionUtils.isNotEmpty(PatientModelList)) {
			throw new DataValidationException(APIErrorKeys.ERROR_CAREGIVER_ASSOCIATED_WITH_PATIENTS);
		}
		Optional<Caregiver> optionalCargiver = caregiverRepository.findById(caregiverId);
		if (optionalCargiver.isPresent()) {
			caregiverRepository.deletepatientsBycaregiverId(caregiverId);
			caregiverRepository.delete(optionalCargiver.get());
			return "success";
		} else {
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
	}

	@Override
	public Caregiver fetchCaregiverByUserId(long userId) throws DataValidationException{
		Caregiver caregiver = caregiverRepository.findByUserDetails_id(userId);
		if (caregiver == null) {
			throw new DataValidationException(ApplicationConstants.CAREGIVER, APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
		return caregiver;
	}

	@Override
	public List<CaregiverModel> fetchCaregiversByPatientId(long patientid) {

		List<Caregiver> caregiverList = caregiverRepository.findByPatients_id(patientid);
		List<CaregiverModel> caregiverModelList = new ArrayList<CaregiverModel>();
		caregiverList.forEach(caregiver -> {
			caregiverModelList.add(utility.convertCaregiverEntityToModel(caregiver));
		});
		return caregiverModelList;
	}

	@Override
	public Caregiver directFetchCaregiverById(long id) {
		Optional<Caregiver> optionalCaregiver = caregiverRepository.findById(id);
		if (optionalCaregiver.isPresent()) {
			Caregiver caregiver = optionalCaregiver.get();
			return caregiver;
		} else {
			return null;
		}
	}

	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public Set<CaregiverModel> addCaregiverFromApp(CaregiverModel caregiverModel, MultipartFile profileImage)
			throws Exception {
	
			UserDetailsModel caregiverUserDetailsModel = null;
			if (caregiverModel.getUserDetails() != null) {
				caregiverUserDetailsModel = caregiverModel.getUserDetails();
				UserDetailsModel existeduserDetailsModel = userDetailsService
						.findByEmailIdIgnoreCase(caregiverUserDetailsModel.getEmailId());
				if (existeduserDetailsModel == null) {
					existeduserDetailsModel =
							userDetailsService.findByEmailIdIgnoreCase(caregiverUserDetailsModel.getEmailId());
				}
				if (existeduserDetailsModel == null) {
					if (profileImage != null && !profileImage.isEmpty()) {
						String oldImageName = ApplicationConstants.EMPTY_STRING;
						String imageNameWitPathUrl = utility.uploadProfilepic(profileImage,
						                                                      caregiverUserDetailsModel.getFirstName(),
						                                                      oldImageName);
						if (imageNameWitPathUrl != null) {
							caregiverUserDetailsModel.setImageUrl(imageNameWitPathUrl);
						}
					}
					caregiverModel.setTypeofDevice(ApplicationConstants.ANDROID);
					return addCaregiverToPatient(caregiverModel, 0L);
				} else {
				logger.error(ApplicationErrors.EMAIL_ALREADY_EXISTED);
					throw new DataValidationException(ApplicationErrors.EMAIL_ALREADY_EXISTED,
					                                  APIErrorKeys.ERROR_EMAIL_ALREADY_EXISTED);

				}
			}
			throw new DataValidationException("wrong data", APIErrorKeys.ERROR_INVALID_DATA);
		
	}


	@Override
	public List<PatientModel> fetchPatietnsByCaregiverIdFromJwtToken(Authentication authentication)
			throws DataValidationException {
		List<PatientModel> patientlist = new ArrayList<>();
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		if (jwtUser != null && jwtUser.getUserId() > 0) {
			Caregiver caregiver = fetchCaregiverByUserId(jwtUser.getUserId());
			if (caregiver != null && jwtUser.getRole()
			                                .equalsIgnoreCase("caregiver")) {
				return patientService.fetchPatientsByCaregiverId(caregiver.getId());
			}
		}
		return patientlist;
	}

	@Override
	public List<PatientModel> fetchPatietnsAloneByCaregiver(Authentication authentication)
			throws DataValidationException {
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		List<PatientModel> patientlist = null;
		if (jwtUser != null && jwtUser.getUserId() > 0) {
			Caregiver caregiver = fetchCaregiverByUserId(jwtUser.getUserId());
			if (caregiver != null && jwtUser.getRole().equalsIgnoreCase("caregiver")) {
				 patientlist = patientService.fetchPatientsByCaregiverId(caregiver.getId());
				if (patientlist != null) {
					for (PatientModel patientModel : patientlist) {
						patientModel.setCaregiverList(null);
						patientModel.setPrescriptions(null);
						patientModel.setPharmacists(null);
						patientModel.setProviders(null);
						patientModel.setCaregivers(null);
					}
					logger.info("Patients returned");
				} else {
					logger.info("No Patients Associated");
				}
			}
		}
		return patientlist;
	}

	@Override
	public CaregiverModel addCaregiverFromWebValidate(CaregiverModel caregivermodel, MultipartFile profileImage,
			JwtUserDetails jwtUser) throws DataValidationException {
		CaregiverModel persistedCaregiverModel = null;
		if (caregivermodel != null) {
			caregivermodel.setTypeofDevice("web");
			UserDetailsModel caregiverUserDetails = caregivermodel.getUserDetails();
			StringUtils.isBlank(caregiverUserDetails.getLastName());
			if (StringUtils.isBlank(caregiverUserDetails.getFirstName())
					|| StringUtils.isBlank(caregiverUserDetails.getLastName())
					|| StringUtils.isBlank(caregiverUserDetails.getRace())
					|| StringUtils.isBlank(caregiverUserDetails.getEmailId())
					|| StringUtils.isBlank(caregiverUserDetails.getGender())
					|| StringUtils.isBlank(caregiverUserDetails.getCountryCode())
					|| StringUtils.isBlank(caregiverUserDetails.getMobileNumber())
					|| StringUtils.isBlank(caregiverUserDetails.getAddress().getLocation())
					|| StringUtils.isBlank(caregiverUserDetails.getAddress().getZipCode())
					|| caregiverUserDetails.validDateOfBirth(UserRoleEnum.CAREGIVER)) {
				logger.error("missing mandatory data in input while adding caregiver");
				throw new DataValidationException(APIErrorKeys.ERROR_INPUT_BADREQUEST);
			}
		}
		// setting profilepic to userdetails imageurl
		if (caregivermodel != null && profileImage != null && !profileImage.isEmpty()) {
			String imageNameWitPathUrl = utility.uploadProfileImage(caregivermodel.getUserDetails(), profileImage);
			if (imageNameWitPathUrl != null) {
				caregivermodel.getUserDetails().setImageUrl(imageNameWitPathUrl);
			}
		}
		if (caregivermodel != null && caregivermodel.getUserDetails().getEmailId() != null) {
			UserDetailsModel userDetailsModel = userDetailsService
					.findByEmailIdIgnoreCase(caregivermodel.getUserDetails().getEmailId());
			if (userDetailsModel == null || caregivermodel.getId() > 0) {
				if (userDetailsModel != null) {
					caregivermodel.getUserDetails().setPassword(userDetailsModel.getPassword());
					caregivermodel.getUserDetails()
							.setType((userDetailsModel.getType() != null) ? userDetailsModel.getType() : null);
				}
				persistedCaregiverModel = addCaregiver(caregivermodel, jwtUser);
			} else {
				logger.error(ApplicationErrors.EMAIL_ALREADY_EXISTED);
				throw new DataValidationException(APIErrorKeys.ERROR_EMAIL_CONFLICT);
			}
		}
		return persistedCaregiverModel;
	}

	@Override
	public String mapPatientToCaegiver(String patientemailid, Long caregiverId) throws DataValidationException {
		
		UserDetailsModel userDetailsModel = userDetailsService.findByEmailIdIgnoreCase(patientemailid);
		if (userDetailsModel == null) {
			logger.error("provided emailid is not existed in Database");
			throw new DataValidationException(APIErrorKeys.ERROR_EMAIL_NOT_EXIST);
		}
		if (userDetailsModel.getRole().getName().equals("patient")) {
			Caregiver caregiver = fetchCaregiverById(caregiverId);
			if (caregiver != null) {
				for (Patient patient : caregiver.getPatients()) {
					if (StringUtils.equalsIgnoreCase(patient.getUserDetails().getEmailId(),
							patientemailid)) {
						logger.error("Patient already mapped");
						throw new DataValidationException(APIErrorKeys.ERROR_PATIENT_EMAIL_ALREADY_MAPPED);
					}
				}
				Patient patient = patientService.findByUserDetails_id(userDetailsModel.getId());
				Set<Caregiver> caregiverSet = patient.getCaregivers();
				caregiverSet.add(caregiver);
				patientService.updatePatientAlone(patient);
				return "success";
			}
		}else {
			throw new DataValidationException(APIErrorKeys.ERROR_USER_NOT_FOUND);
		}
		return "";
	}

	@Override
	public CaregiverModel updateCaregiverFromApp(CaregiverModel caregiverModel, MultipartFile profileImage)
			throws DataValidationException {
		UserDetailsModel caregiverUserDetailsModel = null;
		if (caregiverModel.getUserDetails() != null) {
			caregiverUserDetailsModel = caregiverModel.getUserDetails();
			Caregiver existedCaregiver = fetchCaregiverById(caregiverModel.getId());
			if (existedCaregiver == null) {
				logger.error(ApplicationErrors.NOT_FOUND_CAREGIVER_WITH_ID);
				throw new DataValidationException(APIErrorKeys.ERROR_USER_NOT_FOUND);
			}
			if (profileImage != null && !profileImage.isEmpty()) {
				String oldImageName = ApplicationConstants.EMPTY_STRING;

				if (caregiverUserDetailsModel != null && caregiverUserDetailsModel.getImageUrl() != null) {
					oldImageName = utility.getLastBitFromUrl(caregiverUserDetailsModel.getImageUrl());
				}
				String imageNameWitPathUrl = utility.uploadProfilepic(profileImage,
						caregiverModel.getUserDetails().getFirstName(), oldImageName);
				if (caregiverUserDetailsModel != null && imageNameWitPathUrl != null) {
					caregiverUserDetailsModel.setImageUrl(imageNameWitPathUrl);
				}
				UserDetails caregiverUserdetails = utility.convertUserDetailsModelToEntity(caregiverUserDetailsModel);
				UserDetails recentUserDetails = new UserDetails();
				BeanUtils.copyProperties(caregiverUserdetails, recentUserDetails);

				if (caregiverUserDetailsModel != null && caregiverUserDetailsModel.getAddress() != null) {
					Address address = new Address();
					BeanUtils.copyProperties(caregiverUserDetailsModel.getAddress(), address);
					recentUserDetails.setAddress(address);
				}
				if (StringUtils.isNotBlank(existedCaregiver.getUserDetails().getImageUrl())) {
					recentUserDetails.setImageUrl(existedCaregiver.getUserDetails().getImageUrl());
				}
				if (StringUtils.isNotBlank(caregiverUserdetails.getImageUrl())) {
					recentUserDetails.setImageUrl(caregiverUserdetails.getImageUrl());
				}
				recentUserDetails.setEmailId(existedCaregiver.getUserDetails().getEmailId());
				recentUserDetails.setId(existedCaregiver.getUserDetails().getId());
				recentUserDetails.setRole(existedCaregiver.getUserDetails().getRole());
				recentUserDetails.setPassword(existedCaregiver.getUserDetails().getPassword());
				recentUserDetails.setType((existedCaregiver.getUserDetails().getType() != null)
						? existedCaregiver.getUserDetails().getType() : null);
				recentUserDetails.setEmailId(existedCaregiver.getUserDetails().getEmailId());
				existedCaregiver.setUserDetails(recentUserDetails);

				Caregiver persistedCaregiver = caregiverRepository.save(existedCaregiver);
					userPreferenceService.addUserPreference(persistedCaregiver.getUserDetails().getId(),
							new UserPreferenceModel());
				caregiverModel = utility.convertCaregiverEntityToModel(persistedCaregiver);

				String userLangPreference = utility.getUserPreferedLang(caregiverModel.getId());

				String subject = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
						"emailsubject" + ".updatedprofile", userLangPreference,null);
				String bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
						"profile.update" + ".patient" + ".you", userLangPreference,null);
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(caregiverModel.getUserDetails().getEmailId());
				emailAndSmsUtility.sendEmail(subject, bodyMsg, recipientList);
				String countrycode = caregiverModel.getUserDetails().getCountryCode();
				String mobileNumber = caregiverModel.getUserDetails().getMobileNumber();
				emailAndSmsUtility.sendSms(bodyMsg, countrycode, mobileNumber);
				return caregiverModel;
			}

		}

		return null;
	}

	@Override
	public Set<CaregiverModel> createCaregiverAssociatedwithPatient(CaregiverModel caregiverModel, Long patientid,
			MultipartFile profileImage) throws DataValidationException {
		UserDetailsModel caregiverUserDetailsModel = null;
		Set<CaregiverModel> setCaregiverModel = new HashSet<>();
		if (caregiverModel.getUserDetails() != null) {
			caregiverUserDetailsModel = caregiverModel.getUserDetails();
			UserDetailsModel existeduserDetailsModel = userDetailsService
					.findByEmailIdIgnoreCase(caregiverUserDetailsModel.getEmailId());
			if (existeduserDetailsModel == null) {
				if (profileImage != null && !profileImage.isEmpty()) {
					String oldImageName = ApplicationConstants.EMPTY_STRING;

					if (caregiverUserDetailsModel.getImageUrl() != null) {
						oldImageName = utility.getLastBitFromUrl(caregiverUserDetailsModel.getImageUrl());
					}
					String imageNameWitPathUrl = utility.uploadProfilepic(profileImage,
					                                                      caregiverUserDetailsModel.getFirstName(),
					                                                      oldImageName);
					if (imageNameWitPathUrl != null) {
						caregiverUserDetailsModel.setImageUrl(imageNameWitPathUrl);
					}
				}
				caregiverModel.setUserDetails(caregiverUserDetailsModel);
				return addCaregiverToPatient(caregiverModel,
						patientid);
			} else {
				logger.error(ApplicationErrors.EMAIL_ALREADY_EXISTED);
				throw new DataValidationException(ApplicationConstants.PATIENT, APIErrorKeys.ERROR_EMAIL_CONFLICT);
			}
		}
		
		return setCaregiverModel;
	}

	@Override
	public List<PatientModel> fetchPatientsByCaregiverId(Long cargiverid, Authentication authentication)
			throws DataValidationException {
		Caregiver caregiVer = fetchCaregiverById(cargiverid);
		if (caregiVer == null) {
			logger.error(ApplicationErrors.NOT_FOUND_CAREGIVER_IN_DB);
			throw new DataValidationException(ApplicationConstants.CAREGIVER,APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
		List<Patient> patientList = patientService.directFetchPatientsByCaregiverId(cargiverid);
		List<PatientModel> patientmodels = new ArrayList<PatientModel>();
		if (patientList != null && patientList.size() > 0) {
			List<Long> userIds = new ArrayList<>();
			for (Patient patient : patientList) {
				if (patient.getProviders() != null) {
					for (Provider provider : patient.getProviders()) {
						if (!userIds.contains(provider.getUserDetails()
						                              .getId())) {
							userIds.add(provider.getUserDetails()
							                    .getId());
						}
					}
					if (patient.getCaregivers() != null) {
						for (Caregiver caregiver : patient.getCaregivers()) {
							if (!userIds.contains(caregiver.getUserDetails()
							                               .getId())) {
								userIds.add(caregiver.getUserDetails()
								                     .getId());
							}
						}
					}
					if (patient.getPharmacists() != null) {
						for (Pharmacist pharmacist : patient.getPharmacists()) {
							if (!userIds.contains(pharmacist.getUserDetails()
							                                .getId())) {
								userIds.add(pharmacist.getUserDetails()
								                      .getId());
							}
						}
					}
				}
				patientmodels.add(utility.convertPatientEntityTopatientModel(patient));
			}
		}
		return patientmodels;
	}

	@Override
	public List<PrescriptionModel> fetchPrescriptionsByCaregiverId(Long cargiverid) {
		List<Patient> patientList = patientRepository.findByCaregivers_id(cargiverid);
		List<Prescription> prescriptionListForAllPatientsUnderOnecaregiver = new ArrayList<Prescription>();
		for (Patient patient : patientList) {
			List<Prescription> prescriptionlist = prescriptionRepository.findByPatient_id(patient.getId());
			prescriptionListForAllPatientsUnderOnecaregiver.addAll(prescriptionlist);
		}
		List<PrescriptionModel> PrescriptionModelList = new ArrayList<PrescriptionModel>();
		for (Prescription prescription : prescriptionListForAllPatientsUnderOnecaregiver) {
			PrescriptionModel prescriptionModel = utility.convertPrescriptionEntityToModel(prescription);

			PrescriptionModelList.add(prescriptionModel);
		}
		return PrescriptionModelList;
	}

	@Override
	public List<CaregiverModel> fetchAdminAndSpecificProviderCargivers(Long userid, Authentication authentication) {
		List<CaregiverModel> caregivermodelList = new ArrayList<CaregiverModel>();
		if (userid == 0) {
			caregivermodelList = fetchAdminAndProviderCargivers(0L);
		} else {
			Provider provider = providerRepository.findByUserDetails_id(userid);
			if (provider != null) {
				caregivermodelList = fetchAdminAndProviderCargivers(provider.getId());
			}
		}
		return caregivermodelList;
	}
}
