package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.NDCImageModel;
import com.sg.dayamed.repository.MedicineRepository;
import com.sg.dayamed.service.MedicineService;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MedicineServiceImpl implements MedicineService {

	@Autowired
	private MedicineRepository medicineRepository;

	@Override
	public void updateImages(NDCImageModel ndcImageModel) throws DataValidationException {
		String imagePath = ndcImageModel.getImagePath();
		String urlDBPath = ndcImageModel.getUrlPathInDB();
		try {
			Optional<Path> pathOpt = getAnyImageInPath(imagePath);
			if (pathOpt.isPresent()) {
				String fileName = pathOpt.get()
				                         .getFileName()
				                         .toString();
				//Read the extension of the file
				String fileExt = fileName.substring(fileName.lastIndexOf("."));
				if (!StringUtils.isBlank(fileExt)) {
					//Read all the images from the given path by removing there file extension names
					List<Long> fileNames = getAllImageFileNames(imagePath);
					//Fetch all the records from DB with the list of file names matching the NDC
					List<Medicine> medicineList = medicineRepository.findByNdcCodeIn(fileNames);
					medicineList.forEach(m -> m.setImageURL(urlDBPath + m.getNdcCode() + fileExt));
					medicineRepository.saveAll(medicineList);
				}
			} else {
				throw new DataValidationException(APIErrorFields.IMAGE_PATH,
				                                  APIErrorKeys.ERROR_NO_IMAGE_IN_IMAGE_DIRECTORY,
				                                  new String[]{imagePath});
			}
		} catch (IOException ioException) {
			throw new DataValidationException(APIErrorFields.IMAGE_PATH,
			                                  APIErrorKeys.ERROR_IMAGE_DIRECTORY_NOT_ACCESSIBLE,
			                                  new String[]{imagePath});
		}
	}

	private List<Long> getAllImageFileNames(String imagePath) throws IOException {
		try (final Stream<Path> imageFilesList = Files.list(Paths.get(imagePath))) {
			return imageFilesList
					.map(f -> Long.parseLong(f.getFileName()
					                          .toString()
					                          .substring(0, f.getFileName()
					                                         .toString()
					                                         .lastIndexOf("."))))
					.collect(Collectors.toList());
		}
	}

	private Optional<Path> getAnyImageInPath(String imagePath) throws IOException {
		try (final Stream<Path> imageList = Files.list(Paths.get(imagePath))) {
			return imageList.findFirst();
		}
	}

	@Override
	public Page<Medicine> getMedicines(String nameFilter, int offset, int limit) {
		if (StringUtils.length(nameFilter) < 3) {
			return Page.empty();
		}
		return medicineRepository.findByNameContainingOrderByName(nameFilter, PageRequest.of(offset, limit));
	}
}
