package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.DeviceStatus;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.pojo.DeviceStatusRequestModel;
import com.sg.dayamed.repository.DeviceStatusRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.DeviceStatusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DeviceStatusServiceImpl implements DeviceStatusService {

	@Autowired
	DeviceStatusRepository deviceStatusRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Override
	public DeviceStatus addDeviceStatus(DeviceStatusRequestModel deviceStatusRequestModel) throws  Exception {
		Optional<UserDetails> optionalUserDetails  = userDetailsRepository.findById(deviceStatusRequestModel.getBelongingUserId());
		if(!optionalUserDetails.isPresent()){
			throw new DataValidationException("UserDetails",
			                                  IWSGlobalApiErrorKeys.ERRORS_GENERAL_RECORD_NOT_FOUND);
		}
		DeviceStatus deviceStatus = fetchDeviceStatusByUserID(deviceStatusRequestModel.getBelongingUserId());
		if(deviceStatus == null){
			deviceStatus = new DeviceStatus();
		}
		deviceStatus.setLatitude(deviceStatusRequestModel.getLatitude());
		deviceStatus.setLongitude(deviceStatusRequestModel.getLongitude());
		deviceStatus.setMsg(deviceStatusRequestModel.getMessage());
		deviceStatus.setBatteryStatus(deviceStatusRequestModel.getBatteryStatus());
		deviceStatus.setUserDetails(optionalUserDetails.get());
		deviceStatus.setStatus(deviceStatusRequestModel.getStatus());
		return deviceStatusRepository.save(deviceStatus);
	}

	@Override
	public DeviceStatus fetchDeviceStatusById(long deviceStatusId) {
		Optional<DeviceStatus> optionalDeviceStatus = deviceStatusRepository.findById(deviceStatusId);
		if (optionalDeviceStatus.isPresent()) {
			return optionalDeviceStatus.get();
		} else {
			return null;
		}
	}

	@Override
	public void deleteDeviceStatus(long deviceStatusId) {
		deviceStatusRepository.deleteById(deviceStatusId);
	}

	@Override
	public DeviceStatus fetchDeviceStatusByUserID(long userId) {
		return deviceStatusRepository.findByUserDetails_id(userId);
	}

}
