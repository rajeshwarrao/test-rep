package com.sg.dayamed.service.impl;

import com.sg.dayamed.component.CacheDataComponent;
import com.sg.dayamed.dao.EhCacheConstants;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.entity.Device;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.ConsumerGoodsRepository;
import com.sg.dayamed.repository.DeviceRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.MedicineRepository;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.service.PharmaService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PharmaServiceImpl implements PharmaService {

	@Autowired
	private DeviceRepository deviceRepository;

	@Autowired
	private PharmacistRepository pharmacistRepository;

	@Autowired
	private ConsumerGoodsRepository consumerGoodsRepository;

	@Autowired
	private DiseaseRepository diseaseRepository;

	@Autowired
	private MedicineRepository medicineRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private Utility utility;

	@Autowired
	private SendResetPwdNotification sendPwdResetNtf;

	@Autowired
	PasswordEncryption pwdEncryption;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;

	@Autowired
	private UserPreferenceService userPreferenceService;

	@Override
	public List<PharmacistModel> fetchPharmasists() {
		List<Pharmacist> pharmacistList = pharmacistRepository.findAll();
		List<PharmacistModel> pharmacistModelList = new ArrayList<PharmacistModel>();
		pharmacistList.forEach(pharmacist -> {
			if (pharmacist.getUserDetails()
			              .getUserOptions() == null) {
				PharmacistModel model = utility.convertPharmacistEntityToModel(pharmacist);
				pharmacistModelList.add(model);
			}
		});
		return pharmacistModelList;
	}

	@Override
	public List<Medicine> fetchMedicines() {
		List<Medicine> medicinelist = (List<Medicine>) medicineRepository.findAll();
		return medicinelist;
	}

	@Override
	public List<ConsumerGoods> fetchConsumergoods() {
		List<ConsumerGoods> consumerGoodslist = (List<ConsumerGoods>) consumerGoodsRepository.findAll();
		return consumerGoodslist;
	}

	@Override
	public List<Device> fetchAllDevices() {
		List<Device> devicelist = (List<Device>) deviceRepository.findAll();
		return devicelist;
	}

	@Override
	public List<Disease> fetchDiseases() {
		List<Disease> diseaselist = (List<Disease>) diseaseRepository.findAll();
		return diseaselist;
	}

	@Override
	public Pharmacist findByUserDetails_id(long userid) {
		return pharmacistRepository.findByUserDetails_id(userid);
	}

	@Autowired
	UserDetailsService userDetailsService;

	@Override
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public PharmacistModel addPharmacistValidation(PharmacistModel pharmacistModel, MultipartFile profileImage)
			throws DataValidationException {
		if (pharmacistModel != null) {
			UserDetailsModel pharmacistUserDetails = pharmacistModel.getUserDetails();
			if (StringUtils.isBlank(pharmacistUserDetails.getFirstName())
					|| StringUtils.isBlank(pharmacistUserDetails.getLastName())
					|| StringUtils.isBlank(pharmacistUserDetails.getRace())
					|| StringUtils.isBlank(pharmacistUserDetails.getEmailId())
					|| StringUtils.isBlank(pharmacistUserDetails.getGender())
					|| StringUtils.isBlank(pharmacistUserDetails.getCountryCode())
					|| StringUtils.isBlank(pharmacistUserDetails.getMobileNumber())
					|| StringUtils.isBlank(pharmacistUserDetails.getAddress()
					                                            .getLocation())
					|| StringUtils.isBlank(pharmacistUserDetails.getAddress()
					                                            .getZipCode())
					|| pharmacistUserDetails.validDateOfBirth(UserRoleEnum.PHARMACIST)) {
				throw new DataValidationException("invalidData", APIErrorKeys.ERROR_INPUT_BADREQUEST);
			}
		}
		// setting profilepic to userdetails imageurl
		if (profileImage != null && !profileImage.isEmpty() && pharmacistModel != null) {
			String oldImageName = ApplicationConstants.EMPTY_STRING;
			try {
				if (pharmacistModel.getUserDetails() != null
						&& StringUtils.isNotBlank(pharmacistModel.getUserDetails()
						                                         .getImageUrl())) {
					oldImageName = utility.getLastBitFromUrl(pharmacistModel.getUserDetails()
					                                                        .getImageUrl());
				}
				String imageNameWitPathUrl = utility.uploadProfilepic(profileImage,
				                                                      pharmacistModel.getUserDetails()
				                                                                     .getFirstName(),
				                                                      oldImageName);
				if (imageNameWitPathUrl != null) {
					pharmacistModel.getUserDetails()
					               .setImageUrl(imageNameWitPathUrl);
				}
			} catch (Exception e) {
				e.getMessage();
			}

		}
		if (pharmacistModel != null && pharmacistModel.getUserDetails() != null
				&& StringUtils.isNotBlank(pharmacistModel.getUserDetails()
				                                         .getEmailId())) {
			UserDetailsModel userDetailsModel = userDetailsService
					.findByEmailIdIgnoreCase(pharmacistModel.getUserDetails()
					                                        .getEmailId());
			if (userDetailsModel == null || pharmacistModel.getId() > 0) {
				if (userDetailsModel != null) {
					pharmacistModel.getUserDetails()
					               .setPassword(userDetailsModel.getPassword());
					pharmacistModel.getUserDetails()
					               .setType(StringUtils.isNotBlank(userDetailsModel.getType()) ?
							                        userDetailsModel.getType() :
							                        null);
				}
				PharmacistModel pharmacistmodel = addPharmacist(pharmacistModel);
				return pharmacistmodel;
			} else if (userDetailsModel != null && pharmacistModel.getId() == 0) {
				throw new DataValidationException(APIErrorKeys.ERROR_EMAIL_CONFLICT);
			}
		}
		return null;
	}

	@Override
	public PharmacistModel addPharmacist(PharmacistModel pharmacistModel) {
		Pharmacist pharmacist = utility.convertPharmasistModelToEntity(pharmacistModel);
		long userId = pharmacistModel.getUserDetails()
		                             .getId();
		if (userId == 0) {
			Role role = roleRepository.findByName("pharmacist");
			pharmacist.getUserDetails()
			          .setRole(role);
			pharmacist.getUserDetails()
			          .setPwdChangeToken(
					          pwdEncryption.encrypt(pharmacist.getUserDetails()
					                                          .getEmailId() + System.currentTimeMillis()));
			pharmacist.getUserDetails()
			          .setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));
		}

		UserDetails nonEncryptCaregiverUserDetails = pharmacist.getUserDetails();
		String pwdChangeToken = pwdEncryption.encrypt(pharmacist.getUserDetails()
		                                                        .getEmailId() + System.currentTimeMillis());
		pharmacist.getUserDetails()
		          .setPwdChangeToken(pwdChangeToken);
		pharmacist.getUserDetails()
		          .setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));
		Pharmacist persistdharmacist = pharmacistRepository.save(pharmacist);
		if (persistdharmacist != null && persistdharmacist.getUserDetails() != null) {
			userPreferenceService.addUserPreference(persistdharmacist.getUserDetails()
			                                                         .getId(), new UserPreferenceModel());
		}
		PharmacistModel pharmacistmodel = utility.convertPharmacistEntityToModel(persistdharmacist);
		nonEncryptCaregiverUserDetails.setPwdChangeToken(pwdChangeToken);
		// sending Email & Sms
		if (pharmacistmodel != null) {
			if (userId == 0) {
				sendPwdResetNtf.sendPwdResetNotificationWhenRegst(nonEncryptCaregiverUserDetails, "Pharmacist");
			} else {
				String subject = "Your profile has been updated in Dayamed";
				sendPwdResetNtf.sendEmailNotification(nonEncryptCaregiverUserDetails, "Pharmacist", subject, subject);
			}
		}
		return pharmacistModel;
	}

	@Override
	public String deletePharmacist(long pharmacistId) {
		Optional<Pharmacist> optionalPharmacist = pharmacistRepository.findById(pharmacistId);
		if (optionalPharmacist.isPresent()) {
			pharmacistRepository.delete(optionalPharmacist.get());
			return ApplicationConstants.SUCCESS;
		} else {
			return ApplicationConstants.NOT_EXIST;
		}
	}

	@Override
	public PharmacistModel fetchPharmacistById(Long pharmacistId) throws DataValidationException {
		if (pharmacistId == null || pharmacistId.longValue() == 0) {
			throw new DataValidationException("incorrect", APIErrorKeys.ERROR_INVALID_DATA);
		}
		Optional<Pharmacist> optionalPharmacist = pharmacistRepository.findById(pharmacistId);
		if (optionalPharmacist.isPresent()) {
			Pharmacist pharmacist = optionalPharmacist.get();
			return utility.convertPharmacistEntityToModel(pharmacist);
		}
		throw new DataValidationException("pharmacistNotFound", APIErrorKeys.ERROR_PHARMACIST_ID_NOT_IN_DB);
	}

	@Autowired
	PharmacistService pharmacistService;

	@Override
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public String deletePharmacistValidation(Long pharmacistId) throws DataValidationException {
		if (pharmacistId == null || pharmacistId.longValue() == 0) {
			throw new DataValidationException("incorrect", APIErrorKeys.ERROR_INVALID_DATA);
		}
		List<PatientModel> PatientModelList =
				pharmacistService.fetchPatientsByPharmacistId(pharmacistId);
		if (CollectionUtils.isNotEmpty(PatientModelList)) {
			throw new DataValidationException("pharmacist_associated", APIErrorKeys.ERROR_PHARMACIST_ASSOCIATED);
		}
		String result = deletePharmacist(pharmacistId);
		if (ApplicationConstants.NOT_EXIST.equalsIgnoreCase(result)) {
			throw new DataValidationException("pharmacistNotFound", APIErrorKeys.ERROR_PHARMACIST_ID_NOT_IN_DB);
		}
		return result;
	}
}
