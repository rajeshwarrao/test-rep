package com.sg.dayamed.service.impl;

import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.MyEntityRepositoryCustom;
import com.sg.dayamed.service.GoodShapredAlertProtocal;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodShapredAlertProtocalImpl implements GoodShapredAlertProtocal {

	private static final Logger logger = LoggerFactory.getLogger(GoodShapredAlertProtocalImpl.class);

	@Autowired
	RediseService rediseService;

	@Autowired
	MyEntityRepositoryCustom myEntityRepository;

	@Autowired
	SendResetPwdNotification sendResetPwdNotification;

	public String sendMissedAdherenceNotification() {
		try {
			//logger.info("==sendMissedAdherenceNotification===");
			String userDetailsNotificationQry = (String) rediseService
					.getDataInRedisByKey(RedisKeyConstants.DayamedAlertProtocalNotification);
			List<Map<String, String>> listUserDetailswithDosage =
					userDetailsNotificationQry != null ? myEntityRepository
							.getColmNameAndColumValueAsMap(userDetailsNotificationQry) : null;
			if (listUserDetailswithDosage != null && listUserDetailswithDosage.size() > 0) {
				for (Map<String, String> gsAlertProtocal : listUserDetailswithDosage) {
					String patientId = gsAlertProtocal.get("patientId");
					String gsNotifiStatus = gsAlertProtocal.get("gsNotificationStatus");
					String adharenceIdStr = gsAlertProtocal.get("adharenceId");
					if (!StringUtils.isEmpty(patientId) && !StringUtils.isEmpty(adharenceIdStr)) {
						//logger.info("GS Alert Protocal sending missed notification for patientId ==" + patientId);
						String recepientEamilAndMobileqry = (String) rediseService.getDataInRedisByKey(
								RedisKeyConstants.DayamedAlertProtocalNotificationRecepientEmailqry);
						recepientEamilAndMobileqry = recepientEamilAndMobileqry.replace("$patientId", patientId);
						List<Map<String, String>> recepientMap = myEntityRepository
								.getColmNameAndColumValueAsMap(recepientEamilAndMobileqry);
						if (recepientMap != null && recepientMap.size() > 0) {
							String userNameStr = gsAlertProtocal.get("userName");
							StringBuffer sbUserName = new StringBuffer();
							if (!StringUtils.isEmpty(userNameStr)) {
								for (String name : userNameStr.split(" ")) {
									sbUserName.append(" ")
									          .append(name);
								}
							}
							gsAlertProtocal.put("userName", sbUserName.toString()
							                                          .trim());
							String userName = sbUserName.toString()
							                            .trim();
							String actionStatus = gsAlertProtocal.get("actionStatus");
							String prescribedTime = gsAlertProtocal.get("prescribedTime");
							actionStatus = actionStatus != null && actionStatus.contains("action") ? "No"
									: actionStatus;
							StringBuffer emailSubject = new StringBuffer();
							emailSubject.append(userName)
							            .append(" ")
							            .append(actionStatus)
							            .append(" Action at " + prescribedTime);
							String alertProtocalEmailBody = (String) rediseService.getDataInRedisByKey(
									RedisKeyConstants.DayamedAlertProtocalNotificationRecepientEmailBody);
							StringBuffer emailBody = new StringBuffer(alertProtocalEmailBody);
							String emailBodyStr = replaceStrings(emailBody, gsAlertProtocal);
							// Sending email
							boolean sendEmail =
									sendResetPwdNotification.sendEmailNotificationByRecepients(recepientMap,
									                                                           emailSubject.toString(),
									                                                           emailBodyStr);
							boolean sendSMS = true;
							String alertProtocalSMSEnabled = (String) rediseService.getDataInRedisByKey(
									RedisKeyConstants.DayamedAlertProtocalNotificationRecepientSMSEnabled);
							if ("true".equalsIgnoreCase(alertProtocalSMSEnabled)) {
								String alertProtocalSMSBody = (String) rediseService.getDataInRedisByKey(
										RedisKeyConstants.DayamedAlertProtocalNotificationRecepientSMSBody);
								StringBuffer smsBody = new StringBuffer(alertProtocalSMSBody);
								String smsBodyStr = replaceStrings(smsBody, gsAlertProtocal);
								// sending SMS
								sendSMS = sendResetPwdNotification.sendSMSNotification(recepientMap, smsBodyStr);
							}
							if (sendEmail && sendSMS) {
								String alertProtocalUpdateqry = (String) rediseService.getDataInRedisByKey(
										RedisKeyConstants.DayamedAlertProtocalNotifAdharenceUpdatestatsQry);

								int gsNotificationStatus =
										gsNotifiStatus == null ? 0 : Integer.parseInt(gsNotifiStatus);
								gsNotificationStatus = gsNotificationStatus + 1;
								int adharenceId = Integer.parseInt(adharenceIdStr);
								Map<Integer, Object> stmtMap = new HashMap<>();
								stmtMap.put(1, gsNotificationStatus);
								stmtMap.put(2, adharenceId);
								myEntityRepository.updateQueryPrepareStamt(alertProtocalUpdateqry, stmtMap);
							} else {
								logger.error("sendMissedAdherenceNotification -- Issue with email sent==" + sendEmail +
										             "===sendSMS==>" + sendSMS);
							}
						}
					}
				}
			} else {
				logger.info("GS Alert Protocal userdetails with dosage is null == sendMissedAdherenceNotification==>"
						            + listUserDetailswithDosage);
			}
		} catch (Exception e) {
			logger.error("Exception occurred in the method sendMissedAdherenceNotification", e);
		}
		return null;
	}

	public String replaceStrings(StringBuffer strBuffer, Map<String, String> gsAlertProtocal) {
		String userName = gsAlertProtocal.get("userName");
		String actionStatus = gsAlertProtocal.get("actionStatus");
		String prescribedTime = gsAlertProtocal.get("prescribedTime");
		actionStatus = actionStatus != null && actionStatus.contains("action") ? "No" : actionStatus;
		if (!StringUtils.isEmpty(userName)) {
			strBuffer = new StringBuffer(strBuffer.toString()
			                                      .replace("$userName", userName));
		}
		if (!StringUtils.isEmpty(actionStatus)) {
			strBuffer = new StringBuffer(strBuffer.toString()
			                                      .replace("$actionStatus", actionStatus));
		}
		if (!StringUtils.isEmpty(prescribedTime)) {
			strBuffer = new StringBuffer(strBuffer.toString()
			                                      .replace("$prescribedTime", prescribedTime));
		}
		String medicineName = gsAlertProtocal.get("medicineName");
		if (!StringUtils.isEmpty(medicineName)) {
			strBuffer = new StringBuffer(strBuffer.toString()
			                                      .replace("$medicineName", medicineName));
		}
		return strBuffer.toString();
	}
}
