package com.sg.dayamed.service.impl;

import com.sg.dayamed.dosage.frequency.FrequencyAbstractFactory;
import com.sg.dayamed.dosage.frequency.FrequencyFactory;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.helper.PrescriptionHelper;
import com.sg.dayamed.helper.pojo.DeletePrescriptionVO;
import com.sg.dayamed.helper.pojo.GenericIDAndZoneVO;
import com.sg.dayamed.helper.pojo.UpdatePrescriptionStatusVO;
import com.sg.dayamed.pojo.ConsumerGoodInfoModel;
import com.sg.dayamed.pojo.DosageInfoModel;
import com.sg.dayamed.pojo.FristConsumptionTypeModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PharmaService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.ApplicationErrors;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.MessageResourceUtility;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PrescriptionServiceImpl implements PrescriptionService {

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	Utility utility;

	@Autowired
	NotificationScheduleService jobService;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	PatientService patientService;

	@Autowired
	PHIDataEncryptionService pHIDataEncryptionService;

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	PharmaService pharmaService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	PrescriptionStatusRepository prescriptionStatusRepository;

	@Autowired
	AdherenceService adherenceService;

	@Autowired
	FrequencyAbstractFactory frequencyAbstractFactory;

	@Autowired
	PrescriptionHelper prescriptionHelper;

	@Autowired
	MessageResourceUtility messageResourceUtility;

	private static final Logger logger = LoggerFactory.getLogger(PrescriptionServiceImpl.class);

	@Override
	public PrescriptionModel addPrescription(PrescriptionModel prescriptionModel, long pat_id,
	                                         JwtUserDetails jwtUser) throws DataValidationException {

		Patient patient = patientService.directFetchPatientByPatientId(pat_id);
		if (patient == null) {
			logger.error(ApplicationErrors.NOT_FOUND_PATIENT_IN_DB);
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
		int result = utility.instanceOfPatient(patient, jwtUser);
		if (result == 401) {
			throw new DataValidationException(IWSGlobalApiErrorKeys.ERRORS_AUTHORIZATION_FAILED);
		}
		PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
		prescriptionModel.setPatient(patientModel);
		// New code for caregver start
		long userid = jwtUser.getUserId();

		if (jwtUser != null && UserRoleEnum.CAREGIVER.getRole()
		                                             .equalsIgnoreCase(jwtUser.getRole())) {
			List<ConsumerGoodInfoModel> consumerGoodInfoModelList = prescriptionModel.getCommodityInfoList();
			List<DosageInfoModel> dosageInfoModelList = prescriptionModel.getDosageInfoList();

			prescriptionModel = fetchPrescriptionByPrescriptionId(prescriptionModel.getId(),
			                                                      userid);
			prescriptionModel.setCommodityInfoList(consumerGoodInfoModelList);
			prescriptionModel.setDosageInfoList(dosageInfoModelList);
		}
		prescriptionModel = prescriptionHelper.addPrescription(prescriptionModel, patient, userid);
		return prescriptionModel;
	}

	@Override
	public PrescriptionModel validateFetchLatestPrescriptionByPatientId(long patientId, Authentication authentication)
			throws ParseException, DataValidationException {
		Patient patient = patientService.fetchPatientById(patientId);
		if (patient == null) {
			logger.error(ApplicationErrors.NOT_FOUND_PATIENT_IN_DB);
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
		PrescriptionModel prescriptionmodel = utility.convertPrescriptionEntityToModel(
				fetchLatestPrescriptionByPatientId(patientId));
		String _12hrFormat = dateUtility.convertDate24hoursFormatTo12hourForamat(prescriptionmodel.getDate());
		prescriptionmodel.setDate(_12hrFormat);
		String keyText = pHIDataEncryptionService.getKeyText(null, authentication);
		prescriptionmodel.setPatient(
				pHIDataEncryptionService.encryptPatient(prescriptionmodel.getPatient(), keyText));
		return prescriptionmodel;
	}

	@Override
	public List<PrescriptionModel> fetchPrescriptionListByPatientId(long patientId, Authentication authentication
	) throws DataValidationException {
		Patient patient = patientService.fetchPatientById(patientId);
		if (patient == null) {
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
		List<PrescriptionModel> prescriptionsModelList = patientService
				.fetchPrescriptionlistByPatientId(patientId);
		for (PrescriptionModel prescriptionModel : prescriptionsModelList) {
			String keyText = pHIDataEncryptionService.getKeyText(null, authentication);
			prescriptionModel.setPatient(
					pHIDataEncryptionService.encryptPatient(prescriptionModel.getPatient(), keyText));
		}
		return prescriptionsModelList;
	}

	//should not decript
	@Override
	public PrescriptionModel fetchPrescriptionByPrescriptionId(long id, long userid) throws DataValidationException {
		PrescriptionModel prescriptionModel = null;
		Optional<Prescription> optionalPrescription = prescriptionRepository.findById(id);
		// to show only unsoftdeleted dosageinfo list of that particular
		// prescription
		if (optionalPrescription.isPresent() && userid != 0) {
			optionalPrescription.get()
			                    .getDosageInfoList()
			                    .forEach(dosageInfo -> {
				                    if (dosageInfo.getDosageNotificationList() != null) {
					                    List<DosageNotification> dosageInfoNotificationList =
							                    new ArrayList<DosageNotification>();
					                    dosageInfo.getDosageNotificationList()
					                              .forEach(dosageNotification -> {
						                              if (dosageNotification.getUserId() == userid) {
							                              dosageInfoNotificationList.add(dosageNotification);
						                              }
					                              });
					                    dosageInfo.setDosageNotificationList(dosageInfoNotificationList);
				                    }
			                    });
		}
		if (optionalPrescription.isPresent()) {
			Prescription processedPrescription =
					utility.fetchUnsoftDeletedPrescriptionInfo(optionalPrescription.get());
			prescriptionModel = utility.convertPrescriptionEntityToModel(processedPrescription);
			return prescriptionModel;
		} else {
			logger.warn(ApplicationErrors.NOT_FOUND_PRESCRIPTION_WITH_ID);
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
	}

	@Override
	public Prescription fetchLatestPrescriptionByPatientId(long patientId) {
		Prescription prescription = prescriptionRepository.findTop1ByPatient_idOrderByUpdatedDateDesc(patientId);
		Prescription processedPrescription = utility.fetchUnsoftDeletedPrescriptionInfo(prescription);
		return processedPrescription;
	}

	@Override
	public boolean createSchedulerJobsByPrescription(long prescriptionId, String zoneid)
			throws DataValidationException {

		PrescriptionModel prescriptionModel = fetchPrescriptionByPrescriptionId(prescriptionId, 0);
		Prescription prescription = utility.convertPrescriptionModelToEntity(prescriptionModel);
		boolean result = false;
		if (prescription != null) {
			result = jobService.addSchedulerJob(prescription, zoneid);
		}
		return result;
	}

	private void generateDevisePrescriptionSchuleJobs(long prescriptionId, String zoneid, Prescription prescription,
	                                                  LocalDateTime localDateTime) {

		for (DeviceInfo deviceInfo : prescription.getDeviceInfoList()) {

			int durationLength = deviceInfo.getDuration();
			//int durationLength = 5;
			if (deviceInfo.getDuration() <= 0) {
				durationLength = 1;
			}
			LocalDate prescriptionStartDate = localDateTime.toLocalDate();
			DateUtility dateUtility = new DateUtility();
			for (int i = 0; i < durationLength; i++) {
				LocalDate localDurationDate = dateUtility.addDaysToLocalDate(prescriptionStartDate, i);
				for (String time : deviceInfo.getTime()) {
					/*
					 * for every time in dosageInfo need to create one
					 * row in prescriptionSchedule table
					 */
					// insted of creating two colmns make it as one
					LocalDateTime durationDatewithTime = dateUtility.addTimestringToLacalDate(time,
					                                                                          localDurationDate);
					// this durationDatewithTime has to convert it to
					// UTC ,dateformat :dd-M-yyyy hh:mm:ss
					String durationDatewithTimeUTC = dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
							dateUtility.getFormattedLocalDateTime(durationDatewithTime), zoneid);

					//if trigger time lessthan utc current time
					if (i == 0) {
						LocalDateTime localDateTimeUTC =
								dateUtility.convertdatetimeStringToLocalDateTime(durationDatewithTimeUTC);
						LocalDateTime currentLocalDateTimeUTC = LocalDateTime.now(ZoneOffset.UTC);

						if (localDateTimeUTC.compareTo(currentLocalDateTimeUTC) < 0) {
							localDurationDate =
									dateUtility.addDaysToLocalDate(prescriptionStartDate, durationLength);
							durationDatewithTime = dateUtility.addTimestringToLacalDate(time,
							                                                            localDurationDate);
							durationDatewithTimeUTC = dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
									dateUtility.getFormattedLocalDateTime(durationDatewithTime), zoneid);
						}
					}

					PrescriptionSchedule prescriptionSchedule = new PrescriptionSchedule();
					prescriptionSchedule.setPrescribedTime(
							dateUtility.convertUTCLocalDateTimeStringToObject(durationDatewithTimeUTC));
					LocalDateTime targetfireTime = durationDatewithTime
							.plusMinutes(5);//5 min adding to prescribed time
					String fireDatewithTimeUTC = dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
							dateUtility.getFormattedLocalDateTime(targetfireTime), zoneid);
					prescriptionSchedule.setFireTime(
							dateUtility.convertUTCLocalDateTimeStringToObject(fireDatewithTimeUTC));

					prescriptionSchedule.setDeviceInfo(deviceInfo);
					prescriptionSchedule.setUserId(prescription.getPatient()
					                                           .getUserDetails()
					                                           .getId());
					prescriptionSchedule.setPrescriptionId(prescriptionId);
					prescriptionScheduleRepository.save(prescriptionSchedule);
				}
			}
		}
	}

	private void generateDosagePrescriptionSchuleJobs(long prescriptionId, String zoneid, Prescription prescription) {
		for (DosageInfo dosageInfo : prescription.getDosageInfoList()) {
			//new code based on frequency
			String frequencyJson = dosageInfo.getFrequency();
			try {
				if (!StringUtils.isEmpty(frequencyJson)) {
					JSONObject jsonObj = new JSONObject(frequencyJson);
					String type = jsonObj.getString("type");
					jsonObj.put("patientTimeZone", zoneid);
					String updatedfrequencyJson = jsonObj.toString();
					if (!StringUtils.isEmpty(type)) {
						List<String> listAlrams = new ArrayList<>();
						FrequencyFactory frequencyFactory =
								frequencyAbstractFactory.getFrequencyFactory(type);
						if (frequencyFactory != null) {
							if (dosageInfo.getConsumptionTemplate() == null) {
								listAlrams = frequencyFactory.frequencyCalculation(updatedfrequencyJson,
								                                                   dosageInfo.getTime());
							} else if (dosageInfo.getConsumptionTemplate() != null &&
									ApplicationConstants.CUSTOM.equalsIgnoreCase(dosageInfo.getConsumptionTemplate()
									                                                       .getConsumptionsKey())) {
								//To Do custom
								listAlrams = frequencyFactory.frequencyCalculation(updatedfrequencyJson,
								                                                   dosageInfo.getTime());
							} else {
								ConsumptionTemplate consumptionTemplate = dosageInfo.getConsumptionTemplate();
								if (consumptionTemplate != null) {
									String[] timeArray = consumptionTemplate.getConsumptionsTimes()
									                                        .split(",");
									List<String> consumptionTimeList = new ArrayList<>();
									consumptionTimeList.addAll(Arrays.asList(timeArray));
									listAlrams = frequencyFactory.frequencyCalculation(updatedfrequencyJson,
									                                                   consumptionTimeList);
								}
							}
						}
						//dosageInfo.setAlaramscheduleList(listAlrems); llistAlarms from DB in UTC
						for (String dosageAlaram : listAlrams) {
							// UTC ,dateformat :dd-M-yyyy hh:mm:ss
							String durationDatewithTimeUTC =
									dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
											dateUtility.getFormattedLocalDateTime(
													dateUtility.convertdatetimeStringToLocalDateTime(
															dosageAlaram)), zoneid);
							//have to create prescription schdule record for every alarm.
							PrescriptionSchedule prescriptionSchedule = new PrescriptionSchedule();
							prescriptionSchedule.setPrescribedTime(
									dateUtility.convertUTCLocalDateTimeStringToObject(
											durationDatewithTimeUTC));
							LocalDateTime targetfireTime =
									dateUtility.convertUTCLocalDateTimeStringToObject(
											durationDatewithTimeUTC)
									           .plusMinutes(5);//5 min adding to prescribed time

							prescriptionSchedule.setFireTime(targetfireTime);
							prescriptionSchedule.setDosageInfo(dosageInfo);
							prescriptionSchedule.setUserId(prescription.getPatient()
							                                           .getUserDetails()
							                                           .getId());
							prescriptionSchedule.setPrescriptionId(prescriptionId);
							prescriptionScheduleRepository.save(prescriptionSchedule);
						}
					}
				}
				// end code frequency
			} catch (Exception e) {
				logger.error("Exception when generating prescription schedule", e);
			}
		}
	}

	// code with frequency
	@Override
	public boolean generatePrescriptionSchedule(long prescriptionId, String zoneid) throws DataValidationException {

		PrescriptionModel prescriptionModel = fetchPrescriptionByPrescriptionId(prescriptionId, 0);
		Prescription prescription = utility.convertPrescriptionModelToEntity(prescriptionModel);
		boolean result = false;
		if (prescription != null) {
			//result = jobService.addSchedulerJob(prescription,zoneid);
			final String DATE_Time_FORMAT = "yyyy-MM-dd HH:mm";

			if (StringUtils.isNotBlank(zoneid)) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_Time_FORMAT);
				String prescriptionStartDateString = dateUtility.getDateTimeBasedOnZoneId(zoneid);
				LocalDateTime localDateTime = LocalDateTime.parse(prescriptionStartDateString, formatter);

				List<PrescriptionSchedule> existedPrescriptionScheduleList =
						prescriptionScheduleRepository.findByUserIdAndPrescriptionId(prescription.getPatient()
						                                                                         .getUserDetails()
						                                                                         .getId(),
						                                                             prescriptionId);
				if (CollectionUtils.isNotEmpty(existedPrescriptionScheduleList)) {
					prescriptionScheduleRepository.deleteAll(existedPrescriptionScheduleList);
				}
				if (prescription.getDosageInfoList() != null) {
					generateDosagePrescriptionSchuleJobs(prescriptionId, zoneid, prescription);
				}
				//devicenotification jobs
				if (prescription.getDeviceInfoList() != null) {
					generateDevisePrescriptionSchuleJobs(prescriptionId, zoneid, prescription, localDateTime);
				}
				result = true;
			}
		} else {
			return result;
		}
		return result;
	}

	@Override
	public Prescription fetchPrescriptionEntityByPrescriptionId(long id) {
		Optional<Prescription> optionalPrescription = prescriptionRepository.findById(id);
		return optionalPrescription.isPresent() ? optionalPrescription.get() : null;
	}

	@Override
	public Prescription addPrescription(Prescription prescription) {
		Prescription preScription = prescriptionRepository.save(prescription);
		return preScription;
	}

	@Override
	public List<Prescription> fetchPrescriptionsByPatientId(long patientId) {
		List<Prescription> prescriptionList = prescriptionRepository.findByPatient_id(patientId);
		List<Prescription> processedPrescriptionList = new ArrayList<Prescription>();
		for (Prescription prescription : prescriptionList) {
			Prescription processedPrescription = utility.fetchUnsoftDeletedPrescriptionInfo(prescription);
			processedPrescriptionList.add(processedPrescription);
		}
		return processedPrescriptionList;
	}

	@Override
	public List<Prescription> fetchActivePrescriptionsByPatientId(long patientId, LocalDateTime currentDate) {
		return (List<Prescription>) prescriptionRepository.findByPatient_idAndEndDateGreaterThanOrEndDateIsNull(
				patientId, currentDate);
	}

	public String deletePrescriptionBySP(long presId) {
		return prescriptionRepository.deletePrescription(presId);
	}

	@Override
	public List<PrescriptionModel> fetchActivePrescriptionListByPatientId(GenericIDAndZoneVO genericIDAndZoneVO,
	                                                                      Authentication authentication)
			throws ApplicationException, DataValidationException {
		try {
			String id = genericIDAndZoneVO.getPatientId();
			String zoneId = genericIDAndZoneVO.getZoneId();
			//fetchActivePrescriptionsByPatientId
			logger.debug("fetchActivePrescriptionListByPatientId ===>" + id);
			List<Prescription> prescriptionList = fetchPrescriptionsByPatientId(Long.valueOf(id));

			List<PrescriptionModel> activePrescriptionModelList = new ArrayList<PrescriptionModel>();
			String keyText = pHIDataEncryptionService.getKeyText(null, authentication);
			for (Prescription prescription : prescriptionList) {
				if (prescription.getEndDate() == null) {
					prescription.setPatient(null);
					prescription.setPrescriptionStatus(null);
					PrescriptionModel prescriptionModel = utility.convertPrescriptionEntityToModel(prescription);
					prescriptionModel.setPatient(
							pHIDataEncryptionService.encryptPatient(prescriptionModel.getPatient(), keyText));
					for (DosageInfoModel dosageInfoModel : prescriptionModel.getDosageInfoList()) {
						String frequencyJson = dosageInfoModel.getFrequency();
						if (StringUtils.isNotBlank(frequencyJson)) {
							JSONObject jsonObj = new JSONObject(frequencyJson);
							String type = jsonObj.getString("type");
							if (StringUtils.isNotBlank(type)) {
								jsonObj.put("patientTimeZone", zoneId);
								FrequencyFactory frequencyFactory = frequencyAbstractFactory.getFrequencyFactory(type);
								if (frequencyFactory != null) {
									if (dosageInfoModel.getFristConsumptionType() == null) {
										List<String> listAlarms =
												frequencyFactory.frequencyCalculation(jsonObj.toString(),
												                                      dosageInfoModel.getTime());
										dosageInfoModel.setAlaramscheduleList(listAlarms);
									} else {
										FristConsumptionTypeModel firstConsumptionType =
												dosageInfoModel.getFristConsumptionType();
										if (firstConsumptionType != null) {
											if (ApplicationConstants.CUSTOM.equals(
													firstConsumptionType.getFirstConsumptionTime()
													                    .getName())) {
												List<String> listAlarms =
														frequencyFactory.frequencyCalculation(jsonObj.toString(),
														                                      dosageInfoModel.getTime());
												dosageInfoModel.setAlaramscheduleList(listAlarms);
											} else {
												String[] intakeTimeArray =
														firstConsumptionType.getFirstConsumptionTime()
														                    .getTimes()
														                    .split(",");
												List<String> firstConsumptionIntakeTimeList =
														Arrays.stream(intakeTimeArray)
														      .filter(value -> org.apache.commons.lang.StringUtils.isNotBlank(
																      value))
														      .map(value -> value.trim())
														      .collect(Collectors.toList());
												List<String> listAlarms =
														frequencyFactory.frequencyCalculation(jsonObj.toString(),
														                                      firstConsumptionIntakeTimeList);
												dosageInfoModel.setAlaramscheduleList(listAlarms);
											}
										}
									}
								}
							}
						}
					}
					activePrescriptionModelList.add(prescriptionModel);
				}
			}
			return activePrescriptionModelList;
		} catch (Exception ae) {
			throw new DataValidationException(APIErrorKeys.ERROR_INVALID_DATA);
		}
	}

	@Override
	public PrescriptionModel validateFetchPrescriptionByPrescriptionId(long preid,
	                                                                   Authentication authentication)
			throws DataValidationException {
		PrescriptionModel prescriptionmodel = null;
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		if (jwtUser.getUserId() != null) {
			//prescripton fetching based on caregiver or provider
			prescriptionmodel = fetchPrescriptionByPrescriptionId(preid, jwtUser.getUserId());
			if (prescriptionmodel == null) {
				logger.error(ApplicationErrors.NOT_FOUND_PRESCRIPTION_WITH_ID);
				throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
			}
			int result = utility.instanceOfPrescriptionModel(prescriptionmodel, jwtUser);
			if (result == 401) {
				throw new DataValidationException(IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
			}
			String keyText = pHIDataEncryptionService.getKeyText(null, authentication);
			prescriptionmodel.setPatient(
					pHIDataEncryptionService.encryptPatient(prescriptionmodel.getPatient(), keyText));
		}
		return prescriptionmodel;
	}

	@Override
	public PrescriptionModel updatePrescriptionStatus(PrescriptionModel prescriptionModel,
	                                                  Authentication authentication) {

		Prescription Prescription = utility.convertPrescriptionModelToEntity(prescriptionModel);
		// Prescription.setDate(Date.from(Instant.now()));
		Prescription.setUpdatedDate(LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC));
		prescriptionModel = patientService.updatePrescription(Prescription);
		// send notification to patient,caregiver
		String msg = ApplicationConstants.EMPTY_STRING;
		String title = ApplicationConstants.EMPTY_STRING;
		if (prescriptionModel.getPrescriptionStatus()
		                     .getName()
		                     .equalsIgnoreCase("InProcess")) {
			msg = "Prescription is Ready to Shipment ";
			title = "Prescription order is in process";
		} else if (prescriptionModel.getPrescriptionStatus()
		                            .getName()
		                            .equalsIgnoreCase("Hold")) {
			msg = "Prescription kept hold ";
			title = "Prescription order is in hold";
		}
		if (prescriptionModel.getPatient() != null && Prescription.getPatient()
		                                                          .getUserDetails() != null) {
			// send notification to patient
			notificationService.sendMobileNotification(prescriptionModel.getPatient()
			                                                            .getUserDetails()
			                                                            .getType(), msg,
			                                           title, prescriptionModel.getPatient()
			                                                                   .getUserDetails()
			                                                                   .getToken());
			String keyText = pHIDataEncryptionService.getKeyText(null, authentication);
			prescriptionModel
					.setPatient(pHIDataEncryptionService.encryptPatient(prescriptionModel.getPatient(), keyText));
		}
		return prescriptionModel;
	}

	@Override
	public String updatePrescriptionStatusByStatusString(UpdatePrescriptionStatusVO updatePrescriptionStatusVO) {

		PrescriptionStatus prescriptionStatus =
				prescriptionStatusRepository.findByName(updatePrescriptionStatusVO.getStatus());
		if (prescriptionStatus == null) {
			return ApplicationConstants.BAD_REQUEST;
		}
		Optional<Prescription> optionalPrescription =
				prescriptionRepository.findById(Long.parseLong(updatePrescriptionStatusVO.getPrescriptionId()));
		if (optionalPrescription.isPresent()) {
			Prescription Prescription = optionalPrescription.get();
			Prescription.setPrescriptionStatus(prescriptionStatus);
			patientService.updatePrescription(Prescription);
			return "Status updated";
		} else {
			return ApplicationConstants.BAD_REQUEST;
		}
	}

	@Override
	public DeletePrescriptionVO deletePrescriptionById(long prescriptionId) {
		List<AdherenceDataPoints> listAdherence = adherenceService.findByprescriptionID(prescriptionId);
		DeletePrescriptionVO deletePrescriptionVO = new DeletePrescriptionVO();
		if (listAdherence != null && listAdherence.size() > 0) {
			deletePrescriptionVO.setStatus("fail");
			deletePrescriptionVO.setMessage(
					"Prescription cannot be deleted. Patient started medication and adherence data captured");
			return deletePrescriptionVO;
		}
		//need to validate whether prescription belongs to this provider or not.
		//Optional<Prescription> optionalPrescription = prescriptionRepository.findById(prescriptionId);
		String status = deletePrescriptionBySP(prescriptionId);
		deletePrescriptionVO.setStatus(status);
		String msg = status;
		deletePrescriptionVO.setMessage(status);
		if (StringUtils.equalsIgnoreCase(status, "success")) {
			msg = messageResourceUtility.getErrorMessageFromResource(APIErrorKeys.SUCCESS_PRESCRIPTION_DELETED, null);
			deletePrescriptionVO.setMessage(msg);
		}
		return deletePrescriptionVO;
	}

}
