package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.repository.ScaleRepository;
import com.sg.dayamed.service.ScaleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ScaleServiceImpl implements ScaleService {

	@Autowired
	ScaleRepository scaleRepository;

	/* (non-Javadoc)
	 * @see com.sg.dayamed.service.ScaleService#findByPrescribedTime(java.time.LocalDateTime)
	 */
	@Override
	public List<Scale> findByPrescribedTime(LocalDateTime actualDate) {
		return scaleRepository.findByPrescribedTime(actualDate);
	}

}
