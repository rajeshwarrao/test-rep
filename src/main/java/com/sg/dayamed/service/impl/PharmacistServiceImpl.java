package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.Utility;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("PharmacistService")
@Primary
public class PharmacistServiceImpl implements PharmacistService {

	@Autowired
	PharmacistRepository pharmacistRepository;

	@Autowired
	Utility utility;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	PHIDataEncryptionService pHIDataEncryptionService;

	@Override
	public List<PharmacistModel> fetchPharmacistListByPatientId(Long patientId) throws DataValidationException {
		if (patientId == null || patientId.longValue() == 0) {
			throw new DataValidationException("incorrect", APIErrorKeys.ERROR_INVALID_DATA);
		}
		List<Pharmacist> pharmacistList = pharmacistRepository.findByPatients_id(patientId);
		if (CollectionUtils.isEmpty(pharmacistList)) {
			throw new DataValidationException("pharmacistNotFound", APIErrorKeys.ERROR_PHARMACIST_ID_NOT_IN_DB);
		}
		List<PharmacistModel> pharmacistModelList = new ArrayList<PharmacistModel>();
		pharmacistList.forEach(pharmacist -> {
			pharmacistModelList.add(utility.convertPharmacistEntityToModel(pharmacist));
		});
		return pharmacistModelList;
	}

	@Override
	public PharmacistModel fetchPharmacistByUserId(Long userId) throws DataValidationException {
		if (userId == null || userId.longValue() == 0) {
			throw new DataValidationException("incorrect", APIErrorKeys.ERROR_INVALID_DATA);
		}
		Pharmacist pharmacist = pharmacistRepository.findByUserDetails_id(userId);
		if (pharmacist == null) {
			throw new DataValidationException("pharmacistNotFound", APIErrorKeys.ERROR_PHARMACIST_ID_NOT_IN_DB);
		}
		return utility.convertPharmacistEntityToModel(pharmacist);
	}

	@Override
	public List<PatientModel> fetchPatientsByPharmacistId(long id) {
		List<Patient> patientList = patientRepository.findByPharmacists_idAndStatus(id, 0);
		List<PatientModel> patientModels = new ArrayList<PatientModel>();
		patientList.forEach(patient -> {
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
			patientModels.add(patientModel);
		});
		return patientModels;
	}

	@Override
	public PharmacistModel addPharmacist(Pharmacist pharmacist) {
		Pharmacist pharmaciSt = pharmacistRepository.save(pharmacist);
		return utility.convertPharmacistEntityToModel(pharmaciSt);
	}

	@Override
	public Pharmacist fetchPharmacistById(long pharmacistId) {
		Optional<Pharmacist> optionalPharmacist = pharmacistRepository.findById(pharmacistId);
		return optionalPharmacist.isPresent() ? optionalPharmacist.get() : null;
	}

	@Override
	public Pharmacist directFetchPharmacistById(long pharmacistId) {
		Optional<Pharmacist> optionalPharmacist = pharmacistRepository.findById(pharmacistId);
		return optionalPharmacist.isPresent() ? optionalPharmacist.get() : null;
	}

	@Override
	public Pharmacist directFetchPharmacistByUserId(long userId) {
		Pharmacist pharmacist = pharmacistRepository.findByUserDetails_id(userId);
		return pharmacist;
	}

	@Override
	public List<PatientModel> getPatientsByPharmacistIdentifier(JwtUserDetails jwtUser) throws DataValidationException {
		if (jwtUser != null && jwtUser.getUserId() != null) {
			Pharmacist pharmacist = utility
					.convertPharmasistModelToEntity(fetchPharmacistByUserId(jwtUser.getUserId()));
			List<PatientModel> patientList = fetchPatientsByPharmacistId(pharmacist.getId());
			List<PatientModel> prescriptionPatientList = new ArrayList<PatientModel>();
			for (PatientModel patientModel : patientList) {
				if (CollectionUtils.isNotEmpty(patientModel.getPrescriptions())) {
					prescriptionPatientList.add(patientModel);
				}
			}
			String keyText = pHIDataEncryptionService.getKeyText(jwtUser);
			return pHIDataEncryptionService
					.encryptPatientList(prescriptionPatientList, keyText);
		}
		throw new DataValidationException("incorrect", APIErrorKeys.ERROR_INVALID_DATA);
	}
}
