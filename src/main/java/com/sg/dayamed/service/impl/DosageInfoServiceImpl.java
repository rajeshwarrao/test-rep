package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.service.DosageInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DosageInfoServiceImpl implements DosageInfoService {

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	@Override
	public DosageInfo fetctDosageInfoByID(long dosageInfoId) {
		Optional<DosageInfo> optionalDosageInfo = dosageInfoRepository.findById(dosageInfoId);
		if (optionalDosageInfo.isPresent()) {
			return optionalDosageInfo.get();
		} else {
			return null;
		}
	}

	@Override
	public DosageInfo fetchMaxDurationDosageByPrescription_id(long prescriptionId) {
		DosageInfo dosageInfo =
				dosageInfoRepository.findTop1ByPrescription_idAndDeleteflagOrderByDurationDesc(prescriptionId, false);
		return dosageInfo;
	}

	@Override
	public DosageInfo updateDosageInfo(DosageInfo dosageInfo) {
		return dosageInfoRepository.save(dosageInfo);
	}

}
