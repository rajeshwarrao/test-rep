package com.sg.dayamed.service.impl;

import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.repository.AdherenceRepository;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.util.ApplicationConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AdherenceServiceImpl implements AdherenceService {

	@Autowired
	AdherenceRepository adherenceRepository;

	@Autowired
	ImanticService imanticService;

	@Autowired
	EmailService emailService;

	@Autowired
	PrescriptionService prescriptionService;

	@Override
	public AdherenceDataPoints addAdherence(AdherenceDataPoints adherence, String prescribedTime) {
		AdherenceDataPoints persistedAdherence = adherenceRepository.save(adherence);
		// --- send medication taken events to Imantics
		// Only consumed events we are sending to imantics
		//converting set to list.
		List<String> consumptionstatusList = new ArrayList<String>(adherence.getConsumptionStatus());
		if (consumptionstatusList.get(adherence.getConsumptionStatus()
		                                       .size() - 1)
		                         .equalsIgnoreCase("consumed")) {
			imanticService.medicationTaken(adherence, 1, prescribedTime);
		}
		if (consumptionstatusList.get(adherence.getConsumptionStatus()
		                                       .size() - 1)
		                         .equalsIgnoreCase("skipped") || consumptionstatusList.get(0)
		                                                                              .equalsIgnoreCase(
				                                                                              ApplicationConstants.No_Action)) {
			imanticService.medicationTaken(adherence, 0, prescribedTime);
		}
		return persistedAdherence;
	}

	@Override
	public AdherenceDataPoints fetchAdherenceById(long id) {
		Optional<AdherenceDataPoints> optionalPersistedAdherence = adherenceRepository.findById(id);
		if (optionalPersistedAdherence.isPresent()) {
			return optionalPersistedAdherence.get();
		} else {
			return null;
		}
	}

	@Override
	public List<AdherenceDataPoints> findByDosageInfo_idIn(List<Long> dosageinfoidlist) {
		List<AdherenceDataPoints> adherenceList = new ArrayList<>();
		//List<AdherenceDataPoints> adherenceList = adherenceRepository.findByDosageInfo_idInOrderByTakentimeDesc
		// (dosageinfoidlist);
		return adherenceList;
	}

	@Override
	public List<AdherenceDataPoints> findByprescriptionID(long prescriptionID) {
		List<AdherenceDataPoints> datapoints = adherenceRepository.findByPrescriptionID(prescriptionID);
		return datapoints;
	}

	@Override
	public AdherenceDataPoints findByPrescribedTimeAndprescriptionID(LocalDateTime PrescribedTime,
	                                                                 long prescriptionID) {
		AdherenceDataPoints adherenceDataPoints =
				adherenceRepository.findByPrescribedTimeAndPrescriptionID(PrescribedTime, prescriptionID);
		return adherenceDataPoints;
	}

	@Override
	public List<AdherenceDataPoints> findByPrescribedTimeAndBetweenTimes(LocalDateTime startPrescribedTime,
	                                                                     LocalDateTime endPrescribedTime,
	                                                                     long prescriptionID) {
		List<AdherenceDataPoints> datapoints =
				adherenceRepository.findByPrescribedTimeBetweenAndPrescriptionID(startPrescribedTime,
				                                                                 endPrescribedTime,
				                                                                 prescriptionID);
		return datapoints;
	}

	@Override
	public List<AdherenceDataPoints> findAdherencesBetweenPrescribedTime(LocalDateTime startPrescribedTime,
	                                                                     LocalDateTime endPrescribedTime,
	                                                                     List<Long> prescriptionIDs) {
		return adherenceRepository.findByPrescribedTimeBetweenAndPrescriptionIDIn(startPrescribedTime,
		                                                                          endPrescribedTime, prescriptionIDs);
	}
}
