package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.service.DeviceInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DeviceInfoServiceImpl implements DeviceInfoService {

	@Autowired
	DeviceInfoRepository deviceInfoRepository;

	@Override
	public DeviceInfo fetctDeviceInfoByID(long deviceInfoId) {
		Optional<DeviceInfo> optionalDeviceInfo = deviceInfoRepository.findById(deviceInfoId);
		if (optionalDeviceInfo.isPresent()) {
			return optionalDeviceInfo.get();
		} else {
			return null;
		}

	}

}
