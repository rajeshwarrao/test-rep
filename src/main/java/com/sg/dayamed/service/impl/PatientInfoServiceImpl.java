package com.sg.dayamed.service.impl;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.dao.EhCacheConstants;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Address;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.helper.PatientHelper;
import com.sg.dayamed.helper.PrescriptionHelper;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.helper.pojo.PatientAssociatedActorsPojo;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.ConsumerGoodInfoModel;
import com.sg.dayamed.pojo.DosageInfoModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.repository.ConsumerGoodsInfoRepository;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.repository.DeviceNotificationRepository;
import com.sg.dayamed.repository.DiseaseInfoRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.DosageDeviceRepository;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.repository.GlucoMeterRepository;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.service.PatientInfoService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.service.PrescriptionScheduleService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.ProviderService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PatientInfoServiceImpl implements PatientInfoService {

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ProviderRepository providerRepository;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	Utility utility;

	@Autowired
	PrescriptionStatusRepository prescriptionStatusRepository;

	@Autowired
	DiseaseInfoRepository diseaseInfoRepository;

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	@Autowired
	DeviceInfoRepository deviceInfoRepository;

	@Autowired
	ConsumerGoodsInfoRepository consumerGoodsInfoRepository;

	@Autowired
	HeartRateRepository heartRateRepository;

	@Autowired
	GlucoMeterRepository glucoMeterRepository;

	@Autowired
	BpMonitorRepository bpMonitorRepository;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	DiseaseRepository diseaseRepository;

	@Autowired
	ImanticService imanticService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DosageNotificationRepository dosageNotificationRepository;

	@Autowired
	DeviceNotificationRepository deviceNotificationRepository;

	@Autowired
	SendResetPwdNotification sendPwdResetNtf;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	NotificationScheduleService notificationScheduleService;

	@Autowired
	PrescriptionScheduleService prescriptionScheduleService;

	@Autowired
	NotificationScheduleRepository notificationScheduleRepository;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	@Autowired
	ProviderService providerService;

	@Autowired
	PHIDataEncryptionService pHIDataEncryptionService;

	@Autowired
	PharmacistService pharmacistService;

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	PatientHelper patientHelper;

	@Autowired
	PrescriptionHelper prescriptionHelper;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	BaseRestApiImpl baseRestApiImpl;

	@Autowired
	AdherenceService adherenceService;

	@Autowired
	PasswordEncryption pwdEncryption;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;
	/*
	 * @Autowired BCryptPasswordEncoder bCryptPasswordEncoder;
	 */

	@Override
	public String deletePatientByProcedure(long patientId) {
		Optional<Patient> optionalPatient = patientRepository.findById(patientId);
		if (optionalPatient.isPresent() && optionalPatient.get()
		                                                  .getUserDetails()
		                                                  .getImanticUserid() != null) {
			patientRepository.deletePatient(patientId);
			imanticService.deletePatientByImanticId(optionalPatient.get()
			                                                       .getUserDetails()
			                                                       .getImanticUserid());
		}
		return ApplicationConstants.SUCCESS;
	}

	@Override
	public PatientModel addPatient(Patient patient) {
		// creating patient in ImanticDB and getting back imanticId of that
		// particular patient
		Map<String, String> imanticMap = imanticService.createPatientInImantics(patient);
		// Add Imantics ID to Local patient record and use it to update the
		// patient data on Imantics.
		Patient persistedpatient = null;
		if (imanticMap == null || imanticMap.size() == 0) {
			return null;
		}
		patient.getUserDetails()
		       .setImanticUserid(imanticMap.get("imanticid"));
		Map<String, String> analyticMap = imanticService
				.fetchAnalyticsMap(patient.getUserDetails()
				                          .getImanticUserid());
		patient.setPredictedAdherence(analyticMap.get("predictedAdherence"));
		patient.setCurrentAdherence(analyticMap.get("currentAdherence"));
		patient.setProjectedAdherence(analyticMap.get("projectedAdherence"));
		if (analyticMap.get("emoji") != null) {
			patient.setEmoji(analyticMap.get("emoji"));
		}

		String pwdChangeToken = pwdEncryption
				.encrypt(patient.getUserDetails()
				                .getEmailId() + System.currentTimeMillis());
		UserDetails nonEncryptUserDetails = patient.getUserDetails();
		boolean emailFlag = true;
		boolean smsFlag = true;
		if (patient.getUserDetails() != null) {
			emailFlag = patient.getUserDetails()
			                   .isEmailFlag();
			smsFlag = patient.getUserDetails()
			                 .isSmsFlag();
		}
		patient.getUserDetails()
		       .setPwdChangeToken(pwdChangeToken);
		patient.getUserDetails()
		       .setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));
		patient.getUserDetails()
		       .setEmailFlag(emailFlag);
		patient.getUserDetails()
		       .setSmsFlag(smsFlag);
		persistedpatient = patientRepository.save(patient);
		sendPatientAddOrUpdateNotification(nonEncryptUserDetails, patient);
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(persistedpatient);
		return patientmodel;
	}

	public void sendPatientAddOrUpdateNotification(UserDetails nonEncryptUserDetails, Patient patient) {
		nonEncryptUserDetails.setPwdChangeToken(patient.getUserDetails()
		                                               .getPwdChangeToken());
		sendPwdResetNtf.sendPwdResetNotificationWhenRegst(nonEncryptUserDetails, "Patient");
		patient.setUserDetails(nonEncryptUserDetails);
		notificationService.sendMobileNotification(patient.getUserDetails()
		                                                  .getType(),
		                                           "Your profile has been added into Dayamed as patient",
		                                           "Added profile in Dayamed",
		                                           patient.getUserDetails()
		                                                  .getToken());
		Map<String, String> keyVal = new HashMap<String, String>();
		keyVal.put("patientName",
		           patient.getUserDetails()
		                  .getFirstName() + " " + patient.getUserDetails()
		                                                 .getLastName());
		String userLangPreferencePat = utility.getUserPreferedLang(patient.getId());
		if (patient.getCaregivers() != null) {
			List<String> recipientList = new ArrayList<String>();
			for (Caregiver caregiver : patient.getCaregivers()) {
				String userLangPreference = utility.getUserPreferedLang(caregiver.getUserDetails()
				                                                                 .getId());
				recipientList.add(caregiver.getUserDetails()
				                           .getEmailId());
				if (caregiver.getUserDetails()
				             .getType() != null && (caregiver.getUserDetails()
				                                             .getType()
				                                             .equalsIgnoreCase(ApplicationConstants.ANDROID)
						|| caregiver.getUserDetails()
						            .getType()
						            .equalsIgnoreCase(ApplicationConstants.IOS))) {
					notificationService.sendMobileNotification(caregiver.getUserDetails()
					                                                    .getType(),
					                                           "You are now the caregiver for patient :" +
							                                           patient.getUserDetails()
							                                                  .getFirstName()
							                                           + " " + patient.getUserDetails()
							                                                          .getLastName(),
					                                           "Patient added", caregiver.getUserDetails()
					                                                                     .getToken());
				}
				String msgSms = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal,
				                                                                              "profile.patient.added" +
						                                                                              ".caregiver",
				                                                                              userLangPreference, null);
				emailAndSmsUtility.sendSms(msgSms, caregiver.getUserDetails()
				                                            .getCountryCode(),
				                           caregiver.getUserDetails()
				                                    .getMobileNumber());
			}
			String msgSms = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal,
			                                                                              "profile.patient.added" +
					                                                                              ".caregiver",
			                                                                              userLangPreferencePat, null);
			String emailSubject = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
			                                                                                    "emailsubject" +
					                                                                                    ".patientadded",
			                                                                                    userLangPreferencePat,
			                                                                                    null);
			emailAndSmsUtility.sendEmail(emailSubject, msgSms, recipientList);
		}
	}

	@Autowired
	DosageDeviceRepository dosageDeviceRepository;

	@Override
	public List<PatientModel> fetchPatientsByProviderId(long providerid) {
		List<Patient> patientList = patientRepository.findByProviders_idAndStatus(providerid, 0);
		List<PatientModel> patientModels = new LinkedList<PatientModel>();
		Comparator<UserDetails> byUpdateOn = Comparator.comparing(
				UserDetails::getUpdateOn,
				(ts1, ts2) -> ts1.compareTo(ts2));
		patientList = patientList.stream()
		                         .sorted((p1, p2) -> (p1.getUserDetails()
		                                                .getUpdateOn() != null && p2.getUserDetails()
		                                                                            .getUpdateOn() != null) ?
				                         byUpdateOn.compare(p1.getUserDetails(), p2.getUserDetails()) : 1)
		                         .collect(Collectors.toList());
		for (Patient patient : patientList) {
			List<PrescriptionModel> PrescriptionModelList = new ArrayList<PrescriptionModel>();
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
			for (Prescription prescription : patient.getPrescriptions()) {
				PrescriptionModelList.add(utility
						                          .convertPrescriptionEntityToModel(
								                          utility.fetchUnsoftDeletedPrescriptionInfo(prescription)));
			}
			if (CollectionUtils.isNotEmpty(patientModel.getCaregivers())) {
				Set<CaregiverModel> caregiverList = new HashSet<CaregiverModel>();
				for (CaregiverModel caregiver : patientModel.getCaregivers()) {
					if (caregiver != null) {
						caregiverList.add(caregiver);
					}
				}
				patientModel.setCaregivers(caregiverList);
			}
			if (CollectionUtils.isNotEmpty(patientModel.getProviders())) {
				Set<ProviderModel> providerList = new HashSet<ProviderModel>();
				for (ProviderModel providerModel : patientModel.getProviders()) {
					if (providerModel != null) {
						providerList.add(providerModel);
					}
				}
				patientModel.setProviders(providerList);
			}
			if (CollectionUtils.isNotEmpty(patientModel.getPharmacists())) {
				Set<PharmacistModel> pharmacistList = new HashSet<PharmacistModel>();
				for (PharmacistModel pharmacistModel : patientModel.getPharmacists()) {
					if (pharmacistModel != null) {
						pharmacistList.add(pharmacistModel);
					}
				}
				patientModel.setPharmacists(pharmacistList);
			}
			patientModel.setPrescriptions(PrescriptionModelList);
			patientModels.add(patientModel);
		}
		return patientModels;
	}

	@Override
	public Patient fetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		if (optionalPatient.isPresent()) {
			Patient patinet = optionalPatient.get();
			boolean emailFlag = patinet.getUserDetails()
			                           .isEmailFlag();
			boolean smsFlag = patinet.getUserDetails()
			                         .isSmsFlag();
			patinet.getUserDetails()
			       .setEmailFlag(emailFlag);
			patinet.getUserDetails()
			       .setSmsFlag(smsFlag);
			return patinet;
		}
		return null;
	}

	@Override
	public Patient directFetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		return optionalPatient.isPresent() ? optionalPatient.get() : null;
	}

	@Override
	public Patient decryptPatientProviderfetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		return optionalPatient.isPresent() ? optionalPatient.get() : null;
	}

	public Patient noDecrypCargiverfetchPatientById(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		return optionalPatient.isPresent() ? optionalPatient.get() : null;
	}

	public Patient directFetchPatientByPatientId(long id) {
		Optional<Patient> optionalPatient = patientRepository.findById(id);
		return optionalPatient.isPresent() ? optionalPatient.get() : null;
	}

	@Override
	public Patient findByUserDetails_id(long userid) {
		Patient patient = patientRepository.findByUserDetails_id(userid);
		return patient;
	}

	@Override
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public PatientModel updatePatient(Patient patient) {
		// settint Analytics to patient
		imanticService.updateDemographicData(patient);
		Map<String, String> analyticMap = imanticService.fetchAnalyticsMap(patient.getUserDetails()
		                                                                          .getImanticUserid());

		patient.setPredictedAdherence(analyticMap.get("predictedAdherence"));
		patient.setCurrentAdherence(analyticMap.get("currentAdherence"));
		patient.setProjectedAdherence(analyticMap.get("projectedAdherence"));
		if (analyticMap.get("emoji") != null) {
			patient.setEmoji(analyticMap.get("emoji"));
		}
		UserDetails nonEncryptUserDetails = patient.getUserDetails();
		boolean emailFlag = true;
		boolean smsFlag = true;
		if (patient.getUserDetails() != null) {
			emailFlag = patient.getUserDetails()
			                   .isEmailFlag();
			smsFlag = patient.getUserDetails()
			                 .isSmsFlag();
		}
		patient.getUserDetails()
		       .setEmailFlag(emailFlag);
		patient.getUserDetails()
		       .setSmsFlag(smsFlag);
		patient = patientRepository.save(patient);
		patient.setUserDetails(nonEncryptUserDetails);
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(patient);
		List<String> recipientList = new ArrayList<String>();
		recipientList.add(patient.getUserDetails()
		                         .getEmailId());
		String userLangPreference = utility.getUserPreferedLang(patient.getUserDetails()
		                                                               .getId());
		String msgRes = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
		                                                                              "profile.update.patient.you",
		                                                                              userLangPreference, null);
		String emailSubject = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
		                                                                                    "appconstants.profile" +
				                                                                                    ".updated",
		                                                                                    userLangPreference, new Object[] { utility.getProductOwner() });
		emailAndSmsUtility.sendEmail(emailSubject, msgRes, recipientList);
		recipientList.clear();
		notificationService.sendMobileNotification(patient.getUserDetails()
		                                                  .getType(), msgRes, emailSubject,
		                                           patient.getUserDetails()
		                                                  .getToken());
		// if patient has caregiver
		Map<String, String> msgResourceReplaceKeyVal = new HashMap<String, String>();
		String firstName = patient.getUserDetails()
		                          .getFirstName() != null ? patient.getUserDetails()
		                                                           .getFirstName()
				: "";
		String lastName = patient.getUserDetails()
		                         .getLastName() != null ? patient.getUserDetails()
		                                                         .getLastName() : "";
		String patientName = firstName + " " + lastName;
		msgResourceReplaceKeyVal.put("patientName", patientName);
		if (patient != null && patient.getCaregivers() != null) {
			for (Caregiver caregiver : patient.getCaregivers()) {
				userLangPreference = utility.getUserPreferedLang(caregiver.getUserDetails()
				                                                          .getId());
				recipientList.add(caregiver.getUserDetails()
				                           .getEmailId());
				// sending sms too
				msgRes = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal,
				                                                                       "profile.update.patient" +
						                                                                       ".profile",
				                                                                       userLangPreference, null);
				emailAndSmsUtility.sendSms(msgRes, caregiver.getUserDetails()
				                                            .getCountryCode(),
				                           caregiver.getUserDetails()
				                                    .getMobileNumber());
				if (caregiver.getUserDetails()
				             .getType() != null
						&& (caregiver.getUserDetails()
						             .getType()
						             .equalsIgnoreCase(ApplicationConstants.ANDROID)
						|| caregiver.getUserDetails()
						            .getType()
						            .equalsIgnoreCase(ApplicationConstants.IOS))) {
					notificationService.sendMobileNotification(caregiver.getUserDetails()
					                                                    .getType(), msgRes,
					                                           messageNotificationConstants.getUserPreferedLangTranslatedMsg(
							                                           null,
							                                           ApplicationConstants.UPDATED_PATIET_PROFILE_IN_DAYAMED_TITLE,
							                                           userLangPreference, null),
					                                           caregiver.getUserDetails()
					                                                    .getToken());
				}
			}
			emailAndSmsUtility.sendEmail("appconstants.profile.updated", "profile.update.patient.profile",
			                             msgResourceReplaceKeyVal, recipientList);
			recipientList.clear();
		}
		emailAndSmsUtility.sendSms(msgRes, patient.getUserDetails()
		                                          .getCountryCode(),
		                           patient.getUserDetails()
		                                  .getMobileNumber());
		return patientmodel;
	}

	@Override
	public List<PatientModel> fetchPatientsByCaregiverId(long id) {
		List<Patient> patientlist = patientRepository.findByCaregivers_id(id);
		List<PatientModel> patientmodels = new ArrayList<PatientModel>();
		patientlist.forEach(patient -> {
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
			patientmodels.add(patientModel);
		});
		return patientmodels;
	}

	@Override
	public List<Patient> directFetchPatientsByCaregiverId(long id) {
		List<Patient> patientlist = patientRepository.findByCaregivers_id(id);
		return patientlist;
	}

	@Override
	public PatientModel updatePatientAlone(Patient patient) {
		if (patient.getUserDetails()
		           .getCreateOn() == null) {
			patient.getUserDetails()
			       .setCreateOn(Date.from(Instant.now()));
		} else {
			patient.getUserDetails()
			       .setUpdateOn(Date.from(Instant.now()));
		}
		patient = patientRepository.save(patient);
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(patient);
		return patientmodel;
	}

	@Override
	public Patient fetchByDeviceId(String deviceId) {
		Patient patient = patientRepository.findByDeviceId(deviceId);
		return patient;
	}

	@Override
	public PatientAdherencePojo fetchAdherenceByPatientId(long id) {
		return patientRepository.findAdherenceById(id);
	}

	@Override
	public Patient fetchPatientByUserId(long userID) {
		Patient patient = patientRepository.findByUserDetails_id(userID);
		if (patient != null) {
			if (patient.getProviders() != null && patient.getProviders()
			                                             .size() > 0) {
				for (Provider provider : patient.getProviders()) {
					if (provider.getUserDetails() != null && provider.getUserDetails()
					                                                 .getUserOptions() != null
							&& provider.getUserDetails()
							           .getUserOptions()
							           .getGsPharmacistEnable()
							           .intValue() == 1) {
						patient.setPharmacists(null);
					}
				}
			}
			return patient;
		}
		return null;
	}

	@Override
	public List<Patient> fetchAllPatients() {
		return patientRepository.findAll();
	}

	@Override
	public List<Patient> fetchEncryptPatientsByProviderId(long providerid) {
		return patientRepository.findByProviders_id(providerid);
	}

	public List<PatientModel> fetchPatietnsByProviderId(HttpServletRequest request, String providerid,
	                                                    Authentication authentication) throws Exception {
		try {
			if (StringUtils.isNotBlank(providerid)) {
				ProviderModel providerModel = providerService.fetchProviderById(Long.parseLong(providerid));
				if (providerModel == null) {
					throw new DataValidationException("provider",
					                                  IWSGlobalApiErrorKeys.ERRORS_GENERAL_RECORD_NOT_FOUND);
				}
				List<PatientModel> patientList = fetchPatientsByProviderId(Long.valueOf(providerid));
				String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
				List<PatientModel> patList = pHIDataEncryptionService.encryptPatientList(patientList, keyText);
				return patList;
			} else {
				throw new DataValidationException("providernotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public List<PatientModel> fetchMissedAdherencePatietnsByProviderId(Authentication authentication,
	                                                                   HttpServletRequest request) throws Exception {
		try {
			JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
			Provider provider = providerRepository.findByUserDetails_id(jwtUser.getUserId());
			if (provider != null) {
				List<PatientModel> patientlist = fetchPatientsByProviderId(provider.getId());
				Set<PatientModel> missedPatientSet = new HashSet<PatientModel>();
				String startTime = " 00:00";
				String endTime = " 23:59";
				LocalDate yesterdayLocalDateInUTC = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC)
				                                                 .toLocalDate()
				                                                 .minusDays(1);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				LocalDateTime startLocalDateTime = dateUtility
						.convertdatetimeStringToLocalDateTime(yesterdayLocalDateInUTC.format(formatter) + startTime);
				LocalDateTime endLocalDateTime = dateUtility
						.convertdatetimeStringToLocalDateTime(yesterdayLocalDateInUTC.format(formatter) + endTime);
				if (null != patientlist) {
					for (PatientModel patient : patientlist) {
						boolean adhflag = false;
						if (CollectionUtils.isNotEmpty(patient.getPrescriptions())) {
							List<AdherenceDataPoints> adherenceList = adherenceService
									.findByPrescribedTimeAndBetweenTimes(startLocalDateTime, endLocalDateTime,
									                                     patient.getPrescriptions()
									                                            .get(0)
									                                            .getId());
							for (AdherenceDataPoints adherence : adherenceList) {
								if (adherence.getConsumptionStatus()
								             .contains(ApplicationConstants.CONSUMPTION_STATUS_SKIPPED)
										|| adherence.getConsumptionStatus()
										            .contains(ApplicationConstants.CONSUMPTION_STATUS_NOACTION)) {
									adhflag = true;
								}
							}
						}
						if (adhflag) {
							missedPatientSet.add(patient);
						}
					}
				}
				String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
				List<PatientModel> encyPatlist = pHIDataEncryptionService
						.encryptPatientList(new ArrayList<PatientModel>(missedPatientSet), keyText);
				return encyPatlist;
			} else {
				throw new DataValidationException("providernotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public List<PatientModel> fetchPatietnsByProviderIdFromCookie(Authentication authentication,
	                                                              HttpServletRequest request) throws Exception {

		try {
			JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
			Provider provider = providerRepository.findByUserDetails_id(jwtUser.getUserId());
			if (provider != null) {
				List<PatientModel> patientlist = fetchPatientsByProviderId(provider.getId());
				String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
				List<PatientModel> encyPatlist = pHIDataEncryptionService.encryptPatientList(patientlist, keyText);
				if (jwtUser.isGsPharmacistEnable() && CollectionUtils.isNotEmpty(encyPatlist)) {
					encyPatlist.forEach(pat -> {
						pat.setPharmacists(null);
					});
				}
				return encyPatlist;
			} else {
				throw new DataValidationException("providernotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public List<PatientModel> fetchAllPatients(Authentication authentication, HttpServletRequest request)
			throws Exception {
		List<Patient> patients = patientRepository.findAll();
		List<PatientModel> patientModelList = new ArrayList<>();
		patients.forEach(patient -> {
			patientModelList.add(utility.convertPatientEntityTopatientModel(patient));
		});
		String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
		List<PatientModel> encyPatlist = pHIDataEncryptionService.encryptPatientList(patientModelList, keyText);
		return encyPatlist;
	}

	public PatientModel fetchPatientById(String id, Authentication authentication, HttpServletRequest request)
			throws Exception {
		try {
			if (StringUtils.isNotBlank(id)) {
				Patient patient = fetchPatientById(Long.parseLong(id));
				if (patient != null) {
					String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
					PatientModel encyPatlist = pHIDataEncryptionService
							.encryptPatient(utility.convertPatientEntityTopatientModel(patient), keyText);
					encyPatlist.setEmailFlag(patient.getUserDetails()
					                                .isEmailFlag());
					encyPatlist.setSmsFlag(patient.getUserDetails()
					                              .isSmsFlag());
					JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
					if (ApplicationConstants.CAREGIVER.equalsIgnoreCase(jwtUser.getRole())) {
						encyPatlist.setPharmacists(new HashSet<PharmacistModel>());
					}
					return encyPatlist;
				} else {
					throw new DataValidationException("patientnotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
				}
			} else {
				throw new DataValidationException("patientnotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public PrescriptionModel addPrescription(String patientid, PrescriptionModel prescriptionmodel,
	                                         HttpServletRequest request, Authentication authentication)
			throws Exception {
		try {
			if (StringUtils.isNotBlank(patientid)) {
				throw new DataValidationException("patientnotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
			}
			Patient patient = directFetchPatientByPatientId(Long.parseLong(patientid));
			if (patient == null) {
				throw new DataValidationException("patientnotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
			}
			JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
			int result = utility.instanceOfPatient(patient, jwtUser);
			if (result == 401) {
				throw new DataValidationException("patientUNAUTHORIZED", IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
			}
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
			prescriptionmodel.setPatient(patientModel);
			long userid = jwtUser.getUserId();
			if (jwtUser != null && jwtUser.getRole()
			                              .equalsIgnoreCase(ApplicationConstants.CAREGIVER)) {
				List<ConsumerGoodInfoModel> consumerGoodInfoModelList = prescriptionmodel.getCommodityInfoList();
				List<DosageInfoModel> dosageInfoModelList = prescriptionmodel.getDosageInfoList();
				prescriptionmodel = prescriptionService.fetchPrescriptionByPrescriptionId(prescriptionmodel.getId(),
				                                                                          userid);
				prescriptionmodel.setCommodityInfoList(consumerGoodInfoModelList);
				prescriptionmodel.setDosageInfoList(dosageInfoModelList);
			}
			PrescriptionModel prescriptionModel = prescriptionHelper.addPrescription(prescriptionmodel, patient,
			                                                                         userid);
			if (prescriptionModel != null) {
				return prescriptionModel;
			} else {
				throw new ApplicationException();
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public PatientModel updatePatient(PatientModel patientModelEncrypt, MultipartFile profileImage,
	                                  HttpServletRequest request, Authentication authentication) throws Exception {
		PatientModel patientModel = null;
		if (patientModelEncrypt != null) {
			String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
			patientModel = pHIDataEncryptionService.decryptPatient(patientModelEncrypt, keyText);
		}
		if (patientModel != null) {
			if (patientModel.getUserDetails() != null) {
				UserDetailsModel usedetails = patientModel.getUserDetails();
				if (StringUtils.isBlank(usedetails.getFirstName())
						|| StringUtils.isBlank(usedetails.getLastName())
						|| StringUtils.isBlank(usedetails.getCountryCode())
						|| StringUtils.isBlank(usedetails.getMobileNumber())
						|| StringUtils.isBlank(usedetails.getAddress()
						                                 .getLocation())
						|| StringUtils.isBlank(usedetails.getAddress()
						                                 .getZipCode())) {

					throw new ApplicationException();
				}
			} else {
				throw new ApplicationException();
			}
		} else {
			throw new ApplicationException();
		}
		Patient existedPatient = fetchPatientById(patientModel.getId());
		String imageNameWitPathUrl = null;
		if (existedPatient == null) {
			throw new DataValidationException("patientNotFound", APIErrorKeys.ERROR_USER_NOT_FOUND);
		}
		if (profileImage != null && !profileImage.isEmpty()) {
			String oldImageName = ApplicationConstants.EMPTY_STRING;
			if (existedPatient.getUserDetails() != null && existedPatient.getUserDetails()
			                                                             .getImageUrl() != null) {
				oldImageName = utility.getLastBitFromUrl(existedPatient.getUserDetails()
				                                                       .getImageUrl());
			}
			imageNameWitPathUrl = utility.uploadProfilepic(profileImage,
			                                               patientModel.getUserDetails()
			                                                           .getFirstName(), oldImageName);
		}
		Patient recentPatient = utility.convertPatientModelTopatientEntity(patientModel);
		if (recentPatient.getUserDetails() != null) {
			UserDetails recentPatientUserDetails = recentPatient.getUserDetails();
			if (recentPatientUserDetails.getAddress() != null) {
				Address recentPatientAddress = recentPatientUserDetails.getAddress();
				existedPatient.getUserDetails()
				              .setFirstName(recentPatientUserDetails.getFirstName());
				existedPatient.getUserDetails()
				              .setLastName(recentPatientUserDetails.getLastName());
				existedPatient.getUserDetails()
				              .setMiddleName(recentPatientUserDetails.getMiddleName());
				existedPatient.getUserDetails()
				              .setCountryCode(recentPatientUserDetails.getCountryCode());
				existedPatient.getUserDetails()
				              .setMobileNumber(recentPatientUserDetails.getMobileNumber());
				existedPatient.getUserDetails()
				              .getAddress()
				              .setCity(recentPatientAddress.getCity());
				existedPatient.getUserDetails()
				              .getAddress()
				              .setCountry(recentPatientAddress.getCountry());
				existedPatient.getUserDetails()
				              .getAddress()
				              .setState(recentPatientAddress.getState());
				existedPatient.getUserDetails()
				              .getAddress()
				              .setZipCode(recentPatientAddress.getZipCode());
				existedPatient.getUserDetails()
				              .getAddress()
				              .setLocation(recentPatientAddress.getLocation());
				existedPatient.getUserDetails()
				              .setImageUrl(recentPatientUserDetails.getImageUrl());
				if (imageNameWitPathUrl != null) {
					existedPatient.getUserDetails()
					              .setImageUrl(imageNameWitPathUrl);
				}
			}
		}
		PatientModel updatedpatientModel = updatePatient(existedPatient);
		if (updatedpatientModel != null) {
			String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
			PatientModel encyPatinetModel = pHIDataEncryptionService.encryptPatient(updatedpatientModel, keyText);
			return encyPatinetModel;
		}
		throw new ApplicationException();
	}

	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public PatientModel addPatient(PatientModel patientModel, MultipartFile profileImage, HttpServletRequest request,
	                               Authentication authentication) throws Exception {
		try {
			if (patientModel == null) {
				throw new DataValidationException("invalidData", APIErrorKeys.ERROR_INVALID_DATA);
			}
			String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
			patientModel = pHIDataEncryptionService.decryptPatient(patientModel, keyText);
			if (patientModel.getUserDetails() != null) {
				UserDetailsModel patientUserDetails = patientModel.getUserDetails();
				if (StringUtils.isBlank(patientUserDetails.getFirstName())
						|| StringUtils.isBlank(patientUserDetails.getLastName())
						|| StringUtils.isBlank(patientUserDetails.getAge())
						|| StringUtils.isBlank(patientUserDetails.getRace())
						|| StringUtils.isBlank(patientUserDetails.getEmailId())
						|| StringUtils.isBlank(patientUserDetails.getGender())
						|| StringUtils.isBlank(patientUserDetails.getCountryCode())
						|| StringUtils.isBlank(patientUserDetails.getMobileNumber())
						|| StringUtils.isBlank(patientUserDetails.getAddress()
						                                         .getLocation())
						|| StringUtils.isBlank(patientUserDetails.getAddress()
						                                         .getZipCode())
						|| patientUserDetails.validDateOfBirth(UserRoleEnum.PATIENT)) {
					throw new ApplicationException();
				}
			}
			if (profileImage != null && !profileImage.isEmpty()) {
				String oldImageName = ApplicationConstants.EMPTY_STRING;
				try {
					if (patientModel.getUserDetails() != null && patientModel.getUserDetails()
					                                                         .getImageUrl() != null) {
						oldImageName = utility.getLastBitFromUrl(patientModel.getUserDetails()
						                                                     .getImageUrl());
					}

					String imageNameWitPathUrl = utility.uploadProfilepic(profileImage,
					                                                      patientModel.getUserDetails()
					                                                                  .getFirstName(), oldImageName);
					if (imageNameWitPathUrl != null) {
						patientModel.getUserDetails()
						            .setImageUrl(imageNameWitPathUrl);
					}
				} catch (Exception e) {
					e.getMessage();
				}
			}
			if (patientModel != null && patientModel.getUserDetails() != null
					&& patientModel.getUserDetails()
					               .getEmailId() != null) {
				String encryptUseEmail = patientModel.getUserDetails()
				                                     .getEmailId();
				UserDetailsModel userDetailsModel = userDetailsService.findByEmailIdIgnoreCase(encryptUseEmail);
				if (userDetailsModel == null) {
					userDetailsModel = userDetailsService
							.findByEmailIdIgnoreCase(patientModel.getUserDetails()
							                                     .getEmailId());
				}
				if (userDetailsModel != null
						&& userDetailsModel.getRole()
						                   .getName()
						                   .equalsIgnoreCase(ApplicationConstants.PATIENT)) {
					String imanticId = StringUtils.isEmpty(userDetailsModel.getImanticUserid()) ? ""
							: userDetailsModel.getImanticUserid();
					patientModel.getUserDetails()
					            .setPassword(userDetailsModel.getPassword());
					patientModel.getUserDetails()
					            .setImanticUserid(imanticId);
					patientModel.getUserDetails()
					            .setType((userDetailsModel.getType() != null) ? userDetailsModel.getType() : null);
				}
				if (userDetailsModel == null || patientModel.getId() > 0) {
					PatientModel persistedPatientModel = patientHelper.addPatient(patientModel, authentication);
					if (persistedPatientModel == null) {
						// 208 means problem with Imantics
						throw new DataValidationException("ImanticsIssue",
						                                  APIErrorKeys.ERROR_INVALID_DATA);
					}
					PatientModel savedEncyPatinetModel = pHIDataEncryptionService.encryptPatient(persistedPatientModel,
					                                                                             keyText);
					return savedEncyPatinetModel;
				} else {
					throw new DataValidationException("emailConflict", APIErrorKeys.ERROR_EMAIL_CONFLICT);
				}
			} else {
				throw new ApplicationException();
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public String deletePatient(String id, HttpServletRequest request, Authentication authentication) throws Exception {
		try {
			JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();

			if (StringUtils.isBlank(id)) {
				throw new DataValidationException("patientIdIsNotNull", APIErrorKeys.ERROR_INVALID_DATA);
			}
			Patient patient = fetchPatientById(Long.valueOf(id));
			Set<Provider> providerSet = patient.getProviders();
			boolean underProvider = false;
			for (Provider provider : providerSet) {
				if (provider.getUserDetails()
				            .getEmailId()
				            .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underProvider = true;
				}
			}
			if (!underProvider && !(StringUtils.equalsIgnoreCase(ApplicationConstants.ADMIN, jwtUser.getRole()))) {
				throw new DataValidationException("UNAUTHORIZED", IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
			}

			String result = patientHelper.deletePatient(id);
			if (result.equalsIgnoreCase("success")) {
				return ApplicationConstants.SUCCESS;
			}
			throw new ApplicationException("Patient does not deleted");
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public PatientAssociatedActorsPojo fetchProvidersByPatientId(String patientid) throws Exception {
		try {
			if (StringUtils.isBlank(patientid)) {
				throw new DataValidationException("patientIdIsNotNull", APIErrorKeys.ERROR_INVALID_DATA);
			}
			PatientAssociatedActorsPojo patientAssociatedActorsPojo = new PatientAssociatedActorsPojo();
			List<ProviderModel> providerModelList = providerService
					.fetchProvidersByPatientId(Long.parseLong(patientid));
			List<CaregiverModel> caregiverModelList = caregiverService
					.fetchCaregiversByPatientId(Long.parseLong(patientid));
			List<PharmacistModel> pharmacistModelList = pharmacistService
					.fetchPharmacistListByPatientId(Long.parseLong(patientid));
			patientAssociatedActorsPojo.setPharmacistModelList(pharmacistModelList);
			Patient patient = fetchPatientById(Long.parseLong(patientid));
			boolean gsProviderFlag = false;
			for (Provider provider : patient.getProviders()) {
				if (provider.getUserDetails() != null && provider.getUserDetails()
				                                                 .getUserOptions() != null
						&& provider.getUserDetails()
						           .getUserOptions()
						           .getGsPharmacistEnable()
						           .intValue() == 1) {
					gsProviderFlag = true;
				}
			}
			if (gsProviderFlag) {
				patient.setPharmacists(new HashSet<Pharmacist>());
				patientAssociatedActorsPojo.setPharmacistModelList(new ArrayList<>());
			}
			patientAssociatedActorsPojo.setPatietnID(Long.parseLong(patientid));
			patientAssociatedActorsPojo.setProviderModelList(providerModelList);
			patientAssociatedActorsPojo.setCaregiverModelList(caregiverModelList);
			return patientAssociatedActorsPojo;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
