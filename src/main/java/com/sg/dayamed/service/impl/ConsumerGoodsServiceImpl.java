package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.pojo.ConsumerGoodsModel;
import com.sg.dayamed.repository.ConsumerGoodsRepository;
import com.sg.dayamed.service.ConsumerGoodsService;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ConsumerGoodsServiceImpl implements ConsumerGoodsService {

	@Autowired
	private ConsumerGoodsRepository consumerGoodsRepository;

	@Override
	public List<ConsumerGoodsModel> getConsumerGoodsByType(String type) {
		final List<ConsumerGoods> consumerGoodsEntities = consumerGoodsRepository.findByType(type);
		return consumerGoodsEntities
				.stream()
				.map(this::convertToConsumerGoodsModel)
				.collect(Collectors.toList());
	}

	private ConsumerGoodsModel convertToConsumerGoodsModel(ConsumerGoods consumerGoods) {
		ConsumerGoodsModel consumerGoodsModel = new ConsumerGoodsModel();
		try {
			BeanUtils.copyProperties(consumerGoodsModel, consumerGoods);
		} catch (IllegalAccessException | InvocationTargetException e) {
			log.warn("Exception when copying ConsumerGoods properties");
		}
		return consumerGoodsModel;
	}
}
