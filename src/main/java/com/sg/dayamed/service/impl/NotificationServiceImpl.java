package com.sg.dayamed.service.impl;

import com.sg.dayamed.FcmNotification;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.NotificationReceivedUser;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.PrescriptionHelper;
import com.sg.dayamed.helper.pojo.CaregiverDosageNotificationModel;
import com.sg.dayamed.notification.apns.ApnsNotification;
import com.sg.dayamed.pojo.APNSUpdateTokenRequestModel;
import com.sg.dayamed.pojo.CalendarNotificationModel;
import com.sg.dayamed.pojo.CalenderEventModel;
import com.sg.dayamed.pojo.CalenderNotificationRequestModel;
import com.sg.dayamed.pojo.DosageNotificationModel;
import com.sg.dayamed.pojo.FcmNotificationRequestModel;
import com.sg.dayamed.pojo.NotificationModel;
import com.sg.dayamed.pojo.NotificationRequestModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.VideoNotificationRequestModel;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.repository.NotificationReceivedUserRepository;
import com.sg.dayamed.repository.NotificationRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.DosageInfoService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.Utility;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * @author Naresh.kamisetti
 */
@Slf4j
@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	NotificationReceivedUserRepository notificationReceivedUserRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	RediseService rediseService;

	@Autowired
	ApnsNotification apnsNotification;

	@Autowired
	FcmNotification fcmNotification;

	/**
	 * @param userId
	 * @param deviceType
	 * @param msg
	 * @param title
	 * @param type
	 * @return
	 */
	@Override
	public boolean addNotification(List<Long> receivingUserIdList, long userId, String msg, String title,
	                               String type) {
		try {
			Optional<UserDetails> optionalUserDetails = userDetailsRepository.findById(userId);
			if (optionalUserDetails.isPresent() && optionalUserDetails.get()
			                                                          .getToken() != null) {
				UserDetails userDetails = optionalUserDetails.get();
				if (userDetails.getType() != null) {
					if (userDetails.getType() != null && userDetails.getToken() != null && (userDetails.getType()
					                                                                                   .equalsIgnoreCase(
							                                                                                   ApplicationConstants.ANDROID)
							|| userDetails.getType()
							              .equalsIgnoreCase(ApplicationConstants.IOS))) {
						sendMobileNotification(userDetails.getType(), msg, title, userDetails.getToken());
						return true;
					} else if (userDetails.getType()
					                      .equalsIgnoreCase(ApplicationConstants.WEB)) {
						if (receivingUserIdList != null && receivingUserIdList.size() > 0) {
							addWebNotification(receivingUserIdList, userId, msg, title, type);
						} else {
							addWebNotification(userId, msg, title, type);
						}
						return true;
					}
				}
			}
			return false;
		} catch (Exception e) {
			e.getLocalizedMessage();
			return false;
		}
	}

	/**
	 * @param deviceType ios/andriod
	 * @param msg        the message which is sending to user as notification
	 * @param title      title of notification
	 * @param token      either fcm/apns token
	 * @return Sent_Notification
	 */
	@Override
	public String sendMobileNotification(String deviceType, String msg, String title, String token) {
		try {
			if (deviceType != null && !"".equalsIgnoreCase(deviceType) && token != null &&
					!"".equalsIgnoreCase(token)) {
				if (deviceType.equalsIgnoreCase(ApplicationConstants.ANDROID)) {
					fcmNotification.pushFCMNotification(token, msg, title);
				}
				if (deviceType.equalsIgnoreCase(ApplicationConstants.IOS)) {
					apnsNotification.pushApnsNotification(token, title, msg);
				}
				return "Sent_Notification";
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return ApplicationConstants.EMPTY_STRING;
	}

	/**
	 * this method will send notification by device_type(ios/andriod) of user
	 *
	 * @param userid notification send to whom
	 * @param msg    the message which is sending to user as notification
	 * @param title  title title of notification
	 * @return Sent_Notification
	 */
	@Override
	public String sendMobileNotification(long userid, String msg, String title) {
		try {
			Optional<UserDetails> optionalUserDetails = userDetailsRepository.findById(userid);
			if (optionalUserDetails.isPresent() && optionalUserDetails.get()
			                                                          .getToken() != null
					&& optionalUserDetails.get()
					                      .getType() != null) {
				UserDetails userDetails = optionalUserDetails.get();
				if (userDetails.getType()
				               .equalsIgnoreCase(ApplicationConstants.ANDROID)) {
					fcmNotification.pushFCMNotification(userDetails.getToken(), msg, title);
				}
				if (userDetails.getType()
				               .equalsIgnoreCase(ApplicationConstants.IOS)) {
					apnsNotification.pushApnsNotification(userDetails.getToken(), title, msg);
				}
				return "Sent_Notification";
			} else {
				return "Token_is_Empty";
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return ApplicationConstants.EMPTY_STRING;
	}

	//this code written for zoom video/genaral push notification.(active)

	/**
	 * @param userid
	 * @param msg
	 * @param title
	 * @param roomName
	 * @param callerName
	 * @param vidoeCallSignatureModel
	 * @return
	 */
	@Override
	public String sendMobileVidoeCallNotification(String userid, String msg, String title, String roomName,
	                                              String callerName, VidoeCallSignatureModel vidoeCallSignatureModel) {
		try {
			Optional<UserDetails> optionalUserDetails = userDetailsRepository.findById(Long.parseLong(userid));
			if (optionalUserDetails.isPresent() && optionalUserDetails.get()
			                                                          .getToken() != null) {
				if (title.equalsIgnoreCase(ApplicationConstants.CALLING) && optionalUserDetails.get()
				                                                                               .getType()
				                                                                               .equalsIgnoreCase(
						                                                                               ApplicationConstants.ANDROID)) {
					fcmNotification.pushVideoCallFCMNotification(optionalUserDetails.get()
					                                                                .getToken(), msg, title,
					                                             roomName, callerName, vidoeCallSignatureModel);
				} else if (title.equalsIgnoreCase(ApplicationConstants.CALLING) && optionalUserDetails.get()
				                                                                                      .getType()
				                                                                                      .equalsIgnoreCase(
						                                                                                      ApplicationConstants.IOS)) {
					apnsNotification.callApnsNotification(optionalUserDetails.get()
					                                                         .getVoipToken(), title, msg, roomName,
					                                      callerName, vidoeCallSignatureModel);
				} else {
					sendMobileNotification(optionalUserDetails.get()
					                                          .getType(), msg, title, optionalUserDetails.get()
					                                                                                     .getToken());
					//FcmNotification.pushFCMNotification(optionalUserDetails.get().getToken(), msg, title);
				}
				return ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION;
			} else {
				return ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_TOKEN_IS_EMPTY;
			}

		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return ApplicationConstants.EMPTY_STRING;
	}

	//old code
			/*@Override
			public String sendFCMNotification(String userid, String msg, String title) {
				try {
					// "mobiletoken":
					// "eCR1aJATaBA:APA91bG4tN4-F3fXOELIoZHnGnDq4UFCqQjE586NKINfnUwhlbOSFCVexoN9hoVAjjKSPaXikg
					-eDZrGmlu8BLxNEdlxf1pTWrP0wR4RRetrVPwgxQwQ7YFmM3cGvZ-UXGKT_H_2KQ8G"
					Optional<UserDetails> optionalUserDetails = userDetailsRepository.findById(Long.parseLong(userid));

					if (optionalUserDetails.isPresent() && optionalUserDetails.get().getToken() != null) {
						VidyoToken vidyoToken = new VidyoToken();
						if (title.equalsIgnoreCase(ApplicationConstants.CALLING)) {
							vidyoToken = vidyoTokenService.fetchTokenById(1L);
							fcmNotification.pushvideocallFCMNotification(optionalUserDetails.get().getToken(), msg,
							title,
									vidyoToken.getTokenNumber());
						} else {
							fcmNotification.pushFCMNotification(optionalUserDetails.get().getToken(), msg, title);
								
						}
						return "Sent_Notification";
					} else {
						return "Token_is_Empty";
					}

				} catch (Exception e) {
					e.getLocalizedMessage();
				}
				return ApplicationConstants.EMPTY_STRING;
			}*/

	/**
	 * @param type
	 * @return
	 */
	@Override
	public List<Notification> fetchByNotificationType(String type) {
		List<Notification> notifications = notificationRepository.findBytype(type);
		// after fetchig nptifications in same call we are softly deleting those
		// notifications
		for (Notification notification : notifications) {
			notification.setDeleteflag(true);
			addWebNotification(notification);
		}
		return notifications;
	}

	@Override
	public List<NotificationModel> fetchNotificationModelsByNotificationType(String type)
			throws DataValidationException {
		if (StringUtils.isBlank(type)) {
			throw new DataValidationException("Notification type not empty",
			                                  APIErrorKeys.ERROR_INPUT_BADREQUEST);
		}
		List<Notification> notifications = notificationRepository.findBytype(type);
		List<NotificationModel> notificationModelList = new ArrayList<NotificationModel>();
		for (Notification notification : notifications) {
			notificationModelList.add(utility.convertNotificationEntityToModel(notification));
		}
		return notificationModelList;
	}

	@Autowired
	private SimpMessagingTemplate template;

	/**
	 * For calender notification
	 *
	 * @param notification
	 * @return
	 */
	@Override
	public Notification addWebNotification(Notification notification) {
		log.info("{} ===applciaton name==== {}", ApplicationConstants.CallType.PSTN.name(), notification.getType());
		if (!ApplicationConstants.CallType.PSTN.name()
		                                       .equalsIgnoreCase(notification.getType()) &&
				notification.getVidoeCallSignatureModel() != null) {
			List<NotificationReceivedUser> notificationReceivedUserList = notification
					.getNotificationReceivedUserList();
			if (notificationReceivedUserList != null && notificationReceivedUserList.size() > 0) {
				NotificationReceivedUser notificationReceivedUser = notificationReceivedUserList.get(0);
				UserDetails userDetails = notificationReceivedUser.getUserDetails();
				if (notification.getVidoeCallSignatureModel() != null) {
					template.convertAndSendToUser(
							String.valueOf(userDetails.getId()),
							"/queue/notification",
							notification.getVidoeCallSignatureModel()
					);
				}
				Notification persistedNotification = notificationRepository.save(notification);
				return persistedNotification;
			}
		} else {
			Notification persistedNotification = notificationRepository.save(notification);
			return persistedNotification;
		}
		return null;
	}

	/**
	 * For calender notification
	 *
	 * @param recievingUseridlist
	 * @param belongingUserId
	 * @param msg
	 * @param title
	 * @param type
	 * @return
	 */
	@Override
	public Notification addWebNotification(List<Long> recievingUseridlist, long belongingUserId, String msg,
	                                       String title,
	                                       String type) {
		Notification notification = new Notification();
		notification.setMessage(msg);
		notification.setType(type);
		notification.setTitle(title);
		notification.setDeleteflag(false);
		notification.setTime(Date.from(Instant.now()));
		Optional<UserDetails> optionalBelongingUserDetails = userDetailsRepository.findById(belongingUserId);
		notification.setBelongigngUserDetails(
				optionalBelongingUserDetails.isPresent() ? optionalBelongingUserDetails.get() : null);
		List<NotificationReceivedUser> notificationReceivedUserList = new ArrayList<>();
		for (int i = 0; i < recievingUseridlist.size(); i++) {
			Optional<UserDetails> optionalUserDetails = userDetailsRepository.findById((recievingUseridlist.get(i)));
			if (optionalUserDetails.isPresent()) {
				NotificationReceivedUser notificationReceivedUser = new NotificationReceivedUser();
				notificationReceivedUser.setUserDetails(optionalUserDetails.get());
				notificationReceivedUser.setNotification(notification);
				notificationReceivedUserList.add(notificationReceivedUser);
			}
		}
		notification.setNotificationReceivedUserList(notificationReceivedUserList);
		notification = addWebNotification(notification);
		return notification;
	}

	/**
	 * For calender notification
	 *
	 * @param belongingUserId
	 * @param msg
	 * @param title
	 * @param type
	 * @return
	 */
	@Override
	public Notification addWebNotification(long belongingUserId, String msg, String title,
	                                       String notificationType) {
		Notification notification = new Notification();
		notification.setMessage(msg);
		notification.setType(notificationType);
		notification.setTitle(title);
		notification.setDeleteflag(false);
		notification.setTime(Date.from(Instant.now()));
		Optional<UserDetails> optionalBelongingUserDetails = userDetailsRepository.findById(belongingUserId);
		notification.setBelongigngUserDetails(
				optionalBelongingUserDetails.isPresent() ? optionalBelongingUserDetails.get() : null);
		//adding empty list
		notification.setNotificationReceivedUserList(new ArrayList<NotificationReceivedUser>());
		notification = addWebNotification(notification);
		return notification;
	}

	/**
	 * @return
	 */
	@Override
	public List<Notification> fetchAllNotifications() {
		List<Notification> notifications = notificationRepository.findAll();
		return notifications;
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public String deleteNotificationById(long id) {
		if (notificationRepository.existsById(id)) {
			notificationRepository.deleteById(id);
			return "success";
		} else {
			return "notfound";
		}
	}

	//this code is for twilio video/general push notification(not active)

	/**
	 * @param userid
	 * @param msg
	 * @param title
	 * @param roomName
	 * @param callerName
	 * @return
	 */
	@Override
	public String sendTwilioVideoFCMNotification(String userid, String msg, String title, String roomName,
	                                             String callerName) {
		try {
			// "mobiletoken":
			// "eCR1aJATaBA:APA91bG4tN4-F3fXOELIoZHnGnDq4UFCqQjE586NKINfnUwhlbOSFCVexoN9hoVAjjKSPaXikg
			// -eDZrGmlu8BLxNEdlxf1pTWrP0wR4RRetrVPwgxQwQ7YFmM3cGvZ-UXGKT_H_2KQ8G"

			Optional<UserDetails> optionalUserDetails = userDetailsRepository.findById(Long.parseLong(userid));

			if (optionalUserDetails.isPresent() && optionalUserDetails.get()
			                                                          .getToken() != null) {
				if (title.equalsIgnoreCase(ApplicationConstants.CALLING)) {
					fcmNotification.pushTwilioFCMNotification(optionalUserDetails.get()
					                                                             .getToken(), msg, title, roomName,
					                                          callerName);
				} else {
					fcmNotification.pushFCMNotification(optionalUserDetails.get()
					                                                       .getToken(), msg, title);
				}
				return ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION;
			} else {
				return ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_TOKEN_IS_EMPTY;
			}

		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return ApplicationConstants.EMPTY_STRING;
	}

	/**
	 * @param userid
	 * @param type
	 * @return
	 */
	@Override
	public Notification fetchNotificationsByUseridAndType(long userid, String type) {
		try {
			LocalDateTime currentLocalDateTime = LocalDateTime.now(ZoneOffset.UTC);
			NotificationReceivedUser notificationreceivedUser =
					notificationReceivedUserRepository.findTop1ByuserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc(
							userid, type, false);
			List<NotificationReceivedUser> notificationReceivedUserList = notificationReceivedUserRepository
					.findByuserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc(userid, type,
					                                                                                 false);
			for (NotificationReceivedUser notificationReceivedUser : notificationReceivedUserList) {
				notificationReceivedUser.getNotification()
				                        .setDeleteflag(true);
				addWebNotification(notificationReceivedUser.getNotification());
			}
			if (notificationreceivedUser != null) {
				Duration duration = Duration.between(
						dateUtility.convertDateObjectToLocalDateTimeObject(notificationreceivedUser.getNotification()
						                                                                           .getTime()),
						currentLocalDateTime);
				if (60000 >= duration.toMillis()) {
					return notificationreceivedUser.getNotification();
				}
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
			return null;
		}
		return null;
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public Notification fetchNotificationById(long id) {
		Optional<Notification> optionalNotification = notificationRepository.findById(id);
		if (optionalNotification.isPresent()) {
			return optionalNotification.get();
		} else {
			return null;
		}
	}

	/**
	 * @param belongingUserId
	 * @return
	 */
	@Override
	public List<Notification> fetchNotificationsByBelongingUserId(long belongingUserId) {
		List<Notification> notificationList = notificationRepository.findBybelongigngUserDetails_id(belongingUserId);
//		List<Notification> sortedList = notificationList.stream()
//		                                                .sorted(Comparator.nullsLast(comparator)(Notification::getId))
//		                                                .collect(Collectors.toList());
		notificationList.sort(Comparator.comparing(Notification::getId, Comparator.nullsLast(Comparator.naturalOrder())));
		return notificationList;
	}

	/**
	 * @param start
	 * @param end
	 * @param belonginguserid
	 * @return
	 */
	@Override
	public List<Notification> fetchNotificationsByUseridBetweenDates(Date start, Date end, long belonginguserid) {
		List<Notification> notificationList = new ArrayList<>();
		try {
			notificationList = notificationRepository.findByTimeBetweenAndBelongigngUserDetails_id(start, end,
			                                                                                       belonginguserid);
			return notificationList;
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		return notificationList;
	}

	/**
	 * @param notificationList
	 */
	@Override
	public void deleteNotifications(List<Notification> notificationList) {
		notificationRepository.deleteAll(notificationList);
	}

	@Override
	public Notification getNotificationByRoomName(String roomName) {
		return notificationRepository.findByroomName(roomName);
	}

	public Notification saveNotification(Notification notification) {
		return notificationRepository.save(notification);
	}

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	PatientService patientService;

	@Autowired
	Utility utility;

	@Autowired
	PrescriptionHelper prescriptionHelper;

	@Autowired
	CaregiverService caregiverService;

	@Override
	public CalendarNotificationModel calenderNotificationsWithOutScheduledByPatientId(Long userId,
	                                                                                  CalenderNotificationRequestModel calenderNotificationRequestModel)
			throws ApplicationException, DataValidationException {
		String zoneId = calenderNotificationRequestModel.getZoneId();
		String startTime = " 00:00";
		String endTime = " 23:59";
		LocalDateTime
				startLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				calenderNotificationRequestModel.getStartDate() + startTime, zoneId);
		LocalDateTime endLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				calenderNotificationRequestModel.getEndDate() + endTime, zoneId);
		Date startDateObjInUTC = java.util.Date.from(startLocalDateTimeObjInUTC
				                                             .atZone(ZoneOffset.UTC)
				                                             .toInstant());
		Date endDateObjInUTC = java.util.Date.from(endLocalDateTimeObjInUTC
				                                           .atZone(ZoneOffset.UTC)
				                                           .toInstant());
		UserDetails userDetails = userDetailsService.fetchUserDetailEntitiysById(userId);
		if (userDetails == null) {
			throw new DataValidationException("usernotfound", APIErrorKeys.ERROR_USER_NOT_FOUND);
		}
		List<Notification> notificationList =
				fetchNotificationsByUseridBetweenDates(startDateObjInUTC, endDateObjInUTC, (userId));
		CalendarNotificationModel calendarNotificationModel = new CalendarNotificationModel();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Patient patient = patientService.findByUserDetails_id(userId);
		PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
		calendarNotificationModel.setUserDetails(patientModel.getUserDetails());
		if (patientModel.getCaregivers() != null) {
			calendarNotificationModel.setCaregivers(patientModel.getCaregivers());
		}
		if (patientModel.getProviders() != null) {
			calendarNotificationModel.setProviders(patientModel.getProviders());
		}

		List<CalenderEventModel> calenderEventModelList = new LinkedList<CalenderEventModel>();
		notificationList = notificationList.stream()
		                                   .sorted(Comparator.comparing(Notification::getTime))
		                                   .collect(Collectors.toList());
		for (Notification notification : notificationList) {
			CalenderEventModel calenderEventModel = new CalenderEventModel();
			calenderEventModel.setId(notification.getId());
			calenderEventModel.setTitle(notification.getTitle());
			if (notification.getType() != null) {
				calenderEventModel.setType(notification.getType());
			} else {
				calenderEventModel.setType(ApplicationConstants.EMPTY_STRING);
			}
			calenderEventModel.setDescription(notification.getMessage());
			calenderEventModel.setStart(
					dateUtility.convertUTCDateToLocalDateByZoneID(dateFormat.format(notification.getTime()), zoneId,
					                                              "yyyy-MM-dd"));
			calenderEventModel.setEnd(
					dateUtility.convertUTCDateToLocalDateByZoneID(dateFormat.format(notification.getTime()), zoneId,
					                                              "yyyy-MM-dd"));
			calenderEventModelList.add(calenderEventModel);
		}
		calendarNotificationModel.setEvents(calenderEventModelList);
		return calendarNotificationModel;
	}

	@Override
	public CalendarNotificationModel calenderNotificationsByPatientId(Long userId,
	                                                                  CalenderNotificationRequestModel calenderNotificationRequestModel)
			throws Exception {
		String zoneId = calenderNotificationRequestModel.getZoneId();
		String startTime = " 00:00";
		String endTime = " 23:59";
		LocalDateTime
				startLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				calenderNotificationRequestModel.getStartDate() + startTime, zoneId);
		LocalDateTime endLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				calenderNotificationRequestModel.getEndDate() + endTime, zoneId);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		CalendarNotificationModel calendarNotificationModel =
				calenderNotificationsWithOutScheduledByPatientId(userId, calenderNotificationRequestModel);
		List<PrescriptionSchedule> prescriptionScheduleList =
				prescriptionHelper.fetchPrescriptionSchedulesBetweenActaulDatesByUserId(startLocalDateTimeObjInUTC,
				                                                                        endLocalDateTimeObjInUTC,
				                                                                        userId);
		prescriptionScheduleList = prescriptionScheduleList.stream()
		                                                   .sorted(Comparator.comparing(
				                                                   PrescriptionSchedule::getFireTime))
		                                                   .collect(Collectors.toList());
		for (PrescriptionSchedule schedule : prescriptionScheduleList) {
			CalenderEventModel calenderEventModel = new CalenderEventModel();
			calenderEventModel.setId(schedule.getId());
			//need to re optimize
			String strZonedDateTime = dateUtility.convertUTCLocalDateTimeObjectToZonedLocalDateTimeByZoneID(
					dateFormat.format(java.util.Date.from(schedule.getPrescribedTime()
					                                              .toInstant(ZoneOffset.UTC))), zoneId,
					"yyyy-MM-dd HH:mm");
			if (schedule.getDosageInfo() != null) {
				calenderEventModel.setTitle("Medication has scheduled");//have to write if condition for this
				calenderEventModel.setType("Medication");
				calenderEventModel.setDescription(schedule.getDosageInfo()
				                                          .getMedicine()
				                                          .getName() + " has to take at " + strZonedDateTime);
			}
			if (schedule.getDeviceInfo() != null) {
				calenderEventModel.setTitle("Device has scheduled");//have to write if condition for this
				calenderEventModel.setType("Device");
				calenderEventModel.setDescription(schedule.getDeviceInfo()
				                                          .getDevice()
				                                          .getName() + " reading has to take at " + strZonedDateTime);
			}
			calenderEventModel.setStart(dateUtility.convertUTCDateToLocalDateByZoneID(
					DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
					                 .format(schedule.getPrescribedTime()), zoneId, "yyyy-MM-dd"));
			calenderEventModel.setEnd(dateUtility.convertUTCDateToLocalDateByZoneID(
					DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
					                 .format(schedule.getPrescribedTime()), zoneId, "yyyy-MM-dd"));
			calendarNotificationModel.getEvents()
			                         .add(calenderEventModel);
		}
		return calendarNotificationModel;
	}

	@Override
	public String sendVideoNotificationByUserId(VideoNotificationRequestModel videoNotificationRequestModel)
			throws DataValidationException {
		String msg = ApplicationConstants.EMPTY_STRING;
		String title = ApplicationConstants.CALLING;
		UserDetails belongingUserDetails = null;
		if (videoNotificationRequestModel.getBelongingId() != null) {
			Optional<UserDetails>
					optionalBelongingUserDetails =
					userDetailsRepository.findById(videoNotificationRequestModel.getBelongingId());
			belongingUserDetails = optionalBelongingUserDetails.isPresent() ? optionalBelongingUserDetails.get() :
					null;
		}
		Optional<UserDetails> optionalUserDetails =
				userDetailsRepository.findById(videoNotificationRequestModel.getUserId());
		if (optionalUserDetails.isPresent()) {
			UserDetails userDetails = optionalUserDetails.get();
			Notification notification = new Notification();
			List<NotificationReceivedUser> notificationReceivedUserList = new ArrayList<>();
			notification.setType("call");
			notification.setTitle(title);
			NotificationReceivedUser notificationReceivedUser = new NotificationReceivedUser();
			notificationReceivedUser.setUserDetails(userDetails);
			notificationReceivedUser.setNotification(notification);
			notificationReceivedUserList.add(notificationReceivedUser);
			notification.setNotificationReceivedUserList(notificationReceivedUserList);
			notification.setBelongigngUserDetails(belongingUserDetails);
			if (belongingUserDetails != null) {
				msg = belongingUserDetails.getFirstName() + " " + belongingUserDetails.getLastName() +
						" had a video call with " + userDetails.getFirstName() + " " + userDetails.getLastName();
			}
			notification.setMessage(msg);
			notification.setRoomName(videoNotificationRequestModel.getRoomName());
			notification.setDeleteflag(false);
			notification.setTime(Date.from(Instant.now()));

			if (ApplicationConstants.PATIENT.equalsIgnoreCase(userDetails.getRole()
			                                                             .getName()) && belongingUserDetails != null) {
				String result =
						sendTwilioVideoFCMNotification(String.valueOf(userDetails.getId()), msg,
						                               title,
						                               videoNotificationRequestModel.getRoomName(),
						                               belongingUserDetails.getFirstName() + " " +
								                               belongingUserDetails.getLastName());
				notification.setDeleteflag(true);
				addWebNotification(notification);
				if (result.equals("Token_is_Empty")) {
					throw new DataValidationException("userNotFound", APIErrorKeys.ERROR_USER_NOT_FOUND);
				}
			}
			if (ApplicationConstants.CAREGIVER.equalsIgnoreCase(userDetails.getRole()
			                                                               .getName()) &&
					belongingUserDetails != null) {
				Caregiver caregiver = caregiverService.fetchCaregiverByUserId(optionalUserDetails.get()
				                                                                                 .getId());
				if (caregiver != null && ApplicationConstants.ANDROID.equalsIgnoreCase(caregiver.getUserDetails()
				                                                                                .getType()) &&
						belongingUserDetails != null) {
					sendTwilioVideoFCMNotification(String.valueOf(optionalUserDetails.get()
					                                                                 .getId()),
					                               msg,
					                               title,
					                               videoNotificationRequestModel.getRoomName(),
					                               belongingUserDetails.getFirstName() + " " +
							                               belongingUserDetails.getLastName());
					notification.setDeleteflag(true);
					addWebNotification(notification);
				} else if (caregiver != null) {
					addWebNotification(notification);
				}
			}
			if (ApplicationConstants.PHARMACIST.equalsIgnoreCase(userDetails.getRole()
			                                                                .getName())
					|| ApplicationConstants.PROVIDER.equalsIgnoreCase(userDetails.getRole()
					                                                             .getName())) {
				addWebNotification(notification);
			}
		}
		return ApplicationConstants.SUCCESS;
	}

	@Override
	public String sendFcmNotification(FcmNotificationRequestModel fcmNotificationRequestModel) throws Exception {
		for (int i = 0; i < fcmNotificationRequestModel.getEmailIds()
		                                               .size(); i++) {
			// if userId belongs to caregiver and if he is using mobile has to
			// send direct fcmNotification other wise add notification.
			UserDetails userDetails = userDetailsRepository.findByEmailIdIgnoreCase(
					fcmNotificationRequestModel.getEmailIds()
					                           .get(i));
			if (userDetails != null) {
				Notification notification = new Notification();
				notification.setType(fcmNotificationRequestModel.getTitle());
				notification.setTitle(fcmNotificationRequestModel.getTitle());
				List<NotificationReceivedUser> notificationReceivedUserList = new ArrayList<>();
				NotificationReceivedUser notificationReceivedUser = new NotificationReceivedUser();
				notificationReceivedUser.setUserDetails(userDetails);
				notificationReceivedUser.setNotification(notification);
				notificationReceivedUserList.add(notificationReceivedUser);
				notification.setNotificationReceivedUserList(notificationReceivedUserList);
				UserDetails decryBelongingUserDetails = null;
				Optional<UserDetails> optionalBelongingUserDetails =
						userDetailsRepository.findById(fcmNotificationRequestModel.getBelongingId());
				if (optionalBelongingUserDetails.isPresent()) {
					UserDetails belongingUserDetails = optionalBelongingUserDetails.get();
					decryBelongingUserDetails = belongingUserDetails;
				}
				notification.setBelongigngUserDetails(
						optionalBelongingUserDetails.isPresent() ? optionalBelongingUserDetails.get() : null);
				notification.setTime(Date.from(Instant.now()));
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(userDetails.getEmailId());
				String message = "";
				if (optionalBelongingUserDetails.isPresent()) {
					message = decryBelongingUserDetails.getFirstName() + " "
							+ decryBelongingUserDetails.getLastName() + ":" + fcmNotificationRequestModel.getMessage();
					notification.setMessage(message);
					if (StringUtils.isNotEmpty(fcmNotificationRequestModel.getTitle())) {
						message = decryBelongingUserDetails.getFirstName() + " "
								+ decryBelongingUserDetails.getLastName() + " : " +
								fcmNotificationRequestModel.getMessage();
					}
				} else {
					notification.setMessage(fcmNotificationRequestModel.getMessage());
					if (StringUtils.isNotEmpty(fcmNotificationRequestModel.getTitle())) {
						message = fcmNotificationRequestModel.getMessage();
					}
				}
				if (userDetails.getType() != null &&
						(ApplicationConstants.ANDROID.equalsIgnoreCase(userDetails.getType()) ||
								ApplicationConstants.IOS.equalsIgnoreCase(userDetails.getType()))) {
					String result = sendMobileNotification(userDetails.getType(),
					                                       fcmNotificationRequestModel.getMessage(),
					                                       fcmNotificationRequestModel.getTitle(),
					                                       userDetails.getToken());
					if (result.equals("Sent_Notification")) {
						return ApplicationConstants.SUCCESS;
					} else if (result.equals("Token_is_Empty")) {
						throw new DataValidationException("userNotFound", APIErrorKeys.ERROR_USER_NOT_FOUND);
					} else {
						throw new DataValidationException("Notification not sent", APIErrorKeys.ERROR_USER_NOT_FOUND);
					}
				}
				if (userDetails.getType() != null &&
						userDetails.getType()
						           .equalsIgnoreCase(ApplicationConstants.WEB)) {
					Notification persistedNotification =
							addWebNotification(userDetails.getId(), message,
							                   fcmNotificationRequestModel.getTitle(),
							                   userDetails.getType());
					if (persistedNotification.getId() <= 0) {
						throw new DataValidationException("Notification not sent",
						                                  APIErrorKeys.ERROR_INPUT_BADREQUEST);
					}
				}
			}
		}
		return ApplicationConstants.FAIL;
	}

	@Override
	public UserDetailsModel updateTokenInUserDetails(APNSUpdateTokenRequestModel aPNSUpdateTokenRequestModel) {
		List<UserDetailsModel> userDetailsModelList =
				userDetailsService.fetchUserDetailsByToken(aPNSUpdateTokenRequestModel.getToken());
		for (UserDetailsModel userDetailsModel : userDetailsModelList) {
			userDetailsModel.setToken(null);
			userDetailsModel.setVoipToken(null);
			userDetailsService.updateUserDetails(userDetailsModel);
		}
		UserDetails userDetails =
				userDetailsRepository.findByEmailIdIgnoreCase(aPNSUpdateTokenRequestModel.getEmailId());
		if (userDetails != null) {
			UserDetailsModel userDetailsModel = utility.convertUserDetailsEntityToModel(userDetails);
			userDetailsModel.setToken(aPNSUpdateTokenRequestModel.getToken());
			userDetailsModel.setType(aPNSUpdateTokenRequestModel.getTypeOfDevice());
			if (StringUtils.isNotEmpty(aPNSUpdateTokenRequestModel.getToken())) {
				userDetailsModel.setVoipToken(aPNSUpdateTokenRequestModel.getVoIpToken());
			}
			return userDetailsService.updateUserDetails(userDetailsModel);
		}
		return null;
	}

	@Override
	public NotificationModel addNotificationFromController(NotificationRequestModel notificationRequestModel)
			throws Exception {
		Notification notification = new Notification();
		notification.setMessage(notificationRequestModel.getMessage());
		notification.setType(notificationRequestModel.getType());
		notification.setTitle(notificationRequestModel.getType());
		Optional<UserDetails> optionalBelongingUserDetails =
				userDetailsRepository.findById(notificationRequestModel.getBelongingUserId());
		if (optionalBelongingUserDetails.isPresent()) {
			notification.setBelongigngUserDetails(optionalBelongingUserDetails.get());
			notification.setDeleteflag(false);
			notification.setTime(Date.from(Instant.now()));
			List<NotificationReceivedUser> notificationReceivedUserList = new ArrayList<>();
			for (int i = 0; i < notificationRequestModel.getUserIds()
			                                            .size(); i++) {
				Optional<UserDetails> optionalUserDetails =
						userDetailsRepository.findById(notificationRequestModel.getUserIds()
						                                                       .get(i));
				if (optionalUserDetails.isPresent()) {
					NotificationReceivedUser notificationReceivedUser = new NotificationReceivedUser();
					notificationReceivedUser.setUserDetails(optionalUserDetails.get());
					notificationReceivedUser.setNotification(notification);
					notificationReceivedUserList.add(notificationReceivedUser);
				}
			}
			notification.setNotificationReceivedUserList(notificationReceivedUserList);
			Notification persistedNotification = addWebNotification(notification);
			return
					utility.convertNotificationEntityToModel(persistedNotification);
		}
		return null;
	}

	@Autowired
	DosageInfoService dosageInfoService;

	@Autowired
	DosageNotificationRepository dosageNotificationRepository;

	@Override
	public CaregiverDosageNotificationModel addCaregiverNotifications(
			CaregiverDosageNotificationModel caregiverDosageNotificationModel) throws Exception {
		DosageInfo dosageInfo = dosageInfoService
				.fetctDosageInfoByID(caregiverDosageNotificationModel.getDosageInfoID());
		if (dosageInfo != null) {
			// fetching already available notification for this patinet.
			List<DosageNotification> existedDosageNotificationList = dosageNotificationRepository
					.findByDosageInfo_idAndUserId(dosageInfo.getId(),
					                              caregiverDosageNotificationModel.getOwneruserid());
			CopyOnWriteArrayList<DosageNotification> safeExistedDosageNotificationList =
					new CopyOnWriteArrayList<DosageNotification>(
							existedDosageNotificationList);

			for (DosageNotificationModel dosageNotificationModel :
					caregiverDosageNotificationModel.getDosageNotificationList()) {
				DosageNotification dosageNotification =
						utility.convertDosageNotificationModelToEntity(dosageNotificationModel);
				dosageNotification.setDosageInfo(dosageInfo);
				if (!existedDosageNotificationList.contains(dosageNotification)) {
					if (dosageNotification.getId() == 0) {
						addDosageNotification(dosageNotificationModel, dosageInfo);
					} else if (dosageNotification.getId() != 0) {
						dosageNotificationRepository.save(dosageNotification);
						safeExistedDosageNotificationList.remove(dosageNotification);
					}
				}
				if (safeExistedDosageNotificationList.contains(dosageNotification)) {
					safeExistedDosageNotificationList.remove(dosageNotification);
				}
			}
			dosageNotificationRepository.deleteAll(
					(Iterable<DosageNotification>) safeExistedDosageNotificationList);
		}
		return caregiverDosageNotificationModel;
	}

	@Override
	public List<DosageNotificationModel> fetchDosageNotificationsByCaregiverId(Long caregiverUserId,
	                                                                           Long dosageInfoId) {
		List<DosageNotification> dosageNotificationList = dosageNotificationRepository
				.findByDosageInfo_idAndUserId(dosageInfoId, caregiverUserId);
		List<DosageNotificationModel> dosageNotificationModelList = new ArrayList<DosageNotificationModel>();
		dosageNotificationList.forEach(dosageNotification -> {
			dosageNotificationModelList.add(utility.convertDosageNotificationEntityToModel(dosageNotification));
		});
		return dosageNotificationModelList;
	}

	private DosageNotification addDosageNotification(DosageNotificationModel dosageNotificationModel,
	                                                 DosageInfo dosageInfo) {
		DosageNotification dosageNotification = new DosageNotification();
		dosageNotification.setCountryCode(dosageNotificationModel.getCountryCode());
		dosageNotification.setExpectedTime(dosageNotificationModel.getExpectedTime());
		dosageNotification.setId(dosageNotificationModel.getId());
		dosageNotification.setNotificationType(dosageNotificationModel.getNotificationType());
		dosageNotification.setNotificationValue(dosageNotificationModel.getNotificationValue());
		dosageNotification.setDosageInfo(dosageInfo);
		dosageNotification.setUserId(dosageNotificationModel.getUserId());
		dosageNotification = dosageNotificationRepository.save(dosageNotification);
		return dosageNotification;
	}
}
