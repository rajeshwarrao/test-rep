package com.sg.dayamed.service.impl;

import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.pojo.KeyValueVO;
import com.sg.dayamed.helper.pojo.ResultVO;
import com.sg.dayamed.pojo.PasswordChangeReqModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	Utility utility;

	@Autowired
	PasswordEncryption pwdEncryption;

	@Autowired
	SendResetPwdNotification sendForgotPwdEmail;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;

	@Value("${dayamed.email.subject.pwd.update}")
	String emailPwdUpdateSubject;

	@Value("${dayamed.email.body.pwd.update}")
	String emailBodyForPwdUpdate;

	@Autowired
	UserPreferenceRepository userPreferenceRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	ObjectMapper userPreferenceModelMapper;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public UserDetailsModel findByEmailIdIgnoreCase(String emailid) {

		UserDetails UserDetails = userDetailsRepository.findByEmailIdIgnoreCase(emailid);
		if (UserDetails != null) {
			UserDetailsModel userDetailsModel = utility.convertUserDetailsEntityToModel(UserDetails);
			return userDetailsModel;
		}
		return null;
	}

	@Override
	public UserDetailsModel updateUserDetails(UserDetailsModel userDetailsModel) {
		UserDetails userDetails = userDetailsRepository.findByEmailIdIgnoreCase(userDetailsModel.getEmailId());// need
		// to
		// check
		UserDetails userdetailsEntity = utility.convertUserDetailsModelToEntity(userDetailsModel);
		// userdetailsEntity.setPassword(pwdEncryption.encrypt(userDetails.getFirstName()));
		userDetails = userDetailsRepository.save(userdetailsEntity);
		return utility.convertUserDetailsEntityToModel(userDetails);
	}

	@Override
	public UserDetailsModel fetchUserDetailsById(Long userid) {
		Optional<UserDetails> optionalUserDetailsEntity = userDetailsRepository.findById(userid);
		UserDetailsModel userDetailsModel = null;
		if (optionalUserDetailsEntity.isPresent()) {
			userDetailsModel = utility.convertUserDetailsEntityToModel(optionalUserDetailsEntity.get());
		}
		return userDetailsModel;
	}

	public UserDetails fetchUserDetailEntitiysById(Long userid) {
		Optional<UserDetails> optionalUserDetailsEntity = userDetailsRepository.findById(userid);
		return optionalUserDetailsEntity.isPresent() ? optionalUserDetailsEntity.get() : null;
	}

	@Override
	public UserDetails fetchDecryptedUserDetailsById(Long userid) {
		Optional<UserDetails> optionalUserDetailsEntity = userDetailsRepository.findById(userid);
		return optionalUserDetailsEntity.isPresent() ? optionalUserDetailsEntity.get() : null;
	}

	@Override
	public List<UserDetailsModel> fetchUserDetailsByToken(String token) {
		List<UserDetails> userDetailsList = userDetailsRepository.findByTokenIgnoreCase(token);
		List<UserDetailsModel> userDetailsModelList = new ArrayList<>();
		for (UserDetails userDetailsEntity : userDetailsList) {
			userDetailsModelList.add(utility.convertUserDetailsEntityToModel(userDetailsEntity));
		}
		return userDetailsModelList;
	}

	@Override
	public String resetPassword(String tokenId, String password) {
		UserDetails userDetails = userDetailsRepository.findByPwdChangeToken(tokenId);
		try {
			if (userDetails != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String strPwdExpDate = sdf.format(userDetails.getPwdTokenExpireDate());
				Date datePwdExpDate = sdf.parse(strPwdExpDate);
				if (userDetails.getPwdTokenExpireDate() != null && Date.from(Instant.now())
				                                                       .before(datePwdExpDate)) {
					userDetails.setPassword(password);
					userDetails.setPwdChangeToken(null);
					userDetails.setPwdTokenExpireDate(null);
					userDetailsRepository.save(userDetails);
					return ApplicationConstants.SUCCESS;
				} else {
					return ApplicationConstants.EXPIRED_TOKEN;
				}
			} else {
				return ApplicationConstants.INVALID_TOKEN;
			}
		} catch (Exception e) {
			logger.error("Exception occurred when resetting password", e);
			return ApplicationConstants.INVALID_TOKEN;
		}
	}

	@Override
	public String forgotPassword(String emailId) {
		UserDetails userDetails = userDetailsRepository.findByEmailIdIgnoreCase(emailId);
		if (userDetails != null) {
			String changeToken = pwdEncryption.encrypt(userDetails.getEmailId() + System.currentTimeMillis());
			userDetails.setPwdChangeToken(changeToken);
			userDetails.setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));
			userDetailsRepository.save(userDetails);
			userDetails.setPwdChangeToken(changeToken);
			sendForgotPwdEmail.sendEmailNotification(userDetails, userDetails.getRole()
			                                                                           .getName(),
			                                         emailPwdUpdateSubject, emailBodyForPwdUpdate);
			return ApplicationConstants.SUCCESS;
		} else {
			return ApplicationConstants.INVALID_EMAILID;
		}
	}

	@Override
	public UserDetails save(UserDetails userDetails) {
		return userDetailsRepository.save(userDetails);
	}

	@Override
	public UserPreferenceModel findByEmailId(String emailId) {
		UserPreferenceModel userPreferenceModelUpdated = null;
		try {
			if (StringUtils.isNotEmpty(emailId)) {
				UserDetails userDetails = userDetailsRepository.findByEmailIdIgnoreCase(emailId);
				if (userDetails != null) {
					UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userDetails.getId());
					String dbUserPrefSetting = userPreference.getUserPrefSetting();
					userPreferenceModelUpdated = userPreferenceModelMapper.readValue(dbUserPrefSetting,
					                                                                 UserPreferenceModel.class);
				}
			}
		} catch (Exception e) {

		}
		return userPreferenceModelUpdated;
	}

	@Override
	public List<String> findByEmailIdIn(List<String> emailIds) {
		List<String> effectiveEmails = new ArrayList<>();
		List<String> resultEmails = new ArrayList<>();
		effectiveEmails.addAll(emailIds);
		List<KeyValueVO> keyValueVOs = emailIds.stream()
		                                       .map(email -> this.decryptedToDBString(email))
		                                       .collect(Collectors.toList());
		effectiveEmails.addAll(keyValueVOs.stream()
		                                  .map(KeyValueVO::getValue)
		                                  .collect(Collectors.toList()));
		List<UserDetails> userDetails = userDetailsRepository.findByEmailIdIgnoreCaseIn(effectiveEmails);
		if (CollectionUtils.isNotEmpty(userDetails)) {
			List<Long> userIds = userDetails.stream()
			                                .map(UserDetails::getId)
			                                .collect(Collectors.toList());
			List<UserPreference> userPreferences = userPreferenceRepository.findByUserDetailsIdIn(userIds);
			if (CollectionUtils.isNotEmpty(userPreferences)) {
				userPreferences.stream()
				               .forEach(pref -> {
					               UserPreferenceModel userPreferenceModel = new UserPreferenceModel();
					               try {
						               userPreferenceModelMapper.configure(
								               DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						               userPreferenceModel =
								               userPreferenceModelMapper.readValue(pref.getUserPrefSetting(),
								                                                   UserPreferenceModel.class);
					               } catch (Exception e) {

					               }
					               UserDetails details = pref.getUserDetails();
					               if (!ApplicationConstants.PROVIDER.equalsIgnoreCase(details.getRole()
					                                                                          .getName())) {
						               if (effectiveEmails.contains(details.getEmailId()) &&
								               userPreferenceModel.getEmailFlag()) {
								               resultEmails.add(details.getEmailId());
						               }
					               } else {
						               resultEmails.add(details.getEmailId());
					               }
				               });
			}
		}
		return resultEmails;
	}

	private KeyValueVO decryptedToDBString(String value) {
		return new KeyValueVO(value, value);
	}

	public Boolean findByPhoneNumber(String phoneNumber) {
		KeyValueVO keyValueVO = this.decryptedToDBString(phoneNumber);
		UserDetails userDetails = userDetailsRepository.findTopByMobileNumber(keyValueVO.getValue());
		UserPreferenceModel userPreferenceModel = new UserPreferenceModel();
		if (userDetails != null) {
			UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userDetails.getId());
			if (userPreference != null) {
				String userPref = userPreference.getUserPrefSetting();
				try {
					userPreferenceModel = userPreferenceModelMapper.readValue(userPref,
					                                                          UserPreferenceModel.class);
				} catch (Exception e) {
					logger.error("Error while Deserializing preference model.");
				}
			}
		}
		return userPreferenceModel.getSmsFlag();

	}

	public Boolean findByToken(String token) {
		Boolean pushNotificationFlag = true;
		;
		UserDetails userDetails = userDetailsRepository.findByToken(token);
		if (userDetails != null) {
			UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userDetails.getId());
			if (userPreference != null) {
				String userPref = userPreference.getUserPrefSetting();
				try {
					UserPreferenceModel userPreferenceModelUpdated = userPreferenceModelMapper.readValue(userPref,
					                                                                                     UserPreferenceModel.class);
					pushNotificationFlag = userPreferenceModelUpdated.getPushNotificationFlag();
				} catch (Exception e) {

				}
			}
		}

		return pushNotificationFlag;

	}

	@Override
	public List<ResultVO> findByEmailIdInResultVo(List<String> emailIds) {
		List<String> effectiveEmails = new ArrayList<>();
		List<ResultVO> resultVOs = new ArrayList<>();
		effectiveEmails.addAll(emailIds);
		List<KeyValueVO> keyValueVOs = emailIds.stream()
		                                       .map(email -> this.decryptedToDBString(email))
		                                       .collect(Collectors.toList());
		effectiveEmails.addAll(keyValueVOs.stream()
		                                  .map(KeyValueVO::getValue)
		                                  .collect(Collectors.toList()));
		List<UserDetails> userDetails = userDetailsRepository.findByEmailIdIgnoreCaseIn(effectiveEmails);
		if (CollectionUtils.isNotEmpty(userDetails)) {
			List<Long> userIds = userDetails.stream()
			                                .map(UserDetails::getId)
			                                .collect(Collectors.toList());
			List<UserPreference> userPreferences = userPreferenceRepository.findByUserDetailsIdIn(userIds);
			if (CollectionUtils.isNotEmpty(userPreferences)) {
				userPreferences.stream()
				               .forEach(pref -> {
					               UserPreferenceModel userPreferenceModel = new UserPreferenceModel();
					               try {
						               userPreferenceModelMapper.configure(
								               DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						               userPreferenceModel =
								               userPreferenceModelMapper.readValue(pref.getUserPrefSetting(),
								                                                   UserPreferenceModel.class);
						               UserDetails details = pref.getUserDetails();
						               if (userPreferenceModel.getEmailFlag()) {
							               userPreferenceModel.getLanguagePreference();
							               ResultVO resultVO = new ResultVO();
							               resultVO.setEmailId(details.getEmailId());
							               resultVO.setModel(userPreferenceModel);
							               resultVOs.add(resultVO);
						               }
					               } catch (Exception e) {

					               }

				               });
			}
		}
		return resultVOs;

	}

	@Override
	public void changePassword(PasswordChangeReqModel changePasswordRequest, Long userId)
			throws DataValidationException {
		UserDetails userDetails = fetchUserDetailEntitiysById(userId);
		if (userDetails == null) {
			throw new DataValidationException(APIErrorFields.USER_ID, APIErrorKeys.ERRORS_INVALID_USER_ID);
		}
		if (!StringUtils.equalsIgnoreCase(changePasswordRequest.getCurrentPassword(), userDetails.getPassword())) {
			throw new DataValidationException(APIErrorFields.CURRENT_CREDENTIAL,
			                                  APIErrorKeys.ERRORS_INVALID_CURRENT_CREDENTIALS);
		}
		userDetails.setPassword(changePasswordRequest.getNewPassword());
		save(userDetails);
		logger.info("Password changed successfully for the user {}", userId);
	}

	public List<UserDetails> findUserDetailsByIdInAndRole(List<Long> caregiverIds, String role) {
		Role roleObj = roleRepository.findByName(role);
		return userDetailsRepository.findByIdInAndRole(caregiverIds, roleObj);
	}

}
