package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.ConfigParams;
import com.sg.dayamed.repository.ConfigParamRepository;
import com.sg.dayamed.service.LoadConfigParamsOnRedis;
import com.sg.dayamed.service.RediseService;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoadConfigParamsOnRedisImpl implements LoadConfigParamsOnRedis {

	private static final Logger logger = LoggerFactory.getLogger(LoadConfigParamsOnRedisImpl.class);

	@Autowired
	ConfigParamRepository configParamRepository;

	@Autowired
	RediseService rediseService;

	public void loadConfigParamsOnRedis() {
		List<ConfigParams> listConfig = configParamRepository.findAll();
		if (CollectionUtils.isNotEmpty(listConfig)) {
			listConfig.forEach(configParams -> rediseService.configParamsUpdateInRedis(configParams.getKeyParam(),
			                                                                           configParams.getValueParam()));
			logger.debug("Config Parameters Loaded in to Redis server");
		}
	}
}
