package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.NotificationReceivedUser;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.VideoAndVoiceService;
import com.sg.dayamed.util.ApplicationConstants;

import io.jsonwebtoken.impl.Base64UrlCodec;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ZoomVideoAndVoiceServiceImpl implements VideoAndVoiceService {

	private static final Logger logger = LoggerFactory.getLogger(ZoomVideoAndVoiceServiceImpl.class);

	@Value("${dayamed.zoom.video.apikey}")
	String API_KEY;

	@Value("${dayamed.zoom.video.apisecret}")
	String API_SECRET;

	// @Value("${dayamed.zoom.video.jwttoken}")
	// String JWT_TOKEN;

	@Value("${dayamed.zoom.video.jwttoken.expire}")
	long JWT_TOKEN_expire;

	@Value("${dayamed.zoom.video.listofuser.url}")
	String listOfUser1Url;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	RestTemplate restTemplate;


	public String getJWTTokenGenratorByKeyAndSceret() {
		try {
			logger.info("getJWTTokenGenratorByKeyAndSceret....");
			JSONObject clams = new JSONObject();
			long jwtExpireTime = System.currentTimeMillis()+JWT_TOKEN_expire;
			logger.info("getJWTTokenGenratorByKeyAndSceret==jwtExpireTime========>>"+jwtExpireTime);
			clams.put("iss", API_KEY).put("exp", jwtExpireTime);
			String claimsstr = clams.toString();
			JSONObject header = new JSONObject();
			header.put("typ", "JWT");
			header.put("alg", "HS256");
			String headerStr = header.toString();
			String encodedHeader = Base64UrlCodec.BASE64URL.encode(headerStr.getBytes("UTF-8"));
			String encodedClaims = Base64UrlCodec.BASE64URL.encode(claimsstr.getBytes("UTF-8"));
			String concatenated = encodedHeader + '.' + encodedClaims;
			String signature = getSignatureForJWTToken(concatenated, API_SECRET);
			// System.out.println(encodedHeader+"."+encodedClaims+"."+signature);
			return "Bearer " + encodedHeader + "." + encodedClaims + "." + signature;
		} catch (Exception e) {
			logger.error("Exception occurred in the method getJWTTokenGenratorByKeyAndSceret", e);
		}
		return null;
	}

	public String getSignatureForJWTToken(String encodeStr, String scretKey) {
		try {
			logger.info("getSignatureForJWTToken....encodeStr==>"+encodeStr+"===scretKey==>"+scretKey);
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(scretKey.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			return Base64UrlCodec.BASE64URL.encode(sha256_HMAC.doFinal(encodeStr.getBytes()));
		} catch (Exception e) {
			logger.error("Exception occurred in the method getSignatureForJWTToken", e);
		}
		return null;
	}

	public VidoeCallSignatureModel getZoomVideoCallSignature(int userRole, JwtUserDetails jwtUser) {
		VidoeCallSignatureModel vidoeCallSignatureModel = null;
		try {
			logger.info("getZoomVideoCallSignature....userRole==>"+userRole+"===jwtUser==>"+jwtUser);
			long currentTimeMillSec = System.currentTimeMillis();
			String zoomJwtToken = getJWTTokenGenratorByKeyAndSceret();
			if (zoomJwtToken != null) {
				JSONObject userJsonObj = retrieveUserByEmailId(zoomJwtToken, jwtUser, true);
				String zoomUserId = userJsonObj != null ? userJsonObj.optString("id") : null;
				if (zoomUserId != null) {
					JSONObject zoomMettingResponse = getZoomMettingIdByUserId(zoomJwtToken, zoomUserId, userRole);
					if (zoomMettingResponse != null) {
						String meetingNumber = zoomMettingResponse.getString("id");
						vidoeCallSignatureModel = new VidoeCallSignatureModel();
						vidoeCallSignatureModel.setMeetingNumber(meetingNumber);
						vidoeCallSignatureModel.setJoin_url(zoomMettingResponse.getString("join_url"));
						vidoeCallSignatureModel.setPstn_password(zoomMettingResponse.getString("pstn_password"));
						vidoeCallSignatureModel.setPassword(zoomMettingResponse.getString("password"));
						vidoeCallSignatureModel.setH323_password(zoomMettingResponse.getString("h323_password"));
						vidoeCallSignatureModel.setEncrypted_password(zoomMettingResponse.getString("encrypted_password"));
						vidoeCallSignatureModel.setHost_id(zoomMettingResponse.getString("host_id"));
						vidoeCallSignatureModel.setZoomMeetingResponse(zoomMettingResponse.toString());
						if (ApplicationConstants.ANDROID.equalsIgnoreCase(jwtUser.getTypeOfDevice())
								|| ApplicationConstants.IOS.equalsIgnoreCase(jwtUser.getTypeOfDevice())) {
							String zoomStartUrl = zoomMettingResponse.optString("start_url");
							String zoomAccessToken = zoomStartUrl != null ? zoomStartUrl.split("zak=")[1] : null;
							vidoeCallSignatureModel.setZoomAccessToken(zoomAccessToken);
							vidoeCallSignatureModel.setZoomJWTToken(zoomJwtToken);
							vidoeCallSignatureModel.setZoomUserId(zoomUserId);
							vidoeCallSignatureModel.setApiKey(API_KEY);
							vidoeCallSignatureModel.setRole(userRole);
							vidoeCallSignatureModel.setTime(currentTimeMillSec);
							vidoeCallSignatureModel.setSignature(getZoomSignature(vidoeCallSignatureModel));
						} else {
							vidoeCallSignatureModel.setApiKey(API_KEY);
							vidoeCallSignatureModel.setRole(userRole);
							vidoeCallSignatureModel.setTime(currentTimeMillSec);
							vidoeCallSignatureModel.setUserName(jwtUser.getUserName());
							vidoeCallSignatureModel.setEmail("");
							vidoeCallSignatureModel.setPasswd("");
							vidoeCallSignatureModel.setSignature(getZoomSignature(vidoeCallSignatureModel));
						}
						// vidoeCallSignatureModel.setApiSercet(API_SECRET);
					}
				}
				logger.info("zoom userid ==="+zoomUserId+"====getZoomMeetingResponse=="+vidoeCallSignatureModel.getZoomMeetingResponse());
			}
			logger.info("zoomJwtToken ==="+zoomJwtToken);
		} catch (Exception e) {
			logger.error("Exception at getZoomVideoCallSignature", e);
		}
		return vidoeCallSignatureModel;
	}

	public String getZoomSignature(VidoeCallSignatureModel vidoeCallSignatureModel) {
		try {
			logger.info("getZoomSignature==="+vidoeCallSignatureModel);
			String mettingID = vidoeCallSignatureModel.getMeetingNumber();
			String encodeData = Base64UrlCodec.BASE64.encode(
					API_KEY + mettingID + vidoeCallSignatureModel.getTime() + vidoeCallSignatureModel.getRole());
			String hashData = hashData(encodeData, API_SECRET);
			String signature = API_KEY + "." + mettingID + "." + vidoeCallSignatureModel.getTime() + "."
					+ vidoeCallSignatureModel.getRole() + "." + hashData;
			String responseSign = Base64UrlCodec.BASE64.encode(signature.trim());
			return responseSign;
		} catch (Exception e) {
			logger.error("Exception occurred in the method getZoomSignature", e);
		}
		return null;
	}

	public String hashData(String encodeStr, String scretKey) {
		try {
			logger.info("hashData===encodeStr="+encodeStr+"===scretKey==>>"+scretKey);
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(scretKey.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			return Base64.encodeBase64String(sha256_HMAC.doFinal(encodeStr.getBytes()));
		} catch (Exception e) {
			logger.error("Exception occurred in the method hashData", e);
		}
		return null;
	}

	public JSONObject getZoomMettingIdByUserId(String zoomJWTToken, String userid, int role) {
		try {
			logger.info("getZoomMettingIdByUserId===userid="+userid+"===zoomJWTToken==>>"+zoomJWTToken);
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", zoomJWTToken);
			headers.set("Content-Type", "application/json, application/xml");
			headers.set("Accept", "application/json");
			headers.setContentType(MediaType.APPLICATION_JSON);
			MultiValueMap<String, Object> map = new LinkedMultiValueMap();
			map.add("topic", "Dayamed call");
			map.add("type", 1);
			map.add("userId", userid);
			String url = "https://api.zoom.us/v2/users/" + userid + "/meetings";
			try {
				JSONObject requestJson = new JSONObject();
				requestJson.put("topic", "test");
				requestJson.put("type", 1);// 1- instant metting
				requestJson.put("userId", userid);
				JSONObject settingsJson = new JSONObject();
				settingsJson.put("host_video", true);
				settingsJson.put("participant_video", true);
				settingsJson.put("join_before_host", true);
				requestJson.put("settings", settingsJson);
				logger.info("getZoomMettingIdByUserId===requestJson==>>"+requestJson.toString()+"===url==>>"+url);
				HttpEntity<String> entity = new HttpEntity<String>(requestJson.toString(), headers);
				String answer = restTemplate.postForObject(url, entity, String.class, map);
				JSONObject jsonResponse = new JSONObject(answer);
				return jsonResponse;
			} catch (Exception e) {
				logger.error("Exception occurred in the method getZoomMettingIdByUserId", e);
			}
		} catch (Exception e) {
			logger.error("Exception occurred in the method getZoomMettingIdByUserId", e);
		}
		return null;
	}

	public JSONObject retrieveUserByEmailId(String zoomJWTToken, JwtUserDetails jwtUser, boolean isCreateCalled) {
		try {
			logger.info("retrieveUserByEmailId======zoomJWTToken==>>"+zoomJWTToken+"====="+jwtUser.getUserEmailId());
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", zoomJWTToken);
			HttpEntity entity = new HttpEntity(headers);
			String getUserDetailsUrl = "https://api.zoom.us/v2/users/" + jwtUser.getUserEmailId();
			try{
				ResponseEntity<String> response = restTemplate.exchange(getUserDetailsUrl, HttpMethod.GET, entity,
				                                                        String.class);
				if (HttpStatus.OK.equals(response.getStatusCode())) {
					JSONObject jsonResponse = new JSONObject(response.getBody());
					return jsonResponse;
				} else{
					return createUser(zoomJWTToken, jwtUser);
				}
			}catch(Exception e){
				logger.error("Exception in the method retrieveUserByEmailId for {}", jwtUser.getUserEmailId(), e);
				return createUser(zoomJWTToken, jwtUser);
			}
		} catch (Exception e) {
			logger.error("Exception in the method retrieveUserByEmailId for {}", jwtUser.getUserEmailId(), e);
		}
		return null;
	}

	public JSONObject createUser(String zoomJWTToken, JwtUserDetails jwtUser) {
		logger.info("createUser======zoomJWTToken==>>"+zoomJWTToken+"==jwtUser.getUserEmailId=>>"+jwtUser.getUserEmailId());

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", zoomJWTToken);
		headers.set("Content-Type", "application/json, application/xml");
		headers.set("Accept", "application/json");
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			String createUserUrl = "https://api.zoom.us/v2/users";
			JSONObject requestJson = new JSONObject();
			requestJson.put("action", "custCreate");
			JSONObject userInfoJson = new JSONObject();
			userInfoJson.put("email", jwtUser.getUserEmailId());
			userInfoJson.put("type", 1);
			userInfoJson.put("first_name", jwtUser.getUserName());
			requestJson.put("user_info", userInfoJson);
			logger.info("===requestJson========" + requestJson.toString());
			String reqjson =  requestJson.toString();
			HttpEntity<String> entity = new HttpEntity<String>(reqjson, headers);
			ResponseEntity<String> response = restTemplate.exchange(createUserUrl, HttpMethod.POST, entity,
			                                                        String.class);
			// String answer = restTemplate.exchange(createUserUrl, entity,
			// String.class);
			if (HttpStatus.CREATED.equals(response.getStatusCode())) {
				//return retrieveUserByEmailId(zoomJWTToken, jwtUser, false);
				JSONObject repJson = new JSONObject(response.getBody());
				return repJson;
			}
		} catch (Exception e) {
			logger.error("Exception while creating user {}", jwtUser.getUserEmailId(), e);
		}
		return null;
	}

	@Autowired
	RediseService rediseService;

	public String sendVideoCallNotificationToReciverByUserId(VidoeCallSignatureModel vidoeCallSignatureModel,
	                                                         long callerUserId) {
		try {
			logger.info("==callerUserId==>"+callerUserId+"==inputjson====vidoeCallSignatureModel=="+vidoeCallSignatureModel);
			String msg = ApplicationConstants.EMPTY_STRING;
			String title = ApplicationConstants.CALLING;
			String roomName = vidoeCallSignatureModel.getMeetingNumber();
			long belongingID = callerUserId;
			UserDetails optionalBelongingUserDetails = null;
			optionalBelongingUserDetails = userDetailsService.fetchDecryptedUserDetailsById(callerUserId);
			UserDetails optionalUserDetails = userDetailsService
					.fetchDecryptedUserDetailsById(Long.parseLong(vidoeCallSignatureModel.getRecevierUserID()));
			String callType = vidoeCallSignatureModel.getCallType();
			if (optionalUserDetails != null) {
				Notification notification = new Notification();
				notification.setVidoeCallSignatureModel(vidoeCallSignatureModel);
				List<NotificationReceivedUser> notificationReceivedUserList = new ArrayList<>();
				notification.setType(StringUtils.isEmpty(callType)? ApplicationConstants.CallType.VideoCall.name() : callType);
				vidoeCallSignatureModel.setCallType(StringUtils.isEmpty(callType)? ApplicationConstants.CallType.VideoCall.name() : callType);
				//notification.setType(ApplicationConstants.CALL);
				notification.setTitle(title);
				NotificationReceivedUser notificationReceivedUser = new NotificationReceivedUser();
				notificationReceivedUser.setUserDetails(optionalUserDetails);
				notificationReceivedUser.setNotification(notification);
				notificationReceivedUserList.add(notificationReceivedUser);
				notification.setNotificationReceivedUserList(notificationReceivedUserList);
				notification.setBelongigngUserDetails(optionalBelongingUserDetails);
				if (optionalBelongingUserDetails != null  && optionalUserDetails != null) {
					msg = optionalBelongingUserDetails.getFirstName() + " "
							+ optionalBelongingUserDetails.getLastName() + " had a video call with "
							+ optionalUserDetails.getFirstName() + " " + optionalUserDetails.getLastName();
				}
				notification.setMessage(msg);
				notification.setRoomName(roomName);
				notification.setDeleteflag(false);
				notification.setTime(Date.from(Instant.now()));
				if (optionalUserDetails.getRole().getName().equalsIgnoreCase("patient")
						&& optionalBelongingUserDetails != null && !ApplicationConstants.CallType.PSTN.name().equalsIgnoreCase(vidoeCallSignatureModel.getCallType())) {
					String result = notificationService.sendMobileVidoeCallNotification(
							String.valueOf(optionalUserDetails.getId()), msg, title, roomName,
							optionalBelongingUserDetails.getFirstName() + " "
									+ optionalBelongingUserDetails.getLastName(), vidoeCallSignatureModel);
					notification.setDeleteflag(true);
					notificationService.addWebNotification(notification);
					if (ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_TOKEN_IS_EMPTY.equals(result)) {
						return ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_TOKEN_IS_EMPTY;
					}
				}
				if (optionalUserDetails.getRole().getName().equalsIgnoreCase("caregiver")
						&& optionalBelongingUserDetails != null && !ApplicationConstants.CallType.PSTN.name().equalsIgnoreCase(vidoeCallSignatureModel.getCallType())) {

					Caregiver caregiver = caregiverService.fetchCaregiverByUserId(optionalUserDetails.getId());
					//JwtUser jwtUser = rediseService.getDataInRedis(activeJWTToken, false);
					//logger.info("==caregiver latest type of device =jwtUser==="+jwtUser.getTypeOfDevice());
					//Caregiver caregiver = caregiverRepository.findByUserDetails_id(optionalUserDetails.get().getId());
					if (caregiver != null && caregiver.getUserDetails().getType() != null
							&& (caregiver.getUserDetails().getType().equalsIgnoreCase(ApplicationConstants.ANDROID)
							|| caregiver.getUserDetails().getType()
							            .equalsIgnoreCase(ApplicationConstants.IOS))
							&& optionalBelongingUserDetails != null) {
						// notificationService.sendNotification(userDetails.getEmailId(),belonginguserDetails.getEmailId(),
						// msg, title);logger
						logger.info(optionalUserDetails.getRole().getName()+"==video call notification send to mobile..type====$$$$==="+caregiver.getUserDetails().getType());
						notificationService.sendMobileVidoeCallNotification(String.valueOf(optionalUserDetails.getId()),
						                                                    msg, title, roomName, optionalBelongingUserDetails.getFirstName() + " "
								                                                    + optionalBelongingUserDetails.getLastName(), vidoeCallSignatureModel);
						notification.setDeleteflag(true);
						notificationService.addWebNotification(notification);
					} else if (caregiver != null) {
						notificationService.addWebNotification(notification);
					}
				}
				if (optionalUserDetails.getRole().getName().equalsIgnoreCase("pharmacist")
						|| (optionalUserDetails.getRole().getName().equalsIgnoreCase("provider"))) {
					notificationService.addWebNotification(notification);
				}
				return ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION;
			}
		} catch (Exception e) {
			logger.error("Exception in the method sendVideoCallNotificationToReciverByUserId", e);
		}
		return null;
	}

}
