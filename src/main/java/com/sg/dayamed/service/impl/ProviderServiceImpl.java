package com.sg.dayamed.service.impl;

import com.sg.dayamed.dao.EhCacheConstants;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserOptions;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserOptionsRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.ProviderService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.ApplicationErrors;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProviderServiceImpl implements ProviderService {

	@Autowired
	ProviderRepository providerRepository;

	@Autowired
	Utility utility;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	SendResetPwdNotification sendPwdResetNtf;

	@Autowired

	PasswordEncryption pwdEncryption;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;

	@Autowired
	UserOptionsRepository userOptionsRepository;

	@Autowired
	PharmacistRepository pharmacistRepository;

	@Autowired
	ObjectMapper userPreferenceModelMapper;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	UserPreferenceRepository userPreferenceRepository;

	@Autowired
	UserPreferenceService userPreferenceService;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	@Autowired
	PatientService patientService;

	@Autowired
	DateUtility dateUtility;

	private static final Logger logger = LoggerFactory.getLogger(ProviderServiceImpl.class);

	@Override
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public ProviderModel addProvider(ProviderModel providerModel) {
		Provider provider = utility.convertProviderModelToEntity(providerModel);
		Role role = roleRepository.findByName(ApplicationConstants.PROVIDER);
		final UserDetails providerUserDetails = provider.getUserDetails();
		providerUserDetails.setRole(role);
		boolean isNewProvider = provider.getId() == 0;
		if (isNewProvider) {
			providerUserDetails.setPwdChangeToken(pwdEncryption.encrypt(providerUserDetails.getEmailId()
					                                                            + System.currentTimeMillis()));
			providerUserDetails.setPwdTokenExpireDate(utility.addDaysToCurrentDate(pwdExpireDays));
		}
		Provider providerEntity = null;
		providerEntity = providerRepository.save(provider);
		if (isNewProvider && providerModel.isPharmacist()) {
			UserOptions userOptions = new UserOptions();
			userOptions.setGsPharmacistEnable(1);
			userOptions.setUserDetails(providerEntity.getUserDetails());
			userOptionsRepository.save(userOptions);
			Pharmacist pharmacist = new Pharmacist();
			pharmacist.setCompanyName("GS");
			pharmacist.setUserDetails(providerEntity.getUserDetails());
			pharmacistRepository.save(pharmacist);
		}
		// sending Email and SMS
		if (isNewProvider) {
			// adding userPreferences
			setProviderEmailPreference(providerEntity);
			sendPwdResetNtf.sendPwdResetNotificationWhenRegst(providerUserDetails, "Provider");
		} else {
			sendProviderUpdateNotifications(providerEntity);
		}
		return utility.convertProviderEntityToModel(providerEntity);
	}

	private void sendProviderUpdateNotifications(Provider provider) {
		final UserDetails userDetails = provider.getUserDetails();
		List<String> recipientList = new ArrayList<>();
		recipientList.add(userDetails.getEmailId());
		String userLangPreference = utility.getUserPreferedLang(userDetails.getId());
		final String messageKey = "profile.update.patient.you";
		String msgBundle = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
		                                                                                 messageKey,
		                                                                                 userLangPreference, null);
		final String subjectKey = "appconstants.profile.updated";
		String emailSubject = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null,
		                                                                                    subjectKey,
		                                                                                    userLangPreference, new Object[]{utility.getProductOwner()});
		emailAndSmsUtility.sendEmail(emailSubject, msgBundle, recipientList);
		emailAndSmsUtility.sendSms(msgBundle, userDetails.getCountryCode(), userDetails.getMobileNumber());
	}

	private void setProviderEmailPreference(Provider provider) {
		UserPreferenceModel userPreferenceModel = new UserPreferenceModel();
		userPreferenceModel.setEmailFlag(true);
		userPreferenceService.addUserPreference(provider.getUserDetails()
		                                                .getId(), userPreferenceModel);
	}

	@Override
	public List<ProviderModel> fetchProviders() {
		List<Provider> providerList = providerRepository.findAll();
		return providerList.stream()
		                   .map(this::buildProviderModel)
		                   .collect(Collectors.toList());
	}

	@Override
	public void deleteProviderById(long id) throws DataValidationException {
		List<PatientModel> patientModelList = patientService.fetchPatientsByProviderId(id);
		if (CollectionUtils.isNotEmpty(patientModelList)) {
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDER_ASSOCIATED_WITH_PATIENTS);
		}
		providerRepository.deleteProvider(id);
	}

	@Override
	public ProviderModel fetchProviderById(long providerId) throws DataValidationException {
		Optional<Provider> optionalProvider = providerRepository.findById(providerId);
		if (!optionalProvider.isPresent()) {
			logger.error(ApplicationErrors.NOT_FOUND_PROVIDER_WITH_ID);
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
		return buildProviderModel(optionalProvider.get());
	}

	@Override
	public ProviderModel fetchProviderByUserId(long userId) throws DataValidationException {
		Provider provider = providerRepository.findByUserDetails_id(userId);
		if (provider == null) {
			throw new DataValidationException(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
		}
		return buildProviderModel(provider);
	}

	@Override
	public List<ProviderModel> fetchProvidersByPatientId(long patientId) {
		return providerRepository.findByPatients_id(patientId)
		                         .stream()
		                         .map(this::buildProviderModel)
		                         .collect(Collectors.toList());
	}

	private ProviderModel buildProviderModel(Provider provider) {
		ProviderModel providerModel = utility.convertProviderEntityToModel(provider);
		providerModel.setPharmacist(isPharmacist(provider));
		return providerModel;
	}

	private boolean isPharmacist(Provider provider) {
		UserOptions userOptions = provider.getUserDetails()
		                                  .getUserOptions();
		return userOptions != null && userOptions.getGsPharmacistEnable() == 1;
	}

	@Override
	public Provider directFetchProviderById(long providerId) {
		return providerRepository.findById(providerId)
		                         .orElse(null);
	}

	@Override
	public Provider directFetchProviderByUserId(long userId) {
		return providerRepository.findByUserDetails_id(userId);
	}

	private boolean validateUserDetails(UserDetailsModel userDetails) {
		if (StringUtils.isBlank(userDetails.getFirstName()) || StringUtils.isBlank(userDetails.getLastName())
				|| StringUtils.isBlank(userDetails.getRace())
				|| StringUtils.isBlank(userDetails.getEmailId()) || StringUtils.isBlank(userDetails.getGender())
				|| StringUtils.isBlank(userDetails.getCountryCode())
				|| StringUtils.isBlank(userDetails.getMobileNumber())
				|| StringUtils.isBlank(userDetails.getAddress()
				                                  .getLocation())
				|| StringUtils.isBlank(userDetails.getAddress()
				                                  .getZipCode())
				|| userDetails.validDateOfBirth(UserRoleEnum.PROVIDER)) {
			logger.error("Missing mandatory data in input while adding provider");
			return false;
		}
		return true;
	}

	private String uploadProfileImage(@NonNull UserDetailsModel userDetailsModel, MultipartFile profileImage) {
		String oldImageName = ApplicationConstants.EMPTY_STRING;
		if (userDetailsModel.getImageUrl() != null) {
			oldImageName = utility.getLastBitFromUrl(userDetailsModel.getImageUrl());
		}
		return utility.uploadProfilepic(profileImage, userDetailsModel.getFirstName(), oldImageName);
	}

	@Override
	@CacheEvict(value = EhCacheConstants.USER_DETAILS_CACHE, allEntries = true)
	public ProviderModel validateAddProvider(ProviderModel providerModel, MultipartFile profileImage)
			throws DataValidationException {
		UserDetailsModel providerUserDetailsModel = providerModel.getUserDetails();
		if (providerUserDetailsModel == null) {
			logger.error("Provider userdetails model should not be a null while adding provider");
			throw new DataValidationException(APIErrorKeys.ERROR_INPUT_BADREQUEST);
		}
		if (!validateUserDetails(providerUserDetailsModel)) {
			logger.error("Missing mandatory data in input while adding provider");
			throw new DataValidationException(APIErrorKeys.ERROR_INPUT_BADREQUEST);
		}
		if (profileImage != null && !profileImage.isEmpty()) {
			String imageNameWitPathUrl = uploadProfileImage(providerUserDetailsModel, profileImage);
			if (imageNameWitPathUrl != null) {
				providerUserDetailsModel.setImageUrl(imageNameWitPathUrl);
			}
		}
		ProviderModel providermodel = null;
		UserDetailsModel userDetailsModel = userDetailsService
				.findByEmailIdIgnoreCase(providerUserDetailsModel.getEmailId());
		// this or condition for for both add and update
		if (userDetailsModel == null || providerModel.getId() > 0) {
			if (userDetailsModel != null) {
				providerModel.getUserDetails()
				             .setPassword(userDetailsModel.getPassword());
			}
			providermodel = addProvider(providerModel);
			return providermodel;
		} else {
			logger.error(ApplicationErrors.EMAIL_ALREADY_EXISTED);
			throw new DataValidationException(APIErrorKeys.ERROR_EMAIL_CONFLICT);
		}
	}
}
