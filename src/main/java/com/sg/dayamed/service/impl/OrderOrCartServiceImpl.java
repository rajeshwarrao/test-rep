package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Cart;
import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.repository.CartRepository;
import com.sg.dayamed.repository.ConsumerGoodsRepository;
import com.sg.dayamed.repository.MedicineRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.OrderOrCartService;
import com.sg.dayamed.util.ApplicationConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderOrCartServiceImpl implements OrderOrCartService {

	private static final Logger logger = LoggerFactory.getLogger(OrderOrCartServiceImpl.class);

	@Autowired
	CartRepository cartRepository;

	@Autowired
	MedicineRepository medicineRepository;

	@Autowired
	ConsumerGoodsRepository consumerGoodsRepository;

	public String addOrUpdateCartItems(long userId, String inputJson, JwtUserDetails jwtUser) {
		logger.info("addOrUpdateCartItems.." + inputJson);
		try {
			JSONObject reqJson = new JSONObject(inputJson);
			JSONArray medicienArry = reqJson.optJSONArray("medicienIds");
			List<Cart> listItemsInCart = new ArrayList<>();
			Cart itemCart = null;
			long createdUserId = jwtUser.getUserId();
			String createdUserRole = jwtUser.getRole();
			if (medicienArry != null) {
				for (int i = 0; i < medicienArry.length(); i++) {
					itemCart = new Cart();
					itemCart.setPatientUserId(userId);
					itemCart.setItemId(medicienArry.getLong(i));
					itemCart.setAddedDate(Date.from(Instant.now()));
					itemCart.setItemCategoryType(ApplicationConstants.CART_ITEM_CATEGORY_MEDICIEN);
					itemCart.setCreatedUserId(createdUserId);
					itemCart.setCreatedUserRole(createdUserRole);
					listItemsInCart.add(itemCart);
				}
			}
			JSONArray consumerArry = reqJson.optJSONArray("consumerIds");
			if (consumerArry != null) {
				for (int i = 0; i < consumerArry.length(); i++) {
					itemCart = new Cart();
					itemCart.setPatientUserId(userId);
					itemCart.setItemId(consumerArry.getLong(i));
					itemCart.setAddedDate(Date.from(Instant.now()));
					itemCart.setItemCategoryType(ApplicationConstants.CART_ITEM_CATEGORY_CONSUMER);
					itemCart.setCreatedUserId(createdUserId);
					itemCart.setCreatedUserRole(createdUserRole);
					listItemsInCart.add(itemCart);
				}
			}
			if (listItemsInCart.size() > 0) {
				cartRepository.saveAll(listItemsInCart);
				return ApplicationConstants.SUCCESS;
			}
		} catch (Exception e) {
			logger.error("Exception occurred in the method addOrUpdateCartItems", e);
		}
		return ApplicationConstants.FAIL;
	}

	public String deleteCartItems(long userId, long itemId, String itemCategoryType) {
		logger.info("deleteCartItems.." + userId + "===itemId==" + itemId);
		try {
			long returnValue = cartRepository.deleteByItemIdAndPatientUserIdAndItemCategoryType(itemId, userId,
			                                                                                    itemCategoryType);
			logger.info("===delete status of cart item==" + returnValue);
			return ApplicationConstants.SUCCESS;
		} catch (Exception e) {
			logger.error("Exception occurred in the method deleteCartItems", e);
		}
		return ApplicationConstants.FAIL;
	}

	public List<Object> getCartSummaryByUserId(long userId) {
		logger.info("getCartSummaryByUserId.." + userId);
		List<Object> cartItems = null;
		try {
			List<Cart> cartList = cartRepository.findByPatientUserId(userId);
			if (cartList != null && cartList.size() > 0) {
				cartItems = new ArrayList<>();
				for (Cart cart : cartList) {
					if (ApplicationConstants.CART_ITEM_CATEGORY_MEDICIEN.equalsIgnoreCase(cart.getItemCategoryType())) {
						Optional<Medicine> optMedn = medicineRepository.findById(cart.getItemId());
						if (optMedn.isPresent()) {
							Medicine medicine = optMedn.get();
							medicine.setItemCategoryType(ApplicationConstants.CART_ITEM_CATEGORY_MEDICIEN);
							cartItems.add(medicine);
						}
					} else if (ApplicationConstants.CART_ITEM_CATEGORY_CONSUMER
							.equalsIgnoreCase(cart.getItemCategoryType())) {
						Optional<ConsumerGoods> optMedn = consumerGoodsRepository.findById(cart.getItemId());
						if (optMedn.isPresent()) {
							ConsumerGoods consumerGoods = optMedn.get();
							consumerGoods.setItemCategoryType(ApplicationConstants.CART_ITEM_CATEGORY_CONSUMER);
							cartItems.add(consumerGoods);
						}
					}
				}
			}
			logger.info("getCartSummaryByUserId..cartItems list==>" + cartItems);
		} catch (Exception e) {
			logger.error("Exception occurred in the method getCartSummaryByUserId", e);
		}
		return cartItems;
	}

}
