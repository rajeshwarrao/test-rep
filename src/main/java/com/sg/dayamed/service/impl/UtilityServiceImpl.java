package com.sg.dayamed.service.impl;


import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.pojo.MultiplePrescriptionSchedulingRequestModel;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.service.UtilityService;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.Utility;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class UtilityServiceImpl implements UtilityService {

	@Autowired
	DateUtility dateUtility;

	@Autowired
	NotificationService notificationService;


	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	UserPreferenceService userPreferenceService;
	
	@Autowired
	Utility utility;

	@Override
	public boolean startMultiplePrescriptionScheduling(
			MultiplePrescriptionSchedulingRequestModel multiplePrescriptionSchedulingRequestModel) throws Exception {
		for (int i = 0; i < multiplePrescriptionSchedulingRequestModel.getPrescriptionIds()
		                                                              .size(); i++) {
			long prescriptionId = multiplePrescriptionSchedulingRequestModel.getPrescriptionIds()
			                                                                .get(i);
			Prescription prescription = prescriptionService.fetchPrescriptionEntityByPrescriptionId(prescriptionId);
			LocalDateTime startDate = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
			prescription.setStartDate(startDate);
			prescription.setEndDate(null);
			Prescription persistedPrescription = prescriptionService.addPrescription(prescription);
			if (persistedPrescription != null && persistedPrescription.isCanPatientModify()) {
				String patientPrefTime = userPreferenceService
						.fetchUserPreferenceTimeByUserId(persistedPrescription.getPatient()
						                                                      .getUserDetails()
						                                                      .getId());
				if (StringUtils.isNotBlank(patientPrefTime)) {
					utility.updateDosageInfoOfPrescription(persistedPrescription, patientPrefTime);
				}
			}
			if (!prescriptionService.generatePrescriptionSchedule(prescriptionId,
			                                                      multiplePrescriptionSchedulingRequestModel.getZoneId()) ||
					!prescriptionService.createSchedulerJobsByPrescription(prescriptionId,
					                                                       multiplePrescriptionSchedulingRequestModel.getZoneId())) {
				return false;
			}
		}
		return true;
	}

}
