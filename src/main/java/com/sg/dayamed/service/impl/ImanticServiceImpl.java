package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Adherence_DosageInfoMap;
import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.entity.DiseaseInfo;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.Glucometer;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PulseOximeter;
import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;

import com.imantics.akp.sdk.common.model.MetaData;
import com.imantics.akp.sdk.pid.model.PidDataTable;
import com.imantics.akp.sdk.pid.model.PidValue;
import com.imantics.dm.api.DmBiometricApi;
import com.imantics.dm.api.DmBiometricEventModel;
import com.imantics.dm.api.DmBiomidApi;
import com.imantics.dm.api.DmDemographicDataModel;
import com.imantics.dm.api.DmDiagnosticDataModel;
import com.imantics.dm.api.DmMedicationEventModel;
import com.imantics.dm.api.DmPatientApi;
import com.imantics.dm.api.DmPatientDataApi;
import com.imantics.dm.api.DmPatientDataModel;
import com.imantics.dm.api.DmPrescriptionDataModel;
import com.imantics.dm.common.model.DmPrescriptionEventModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

@Service
public class ImanticServiceImpl implements ImanticService {

	DmPatientDataApi dmPatientDataApi;

	DmBiomidApi dmBiomidApi;

	private static final Logger logger = LoggerFactory.getLogger(ImanticServiceImpl.class);

	@Autowired
	PatientService patientService;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";

	@Override
	public String getImanticId() {
		// TODO Auto-generated method stub
		return null;
	}

	public ImanticServiceImpl() {
		String key = "024c325b-7a29-459c-bcac-73f8fb8b8a59";
		DmPatientApi dmPatientApi = new DmPatientApi(key);

		DmBiometricApi dmBiometricApi = new DmBiometricApi(key);
		dmPatientDataApi = dmPatientApi.Initialize();
		dmBiomidApi = dmBiometricApi.Initialize();
		if (dmPatientDataApi == null || dmBiomidApi == null) {
			logger.error("Imantic server down.");
		}
	}

	Random r = new Random();

	public Long genRandomNumber() {
		long lowerLimit = 100L;
		long upperLimit = 100000L;
		long genpatientId = lowerLimit + ((long) (r.nextDouble() * (upperLimit - lowerLimit)));
		return genpatientId;
	}

	@Override
	public Map<String, String> createPatientInImantics(Patient patient) {

		//dmBiometricApi.Initialize();
		DmPatientDataModel dmPatientDataModel = null;
		Map<String, String> imanticMap = new HashMap<>();
		if (patient != null) {
			Patient existedpatient = null;
			// creating random number till that number not existing in Imantic DB temp fix till production
			do {
				long patietID = genRandomNumber();
				existedpatient = patientService.fetchPatientById(patietID);
				patient.setId(patietID);
			} while (existedpatient != null);

			if (dmPatientDataApi != null) {
				// adding patient to Imantics DB
				dmPatientDataModel = new DmPatientDataModel(Long.toString(patient.getId()), patient.getUserDetails()
				                                                                                   .getEmailId() +
						" :Any descriptive data",
				                                            "0"); // always "0" for now
				logger.info("Going to create Patient in Imantics");

				if (!dmPatientDataApi.createPatient(dmPatientDataModel)) {
					logger.error("Patient creation failed in Imantics");
					return null;
				}
				logger.info("Created Patient in Imantics");
				//persisting userdetails(DemographicData) to imantic DB
				patient.getUserDetails()
				       .setImanticUserid(dmPatientDataModel.get_id());
				DmPatientDataApi dmPatientDataApi = updateDemographicData(patient);
				imanticMap.put("imanticid", dmPatientDataModel.getPatientId());
				//imanticMap.put("predictedadherence", Double.toString(dmPatientDataApi.getPredictedAdherence()));
				// getiing this value from another api provided by Imantics.
				return imanticMap;
			} else {
				logger.error("dmPatientDataApi object not initiated.");
				return imanticMap;
			}
		}
		return imanticMap;
	}

	@Override
	public DmPatientDataApi updateDemographicData(Patient persistedpatient) {
		DmDemographicDataModel dmDemographicDataModel = new DmDemographicDataModel();

		DmPatientDataModel dmPatientDataModel = dmPatientDataApi
				.getPatientById(persistedpatient.getUserDetails()
				                                .getImanticUserid());
		if (dmPatientDataModel == null) {
			logger.error("ImanticId/UserDetails missing while updating demographic data");
			return null;
		}

		UserDetails userDetais = persistedpatient.getUserDetails();
		dmDemographicDataModel.setAge(userDetais.getAge()); // age mandatory
		dmDemographicDataModel.setGender(userDetais.getGender()); // gender
		// mandatory
		dmDemographicDataModel.setRace(userDetais.getRace()); // race mandatory
		dmDemographicDataModel.setReadmitstatus("0");
		dmDemographicDataModel.setAddress(userDetais.getAddress()
		                                            .getLocation());
		dmDemographicDataModel.setCity(userDetais.getAddress()
		                                         .getCity());
		dmDemographicDataModel.setCountrycode(userDetais.getAddress()
		                                                .getZipCode());
		dmDemographicDataModel.setState(userDetais.getAddress()
		                                          .getState());
		dmDemographicDataModel.setHospital("Hospital");
		if (dmPatientDataModel.getMetadata() == null) {
			dmPatientDataModel.setMetadata(new ArrayList<MetaData>());
		}
		dmPatientDataModel.setDemographicMData(dmDemographicDataModel);
		dmPatientDataApi.updateEntirePatient(dmPatientDataModel);
		return dmPatientDataApi;
	}

	@Override
	public String createPrescription(Prescription prescription) {
		try {
			// saving prescription details in Imantics DB
			if (prescription.getPatient()
			                .getUserDetails() != null) {
				DmPatientDataModel dmPatientDataModel = dmPatientDataApi
						.getPatientById(prescription.getPatient()
						                            .getUserDetails()
						                            .getImanticUserid());
				dmPatientDataModel.purgeDiagAndPrescriptionInfo();
				//temp code
				MetaData metaData = new MetaData();
				metaData.setKey("adherenceThreshold");
				metaData.setValue("70");
				if (dmPatientDataModel.getMetadata() != null) {
					dmPatientDataModel.getMetadata()
					                  .add(metaData);
				}
				String[] diagnoses = new String[prescription.getDiseaseInfoList()
				                                            .size()]; // array of diagnosis
				//Multiple Disese support
				int i = 0;
				for (DiseaseInfo diseaseInfo : prescription.getDiseaseInfoList()) {
					if (diseaseInfo.getDisease() != null) {
						diagnoses[i] = diseaseInfo.getDisease()
						                          .getName();
						i++;
					}
				}
				ArrayList<DmPrescriptionDataModel> dmPrescriptionDataModelList =
						new ArrayList<DmPrescriptionDataModel>();
				for (DosageInfo dosageInfo : prescription.getDosageInfoList()) {
					dmPrescriptionDataModelList
							.add(new DmPrescriptionDataModel(dosageInfo.getMedicine()
							                                           .getName(), "1"));
				}
				// add diagnosis and prescriptions to patient
				ArrayList<DmDiagnosticDataModel> dmDiagnosticDataModelList = new ArrayList<DmDiagnosticDataModel>();
				DmDiagnosticDataModel dmDiagnosticDataModel = new DmDiagnosticDataModel(diagnoses,
				                                                                        dmPrescriptionDataModelList);
				dmDiagnosticDataModelList.add(dmDiagnosticDataModel);
				dmPatientDataModel.setDiagnosticMData(dmDiagnosticDataModelList);
				// update patient with prescription data
				//Date d1 = new Date();
				logger.info("Going to create prescription in Imantics");
				dmPatientDataApi.updateEntirePatient(dmPatientDataModel);
				//Date d2 = new Date();
				logger.info("created prescription in Imantics");
				//long diff = d2.getTime() - d1.getTime();

				return ApplicationConstants.SUCCESS;
			}
			return ApplicationConstants.FAIL;
		} catch (Exception ex) {
			ex.getMessage();
			logger.error("problem in imantics while adding prescription");
			return ApplicationConstants.FAIL;
		}
	}

	@Override
	public boolean medicationTaken(AdherenceDataPoints adherence, int consumptionStatusflag, String prescribedTime) {

		DmMedicationEventModel dmMedicationEventModel = null;
		//DosageInfo dosageInfo = null;
		for (Adherence_DosageInfoMap adherence_DosageInfoMap : adherence.getAdherenceDosageInfoMapList()) {
			Map<String, String> map = new HashMap<String, String>();
			if (adherence.getLocation()
			             .size() > 0) {
				map.put("latitude", adherence.getLocation()
				                             .get(0));
				map.put("longitude", adherence.getLocation()
				                              .get(1));
			}

			map.put(ApplicationConstants.PRESCRIBEDTIME, prescribedTime);
			//Date observedTime =dateUtility.convertLocalDateTimeToDateObjectAs_it_is(adherence.getObservedTime());
			Date observedTime = (new DateUtility().convertdateStringToDateObject(
					new DateUtility().getFormattedLocalDateTime(adherence.getObservedTime()), "yyyy-MM-dd HH:mm"));
			//dosageInfo = dosageInfoRepository.findOne(adherence_DosageInfoMap.getDosageinfoId());
			Optional<DosageInfo> optionalDosageInfo =
					dosageInfoRepository.findById(adherence_DosageInfoMap.getDosageinfoId());
			if (optionalDosageInfo.isPresent()) {
				dmMedicationEventModel = new DmMedicationEventModel(
						optionalDosageInfo.get()
						                  .getPrescription()
						                  .getPatient()
						                  .getUserDetails()
						                  .getImanticUserid(), map);
				DmPrescriptionEventModel dmPrescriptionEventModel = new DmPrescriptionEventModel(
						optionalDosageInfo.get()
						                  .getMedicine()
						                  .getName(), observedTime, consumptionStatusflag);
				dmMedicationEventModel.addMedication(dmPrescriptionEventModel);
			} else {
				logger.error("medication taken event not triggered to Imantics");
				return false;
			}
		}
		dmBiomidApi.postMedicationTakenEvent(dmMedicationEventModel);
		logger.info("medication taken event triggered to Imantics");
		return true;

	}

	@Override
	public boolean biometricEvent(Object obj, LocalDateTime prescribedDateTime, LocalDateTime observedDateTime) {

		ArrayList<String> biometricNameList = new ArrayList<String>();
		ArrayList<String[]> biometricTabularValueList = new ArrayList<String[]>();
		ArrayList<Date> dateList = new ArrayList<Date>();
		Map<String, String> map = new HashMap<String, String>();
		DmBiometricEventModel dmBiometricEventModel = null;
		if (obj instanceof HeartRate) {
			try {
				HeartRate hr = (HeartRate) obj;

				if (hr.getLocation() != null && hr.getLocation()
				                                  .size() > 0) {
					map.put("latitude", hr.getLocation()
					                      .get(0));
					map.put("longitude", hr.getLocation()
					                       .get(1));
				}

				map.put(ApplicationConstants.PRESCRIBEDTIME,
				        new DateUtility().getFormattedLocalDateTime(prescribedDateTime));
				if (hr.getReadingValue() != null) {
					map.put("consumptionstatus", "taken");
					biometricNameList.add("pulse");
					biometricTabularValueList.add(new String[]{hr.getReadingValue()});
				} else {
					map.put("consumptionstatus", "No_Action pulse"); // if he doesn't take any action
				}
				Date observedTime = (new DateUtility().convertdateStringToDateObject(
						new DateUtility().getFormattedLocalDateTime(observedDateTime), "yyyy-MM-dd HH:mm"));
				dateList.add(observedTime);
				if (hr.getPatient() != null && hr.getPatient()
				                                 .getUserDetails() != null) {
					dmBiometricEventModel = new DmBiometricEventModel(hr.getPatient()
					                                                    .getUserDetails()
					                                                    .getImanticUserid(), map);
				}
			} catch (Exception e) {
				e.getLocalizedMessage();
				logger.error("Problem while adding Heart Rate in Imantics");
			}
		}
		if (obj instanceof Glucometer) {
			try {
				Glucometer gluco = (Glucometer) obj;

				if (gluco.getLocation() != null && gluco.getLocation()
				                                        .size() > 0) {
					map.put("latitude", gluco.getLocation()
					                         .get(0));
					map.put("longitude", gluco.getLocation()
					                          .get(1));
				}
				map.put(ApplicationConstants.PRESCRIBEDTIME,
				        new DateUtility().getFormattedLocalDateTime(prescribedDateTime));
				if (gluco.getReadingValue() != null) {
					biometricNameList.add("glucose");
					biometricTabularValueList.add(new String[]{gluco.getReadingValue()});
				} else {
					map.put("consumptionstatus", "No_Action glucose");// if he doesn't take any action
				}
				Date observedTime = (new DateUtility().convertdateStringToDateObject(
						new DateUtility().getFormattedLocalDateTime(observedDateTime), "yyyy-MM-dd HH:mm"));
				dateList.add(observedTime);
				if (gluco.getPatient() != null && gluco.getPatient()
				                                       .getUserDetails() != null) {
					dmBiometricEventModel = new DmBiometricEventModel(gluco.getPatient()
					                                                       .getUserDetails()
					                                                       .getImanticUserid(), map);
				}
			} catch (Exception e) {
				e.getLocalizedMessage();
				logger.error("Problem while adding BP in Imantics");
			}

		}
		//scale need to verify from imantic platform
		if (obj instanceof Scale) {
			try {
				Scale scale = (Scale) obj;
				if (scale.getLocation() != null && scale.getLocation()
				                                        .size() > 0) {
					map.put("latitude", scale.getLocation()
					                         .get(0));
					map.put("longitude", scale.getLocation()
					                          .get(1));
				}
				map.put(ApplicationConstants.PRESCRIBEDTIME,
				        new DateUtility().getFormattedLocalDateTime(prescribedDateTime));
				if (scale.getReading() != null) {
					biometricNameList.add("weight");
					biometricTabularValueList.add(new String[]{scale.getReading()});
				} else {
					map.put("consumptionstatus", "No_Action weight");// if he doesn't take any action
				}

				Date observedTime = (new DateUtility().convertdateStringToDateObject(
						new DateUtility().getFormattedLocalDateTime(observedDateTime), "yyyy-MM-dd HH:mm"));
				dateList.add(observedTime);
				if (scale.getPatient() != null && scale.getPatient()
				                                       .getUserDetails() != null) {
					dmBiometricEventModel = new DmBiometricEventModel(scale.getPatient()
					                                                       .getUserDetails()
					                                                       .getImanticUserid(), map);
				}
			} catch (Exception e) {
				e.getLocalizedMessage();
				logger.error("Problem while adding scale in Imantics");
			}
		}
		//Bp Moniter
		if (obj instanceof BpMonitor) {
			try {
				BpMonitor bpMonitor = (BpMonitor) obj;
				Date observedTime = (new DateUtility().convertdateStringToDateObject(
						new DateUtility().getFormattedLocalDateTime(observedDateTime), "yyyy-MM-dd HH:mm"));
				dateList.add(observedTime);
				if (bpMonitor.getLocation() != null && bpMonitor.getLocation()
				                                                .size() > 0) {
					map.put("latitude", bpMonitor.getLocation()
					                             .get(0));
					map.put("longitude", bpMonitor.getLocation()
					                              .get(1));
				}
				map.put(ApplicationConstants.PRESCRIBEDTIME,
				        new DateUtility().getFormattedLocalDateTime(prescribedDateTime));
				//map.put(ApplicationConstants.TAKENTIME, new DateUtility().getFormattedLocalDateTime(takenDateTime));
				if (bpMonitor.getDiastolicpressureValue() != null && bpMonitor.getSystolicpressureValue() != null) {
					biometricNameList.add("bpDia");
					biometricNameList.add("bpSys");
					biometricTabularValueList.add(new String[]{bpMonitor.getDiastolicpressureValue()
					                                                    .replace("mmHg", "").trim(),
							bpMonitor.getSystolicpressureValue()
							         .replace("mmHg", "").trim()});
				} else {
					map.put("consumptionstatus", "No_Action bpSysbpDia");// if he doesn't take any action
				}
				if (bpMonitor.getPatient() != null && bpMonitor.getPatient()
				                                               .getUserDetails() != null) {
					dmBiometricEventModel = new DmBiometricEventModel(bpMonitor.getPatient()
					                                                           .getUserDetails()
					                                                           .getImanticUserid(), map);
				}
			} catch (Exception e) {
				e.getLocalizedMessage();
				logger.error("Problem while adding BP in Imantics");
			}

		}
		//steps need to verify from imantic platform
		if (obj instanceof Steps) {
			try {
				Steps steps = (Steps) obj;
				if (steps.getLocation() != null && steps.getLocation()
				                                        .size() > 0) {
					map.put("latitude", steps.getLocation()
					                         .get(0));
					map.put("longitude", steps.getLocation()
					                          .get(1));
				}
				map.put(ApplicationConstants.PRESCRIBEDTIME,
				        new DateUtility().getFormattedLocalDateTime(prescribedDateTime));
				Date observedTime = (new DateUtility().convertdateStringToDateObject(
						new DateUtility().getFormattedLocalDateTime(observedDateTime), "yyyy-MM-dd HH:mm"));
				dateList.add(observedTime);
				if (steps.getStepCount() != 0) {
					biometricNameList.add("activity");
					biometricTabularValueList.add(new String[]{Integer.toString(steps.getStepCount())});
				} else {
					map.put("consumptionstatus", "No_Action activity");// if he doesn't take any action
				}

				//dateList.add(new Date());
				if (steps.getPatient() != null && steps.getPatient()
				                                       .getUserDetails() != null) {
					dmBiometricEventModel = new DmBiometricEventModel(steps.getPatient()
					                                                       .getUserDetails()
					                                                       .getImanticUserid(), map);
				}
			} catch (Exception e) {
				e.getLocalizedMessage();
				logger.error("Problem while adding steps in Imantics");
			}

		}
		if (obj instanceof PulseOximeter) {
			try {
			} catch (Exception e) {
				e.getLocalizedMessage();
				logger.error("Problem while adding BP in Imantics");
			}
			PulseOximeter PulseOximeter = (PulseOximeter) obj;
			Date observedTime = (new DateUtility().convertdateStringToDateObject(
					new DateUtility().getFormattedLocalDateTime(observedDateTime), "yyyy-MM-dd HH:mm"));
			biometricNameList.add("pulseOximetry");
			biometricTabularValueList.add(new String[]{PulseOximeter.getSp02()});
			dateList.add(observedTime);
			if (PulseOximeter.getPatient() != null && PulseOximeter.getPatient()
			                                                       .getUserDetails() != null) {
				dmBiometricEventModel = new DmBiometricEventModel(PulseOximeter.getPatient()
				                                                               .getUserDetails()
				                                                               .getImanticUserid());
			}
		}
		if (dmBiometricEventModel != null) {
			boolean flag =
					dmBiomidApi.postBiometricEvent(dmBiometricEventModel, biometricNameList, biometricTabularValueList,
					                               dateList);
			map.clear();
		}
		return true;
	}

	@Override
	public boolean deletePatientByImanticId(String imanticId) {
		boolean result = false;
		try {
			DmPatientDataModel dmPatientDataModel = dmPatientDataApi.getPatientById(imanticId);
			if (dmPatientDataModel != null) {
				logger.info("patient going to delete in imantic DB");
				result = dmPatientDataApi.deletePatient(imanticId);
				logger.info("patient got deleted in imantic DB");
			}

		} catch (Exception e) {
			logger.error("Error happend while deleting patient in Imantic DB " + imanticId);
			return result;
		}
		return result;
	}

	@Override
	public boolean deletePatientBypatientId(String localDbpatientId) {

		DmPatientDataModel dmPatientDataModel =
				(DmPatientDataModel) dmPatientDataApi.getPatientByName(localDbpatientId);
		boolean result = false;
		if (dmPatientDataModel != null) {
			result = deletePatientByImanticId(dmPatientDataModel.getPatientId());
		}
		return result;
	}

	//new String [] { "bpDia", "bpSys", "pulse", "weight", "pulseOximetry", "activity", "spirometer", "pt", "glucose" }
	@Override
	public List<PidDataTable> fetchBiometricData(String patientId, Date startDate, Date endDate,
	                                             ArrayList<String> biometricNameList) {
		List<PidDataTable> biometricsList =
				dmBiomidApi.RetrieveBiometricData(patientId, startDate, endDate, biometricNameList);
		return biometricsList;
	}

	//new String [] { "bpDia", "bpSys", "pulse", "weight", "pulseOximetry", "activity", "spirometer", "pt", "glucose" }
	@Override
	public List<PidDataTable> fetchPredictedBiometricData(String patientId, Date startDate, Date endDate,
	                                                      ArrayList<String> biometricNameList) {
		List<PidDataTable> predictedBiometricsList =
				dmBiomidApi.RetrievePredictedBiometricData(patientId, startDate, endDate, biometricNameList);
		return predictedBiometricsList;
	}

	//{ "projectedAdherence", "currentAdherence" }

	//aFrom now not needed this service.
	/*@Override
	public  Map<String,String> fetchAnalytics(String patientId, Date startDate, Date endDate,
			ArrayList<String> analyticNameList) {
		 List<PidDataTable> pidDataTableList = dmBiomidApi.RetrieveAnalyticData(patientId, startDate, endDate,
		 analyticNameList);
		 Map<String,String> analyticMap = new HashMap<>();
		 for (PidDataTable pidDataTable : pidDataTableList) {
				 for (PidValue pidValue : pidDataTable.getValues()) {
					 analyticMap.put(pidDataTable.getPid(), pidValue.getValue());
				 } 	
		 }
		return analyticMap;
	}*/

	@Override
	public Map<String, List<Map<String, String>>> fetchAnalyticsMap(String patientImanticId, Date startDate,
	                                                                Date endDate,
	                                                                ArrayList<String> analyticNameList) {
		Format formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");//04/01/2014
		Map<String, List<Map<String, String>>> analyticReadings = new HashMap<>();
		List<PidDataTable> pidAnalyticDataTableList =
				dmBiomidApi.RetrieveAnalyticData(patientImanticId, startDate, endDate, analyticNameList);

		for (String analyticName : analyticNameList) {
			List<Map<String, String>> analyticsValueMapList = new ArrayList<>();
			for (PidDataTable pidDataTable : pidAnalyticDataTableList) {
				if (pidDataTable.getPid()
				                .equals(analyticName)) {
					for (PidValue pidValue : pidDataTable.getValues()) {
						Map<String, String> valueMap = new LinkedHashMap<>();
						valueMap.put("x", formatter.format(
								new Date(new Timestamp(Long.parseLong(pidValue.getTimestamp())).getTime())));
						valueMap.put("y", pidValue.getValue()
						                          .replace("lbs", "")
						                          .trim());
						analyticsValueMapList.add(valueMap);
					}
					analyticReadings.put(pidDataTable.getPid(), analyticsValueMapList);
				}
			}
			Collections.sort(analyticsValueMapList, new ListMapComparatorByDate());
		}
		return analyticReadings;
	}

	class ListMapComparatorByDate implements Comparator<Map<String, String>> {

		DateFormat f = new SimpleDateFormat("MM/dd/yyyy HH:mm");

		@Override
		public int compare(Map<String, String> o1, Map<String, String> o2) {
			try {
				return f.parse(o1.get("x"))
				        .compareTo(f.parse(o2.get("x")));
			} catch (ParseException e) {
				throw new IllegalArgumentException(e);
			}
		}
	}
	
	/*@Override
	public Double getPredictedAdherenceByDemographicdataAndImanticID(Patient patient, String imanticId) {
		DmPatientDataModel dmPatientDataModel = dmPatientDataApi.getPatientById(imanticId);
		//DmPatientDataApi dmPatientDataApi = updateDemographicData(dmPatientDataModel, patient);
		return dmPatientDataApi.getPredictedAdherence();
	}*/

	//Single Api to get all analytic values predicted/projected/current
	@Override
	public Map<String, String> fetchAnalyticsMap(String patientImanticId) {
		if (dmPatientDataApi != null && dmBiomidApi != null) {
			DmPatientDataModel dmPatientDataModel = dmPatientDataApi.getPatientById(patientImanticId);
			DmDemographicDataModel dmDemographicDataModel = dmPatientDataModel.getDemographicMData();
			DmDiagnosticDataModel dmDiagnosticDataModel = dmPatientDataModel.getDiagnosticMData();

			Map<String, String> adherenceMap =
					dmBiomidApi.getAdherence(patientImanticId, dmPatientDataModel, dmDiagnosticDataModel);
			logger.info("Fetching latest analytics from Imantic DB");
			return adherenceMap;
		} else {
			logger.error("Imantic server down");
			return new HashMap<String, String>();
		}
	}

}
