package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.entity.Glucometer;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.PulseOximeter;
import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.pojo.BpMonitorModel;
import com.sg.dayamed.pojo.GlucometerModel;
import com.sg.dayamed.pojo.HeartRateModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PulseOximeterModel;
import com.sg.dayamed.pojo.ScaleModel;
import com.sg.dayamed.pojo.StepsModel;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.repository.GlucoMeterRepository;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.repository.PulseOximeterRepository;
import com.sg.dayamed.repository.ScaleRepository;
import com.sg.dayamed.repository.StepsRepository;
import com.sg.dayamed.service.BiometricService;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.util.Utility;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class BiometricServiceImpl implements BiometricService {

	@Autowired
	HeartRateRepository heartRateRepository;

	@Autowired
	GlucoMeterRepository glucoMeterRepository;

	@Autowired
	StepsRepository stepsRepository;

	@Autowired
	ScaleRepository scaleRepository;

	@Autowired
	PulseOximeterRepository pulseOximeterRepository;

	@Autowired
	BpMonitorRepository bpmonitorRepository;

	@Autowired
	ImanticService imanticService;

	@Autowired
	Utility utility;

	DateTimeFormatter dayamedGeneralFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	@Override
	public HeartRateModel addHeartRate(HeartRateModel heartratemodel) {
		HeartRate heartRate = convertHeartRateModeltoEntity(heartratemodel);
		HeartRate persistedheartRate = heartRateRepository.save(heartRate);
		//send heart rate details to Imantics Have to implement
		boolean result = imanticService.biometricEvent(heartRate,
		                                               LocalDateTime.parse(heartratemodel.getPrescribedTime(),
		                                                                   dayamedGeneralFormatter),
		                                               LocalDateTime.parse(heartratemodel.getObservedTime(),
		                                                                   dayamedGeneralFormatter));
		HeartRateModel hrModel = convertHeartRateEntityToModel(persistedheartRate);
		return hrModel;
	}

	public HeartRate convertHeartRateModeltoEntity(HeartRateModel heartratemodel) {

		HeartRate heartRate = new HeartRate();
		BeanUtils.copyProperties(heartratemodel, heartRate);
		if (heartratemodel.getPrescribedTime() != null) {
			heartRate.setPrescribedTime(
					LocalDateTime.parse(heartratemodel.getPrescribedTime(), dayamedGeneralFormatter));
		}
		if (heartratemodel.getObservedTime() != null) {
			heartRate.setObservedTime(LocalDateTime.parse(heartratemodel.getObservedTime(), dayamedGeneralFormatter));
		}
		Patient patient = utility.convertPatientModelTopatientEntity(heartratemodel.getPatient());
		heartRate.setPatient(patient);
		return heartRate;
	}

	public HeartRateModel convertHeartRateEntityToModel(HeartRate heartrate) {
		HeartRateModel hrModel = new HeartRateModel();
		BeanUtils.copyProperties(heartrate, hrModel);
		if (heartrate.getPrescribedTime() != null) {
			hrModel.setPrescribedTime(heartrate.getPrescribedTime()
			                                   .format(dayamedGeneralFormatter));
		}
		if (heartrate.getObservedTime() != null) {
			hrModel.setObservedTime(heartrate.getObservedTime()
			                                 .format(dayamedGeneralFormatter));
		}
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(heartrate.getPatient());
		hrModel.setPatient(biometricPatientResponseProcessedData(patientmodel));
		return hrModel;
	}

	public Glucometer convertGlucoModeltoEntity(GlucometerModel glucometerModel) {
		Glucometer glucometer = new Glucometer();
		BeanUtils.copyProperties(glucometerModel, glucometer);
		if (glucometerModel.getPrescribedTime() != null) {
			glucometer.setPrescribedTime(
					LocalDateTime.parse(glucometerModel.getPrescribedTime(), dayamedGeneralFormatter));
		}
		if (glucometerModel.getObservedTime() != null) {
			glucometer.setObservedTime(LocalDateTime.parse(glucometerModel.getObservedTime(),
			                                               dayamedGeneralFormatter));
		}
		Patient patient = utility.convertPatientModelTopatientEntity(glucometerModel.getPatient());
		glucometer.setPatient(patient);

		return glucometer;
	}

	public GlucometerModel convertGlucoEntityToModel(Glucometer glucometer) {
		GlucometerModel glucometerModel = new GlucometerModel();
		BeanUtils.copyProperties(glucometer, glucometerModel);
		if (glucometer.getPrescribedTime() != null) {
			glucometerModel.setPrescribedTime(glucometer.getPrescribedTime()
			                                            .format(dayamedGeneralFormatter));
		}
		if (glucometer.getObservedTime() != null) {
			glucometerModel.setObservedTime(glucometer.getObservedTime()
			                                          .format(dayamedGeneralFormatter));
		}
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(glucometer.getPatient());
		glucometerModel.setPatient(biometricPatientResponseProcessedData(patientmodel));
		return glucometerModel;
	}

	@Override
	public List<HeartRateModel> fetchHeartRatesByPatientId(long id) {

		//heartRateRepository.findByDateBetweenAndPatient_id(Date.from(Instant.now()), Utility.getMeYesterday());

		List<HeartRate> heartratelist = heartRateRepository.findByPatient_id(id);
		List<HeartRateModel> heartRateModelList = new ArrayList<>();
		heartratelist.forEach(heartrate -> {
			HeartRateModel hrModel = convertHeartRateEntityToModel(heartrate);
			heartRateModelList.add(hrModel);
		});
		return heartRateModelList;
	}

	@Override
	public PulseOximeterModel addPulseOximeter(PulseOximeterModel pulseOximeterModel) {
		PulseOximeter pulseOximeter = convertPulseOximeterModeltoEntity(pulseOximeterModel);
		PulseOximeter persistedPulseOximeter = pulseOximeterRepository.save(pulseOximeter);
		//send glucose reading details to Imantics 
		boolean result = imanticService.biometricEvent(pulseOximeter, null, null);
		PulseOximeterModel pulseOximetermodel = convertPulseOximeterEntityToModel(persistedPulseOximeter);
		return pulseOximetermodel;
	}

	public PulseOximeterModel convertPulseOximeterEntityToModel(PulseOximeter pulseOximeter) {
		PulseOximeterModel pulseOximeterModel = new PulseOximeterModel();
		BeanUtils.copyProperties(pulseOximeter, pulseOximeterModel);
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(pulseOximeter.getPatient());
		pulseOximeterModel.setPatient(patientmodel);
		return pulseOximeterModel;
	}

	public PulseOximeter convertPulseOximeterModeltoEntity(PulseOximeterModel pulseOximeterModel) {
		PulseOximeter pulseOximeter = new PulseOximeter();
		BeanUtils.copyProperties(pulseOximeterModel, pulseOximeter);
		Patient patient = utility.convertPatientModelTopatientEntity(pulseOximeterModel.getPatient());
		pulseOximeter.setPatient(patient);
		return pulseOximeter;
	}

	@Override
	public StepsModel addSteps(StepsModel stepsModel) {
		Steps steps = convertStepsModeltoEntity(stepsModel);
		Steps persistedSteps = stepsRepository.save(steps);
		//send glucose reading details to Imantics 
		boolean result = imanticService.biometricEvent(steps, LocalDateTime.parse(stepsModel.getPrescribedTime(),
		                                                                          dayamedGeneralFormatter),
		                                               LocalDateTime.parse(stepsModel.getObservedTime(),
		                                                                   dayamedGeneralFormatter));
		StepsModel stepsmodel = convertStepsEntityToModel(persistedSteps);
		return stepsmodel;
	}

	@Override
	public ScaleModel addScale(ScaleModel scaleModel) {

		Scale scale = convertScaleModeltoEntity(scaleModel);
		Scale persistedScale = scaleRepository.save(scale);
		//send scale reading details to Imantics 
		boolean result = imanticService.biometricEvent(persistedScale,
		                                               LocalDateTime.parse(scaleModel.getPrescribedTime(),
		                                                                   dayamedGeneralFormatter),
		                                               LocalDateTime.parse(scaleModel.getObservedTime(),
		                                                                   dayamedGeneralFormatter));
		ScaleModel scalemodel = convertScaleEntityToModel(persistedScale);
		return scalemodel;
	}

	@Override
	public List<ScaleModel> fetchScaleByPatientId(long patientid) {
		// TODO Auto-generated method stub
		return null;
	}

	public ScaleModel convertScaleEntityToModel(Scale scale) {
		ScaleModel scaleModel = new ScaleModel();
		BeanUtils.copyProperties(scale, scaleModel);
		if (scale.getPrescribedTime() != null) {
			scaleModel.setPrescribedTime(scale.getPrescribedTime()
			                                  .format(dayamedGeneralFormatter));
		}
		if (scale.getObservedTime() != null) {
			scaleModel.setObservedTime(scale.getObservedTime()
			                                .format(dayamedGeneralFormatter));
		}
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(scale.getPatient());
		scaleModel.setPatient(biometricPatientResponseProcessedData(patientmodel));
		return scaleModel;
	}

	public PatientModel biometricPatientResponseProcessedData(PatientModel patientmodel) {

		if (patientmodel != null && patientmodel.getPrescriptions() != null && patientmodel.getUserDetails() != null) {
			patientmodel.setPrescriptions(null);
			//patientmodel.getUserDetails().setPassword(null);

			if (patientmodel.getCaregivers() != null) {
				patientmodel.setCaregivers(null);
			}
			if (patientmodel.getProviders() != null) {
				patientmodel.setProviders(null);
			}
			if (patientmodel.getPharmacists() != null) {
				patientmodel.setPharmacists(null);
			}
		}

		return patientmodel;
	}

	public Scale convertScaleModeltoEntity(ScaleModel scaleModel) {
		Scale scale = new Scale();
		BeanUtils.copyProperties(scaleModel, scale);
		if (scaleModel.getPrescribedTime() != null) {
			scale.setPrescribedTime(LocalDateTime.parse(scaleModel.getPrescribedTime(), dayamedGeneralFormatter));
		}
		if (scaleModel.getObservedTime() != null) {
			scale.setObservedTime(LocalDateTime.parse(scaleModel.getObservedTime(), dayamedGeneralFormatter));
		}
		Patient patient = utility.convertPatientModelTopatientEntity(scaleModel.getPatient());
		scale.setPatient(patient);
		return scale;
	}

	public StepsModel convertStepsEntityToModel(Steps steps) {
		StepsModel stepsModel = new StepsModel();
		BeanUtils.copyProperties(steps, stepsModel);
		if (steps.getPrescribedTime() != null) {
			stepsModel.setPrescribedTime(steps.getPrescribedTime()
			                                  .format(dayamedGeneralFormatter));
		}
		if (steps.getObservedTime() != null) {
			stepsModel.setObservedTime(steps.getObservedTime()
			                                .format(dayamedGeneralFormatter));
		}
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(steps.getPatient());
		stepsModel.setPatient(biometricPatientResponseProcessedData(patientmodel));
		return stepsModel;
	}

	public Steps convertStepsModeltoEntity(StepsModel stepsModel) {
		Steps steps = new Steps();
		BeanUtils.copyProperties(stepsModel, steps);
		if (stepsModel.getPrescribedTime() != null) {
			steps.setPrescribedTime(LocalDateTime.parse(stepsModel.getPrescribedTime(), dayamedGeneralFormatter));
		}
		if (stepsModel.getObservedTime() != null) {
			steps.setObservedTime(LocalDateTime.parse(stepsModel.getObservedTime(), dayamedGeneralFormatter));
		}
		Patient patient = utility.convertPatientModelTopatientEntity(stepsModel.getPatient());
		steps.setPatient(patient);
		return steps;
	}

	@Override
	public GlucometerModel addGlucometer(GlucometerModel glucometerModel) {

		Glucometer glucometer = convertGlucoModeltoEntity(glucometerModel);
		Glucometer persistedGlucometer = glucoMeterRepository.save(glucometer);
		//send glucose reading details to Imantics 
		boolean result = imanticService.biometricEvent(glucometer,
		                                               LocalDateTime.parse(glucometerModel.getPrescribedTime(),
		                                                                   dayamedGeneralFormatter),
		                                               LocalDateTime.parse(glucometerModel.getObservedTime(),
		                                                                   dayamedGeneralFormatter));
		GlucometerModel glucometermodel = convertGlucoEntityToModel(persistedGlucometer);
		return glucometermodel;
	}

	@Override
	public List<GlucometerModel> fetchGlocoReadingsByPatientId(long id) {
		List<Glucometer> Glucometerlist = glucoMeterRepository.findByPatient_id(id);
		List<GlucometerModel> glucometerModelList = new ArrayList<>();
		Glucometerlist.forEach(glucoreading -> {
			GlucometerModel glucoModel = convertGlucoEntityToModel(glucoreading);
			glucometerModelList.add(glucoModel);
		});
		return glucometerModelList;
	}

	public BpMonitor convertBpMonitorModeltoEntity(BpMonitorModel bpmonitorModel) {
		BpMonitor bpMonitor = new BpMonitor();
		BeanUtils.copyProperties(bpmonitorModel, bpMonitor);
		if (bpmonitorModel.getPrescribedTime() != null) {
			bpMonitor.setPrescribedTime(
					LocalDateTime.parse(bpmonitorModel.getPrescribedTime(), dayamedGeneralFormatter));
		}
		if (bpmonitorModel.getObservedTime() != null) {
			bpMonitor.setObservedTime(LocalDateTime.parse(bpmonitorModel.getObservedTime(), dayamedGeneralFormatter));
		}
		Patient patient = utility.convertPatientModelTopatientEntity(bpmonitorModel.getPatient());
		bpMonitor.setPatient(patient);
		return bpMonitor;
	}

	public BpMonitorModel convertBpMonitorEntityToModel(BpMonitor bpmonitor) {
		BpMonitorModel bpmonitorModel = new BpMonitorModel();
		BeanUtils.copyProperties(bpmonitor, bpmonitorModel);
		if (bpmonitor.getPrescribedTime() != null) {
			bpmonitorModel.setPrescribedTime(bpmonitor.getPrescribedTime()
			                                          .format(dayamedGeneralFormatter));
		}
		if (bpmonitor.getObservedTime() != null) {
			bpmonitorModel.setObservedTime(bpmonitor.getObservedTime()
			                                        .format(dayamedGeneralFormatter));
		}
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(bpmonitor.getPatient());
		bpmonitorModel.setPatient(biometricPatientResponseProcessedData(patientmodel));
		return bpmonitorModel;
	}

	@Override
	public BpMonitorModel addBpReading(BpMonitorModel bpmonitorModel) {

		BpMonitor bpMonitor = convertBpMonitorModeltoEntity(bpmonitorModel);
		BpMonitor persistedbpMonitor = bpmonitorRepository.save(bpMonitor);
		imanticService.biometricEvent(persistedbpMonitor,
		                              LocalDateTime.parse(bpmonitorModel.getPrescribedTime(), dayamedGeneralFormatter),
		                              LocalDateTime.parse(bpmonitorModel.getObservedTime(), dayamedGeneralFormatter));
		BpMonitorModel glucometermodel = convertBpMonitorEntityToModel(persistedbpMonitor);
		return glucometermodel;
	}

	@Override
	public List<BpMonitorModel> fetchBpeadingsByPatientId(long id) {

		List<BpMonitor> bpMonitorlist = bpmonitorRepository.findByPatient_id(id);
		List<BpMonitorModel> bpMonitorModelList = new ArrayList<>();
		bpMonitorlist.forEach(bpreading -> {
			BpMonitorModel bpMonitormodel = convertBpMonitorEntityToModel(bpreading);
			bpMonitorModelList.add(bpMonitormodel);
		});
		return bpMonitorModelList;
	}

	@Override
	public List<StepsModel> fetchStepsByPatientId(long patientid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PulseOximeterModel> fetchPulseOximeterByPatientId(long patientid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GlucometerModel fetchGlucometerByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime,
	                                                                      String deviceInfoId) {
		Glucometer glucometer = glucoMeterRepository.findByPrescribedTimeAndDeviceInfoId(prescribedTime, deviceInfoId);
		if (glucometer != null) {
			return convertGlucoEntityToModel(glucometer);
		}

		return null;
	}

	@Override
	public BpMonitorModel fetchBpMonitorByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime,
	                                                                    String deviceInfoId) {
		BpMonitor bpMonitor = bpmonitorRepository.findByPrescribedTimeAndDeviceInfoId(prescribedTime, deviceInfoId);
		if (bpMonitor != null) {
			return convertBpMonitorEntityToModel(bpMonitor);
		}
		return null;
	}

	@Override
	public ScaleModel fetchScaleByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId) {
		Scale scale = scaleRepository.findByPrescribedTimeAndDeviceInfoId(prescribedTime, deviceInfoId);
		if (scale != null) {
			return convertScaleEntityToModel(scale);
		}
		return null;
	}

	@Override
	public StepsModel fetchStepsByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime, String deviceInfoId) {
		Steps steps = stepsRepository.findByPrescribedTimeAndDeviceInfoId(prescribedTime, deviceInfoId);
		if (steps != null) {
			return convertStepsEntityToModel(steps);
		}
		return null;
	}

	@Override
	public HeartRateModel fetchHeartRateByPrescribedTimeAndDeviceInfoId(LocalDateTime prescribedTime,
	                                                                    String deviceInfoId) {
		HeartRate heartRate = heartRateRepository.findByPrescribedTimeAndDeviceInfoId(prescribedTime, deviceInfoId);
		if (heartRate != null) {
			return convertHeartRateEntityToModel(heartRate);
		}
		return null;
	}

}
