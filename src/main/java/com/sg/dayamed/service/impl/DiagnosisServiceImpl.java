package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Diagnosis;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.DiagnosisModel;
import com.sg.dayamed.repository.DiagnosisRepository;
import com.sg.dayamed.service.DiagnosisService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.Utility;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DiagnosisServiceImpl implements DiagnosisService {

	@Autowired
	DiagnosisRepository diagnosisRepository;

	@Autowired
	Utility utility;

	@Override
	public List<Diagnosis> fetchDiagnosisIntelligenceByDescription(String name) throws DataValidationException {
		if (StringUtils.length(name) >= 3) {
			List<Diagnosis> diagnosisList = diagnosisRepository.findByNameIgnoreCaseContaining(name);
			return diagnosisList;
		} else {
			throw new DataValidationException(APIErrorKeys.ERRORS_INVALID_LENGTH_FOR_QUERY);
		}
	}

	@Override
	public List<DiagnosisModel> fetchDiagnosisModelIntelligenceByDescription(String name)
			throws DataValidationException {
		List<Diagnosis> diagnosisList = fetchDiagnosisIntelligenceByDescription(name);
		List<DiagnosisModel> diagnosisModelList = new ArrayList<>();
		for (Diagnosis diagnosis : diagnosisList) {
			diagnosisModelList.add(utility.convertDiagnosisEntityToModel(diagnosis));
		}
		return diagnosisModelList;
	}

	@Override
	public Diagnosis addDiagnosis(Diagnosis diagnosis) {
		return diagnosisRepository.save(diagnosis);
	}

}
