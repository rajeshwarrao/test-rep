package com.sg.dayamed.service.impl;

import com.sg.dayamed.pdfhelper.PatientPdfBody;
import com.sg.dayamed.pdfhelper.PatientPdfPageEventHelper;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.MyEntityRepositoryCustom;
import com.sg.dayamed.service.PaitentAdherenceWeeklyPDFReport;
import com.sg.dayamed.service.RediseService;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

@Slf4j
@Service
public class PaitentAdherenceWeeklyPDFReportImpl implements PaitentAdherenceWeeklyPDFReport {

	@Value("${spring.datasource.driver-class-name}")
	String driverName;

	@Value("${spring.datasource.url}")
	String datasourceUrl;

	@Value("${spring.datasource.username}")
	String dbUserName;

	@Value("${spring.datasource.password}")
	String dbPassword;
	
	String dbEncryptionCredentials = "password";

	@Autowired
	RediseService rediseService;

	@Autowired
	MyEntityRepositoryCustom myEntityRepositoryCustom;

	Random rnd = new Random();

	public Object getValueFromCacheBasedOnKey(String key) {
		return rediseService.getDataInRedisByKey(key);
	}

	public void getPatientRecoreds(String fromDate, String toDate, String emailId) {
		getPatientWeeklyReport(fromDate, toDate, emailId);
	}

	public void getPatientRecoreds() {
		getPatientWeeklyReport(null, "", null);
	}

	public void getPatientWeeklyReport(String fromDate, String toDate, String emailId) {
		try {
			//if (org.apache.commons.lang.StringUtils.isEmpty(toDate)) {
			// toDate = getDateInString(new Date());
			String dateString = getDateInString(Date.from(Instant.now()));
			//}
			String patientDetailsQuery = (String) getValueFromCacheBasedOnKey(
					RedisKeyConstants.patientDetailsQueryForWeeklyReoprt);
			//patientDetailsQuery = patientDetailsQuery.replace("$password", dbEncryptionCredentials);
			List<Map<String, String>> userDetailsResult = myEntityRepositoryCustom
					.getColmNameAndColumValueAsMap(patientDetailsQuery);
			// LinkedHashMap<String, String> userDetailsJson;
			String strLastNoDays = (String) getValueFromCacheBasedOnKey(
					RedisKeyConstants.patientNoOfDaysAdherenceDataForWeeklyReportFooter);
			if (userDetailsResult != null && userDetailsResult.size() > 0) {
				List<Map<String, String>> listUniqueUser = new ArrayList();
				Map<String, Map<String, String>> listUniqeUserMap = new HashMap();
				for (Map<String, String> userDetailsJson : userDetailsResult) {
					String patinetID = userDetailsJson.get("PatientId");
					if (listUniqeUserMap.containsKey(patinetID)) {
						String prescId = userDetailsJson.get("patientPrescrId");
						Map<String, String> userMap = listUniqeUserMap.get(patinetID);
						String prescIDs = userMap.get("patientPrescrId");
						prescIDs = prescIDs + "," + prescId;
						userMap.put("patientPrescrId", prescIDs);
					} else {
						listUniqeUserMap.put(userDetailsJson.get("PatientId"), userDetailsJson);
					}
				}
				for (Map<String, String> userDetailsJson : listUniqeUserMap.values()) {
					if (userDetailsJson != null && userDetailsJson.size() > 0) {
						String emailRecepintQuery = (String) getValueFromCacheBasedOnKey(
								RedisKeyConstants.patientDetailsQueryForRecipienEamils);
						emailRecepintQuery = emailRecepintQuery.replace("$password", dbEncryptionCredentials);
						String userId = userDetailsJson.get("PatientId");
						String preId = userDetailsJson.remove("patientPrescrId");
						Map<Integer, Object> emailReceParams = new HashMap<Integer, Object>();
						int intUserId = Integer.parseInt(userId);
						emailReceParams.put(1, intUserId);
						emailReceParams.put(2, intUserId);
						List<Map<String, String>> mailIDResult = myEntityRepositoryCustom
								.getColmNameAndColumValueAsMapWithInParams(emailRecepintQuery, emailReceParams);
						StringBuffer sbEmailRecepient = new StringBuffer();
						mailIDResult.forEach(map -> {
							map.forEach((k, v) -> {
								sbEmailRecepient.append(v)
								                .append(",");
							});
						});
						String strSbEmailRecepient = sbEmailRecepient.length() > 1
								? sbEmailRecepient.substring(0, sbEmailRecepient.length() - 1)
								: sbEmailRecepient.toString();
								strSbEmailRecepient = StringUtils.isEmpty(emailId) ? strSbEmailRecepient : emailId;
						//if (StringUtils.isEmpty(fromDate)) {
						int noOfDays = strLastNoDays != null ? Integer.parseInt(strLastNoDays) : 0;
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, -noOfDays);
						String fromString = getDateInString(cal.getTime());
						//}
						String dateHeader = "From : " + fromString + " To : " + dateString;
						userDetailsJson.put("dateHeader", dateHeader);
						String adherenceData = (String) getValueFromCacheBasedOnKey(
								RedisKeyConstants.patientAdherenceDataQueryForWeeklyReport);
						Map<Integer, Object> adherenceDataParams = new HashMap<Integer, Object>();
						adherenceDataParams.put(1, preId);
						adherenceDataParams.put(2, fromString);
						adherenceDataParams.put(3, dateString);
						List<Map<String, String>> adhrRs = myEntityRepositoryCustom
								.getColmNameAndColumValueAsMapWithInParams(adherenceData, adherenceDataParams);
						if(!CollectionUtils.isEmpty(adhrRs))
						email(adhrRs, userDetailsJson, strSbEmailRecepient);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception occurred when getting weekly report of patient", e);
		}
	}

	public String getDateInString(Date dateObj) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(dateObj);
	}

	public void email(List<Map<String, String>> adhrRs, Map<String, String> userDetailsJson, String recipient) {

		// recipient =
		// "naresh.kamisetti@senecaglobal.com,durga.vundavalli@senecaglobal.com";
		// recipient = "rajeshwarrao.kodati@senecaglobal.com";
		String encryPdfName = userDetailsJson.remove("pdfName");

		String subject = encryPdfName + "- patient weekly report";
		String emailSmtphost = (String) getValueFromCacheBasedOnKey(RedisKeyConstants.emailHost);
		String strport = (String) getValueFromCacheBasedOnKey(RedisKeyConstants.emailPort);
		int port = Integer.parseInt(strport);
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", emailSmtphost);
		props.put("mail.smtp.port", port);

		String emailSmtpUsername = (String) getValueFromCacheBasedOnKey(RedisKeyConstants.emailUsername);
		String emailSmtpPassword = (String) getValueFromCacheBasedOnKey(RedisKeyConstants.emailCredentials);
		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailSmtpUsername,
				                                  StringUtils.isEmpty(emailSmtpPassword) ? "null" : emailSmtpPassword);
			}
		});
		ByteArrayOutputStream outputStream = null;

		try {
			MimeBodyPart textBodyPart = new MimeBodyPart();
			String emailConent = (String) getValueFromCacheBasedOnKey(RedisKeyConstants.weeklyReportEmailContent);
			String pdfPassword = String.valueOf(getRandomNumber());
			if (emailConent != null) {
				emailConent = emailConent.replace("$password", pdfPassword);
			}
			textBodyPart.setText(emailConent);

			// now write the PDF content to the output stream
			outputStream = new ByteArrayOutputStream();
			writePdf(outputStream, adhrRs, userDetailsJson, pdfPassword);
			byte[] bytes = outputStream.toByteArray();

			// construct the pdf body part
			DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
			MimeBodyPart pdfBodyPart = new MimeBodyPart();
			pdfBodyPart.setDataHandler(new DataHandler(dataSource));
			pdfBodyPart.setFileName(encryPdfName + ".pdf");

			// construct the mime multi part
			MimeMultipart mimeMultipart = new MimeMultipart();
			mimeMultipart.addBodyPart(textBodyPart);
			mimeMultipart.addBodyPart(pdfBodyPart);

			String emailSender = (String) getValueFromCacheBasedOnKey(RedisKeyConstants.emailSender);
			// create the sender/recipient addresses
			InternetAddress iaSender = new InternetAddress(emailSender);
			// InternetAddress iaRecipient = new InternetAddress(recipient);

			// construct the mime message
			MimeMessage mimeMessage = new MimeMessage(session);
			mimeMessage.setSender(iaSender);
			mimeMessage.setSubject(subject);
			// mimeMessage.setRecipient(Message.RecipientType.TO, iaRecipient);
			mimeMessage.setRecipients(Message.RecipientType.TO, recipient);
			mimeMessage.setContent(mimeMultipart);

			// send off the email
			Transport.send(mimeMessage);

		} catch (Exception ex) {
			log.error("Exception occurred when emailing", ex);
		} finally {
			// clean off
			if (null != outputStream) {
				try {
					outputStream.close();
					outputStream = null;
				} catch (Exception ex) {
					log.error("Exception occurred when closing the streams when emailing", ex);
				}
			}
		}
	}

	/**
	 * Writes the content of a PDF file (using iText API) to the
	 * {@link OutputStream}.
	 *
	 * @param outputStream {@link OutputStream}.
	 * @throws Exception
	 */
	public void writePdf(OutputStream outputStream, List<Map<String, String>> adhrRs,
	                     Map<String, String> userDetailsJson, String pdfPassword) throws Exception {
		String ownerPass = getRandomNumber() + "";
		// Document document = new Document();
		Document document = new Document(PageSize.A4, 36, 36, 90, 36);
		PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);
		pdfWriter.setEncryption(pdfPassword.getBytes(), ownerPass.getBytes(), PdfWriter.ALLOW_PRINTING,
		                        PdfWriter.ENCRYPTION_AES_128);
		Rectangle rectangle = new Rectangle(50, 50, 550, 800);

		pdfWriter.setBoxSize("rectangle", rectangle);
		String pdfImagePath = (String) getValueFromCacheBasedOnKey(
				RedisKeyConstants.patientAdherenceDataForWeeklyReportImagePath);
		String footerLinks = (String) getValueFromCacheBasedOnKey(
				RedisKeyConstants.patientAdherenceDataForWeeklyReportFooterLinks);
		PatientPdfPageEventHelper headerAndFooter = new PatientPdfPageEventHelper(userDetailsJson, pdfImagePath,
		                                                                          footerLinks);
		pdfWriter.setPageEvent(headerAndFooter);
		document.open();
		BaseFont bf = BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, BaseFont.EMBEDDED);
		Font font = new Font(bf, 12);
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(60);
		float[] columnWidths = new float[]{16f, 2f, 30f};
		table.setWidths(columnWidths);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		if (userDetailsJson != null && userDetailsJson.size() > 0) {
			userDetailsJson.entrySet()
			               .forEach(entry -> {
				               if (entry.getKey() != null && !"".equalsIgnoreCase(entry.getKey()
				                                                                       .trim()) &&
						               entry.getValue() != null && !"dateHeader".equalsIgnoreCase(entry.getKey())) {
					               String strKey = entry.getKey();
					               // strKey = !StringUtils.isEmpty(strKey) ? strKey : strKey;
					               strKey = strKey.replace("_", " ");
					               PdfPCell cell = new PdfPCell(new Phrase(strKey, font));
					               cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					               cell.setBorder(Rectangle.NO_BORDER);
					               table.addCell(cell);

					               PdfPCell cell2 = new PdfPCell(
							               new Phrase(strKey == null || "".equalsIgnoreCase(strKey.trim()) ? "" :
									                          ":"));
					               cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					               cell2.setBorder(Rectangle.NO_BORDER);
					               table.addCell(cell2);
					               String entryValue = entry.getValue();
					               if (strKey != null && "Name".equalsIgnoreCase(strKey)) {
						               StringBuffer sb = new StringBuffer();
						               for (String stName : entry.getValue()
						                                         .split(" ")) {
							               if (!StringUtils.isEmpty(stName)) {
								               sb.append(stName.trim());
							               }
						               }
						               entryValue = sb.toString()
						                              .trim();
					               } else if ("Mobile_Number".equalsIgnoreCase(strKey) ||
							               "Email".equalsIgnoreCase(strKey)
							               || "ZipCode".equalsIgnoreCase(strKey) ||
							               "Country".equalsIgnoreCase(strKey)) {
						               if (!StringUtils.isEmpty(entry.getValue())) {
							               entryValue = entry.getValue();
						               }
					               } else if ("address".equalsIgnoreCase(strKey)) {
						               StringBuffer sb = new StringBuffer();
						               for (String stName : entry.getValue()
						                                         .split(",")) {
							               if (!StringUtils.isEmpty(stName)) {
								               sb.append(stName.trim());
							               }
						               }
						               entryValue = sb.length() > 1 ? sb.substring(0, sb.length() - 1)
						                                                .toString()
						                                                .trim()
								               : sb.toString();
					               }
					               PdfPCell cell3 = new PdfPCell(new Phrase(entryValue));
					               cell3.setHorizontalAlignment(
							               StringUtils.isEmpty(strKey) ? Element.ALIGN_RIGHT : Element.ALIGN_LEFT);
					               cell3.setBorder(Rectangle.NO_BORDER);
					               table.addCell(cell3);
				               }
			               });
			Paragraph para2 = new Paragraph(userDetailsJson.remove("dateHeader"), font);
			para2.setAlignment(Paragraph.ALIGN_RIGHT);
			para2.setSpacingAfter(15);
			// document.add(new Paragraph(" "));
			document.add(table);
			document.add(para2);
		}
		// document.add(new Paragraph(" "));
		PatientPdfBody pdfBody = new PatientPdfBody();
		pdfBody.patientPdfBody(adhrRs, document);
		document.add(new Paragraph(" "));
		if(!StringUtils.isEmpty(footerLinks)){
			Paragraph linkPara = new Paragraph("Useful Links: ");
			for(String link : footerLinks.split(",")){
				Phrase phrase = new Phrase();
				Chunk chunk = new Chunk(link +" \n");
				chunk.setAnchor(link);
				phrase.add(chunk);
				linkPara.add(phrase);
			}
			document.add(linkPara);
		}

		document.close();
	}

	public int getRandomNumber() {
		return 100000 + rnd.nextInt(900000);
	}
}
