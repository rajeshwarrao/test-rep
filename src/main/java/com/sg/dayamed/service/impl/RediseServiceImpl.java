package com.sg.dayamed.service.impl;

import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.security.SecurityConstants;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.ApplicationConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RediseServiceImpl implements RediseService {

	@Autowired
	private RedisTemplate<String, Object> redisUserTemplate;

	public String saveDataInRedis(String jwtTokenKey, JwtUser jwtUser) {
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		setExpireTime(jwtTokenKey, jwtUser);
		return ApplicationConstants.RESPONSE_STATUS;
	}

	public String configParamsUpdateInRedis(String keyObj, Object valueObject) {
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		value.set(keyObj, valueObject);
		return ApplicationConstants.RESPONSE_STATUS;
	}

	public Object getDataInRedisByKey(String key) {
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		return value.get(key);
	}

	/**
	 * This method is similar to the method {@link #getDataInRedisByKey}. Only difference is that this throws
	 * exception in case the key is not found.
	 *
	 * @param key
	 * @return
	 */
	@Override
	public Object getDataInRedisByKeyOrFail(String key) {
		final Object dataInRedisByKey = getDataInRedisByKey(key);
		if (dataInRedisByKey == null) {
			throw new RuntimeException(String.format("The key %s does not exist in Redis.", key));
		}
		return dataInRedisByKey;
	}

	public String saveObjectWithExpire(String keyObject, Object valueObj, int expireTime) {
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		value.set(keyObject, valueObj, expireTime, TimeUnit.MINUTES);
		return ApplicationConstants.RESPONSE_STATUS;
	}

	public String saveVedioCallRecevierNotifacation(String recevierUserId,
	                                                VidoeCallSignatureModel vidoeCallSignatureModel) {
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		long expireTime = SecurityConstants.VIDEO_CALL_RECEVIER_USER_NOTIFCATION_EXPIRATION_TIME;
		value.set(recevierUserId, vidoeCallSignatureModel, expireTime, TimeUnit.SECONDS);
		return ApplicationConstants.RESPONSE_STATUS;
	}

	public VidoeCallSignatureModel getVedioCallRecevierNotifacation(String recevierUserId) {
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		VidoeCallSignatureModel vidoeCallSignatureModel =
				(VidoeCallSignatureModel) value.get(String.valueOf(recevierUserId));
		return vidoeCallSignatureModel;
	}

	public JwtUser getDataInRedis(String jwtTokenKey, boolean updateSessionExpire) {
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		JwtUser jwtUser = (JwtUser) value.get(jwtTokenKey);
		if (jwtUser != null) {
			if (jwtUser != null && updateSessionExpire) {
				setExpireTime(jwtTokenKey, jwtUser);
			}
		}
		return jwtUser;
	}

	public void setExpireTime(String jwtTokenKey, JwtUser jwtUser) {
		long expireTime = SecurityConstants.EXPIRATION_TIME;
		ValueOperations<String, Object> value = redisUserTemplate.opsForValue();
		if (ApplicationConstants.IOS.equalsIgnoreCase(jwtUser.getTypeOfDevice())
				|| ApplicationConstants.ANDROID.equalsIgnoreCase(jwtUser.getTypeOfDevice())) {
			expireTime = SecurityConstants.MOBILE_USER_EXPIRATION_TIME;
		}
		value.set(jwtTokenKey, jwtUser, expireTime / 1000, TimeUnit.SECONDS);
		value.set(RedisKeyConstants.userLatestJWTActiveToken + jwtUser.getUserId(), jwtTokenKey, expireTime / 1000,
		          TimeUnit.SECONDS);
	}

	public boolean removeTokenFromCache(String token) {
		return redisUserTemplate.delete(token);
	}

}
