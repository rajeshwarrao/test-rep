package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.service.PrescriptionScheduleService;
import com.sg.dayamed.util.DateUtility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class PrescriptionScheduleServiceImpl implements PrescriptionScheduleService {

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	DateUtility dateUtility;

	@Override
	public boolean updatePrescriptionScheduleByDeviceInfo(LocalDateTime ldtPrescribedDatetimeInUTC, Long infoId,
	                                                      String consumptionStatus) {
		try {
			//Fetching data based on AcualdateTime and dosageID
			List<PrescriptionSchedule> PrescriptionScheduleList =
					prescriptionScheduleRepository.findByPrescribedTimeAndDeviceInfo_id(ldtPrescribedDatetimeInUTC,
					                                                                    infoId);
			for (PrescriptionSchedule prescriptionSchedule : PrescriptionScheduleList) {
				List<String> consumptionStatusList = new ArrayList<>();
				if (prescriptionSchedule.getConsumptionStatus()
				                        .size() > 0) {
					consumptionStatusList = prescriptionSchedule.getConsumptionStatus();
				}
				consumptionStatusList.add(consumptionStatus.trim());
				prescriptionSchedule.setConsumptionStatus(consumptionStatusList);
				prescriptionScheduleRepository.save(prescriptionSchedule);
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
			return false;
		}
		return true;

	}

	@Override
	public boolean updatePrescriptionScheduleByDosageInfo(LocalDateTime ldtPrescribedDatetimeInUTC, Long infoId,
	                                                      String consumptionStatus) {
		try {
			//Fetching data based on AcualdateTime and dosageID
			List<PrescriptionSchedule> PrescriptionScheduleList =
					prescriptionScheduleRepository.findByPrescribedTimeAndDosageInfo_id(ldtPrescribedDatetimeInUTC,
					                                                                    infoId);
			for (PrescriptionSchedule prescriptionSchedule : PrescriptionScheduleList) {
				List<String> consumptionStatusList = new LinkedList<>();
				if (prescriptionSchedule.getConsumptionStatus() != null && prescriptionSchedule.getConsumptionStatus()
				                                                                               .size() > 0) {
					consumptionStatusList = prescriptionSchedule.getConsumptionStatus();
				}
				consumptionStatusList.add(consumptionStatus.trim());
				prescriptionSchedule.setConsumptionStatus(consumptionStatusList);
				prescriptionScheduleRepository.save(prescriptionSchedule);
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
			return false;
		}
		return true;
	}

	/*@Override
	public List<PrescriptionSchedule> fetchJobsByUserId(Long userId) {
		List<PrescriptionSchedule> existedPrescriptionScheduleList = prescriptionScheduleRepository.findByUserId
		(userId);
		return existedPrescriptionScheduleList;
	}*/

	@Override
	public List<PrescriptionSchedule> fetchJobsByUserIdAndPrescriptionId(Long userID, Long prescriptionId) {
		// findByUserIdAndPrescriptionId
		List<PrescriptionSchedule> existedPrescriptionScheduleList =
				prescriptionScheduleRepository.findByUserIdAndPrescriptionId(userID, prescriptionId);
		return existedPrescriptionScheduleList;
	}

	@Override
	public List<PrescriptionSchedule> fetchJobsByDosageInfoIds(List<Long> dosageInfoIds) {
		List<PrescriptionSchedule> existedPrescriptionScheduleList =
				prescriptionScheduleRepository.findByDosageInfo_idIn(dosageInfoIds);
		return existedPrescriptionScheduleList;
	}
}
