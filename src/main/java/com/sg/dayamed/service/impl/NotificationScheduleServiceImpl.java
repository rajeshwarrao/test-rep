package com.sg.dayamed.service.impl;

import com.sg.dayamed.dosage.frequency.FrequencyAbstractFactory;
import com.sg.dayamed.dosage.frequency.FrequencyFactory;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.DeviceNotification;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Service
public class NotificationScheduleServiceImpl implements NotificationScheduleService {

	@Autowired
	NotificationScheduleRepository notificationScheduleRepository;

	@Autowired
	DateUtility dateUtility;
	@Autowired
	private DataSource dataSource;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(NotificationScheduleServiceImpl.class);

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void deletebulkNotificationSchdule(List<BigInteger> nsIds) {
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		namedParameterJdbcTemplate.update("DELETE FROM notification_schedule WHERE id IN (:ids)",
		                                  java.util.Collections.singletonMap("ids", nsIds));

	}
	@Autowired
	FrequencyAbstractFactory frequencyAbstractFactory;

	@Override
	public boolean addSchedulerJob(Prescription prescription, String zoneid) {
		final String DATE_Time_FORMAT = "yyyy-MM-dd HH:mm";

		if (prescription != null && StringUtils.isNotBlank(zoneid)) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_Time_FORMAT);
			String prescriptionStartDateString = dateUtility.getDateTimeBasedOnZoneId(zoneid);
			LocalDateTime localDateTime = LocalDateTime.parse(prescriptionStartDateString, formatter);
			LocalDate prescriptionStartDate = localDateTime.toLocalDate();
			Patient patient = prescription.getPatient();

			List<NotificationSchedule> existedSchedulerJobList = notificationScheduleRepository.findByUserIdAndPrescriptionId(prescription.getPatient().getUserDetails().getId(), prescription.getId());
			if(CollectionUtils.isNotEmpty(existedSchedulerJobList)){
				notificationScheduleRepository.deleteAll(existedSchedulerJobList);
			}
			if (prescription.getDosageInfoList() != null) {
				for (DosageInfo dosageInfo : prescription.getDosageInfoList()) {
					String frequencyJson = dosageInfo.getFrequency();

					try {
						if (!StringUtils.isEmpty(frequencyJson)) {
							JSONObject jsonObj = new JSONObject(frequencyJson);
							String type = jsonObj.getString("type");
							jsonObj.put("patientTimeZone", zoneid);
							String updatedfrequencyJson = jsonObj.toString();
							if (!StringUtils.isEmpty(type)) {
								FrequencyFactory frequencyFactory = frequencyAbstractFactory.getFrequencyFactory(type);
								List<String> listAlrams = new ArrayList<String>();
								if(frequencyFactory != null && dosageInfo != null && dosageInfo.getConsumptionTemplate() == null){
									listAlrams = frequencyFactory.frequencyCalculation(updatedfrequencyJson,
									                                                   dosageInfo.getTime());
								}
								else if(dosageInfo.getConsumptionTemplate().getConsumptionsKey().equalsIgnoreCase(ApplicationConstants.CUSTOM)){
									//To Do Custom
									listAlrams = frequencyFactory.frequencyCalculation(updatedfrequencyJson,
									                                                   dosageInfo.getTime());
								}else{
									ConsumptionTemplate consumptionTemplate =dosageInfo.getConsumptionTemplate();
									String[] timeArray = consumptionTemplate.getConsumptionsTimes().split(",");
									List<String> consumptionTimeList = new ArrayList<String>();
									consumptionTimeList.addAll(Arrays.asList(timeArray));
									listAlrams = frequencyFactory.frequencyCalculation(updatedfrequencyJson,
									                                                   consumptionTimeList);
								}
								// dosageInfo.setAlaramscheduleList(listAlrems);
								// llistAlarms from DB in UTC
								for (String dosageAlaram : listAlrams) {
									String durationDatewithTimeUTC = dateUtility
											.convertLocalTimeToUTCTimeStringByzoneid(
													dateUtility.getFormattedLocalDateTime(dateUtility
															                                      .convertdatetimeStringToLocalDateTime(dosageAlaram)),
													zoneid);
									// have to create prescription schdule
									// record for every alarm.
									if (dosageInfo.getDosageNotificationList() != null) {
										for (DosageNotification dosageNotification : dosageInfo
												.getDosageNotificationList()) {
											NotificationSchedule schedlerJobs = new NotificationSchedule();
											schedlerJobs.setActualDate(dateUtility
													                           .convertUTCLocalDateTimeStringToObject(durationDatewithTimeUTC));
											schedlerJobs.setDosageInfo(dosageNotification.getDosageInfo());
											schedlerJobs.setNotificationType(dosageNotification.getNotificationType());
											schedlerJobs.setOwner_userId(dosageNotification.getUserId());

											// fire time based on expected time
											// in
											// dosageNotification.
											int expectedTime = 0;
											if (dosageNotification.getExpectedTime() != null) {
												expectedTime = Integer.parseInt(dosageNotification.getExpectedTime());
											}
											LocalDateTime targetfireTime = schedlerJobs.getActualDate()
											                                           .plusMinutes(expectedTime);
											// this durationDatewithTime has to
											// convert it
											// to UTC ,dateformat :dd-M-yyyy
											// hh:mm:ss
//											String durationfireDatewithTimeUTC = dateUtility
//													.convertLocalTimeToUTCTimeStringByzoneid(
//															dateUtility.getFormattedLocalDateTime(targetfireTime),
//															zoneid);
											schedlerJobs.setFireTime(dateUtility.convertUTCLocalDateTimeStringToObject(
													dateUtility.getFormattedLocalDateTime(targetfireTime)));

											schedlerJobs.setUserId(prescription.getPatient().getUserDetails().getId());
											schedlerJobs.setPrescriptionId(prescription.getId());

											if (dosageNotification.getNotificationType().equalsIgnoreCase("sms")) {
												schedlerJobs.setNotificationValue(dosageNotification.getCountryCode()
														                                  + dosageNotification.getNotificationValue());
												notificationScheduleRepository.save(schedlerJobs);
											} else if (dosageNotification.getNotificationType()
											                             .equalsIgnoreCase("video")) {
												if (dosageNotification.getNotificationValue()
												                      .equalsIgnoreCase("caregiver")) {
													Set<Caregiver> litCargiver = patient.getCaregivers();
													for (Caregiver caregiver : litCargiver) {
														// Caregiver caregiver =
														// caregiverOptional.get();
														schedlerJobs.setNotificationValue(
																caregiver.getUserDetails().getCountryCode()
																		+ caregiver.getUserDetails().getMobileNumber());
														notificationScheduleRepository.save(schedlerJobs);
														// sendMail
														NotificationSchedule secondschedlerJob = makeschedlerJobsObject(
																schedlerJobs, caregiver.getUserDetails().getEmailId());
														notificationScheduleRepository.save(secondschedlerJob);
													}
												}
												if (dosageNotification.getNotificationValue()
												                      .equalsIgnoreCase("provider")) {
													Optional<Provider> providerOptional = patient.getProviders()
													                                             .stream().findFirst();
													if (providerOptional.isPresent()) {
														Provider provider = providerOptional.get();
														// send SMS
														schedlerJobs.setNotificationValue(
																provider.getUserDetails().getCountryCode()
																		+ provider.getUserDetails().getMobileNumber());
														notificationScheduleRepository.save(schedlerJobs);
														// sendMail 2nd job
														NotificationSchedule secondschedlerJob = makeschedlerJobsObject(
																schedlerJobs, provider.getUserDetails().getEmailId());
														notificationScheduleRepository.save(secondschedlerJob);
													}
												}
												if (dosageNotification.getNotificationValue()
												                      .equalsIgnoreCase("pharmacist")) {
													Optional<Pharmacist> pharmacistOptional = patient.getPharmacists()
													                                                 .stream().findFirst();
													if (pharmacistOptional.isPresent()) {
														Pharmacist pharmacist = pharmacistOptional.get();
														schedlerJobs.setNotificationValue(pharmacist.getUserDetails()
														                                            .getCountryCode()
																                                  + pharmacist.getUserDetails().getMobileNumber());
														notificationScheduleRepository.save(schedlerJobs);
														// sendMail
														NotificationSchedule secondschedlerJob = makeschedlerJobsObject(
																schedlerJobs, pharmacist.getUserDetails().getEmailId());
														notificationScheduleRepository.save(secondschedlerJob);
													}
												}
											} else {
												schedlerJobs.setNotificationValue(
														dosageNotification.getNotificationValue());
												notificationScheduleRepository.save(schedlerJobs);
											}

										}
									}
								}

							}
						}

					} catch (Exception e) {
						logger.error("Exception occurred when adding scheduler job", e);
					}
				}
			}

			//device not yet done
			//devicenotification jobs
			if (prescription.getDeviceInfoList() != null) {
				for (DeviceInfo deviceInfo : prescription.getDeviceInfoList()) {

					int durationLength = deviceInfo.getDuration();
					//int durationLength = 5;
					if (deviceInfo.getDuration() <= 0) {
						durationLength = 1;
					}
					DateUtility dateUtility = new DateUtility();
					for (int i = 0; i < durationLength; i++) {
						LocalDate localDurationDate = dateUtility.addDaysToLocalDate(prescriptionStartDate, i);
						for (String time : deviceInfo.getTime()) {
							/*
							 * for every time in dosageInfo need to create one
							 * row in SchedularJob table
							 */
							// insted of creating two colmns make it as one
							LocalDateTime durationDatewithTime = dateUtility.addTimestringToLacalDate(time,
							                                                                          localDurationDate);
							// this durationDatewithTime has to convert it to
							// UTC ,dateformat :dd-M-yyyy hh:mm:ss
							String durationDatewithTimeUTC = dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
									dateUtility.getFormattedLocalDateTime(durationDatewithTime), zoneid);
							//New code start
							//if trigger time lessthan utc current time
							if(i== 0){
								LocalDateTime  localDateTimeUTC = dateUtility.convertdatetimeStringToLocalDateTime(durationDatewithTimeUTC);
								LocalDateTime  currentLocalDateTimeUTC = LocalDateTime.now(ZoneOffset.UTC);

								if(localDateTimeUTC.compareTo(currentLocalDateTimeUTC) < 0){
									localDurationDate = dateUtility.addDaysToLocalDate(prescriptionStartDate, durationLength);
									durationDatewithTime = dateUtility.addTimestringToLacalDate(time,
									                                                            localDurationDate);
									durationDatewithTimeUTC = dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
											dateUtility.getFormattedLocalDateTime(durationDatewithTime), zoneid);
								}
							}
							//new code end

							if( deviceInfo.getDeviceNotificationList() != null){
								for (DeviceNotification deviceNotification : deviceInfo.getDeviceNotificationList()) {
									NotificationSchedule schedlerJobs = new NotificationSchedule();
									schedlerJobs.setActualDate(
											dateUtility.convertUTCLocalDateTimeStringToObject(durationDatewithTimeUTC));

									schedlerJobs.setDeviceInfo(deviceNotification.getDeviceInfo());
									schedlerJobs.setNotificationType(deviceNotification.getNotificationType());
									schedlerJobs.setPrescriptionId(prescription.getId());

									if(deviceNotification.getNotificationType().equalsIgnoreCase("sms")){
										schedlerJobs.setNotificationValue(deviceNotification.getCountryCode()+deviceNotification.getNotificationValue());
									}else{
										schedlerJobs.setNotificationValue(deviceNotification.getNotificationValue());
									}


									// fire time based on expected time in
									// dosageNotification.
									int expectedTime = 0;
									if(deviceNotification.getExpectedTime() != null){
										expectedTime =  Integer.parseInt(deviceNotification.getExpectedTime());
									}

									LocalDateTime targetfireTime = durationDatewithTime
											.plusMinutes(expectedTime);
									// this durationDatewithTime has to convert it
									// to UTC ,dateformat :dd-M-yyyy hh:mm:ss
									String durationfireDatewithTimeUTC = dateUtility.convertLocalTimeToUTCTimeStringByzoneid(
											dateUtility.getFormattedLocalDateTime(targetfireTime), zoneid);

									schedlerJobs.setFireTime(
											dateUtility.convertUTCLocalDateTimeStringToObject(durationfireDatewithTimeUTC));
									schedlerJobs.setUserId(prescription.getPatient().getUserDetails().getId());
									notificationScheduleRepository.save(schedlerJobs);
								}

							}//if end
						}
					}
				}
			}

			//device Notification end
		}else{
			return false;
		}
		return true;
	}


	private NotificationSchedule makeschedlerJobsObject(NotificationSchedule schedlerJobs,String notificationValue){
		NotificationSchedule notificationSchedule = new NotificationSchedule();
		notificationSchedule.setActualDate(schedlerJobs.getActualDate());

		List<String> consumptionListStatus = new ArrayList<String>();
		consumptionListStatus.addAll(schedlerJobs.getConsumptionStatus());
		notificationSchedule.setConsumptionStatus(consumptionListStatus);
		notificationSchedule.setFireTime(schedlerJobs.getFireTime());
		notificationSchedule.setUserId(schedlerJobs.getUserId());
		notificationSchedule.setDosageInfo(schedlerJobs.getDosageInfo());
		notificationSchedule.setNotificationType(schedlerJobs.getNotificationType());
		notificationSchedule.setNotificationValue(notificationValue);
		notificationSchedule.setOwner_userId(schedlerJobs.getOwner_userId());
		notificationSchedule.setPrescriptionId(schedlerJobs.getPrescriptionId());
		return notificationSchedule;
	}

	@Override
	public boolean updateSchedulerJobByDeviceInfo(LocalDateTime ldtPrescribedDatetimeInUTC,Long infoId, String consumptionStatus) {
		try{
			//Fetching data based on AcualdateTime and dosageID
			List<NotificationSchedule> SchedlerJobs = notificationScheduleRepository.findByActualDateAndDeviceInfo_id(ldtPrescribedDatetimeInUTC,infoId);
			for(NotificationSchedule schedlerJob :SchedlerJobs){
				List<String> consumptionStatusList = new ArrayList<>();
				if(schedlerJob.getConsumptionStatus() != null){
					consumptionStatusList = schedlerJob.getConsumptionStatus();
				}
				consumptionStatusList.add(consumptionStatus.trim());
				schedlerJob.setConsumptionStatus(consumptionStatusList);
				notificationScheduleRepository.save(schedlerJob);
			}
		}catch(Exception e){
			e.getLocalizedMessage();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateNotificationSchedulerJobByDosageInfo(LocalDateTime ldtPrescribedDatetimeInUTC, Long infoId,
	                                                          String consumptionStatus) {
		try{
			//Fetching data based on AcualdateTime and dosageID
			List<NotificationSchedule> SchedlerJobs = notificationScheduleRepository.findByActualDateAndDosageInfo_id(ldtPrescribedDatetimeInUTC,infoId);
			for(NotificationSchedule schedlerJob :SchedlerJobs){
				List<String> consumptionStatusList = new ArrayList<>();
				if(schedlerJob.getConsumptionStatus().size() > 0){
					consumptionStatusList = schedlerJob.getConsumptionStatus();
					consumptionStatusList.add(consumptionStatus.trim());
					schedlerJob.setConsumptionStatus(consumptionStatusList);
				}else{
					consumptionStatusList.add(consumptionStatus.trim());
					schedlerJob.setConsumptionStatus(consumptionStatusList);
				}
				notificationScheduleRepository.save(schedlerJob);
			}
		}catch(Exception e){
			e.getLocalizedMessage();
			return false;
		}
		return true;
	}

	/*@Override
	public List<NotificationSchedule> fetchJobsByUserId(Long userId) {
		List<NotificationSchedule> existedSchedulerJobList = notificationScheduleRepository.findByUserId(userId);
		return existedSchedulerJobList;
	}*/

	@Override
	public List<NotificationSchedule> fetchJobsByUserIdAndPrescriptionId(Long userId, Long prescriptionId) {
		List<NotificationSchedule> existedSchedulerJobList = notificationScheduleRepository.findByUserIdAndPrescriptionId(userId,prescriptionId);
		return existedSchedulerJobList;
	}

	@Override
	public List<NotificationSchedule> fetchJobsByDosageInfoIds(List<Long> dosageInfoIds) {
		List<NotificationSchedule> existedNotificationScheduleList = notificationScheduleRepository.findByDosageInfo_idIn(dosageInfoIds);
		return existedNotificationScheduleList;
	}

}
