package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.repository.CaregiverRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.AuthenticateService;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.Utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Service
public class AuthenticateServiceImpl implements AuthenticateService {

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ProviderRepository provoiderRepository;

	@Autowired
	PharmacistService pharmacistService;

	@Autowired
	CaregiverRepository caregiverRepository;

	@Autowired
	Utility utility;

	@Autowired
	PrescriptionService prescriptionService;

	private void setCookie(String name, String value, HttpServletResponse response) {
		Cookie cookie = new Cookie(name, value);
		cookie.setHttpOnly(true);
		cookie.setMaxAge(60 * 60 * 2);//2 hour
		response.addCookie(cookie);
	}

	@Override
	public UserDetailsModel login(String username, String password, String typeofdivice, HttpServletRequest request,
	                              HttpServletResponse response) {
		UserDetails userDetails = null;
		UserDetailsModel userDetailsModel = null;

		//setCookie("username",username,response);
		HttpSession session = request.getSession();
		session.setAttribute("username", username);

		userDetails = userDetailsRepository.findByEmailIdIgnoreCase(username);

		if (userDetails != null && password.equalsIgnoreCase(userDetails.getPassword())) {
			Role role = userDetails.getRole();
			userDetailsModel = utility.convertUserDetailsEntityToModel(userDetails);
			setCookie("userid", Long.toString(userDetailsModel.getId()), response);
			if (role != null && role.getName()
			                        .equalsIgnoreCase("provider")) {
				Provider provider = provoiderRepository.findByUserDetails_id(userDetails.getId());
				ProviderModel providerModel = utility.convertProviderEntityToModel(provider);
				//userDetailsModel.setProvider(providerModel);
				setCookie("role", "provider", response);
				setCookie("providerid", Long.toString(providerModel.getId()), response);
			}
			if (role != null && role.getName()
			                        .equalsIgnoreCase("pharmacist")) {
				try{
				PharmacistModel pharmacistModel = pharmacistService.fetchPharmacistByUserId(userDetails.getId());
				userDetailsModel.setPharmasist(pharmacistModel);
				setCookie("role", "pharmacist", response);
				setCookie("pharmacistid", Long.toString(pharmacistModel.getId()), response);
				}catch(Exception e){
					e.getMessage();
				}
			}
			if (role != null && role.getName()
			                        .equalsIgnoreCase("caregiver")) {
				Caregiver caregiver;
				try {
					caregiver = caregiverService.fetchCaregiverByUserId(userDetails.getId());
				UserDetails nonEncryptCaregiverUserDetails = caregiver.getUserDetails();
				//caregiver.setTypeofDevice(typeofdivice);
				caregiver = caregiverRepository.save(caregiver);
				caregiver.setUserDetails(nonEncryptCaregiverUserDetails);
				CaregiverModel caregiverModel = utility.convertCaregiverEntityToModel(caregiver);
				userDetailsModel.setCaregiver(caregiverModel);
				setCookie("role", "caregiver", response);
				setCookie("caregiverid", Long.toString(caregiverModel.getId()), response);
				} catch (DataValidationException e) {
					try {
						throw new DataValidationException(ApplicationConstants.CAREGIVER, APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB);
					} catch (DataValidationException e1) {
					}
				}
			}
			if (role != null && role.getName()
			                        .equalsIgnoreCase("patient")) {
				Patient patient = patientRepository.findByUserDetails_id(userDetails.getId());
				PatientModel patModel = utility.convertPatientEntityTopatientModel(patient);
				userDetailsModel.setPatient(patModel);
				setCookie("role", "patient", response);
				setCookie("patientid", Long.toString(patModel.getId()), response);
			}
			if (role != null && role.getName()
			                        .equalsIgnoreCase("admin")) {
				setCookie("role", "admin", response);
			}
		}
		return userDetailsModel;
	}

	@Override
	public UserDetailsModel login(String emailid) {
		UserDetails userDetails = userDetailsRepository.findByEmailIdIgnoreCase(emailid);
		UserDetailsModel userDetailsModel = null;
		if (userDetails != null) {
			userDetailsModel = utility.convertUserDetailsEntityToModel(userDetails);
		}
		return userDetailsModel;
	}

}
