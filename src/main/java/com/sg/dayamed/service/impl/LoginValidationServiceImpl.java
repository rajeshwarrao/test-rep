package com.sg.dayamed.service.impl;

import com.sg.dayamed.adapter.users.UserInfoLoadingProcessor;
import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserOptions;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.security.jwtsecurity.security.JwtGenerator;
import com.sg.dayamed.service.LoginValidationService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class LoginValidationServiceImpl implements LoginValidationService {

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	RediseService rediseService;

	@Autowired
	UserInfoLoadingProcessor userInfoLoadingProcessor;

	@Override
	public UserDetailsWithJwtModel loginValidation(LoginRequestModel loginRequestModel,
	                                               HttpServletRequest request, JwtGenerator jwtGenerator)
			throws Exception {
		UserDetails userDetails = null;
		try {
			String userName = loginRequestModel.getUserName();
			String dbEncrptionEnable =
					(String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
				userDetails = userDetailsRepository.findByEmailIdIgnoreCase(userName);
			if (userDetails != null && loginRequestModel.getPassword()
			                                            .equalsIgnoreCase(userDetails.getPassword())) {
				UserOptions userOptions = userDetails.getUserOptions();
				userDetails.setUserOptions(userOptions);
				if (StringUtils.isEmpty(loginRequestModel.getTypeOfDevice())) {
					loginRequestModel.setTypeOfDevice(request.getHeader("typeofdevice"));
				}
				UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(userDetails.getRole()
				                                                                    .getName());
				return userInfoLoadingProcessor.getUserDetailsWithModel(userRoleEnum, loginRequestModel, jwtGenerator,
				                                                        userDetails);
			} else {
				throw new DataValidationException("incorrect", IWSGlobalApiErrorKeys.ERRORS_AUTHENTICATION_FAILED);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
