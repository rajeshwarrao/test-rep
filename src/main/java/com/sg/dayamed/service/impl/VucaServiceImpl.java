package com.sg.dayamed.service.impl;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.pojo.VucaTokenModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.VucaService;
import com.sg.dayamed.service.util.APIErrorKeys;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.springframework.http.HttpStatus.OK;

@Service
public class VucaServiceImpl implements VucaService {

	@Autowired
	private RediseService rediseService;

	private static final Logger logger = LoggerFactory.getLogger(VucaServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	private String fetchVucaToken() throws ApplicationException {
		String vucaToken = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.vucavediotoken);
		if (StringUtils.isNotBlank(vucaToken)) {
			return vucaToken;
		}

		String partnerKey = (String) rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucavediopartnerkey);
		String strBaseUrl = (String) rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_BASE_URL);
		String url = strBaseUrl + partnerKey;
		ResponseEntity<VucaTokenModel> response = restTemplate.getForEntity(url, VucaTokenModel.class);
		final VucaTokenModel vucaTokenModel = response.getBody();
		if (!OK.equals(response.getStatusCode())
				|| vucaTokenModel == null
				|| StringUtils.isBlank(vucaTokenModel.getBaseUrl())) {
			final String errorMessage = getErrorMessage(vucaTokenModel);
			logger.error("Unable to get VUCA token. {}", errorMessage);
			throw new ApplicationException(null, null, APIErrorKeys.ERROR_UNABLE_TO_GET_VUCA_TOKEN,
			                               new String[]{errorMessage});
		} else {
			//"BaseUrl": "https://widget.medsoncue.com/default.aspx?t=6aa99752-147f-4d55-9041-d25c72787f74"
			vucaToken = vucaTokenModel.getBaseUrl()
			                          .split("t=")[1];
			int expiryTimeInMinutes = findTokenExpiryTimeInMinutes(vucaTokenModel.getExpirationDateTime_UTC());
			rediseService.saveObjectWithExpire(RedisKeyConstants.vucavediotoken, vucaToken, expiryTimeInMinutes);
			return vucaToken;
		}
	}

	private String getErrorMessage(VucaTokenModel vucaTokenModel) {
		return vucaTokenModel != null ? vucaTokenModel.getStatusMessage() : "";
	}

	private int findTokenExpiryTimeInMinutes(String expirationDateTimeInUTCAsString) {
		final LocalDateTime expirationDateTimeInUTC = LocalDateTime.parse(expirationDateTimeInUTCAsString);
		final LocalDateTime currentDateTimeInUTC = LocalDateTime.now(ZoneId.of("UTC"));
		return (int) Duration.between(currentDateTimeInUTC, expirationDateTimeInUTC)
		                     .toMinutes();
	}

	@Override
	public String fetchVucaVideoURL(String ndc) throws ApplicationException {
		String formattedNdc = ndc.replace("-", "");
		if (formattedNdc.length() > 9) {
			formattedNdc = formattedNdc.substring(0, 9);
		}
		String vucapharmacyID = (String) rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID);
		String strWidgetUrl =
				(String) rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL);
		return buildVideoURL(formattedNdc, fetchVucaToken(), vucapharmacyID, strWidgetUrl);
	}

	private String buildVideoURL(String formattedNdc, String vucaToken, String vucapharmacyID, String strWidgetUrl) {
		return strWidgetUrl + vucaToken + "&c=" + vucapharmacyID + "&idtype=1&drugid=" + formattedNdc + "&l=en";
	}
}
