package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.repository.StepsRepository;
import com.sg.dayamed.service.StepsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class StepsServiceImpl implements StepsService {

	@Autowired
	StepsRepository stepsRepository;

	@Override
	public List<Steps> findByPrescribedTime(LocalDateTime actualDate) {
		return stepsRepository.findByPrescribedTime(actualDate);
	}

}
