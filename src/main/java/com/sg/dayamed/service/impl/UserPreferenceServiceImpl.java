package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class UserPreferenceServiceImpl implements UserPreferenceService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	UserPreferenceRepository userPreferenceRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	Utility utility;

	@Override
	public String fetchUserPreferenceTimeByUserId(Long userid) {

		UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userid);
		if (userPreference != null && userPreference.getUserPrefSetting() != null) {
			try {
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				UserPreferenceModel model =
						objectMapper.readValue(userPreference.getUserPrefSetting(), UserPreferenceModel.class);
				return model != null ? model.getFirstConsumption() : null;
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}

	@Override
	public UserPreference addUserPreference(Long userId, UserPreferenceModel userPreferenceModel) {

		UserDetails userDetails = userDetailsService.fetchUserDetailEntitiysById(userId);
		UserPreference existedUserPreference = userPreferenceRepository.findByUserDetailsId(userId);
		try {
			if (userDetails != null && existedUserPreference == null) {
				UserPreferenceModel UserPreferenceModel = new UserPreferenceModel();
				UserPreference userPreference = new UserPreference();
				String userSettingInJson = objectMapper.writeValueAsString(UserPreferenceModel);
				userPreference.setUserDetails(userDetails);

				userPreference.setUserPrefSetting(userSettingInJson);
				return userPreferenceRepository.save(userPreference);
			} else if (userDetails != null && existedUserPreference != null && userPreferenceModel != null) {
				// updating userPref
				existedUserPreference.setUserDetails(userDetails);
				return userPreferenceRepository.save(existedUserPreference);
			}
		} catch (JsonProcessingException e) {
			logger.error("json parsing exception while adding userpref of patient");
		} catch (Exception e) {
			logger.error("Excepton while executing addUserPreference", e);
		}
		return null;
	}

	@Override
	public UserPreferenceModel findUserSettingsByUserId(Long userId) throws IOException {
		final UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userId);
		if (userPreference == null) {
			return null;
		}
		return objectMapper.readValue(userPreference.getUserPrefSetting(), UserPreferenceModel.class);
	}

	@Override
	public UserPreferenceModel updateUserPreference(Long userId, UserPreferenceModel settingsUpdateRequest)
			throws Exception {
		UserPreferenceModel savedSettings = new UserPreferenceModel();
		UserPreference userPreference = userPreferenceRepository.findByUserDetailsId(userId);
		if (userPreference != null) {
			String settingsAsString = userPreference.getUserPrefSetting();
			savedSettings = objectMapper.readValue(settingsAsString, UserPreferenceModel.class);
		} else {
			UserDetails userDetails = userDetailsService.fetchDecryptedUserDetailsById(userId);
			userPreference = new UserPreference();
			userPreference.setUserDetails(userDetails);
		}
		final long countOfCopiedProperties = copyNonnullProperties(settingsUpdateRequest, savedSettings);
		if (countOfCopiedProperties == 0) {
			return savedSettings;
		}
		userPreference.setUserPrefSetting(objectMapper.writeValueAsString(savedSettings));
		userPreferenceRepository.save(userPreference);

		updatePrescriptionDosageInfo(userId, settingsUpdateRequest.getFirstConsumption());
		return savedSettings;
	}

	private void updatePrescriptionDosageInfo(Long userId, String firstConsumption) {
		if (StringUtils.isBlank(firstConsumption) || firstConsumption.equalsIgnoreCase("null")) {
			return;
		}
		utility.updateDosageInfoOfprescriptions(userId, firstConsumption);
	}

	private long copyNonnullProperties(UserPreferenceModel source, UserPreferenceModel target)
			throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		AtomicInteger modifiedPropertiesCount = new AtomicInteger(0);
		PropertyUtils.describe(source)
		             .entrySet()
		             .stream()
		             .filter(propertyEntry -> isPropertyCopiable(source, propertyEntry))
		             .forEach(propertyEntry -> {
			             try {
				             PropertyUtils.setProperty(target, propertyEntry.getKey(), propertyEntry.getValue());
				             modifiedPropertiesCount.getAndIncrement();
			             } catch (Exception copyException) {
				             logger.warn("Exception when copying user preference", copyException);
			             }
		             });
		return modifiedPropertiesCount.get();
	}

	private boolean isPropertyCopiable(UserPreferenceModel object, Map.Entry<String, Object> propertyEntry) {
		return propertyEntry.getValue() != null &&
				!propertyEntry.getKey()
				              .equals("class") &&
				PropertyUtils.isWriteable(object, propertyEntry.getKey());
	}

}
