package com.sg.dayamed.service.impl;

import com.sg.dayamed.dosage.frequency.FrequencyAbstractFactory;
import com.sg.dayamed.dosage.frequency.FrequencyFactory;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.pojo.AdherenceReportModel;
import com.sg.dayamed.pojo.AdherenceReportResModel;
import com.sg.dayamed.pojo.DosageInfoModel;
import com.sg.dayamed.pojo.FristConsumptionTypeModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.repository.AdherenceRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.service.AdherenceReportService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.DosageTimeOfTheDay;

import io.jsonwebtoken.lang.Collections;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Service
public class AdherenceReportServiceImpl implements AdherenceReportService {

	private static final Logger logger = LoggerFactory.getLogger(AdherenceReportServiceImpl.class);

	@Autowired
	PatientService patientService;

	@Autowired
	Utility utility;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	AdherenceRepository adherenceRepository;

	@Autowired
	FrequencyAbstractFactory frequencyAbstractFactory;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");

	DateTimeFormatter timeFormatter_24Hour = DateTimeFormatter.ofPattern("hh:mm a");

	public AdherenceReportResModel getAdherenceReport(List<Long> providerIds, String recipientEmail) {
		Instant start = Instant.now();
		AdherenceReportResModel adherenceReportResModel = new AdherenceReportResModel();
		adherenceReportResModel.setStatus(ApplicationConstants.FAIL);
		try {
			List<AdherenceReportModel> adherenceReportModelList = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(providerIds)) {
				for (long provideID : providerIds) {
					List<Patient> patientlist = provideID != 0
							? patientService.fetchEncryptPatientsByProviderId(provideID)
							: patientService.fetchAllPatients();
					if (Collections.isEmpty(patientlist)) {
						adherenceReportResModel.setMessage("Patient list is empty for provider " + provideID);
					} else {
						adherenceReportModelList.addAll(getAdherenceReportModelList(patientlist));
					}
				}
			} else {
				List<Patient> patientlist = patientService.fetchAllPatients();
				adherenceReportModelList = getAdherenceReportModelList(patientlist);
			}

			if (!Collections.isEmpty(adherenceReportModelList)) {
				adherenceReportResModel.setStatus(ApplicationConstants.SUCCESS);
				adherenceReportResModel.setMessage(ApplicationConstants.SUCCESS);
				adherenceReportResModel.setAdherenceReportHeaders(getAdherenceReportHeaders());
				adherenceReportResModel.setAdherenceReportModelList(adherenceReportModelList);
			} else {
				adherenceReportResModel.setMessage("Patients do not have Prescriptions");
			}
		} catch (Exception e) {
			logger.error("Exception occured in getAdherenceReport service impl.." + e.getMessage());
		}
		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish)
		                           .toMillis();  //in millis
		logger.info("Time taken to generate report in milli seconds --> " + timeElapsed);
		return adherenceReportResModel;
	}

	public Map<String, String> getAdherenceReportHeaders() {
		Map<String, String> adherenceReportHeadersMap = new LinkedHashMap<>();
		adherenceReportHeadersMap.put("providerName", "Provider Name");
		adherenceReportHeadersMap.put("patientName", "Patient Name");
		adherenceReportHeadersMap.put("patientEmailAddress", "Patient Email Address");
		adherenceReportHeadersMap.put("patientId", "Patient ID");
		adherenceReportHeadersMap.put("prescriptionId", "Prescription ID");
		adherenceReportHeadersMap.put("observedTime", "Observed Time");
		adherenceReportHeadersMap.put("eventTime", "Event Time");
		adherenceReportHeadersMap.put("adherenceAction", "Adherence Action");
		adherenceReportHeadersMap.put("adherenceActionType", "Adherence ActionType");
		adherenceReportHeadersMap.put("medicationName", "Medication Name");
		adherenceReportHeadersMap.put("medicationQuantity", "Medication Quantity");
		adherenceReportHeadersMap.put("dosageTimeOfTheDay", "Dosage Time Of Day");
		adherenceReportHeadersMap.put("zone", "Time Zone");
		adherenceReportHeadersMap.put("dosageOfDay", "Dosage Of Day");
		return adherenceReportHeadersMap;
	}

	public List<AdherenceReportModel> getAdherenceReportModelList(List<Patient> patientlist) {
		List<AdherenceReportModel> adherenceReportModelList = new ArrayList<>();
		try {
			if (!Collections.isEmpty(patientlist)) {
				TimeZone timeZone = TimeZone.getDefault();
				for (Patient patient : patientlist) {
					PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
					List<ProviderModel> listProvider = new ArrayList(patientModel.getProviders());
					ProviderModel provider = !Collections.isEmpty(listProvider) ? listProvider.get(0) : null;
					List<Prescription> presList = prescriptionRepository.findByPatient_id(patientModel.getId());
					for (Prescription prescription : presList) {
						for (DosageInfo dosageInfo : prescription.getDosageInfoList()) {
							if (!dosageInfo.isDeleteflag()) {
								DosageInfoModel dosageInfoModel = utility.convertDosageinfoEntityToModel(dosageInfo);
								List<String> times = getTimes(dosageInfoModel);
								getAlarmListOfDosageInfo(dosageInfoModel);
								List<LocalDateTime> localDates = dosageInfoModel.getAlaramscheduleList()
								                                                .stream()
								                                                .map(date -> dateUtility.convertLocalTimeToUTCWithZoneId(
										                                                date.trim(), timeZone.getID()))
								                                                .distinct()
								                                                .filter(date -> !date.isAfter(
										                                                LocalDateTime.now()))
								                                                .collect(Collectors.toList());
								List<AdherenceDataPoints> adherenceDataPointsList =
										adherenceRepository.findByPrescriptionIDAndAdherenceDosageInfoMapList_DosageinfoId(
												prescription.getId(), dosageInfo.getId());
								for (LocalDateTime localDateTime : localDates) {
									List<AdherenceDataPoints> dataPoints = adherenceDataPointsList.stream()
									                                                              .filter(data -> !localDateTime.isBefore(
											                                                              data.getPrescribedTime()
											                                                                  .plusDays(
													                                                                  -1)))
									                                                              .
											                                                              filter(data -> !localDateTime.isAfter(
													                                                              data.getPrescribedTime()
													                                                                  .plusDays(
															                                                                  1)))
									                                                              .collect(
											                                                              Collectors.toList());
									List<AdherenceReportModel> reportResModels = new ArrayList<>();
									AdherenceDataPoints adherenceDataPoints = null;
									String zone = null;
									if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(dataPoints)) {
										List<Long> minutesList = dataPoints.stream()
										                                   .map(data -> Duration.between(
												                                   data.getPrescribedTime(),
												                                   localDateTime)
										                                                        .toMinutes())
										                                   .collect(Collectors.toList());
										for (Integer index = 0; index < minutesList.size(); index++) {
											String zoneDetails = getTimeZone(minutesList.get(index));
											if (StringUtils.isNotBlank(zoneDetails)) {
												zone = zoneDetails;
												adherenceDataPoints = dataPoints.get(index);
												break;
											}
										}
										AdherenceReportModel adherenceReportModel =
												createAdherenceReportModel(patientModel, provider, prescription,
												                           dosageInfo, localDateTime,
												                           adherenceDataPoints, zone, times);
										reportResModels.add(adherenceReportModel);
									} else {
										AdherenceReportModel adherenceReportModel =
												createAdherenceReportModel(patientModel, provider, prescription,
												                           dosageInfo, localDateTime, null, null,
												                           times);
										reportResModels.add(adherenceReportModel);
									}
									adherenceReportModelList.addAll(reportResModels);
								}
							}
						}
					}
				}
			}
			return adherenceReportModelList;
		} catch (Exception e) {
			logger.error("Exception occurred in getAdherenceReportModelList service impl.." + e.getMessage());
		}
		return adherenceReportModelList;
	}

	private String getDosageTimeOfTheDay(List<String> times, LocalDateTime dateTime) {
		for (Integer index = 0; index < times.size(); index++) {
			if (times.get(index)
			         .equalsIgnoreCase(timeFormatter_24Hour.format(dateTime))) {
				DosageTimeOfTheDay dosageTimeOfTheDay = DosageTimeOfTheDay.getTimeOfTheDay(index + 1);
				return dosageTimeOfTheDay.getValue();
			}
		}
		return null;
	}

	private AdherenceReportModel createAdherenceReportModel(PatientModel patientModel, ProviderModel provider,
	                                                        Prescription prescription, DosageInfo dosageInfo,
	                                                        LocalDateTime localDateTime,
	                                                        AdherenceDataPoints adherenceDataPoints, String zone,
	                                                        List<String> times) {
		AdherenceReportModel adherenceReportModel = new AdherenceReportModel();
		adherenceReportModel.setPrescriptionId(prescription.getId());
		adherenceReportModel.setPatientEmailAddress(patientModel.getUserDetails()
		                                                        .getEmailId());
		adherenceReportModel.setPatientName(patientModel.getUserDetails()
		                                                .getFirstName() + " " + patientModel.getUserDetails()
		                                                                                    .getLastName());
		adherenceReportModel.setProviderName(provider != null ? provider.getUserDetails()
		                                                                .getFirstName() + " " +
				provider.getUserDetails()
				        .getLastName() : "");
		adherenceReportModel.setPatientId(patientModel.getId());
		adherenceReportModel.setMedicationName(dosageInfo.getMedicine()
		                                                 .getName());
		adherenceReportModel.setMedicationQuantity("1");
		if (adherenceDataPoints != null) {
			String eventTime = dateUtility.convertUTCDateToLocalDateByZoneID(adherenceDataPoints.getPrescribedTime()
			                                                                                    .format(formatter),
			                                                                 zone, "MM/dd/yyyy HH:mm");
			adherenceReportModel.setEventTime(eventTime);
			String observedTimeStr =
					dateUtility.convertUTCDateToLocalDateByZoneID(adherenceDataPoints.getObservedTime()
					                                                                 .format(formatter),
					                                              zone, "MM/dd/yyyy HH:mm");
			adherenceReportModel.setObservedTime(observedTimeStr);
			adherenceReportModel.setAdherenceAction(adherenceDataPoints.getConsumptionStatus()
			                                                           .stream()
			                                                           .distinct()
			                                                           .map(value -> StringUtils.capitalize(value))
			                                                           .collect(Collectors.joining(",")));
			adherenceReportModel.setAdherenceActionType(adherenceDataPoints.getHow() != null ?
					                                            adherenceDataPoints.getHow()
					                                                               .stream()
					                                                               .collect(Collectors.joining(",")) :
					                                            null);
		} else {
			adherenceReportModel.setEventTime(localDateTime.format(dateTimeFormatter));
			adherenceReportModel.setAdherenceAction("No contact method");
		}
		adherenceReportModel.setDosageTimeOfTheDay(timeFormatter_24Hour.format(localDateTime));
		adherenceReportModel.setZone(StringUtils.isNotBlank(zone) ? zone : "UTC");
		adherenceReportModel.setDosageOfDay(getDosageTimeOfTheDay(times, localDateTime));
		return adherenceReportModel;
	}

	public void getAlarmListOfDosageInfo(DosageInfoModel dosageInfoModel) {
		try {
			String frequencyJson = dosageInfoModel.getFrequency();
			if (StringUtils.isNotBlank(frequencyJson)) {
				JSONObject jsonObj = new JSONObject(frequencyJson);
				String type = jsonObj.getString("type");

				if (StringUtils.isNotBlank(type)) {
					TimeZone timeZone = TimeZone.getDefault();
					jsonObj.put("patientTimeZone", timeZone.getID());
					jsonObj.put("pastDatesEnable", true);
					FrequencyFactory frequencyFactory = frequencyAbstractFactory.getFrequencyFactory(type);
					if (frequencyFactory != null) {
						if (dosageInfoModel.getFristConsumptionType() == null) {
							List<String> listAlarms = frequencyFactory.frequencyCalculation(jsonObj.toString(),
							                                                                dosageInfoModel.getTime());
							dosageInfoModel.setAlaramscheduleList(listAlarms);
						} else {
							FristConsumptionTypeModel firstConsumptionType = dosageInfoModel.getFristConsumptionType();
							if (firstConsumptionType != null) {
								if (ApplicationConstants.CUSTOM.equals(firstConsumptionType.getFirstConsumptionTime()
								                                                           .getName())) {
									List<String> listAlarms = frequencyFactory.frequencyCalculation(jsonObj.toString(),
									                                                                dosageInfoModel.getTime());
									dosageInfoModel.setAlaramscheduleList(listAlarms);
								} else {
									String[] intakeTimeArray = firstConsumptionType.getFirstConsumptionTime()
									                                               .getTimes()
									                                               .split(",");
									List<String> firstConsumptionIntakeTimeList = Arrays.stream(intakeTimeArray)
									                                                    .filter(value -> org.apache.commons.lang.StringUtils.isNotBlank(
											                                                    value))
									                                                    .map(value -> value.trim())
									                                                    .collect(Collectors.toList());
									List<String> listAlarms = frequencyFactory.frequencyCalculation(jsonObj.toString(),
									                                                                firstConsumptionIntakeTimeList);
									dosageInfoModel.setAlaramscheduleList(listAlarms);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception occured in getAlarmListOfDosageinfo service impl.." + e.getMessage());
		}
	}

	private List<String> getTimes(DosageInfoModel dosageInfoModel) {
		FristConsumptionTypeModel consumptionTimeModel = dosageInfoModel.getFristConsumptionType();
		List<String> times;
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm a", Locale.ENGLISH);
		if (consumptionTimeModel == null) {
			times = dosageInfoModel.getTime();
		} else {
			if (ApplicationConstants.CUSTOM.equalsIgnoreCase(consumptionTimeModel.getFirstConsumptionTime()
			                                                                     .getName())) {
				times = dosageInfoModel.getTime();
			} else {
				String[] intakeTimeArray = consumptionTimeModel.getFirstConsumptionTime()
				                                               .getTimes()
				                                               .split(",");
				times = Arrays.stream(intakeTimeArray)
				              .filter(value -> org.apache.commons.lang.StringUtils.isNotBlank(value))
				              .map(value -> value.trim())
				              .collect(Collectors.toList());
				times = times.stream()
				             .map(time -> modifyTime(time))
				             .collect(Collectors.toList());
			}
		}
		java.util.Collections.sort(times, Comparator.comparing((String s) -> LocalTime.parse(s.toUpperCase(), dtf)));
		return times;
	}

	private String modifyTime(String time) {
		if (StringUtils.substringBefore(time, ":")
		               .length() == 1) {
			return "0" + time;
		}
		return time;
	}

	private String getTimeZone(Long minutes) {
		if (new Long("-300").equals(minutes)) {
			return "US/Central";
		} else if (new Long("330").equals(minutes)) {
			return "Asia/Kolkata";
		} else if (new Long("-240").equals(minutes)) {
			return "America/New_York";
		}
		return null;
	}

}
