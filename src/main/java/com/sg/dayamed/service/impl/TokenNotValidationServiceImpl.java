package com.sg.dayamed.service.impl;

import com.sg.dayamed.controller.ws.ImanticNotificationReqModel;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.GSVUCAToken;
import com.sg.dayamed.entity.ImanticNotification;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingObjectReqModel;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingReqModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.ImanticNotificationRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.commons.VUCAURestResponse;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.TokenNotValidationService;
import com.sg.dayamed.service.VucaService;
import com.sg.dayamed.service.WeeklyEmailNotificationToPatient;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DataForImantics;
import com.sg.dayamed.util.EmailAndSmsUtility;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
public class TokenNotValidationServiceImpl implements TokenNotValidationService {

	private static final Logger logger = LoggerFactory.getLogger(TokenNotValidationServiceImpl.class);

	@Autowired
	NotificationService notificationService;

	@Autowired
	RediseService rediseService;

	@Autowired
	VucaService vucaService;

	@Autowired
	WeeklyEmailNotificationToPatient weeklyEmailNotificationToPatient;

	ReentrantLock lock = new ReentrantLock();

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Value("${ndcimages.folder.path}")
	String ndcImagesFolderPath;

	@Value("${profilepics.folder.path}")
	String profilePicsFolderPath;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	PatientService patientService;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	ImanticNotificationRepository imanticNotificationRepository;

	@Override
	public Integer endMeeting(ZoomEndMeetingReqModel zoomEndMeetingReqModel, String zoomNotificationTokenFromApi)
			throws Exception {
		try {
			logger.debug("== zoomEndMeetingReqModel=====" + zoomEndMeetingReqModel);
			String zoomNotificationToken = (String) rediseService
					.getDataInRedisByKey(RedisKeyConstants.ZOOM_NOTIFICATION_TOKEN);
			if (zoomNotificationTokenFromApi != null
					&& zoomNotificationTokenFromApi.equalsIgnoreCase(zoomNotificationToken)
					&& zoomEndMeetingReqModel != null && zoomEndMeetingReqModel.getPayload() != null
					&& zoomEndMeetingReqModel.getPayload()
					                         .getObject() != null) {
				ZoomEndMeetingObjectReqModel zoomEndMeetingObjectReqModel = zoomEndMeetingReqModel.getPayload()
				                                                                                  .getObject();
				String meetingNumber = zoomEndMeetingObjectReqModel.getId();
				Notification notification = notificationService.getNotificationByRoomName(meetingNumber);
				if (notification != null) {
					Date meetingStartTime = notification.getTime();
					Date meetingEndTime = new Date();
					DateTime dt1 = new DateTime(meetingStartTime);
					DateTime dt2 = new DateTime(meetingEndTime);
					long minutes = Minutes.minutesBetween(dt1, dt2)
					                      .getMinutes();
					notification.setMeetingDuration(minutes);
					notificationService.saveNotification(notification);
				}
			}
		} catch (Exception e) {
			logger.error("Exception occured while saveing notification.." + e.getMessage());
			throw new ApplicationException(e);
		}
		return HttpStatus.OK.value();
	}

	@Override
	public RestResponse getVUCAUrlbyGSToken(String gsToken) throws Exception {
		VUCAURestResponse vucaRestResponse = new VUCAURestResponse();
		vucaRestResponse.setStatus(ApplicationConstants.FAIL);
		try {
			GSVUCAToken gsVUCAToken = weeklyEmailNotificationToPatient.getNdcCodeByGSToken(gsToken);
			if (gsVUCAToken != null) {
				String vucaVideo = vucaService.fetchVucaVideoURL(gsVUCAToken.getNdcCode());
				if (!StringUtils.isEmpty(vucaVideo)) {
					vucaRestResponse.setStatus(ApplicationConstants.SUCCESS);
					vucaRestResponse.setVucaurl(vucaVideo);
					return vucaRestResponse;
				}
				vucaRestResponse.setMessage(ApplicationConstants.VUCA_URL_GETTING_NULL);
			} else {
				vucaRestResponse.setMessage(ApplicationConstants.INVALID_VUCA_TOKEN);
			}
			return vucaRestResponse;
		} catch (Exception e) {
			logger.error("Exception occurred when getVUCAUrlbyGSToken", e);
			return vucaRestResponse;
		}
	}

	public RestResponse webErrorLogsSave(String inputJson) throws Exception {
		String folderPath = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.WEB_ERRORLOG_FILEPATH);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
		String strFileSuffix = sdf.format(new Date());
		String fileName = strFileSuffix + "_errorlogs.txt";
		logger.debug("Error json {}", inputJson);
		VUCAURestResponse vucaRestResponse = new VUCAURestResponse();
		lock.lock();
		try (FileWriter errorFileWriter = new FileWriter(folderPath + "/" + fileName, true)) {
			vucaRestResponse.setStatus("Error while logs save");
			errorFileWriter.write(inputJson + "\n");
			vucaRestResponse.setStatus("Logs saved successfully");
		} catch (Exception e) {
			logger.error("Exception when saving error log", e);
			throw new ApplicationException(e);
		} finally {
			lock.unlock();
		}
		return vucaRestResponse;
	}

	public String fetchNDCImages(String imgname, HttpServletResponse response) throws Exception {
		if (StringUtils.isEmpty(imgname)) {
			logger.error("NDC image name must not be empty or null ");
			throw new DataValidationException("incorrect", APIErrorKeys.ERROR_INVALID_DATA);
		}
		try (FileInputStream fis = new FileInputStream(ndcImagesFolderPath + imgname)) {
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			StreamUtils.copy(fis, response.getOutputStream());
			return ApplicationConstants.SUCCESS;
		} catch (Exception e) {
			logger.error("NDC Image path not found: " + ndcImagesFolderPath + imgname, e);
			throw new ApplicationException(e);
		}
	}

	public String getDeviceImageOrmedicineImageOrProfileImagetestOrImage(String imgname, HttpServletResponse response)
			throws Exception {
		try (FileInputStream fis = new FileInputStream(profilePicsFolderPath + imgname)) {
			if (imgname == null || "".equals(imgname)) {
				logger.error("image name must not be empty or null");
				throw new DataValidationException("incorrect", APIErrorKeys.ERROR_INVALID_DATA);
			}
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			StreamUtils.copy(fis, response.getOutputStream());
			return ApplicationConstants.SUCCESS;
		} catch (Exception e) {
			logger.error("Device Image path not found");
			throw new ApplicationException(e);
		}
	}

	public List<DataForImantics> fetchPatientAttributes(Optional<String> patientId) throws Exception {
		try {
			List<DataForImantics> dataForImanticsLIst = new ArrayList<>();
			DataForImantics dataForImantics = new DataForImantics();
			List<Prescription> prescriptionList = new ArrayList<>();
			if (patientId.isPresent()) {
				prescriptionList = prescriptionRepository.findByPatient_id(Long.parseLong(patientId.get()));
			} else {
				prescriptionList = prescriptionRepository.findAll();
			}
			if (prescriptionList.size() <= 0) {
				throw new DataValidationException("No Records Found",
				                                  IWSGlobalApiErrorKeys.ERRORS_PUBLIC_RECORD_NOT_FOUND);
			}
			for (Prescription prescription : prescriptionList) {
				dataForImantics = getImanticDeviceInfoMap(prescription);
				dataForImanticsLIst.add(dataForImantics);
			}
			return dataForImanticsLIst;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private DataForImantics getImanticDeviceInfoMap(Prescription prescription) {
		if (prescription != null) {
			DataForImantics dataForImantics = new DataForImantics();
			dataForImantics.setPrescriptionId(prescription.getId());
			dataForImantics.setPatientId(prescription.getPatient()
			                                         .getId());
			dataForImantics.setImanticId(prescription.getPatient()
			                                         .getUserDetails()
			                                         .getImanticUserid());
			dataForImantics.setPatientName(prescription.getPatient()
			                                           .getUserDetails()
			                                           .getFirstName() + " "
					                               + prescription.getPatient()
					                                             .getUserDetails()
					                                             .getLastName());
			Map<Long, String> dosageInfoMap = new HashMap<>();
			dosageInfoMap = prescription.getDosageInfoList()
			                            .stream()
			                            .filter(dosageInfo -> !dosageInfo.isDeleteflag())
			                            .collect(Collectors.toMap(dosageInfo -> dosageInfo.getId(),
			                                                      dosageInfo -> dosageInfo.getMedicine()
			                                                                              .getName()));
			dataForImantics.setDosageInfoIds(dosageInfoMap);
			Map<Long, String> deviceInfoMap = new HashMap<>();
			deviceInfoMap = prescription.getDeviceInfoList()
			                            .stream()
			                            .filter(deviceInfo -> !deviceInfo.isDeleteflag())
			                            .collect(Collectors.toMap(deviceInfo -> deviceInfo.getId(),
			                                                      deviceInfo -> deviceInfo.getDevice()
			                                                                              .getName()));
			dataForImantics.setDeviceInfoIds(deviceInfoMap);
			return dataForImantics;
		}
		return null;
	}

	public String addImanticNotification(ImanticNotificationReqModel notificationModel) throws Exception {
		ImanticNotification imanticNotification = new ImanticNotification();
		try {
			UserDetails belonginguserDetails = userDetailsRepository
					.findByImanticUserid(notificationModel.getImanticId());
			ImanticNotification persistedImanticnotification = null;
			Notification persistedNotification = null;
			if (belonginguserDetails != null) {
				Patient patient = patientService.fetchPatientByUserId(belonginguserDetails.getId());
				List<Long> userDetailsIdList = new ArrayList<Long>();
				List<String> recipientMobileNumberList = new ArrayList<String>();
				List<String> alertTypeList = notificationModel.getAlertType();
				String subject = "Observation alert";
				String bodyMsg = notificationModel.getMessage();
				List<String> recipientEmailList = new ArrayList<String>();
				List<String> toListRole = notificationModel.getToList();
				if (patient != null) {
					if (toListRole.contains("patient")) {
						if (patient.getUserDetails() != null) {
							userDetailsIdList.add(patient.getUserDetails()
							                             .getId());
							recipientEmailList.add(patient.getUserDetails()
							                              .getEmailId());
							recipientMobileNumberList.add(patient.getUserDetails()
							                                     .getCountryCode()
									                              + patient.getUserDetails()
									                                       .getMobileNumber());
						}
					}
					if (toListRole.contains("provider")) {
						if (patient.getProviders()
						           .size() > 0) {
							for (Provider provider : patient.getProviders()) {
								userDetailsIdList.add(provider.getUserDetails()
								                              .getId());
								recipientEmailList.add(provider.getUserDetails()
								                               .getEmailId());
								recipientMobileNumberList.add(provider.getUserDetails()
								                                      .getCountryCode()
										                              + provider.getUserDetails()
										                                        .getMobileNumber());
							}
						}
					}
					if (toListRole.contains("caregiver")) {
						if (patient.getCaregivers()
						           .size() > 0) {
							for (Caregiver caregiver : patient.getCaregivers()) {
								userDetailsIdList.add(caregiver.getUserDetails()
								                               .getId());
								recipientEmailList.add(caregiver.getUserDetails()
								                                .getEmailId());
								recipientMobileNumberList.add(caregiver.getUserDetails()
								                                       .getCountryCode()
										                              + caregiver.getUserDetails()
										                                         .getMobileNumber());
							}
						}
					}
					if (toListRole.contains("pharmacist")) {
						if (patient.getPharmacists()
						           .size() > 0) {
							for (Pharmacist pharmacist : patient.getPharmacists()) {
								userDetailsIdList.add(pharmacist.getUserDetails()
								                                .getId());
								recipientEmailList.add(pharmacist.getUserDetails()
								                                 .getEmailId());
								recipientMobileNumberList.add(pharmacist.getUserDetails()
								                                        .getCountryCode()
										                              + pharmacist.getUserDetails()
										                                          .getMobileNumber());
							}
						}
					}
				}
				imanticNotification.setBelongingUserDetails(belonginguserDetails);
				imanticNotification.setMessage(notificationModel.getMessage());
				imanticNotification.setDate(Date.from(Instant.now()));
				imanticNotification.setToUserList(userDetailsIdList);
				persistedImanticnotification = imanticNotificationRepository.save(imanticNotification);
				Notification notification = new Notification();
				notification.setMessage(notificationModel.getMessage());
				notification.setType("alert");
				notification.setTitle("Observation alert");
				notification.setBelongigngUserDetails(belonginguserDetails);
				notification.setDeleteflag(false);
				notification.setTime(Date.from(Instant.now()));
				persistedNotification = notificationService.addWebNotification(notification);
				if (persistedNotification == null) {
					logger.error("probem while adding imantic alert into notification table");
				}
				if (alertTypeList.contains("email")) {
					emailAndSmsUtility.sendEmail(subject, bodyMsg, recipientEmailList);
				}
				if (alertTypeList.contains("sms")) {
					for (String mobileNumber : recipientMobileNumberList) {
						emailAndSmsUtility.sendSms(bodyMsg, mobileNumber);
					}
				}
			} else {
				logger.error("user not found with provided userid(imanticID)");
				throw new DataValidationException("UsernotFound", APIErrorKeys.ERROR_INVALID_DATA);
			}
			if (persistedImanticnotification != null && persistedNotification != null) {
				return ApplicationConstants.SUCCESS;
			} else {
				throw new DataValidationException("UsernotFound", APIErrorKeys.ERROR_INVALID_DATA);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}

	}

}
