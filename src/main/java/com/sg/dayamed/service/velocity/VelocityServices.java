package com.sg.dayamed.service.velocity;

import java.util.List;

import com.sg.dayamed.adapter.notifications.vo.DosageDetailsVO;

public interface VelocityServices {

	void sendCustomEmailForDosages(List<DosageDetailsVO> dosageDetailsVOS, String emailId);
}
