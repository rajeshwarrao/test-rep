package com.sg.dayamed.service.velocity.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sg.dayamed.adapter.notifications.VelocityEngine;
import com.sg.dayamed.adapter.notifications.vo.DosageDetailsVO;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.velocity.VelocityServices;
import com.sg.dayamed.util.EmailAndSmsUtility;

@Service
public class VelocityServicesImpl implements VelocityServices {

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	VelocityEngine velocityEngine;

	@Autowired
	RediseService rediseService;


	@Autowired
	com.sg.dayamed.adapter.notifications.VelocityMessageFormatter velocityMessageFormatter;

	public void sendCustomEmailForDosages(List<DosageDetailsVO> dosageDetailsVOS, String emailId) {
		if (dosageDetailsVOS != null && dosageDetailsVOS.size() > 0) {
			String bodyTemplate = velocityMessageFormatter.formatCustomDosageNotification(dosageDetailsVOS);
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(emailId);
			emailAndSmsUtility.sendEmail("Medicine Status Notification", bodyTemplate, recipientList, true);
		}
	}
}
