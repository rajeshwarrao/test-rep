package com.sg.dayamed.service;

import com.sg.dayamed.entity.BpMonitor;

import java.time.LocalDateTime;
import java.util.List;

public interface BpMonitorService {

	public List<BpMonitor> findByPrescribedTime(LocalDateTime prescription);
}
