package com.sg.dayamed.service;


import com.sg.dayamed.pojo.MultiplePrescriptionSchedulingRequestModel;

public interface UtilityService {

	boolean startMultiplePrescriptionScheduling(
			MultiplePrescriptionSchedulingRequestModel multiplePrescriptionSchedulingRequestModel) throws Exception;

}
