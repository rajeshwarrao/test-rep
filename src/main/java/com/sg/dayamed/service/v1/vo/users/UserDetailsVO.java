package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;
import com.sg.dayamed.entity.view.PatientPharmacistAssociationsV;
import com.sg.dayamed.entity.view.PatientProviderAssociationsV;
import com.sg.dayamed.rest.controller.ws.users.WSAddress;
import com.sg.dayamed.rest.controller.ws.users.WSRole;
import com.sg.dayamed.rest.controller.ws.users.WSUserDetails;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserDetailsVO {

	private Long id;

	private Integer age;

	private String appVersion;

	private String city;

	private String country;

	private String countryCode;

	private LocalDateTime userCreatedOn;

	private String userDeviceType;

	private String emailId;

	private Boolean emailFlag;

	private String firstName;

	private String gender;

	private String userImageUrl;

	private String imanticUserId;

	private LocalDateTime lastActiveTime;

	private LocalDateTime lastLoginTime;

	private String lastName;

	private String location;

	private String middleName;

	private String mobileNumber;

	private String osVersion;

	private String pwdChangeToken;

	private LocalDateTime pwdTokenExpireDate;

	private String race;

	private Boolean smsFlag;

	private String state;

	private String token;

	private LocalDateTime userUpdatedOn;

	private String userZoneId;

	private String voipToken;

	private String zipCode;

	private String deviceId;

	private Long roleId;

	private String role;

	private Boolean pharmacistEnable;

	public UserDetailsVO(UserPreferenceVO preferenceVO) {
		this.age = preferenceVO.getAge();
		this.appVersion = preferenceVO.getAppVersion();
		this.city = preferenceVO.getCity();
		this.country = preferenceVO.getCountry();
		this.countryCode = preferenceVO.getCountryCode();
		this.deviceId = preferenceVO.getUserDeviceType();
		this.emailFlag = preferenceVO.getEmailFlag();
		this.firstName = preferenceVO.getFirstName();
		this.emailId = preferenceVO.getEmailId();
		this.gender = preferenceVO.getGender();
		this.id = preferenceVO.getUserId();
		this.imanticUserId = preferenceVO.getImanticUserId();
		this.lastActiveTime = preferenceVO.getLastActiveTime();
		this.lastLoginTime = preferenceVO.getLastLoginTime();
		this.lastName = preferenceVO.getLastName();
		this.location = preferenceVO.getLocation();
		this.middleName = preferenceVO.getMiddleName();
		this.mobileNumber = preferenceVO.getMobileNumber();
		this.osVersion = preferenceVO.getOsVersion();
		this.pwdChangeToken = preferenceVO.getPwdChangeToken();
		this.pwdTokenExpireDate = preferenceVO.getPwdTokenExpireDate();
		this.race = preferenceVO.getRace();
		this.smsFlag = preferenceVO.getSmsFlag();
		this.state = preferenceVO.getState();
		this.token = preferenceVO.getToken();
		this.userCreatedOn = preferenceVO.getUserCreatedOn();
		this.userDeviceType = preferenceVO.getUserDeviceType();
		this.userImageUrl = preferenceVO.getUserImageUrl();
		this.userUpdatedOn = preferenceVO.getUserUpdatedOn();
		this.userZoneId = preferenceVO.getUserZoneId();
		this.voipToken = preferenceVO.getVoipToken();
		this.zipCode = preferenceVO.getZipCode();
		this.role = preferenceVO.getRole();
		this.roleId = preferenceVO.getRoleId();
		this.pharmacistEnable = preferenceVO.getPharmacistEnable();
	}

	public WSUserDetails toWsUserDetails() {
		WSUserDetails userDetails = new WSUserDetails();
		userDetails.setAge(this.age != null ? String.valueOf(this.age) : null);
		userDetails.setAppVersion(this.appVersion);
		userDetails.setCountryCode(this.countryCode);
		userDetails.setDeviceId(this.deviceId);
		userDetails.setEmailFlag(this.emailFlag != null ? this.emailFlag : false);
		userDetails.setEmailId(this.emailId);
		userDetails.setFirstName(this.firstName);
		userDetails.setGender(this.gender);
		userDetails.setId(this.id);
		userDetails.setImanticUserId(this.imanticUserId);
		userDetails.setLastActiveTime(this.lastActiveTime);
		userDetails.setLastLoginTime(this.lastLoginTime);
		userDetails.setLastName(this.lastName);
		userDetails.setMiddleName(this.middleName);
		userDetails.setMobileNumber(this.mobileNumber);
		userDetails.setOsVersion(this.osVersion);
		userDetails.setPwdChangeToken(this.pwdChangeToken);
		userDetails.setPwdTokenExpireDate(this.pwdTokenExpireDate);
		userDetails.setRace(this.race);
		userDetails.setSmsFlag(this.smsFlag != null ? this.smsFlag : false);
		userDetails.setToken(this.token);
		userDetails.setCreatedOn(this.userCreatedOn);
		userDetails.setDeviceType(this.userDeviceType);
		userDetails.setImageUrl(StringUtils.isBlank(this.userImageUrl) ? StringUtils.EMPTY : this.userImageUrl);
		userDetails.setUpdatedOn(this.userUpdatedOn);
		userDetails.setZoneId(this.userZoneId);
		userDetails.setVoipToken(this.voipToken);
		if (this.roleId != null) {
			userDetails.setRole(new WSRole(this.roleId, this.role));
		}
		userDetails.setAddress(new WSAddress(this.location, this.city, this.state, this.country, this.zipCode));
		return userDetails;
	}

	public UserDetailsVO(PatientProviderAssociationsV associationsV) {
		this.age = associationsV.getAge();
		this.appVersion = associationsV.getAppVersion();
		this.city = associationsV.getCity();
		this.country = associationsV.getCountry();
		this.countryCode = associationsV.getCountryCode();
		this.deviceId = associationsV.getUserDeviceType();
		this.emailFlag = associationsV.getEmailFlag();
		this.firstName = associationsV.getFirstName();
		this.emailId = associationsV.getEmailId();
		this.gender = associationsV.getGender();
		this.id = associationsV.getUserId();
		this.imanticUserId = associationsV.getImanticUserId();
		this.lastActiveTime = associationsV.getLastActiveTime();
		this.lastLoginTime = associationsV.getLastLoginTime();
		this.lastName = associationsV.getLastName();
		this.location = associationsV.getLocation();
		this.middleName = associationsV.getMiddleName();
		this.mobileNumber = associationsV.getMobileNumber();
		this.osVersion = associationsV.getOsVersion();
		this.pwdChangeToken = associationsV.getPwdChangeToken();
		this.pwdTokenExpireDate = associationsV.getPwdTokenExpireDate();
		this.race = associationsV.getRace();
		this.smsFlag = associationsV.getSmsFlag();
		this.state = associationsV.getState();
		this.token = associationsV.getToken();
		this.userCreatedOn = associationsV.getUserCreatedOn();
		this.userDeviceType = associationsV.getUserDeviceType();
		this.userImageUrl = associationsV.getUserImageUrl();
		this.userUpdatedOn = associationsV.getUserUpdatedOn();
		this.userZoneId = associationsV.getUserZoneId();
		this.voipToken = associationsV.getVoipToken();
		this.zipCode = associationsV.getZipCode();
	}

	public UserDetailsVO(PatientCaregiverAssociationsV associationsV) {
		this.age = associationsV.getAge();
		this.appVersion = associationsV.getAppVersion();
		this.city = associationsV.getCity();
		this.country = associationsV.getCountry();
		this.countryCode = associationsV.getCountryCode();
		this.deviceId = associationsV.getUserDeviceType();
		this.emailFlag = associationsV.getEmailFlag();
		this.firstName = associationsV.getFirstName();
		this.emailId = associationsV.getEmailId();
		this.gender = associationsV.getGender();
		this.id = associationsV.getUserId();
		this.imanticUserId = associationsV.getImanticUserId();
		this.lastActiveTime = associationsV.getLastActiveTime();
		this.lastLoginTime = associationsV.getLastLoginTime();
		this.lastName = associationsV.getLastName();
		this.location = associationsV.getLocation();
		this.middleName = associationsV.getMiddleName();
		this.mobileNumber = associationsV.getMobileNumber();
		this.osVersion = associationsV.getOsVersion();
		this.pwdChangeToken = associationsV.getPwdChangeToken();
		this.pwdTokenExpireDate = associationsV.getPwdTokenExpireDate();
		this.race = associationsV.getRace();
		this.smsFlag = associationsV.getSmsFlag();
		this.state = associationsV.getState();
		this.token = associationsV.getToken();
		this.userCreatedOn = associationsV.getUserCreatedOn();
		this.userDeviceType = associationsV.getUserDeviceType();
		this.userImageUrl = associationsV.getUserImageUrl();
		this.userUpdatedOn = associationsV.getUserUpdatedOn();
		this.userZoneId = associationsV.getUserZoneId();
		this.voipToken = associationsV.getVoipToken();
		this.zipCode = associationsV.getZipCode();
	}

	public UserDetailsVO(PatientPharmacistAssociationsV associationsV) {
		this.age = associationsV.getAge();
		this.appVersion = associationsV.getAppVersion();
		this.city = associationsV.getCity();
		this.country = associationsV.getCountry();
		this.countryCode = associationsV.getCountryCode();
		this.deviceId = associationsV.getUserDeviceType();
		this.emailFlag = associationsV.getEmailFlag();
		this.firstName = associationsV.getFirstName();
		this.emailId = associationsV.getEmailId();
		this.gender = associationsV.getGender();
		this.id = associationsV.getUserId();
		this.imanticUserId = associationsV.getImanticUserId();
		this.lastActiveTime = associationsV.getLastActiveTime();
		this.lastLoginTime = associationsV.getLastLoginTime();
		this.lastName = associationsV.getLastName();
		this.location = associationsV.getLocation();
		this.middleName = associationsV.getMiddleName();
		this.mobileNumber = associationsV.getMobileNumber();
		this.osVersion = associationsV.getOsVersion();
		this.pwdChangeToken = associationsV.getPwdChangeToken();
		this.pwdTokenExpireDate = associationsV.getPwdTokenExpireDate();
		this.race = associationsV.getRace();
		this.smsFlag = associationsV.getSmsFlag();
		this.state = associationsV.getState();
		this.token = associationsV.getToken();
		this.userCreatedOn = associationsV.getUserCreatedOn();
		this.userDeviceType = associationsV.getUserDeviceType();
		this.userImageUrl = associationsV.getUserImageUrl();
		this.userUpdatedOn = associationsV.getUserUpdatedOn();
		this.userZoneId = associationsV.getUserZoneId();
		this.voipToken = associationsV.getVoipToken();
		this.zipCode = associationsV.getZipCode();
	}
}
