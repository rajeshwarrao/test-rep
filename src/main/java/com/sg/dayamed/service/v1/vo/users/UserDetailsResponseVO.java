package com.sg.dayamed.service.v1.vo.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserDetailsResponseVO {

	private List<UserDetailsVO> users = new ArrayList<>();

	private List<ProviderVO> providers = new ArrayList<>();

	private List<CaregiverVO> caregivers = new ArrayList<>();

	private List<PharmacistVO> pharmacists = new ArrayList<>();

	private Integer totalRecords = NumberUtils.INTEGER_ZERO;
}
