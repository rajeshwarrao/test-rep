package com.sg.dayamed.service.v1.vo.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PatientDetailsResponseVO {

	private List<PatientDetailsVO> patientDetails = new ArrayList<>();

	private Integer totalRecords = 0;
}
