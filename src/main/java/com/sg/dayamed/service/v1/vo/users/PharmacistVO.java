package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.rest.controller.ws.users.WSPharmacist;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PharmacistVO {

	private Long id;

	private String company;

	private UserDetailsVO userDetails;

	public PharmacistVO(UserPreferenceVO userPreferenceVO) {
		this.id = userPreferenceVO.getPharmacistId();
		this.userDetails = new UserDetailsVO(userPreferenceVO);
	}

	public WSPharmacist toWsPharmacist() {
		WSPharmacist pharmacist = new WSPharmacist();
		pharmacist.setCompany(this.company);
		pharmacist.setId(this.id);
		pharmacist.setUserDetails(userDetails != null ? userDetails.toWsUserDetails() : null);
		return pharmacist;
	}
}
