package com.sg.dayamed.service.v1.impl;

import com.sg.dayamed.adapter.patients.PatientLoadingProcessor;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.v1.PharmacistService;
import com.sg.dayamed.service.v1.PrescriptionInfoVService;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/

@Service("pharmacistServiceV1")
public class PharmacistServiceImpl extends com.sg.dayamed.service.impl.PharmacistServiceImpl
		implements PharmacistService {

	@Autowired
	PrescriptionInfoVService prescriptionInfoVService;

	@Autowired
	PatientLoadingProcessor loadingProcessor;

	@Autowired
	UserValidator userValidator;

	@Override
	public PatientDetailsResponseVO getPatientsForGSEnabled(JwtUserDetails userDetails, String status,
	                                                        LoadPatientRequestVO requestVO)
			throws ApplicationException {
		PatientDetailsResponseVO responseVO;
		List<PatientDetailsVO> detailsVS = new ArrayList<>();
		try {
			Integer limit = requestVO.getLimit();
			Integer offset = requestVO.getOffset();
			requestVO.setLimit(Integer.MAX_VALUE);
			requestVO.setOffset(NumberUtils.INTEGER_ZERO);
			UserRoleEnum userRoleEnum = userValidator.validatePharmacistPatients(userDetails);
			responseVO = loadingProcessor.getPatients(userRoleEnum, requestVO);
			List<Long> patientIds = responseVO.getPatientDetails()
			                                  .stream()
			                                  .map(PatientDetailsVO::getId)
			                                  .distinct()
			                                  .collect(Collectors.toList());
			PharmacistModel pharmacistModel = this.fetchPharmacistByUserId(userDetails.getUserId());
			if (pharmacistModel == null) {
				throw new DataValidationException(APIErrorFields.AUTHORIZATION,
				                                  IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
			}
			Long id = pharmacistModel.getId();
			filterAndCreatePageForPatients(userDetails, status, requestVO, responseVO, detailsVS, limit, offset,
			                               patientIds, id);
			return responseVO;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private void filterAndCreatePageForPatients(JwtUserDetails userDetails, String status,
	                                            LoadPatientRequestVO requestVO, PatientDetailsResponseVO responseVO,
	                                            List<PatientDetailsVO> detailsVS, Integer limit, Integer offset,
	                                            List<Long> patientIds, Long id) throws ApplicationException {
		List<Long> filteredPatientIds =
				prescriptionInfoVService.getPatientsByPrescriptionStatus(status, id, userDetails, patientIds);
		List<PatientDetailsVO> filteredPatientDetailsVOS = responseVO.getPatientDetails()
		                                                             .stream()
		                                                             .filter(res -> filteredPatientIds.contains(
				                                                             res.getId()))
		                                                             .collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(filteredPatientDetailsVOS)) {
			List<PatientDetailsVO> patientDetailsVOList = filteredPatientDetailsVOS;

			AtomicInteger count = new AtomicInteger();
			Collection<List<PatientDetailsVO>> collections;
			collections = patientDetailsVOList.stream()
			                                  .collect(Collectors.groupingBy(it -> count.getAndIncrement() / limit))
			                                  .values();
			Integer index = 0;
			for (Collection collection : collections) {
				if (offset.equals(index)) {
					detailsVS = new ArrayList<>(collection);
					break;
				}
				index++;
			}

			responseVO.setPatientDetails(detailsVS);
			responseVO.setTotalRecords(StringUtils.isNotBlank(requestVO.getQuery()) ? patientDetailsVOList.size() :
					                           filteredPatientDetailsVOS.size());
		} else {
			responseVO.setTotalRecords(filteredPatientDetailsVOS.size());
			responseVO.setPatientDetails(filteredPatientDetailsVOS);
		}
	}
}
