package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.entity.view.UserPreferencesV;
import com.sg.dayamed.pojo.UserPreferenceModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserPreferenceVO {

	private String id;

	private Long userId;

	private String emailId;

	private String firstName;

	private String middleName;

	private String lastName;

	private Integer age;

	private String city;

	private String country;

	private String countryCode;

	private LocalDateTime userCreatedDate;

	private String userDeviceType;

	private Boolean emailFlag;

	private Boolean smsFlag;

	private String gender;

	private Long roleId;

	private String role;

	private LocalDateTime userUpdatedDate;

	private LocalDateTime lastActiveTime;

	private LocalDateTime lastLoginTime;

	private String userLocation;

	private String mobileNumber;

	private String userToken;

	private String voipToken;

	private String race;

	private String zipCode;

	private String zoneId;

	private Long userPrefId;

	private String userPref;

	private Long patientId;

	private Long providerId;

	private Long caregiverId;

	private Boolean adminCaregiver;

	private Long pharmacistId;

	private String appVersion;

	private String imanticUserId;

	private String osVersion;

	private String location;

	private String pwdChangeToken;

	private LocalDateTime pwdTokenExpireDate;

	private String state;

	private String token;

	private LocalDateTime userCreatedOn;

	private String userImageUrl;

	private LocalDateTime userUpdatedOn;

	private String userZoneId;

	private Boolean pharmacistEnable;

	private UserPreferenceModel userPreference;

	public UserPreferenceVO(UserPreferencesV preferencesV) {
		if (preferencesV != null) {
			this.id = preferencesV.getId();
			this.adminCaregiver = preferencesV.getAdminCaregiver();
			this.age = preferencesV.getAge();
			this.caregiverId = preferencesV.getCaregiverId();
			this.city = preferencesV.getCity();
			this.country = preferencesV.getCountry();
			this.countryCode = preferencesV.getCountryCode();
			this.emailFlag = preferencesV.getEmailFlag();
			this.firstName = preferencesV.getFirstName();
			this.gender = preferencesV.getGender();
			this.emailId = preferencesV.getEmailId();
			this.lastActiveTime = preferencesV.getLastActiveTime();
			this.lastLoginTime = preferencesV.getLastLoginTime();
			this.lastName = preferencesV.getLastName();
			this.middleName = preferencesV.getMiddleName();
			this.mobileNumber = preferencesV.getMobileNumber();
			this.patientId = preferencesV.getPatientId();
			this.userId = preferencesV.getUserId();
			this.providerId = preferencesV.getProviderId();
			this.pharmacistId = preferencesV.getPharmacistId();
			this.userLocation = preferencesV.getLocation();
			this.userToken = preferencesV.getUserToken();
			this.voipToken = preferencesV.getVoipToken();
			this.userPref = preferencesV.getUserPref();
			this.userPreference = preferencesV.getUserPreference();
			this.userPrefId = preferencesV.getUserPrefId();
			this.race = preferencesV.getRace();
			this.role = preferencesV.getRole();
			this.roleId = preferencesV.getRoleId();
			this.appVersion = preferencesV.getAppVersion();
			this.osVersion = preferencesV.getOsVersion();
			this.location = preferencesV.getLocation();
			this.pwdChangeToken = preferencesV.getPwdChangeToken();
			this.pwdTokenExpireDate = preferencesV.getPwdTokenExpireDate();
			this.state = preferencesV.getState();
			this.token = preferencesV.getToken();
			this.userCreatedOn = preferencesV.getUserCreatedOn();
			this.userImageUrl = preferencesV.getUserImageUrl();
			this.userUpdatedOn = preferencesV.getUserUpdatedOn();
			this.userZoneId = preferencesV.getUserZoneId();
			this.pharmacistEnable = preferencesV.getPharmacistEnable();
			this.zipCode = preferencesV.getZipCode();
			this.imanticUserId = preferencesV.getImaticUserId();
		}
	}
}