package com.sg.dayamed.service.v1.vo.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PatientDetailsVO {

	private Long id;

	private String emoji;

	private String currentAdherence;

	private String patientDeviceId;

	private String predictedAdherence;

	private String projectedAdherence;

	private Integer patientStatus;

	private UserDetailsVO userDetails;
}
