package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.rest.controller.ws.users.WSCaregiver;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class CaregiverVO {

	private Long id;

	private Boolean adminFlag = false;

	private UserDetailsVO userDetails;

	public CaregiverVO(UserPreferenceVO userPreferenceVO) {
		this.id = userPreferenceVO.getCaregiverId();
		this.adminFlag = userPreferenceVO.getAdminCaregiver();
		this.userDetails = new UserDetailsVO(userPreferenceVO);
	}

	public WSCaregiver toWsCaregiver() {
		WSCaregiver caregiver = new WSCaregiver();
		caregiver.setId(this.id);
		caregiver.setAdminFlag(this.adminFlag);
		caregiver.setUserDetails(this.userDetails != null ? this.userDetails.toWsUserDetails() : null);
		return caregiver;
	}
}
