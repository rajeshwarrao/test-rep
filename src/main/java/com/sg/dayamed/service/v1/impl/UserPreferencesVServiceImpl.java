package com.sg.dayamed.service.v1.impl;

import com.sg.dayamed.entity.view.UserPreferencesV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.specification.UserPreferencesVSpecification;
import com.sg.dayamed.repository.view.UserPreferencesVRepository;
import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.vo.users.CaregiverVO;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PharmacistVO;
import com.sg.dayamed.service.v1.vo.users.ProviderVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Service("UserPreferencesVService")
public class UserPreferencesVServiceImpl implements UserPreferencesVService {

	@Autowired
	UserPreferencesVRepository preferencesVRepository;

	@Override
	public List<UserPreferenceVO> getUserPreferences(List<Long> userIds) throws ApplicationException {
		try {
			List<UserPreferencesV> userPreferencesVS = preferencesVRepository.findByUserIdIn(userIds);
			return userPreferencesVS.stream()
			                        .map(UserPreferenceVO::new)
			                        .collect(Collectors.toList());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private List<UserPreferenceVO> sortUserPreferences(LoadPatientRequestVO requestVO,
	                                                   List<UserPreferenceVO> userPreferenceVOS) {
		if (PatientSortByEnum.FIRST_NAME.equals(requestVO.getSortByEnum()) &&
				SortOrderEnum.ASC.equals(requestVO.getSortOrderEnum())) {
			userPreferenceVOS = userPreferenceVOS.stream()
			                                     .sorted(Comparator.comparing(UserPreferenceVO::getFirstName,
			                                                                  String::compareToIgnoreCase))
			                                     .collect(Collectors.toList());
		} else if (PatientSortByEnum.FIRST_NAME.equals(requestVO.getSortByEnum()) &&
				SortOrderEnum.DESC.equals(requestVO.getSortOrderEnum())) {
			userPreferenceVOS = userPreferenceVOS.stream()
			                                     .sorted(Comparator.comparing(UserPreferenceVO::getFirstName,
			                                                                  String::compareToIgnoreCase)
			                                                       .reversed())
			                                     .collect(Collectors.toList());
		}
		return userPreferenceVOS;
	}

	@Override
	public UserDetailsResponseVO getUserPreferences(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO)
			throws ApplicationException {
		UserDetailsResponseVO responseVO = new UserDetailsResponseVO();
		List<UserPreferenceVO> preferenceVOList = new ArrayList<>();
		try {
			List<UserPreferencesV> userPreferencesVS =
					preferencesVRepository.findAll(new UserPreferencesVSpecification(userRoleEnum, requestVO));
			List<UserPreferenceVO> userPreferenceVOS = userPreferencesVS.stream()
			                                                            .map(UserPreferenceVO::new)
			                                                            .collect(Collectors.toList());
			List<UserPreferenceVO> preferenceVOS = userPreferenceVOS;
			if (StringUtils.isNotBlank(requestVO.getQuery())) {
				preferenceVOS = preferenceVOS.stream()
				                             .filter(detail -> StringUtils.containsIgnoreCase(detail.getFirstName(),
				                                                                              requestVO.getQuery()) ||
						                             StringUtils.containsIgnoreCase(detail.getLastName(),
						                                                            requestVO.getQuery()))
				                             .collect(Collectors.toList());
			}
			preferenceVOS = sortUserPreferences(requestVO, preferenceVOS);
			if (CollectionUtils.isNotEmpty(preferenceVOS)) {
				AtomicInteger count = new AtomicInteger();
				Collection<List<UserPreferenceVO>> collections;
				collections = preferenceVOS.stream()
				                           .collect(Collectors.groupingBy(
						                           it -> count.getAndIncrement() / requestVO.getLimit()))
				                           .values();
				Integer index = 0;
				for (Collection collection : collections) {
					if (requestVO.getOffset()
					             .equals(index)) {
						preferenceVOList = new ArrayList<>(collection);
						break;
					}
					index++;
				}
				responseVO = generateUserDetailsResponseVO(preferenceVOList, userRoleEnum, preferenceVOS.size());
			}

			return responseVO;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	@Override
	public List<UserPreferenceVO> getAllUserPreferences() {
		List<UserPreferencesV> userPreferencesVS = preferencesVRepository.findAll();
		return userPreferencesVS.stream()
		                        .map(UserPreferenceVO::new)
		                        .collect(Collectors.toList());
	}

	private UserDetailsResponseVO generateUserDetailsResponseVO(List<UserPreferenceVO> userPreferenceVOS,
	                                                            UserRoleEnum userRoleEnum, Integer totalRecords) {
		UserDetailsResponseVO responseVO = new UserDetailsResponseVO();
		responseVO.setTotalRecords(totalRecords);
		if (UserRoleEnum.PROVIDER.equals(userRoleEnum)) {
			responseVO.setProviders(userPreferenceVOS.stream()
			                                         .map(ProviderVO::new)
			                                         .collect(Collectors.toList()));
		} else if (UserRoleEnum.CAREGIVER.equals(userRoleEnum)) {
			responseVO.setCaregivers(userPreferenceVOS.stream()
			                                          .map(CaregiverVO::new)
			                                          .collect(Collectors.toList()));
		} else if (UserRoleEnum.PHARMACIST.equals(userRoleEnum)) {
			responseVO.setPharmacists(userPreferenceVOS.stream()
			                                           .map(PharmacistVO::new)
			                                           .collect(Collectors.toList()));
		}
		return responseVO;
	}
}
