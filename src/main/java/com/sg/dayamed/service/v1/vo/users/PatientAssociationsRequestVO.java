package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 19/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PatientAssociationsRequestVO {

	private JwtUserDetails userDetails;

	private Long patientId;

	private Long caregiverId;

	private Long providerId;

	public PatientAssociationsRequestVO(JwtUserDetails userDetails, Long patientId, Long caregiverId,
	                                    Long providerId) {
		this.userDetails = userDetails;
		this.patientId = patientId;
		this.caregiverId = caregiverId;
		this.providerId = providerId;
	}
}
