package com.sg.dayamed.service.v1;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.enums.UserRoleEnum;

import java.util.List;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/

public interface UserPreferencesVService {

	List<UserPreferenceVO> getUserPreferences(List<Long> userIds) throws ApplicationException;

	UserDetailsResponseVO getUserPreferences(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO)
			throws ApplicationException;

	List<UserPreferenceVO> getAllUserPreferences();
}
