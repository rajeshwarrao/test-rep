package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 19/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class LoadPatientRequestVO {

	private JwtUserDetails userDetails;

	private Integer offset;

	private Integer limit;

	private PatientSortByEnum sortByEnum;

	private SortOrderEnum sortOrderEnum;

	private Boolean nonAdherentFlag = false;

	private Long providerId;

	private Long caregiverId;

	private String query;

	public LoadPatientRequestVO(JwtUserDetails userDetails, Integer limit, Integer offset, String sortBy,
	                            String sortOrder, Boolean nonAdherentFlag, Long providerId, Long caregiverId,
	                            String query) {
		this.userDetails = userDetails;
		this.caregiverId = caregiverId;
		this.limit = limit;
		this.offset = offset;
		this.nonAdherentFlag = nonAdherentFlag;
		this.providerId = providerId;
		this.sortByEnum = PatientSortByEnum.getPatientSortByEnum(sortBy);
		this.sortOrderEnum = SortOrderEnum.getSortTypeEnum(sortOrder);
		this.query = query;
	}
}
