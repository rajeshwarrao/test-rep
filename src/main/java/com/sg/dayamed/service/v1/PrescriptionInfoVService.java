package com.sg.dayamed.service.v1;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import java.util.List;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/

public interface PrescriptionInfoVService {

	List<Long> getPatientsByPrescriptionStatus(String status, Long pharmacistId, JwtUserDetails userDetails,
	                                           List<Long> patientIds) throws ApplicationException;
}
