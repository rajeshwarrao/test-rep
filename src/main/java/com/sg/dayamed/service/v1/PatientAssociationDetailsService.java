package com.sg.dayamed.service.v1;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/

public interface PatientAssociationDetailsService {

	PatientDetailsResponseVO getPatients(LoadPatientRequestVO requestVO) throws ApplicationException;

	PatientAssociationsVO getPatientAssociations(PatientAssociationsRequestVO requestVO) throws ApplicationException;

	PatientDetailsResponseVO searchPatients(LoadPatientRequestVO requestVO, String query) throws ApplicationException;
}
