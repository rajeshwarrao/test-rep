package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.util.enums.UserRoleEnum;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PatientAssociationsVO {

	private PatientDetailsVO patient;

	private List<CaregiverVO> caregivers = new ArrayList<>();

	private List<ProviderVO> providers = new ArrayList<>();

	private List<PharmacistVO> pharmacists = new ArrayList<>();

	public PatientAssociationsVO(List<UserPreferenceVO> userPreferenceVOS, UserRoleEnum userRoleEnum) {
		List<UserPreferenceVO> caregiverPrefs = new ArrayList<>();
		List<UserPreferenceVO> pharmacistPrefs = new ArrayList<>();
		List<UserPreferenceVO> providerPrefs = new ArrayList<>();
		if (UserRoleEnum.PROVIDER.equals(userRoleEnum)) {
			caregiverPrefs = userPreferenceVOS.stream()
			                                  .filter(pref -> StringUtils.equalsIgnoreCase(
					                                  UserRoleEnum.CAREGIVER.getRole(), pref.getRole()))
			                                  .collect(Collectors.toList());
			pharmacistPrefs = userPreferenceVOS.stream()
			                                   .filter(pref -> StringUtils.equalsIgnoreCase(
					                                   UserRoleEnum.PHARMACIST.getRole(), pref.getRole()))
			                                   .collect(Collectors.toList());
		} else if (UserRoleEnum.CAREGIVER.equals(userRoleEnum)) {
			providerPrefs = userPreferenceVOS.stream()
			                                 .filter(pref -> StringUtils.equalsIgnoreCase(
					                                 UserRoleEnum.PROVIDER.getRole(), pref.getRole()))
			                                 .collect(Collectors.toList());
			pharmacistPrefs = userPreferenceVOS.stream()
			                                   .filter(pref -> StringUtils.equalsIgnoreCase(
					                                   UserRoleEnum.PHARMACIST.getRole(), pref.getRole()))
			                                   .collect(Collectors.toList());
		} else if (UserRoleEnum.PHARMACIST.equals(userRoleEnum)) {
			providerPrefs = userPreferenceVOS.stream()
			                                 .filter(pref -> StringUtils.equalsIgnoreCase(
					                                 UserRoleEnum.PROVIDER.getRole(), pref.getRole()))
			                                 .collect(Collectors.toList());
			pharmacistPrefs = userPreferenceVOS.stream()
			                                   .filter(pref -> StringUtils.equalsIgnoreCase(
					                                   UserRoleEnum.PHARMACIST.getRole(), pref.getRole()))
			                                   .collect(Collectors.toList());
		}
		this.providers = providerPrefs.stream()
		                              .map(pref -> new ProviderVO(pref))
		                              .collect(Collectors.toList());
		this.caregivers = caregiverPrefs.stream()
		                                .map(pref -> new CaregiverVO(pref))
		                                .collect(Collectors.toList());
		this.pharmacists = pharmacistPrefs.stream()
		                                  .map(pref -> new PharmacistVO(pref))
		                                  .collect(Collectors.toList());
	}
}
