package com.sg.dayamed.service.v1.impl;

import com.sg.dayamed.adapter.patients.PatientLoadingProcessor;
import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.specification.PatientAssociationSpecification;
import com.sg.dayamed.repository.specification.PatientLoadingSpecification;
import com.sg.dayamed.repository.view.PatientAssociationDetailsVRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.v1.PatientAssociationDetailsService;
import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import ma.glasnost.orika.MapperFacade;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@Service("PatientAssociationDetailsService")
public class PatientAssociationDetailsServiceImpl implements PatientAssociationDetailsService {

	@Autowired
	PatientAssociationDetailsVRepository associationDetailsVRepository;

	@Autowired
	UserPreferencesVService userPreferencesVService;

	@Autowired
	PatientLoadingProcessor patientLoadingProcessor;

	@Autowired
	MapperFacade mapperFacade;

	@Autowired
	UserValidator userValidator;

	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
		Map<Object, String> seen = new ConcurrentHashMap<>();
		return t -> seen.put(keyExtractor.apply(t), "") == null;
	}

	@Override
	public PatientDetailsResponseVO getPatients(LoadPatientRequestVO requestVO) throws ApplicationException {
		PatientDetailsResponseVO responseVO;
		try {
			String authority = requestVO.getUserDetails()
			                            .getAuthorities()
			                            .stream()
			                            .map(auth -> auth.getAuthority())
			                            .findFirst()
			                            .orElse(null);
			UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(authority);
            /*userValidator.validateGetPatients(userRoleEnum, requestVO);
            responseVO = getPatientDetailsResponse(userRoleEnum, requestVO);*/
			return patientLoadingProcessor.getPatients(userRoleEnum, requestVO);
			//return responseVO;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	@Override
	public PatientAssociationsVO getPatientAssociations(PatientAssociationsRequestVO requestVO)
			throws ApplicationException {
		PatientAssociationsVO patientAssociationsVO = new PatientAssociationsVO();
		try {
			JwtUserDetails userDetails = requestVO.getUserDetails();
			String authority = userDetails.getAuthorities()
			                              .stream()
			                              .map(auth -> auth.getAuthority())
			                              .findFirst()
			                              .orElse(null);
			UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(authority);
			userValidator.validateGetPatientAssociations(userRoleEnum, requestVO);
			List<PatientAssociationDetailsV> associationDetailsVS =
					associationDetailsVRepository.findAll(new PatientAssociationSpecification(userRoleEnum,
					                                                                          requestVO));
			if (CollectionUtils.isNotEmpty(associationDetailsVS)) {
				List<Long> providerUserIds = associationDetailsVS.stream()
				                                                 .filter(ass -> ass.getProviderUserId() != null)
				                                                 .map(PatientAssociationDetailsV::getProviderUserId)
				                                                 .
						                                                 distinct()
				                                                 .collect(Collectors.toList());
				List<Long> caregiverUserIds = associationDetailsVS.stream()
				                                                  .filter(ass -> ass.getCaregiverUserId() != null)
				                                                  .map(PatientAssociationDetailsV::getCaregiverUserId)
				                                                  .
						                                                  distinct()
				                                                  .collect(Collectors.toList());
				List<Long> pharmacistUserIds = associationDetailsVS.stream()
				                                                   .filter(ass -> ass.getPharmacistUserId() != null)
				                                                   .map(PatientAssociationDetailsV::getPharmacistUserId)
				                                                   .
						                                                   distinct()
				                                                   .collect(Collectors.toList());
				List<Long> userIds = Stream.of(pharmacistUserIds, providerUserIds, caregiverUserIds)
				                           .flatMap(id -> id.stream())
				                           .filter(userId -> !userId.equals(userDetails.getUserId()))
				                           .
						                           distinct()
				                           .collect(Collectors.toList());

				List<UserPreferenceVO> userPreferenceVOS = userPreferencesVService.getUserPreferences(userIds);

				patientAssociationsVO = new PatientAssociationsVO(userPreferenceVOS, userRoleEnum);
				patientAssociationsVO.setPatient(mapperFacade.map(associationDetailsVS.get(0),
				                                                  PatientDetailsVO.class));
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
		return patientAssociationsVO;
	}

	@Override
	public PatientDetailsResponseVO searchPatients(LoadPatientRequestVO requestVO, String query)
			throws ApplicationException {
		PatientDetailsResponseVO responseVO = new PatientDetailsResponseVO();
		List<PatientAssociationDetailsV> detailsVS = new ArrayList<>();
		JwtUserDetails userDetails = requestVO.getUserDetails();
		String authority = userDetails.getAuthorities()
		                              .stream()
		                              .map(auth -> auth.getAuthority())
		                              .findFirst()
		                              .orElse(null);
		UserRoleEnum userRoleEnum = UserRoleEnum.getUserRoleEnum(authority);
		try {
			userValidator.validateSearchPatients(userRoleEnum, requestVO, query);
			List<PatientAssociationDetailsV> patientAssociationDetailsVS =
					associationDetailsVRepository.findAll(new PatientLoadingSpecification(userRoleEnum, requestVO));
			patientAssociationDetailsVS = patientAssociationDetailsVS.stream()
			                                                         .filter(distinctByKey(
					                                                         detail -> detail.getPatientId()))
			                                                         .collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(patientAssociationDetailsVS)) {
				patientAssociationDetailsVS = patientAssociationDetailsVS.stream()
				                                                         .filter(detail ->
						                                                                 StringUtils.containsIgnoreCase(
								                                                                 detail.getFirstName(),
								                                                                 query) ||
								                                                                 StringUtils.containsIgnoreCase(
										                                                                 detail.getLastName(),
										                                                                 query))
				                                                         .collect(Collectors.toList());
				patientAssociationDetailsVS = sortPatientAssociationDetailsVS(requestVO, patientAssociationDetailsVS);

				AtomicInteger count = new AtomicInteger();
				Collection<List<PatientAssociationDetailsV>> collections;
				collections = patientAssociationDetailsVS.stream()
				                                         .collect(Collectors.groupingBy(
						                                         it -> count.getAndIncrement() / requestVO.getLimit()))
				                                         .values();
				Integer index = 0;
				for (Collection collection : collections) {
					if (requestVO.getOffset()
					             .equals(index)) {
						detailsVS = new ArrayList<>(collection);
						break;
					}
					index++;
				}

				List<PatientDetailsVO> patientDetailsVOS = detailsVS.stream()
				                                                    .map(details -> mapperFacade.map(details,
				                                                                                     PatientDetailsVO.class))
				                                                    .collect(Collectors.toList());
				responseVO.setPatientDetails(patientDetailsVOS);
				responseVO.setTotalRecords(patientAssociationDetailsVS.size());
			}
			return responseVO;
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private List<PatientAssociationDetailsV> sortPatientAssociationDetailsVS(LoadPatientRequestVO requestVO,
	                                                                         List<PatientAssociationDetailsV> patientAssociationDetailsVS) {
		if (PatientSortByEnum.FIRST_NAME.equals(requestVO.getSortByEnum()) &&
				SortOrderEnum.ASC.equals(requestVO.getSortOrderEnum())) {
			patientAssociationDetailsVS = patientAssociationDetailsVS.stream()
			                                                         .sorted(Comparator.comparing(
					                                                         PatientAssociationDetailsV::getFirstName,
					                                                         String::compareToIgnoreCase))
			                                                         .collect(Collectors.toList());
		} else if (PatientSortByEnum.FIRST_NAME.equals(requestVO.getSortByEnum()) &&
				SortOrderEnum.DESC.equals(requestVO.getSortOrderEnum())) {
			patientAssociationDetailsVS = patientAssociationDetailsVS.stream()
			                                                         .sorted(Comparator.comparing(
					                                                         PatientAssociationDetailsV::getFirstName,
					                                                         String::compareToIgnoreCase)
			                                                                           .reversed())
			                                                         .collect(Collectors.toList());
		}
		return patientAssociationDetailsVS;
	}

}
