package com.sg.dayamed.service.v1.impl;

import com.sg.dayamed.entity.view.PrescriptionInfoV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.view.PrescriptionInfoVRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.service.v1.PrescriptionInfoVService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@Service
public class PrescriptionInfoVServiceImpl implements PrescriptionInfoVService {

	@Autowired
	PrescriptionInfoVRepository prescriptionInfoVRepository;

	@Autowired
	PharmacistService pharmacistService;

	@Override
	public List<Long> getPatientsByPrescriptionStatus(String status, Long pharmacistId, JwtUserDetails userDetails,
	                                                  List<Long> patientIds) throws ApplicationException {
		try {
			List<PrescriptionInfoV> prescriptionInfoVS =
					prescriptionInfoVRepository.findByStatusIgnoreCaseAndPharmacistIdAndPatientIdIn(status,
					                                                                                pharmacistId,
					                                                                                patientIds);
			return prescriptionInfoVS.stream()
			                         .map(PrescriptionInfoV::getPatientId)
			                         .distinct()
			                         .collect(Collectors.toList());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
}
