package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.rest.controller.ws.users.WSProvider;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class ProviderVO {

	private Long id;

	private String license;

	private String specialization;

	private UserDetailsVO userDetails;

	public ProviderVO(UserPreferenceVO userPreferenceVO) {
		this.id = userPreferenceVO.getProviderId();
		this.userDetails = new UserDetailsVO(userPreferenceVO);
	}

	public WSProvider toWsProvider() {
		WSProvider provider = new WSProvider();
		provider.setId(this.id);
		provider.setLicense(this.license);
		provider.setSpecialization(this.specialization);
		provider.setUserDetails(this.userDetails != null ? this.userDetails.toWsUserDetails() : null);
		return provider;
	}
}
