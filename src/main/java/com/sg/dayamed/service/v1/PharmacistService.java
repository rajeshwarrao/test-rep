package com.sg.dayamed.service.v1;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/

public interface PharmacistService extends com.sg.dayamed.service.PharmacistService {

	PatientDetailsResponseVO getPatientsForGSEnabled(JwtUserDetails userDetails, String status,
	                                                 LoadPatientRequestVO requestVO) throws ApplicationException;
}
