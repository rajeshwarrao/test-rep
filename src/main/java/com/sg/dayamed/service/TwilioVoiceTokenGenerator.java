package com.sg.dayamed.service;

import com.google.common.collect.Lists;
import com.twilio.jwt.client.ClientCapability;
import com.twilio.jwt.client.IncomingClientScope;
import com.twilio.jwt.client.OutgoingClientScope;
import com.twilio.jwt.client.Scope;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Class that generates a Twilio voice capability token based on the client requesting it.
 */
@Service("TwilioVoiceTokenService")
public class TwilioVoiceTokenGenerator {

	@Value("${TWILIO_ACCOUNT_SID}")
	String TWILIO_ACCOUNT_SID;

	@Value("${TWILIO_AUTH_TOKEN}")
	String TWILIO_AUTH_TOKEN;

	@Value("${TWILIO_APPLICATION_SID}")
	String TWILIO_APPLICATION_SID;

	@Value("${TWILIO_PHONE_NUMBER}")
	String TWILIO_PHONE_NUMBER;

	private ClientCapability.Builder capabilityBuilder;

	private String identity;

	public ClientCapability.Builder getCapabilityBuilder() {
		return capabilityBuilder;
	}

	public void setCapabilityBuilder() {

		try {
			this.capabilityBuilder =
					new ClientCapability.Builder(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	/**
	 * Generates JWT token based on the client identity requesting it
	 */

	public String generateToken() {

		OutgoingClientScope outgoingScope = new OutgoingClientScope.Builder(TWILIO_APPLICATION_SID).build();

		IncomingClientScope incomingScope = new IncomingClientScope(identity);

		List<Scope> scopes = Lists.newArrayList(outgoingScope, incomingScope);

		String token = capabilityBuilder.scopes(scopes)
		                                .build()
		                                .toJwt();

		return token;
	}
}
