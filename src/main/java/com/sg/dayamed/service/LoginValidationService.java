package com.sg.dayamed.service;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.security.jwtsecurity.security.JwtGenerator;

import javax.servlet.http.HttpServletRequest;

public interface LoginValidationService {

	public UserDetailsWithJwtModel loginValidation(LoginRequestModel loginRequestModel,
	                                               HttpServletRequest request, JwtGenerator jwtGenerator)
			throws Exception;

}
