package com.sg.dayamed.email;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.apache.commons.lang3.StringUtils;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageModel {

	private String tocontact;

	private String countryPhoneCode;

	private String message;

	private String url;

	public String getTocontact() {
		return tocontact;
	}

	public String contactWithCountryCode() {
		return "+" + StringUtils.trimToEmpty(countryPhoneCode) + tocontact;
	}

}
