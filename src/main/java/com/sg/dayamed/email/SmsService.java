package com.sg.dayamed.email;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SmsService {

	private final static String ACCOUNT_SID = "ACb390340f7782cd5e11ff249fce0d5e7a";

	private final static String AUTH_TOKEN = "324a4a8fa303a03ebd500df71faf5c77";

	private final static String FROM_CONTACT = "+17738394336";

	private static final Logger logger = LoggerFactory.getLogger(SmsService.class);

	public SmsService() {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}

	public boolean sendSMSNotification(MessageModel messageModel) {
		boolean flag = false;
		if (messageModel == null) {
			logger.error("Message model is null to send SMS");
		} else if (StringUtils.isBlank(messageModel.getTocontact())) {
			logger.error("Contact is empty or null to send SMS");
		} else {
			String contact = messageModel.contactWithCountryCode();
			try {
				Message.creator(new PhoneNumber(contact), new PhoneNumber(FROM_CONTACT), messageModel.getMessage())
				       .create();
				flag = true;
				logger.info("SMS sent to {} ", contact);
			} catch (Exception smsException) {
				logger.error("Error while sending SMS to {}", contact, smsException);
			}
		}
		return flag;
	}
}
