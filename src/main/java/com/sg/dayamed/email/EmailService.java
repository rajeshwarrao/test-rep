package com.sg.dayamed.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Component
public class EmailService {

	@Value("${dayamed.email.host}")
	String host;

	@Value("${dayamed.email.port}")
	String port;

	@Value("${dayamed.email.username}")
	String userName;

	@Value("${dayamed.email.password}")
	String passWord;

	@Value("${dayamed.email.from}")
	String from;

	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	public void sendEmail(Email emailObj) {

		final String username = userName;
		final String password = passWord;

		// Recipient's email ID needs to be mentioned.
		if (emailObj != null && emailObj.getRecipientList()
		                                .size() > 0) {
			String recipientsCommaSeparated = String.join(",", emailObj.getRecipientList());
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);

			// Get the Session object.
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			try {
				//logger.info("mail user : "+ userName +"To : "+passWord);
				//logger.info("host : "+ host +"To : "+recipientsCommaSeparated);
				// Create a default MimeMessage object.
				Message message = new MimeMessage(session);
				// Set From: header field of the header.
				message.setFrom(new InternetAddress(from));
				// Set To: header field of the header.
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientsCommaSeparated));
				// Set Subject: header field
				message.setSubject(emailObj.getSubject());

				message.setContent(emailObj.getBody(), "text/html; charset=utf-8");
				// Now set the actual message
				//message.setText();

				// Send message

				Transport.send(message);
				//logger.info("Mail sent successfully to... :" + recipientsCommaSeparated);
			} catch (MessagingException e) {
				logger.error("Error while sending mails : MessagingException", e);
				throw new RuntimeException(e.getLocalizedMessage());
			} catch (Exception e) {
				logger.error("Error while sending mails", e);
				throw new RuntimeException(e.getLocalizedMessage());
			}
		}
	}
}
