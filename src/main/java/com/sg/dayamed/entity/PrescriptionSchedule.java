package com.sg.dayamed.entity;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "Prescription_Schedule")
public class PrescriptionSchedule implements Serializable {

	private static final long serialVersionUID = 3074959957593829140L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "actual_date")
	private LocalDateTime prescribedTime;

	@Column(name = "fire_time")
	private LocalDateTime fireTime;

	@Column(name = "prescrptionId")
	private Long prescriptionId;

	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public LocalDateTime getFireTime() {
		return fireTime;
	}

	public void setFireTime(LocalDateTime fireTime) {
		this.fireTime = fireTime;
	}

	@Column(name = "userid")
	private long userId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dosageinfo_id", referencedColumnName = "patient_dosage_info_id")
	private DosageInfo dosageInfo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "deviceinfo_id", referencedColumnName = "device_info_id")
	private DeviceInfo deviceInfo;

	@ElementCollection
	@CollectionTable(name = "prescriptionschedule_consumptionstatus", joinColumns = @JoinColumn(name =
			                                                                                            "prescriptionschedule_id"))
	@Column(name = "consumptionstatus")
	private List<String> consumptionStatus = new LinkedList<String>();

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(LocalDateTime prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public DosageInfo getDosageInfo() {
		return dosageInfo;
	}

	public void setDosageInfo(DosageInfo dosageInfo) {
		this.dosageInfo = dosageInfo;
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

}
