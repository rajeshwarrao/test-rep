package com.sg.dayamed.entity.usho.image;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UshoImagesRepository extends JpaRepository<UshoImages, Long> {

}
