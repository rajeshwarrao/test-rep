package com.sg.dayamed.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "Medicine")
@Getter
@Setter
public class Medicine implements Serializable {

	private static final long serialVersionUID = 6090706367418566240L;

	@Id
	@Column(name = "medicine_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "medicine_name")
	private String name;

	@Column(name = "medicine_type")
	private String type;

	@Column(name = "medicine_description")
	private String description;

	@Column(name = "medicine_category")
	private String category;

	@Column(name = "imageUrl")
	private String imageURL;

	@Column(name = "ndc_code")
	private Long ndcCode;

	@Column(name = "ndc_type")
	private String ndcType;

	@Column(name = "gen_code")
	private Long genCode;

	@Column(name = "packager")
	private String packager;

	@Column(name = "vuca_video_available")
	private Boolean vucaVideoAvailable;

	@javax.persistence.Transient
	private String itemCategoryType;
}
