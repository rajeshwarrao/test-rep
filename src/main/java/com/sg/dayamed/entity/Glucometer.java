package com.sg.dayamed.entity;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "Glucometer")
public class Glucometer implements Serializable {

	private static final long serialVersionUID = -7607089302214183751L;

	@Id
	@Column(name = "glucometer_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "prescribed_time")
	private LocalDateTime prescribedTime;

	@Column(name = "observed_time")
	private LocalDateTime observedTime;

	@ElementCollection
	@CollectionTable(name = "gluco_consumptionstatus", joinColumns = @JoinColumn(name = "gluco_id"))
	@Column(name = "consumptionstatus")
	private List<String> consumptionStatus = new LinkedList<String>();

	@Column(name = "glucometer_reading")
	private String readingValue;

	@Column(name = "location")
	private ArrayList<String> location;

	@Column(name = "deviceInfoId")
	private String deviceInfoId;

	@Column(name = "prescriptionid")
	private long prescriptionID;

	@ManyToOne(cascade = CascadeType.REFRESH)
	private Patient patient;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getReadingValue() {
		return readingValue;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void setReadingValue(String readingValue) {
		this.readingValue = readingValue;
	}

	public LocalDateTime getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(LocalDateTime prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	public LocalDateTime getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(LocalDateTime observedTime) {
		this.observedTime = observedTime;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public String getDeviceInfoId() {
		return deviceInfoId;
	}

	public void setDeviceInfoId(String deviceInfoId) {
		this.deviceInfoId = deviceInfoId;
	}

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

}
