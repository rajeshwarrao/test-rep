package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;



@Getter
@Setter
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@Table(name = "user_details")
public class UserDetails implements Serializable {

	private static final long serialVersionUID = -6638676289836299123L;

	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "first_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "first_name", read = "CAST(AES_DECRYPT(first_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))", write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String firstName;

	@Column(name = "last_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "last_name", read = "CAST(AES_DECRYPT(last_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))", write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String lastName;

	@Column(name = "middle_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "middle_name", read = "CAST(AES_DECRYPT(middle_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))", write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String middleName;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "mobile_number", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "mobile_number", read = "CAST(AES_DECRYPT(mobile_number, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))", write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String mobileNumber;

	@Column(name = "email_id", columnDefinition = "VARBINARY" , unique = true)
	@ColumnTransformer(forColumn = "email_id", read = "CAST(AES_DECRYPT(email_id, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))", write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String emailId;

	@Column(name = "gender")
	private String gender;

	@Column(name = "age")
	private Integer age;

	@Column(name = "date_of_birth")
	private LocalDate dateOfBirth;

	@Column(name = "race")
	private String race;

	@Column(name = "imantic_userid")
	private String imanticUserid;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "password")
	private String password;

	@Column(name = "token", length = 200)
	private String token;

	@Column(name = "voiptoken", length = 200)
	private String voipToken;

	@Column(name = "device_type")
	private String type;

	@Embedded
	private Address address;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "userDetails")
	@JsonBackReference(value = "patient-userDetails")
	private Patient patient;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "role_id")
	private Role role;

	@Column(name = "pwd_change_token")
	private String pwdChangeToken;

	@Column(name = "pwd_token_expire_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date pwdTokenExpireDate;

	@Column(name = "app_version")
	private String appVersion;

	@Column(name = "os_version")
	private String osVersion;

	@Column(name = "last_logintime")
	private Date lastLoginTime;

	@Column(name = "last_activetime")
	private Date lastActiveTime;

	@Column(name = "created_on", updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createOn;

	@Column(name = "updated_on", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updateOn;

	@Column(name = "smsflag", columnDefinition = "boolean default true")
	private boolean smsFlag;

	@Column(name = "emailflag", columnDefinition = "boolean default true")
	private boolean emailFlag;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "userDetails")
	@JsonBackReference(value = "useroptions-userDetails")
	private UserOptions userOptions;

	@Column(name = "user_zoneid")
	private String userZoneId;

}
