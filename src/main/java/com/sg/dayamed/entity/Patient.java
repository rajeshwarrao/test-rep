package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "dayamed_patient")
public class Patient implements Serializable {

	private static final long serialVersionUID = 1552635787467712605L;

	@Id
	@Column(name = "patient_id")
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "predictedadherence")
	private String predictedAdherence;

	@Column(name = "currentadherence")
	private String currentAdherence;

	@Column(name = "projectedadherence")
	private String projectedAdherence;

	@Column(name = "emoji")
	private String emoji;

	@Column(name = "status")
	private Integer status = 0;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getEmoji() {
		return emoji;
	}

	public void setEmoji(String emoji) {
		this.emoji = emoji;
	}

	@Column(name = "deviceId")
	private String deviceId;

	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	@JsonManagedReference(value = "patient-userDetails")
	private UserDetails userDetails;

	/*@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Caregiver caregiver;*/

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "patient_caregiver_map", joinColumns = @JoinColumn(name = "patient_id"), inverseJoinColumns =
	@JoinColumn(name = "caregiver_id"))
	private Set<Caregiver> caregivers;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "patient_provider_map", joinColumns = @JoinColumn(name = "patient_id"), inverseJoinColumns =
	@JoinColumn(name = "provider_id"))
	private Set<Provider> providers;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "patient_pharmasist_map", joinColumns = @JoinColumn(name = "patient_id"), inverseJoinColumns =
	@JoinColumn(name = "pharmasist_id"))
	private Set<Pharmacist> pharmacists;

	//@JsonIgnore
	@JsonBackReference(value = "patient_Prescription")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "patient")
	public List<Prescription> Prescriptions = new ArrayList<Prescription>();

	public List<Prescription> getPrescriptions() {
		return Prescriptions;
	}

	public void setPrescriptions(List<Prescription> prescriptions) {
		Prescriptions = prescriptions;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPredictedAdherence() {
		return predictedAdherence;
	}

	public void setPredictedAdherence(String predictedAdherence) {
		this.predictedAdherence = predictedAdherence;
	}

	public String getCurrentAdherence() {
		return currentAdherence;
	}

	public void setCurrentAdherence(String currentAdherence) {
		this.currentAdherence = currentAdherence;
	}

	public String getProjectedAdherence() {
		return projectedAdherence;
	}

	public void setProjectedAdherence(String projectedAdherence) {
		this.projectedAdherence = projectedAdherence;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public Set<Provider> getProviders() {
		return providers;
	}

	public void setProviders(Set<Provider> providers) {
		this.providers = providers;
	}

	public Set<Pharmacist> getPharmacists() {
		return pharmacists;
	}

	public void setPharmacists(Set<Pharmacist> pharmacists) {
		this.pharmacists = pharmacists;
	}

	public Set<Caregiver> getCaregivers() {
		return caregivers;
	}

	public void setCaregivers(Set<Caregiver> caregivers) {
		this.caregivers = caregivers;
	}

}
