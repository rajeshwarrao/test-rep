package com.sg.dayamed.entity;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "notification_schedule")
public class NotificationSchedule implements Serializable {

	private static final long serialVersionUID = -1572713277889314863L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "userid")
	private long userId;

	@Column(name = "actual_date")
	private LocalDateTime actualDate;
	
	/*@Column(name = "actual_time")
	private String actualTime;*/

	@Column(name = "fire_time")
	private LocalDateTime fireTime;

	@Column(name = "prescrptionId")
	private Long prescriptionId;

	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	@Column(name = "owner_userid")
	private Long owner_userId;

	@ElementCollection
	@CollectionTable(name = "notificationschedule_consumptionstatus", joinColumns = @JoinColumn(name =
			                                                                                            "notificationschedule_id"))
	@Column(name = "consumptionstatus")
	private List<String> consumptionStatus = new LinkedList<String>();

	@Column(name = "notificationType")
	private String notificationType;

	@Column(name = "notificationvalue")
	private String notificationValue;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dosageinfo_id", referencedColumnName = "patient_dosage_info_id")
	private DosageInfo dosageInfo;
	
	/*@Column(name = "dosageinfo_id")
	private long dosageInfo;*/

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "deviceinfo_id", referencedColumnName = "device_info_id")
	private DeviceInfo deviceInfo;
	
	/*@Column(name = "deviceinfo_id")
	private long deviceInfo;*/

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public LocalDateTime getActualDate() {
		return actualDate;
	}

	public void setActualDate(LocalDateTime actualDate) {
		this.actualDate = actualDate;
	}

	public LocalDateTime getFireTime() {
		return fireTime;
	}

	public void setFireTime(LocalDateTime fireTime) {
		this.fireTime = fireTime;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getNotificationValue() {
		return notificationValue;
	}

	public void setNotificationValue(String notificationValue) {
		this.notificationValue = notificationValue;
	}

	public Long getOwner_userId() {
		return owner_userId;
	}

	public void setOwner_userId(Long owner_userId) {
		this.owner_userId = owner_userId;
	}

	public DosageInfo getDosageInfo() {
		return dosageInfo;
	}

	public void setDosageInfo(DosageInfo dosageInfo) {
		this.dosageInfo = dosageInfo;
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

}
