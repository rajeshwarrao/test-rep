package com.sg.dayamed.entity;

import lombok.Data;

import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Data
@Embeddable
public class Address {

	@Column(name = "country", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "country",
	                   read = "CAST(AES_DECRYPT(country, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))",
	                   write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String country;

	@Column(name = "state", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "state",
	                   read = "CAST(AES_DECRYPT(state, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))",
	                   write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String state;

	@Column(name = "city", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "city",
	                   read = "CAST(AES_DECRYPT(city, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))",
	                   write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String city;

	@Column(name = "location", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "location",
	                   read = "CAST(AES_DECRYPT(location, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))",
	                   write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String location;

	@Column(name = "zip_code", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "zip_code",
	                   read = "CAST(AES_DECRYPT(zip_code, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))",
	                   write = "AES_ENCRYPT(?, DB_ENCRYPTION_PASSWORD())")
	private String zipCode;

}
