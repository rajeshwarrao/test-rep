package com.sg.dayamed.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "user_role")
public class Role implements Serializable {

	private static final long serialVersionUID = -3949529757461789108L;

	@Id
	@Column(name = "user_role_id")
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "user_role_name")
	private String name;

	@Column(name = "user_role_discription")
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
