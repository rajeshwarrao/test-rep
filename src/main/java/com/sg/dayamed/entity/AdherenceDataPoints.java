package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(name = "Adherence")
public class AdherenceDataPoints implements Serializable {

	private static final long serialVersionUID = 1035106348303013190L;

	@Id
	@Column(name = "adherence_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "location")
	private ArrayList<String> location;

	@ElementCollection
	@CollectionTable(name = "adherence_consumptionstatus", joinColumns = @JoinColumn(name = "Adherence_id"))
	@Column(name = "consumptionstatus")
	private List<String> consumptionStatus = new ArrayList<String>();

	@Column(name = "gs_notification_status")
	private Long goodShapredNotificationStatus;

	public long getGoodShapredNotificationStatus() {
		return goodShapredNotificationStatus;
	}

	public void setGoodShapredNotificationStatus(Long goodShapredNotificationStatus) {
		this.goodShapredNotificationStatus = goodShapredNotificationStatus == null ? 0 : goodShapredNotificationStatus;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public HashSet<String> getHow() {
		return how;
	}

	public void setHow(HashSet<String> how) {
		this.how = how;
	}

	@Column(name = "how")
	private HashSet<String> how;

	@Column(name = "observedtime")
	private LocalDateTime observedTime;

	@Column(name = "prescribedtime")
	private LocalDateTime prescribedTime;
	
	/*@Column(name = "consumption_status")
	private String consumptionStatus;*/
	
	
	/*@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "dosageinfo_id", referencedColumnName = "patient_dosage_info_id")
	//@JsonManagedReference(value = "adherence-dosageinfo")
	private DosageInfo dosageInfo;*/
	
	/*@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL} ,mappedBy = "adherenceDataPoints")
	private List<DosageInfo> dosageInfoList;*/
	
	/*@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "deviceinfo_id", referencedColumnName = "id")
	@JsonManagedReference(value = "adherence-deviceinfo")
	private DeviceInfo deviceInfo;*/

	public LocalDateTime getPrescribedTime() {
		return prescribedTime;
	}

	public LocalDateTime getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(LocalDateTime observedTime) {
		this.observedTime = observedTime;
	}

	public void setPrescribedTime(LocalDateTime prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL}, mappedBy = "adherence")
	private List<Adherence_DosageInfoMap> adherenceDosageInfoMapList;
	

	/*public List<Adherence_DosageInfoMap> getAdherenceDosageInfoMapList() {
		return Adherence_DosageInfoMapList;
	}

	public void setAdherenceDosageInfoMapList(List<Adherence_DosageInfoMap> adherenceDosageInfoMapList) {
		Adherence_DosageInfoMapList = adherenceDosageInfoMapList;
	}*/

	public List<Adherence_DosageInfoMap> getAdherenceDosageInfoMapList() {
		return adherenceDosageInfoMapList;
	}

	public void setAdherenceDosageInfoMapList(List<Adherence_DosageInfoMap> adherenceDosageInfoMapList) {
		this.adherenceDosageInfoMapList = adherenceDosageInfoMapList;
	}

	//private ArrayList<String> dosageInfoIds;
	private long prescriptionID;

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

}
