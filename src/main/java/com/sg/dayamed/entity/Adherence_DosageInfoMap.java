package com.sg.dayamed.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "adherence_dosageinfo_map")
public class Adherence_DosageInfoMap implements Serializable {

	private static final long serialVersionUID = 5190862870398012251L;

	@Id
	@Column(name = "adherence_dosageinfo_map_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private AdherenceDataPoints adherence;

	@Column(name = "dosageinfo_id")
	private Long dosageinfoId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public AdherenceDataPoints getAdherence() {
		return adherence;
	}

	public void setAdherence(AdherenceDataPoints adherence) {
		this.adherence = adherence;
	}

	public Long getDosageinfoId() {
		return dosageinfoId;
	}

	public void setDosageinfoId(Long dosageinfoId) {
		this.dosageinfoId = dosageinfoId;
	}

}
