package com.sg.dayamed.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "cart")
public class Cart implements Serializable {

	private static final long serialVersionUID = 6947017363145419723L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "patient_user_id")
	private long patientUserId;

	@Column(name = "item_id")
	private long itemId;

	@Column(name = "item_prescription_id")
	private long itemPrescriptionId;

	@Column(name = "item_category_type")
	private String itemCategoryType;

	@Column(name = "added_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date addedDate;

	@Column(name = "created_user_id", nullable = true)
	private Long createdUserId;

	@Column(name = "created_userrole")
	private String createdUserRole;

	public long getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(Long createdUserId) {
		this.createdUserId = createdUserId == null ? 0 : createdUserId;
	}

	public String getCreatedUserRole() {
		return createdUserRole;
	}

	public void setCreatedUserRole(String createdUserRole) {
		this.createdUserRole = createdUserRole;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPatientUserId() {
		return patientUserId;
	}

	public void setPatientUserId(long patientUserId) {
		this.patientUserId = patientUserId;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public long getItemPrescriptionId() {
		return itemPrescriptionId;
	}

	public void setItemPrescriptionId(long itemPrescriptionId) {
		this.itemPrescriptionId = itemPrescriptionId;
	}

	public String getItemCategoryType() {
		return itemCategoryType;
	}

	public void setItemCategoryType(String itemCategoryType) {
		this.itemCategoryType = itemCategoryType;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
