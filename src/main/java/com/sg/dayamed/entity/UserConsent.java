package com.sg.dayamed.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "userconsent")
public class UserConsent implements Serializable {

	private static final long serialVersionUID = 110508009033156233L;

	@Id
	@Column(name = "userconsent_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "status")
	private boolean status;

	@Column(name = "version")
	private String version;

	@Column(name = "userid")
	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
