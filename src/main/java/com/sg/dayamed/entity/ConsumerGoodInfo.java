package com.sg.dayamed.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;

@Entity
@Table(name = "consumergoods_info")
public class ConsumerGoodInfo implements Serializable {

	private static final long serialVersionUID = 2486550970783760157L;

	@Id
	@Column(name = "consumergoods_info_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "time")
	//@ElementCollection(targetClass=String.class)
	private ArrayList<String> time;

	@Column(name = "goods_comment")
	private String Comment;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	private ConsumerGoods commodity;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	//@JsonManagedReference(value="prescription-commodityinfo")
	private Prescription prescription;

	@Column(name = "deleteflag")
	private boolean deleteflag;

	public boolean isDeleteflag() {
		return deleteflag;
	}

	public void setDeleteflag(boolean deleteflag) {
		this.deleteflag = deleteflag;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArrayList<String> getTime() {
		return time;
	}

	public void setTime(ArrayList<String> time) {
		this.time = time;
	}

	public Prescription getPrescription() {
		return prescription;
	}

	public void setPrescription(Prescription prescription) {
		this.prescription = prescription;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public ConsumerGoods getCommodity() {
		return commodity;
	}

	public void setCommodity(ConsumerGoods commodity) {
		this.commodity = commodity;
	}

}
