package com.sg.dayamed.entity;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.ZonedDateTime;

@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "user_pref")
public class UserPreference {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "user_setting", columnDefinition = "TEXT")
	private String userPrefSetting;

	@Column(name = "created_on", updatable = false)
	private ZonedDateTime createOn = ZonedDateTime.now();

	@Column(name = "updated_on")
	@UpdateTimestamp
	private ZonedDateTime updateOn = ZonedDateTime.now();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	UserDetails userDetails;

}
