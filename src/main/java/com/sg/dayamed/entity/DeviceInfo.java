package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "device_info")
public class DeviceInfo implements Serializable {

	private static final long serialVersionUID = -8729378933245912550L;

	@Id
	@Column(name = "device_info_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "device_info_comment")
	private String comment;

	@Column(name = "device_info_time")
	private ArrayList<String> time;

	@Column(name = "deleteflag")
	private boolean deleteflag;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)    //have to check this relation
	private Device device;

	@Column(name = "device_duration")
	private int duration;

	@Column(name = "serialNo")
	private String serialNo;

	@Column(name = "status")
	private String status;

	@JsonManagedReference(value = "deviceInfo-Prescription")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	//@JsonManagedReference(value="prescription-deviceinfo")
	private Prescription prescription;

	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "deviceInfo")
	private List<DeviceNotification> deviceNotificationList;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public List<DeviceNotification> getDeviceNotificationList() {
		return deviceNotificationList;
	}

	public void setDeviceNotificationList(List<DeviceNotification> deviceNotificationList) {
		this.deviceNotificationList = deviceNotificationList;
	}

	public boolean isDeleteflag() {
		return deleteflag;
	}

	public void setDeleteflag(boolean deleteflag) {
		this.deleteflag = deleteflag;
	}

	public Prescription getPrescription() {
		return prescription;
	}

	public void setPrescription(Prescription prescription) {
		this.prescription = prescription;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public ArrayList<String> getTime() {
		return time;
	}

	public void setTime(ArrayList<String> time) {
		this.time = time;
	}

}
