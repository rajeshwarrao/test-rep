package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "dayamed_caregiver")
public class Caregiver implements Serializable {

	private static final long serialVersionUID = 715948623399218020L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/*@Column(name="typeofdevice")
	private String typeofDevice;*/
	
	/*@Column(name = "deviceId")
	private String deviceId;*/

	@Column(name = "adminflag")
	private boolean adminflag;

	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserDetails userDetails;

	@ManyToMany(cascade = {CascadeType.REFRESH})
	@JoinTable(name = "caregiver_provider_map", joinColumns = @JoinColumn(name = "caregiver_id"), inverseJoinColumns =
	@JoinColumn(name = "provider_id"))
	private Set<Provider> providers;

	@JsonIgnore
	@ManyToMany(mappedBy = "caregivers")
	private Set<Patient> patients;

	public Set<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Set<Patient> patients) {
		this.patients = patients;
	}

	public boolean isAdminflag() {
		return adminflag;
	}

	public void setAdminflag(boolean adminflag) {
		this.adminflag = adminflag;
	}

	/*public String getTypeofDevice() {
		return typeofDevice;
	}

	public void setTypeofDevice(String typeofDevice) {
		this.typeofDevice = typeofDevice;
	}*/

	public Set<Provider> getProviders() {
		return providers;
	}

	public void setProviders(Set<Provider> providers) {
		this.providers = providers;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

}
