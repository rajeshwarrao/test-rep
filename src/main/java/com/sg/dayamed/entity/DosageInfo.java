package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "patient_dosage_info")
public class DosageInfo implements Serializable {

	private static final long serialVersionUID = 6847017333035309629L;

	@Id
	@Column(name = "patient_dosage_info_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "dosage_intake_time")
	private ArrayList<String> time;

	@Column(name = "disage_course_duration")
	private int duration;

	@Lob
	@Column(name = "comnent")
	private String comment;

	@Column(name = "identitycode")
	private String identityCode;

	@Column(name = "adherencetype")
	private String adherenceType;

	@Column(name = "pharmacistcomment")
	private String pcomment;

	@Column(name = "deleteflag")
	private boolean deleteflag;

	@Column(name = "consumeDuration")
	private Integer consumeDuration;

	@Column(name = "frequencyJson", columnDefinition = "TEXT")
	private String frequency;

	@Column(name = "custom_video_link")
	private String customVideoLink;
	
	@Column(name = "dosage_time_image_url")
	private String dosageTimeImageUrl;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "consumption_template", referencedColumnName = "consumptions_key")
	private ConsumptionTemplate consumptionTemplate;


	@JsonManagedReference(value = "dosageInfo-Prescription")
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "prescription_id", referencedColumnName = "patient_prescription_id")
	private Prescription prescription;
	
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Medicine medicine;

	@JsonIgnore
	@OneToMany(cascade = {CascadeType.ALL}, mappedBy = "dosageInfo")
	private List<DosageNotification> dosageNotificationList;

}
