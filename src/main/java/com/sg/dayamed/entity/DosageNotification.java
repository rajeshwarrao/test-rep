package com.sg.dayamed.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "dosage_notification")
@Getter
@Setter
public class DosageNotification implements Serializable {

	private static final long serialVersionUID = -5594009921204790611L;

	@Id
	@Column(name = "dosage_info_notification_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "expectedTime")
	private String expectedTime;

	@Column(name = "notificationType")
	private String notificationType;

	@Column(name = "notificationValue")
	private String notificationValue;

	@Column(name = "countrycode")
	private String countryCode;

	@Column(name = "owner_userid")
	private long userId;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "dosage_info_id", referencedColumnName = "patient_dosage_info_id")
	private DosageInfo dosageInfo;
}
