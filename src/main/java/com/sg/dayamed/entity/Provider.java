package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "dayamed_provider")
public class Provider implements Serializable {

	private static final long serialVersionUID = 8156039239899460826L;

	@Id
	@Column(name = "provider_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "provider_specilization_on")
	private String specialization;

	@Column(name = "licence")
	private String licence;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	//@JsonBackReference(value = "provider-userDetails")
	private UserDetails userDetails;

	//@JoinTable(name = "provider_caregiver_map", joinColumns = @JoinColumn(name = "provider_id"), inverseJoinColumns
	// = @JoinColumn(name = "caregiver_id"))
	/*@JsonIgnore
	@ManyToMany(mappedBy = "providers")
	private Set<Caregiver> caregivers;*/
	
	/*public Set<Caregiver> getCaregivers() {
		return caregivers;
	}

	public void setCaregivers(Set<Caregiver> caregivers) {
		this.caregivers = caregivers;
	}*/

	@JsonIgnore
	@ManyToMany(mappedBy = "providers")
	private Set<Patient> patients;

	public Set<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Set<Patient> patients) {
		this.patients = patients;
	}

	public String getSpecialization() {
		return specialization;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
}
