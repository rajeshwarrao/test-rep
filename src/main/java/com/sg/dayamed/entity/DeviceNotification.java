package com.sg.dayamed.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "deviceinfo_notification")
public class DeviceNotification implements Serializable {

	private static final long serialVersionUID = -23636055638402210L;

	@Id
	@Column(name = "deviceinfo_notification_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "expectedTime")
	private String expectedTime;

	@Column(name = "notificationType")
	private String notificationType;

	@Column(name = "notificationValue")
	private String notificationValue;

	@Column(name = "countrycode")
	private String countryCode;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Column(name = "greater_than_reading1")
	private String greaterThanReading1;

	@Column(name = "greater_than_reading2")
	private String greaterThanReading2;

	@Column(name = "less_than_reading1")
	private String lessThanReading1;

	@Column(name = "less_than_reading2")
	private String lessThanReading2;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "device_infoid", referencedColumnName = "device_info_id")
	private DeviceInfo deviceInfo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGreaterThanReading2() {
		return greaterThanReading2;
	}

	public void setGreaterThanReading2(String greaterThanReading2) {
		this.greaterThanReading2 = greaterThanReading2;
	}

	public String getLessThanReading2() {
		return lessThanReading2;
	}

	public void setLessThanReading2(String lessThanReading2) {
		this.lessThanReading2 = lessThanReading2;
	}

	public String getExpectedTime() {
		return expectedTime;
	}

	public void setExpectedTime(String expectedTime) {
		this.expectedTime = expectedTime;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getNotificationValue() {
		return notificationValue;
	}

	public void setNotificationValue(String notificationValue) {
		this.notificationValue = notificationValue;
	}

	public String getGreaterThanReading1() {
		return greaterThanReading1;
	}

	public void setGreaterThanReading1(String greaterThanReading1) {
		this.greaterThanReading1 = greaterThanReading1;
	}

	public String getLessThanReading1() {
		return lessThanReading1;
	}

	public void setLessThanReading1(String lessThanReading1) {
		this.lessThanReading1 = lessThanReading1;
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

}

