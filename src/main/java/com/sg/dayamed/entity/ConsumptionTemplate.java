package com.sg.dayamed.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "consumption_template")
public class ConsumptionTemplate implements Serializable {

	private static final long serialVersionUID = 7430231276832363865L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "frequency_id")
	private Long frequencyId;

	@Column(name = "frequency_name")
	private String frequencyName;

	@Column(name = "consumptions_key")
	private String consumptionsKey;

	@Column(name = "consumptions_desc")
	private String consumptionsDesc;

	@Column(name = "consumptions_id")
	private Long consumptionsId;

	@Column(name = "consumptions_times")
	private String consumptionsTimes;

}
