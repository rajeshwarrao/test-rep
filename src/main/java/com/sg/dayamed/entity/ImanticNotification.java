package com.sg.dayamed.entity;

import com.sg.dayamed.pojo.SuperClass;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "imantic_notification")
public class ImanticNotification extends SuperClass implements Serializable {

	private static final long serialVersionUID = 6848107577053459178L;

	@Id
	@Column(name = "imanticnotification_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "message")
	private String message;

	@Column(name = "date")
	private Date date;

	@ManyToOne(cascade = CascadeType.MERGE)
	private UserDetails belongingUserDetails;

	@ElementCollection
	@CollectionTable(name = "imanticnotification_touser", joinColumns = @JoinColumn(name = "Imanticnotification_id"))
	@Column(name = "touser")
	private List<Long> toUserList = new ArrayList<Long>();
	
	
	/*@ElementCollection
	@CollectionTable(name = "adherence_consumptionstatus", joinColumns = @JoinColumn(name = "Adherence_id"))
	@Column(name = "consumptionstatus")
	private List<String> consumptionStatus = new ArrayList<String>();*/

	public long getId() {
		return id;
	}

	public List<Long> getToUserList() {
		return toUserList;
	}

	public void setToUserList(List<Long> toUserList) {
		this.toUserList = toUserList;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public UserDetails getBelongingUserDetails() {
		return belongingUserDetails;
	}

	public void setBelongingUserDetails(UserDetails belongingUserDetails) {
		this.belongingUserDetails = belongingUserDetails;
	}

}
