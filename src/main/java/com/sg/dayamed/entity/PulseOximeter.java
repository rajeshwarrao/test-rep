package com.sg.dayamed.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "pulse_oximeter")
public class PulseOximeter implements Serializable {

	private static final long serialVersionUID = -6492296139080898573L;

	@Id
	@Column(name = "pulse_oximeter_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "sp02")
	private String sp02;

	@Column(name = "pulse")
	private int pulse;

	@Column(name = "taken_time")
	private LocalDateTime takenTime;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Patient patient;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSp02() {
		return sp02;
	}

	public void setSp02(String sp02) {
		this.sp02 = sp02;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public LocalDateTime getTakenTime() {
		return takenTime;
	}

	public void setTakenTime(LocalDateTime takenTime) {
		this.takenTime = takenTime;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}
