package com.sg.dayamed.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "consentdocument")
public class ConsentDocument implements Serializable {

	private static final long serialVersionUID = -1452928506236650783L;

	@Id
	@Column(name = "document_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Lob
	@Column(name = "docbody")
	private String docBody;

	@Column(name = "publicationdate")
	private LocalDateTime publicationDate;

	public LocalDateTime getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(LocalDateTime publicationDate) {
		this.publicationDate = publicationDate;
	}

	@Column(name = "version", nullable = false)
	private String version;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDocBody() {
		return docBody;
	}

	public void setDocBody(String docBody) {
		this.docBody = docBody;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
