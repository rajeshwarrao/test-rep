package com.sg.dayamed.entity;

import com.sg.dayamed.pojo.VidoeCallSignatureModel;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "notification")
public class Notification implements Serializable {

	private static final long serialVersionUID = -7700805073537871772L;

	@Id
	@Column(name = "notification_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "message")
	private String message;

	@Column(name = "notification_type")
	private String type;

	@Column(name = "notification_title")
	private String title;

	@ManyToOne(cascade = CascadeType.MERGE)
	private UserDetails belongigngUserDetails;

	@JsonBackReference(value = "notificationreceiveduser-notification")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "notification")
	public List<NotificationReceivedUser> notificationReceivedUserList = new ArrayList<>();

	@Column(name = "time")
	private Date time;

	@Column(name = "roomname")
	private String roomName;

	@Column(name = "deleteflag")
	private boolean deleteflag;

	@Column(name = "meeting_duration")
	private Long meetingDuration;
	
	/*@ManyToOne(cascade = CascadeType.MERGE)
	private UserDetails userDetails;*/

	@Transient
	private VidoeCallSignatureModel vidoeCallSignatureModel;

	public VidoeCallSignatureModel getVidoeCallSignatureModel() {
		return vidoeCallSignatureModel;
	}

	public void setVidoeCallSignatureModel(VidoeCallSignatureModel vidoeCallSignatureModel) {
		this.vidoeCallSignatureModel = vidoeCallSignatureModel;
	}

	public Long getMeetingDuration() {
		return meetingDuration;
	}

	public void setMeetingDuration(Long meetingDuration) {
		this.meetingDuration = meetingDuration;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<NotificationReceivedUser> getNotificationReceivedUserList() {
		return notificationReceivedUserList;
	}

	public void setNotificationReceivedUserList(List<NotificationReceivedUser> notificationReceivedUserList) {
		this.notificationReceivedUserList = notificationReceivedUserList;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public boolean isDeleteflag() {
		return deleteflag;
	}

	public void setDeleteflag(boolean deleteflag) {
		this.deleteflag = deleteflag;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UserDetails getBelongigngUserDetails() {
		return belongigngUserDetails;
	}

	public void setBelongigngUserDetails(UserDetails belongigngUserDetails) {
		this.belongigngUserDetails = belongigngUserDetails;
	}

}
