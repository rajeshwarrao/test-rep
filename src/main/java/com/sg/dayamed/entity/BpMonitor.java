package com.sg.dayamed.entity;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "bp_monitor")
public class BpMonitor implements Serializable {

	private static final long serialVersionUID = 1671943747440078262L;

	@Id
	@Column(name = "bp_monitor_id ")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "systolic_pressure_reading_in_mmHg")
	private String systolicpressureValue;

	@Column(name = "diastolic_pressure_reading_in_mmHg")
	private String diastolicpressureValue;

	@Column(name = "prescribed_time")
	private LocalDateTime prescribedTime;

	@Column(name = "observed_time")
	private LocalDateTime observedTime;

	@ElementCollection
	@CollectionTable(name = "bp_consumptionstatus", joinColumns = @JoinColumn(name = "bp_id"))
	@Column(name = "consumptionstatus")
	private List<String> consumptionStatus = new LinkedList<String>();

	@Column(name = "location")
	private ArrayList<String> location;

	@Column(name = "prescriptionid")
	private long prescriptionID;

	@Column(name = "deviceInfoId")
	private String deviceInfoId;

	@ManyToOne(cascade = CascadeType.REFRESH)
	private Patient patient;

	public LocalDateTime getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(LocalDateTime prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getSystolicpressureValue() {
		return systolicpressureValue;
	}

	public void setSystolicpressureValue(String systolicpressureValue) {
		this.systolicpressureValue = systolicpressureValue;
	}

	public String getDiastolicpressureValue() {
		return diastolicpressureValue;
	}

	public void setDiastolicpressureValue(String diastolicpressureValue) {
		this.diastolicpressureValue = diastolicpressureValue;
	}

	public LocalDateTime getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(LocalDateTime observedTime) {
		this.observedTime = observedTime;
	}

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

	public String getDeviceInfoId() {
		return deviceInfoId;
	}

	public void setDeviceInfoId(String deviceInfoId) {
		this.deviceInfoId = deviceInfoId;
	}

}
