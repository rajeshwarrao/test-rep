package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "patient_prescription")
public class Prescription implements Serializable {

	private static final long serialVersionUID = -8125336984802429510L;

	@Id
	@Column(name = "patient_prescription_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/*@Column(name = "patient_illness_description", length = 5000)
	private String problem;*/
	@Column(name = "patient_illness_diagnosis", length = 5000)
	private String solution;
	/*@Column(name = "patient_prescription_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;*/

	@Column(name = "updated_date")
	private LocalDateTime updatedDate;//updatedDate

	@Column(name = "creted_date")
	private LocalDateTime cretedDate;

	@Column(name = "start_date")
	private LocalDateTime startDate;

	@Column(name = "end_date")
	private LocalDateTime endDate;

	@Column(name = "provider_comment")
	private String comment;

	@Column(name = "expected_adherence")
	private String expectedAdherence;

	@Transient
	private String disease;

	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "status", referencedColumnName = "prescription_status_id")
	private PrescriptionStatus prescriptionStatus;
	
	/*@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "diagnosis", referencedColumnName = "id")
	private Diagnosis diagnosis;*/

	@ManyToMany(cascade = {CascadeType.REFRESH})
	@JoinTable(name = "prescription_diagnosis_map", joinColumns = @JoinColumn(name = "prescription_id"),
	           inverseJoinColumns = @JoinColumn(name = "diagnosis_id"))
	private List<Diagnosis> DiagnosisList;

	public List<Diagnosis> getDiagnosisList() {
		return DiagnosisList;
	}

	public void setDiagnosisList(List<Diagnosis> diagnosisList) {
		DiagnosisList = diagnosisList;
	}

	@JsonManagedReference(value = "patient_Prescription")
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	public Patient patient;

	@JsonBackReference(value = "dosageInfo-Prescription")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "prescription")
	public List<DosageInfo> dosageInfoList = new ArrayList<>();

	@JsonBackReference(value = "dosagedevice_Prescription")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "prescription")
	private List<DosageDevice> dosageDevices = new ArrayList<>();

	@JsonBackReference(value = "deviceinfo_Prescription")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "prescription")
	private List<DeviceInfo> deviceInfoList = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "prescription")
	private List<ConsumerGoodInfo> commodityInfoList;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "prescription")
	public List<DiseaseInfo> diseaseInfoList = new ArrayList<>();

	@Column(name = "can_patient_modify", columnDefinition = "boolean default false")
	private boolean canPatientModify;

	public boolean isCanPatientModify() {
		return canPatientModify;
	}

	public void setCanPatientModify(boolean canPatientModify) {
		this.canPatientModify = canPatientModify;
	}

	public List<DiseaseInfo> getDiseaseInfoList() {
		return diseaseInfoList;
	}

	public void setDiseaseInfoList(List<DiseaseInfo> diseaseInfoList) {
		this.diseaseInfoList = diseaseInfoList;
	}

	public long getId() {
		return id;
	}

	public List<DosageDevice> getDosageDevices() {
		return dosageDevices;
	}

	public void setDosageDevices(List<DosageDevice> dosageDevices) {
		this.dosageDevices = dosageDevices;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<DosageInfo> getDosageInfoList() {
		return dosageInfoList;
	}

	public void setDosageInfoList(List<DosageInfo> dosageInfoList) {
		this.dosageInfoList = dosageInfoList;
	}

	/*public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}*/

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public PrescriptionStatus getPrescriptionStatus() {
		return prescriptionStatus;
	}

	public void setPrescriptionStatus(PrescriptionStatus prescriptionStatus) {
		this.prescriptionStatus = prescriptionStatus;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	/*public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}*/

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public LocalDateTime getCretedDate() {
		return cretedDate;
	}

	public void setCretedDate(LocalDateTime cretedDate) {
		this.cretedDate = cretedDate;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getExpectedAdherence() {
		return expectedAdherence;
	}

	public void setExpectedAdherence(String expectedAdherence) {
		this.expectedAdherence = expectedAdherence;
	}

	public List<DeviceInfo> getDeviceInfoList() {
		return deviceInfoList;
	}

	public void setDeviceInfoList(List<DeviceInfo> deviceInfoList) {
		this.deviceInfoList = deviceInfoList;
	}

	public List<ConsumerGoodInfo> getCommodityInfoList() {
		return commodityInfoList;
	}

	public void setCommodityInfoList(List<ConsumerGoodInfo> commodityInfoList) {
		this.commodityInfoList = commodityInfoList;
	}

}
