package com.sg.dayamed.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "user_options")
public class UserOptions implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	@JsonManagedReference(value = "useroptions-userDetails")
	private UserDetails userDetails;

	@Column(name = "gs_pharmacist_enable")
	private Integer gsPharmacistEnable;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public Integer getGsPharmacistEnable() {
		return gsPharmacistEnable;
	}

	public void setGsPharmacistEnable(Integer gsPharmacistEnable) {
		this.gsPharmacistEnable = gsPharmacistEnable;
	}

}
