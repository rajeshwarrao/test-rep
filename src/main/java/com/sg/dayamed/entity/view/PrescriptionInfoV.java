package com.sg.dayamed.entity.view;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "prescription_info_v")
public class PrescriptionInfoV {

	@Id
	private String id;

	@Column(name = "prescription_id")
	private Long prescriptionId;

	@Column(name = "provider_comment")
	private String providerComment;

	@Column(name = "prescription_updated_date")
	private LocalDateTime presUpdatedDate;

	@Column(name = "expected_adherence")
	private String expectedAdherence;

	@Column(name = "status_id")
	private Integer statusId;

	@Column(name = "status")
	private String status;

	@Column(name = "pres_created_date")
	private LocalDateTime presCreatedDate;

	@Column(name = "pres_start_date")
	private LocalDateTime presStartDate;

	@Column(name = "pres_end_date")
	private LocalDateTime presEndDate;

	@Column(name = "disease_info_id")
	private Long diseaseInfoId;

	@Column(name = "disease_id")
	private Long diseaseId;

	@Column(name = "disease")
	private String disease;

	@Column(name = "disease_description")
	private String diseaseDescription;

	@Column(name = "disease_delete_flag")
	private Boolean diseaseDeleteFlag;

	@Column(name = "diagnosis_id")
	private Long diagnosisId;

	@Column(name = "diagnosis")
	private String diagnosis;

	@Column(name = "patient_id")
	private Long patientId;

	@Column(name = "current_adherence")
	private String currentAdherence;

	@Column(name = "patient_device_id")
	private String patientDeviceId;

	@Column(name = "emoji")
	private String emoji;

	@Column(name = "predicted_adherence")
	private String predictedAdherence;

	@Column(name = "projected_adherence")
	private String projectedAdherence;

	@Column(name = "patient_status")
	private Integer patientStatus;

	@Column(name = "patient_user_id")
	private Long patientUserId;

	@Column(name = "provider_id")
	private Long providerId;

	@Column(name = "caregiver_id")
	private Long caregiverId;

	@Column(name = "pharmacist_id")
	private Long pharmacistId;

	@Column(name = "provider_user_id")
	private Long providerUserId;

	@Column(name = "caregiver_user_id")
	private Long caregiverUserId;

	@Column(name = "pharmacist_user_id")
	private Long pharmacistUserId;
}
