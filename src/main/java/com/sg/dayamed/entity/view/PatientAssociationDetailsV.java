package com.sg.dayamed.entity.view;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Created by Eresh Gorantla on 12/May/2019
 **/
@Entity
@Table(name = "patient_association_details_v")
@Getter
@Setter
@NoArgsConstructor
public class PatientAssociationDetailsV {

	@Id
	private String id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "patient_id")
	private Long patientId;

	@Column(name = "provider_id")
	private Long providerId;

	@Column(name = "caregiver_id")
	private Long caregiverId;

	@Column(name = "pharmacist_id")
	private Long pharmacistId;

	@Column(name = "age")
	private Integer age;

	@Column(name = "app_version")
	private String appVersion;

	@Column(name = "city", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "city",
	                   read = "CAST(AES_DECRYPT(city, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String city;

	@Column(name = "country", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "country", read = "CAST(AES_DECRYPT(country, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String country;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "user_created_on")
	private LocalDateTime userCreatedOn;

	@Column(name = "user_device_type")
	private String userDeviceType;

	@Column(name = "email_id", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "email_id", read = "CAST(AES_DECRYPT(email_id, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String emailId;

	@Column(name = "email_flag")
	private Boolean emailFlag;

	@Column(name = "first_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "first_name", read = "CAST(AES_DECRYPT(first_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String firstName;

	@Column(name = "gender")
	private String gender;

	@Column(name = "user_image_url")
	private String userImageUrl;

	@Column(name = "imatic_user_id")
	private String imanticUserId;

	@Column(name = "last_active_time")
	private LocalDateTime lastActiveTime;

	@Column(name = "last_login_time")
	private LocalDateTime lastLoginTime;

	@Column(name = "last_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "last_name",  read = "CAST(AES_DECRYPT(last_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String lastName;

	@Column(name = "location", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "location", read = "CAST(AES_DECRYPT(location, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String location;

	@Column(name = "middle_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "middle_name", read = "CAST(AES_DECRYPT(middle_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String middleName;

	@Column(name = "mobile_number", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "mobile_number", read = "CAST(AES_DECRYPT(mobile_number, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String mobileNumber;

	@Column(name = "os_version")
	private String osVersion;

	@Column(name = "pwd_change_token")
	private String pwdChangeToken;

	@Column(name = "user_password")
	private String userPassword;

	@Column(name = "pwd_token_expire_date")
	private LocalDateTime pwdTokenExpireDate;

	@Column(name = "race")
	private String race;

	@Column(name = "sms_flag")
	private Boolean smsFlag;

	@Column(name = "state", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "state", read = "CAST(AES_DECRYPT(state, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String state;

	@Column(name = "token")
	private String token;

	@Column(name = "user_updated_on")
	private LocalDateTime userUpdatedOn;

	@Column(name = "user_zoned_id")
	private String userZoneId;

	@Column(name = "voip_token")
	private String voipToken;

	@Column(name = "zip_code", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "zip_code",  read = "CAST(AES_DECRYPT(zip_code, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String zipCode;

	@Column(name = "current_adherence")
	private String currentAdherence;

	@Column(name = "patient_device_id")
	private String patientDeviceId;

	@Column(name = "emoji")
	private String emoji;

	@Column(name = "predicted_adherence")
	private String predictedAdherence;

	@Column(name = "projected_adherence")
	private String projectedAdherence;

	@Column(name = "patient_status")
	private Integer patientStatus;

	@Column(name = "provider_user_id")
	private Long providerUserId;

	@Column(name = "provider_first_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "provider_first_name",  read = "CAST(AES_DECRYPT(provider_first_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String providerFirstName;

	@Column(name = "provider_last_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "provider_last_name",  read = "CAST(AES_DECRYPT(provider_last_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String providerLastName;

	@Column(name = "provider_middle_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "provider_middle_name",  read = "CAST(AES_DECRYPT(provider_middle_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String providerMiddleName;

	@Column(name = "provider_email_id", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "provider_email_id",  read = "CAST(AES_DECRYPT(provider_email_id, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String providerEmailId;

	@Column(name = "provider_mobile_number", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "provider_mobile_number",  read = "CAST(AES_DECRYPT(provider_mobile_number, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String providerMobileNumber;

	@Column(name = "provider_license")
	private String providerLicense;

	@Column(name = "provider_specialization")
	private String providerSpecialization;

	@Column(name = "caregiver_user_id")
	private Long caregiverUserId;

	@Column(name = "caregiver_first_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "caregiver_first_name",  read = "CAST(AES_DECRYPT(caregiver_first_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String caregiverFirstName;

	@Column(name = "caregiver_last_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "caregiver_last_name",  read = "CAST(AES_DECRYPT(caregiver_last_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String caregiverLastName;

	@Column(name = "caregiver_middle_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "caregiver_middle_name",  read = "CAST(AES_DECRYPT(caregiver_middle_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String caregiverMiddleName;

	@Column(name = "caregiver_email_id", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "caregiver_email_id",  read = "CAST(AES_DECRYPT(caregiver_email_id, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String caregiverEmailId;

	@Column(name = "caregiver_mobile_number", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "caregiver_mobile_number",  read = "CAST(AES_DECRYPT(caregiver_mobile_number, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String caregiverMobileNumber;

	@Column(name = "caregiverAdminFlag")
	private Boolean caregiverAdminFlag;

	@Column(name = "pharmacist_user_id")
	private Long pharmacistUserId;

	@Column(name = "pharmacist_first_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "pharmacist_first_name",  read = "CAST(AES_DECRYPT(pharmacist_first_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String pharmacistFirstName;

	@Column(name = "pharmacist_last_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "pharmacist_last_name",  read = "CAST(AES_DECRYPT(pharmacist_last_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String pharmacistLastName;

	@Column(name = "pharmacist_middle_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "pharmacist_middle_name",  read = "CAST(AES_DECRYPT(pharmacist_middle_name, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String pharmacistMiddleName;

	@Column(name = "pharmacist_email_id", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "pharmacist_email_id",  read = "CAST(AES_DECRYPT(pharmacist_email_id, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String pharmacistEmailId;

	@Column(name = "pharmacist_mobile_number", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "pharmacist_mobile_number",  read = "CAST(AES_DECRYPT(pharmacist_mobile_number, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String pharmacistMobileNumber;

	@Column(name = "pharmacy_company")
	private String pharmacyCompany;
}
