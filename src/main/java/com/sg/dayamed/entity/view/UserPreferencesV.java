package com.sg.dayamed.entity.view;

import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.util.BeanUtil;
import com.sg.dayamed.util.StringCryptoConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@Entity
@Table(name = "user_preferences_v")
@Getter
@Setter
@NoArgsConstructor
public class UserPreferencesV {

	@Id
	private String id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "email_id", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "email_id", read = "CAST(AES_DECRYPT(email_id, DB_ENCRYPTION_PASSWORD()) AS CHAR" +
			"(250))")
	private String emailId;

	@Column(name = "first_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "first_name", read = "CAST(AES_DECRYPT(first_name, DB_ENCRYPTION_PASSWORD()) AS " +
			"CHAR(250))")
	private String firstName;

	@Column(name = "middle_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "middle_name", read = "CAST(AES_DECRYPT(middle_name, DB_ENCRYPTION_PASSWORD()) AS " +
			"CHAR(250))")
	private String middleName;

	@Column(name = "last_name", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "last_name", read = "CAST(AES_DECRYPT(last_name, DB_ENCRYPTION_PASSWORD()) AS CHAR" +
			"(250))")
	private String lastName;

	@Column(name = "age")
	private Integer age;

	@Column(name = "city", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "city",
	                   read = "CAST(AES_DECRYPT(city, DB_ENCRYPTION_PASSWORD()) AS CHAR(250))")
	private String city;

	@Column(name = "country", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "country", read = "CAST(AES_DECRYPT(country, DB_ENCRYPTION_PASSWORD()) AS CHAR" +
			"(250))")
	private String country;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "user_created_date")
	private LocalDateTime userCreatedDate;

	@Column(name = "user_device_type")
	private String userDeviceType;

	@Column(name = "email_flag")
	private Boolean emailFlag;

	@Column(name = "sms_flag")
	private Boolean smsFlag;

	@Column(name = "gender")
	private String gender;

	@Column(name = "imantic_useer_id")
	private String imaticUserId;

	@Column(name = "role_id")
	private Long roleId;

	@Column(name = "role")
	private String role;

	@Column(name = "user_updated_date")
	private LocalDateTime userUpdatedDate;

	@Column(name = "last_active_time")
	private LocalDateTime lastActiveTime;

	@Column(name = "last_login_time")
	private LocalDateTime lastLoginTime;

	@Column(name = "mobile_number", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "mobile_number", read = "CAST(AES_DECRYPT(mobile_number, DB_ENCRYPTION_PASSWORD()) " +
			"AS CHAR(250))")
	private String mobileNumber;

	@Column(name = "user_token")
	private String userToken;

	@Column(name = "voip_token")
	private String voipToken;

	@Column(name = "race")
	private String race;

	@Column(name = "zip_code", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "zip_code", read = "CAST(AES_DECRYPT(zip_code, DB_ENCRYPTION_PASSWORD()) AS CHAR" +
			"(250))")
	private String zipCode;

	@Column(name = "zone_id")
	private String zoneId;

	@Column(name = "user_pref_id")
	private Long userPrefId;

	@Column(name = "user_pref")
	private String userPref;

	@Column(name = "patient_id")
	private Long patientId;

	@Column(name = "provider_id")
	private Long providerId;

	@Column(name = "caregiver_id")
	private Long caregiverId;

	@Column(name = "admin_caregiver")
	private Boolean adminCaregiver;

	@Column(name = "pharmacist_id")
	private Long pharmacistId;

	@Column(name = "password")
	private String password;

	@Column(name = "app_version")
	private String appVersion;

	@Column(name = "imatic_user_id")
	private String imanticUserId;

	@Column(name = "os_version")
	private String osVersion;

	@Column(name = "location", columnDefinition = "VARBINARY")
	@ColumnTransformer(forColumn = "location", read = "CAST(AES_DECRYPT(location, DB_ENCRYPTION_PASSWORD()) AS CHAR" +
			"(250))")
	private String location;

	@Column(name = "pwd_change_token")
	private String pwdChangeToken;

	@Column(name = "pwd_token_expire_date")
	private LocalDateTime pwdTokenExpireDate;

	@Column(name = "state")
	@Convert(converter = StringCryptoConverter.class)
	private String state;

	@Column(name = "token")
	private String token;

	@Column(name = "user_created_on")
	private LocalDateTime userCreatedOn;

	@Column(name = "user_image_url")
	private String userImageUrl;

	@Column(name = "user_updated_on")
	private LocalDateTime userUpdatedOn;

	@Column(name = "user_zone_id")
	private String userZoneId;

	@Column(name = "pharmacist_enable")
	private Boolean pharmacistEnable;

	@Transient
	private UserPreferenceModel userPreference;

	@Transient
	private ObjectMapper objectMapper;

	public ObjectMapper getObjectMapper() {
		if (this.objectMapper == null) {
			return BeanUtil.getBean(ObjectMapper.class);
		}
		return this.objectMapper;
	}

	public UserPreferenceModel getUserPreference() {
		try {
			if (StringUtils.isNotBlank(this.userPref)) {
				return getObjectMapper().readValue(this.userPref, UserPreferenceModel.class);
			}
		} catch (Exception e) {

		}
		return null;
	}
}
