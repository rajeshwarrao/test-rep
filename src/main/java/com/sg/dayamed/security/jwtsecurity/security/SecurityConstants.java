package com.sg.dayamed.security.jwtsecurity.security;

public class SecurityConstants {

	public static final String SECRET = "SenacaDayamed";

	public static final long EXPIRATION_TIME = 864_000_0; // 1 day

	public static final long VIDEO_CALL_RECEVIER_USER_NOTIFCATION_EXPIRATION_TIME = 60; // 1 day

	//public static final long EXPIRATION_TIME = 180000; // 3 min
	public static final String TOKEN_PREFIX = "Bearer ";

	public static final String HEADER_STRING = "Authorization";

	public static final String LOGIN_URL = "/loginuser";

	public static final String UTILITY_URL = "/utility/**";

	public static final String CORS_URL = "Origin";

	public static final long MOBILE_USER_EXPIRATION_TIME = 864_000_00;              //345600000; // 4 days

	public static final long TEMP_JWT_TOKEN_EXPIRATION_TIME = 5000;

}
