package com.sg.dayamed.security.jwtsecurity.model;

public class JwtUser {

	private String userName;

	private long userId;

	private String password;

	private String role;

	private String deviceId;

	private String typeOfDevice;

	private String userEmailId;

	private boolean gsPharmacistEnable;

	public boolean isGsPharmacistEnable() {
		return gsPharmacistEnable;
	}

	public void setGsPharmacistEnable(boolean gsPharmacistEnable) {
		this.gsPharmacistEnable = gsPharmacistEnable;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}

	public String getRole() {
		return role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTypeOfDevice() {
		return typeOfDevice;
	}

	public void setTypeOfDevice(String typeOfDevice) {
		this.typeOfDevice = typeOfDevice;
	}

}
