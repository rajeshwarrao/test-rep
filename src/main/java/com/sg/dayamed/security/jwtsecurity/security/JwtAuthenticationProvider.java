package com.sg.dayamed.security.jwtsecurity.security;

import com.sg.dayamed.security.jwtsecurity.model.JwtAuthenticationToken;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private JwtValidator validator;

	private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
	                                              UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
			throws AuthenticationException {

	}

	@Override
	protected UserDetails retrieveUser(String username,
	                                   UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
			throws AuthenticationException {

		JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) usernamePasswordAuthenticationToken;
		String token = jwtAuthenticationToken.getToken();

		JwtUser jwtUser = null;
		try {
			jwtUser = validator.validate(token);
		} catch (UsernameNotFoundException e) {
			logger.error("Jwt token expired");
			throw new UsernameNotFoundException("Token expired");
		}
		if (jwtUser == null) {
			logger.error("Jwt token is incorrect");
			throw new UsernameNotFoundException("JWT Token is incorrect");
		}

		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList(jwtUser.getRole());
		return new JwtUserDetails(jwtUser.getUserName(), jwtUser.getRole(), jwtUser.getUserId(), token,
		                          grantedAuthorities, jwtUser.getDeviceId(), jwtUser.getTypeOfDevice(),
		                          jwtUser.getUserEmailId(), jwtUser.isGsPharmacistEnable());
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return (JwtAuthenticationToken.class.isAssignableFrom(aClass));
	}
}
