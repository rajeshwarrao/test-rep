package com.sg.dayamed.security.jwtsecurity.security;

import com.sg.dayamed.security.jwtsecurity.model.JwtAuthenticationToken;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {

	private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

	public JwtAuthenticationTokenFilter() {
		super("/rest/**");
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
	                                            HttpServletResponse httpServletResponse)
			throws AuthenticationException, IOException, ServletException {

		String header = httpServletRequest.getHeader(SecurityConstants.HEADER_STRING);

		if (httpServletRequest.getMethod()
		                      .equals("OPTIONS")) {
			httpServletResponse.setStatus(200);
			httpServletResponse.setContentType("application/json");
			String corsUrl = (String) httpServletRequest.getHeader(SecurityConstants.CORS_URL);
			if (corsUrl == null || corsUrl.trim()
			                              .equals("")) {
				corsUrl = "http://192.168.5.28:4200";
			}
			httpServletResponse.setHeader("Access-Control-Allow-Origin", corsUrl);
			httpServletResponse.setHeader("Access-Control-Allow-Methods", "OPTIONS,HEAD,GET,PUT,POST,DELETE,PATCH");
			//httpServletResponse.setHeader("Access-Control-Allow-Headers", "authorization, notifiUserID");
			httpServletResponse.setHeader("Access-Control-Allow-Headers",
			                              "Authorization, x-xsrf-token, Access-Control-Allow-Headers, Origin, " +
					                              "Accept," +
					                              " " +
					                              "X-Requested-With, " +
					                              "Content-Type, Access-Control-Request-Method, " +
					                              "Access-Control-Request-Headers, notifiUserID");
			httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");

			return null;
		}

       /* if (header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
        	logger.error("JWT Token is missing");
            throw new UsernameNotFoundException("JWT Token is missing");
        }*/

		if (header == null || header.isEmpty() || "".equals(header)) {
			logger.error("JWT Token is missing");
			throw new UsernameNotFoundException("JWT Token is missing");
		}

		//String authenticationToken = header.substring(6);
		String authenticationToken = header.trim();

		JwtAuthenticationToken token = new JwtAuthenticationToken(authenticationToken);
		return getAuthenticationManager().authenticate(token);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
	                                        FilterChain chain,
	                                        Authentication authResult) throws IOException, ServletException {
		super.successfulAuthentication(request, response, chain, authResult);
		JwtUserDetails jwtUserDetails = (JwtUserDetails) authResult.getPrincipal();
		UserDetails userDetails = jwtUserDetails;
		UsernamePasswordAuthenticationToken authentication =
				new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
		SecurityContextHolder.getContext()
		                     .setAuthentication(authentication);
		chain.doFilter(request, response);
	}
}

