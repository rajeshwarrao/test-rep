package com.sg.dayamed.security.jwtsecurity.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class JwtUserDetails implements UserDetails {

	private String userName;

	private String token;

	private String role;

	private Long userId;

	private String deviceId;

	private String typeOfDevice;

	private String userEmailId;

	private Collection<? extends GrantedAuthority> authorities;

	private boolean gsPharmacistEnable;

	public JwtUserDetails(String userName, String role, Long userid, String token,
	                      List<GrantedAuthority> grantedAuthorities, String deviceId, String typeOfDevice,
	                      String userEmailId,
	                      boolean gsPharmacistEnable) {

		this.userName = userName;
		this.role = role;
		this.userId = userid;
		this.token = token;
		this.authorities = grantedAuthorities;
		this.deviceId = deviceId;
		this.typeOfDevice = typeOfDevice;
		this.userEmailId = userEmailId;
		this.gsPharmacistEnable = gsPharmacistEnable;
	}

	public boolean isGsPharmacistEnable() {
		return gsPharmacistEnable;
	}

	public void setGsPharmacistEnable(boolean gsPharmacistEnable) {
		this.gsPharmacistEnable = gsPharmacistEnable;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getUserName() {
		return userName;
	}

	public String getToken() {
		return token;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTypeOfDevice() {
		return typeOfDevice;
	}

	public void setTypeOfDevice(String typeOfDevice) {
		this.typeOfDevice = typeOfDevice;
	}

}
