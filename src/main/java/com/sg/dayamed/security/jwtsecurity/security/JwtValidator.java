package com.sg.dayamed.security.jwtsecurity.security;

import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.service.RediseService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class JwtValidator {

	private String secret = SecurityConstants.SECRET;

	@Autowired
	RediseService rediseService;

	public JwtUser validate(String token) {

		JwtUser jwtUser = null;
		try {
			jwtUser = rediseService.getDataInRedis(token, true);
			if (jwtUser == null) {
				throw new UsernameNotFoundException("Token expired");
			} else {
				Claims claims = Jwts.claims()
				                    .setSubject(jwtUser.getUserName());
				claims.put("userId", String.valueOf(jwtUser.getUserId()));
				claims.put("role", jwtUser.getRole()
				                          .toUpperCase());
				long expireTime = SecurityConstants.TEMP_JWT_TOKEN_EXPIRATION_TIME;
				Jwts.builder()
				    .setClaims(claims)
				    .setExpiration(new Date(System.currentTimeMillis() + expireTime))
				    .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET)
				    .compact();
			}
		} catch (Exception e) {
			log.error("Exception occurred when validating JWT token", e);
			throw new UsernameNotFoundException("Token expired");
		}
		return jwtUser;
	}
}
