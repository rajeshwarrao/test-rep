package com.sg.dayamed.security.jwtsecurity.security;

import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.service.RediseService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtGenerator {

	@Autowired
	RediseService rediseService;

	public String generate(JwtUser jwtUser) {
		Claims claims = Jwts.claims()
		                    .setSubject(jwtUser.getUserName());
		claims.put("userId", String.valueOf(jwtUser.getUserId()));
		claims.put("role", jwtUser.getRole()
		                          .toUpperCase());
		String token = Jwts.builder()
		                   .setClaims(claims)
		                   .setExpiration(new Date(
				                   System.currentTimeMillis() + SecurityConstants.TEMP_JWT_TOKEN_EXPIRATION_TIME))
		                   .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET)
		                   .compact();
		rediseService.saveDataInRedis(token, jwtUser);
		return token;
	}
}
