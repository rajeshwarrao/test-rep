package com.sg.dayamed.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.ProviderService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/rest/providers")
@Api(value = "provider")
public class ProviderController extends BaseRestApi {

	@Autowired
	ProviderRepository providerRepository;

	@Autowired
	ProviderService providerService;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	Utility utility;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	PatientService patientService;

	@Autowired
	ObjectMapper objectMapper;

	@PreAuthorize("hasAnyRole('PROVIDER','ADMIN')")
	@PostMapping(value = "/add", produces = "application/json", consumes = {"application/json", "multipart/form-data",
			"application/octet-stream"})
	public ResponseEntity<Object> createProvider(@RequestPart("providerInfo") String providerInfo,
	                                             @RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage)
			throws RestApiException {
		return inboundServiceCallGeneric(providerInfo, service -> {
			ProviderModel providerModel = objectMapper.readValue(providerInfo, ProviderModel.class);
			ProviderModel actualProviderModel = providerService.validateAddProvider(providerModel, profileImage);
			return ResponseEntity.ok(actualProviderModel);
		});
	}

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public ResponseEntity<Object> deleteProvider(@PathVariable(value = "id") Long id)
			throws RestApiException {
		return inboundServiceCallGeneric(id, service -> {
			providerService.deleteProviderById(id);
			return ResponseEntity.ok(new StringResponse(ApplicationConstants.SUCCESS));
		});
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping(produces = "application/json")

	public ResponseEntity<Object> listProvider() throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			List<ProviderModel> providerList = providerService.fetchProviders();
			return ResponseEntity.ok(providerList);
		});
	}

	@GetMapping(value = "/byuser/{userid}", produces = "application/json")
	@ApiOperation(value = "GetProviderByID", notes = "Returns getProvider Id")
	public ResponseEntity<Object> fetchProviderByUserId(@PathVariable(value = "userid") Long userid)
			throws RestApiException {
		return inboundServiceCallGeneric(userid, service -> {
			ProviderModel providerModel = providerService.fetchProviderByUserId(userid);
			return ResponseEntity.ok(providerModel);
		});
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	@ApiOperation(value = "GetProviderByID", notes = "Returns getProvider Id")
	public ResponseEntity<Object> fetchProviderById(@PathVariable(value = "id") Long providerid)
			throws RestApiException {
		return inboundServiceCallGeneric(providerid, service -> {
			ProviderModel providerModel = providerService.fetchProviderById(providerid);
			if (providerModel != null) {
				return ResponseEntity.ok(providerModel);
			}
			return new ResponseEntity<>(providerModel, HttpStatus.NOT_FOUND);
		});
	}

	@GetMapping(value = "/bypatient/{patientid}", produces = "application/json")
	@ApiOperation(value = "fetch ProvidersBy PatientId", notes = "Returns provider List")
	public ResponseEntity<Object> fetchProvidersByPatientId(@PathVariable(value = "patientid") Long patientid)
			throws RestApiException {
		return inboundServiceCallGeneric(patientid, service -> {
			List<ProviderModel> providerModelList = providerService.fetchProvidersByPatientId(patientid);
			return ResponseEntity.ok(providerModelList);
		});
	}
}

