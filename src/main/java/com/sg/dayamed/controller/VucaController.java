package com.sg.dayamed.controller;

import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.VucaService;
import com.sg.dayamed.util.StringResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/vuca")
@Api(value = "Vuca Operations")
public class VucaController extends BaseRestApi {

	@Autowired
	private VucaService vucaService;

	@GetMapping(value = "/medicinevideo/url/{ndccode}", produces = "application/json")
	@ApiOperation(value = "Fetch VUCA medicine video URL", notes = "Returns VUCA medicine video URL")
	public ResponseEntity<Object> fetchVucaMedicineVideoURL(@PathVariable(value = "ndccode") String ndc)
			throws RestApiException {
		return inboundServiceCallGeneric(ndc, service -> {
			final String vucaVideoURL = vucaService.fetchVucaVideoURL(ndc);
			return ResponseEntity.ok(new StringResponse(vucaVideoURL));
		});
	}
}
