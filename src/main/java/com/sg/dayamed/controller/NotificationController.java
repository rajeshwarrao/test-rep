package com.sg.dayamed.controller;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.helper.pojo.CaregiverDosageNotificationModel;
import com.sg.dayamed.pojo.APNSUpdateTokenRequestModel;
import com.sg.dayamed.pojo.CalendarNotificationModel;
import com.sg.dayamed.pojo.CalenderNotificationRequestModel;
import com.sg.dayamed.pojo.DosageNotificationModel;
import com.sg.dayamed.pojo.FcmNotificationRequestModel;
import com.sg.dayamed.pojo.NotificationModel;
import com.sg.dayamed.pojo.NotificationRequestModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.VideoNotificationRequestModel;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.repository.ImanticNotificationRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.DosageInfoService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.UtilityService;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@RestController
//@RequestMapping("/rest/notifications")
@Api(value = "Notification Operations")
public class NotificationController extends BaseRestApi {

	@Autowired
	NotificationService notificationService;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	ProviderRepository provoiderRepository;

	@Autowired
	ImanticNotificationRepository imanticNotificationRepository;

	@Autowired
	PatientService patientService;

	@Autowired
	Utility utility;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	UtilityService utilityService;

	@Autowired
	PrescriptionService prescriptionService;

	private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);

	@PostMapping(value = "/rest/notifications/add", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Add Notification", notes = "Returns Notification")
	public ResponseEntity<Object> addNotification(@RequestBody NotificationRequestModel notificationRequestModel)
			throws RestApiException {
		return inboundServiceCallGeneric(notificationRequestModel, service -> {
			NotificationModel notificationModel =
					notificationService.addNotificationFromController(notificationRequestModel);
			return ResponseEntity.ok(notificationModel);
		});
	}

	@GetMapping(value = "/rest/notifications/{type}", produces = "application/json")
	@ApiOperation(value = "Fetch Notification By Type", notes = "Returns Notifications by type")
	public ResponseEntity<Object> fetchNotificationByType(@PathVariable(value = "type") String type)
			throws RestApiException {
		return inboundServiceCallGeneric(type, service -> {
			List<NotificationModel> listNotificationModel =
					notificationService.fetchNotificationModelsByNotificationType(type);
			return ResponseEntity.ok(listNotificationModel);
		});
	}

	@GetMapping(value = "/rest/notifications/belonginguser/{userid}", produces = "application/json")
	@ApiOperation(value = "Fetch Notification By BelongingUserId", notes = "Returns Notifications by BelongingUserId")
	public ResponseEntity<Object> fetchNotificationByBelongingUserId(
			@PathVariable(value = "userid") Long userId) throws RestApiException {
		return inboundServiceCallGeneric(userId, service -> {
			List<Notification> notificationList = notificationService.fetchNotificationsByBelongingUserId(userId);
			List<NotificationModel> notificationModelList = new ArrayList<NotificationModel>();
			for (Notification notification : notificationList) {
				notificationModelList.add(utility.convertNotificationEntityToModel(notification));
			}
			return ResponseEntity.ok(notificationModelList);
		});
	}

	// delete Notification byid
	@DeleteMapping(value = "/rest/notifications/delete/{id}", produces = "application/json")
	@ApiOperation(value = "delete Notification by id", notes = "delete Notification")
	public ResponseEntity<Object> deleteNotifications(@PathVariable(value = "id") Long id) throws RestApiException {
		return inboundServiceCallGeneric(id, service -> {
			Notification notification = notificationService.fetchNotificationById(id);
			if (notification != null) {
				notification.setDeleteflag(true);
				notificationService.addWebNotification(notification);
				return new ResponseEntity<Object>("deleted", HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
			}
		});
	}

	@PostMapping(value = "/rest/utility/patient/notifications/byuserid/{id}", produces = "application/json", consumes =
			"application/json")
	@ApiOperation(value = "notificationsByPatientId", notes = "notificationsByPatientId")
	public ResponseEntity<Object> calenderNotificationsWithOutScheduledByPatientId(
			@PathVariable(value = "id") Long userId,
			@RequestBody CalenderNotificationRequestModel calenderNotificationRequestModel) throws RestApiException {
		return inboundServiceCallGeneric(calenderNotificationRequestModel, service -> {
			CalendarNotificationModel calendarNotificationModel =
					notificationService.calenderNotificationsWithOutScheduledByPatientId(userId,
					                                                                     calenderNotificationRequestModel);
			return ResponseEntity.ok(calendarNotificationModel);
		});
	}

	@PostMapping(value = "/rest/utility/patient/calendernotifications/byuserid/{id}", produces = "application/json",
	             consumes = "application/json")
	@ApiOperation(value = "calenderNotificationsByPatientId", notes = "calenderNotificationsByPatientId")
	public ResponseEntity<Object> calenderNotificationsByPatientId(@PathVariable(value = "id") Long userId,
	                                                               @RequestBody CalenderNotificationRequestModel calenderNotificationRequestModel)
			throws RestApiException {
		return inboundServiceCallGeneric(calenderNotificationRequestModel, service -> {
			CalendarNotificationModel calendarNotificationModel =
					notificationService.calenderNotificationsByPatientId(userId,
					                                                     calenderNotificationRequestModel);
			return ResponseEntity.ok(calendarNotificationModel);
		});
	}

	@PostMapping(value = "/rest/utility/sendvideonotification", produces = "application/json", consumes =
			"application" +
					"/json")
	@ApiOperation(value = "sendVideoNotification", notes = "Returns videoNotification")
	public ResponseEntity<Object> sendVideoNotificationByUserId(
			@RequestBody VideoNotificationRequestModel videoNotificationRequestModel) throws RestApiException {
		return inboundServiceCallGeneric(videoNotificationRequestModel, service -> {
			String status = notificationService.sendVideoNotificationByUserId(videoNotificationRequestModel);
			return ResponseEntity.ok(status);
		});
	}

	@PostMapping(value = "/rest/utility/fcmnotification", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "fcmNotification", notes = "Returns fcmNotification")
	public ResponseEntity<Object> sendFcmNotification(
			@RequestBody FcmNotificationRequestModel fcmNotificationRequestModel) throws RestApiException {
		return inboundServiceCallGeneric(fcmNotificationRequestModel, service -> {
			String status = notificationService.sendFcmNotification(fcmNotificationRequestModel);
			return ResponseEntity.ok(status);
		});
	}

	@PostMapping(value = "/rest/utility/updatetoken", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "update Apns Token", notes = "Returns ApnsToken")
	public ResponseEntity<Object> updateTokenInUserDetails(
			@RequestBody APNSUpdateTokenRequestModel aPNSUpdateTokenRequestModel)
			throws RestApiException {
		return inboundServiceCallGeneric(aPNSUpdateTokenRequestModel, service -> {
			UserDetailsModel userDetailsModel =
					notificationService.updateTokenInUserDetails(aPNSUpdateTokenRequestModel);
			return ResponseEntity.ok(userDetailsModel);
		});
	}

	@PostMapping(value = "/rest/caregivers/notification/add", produces = "application/json", consumes = "application" +
			"/json")
	@ApiOperation(value = "Add Caegiver DosageNotification", notes = "Returns Caegiver DosageNotification")
	public ResponseEntity<Object> addCaregiverNotifications(
			@RequestBody CaregiverDosageNotificationModel caregiverDosageNotificationModel) throws Exception {
		return inboundServiceCallGeneric(caregiverDosageNotificationModel, service -> {
			CaregiverDosageNotificationModel resCaregiverDosageNotificationModel =
					notificationService.addCaregiverNotifications(caregiverDosageNotificationModel);
			return ResponseEntity.ok(resCaregiverDosageNotificationModel);
		});
	}

	@GetMapping(value = "/rest/caregivers/dosagenotification/userid/{userid}/dosageinfo/{infoid}", produces = "application/json")
	@ApiOperation(value = "Fetch DosageNotificationsByCaregiverId", notes = "Returns " +
			"DosageNotificationsByCaregiverId ")
	public ResponseEntity<Object> fetchDosageNotificationsByCaregiverId(
			@PathVariable(value = "userid") Long caregiverUserId,
			@PathVariable(value = "infoid") Long dosageInfoId)  throws  Exception{
		return inboundServiceCallGeneric(caregiverUserId, service -> {
		List<DosageNotificationModel> dosageNotificationModelList= notificationService.fetchDosageNotificationsByCaregiverId(caregiverUserId, dosageInfoId);
			return ResponseEntity.ok(dosageNotificationModelList);
		});
	}

}
