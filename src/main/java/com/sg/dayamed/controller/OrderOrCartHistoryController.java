package com.sg.dayamed.controller;

import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.OrderOrCartService;
import com.sg.dayamed.util.ApplicationConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest")
@Api(value = "Order Or Cart Operations")
public class OrderOrCartHistoryController {

	private static final Logger logger = LoggerFactory.getLogger(OrderOrCartHistoryController.class);

	@Autowired
	OrderOrCartService orderOrCartService;

	@PostMapping(value = "/addorupdateitem/{userid}", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "add or update items in cart", notes = "Returns String")
	public ResponseEntity<String> addOrUpdateItems(HttpServletRequest request, Authentication authentication,
	                                               @RequestBody String inputjson,
	                                               @PathVariable(value = "userid") Long patientUserId)
			throws JSONException {
		logger.info("==addOrUpdateItems==inputjson=>>" + inputjson);
		JSONObject reqJson = new JSONObject(inputjson);
		logger.info(patientUserId + "==addOrUpdateItems==inputjson=>>" + inputjson);
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		if (patientUserId != null && (reqJson.optJSONArray("medicienIds") != null && reqJson.getJSONArray(
				"medicienIds")
		                                                                                    .length() > 0)
				|| (reqJson.optJSONArray("consumerIds") != null
				&& reqJson.getJSONArray("consumerIds")
				          .length() > 0)) {
			String status = orderOrCartService.addOrUpdateCartItems(patientUserId, inputjson, jwtUser);
			return new ResponseEntity<String>(status, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/getcartsummary/{userid}", produces = "application/json")
	public ResponseEntity<List<Object>> getCartItems(HttpServletRequest request, Authentication authentication,
	                                                 @PathVariable(value = "userid") Optional<Long> userid) {
		logger.info("==addOrUpdateItems==inputjson=>>" + userid);
		long patientUserId;
		if (userid.isPresent()) {
			patientUserId = userid.get();
		} else {
			JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
			patientUserId = jwtUser.getUserId();
		}
		List<Object> listCartItems = orderOrCartService.getCartSummaryByUserId(patientUserId);
		return new ResponseEntity<List<Object>>(listCartItems, HttpStatus.OK);
	}

	@GetMapping(value = "/deleteitemfromcart/{itemId}", produces = "application/json")
	public ResponseEntity<String> deleteItemFromCart(HttpServletRequest request, Authentication authentication,
	                                                 @PathVariable(value = "itemId") Long itemId,
	                                                 @RequestParam("itemCategoryType") String itemCategoryType,
	                                                 @RequestParam("patientUserId") long patientUserId) {
		logger.info("==deleteItemFromCart==itemId=>>" + itemId + "====>>" + itemCategoryType);
		if (itemId > 0) {
			orderOrCartService.deleteCartItems(patientUserId, itemId, itemCategoryType);
			return new ResponseEntity<String>(ApplicationConstants.SUCCESS, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
