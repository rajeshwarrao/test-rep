package com.sg.dayamed.controller;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.helper.pojo.PatientAssociatedActorsPojo;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.PatientInfoService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.ApplicationErrors;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@CrossOrigin
@Api(value = "Patient Operations")
public class PatientController extends BaseRestApiImpl {

	@Autowired
	PatientInfoService patientInfoService;

	@Autowired
	Utility utility;

	@Autowired
	ObjectMapper objectMapper;

	// fetchPatientsByProviderId for Admin
	@GetMapping(value = "/provider/{providerid}", produces = "application/json")
	@ApiOperation(value = "Fetch Patient list ByProviderId", notes = "Returns patient list")
	public ResponseEntity<Object> fetchPatientsByProviderId(HttpServletRequest request,
	                                                        @PathVariable(value = "providerid") String providerid,
	                                                        Authentication authentication)
			throws RestApiException {
		return inboundServiceCallGeneric(providerid, service -> {
			List<PatientModel> listPatientModel = patientInfoService.fetchPatietnsByProviderId(request, providerid,
			                                                                                   authentication);
			return ResponseEntity.ok(listPatientModel);
		});
	}

	@ApiResponses(value = {@ApiResponse(code = 401, message = ApplicationErrors.ACCESS_DENIED_PROVIDER)})
	@PreAuthorize("hasRole('PROVIDER')")
	@GetMapping(value = "/rest/patients/missed/adherence/provider", produces = "application/json")
	@ApiOperation(value = "Fetch Patient list ByProviderId", notes = "Returns patient list")
	public ResponseEntity<Object> fetchMissedAdherencePatientsByProviderId(Authentication authentication,
	                                                                       HttpServletRequest request)
			throws RestApiException {
		return inboundServiceCallGeneric(request, service -> {
			List<PatientModel> listPatientModel = patientInfoService
					.fetchMissedAdherencePatietnsByProviderId(authentication, request);
			return ResponseEntity.ok(listPatientModel);
		});
	}

	@ApiResponses(value = {@ApiResponse(code = 401, message = ApplicationErrors.ACCESS_DENIED_PROVIDER)})
	@PreAuthorize("hasRole('PROVIDER')")
	@GetMapping(produces = "application/json", value = "/rest/patients")
	@ApiOperation(value = "Fetch Patient list ByProviderId", notes = "Returns patient list")
	public ResponseEntity<Object> fetchPatientsByProviderIdFromCookie(Authentication authentication,
	                                                                  HttpServletRequest request)
			throws RestApiException {
		return inboundServiceCallGeneric(request, service -> {
			List<PatientModel> listPatientModel =
					patientInfoService.fetchPatietnsByProviderIdFromCookie(authentication,
					                                                       request);
			return ResponseEntity.ok(listPatientModel);
		});
	}

	@GetMapping(value = "/rest/patients/all", produces = "application/json")
	@ApiOperation(value = "Fetch All", notes = "Returns patients")
	public ResponseEntity<Object> fetchAllPatients(Authentication authentication, HttpServletRequest request)
			throws RestApiException {
		return inboundServiceCallGeneric(request, service -> {
			List<PatientModel> listPatientModel = patientInfoService.fetchAllPatients(authentication, request);
			return ResponseEntity.ok(listPatientModel);
		});
	}

	@GetMapping(value = "/rest/patients/{id}", produces = "application/json")
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<Object> fetchPatientById(@PathVariable(value = "id") String id,
	                                               Authentication authentication,
	                                               HttpServletRequest request) throws Exception {
		return inboundServiceCallGeneric(id, service -> {
			PatientModel patientModel = patientInfoService.fetchPatientById(id, authentication, request);
			return ResponseEntity.ok(patientModel);
		});
	}

	@PostMapping(value = "/rest/patients/update", produces = "application/json", consumes = {"application/json",
			"multipart/form-data", "application/octet-stream"})
	public ResponseEntity<Object> updatePatient(@RequestPart("patientInfo") String patientInfo,
	                                            @RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage,
	                                            HttpServletRequest request, Authentication authentication)
			throws Exception {
		return inboundServiceCallGeneric(patientInfo, service -> {
			PatientModel patientModelEncrypt = objectMapper.readValue(patientInfo, PatientModel.class);
			PatientModel patientModel = patientInfoService.updatePatient(patientModelEncrypt, profileImage, request,
			                                                             authentication);
			return ResponseEntity.ok(patientModel);
		});
	}

	@PostMapping(value = "/rest/patients/add", produces = "application/json", consumes = {"application/json",
			"multipart/form-data", "application/octet-stream"})
	public ResponseEntity<Object> addPatient(@RequestPart("patientInfo") String patientInfo,
	                                         @RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage,
	                                         HttpServletRequest request, Authentication authentication)
			throws Exception {
		return inboundServiceCallGeneric(patientInfo, service -> {
			PatientModel patientModel = objectMapper.readValue(patientInfo, PatientModel.class);
			PatientModel persistPatientModel = patientInfoService.addPatient(patientModel, profileImage, request,
			                                                                 authentication);
			return new ResponseEntity<Object>(persistPatientModel, HttpStatus.CREATED);
		});
	}

	@PreAuthorize("hasAnyRole('PROVIDER','ADMIN')")
	@DeleteMapping(value = "/rest/patients/delete/{id}", produces = "application/json")
	public ResponseEntity<Object> deletePatient(@PathVariable(value = "id") String id, HttpServletRequest request,
	                                            Authentication authentication) throws Exception {
		return inboundServiceCallGeneric(id, service -> {
			patientInfoService.deletePatient(id, request, authentication);
			return ResponseEntity.ok(new StringResponse(ApplicationConstants.SUCCESS));
		});
	}

	@GetMapping(value = "/rest/patients/associatedusers/bypatient/{patientid}", produces = "application/json")
	@ApiOperation(value = "fetch ProvidersBy PatientId", notes = "Returns provider List")
	public ResponseEntity<Object> fetchProvidersByPatientId(@PathVariable(value = "patientid") String patientid)
			throws Exception {
		return inboundServiceCallGeneric(patientid, service -> {
			PatientAssociatedActorsPojo patientAssociatedActorsPojo = patientInfoService
					.fetchProvidersByPatientId(patientid);
			return ResponseEntity.ok(patientAssociatedActorsPojo);
		});
	}
}
