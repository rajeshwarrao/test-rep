/**
 *
 */
package com.sg.dayamed.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.PharmaService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.StringResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author venkatrao.veginati
 *
 */
@RestController
@RequestMapping("/rest/pharmacists")
@Api(value = "Pharmacist Operations")
public class PharmacistController extends BaseRestApi {

	@Autowired
	private PharmaService pharmaService;

	@Autowired
	private PharmacistService pharmacistService;

	@Autowired
	ObjectMapper objectMapper;

	@PreAuthorize("hasAnyRole('PROVIDER','PHARMACIST')")
	@GetMapping(value = "/patients", produces = "application/json")
	@ApiOperation(value = "Fetch Patient list By Pharmacist Id", notes = "Returns patient list")
	public ResponseEntity<Object> getPatientsByPharmacistIdentifier(@CurrentUser JwtUserDetails jwtUser) throws
	                                                                                                     RestApiException {
		return inboundServiceCallGeneric(jwtUser, service -> {
			Object listPatients = pharmacistService.getPatientsByPharmacistIdentifier(jwtUser);
			return ResponseEntity.ok(listPatients);
		});
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Get Pharmacists", notes = "Returns all pharmacists")
	public ResponseEntity<Object> fetchPharmacists() throws
	                                                 RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			Object pharmacistList = pharmaService.fetchPharmasists();
			return ResponseEntity.ok(pharmacistList);
		});
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public ResponseEntity<Object> deletePharmacist(@PathVariable(value = "id") Long id) throws
	                                                                                    RestApiException {
		return inboundServiceCallGeneric(id, service -> {
			pharmaService.deletePharmacistValidation(id);
			return ResponseEntity.ok(new StringResponse(ApplicationConstants.SUCCESS));
		});
	}

	@PreAuthorize("hasAnyRole('PROVIDER','PHARMACIST','ADMIN')")
	@GetMapping(value = "/{id}", produces = "application/json")
	@ApiOperation(value = "Get Pharmacists", notes = "Returns all pharmacists")
	public ResponseEntity<Object> fetchPharmacistsById(@PathVariable(value = "id") Long pharmacistId
	) throws RestApiException {
		return inboundServiceCallGeneric(pharmacistId, service -> {
			Object pharmacistModel = pharmaService.fetchPharmacistById(pharmacistId);
			return ResponseEntity.ok(pharmacistModel);
		});
	}

	@PreAuthorize("hasAnyRole('PROVIDER','PHARMACIST','ADMIN')")
	@GetMapping(value = "/byuser/{userid}", produces = "application/json")
	@ApiOperation(value = "Get Pharmacists", notes = "Returns all pharmacists")
	public ResponseEntity<Object> fetchPharmacistsByUserId(@PathVariable(value = "userid") Long userId
	) throws RestApiException {
		return inboundServiceCallGeneric(userId, service -> {
			Object pharmacistModel = pharmacistService.fetchPharmacistByUserId(userId);
			return ResponseEntity.ok(pharmacistModel);
		});
	}

	@PreAuthorize("hasAnyRole('ADMIN','PHARMACIST')")
	@PostMapping(value = "/add", produces = "application/json", consumes = {"application/json", "multipart/form-data",
			"application/octet-stream"})
	@ApiOperation(value = "Add only Caregiver", notes = "Returns Caregiver")
	public ResponseEntity<Object> addPharmacist(@RequestPart("pharmacistInfo") String pharmacistModelInfo,
	                                            @RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage)
			throws RestApiException {
		return inboundServiceCallGeneric(pharmacistModelInfo, service -> {
			PharmacistModel pharmacistModel = objectMapper.readValue(pharmacistModelInfo, PharmacistModel.class);
			Object persistPharmacistModel = pharmaService.addPharmacistValidation(pharmacistModel, profileImage);
			return ResponseEntity.ok(persistPharmacistModel);
		});
	}

	@PreAuthorize("hasAnyRole('PROVIDER','PHARMACIST','ADMIN')")
	@GetMapping(value = "/bypatient/{patientid}", produces = "application/json")
	@ApiOperation(value = "fetch ProvidersBy PatientId", notes = "Returns Pharmacist List")
	public ResponseEntity<Object> fetchPharmacistsByPatientId(
			@PathVariable(value = "patientid") Long patientId) throws RestApiException {
		return inboundServiceCallGeneric(patientId, service -> {
			Object listPharmacistModel = pharmacistService.fetchPharmacistListByPatientId(patientId);
			return ResponseEntity.ok(listPharmacistModel);
		});
	}

}
