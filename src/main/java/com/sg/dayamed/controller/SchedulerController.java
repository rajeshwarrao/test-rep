package com.sg.dayamed.controller;

import com.sg.dayamed.pojo.ConsumptionFrequencyResponseModel;
import com.sg.dayamed.pojo.FirstConsumptionResponseModel;
import com.sg.dayamed.pojo.MultiplePrescriptionSchedulingRequestModel;
import com.sg.dayamed.pojo.PrescriptionSchedulingRequestModel;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.service.ConsumptionTemplateService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.UtilityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "Scheduler Operations")
public class SchedulerController extends BaseRestApi {

	@Autowired
	UtilityService utilityService;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	ConsumptionTemplateService consumptionTemplateService;

	@PostMapping(value = "/rest/utility/startprescriptionscheduling", produces = "application/json", consumes =
			"application/json")
	@ApiOperation(value = "prescriptionscheduling", notes = "Added Scheduled Jobs")
	public boolean startPrescriptionScheduling(
			@RequestBody PrescriptionSchedulingRequestModel prescriptionSchedulingRequestModel) throws Exception {

		if (prescriptionSchedulingRequestModel.getFlag() == 1) {
			prescriptionService.generatePrescriptionSchedule(prescriptionSchedulingRequestModel.getPrescriptionId(),
			                                                 prescriptionSchedulingRequestModel.getZoneId());
			return prescriptionService.createSchedulerJobsByPrescription(
					prescriptionSchedulingRequestModel.getPrescriptionId(),
					prescriptionSchedulingRequestModel.getZoneId());
		}
		return false;
	}

	@PostMapping(value = "/rest/utility/startprescriptionscheduling/multiple", produces = "application/json",
	             consumes =
			             "application/json")
	@ApiOperation(value = "prescriptionscheduling", notes = "Added Scheduled Jobs")
	public boolean startMultiplePrescriptionScheduling(
			@RequestBody MultiplePrescriptionSchedulingRequestModel multiplePrescriptionSchedulingRequestModel)
			throws Exception {
		return utilityService.startMultiplePrescriptionScheduling(multiplePrescriptionSchedulingRequestModel);
	}

	@GetMapping(value = "/rest/utility/frequencylist", produces = "application/json")
	public ResponseEntity<Object> getFrequencyList() throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			ConsumptionFrequencyResponseModel ConsumptionFrequencyResponseModel =
					consumptionTemplateService.getFrequencyList();
			return ResponseEntity.ok(ConsumptionFrequencyResponseModel);
		});
	}

	@GetMapping(value = "/rest/utility/firstconsumption/{frequencyId}", produces = "application/json")
	public ResponseEntity<Object> getFirstConsumption(@PathVariable(value = "frequencyId") Long frequencyId)
			throws RestApiException {
		return inboundServiceCallGeneric(frequencyId, service -> {
			FirstConsumptionResponseModel
					firstConsumptionResponseModel = consumptionTemplateService.getFirstConsumption(frequencyId);
			return ResponseEntity.ok(firstConsumptionResponseModel);
		});
	}
}
