package com.sg.dayamed.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sg.dayamed.commons.rest.RestRequest;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.PharmaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/rest/pharma")
@Api(value = "Pharma Operations")
public class PharmaController extends BaseRestApi {

	@Autowired
	PharmaService pharmaService;

	@Value("${profilepics.folder.path}")
	String profilePicsFolderPath;

	@GetMapping(value = "/medicine", produces = "application/json")
	@ApiOperation(value = "Get Medicines", notes = "Returns all medicines")
	public ResponseEntity<Object> fetchMedicines() throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			Object medicinelist = pharmaService.fetchMedicines();
			return ResponseEntity.ok(medicinelist);
		});
	}

	@GetMapping(value = "/commodities", produces = "application/json")
	@ApiOperation(value = "Get Consumergoods", notes = "Returns all Consumergoods")
	public ResponseEntity<Object> fetchConsumergoods() throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			Object consumerGoodList = pharmaService.fetchConsumergoods();
			return ResponseEntity.ok(consumerGoodList);
		});
	}

	@GetMapping(value = "/devices", produces = "application/json")
	@ApiOperation(value = "Get Devices", notes = "Returns all Devices")
	public ResponseEntity<Object> fetchDevices() throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			Object deviceList = pharmaService.fetchAllDevices();
			return ResponseEntity.ok(deviceList);
		});
	}

	@GetMapping(value = "/diseases", produces = "application/json")
	@ApiOperation(value = "Get Consumergoods", notes = "Returns all Consumergoods")
	public ResponseEntity<Object> fetchDeceases() throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			Object deceaseList = pharmaService.fetchDiseases();
			return ResponseEntity.ok(deceaseList);
		});
	}
}
