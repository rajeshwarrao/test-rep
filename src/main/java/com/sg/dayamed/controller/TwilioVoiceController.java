package com.sg.dayamed.controller;

import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.TwilioVoiceService;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.TwilioVoiceModel;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class TwilioVoiceController extends BaseRestApi {

	@Autowired
	TwilioVoiceService twilioVoiceService;

	@PostMapping(value = "/generateTwilioVoiceAuthToken")
	@ApiOperation(value = "Fetch TwilioVoiceAuthToken", notes = "Returns TwilioVoiceAuthToken")
	public ResponseEntity<Object> fetchTwilioVoiceToken(@RequestBody TwilioVoiceModel twilioObj)
			throws RestApiException {
		return inboundServiceCallGeneric(twilioObj, service -> {
			TwilioVoiceModel tokenmodel = twilioVoiceService.generateVoiceToken(twilioObj.getIdentity());
			return ResponseEntity.ok(tokenmodel);
		});
	}

	@PostMapping(value = "/voicecall/connect")
	@ApiOperation(value = "Voice call connect", notes = "Returns Voice Twiml to connect to a client identity")
	public ResponseEntity<Object> getVoiceXMLResponse(@RequestBody TwilioVoiceModel twilioObj) throws RestApiException {
		return inboundServiceCallGeneric(twilioObj,
		                                 service -> ResponseEntity.ok(new StringResponse(
				                                 twilioVoiceService.connectVoiceCall(twilioObj.getIdentity()))));
	}
}
