package com.sg.dayamed.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.imantics.akp.sdk.pid.model.PidDataTable;
import com.imantics.akp.sdk.pid.model.PidValue;
import com.imantics.akp.sdk.utils.DateUtils;

import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.helper.pojo.LatestGraphJsonModel;
import com.sg.dayamed.helper.pojo.LatestGraphSuperJsonModel;
import com.sg.dayamed.pojo.BiometricJsonModel;
import com.sg.dayamed.pojo.BiometricSuperJsonModel;
import com.sg.dayamed.pojo.BpMonitorModel;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.GlucometerModel;
import com.sg.dayamed.pojo.GraphJsonModel;
import com.sg.dayamed.pojo.GraphSuperJsonModel;
import com.sg.dayamed.pojo.HeartRateModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PulseOximeterModel;
import com.sg.dayamed.pojo.ScaleModel;
import com.sg.dayamed.pojo.SkipOrDelayDeviseInfo;
import com.sg.dayamed.pojo.StepsModel;
import com.sg.dayamed.pojo.adherence.BPAdherenceFromExcelModel;
import com.sg.dayamed.pojo.adherence.GlucoseAdherenceFromExcelModel;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.service.BiometricService;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionScheduleService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/rest/biometrics")
@Api(value = "Patient Operations")
public class BiometricController {

	@Autowired
	BiometricService biometricService;

	@Autowired
	PatientService patientService;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ImanticService imanticService;

	@Autowired
	NotificationScheduleService jobService;

	@Autowired
	Utility utility;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	DeviceInfoRepository deviceInfoRepository;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	PrescriptionScheduleService prescriptionScheduleService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	private static final Logger logger = LoggerFactory.getLogger(BiometricController.class);

	DateTimeFormatter dayamedGeneralFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	Format dateformatterForGraph = new SimpleDateFormat("MM/dd/yyyy HH:mm");// 04/01/2014

	NumberFormat decimalFormat = new DecimalFormat("#0.00");

	// fetchHeartRateListBypatientId -Mobile
	@ApiIgnore
	@GetMapping(value = "/{patientid}/heartrates", produces = "application/json")
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<BiometricSuperJsonModel> fetchHeartRatesBypatientId(
			@PathVariable(value = "patientid") String patientid) {
		List<HeartRateModel> heartratemodels = biometricService
				.fetchHeartRatesByPatientId(Long.parseLong(patientid));
		if (heartratemodels.size() > 0) {
			List<BiometricJsonModel> heartRatevalues = new ArrayList<BiometricJsonModel>();
			BiometricSuperJsonModel hrsuperjsonModel = new BiometricSuperJsonModel();
			heartratemodels.forEach(heartrateModel -> {
				BiometricJsonModel hrjsonModel = new BiometricJsonModel();
				// hrjsonModel.setDate(heartrateModel.getDate());
				// hrjsonModel.setName(dateUtility.getFormattedLocalDateTime(LocalDateTime.parse(heartrateModel.getObservedTime())));
				hrjsonModel.setName(heartrateModel.getObservedTime());
				hrjsonModel.setValue(heartrateModel.getReadingValue());
				heartRatevalues.add(hrjsonModel);
			});
			hrsuperjsonModel.setName("Heart Rate");
			hrsuperjsonModel.setSeries(heartRatevalues);
			return new ResponseEntity<BiometricSuperJsonModel>(hrsuperjsonModel, HttpStatus.OK);
		}
		return new ResponseEntity<BiometricSuperJsonModel>(HttpStatus.NO_CONTENT);
	}

	// add steps data

	/**
	 * when adding vital need to add in respective vital table, and
	 * consumpton_status should be update in scheduler_jobs,prescrition_schedule
	 * and notification tables.
	 *
	 * @param stepsmodel
	 * @return StepsModel
	 */
	@PostMapping(value = "/addsteps", produces = "application/json", consumes = "application/json")
	public ResponseEntity<StepsModel> addSteps(@RequestBody String stepsmodel) throws JSONException {
		/*
		 * { "observedtime": "2018-03-21 12:54", "stepcount": "82", "patientid":
		 * "97547", "zoneid": "Asia/Calcutta", "latitude": "56.130721",
		 * "longitude": "106.346337", "deviceinfoid": "2", "prescriptionid": 1,
		 * "prescribedtime": "2015-07-16 17:07", "consumptionstatus":
		 * "taken/delayed" }
		 */
		if (StringUtils.isBlank(stepsmodel)) {
			logger.error("steps object must not be empty or null");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		JSONObject jsonobj = new JSONObject(stepsmodel);
		if (!jsonobj.has("zoneid") || !jsonobj.has("prescribedtime") || !jsonobj.has("stepcount")
				|| !jsonobj.has("consumptionstatus") || !jsonobj.has("observedtime") || !jsonobj.has("deviceinfoid") || !jsonobj.has("prescriptionid")) {
			logger.error("missing required data while adding steps");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
				                                            .trim());
		if (jsonobj.has("deviceinfoid")) {
			Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository
					.findById(Long.parseLong(jsonobj.getString("deviceinfoid")));

			if (optionaldeviceInfo.isPresent()) {
				if (!optionaldeviceInfo.get()
				                       .getDevice()
				                       .getName()
				                       .equalsIgnoreCase(ApplicationConstants.STEPS)) {
					logger.error("DeviceinfoId provided by you is not beloging to steps");
					return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
				}
			} else {
				logger.error("DeviceinfoId provided by you is not available in database");
				return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
			}
			if (optionaldeviceInfo.get()
			                      .getPrescription()
			                      .getId() != jsonobj.getLong("prescriptionid")
					|| optionaldeviceInfo.get()
					                     .getPrescription()
					                     .getPatient()
					                     .getId() != Long
					.parseLong(jsonobj.getString("patientid"))) {
				logger.error("prescription id provided by you is not belonging to this patient");
				return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
			}
		}
		StepsModel stepsModel = biometricService
				.fetchStepsByPrescribedTimeAndDeviceInfoId(
						dateUtility.convertdatetimeStringToLocalDateTime(
								prescribedLDTInUTC.format(dayamedGeneralFormatter)),
						jsonobj.getString("deviceinfoid"));
		if (stepsModel == null) {
			stepsModel = new StepsModel();
			if (jsonobj.has("prescriptionid")) {
				stepsModel.setPrescriptionID(jsonobj.getLong("prescriptionid"));
			}
			if (jsonobj.has("deviceinfoid")) {
				stepsModel.setDeviceInfoId(jsonobj.getString("deviceinfoid"));
			}
			stepsModel.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));
		}
		stepsModel.setStepCount(Integer.parseInt(jsonobj.getString("stepcount")));
		stepsModel.getConsumptionStatus()
		          .add(jsonobj.getString("consumptionstatus"));

		ArrayList<String> location = new ArrayList<String>();
		if (jsonobj.has("latitude")) {
			location.add(jsonobj.getString("latitude"));
		}
		if (jsonobj.has("longitude")) {
			location.add(jsonobj.getString("longitude"));
		}
		stepsModel.setLocation(location);
		PatientModel patientModel = utility.convertPatientEntityTopatientModel(
				patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
		if (patientModel.getId() == 0) {
			return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
		}
		stepsModel.setPatient(patientModel);

		LocalDateTime observedlocalDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("observedtime"), jsonobj.getString("zoneid"));
		stepsModel.setObservedTime(observedlocalDateTimeInUTC.format(dayamedGeneralFormatter));

		// Fetching data based on AcualdateTime and dosageID from
		// SchedulerJobs.
		jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC,
		                                          Long.parseLong(jsonobj.getString("deviceinfoid")), jsonobj.getString("consumptionstatus")
		                                                                                                    .trim());
		// end update consumption status in schedulerjob
		// Fetching data based on AcualdateTime and dosageID from
		// PrescriptionSchedule
		prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
		                                                                   Long.parseLong(jsonobj.getString("deviceinfoid")),
		                                                                   jsonobj.getString("consumptionstatus")
		                                                                          .trim());
		// end Adding steps datapoint
		StepsModel persistedStepsModel = biometricService.addSteps(stepsModel);
		String msg = ApplicationConstants.EMPTY_STRING;
		Long userId = patientModel.getUserDetails()
		                          .getId();
		String userLangPreference = utility.getUserPreferedLang(userId);
		if (persistedStepsModel != null) {
			Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository
					.findById(Long.parseLong(persistedStepsModel.getDeviceInfoId()));
			DeviceInfo deviceInfo = null;
			if (optionaldeviceInfo.isPresent()) {
				deviceInfo = optionaldeviceInfo.get();
			}
			if (deviceInfo != null && deviceInfo.getDeviceNotificationList() != null
					&& deviceInfo.getDeviceNotificationList()
					             .size() > 0
					&& deviceInfo.getDeviceNotificationList()
					             .get(0) != null) {
				// handling greaterthan and lessthan readings
				if (deviceInfo.getDeviceNotificationList()
				              .get(0)
				              .getGreaterThanReading1() != null
						&& (Float.parseFloat(jsonobj.getString("stepcount")
						                            .trim()) > (Float.parseFloat(
						deviceInfo.getDeviceNotificationList()
						          .get(0)
						          .getGreaterThanReading1())))) {
					msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "stepcount.reading.lessthan", userLangPreference,
					                                                                    null)
							+ deviceInfo.getDeviceNotificationList()
							            .get(0)
							            .getGreaterThanReading1();
				}
				if (deviceInfo.getDeviceNotificationList()
				              .get(0)
				              .getLessThanReading1() != null
						&& !(Float.parseFloat(jsonobj.getString("stepcount")
						                             .trim()) < (Float.parseFloat(
						deviceInfo.getDeviceNotificationList()
						          .get(0)
						          .getLessThanReading1())))) {
					msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "stepcount.reading.beyond", userLangPreference,
					                                                                    null)
							+ deviceInfo.getDeviceNotificationList()
							            .get(0)
							            .getLessThanReading1();
				}
			}
		}

		// sending Mail to Patient
		if (persistedStepsModel != null) {
			Map<String, String> msgResourceReplaceKeyVal =
					messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonobj, "reading", jsonobj.getString("reading"));
			String bodyMsg =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.stepcount.you", userLangPreference,
					                                                              null) + msg;
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(patientModel.getUserDetails()
			                              .getEmailId());
			emailAndSmsUtility.sendEmail(ApplicationConstants.BIOMETRIC_STATUS, bodyMsg, recipientList);
			//emailAndSmsUtility.sendEmail(ApplicationConstants.BIOMETRIC_STATUS, "biometric.test.stepcount.you", msgResourceReplaceKeyVal, recipientList);

			emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
			                                                .getCountryCode()
					, patientModel.getUserDetails()
					              .getMobileNumber());
			// Adding notification to Notification table. for history in
			// calender
			notificationService.addWebNotification(patientModel.getUserDetails()
			                                                   .getId(),
			                                       bodyMsg, ApplicationConstants.DEVICE_STATUS_WIT_, ApplicationConstants.VITAL_TAKEN_WIT);
			//sending mobile notification
			notificationService.sendMobileNotification(patientModel.getUserDetails()
			                                                       .getType(), bodyMsg, ApplicationConstants.BIOMETRIC_STATUS, patientModel.getUserDetails()
			                                                                                                                               .getToken());
			// empty reciveruseridlist as first parameter
			// sending notification and mails to caregiver
			String patientName = patientModel.getUserDetails()
			                                 .getFirstName() + " "
					+ patientModel.getUserDetails()
					              .getLastName();
				/*bodyMsg = patientName + " have successfully taken his/her Step count. count is: "
				+ jsonobj.getString("stepcount") + ". prescribedTime is "+jsonobj.getString("prescribedtime").trim()+ " and noticed at "+jsonobj.getString
				("observedtime").trim() + msg;*/
			msgResourceReplaceKeyVal.put("patientName", patientName);
			bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.stepcount.patientname",
			                                                                        userLangPreference, null) + msg;
			List<String> caregiverMailList = new ArrayList<>();
			for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
				caregiverMailList.add(caregiverModel.getUserDetails()
				                                    .getEmailId());
				emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
				                                                  .getCountryCode()
						, caregiverModel.getUserDetails()
						                .getMobileNumber());
				notificationService.addWebNotification(caregiverModel.getUserDetails()
				                                                     .getId(),
				                                       bodyMsg, ApplicationConstants.VITAL_TAKEN, "device_status");
				if (caregiverModel.getUserDetails()
				                  .getType() != null &&
						(caregiverModel.getUserDetails()
						               .getType()
						               .equalsIgnoreCase(ApplicationConstants.ANDROID) || caregiverModel.getUserDetails()
						                                                                                .getType()
						                                                                                .equalsIgnoreCase(ApplicationConstants.IOS))) {
					notificationService.sendMobileNotification(caregiverModel.getUserDetails()
					                                                         .getType(), bodyMsg, ApplicationConstants.VITAL_TAKEN,
					                                           caregiverModel.getUserDetails()
					                                                         .getToken());
				}
			}
			if (caregiverMailList.size() > 0) {
				emailAndSmsUtility.sendEmail("emailsubject.medicinestatus", "biometric.test.stepcount.patientname", msgResourceReplaceKeyVal,
				                             caregiverMailList);
			}
		}
		return new ResponseEntity<StepsModel>(persistedStepsModel, HttpStatus.OK);

	}

	@PostMapping(value = "/deviceinfos/skipordelay", produces = "application/json", consumes = "application/json")
	public ResponseEntity<SkipOrDelayDeviseInfo> skiOrDelayDeviceInfos(@RequestBody String skiporDelayDeviseInfo) throws JSONException {
		/*
		 * {"observedtime":"2018-03-21 12:54", "patientid":"97547",
		 * "zoneid":"Asia/Calcutta", "latitude": "56.130721", "longitude":
		 * "106.346337", "deviceinfoids": ["8","4"] "prescriptionid": 1,
		 * "prescribedtime":"2015-07-16 17:07", "consumptionstatus":"delayed"}
		 */

		JSONObject jsonobj = new JSONObject(skiporDelayDeviseInfo);
		if (!jsonobj.has("deviceinfoids") || !jsonobj.has("consumptionstatus") || !jsonobj.has("observedtime") || !jsonobj.has("zoneid")
				|| !jsonobj.has("prescribedtime") || !jsonobj.has("prescriptionid") || !jsonobj.has("patientid")
		) {
			logger.error("Missing json data while skip or delay");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		JSONArray deviceInfoIdArray = jsonobj.getJSONArray("deviceinfoids");
		SkipOrDelayDeviseInfo skipOrDelayDeviseInfo = new SkipOrDelayDeviseInfo();
		for (int i = 0; i < deviceInfoIdArray.length(); i++) {
			deviceInfoIdArray.getString(i);
			// DeviceInfo deviceInfo =
			// deviceInfoRepository.findOne(Long.parseLong(deviceInfoIdArray.getString(i)));
			Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository
					.findById(Long.parseLong(deviceInfoIdArray.getString(i)));

			skipOrDelayDeviseInfo.setConsumptionStatus(jsonobj.getString("consumptionstatus")
			                                                  .trim());

			ArrayList<String> location = new ArrayList<>();
			if (jsonobj.has("latitude")) {
				location.add(jsonobj.getString("latitude"));
			}
			if (jsonobj.has("longitude")) {
				location.add(jsonobj.getString("longitude"));
			}
			skipOrDelayDeviseInfo.setLocation(location);
			LocalDateTime localDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
					jsonobj.getString("observedtime")
					       .trim(), jsonobj.getString("zoneid")
					                       .trim());

			skipOrDelayDeviseInfo.setObservedTime(localDateTimeInUTC.format(dayamedGeneralFormatter));
			LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
					jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
					                                            .trim());

			skipOrDelayDeviseInfo.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));

			PatientModel patientmodel = utility.convertPatientEntityTopatientModel(
					patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
			if (patientmodel.getId() == 0) {
				return new ResponseEntity<SkipOrDelayDeviseInfo>(HttpStatus.BAD_REQUEST);
			}

			skipOrDelayDeviseInfo.setPatient(patientmodel);
			skipOrDelayDeviseInfo.setPrescriptionID(jsonobj.getLong("prescriptionid"));

			skipOrDelayDeviseInfo.setReading1(null);
			skipOrDelayDeviseInfo.setReading2(null);
			List<String> skipOrDelayeddeviceNameList = new ArrayList<String>();

			if (optionaldeviceInfo.isPresent()) {
				DeviceInfo deviceInfo = optionaldeviceInfo.get();
				// based on switch need to add device for delay or skip in
				// DB
				switch (deviceInfo.getDevice()
				                  .getName()) {

					case ApplicationConstants.BP_METER: {
						BpMonitorModel bpMonitorModel = biometricService
								.fetchBpMonitorByPrescribedTimeAndDeviceInfoId(
										dateUtility.convertdatetimeStringToLocalDateTime(
												skipOrDelayDeviseInfo.getPrescribedTime()),
										Long.toString(deviceInfo.getId()));
						if (bpMonitorModel == null) {
							bpMonitorModel = new BpMonitorModel();
							bpMonitorModel.setDeviceInfoId(Long.toString(deviceInfo.getId()));
							bpMonitorModel.setPrescribedTime(skipOrDelayDeviseInfo.getPrescribedTime());
							bpMonitorModel.setPatient(skipOrDelayDeviseInfo.getPatient());
							bpMonitorModel.setPrescriptionID(skipOrDelayDeviseInfo.getPrescriptionID());
						}
						bpMonitorModel.getConsumptionStatus()
						              .add(skipOrDelayDeviseInfo.getConsumptionStatus());
						bpMonitorModel.setDiastolicpressureValue(null);
						bpMonitorModel.setSystolicpressureValue(null);
						bpMonitorModel.setLocation(skipOrDelayDeviseInfo.getLocation());
						bpMonitorModel.setObservedTime(skipOrDelayDeviseInfo.getObservedTime());

						// Fetching data based on AcualdateTime and deviceID
						jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC, deviceInfo.getId(),
						                                          skipOrDelayDeviseInfo.getConsumptionStatus());
						// To update consumption status in PrescriptionSchedule
						// table
						prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
						                                                                   deviceInfo.getId(), skipOrDelayDeviseInfo.getConsumptionStatus());
						// end update consumption status in PrescriptionSchedule
						// table

						biometricService.addBpReading(bpMonitorModel);
						skipOrDelayeddeviceNameList.add(ApplicationConstants.BP_METER);
					}
					break;
					case ApplicationConstants.GLUCO_METER: {
						GlucometerModel glucometerModel = biometricService
								.fetchGlucometerByPrescribedTimeAndDeviceInfoId(
										dateUtility.convertdatetimeStringToLocalDateTime(
												skipOrDelayDeviseInfo.getPrescribedTime()),
										Long.toString(deviceInfo.getId()));
						if (glucometerModel == null) {
							glucometerModel = new GlucometerModel();
							glucometerModel.setDeviceInfoId(Long.toString(deviceInfo.getId()));
							glucometerModel.setPrescribedTime(skipOrDelayDeviseInfo.getPrescribedTime());
							glucometerModel.setPatient(skipOrDelayDeviseInfo.getPatient());
							glucometerModel.setPrescriptionID(skipOrDelayDeviseInfo.getPrescriptionID());
						}

						glucometerModel.getConsumptionStatus()
						               .add(skipOrDelayDeviseInfo.getConsumptionStatus());
						glucometerModel.setLocation(skipOrDelayDeviseInfo.getLocation());
						glucometerModel.setObservedTime(skipOrDelayDeviseInfo.getObservedTime());
						glucometerModel.setReadingValue(null);

						// Fetching data based on AcualdateTime and dosageID
						jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC, deviceInfo.getId(),
						                                          skipOrDelayDeviseInfo.getConsumptionStatus());
						// To update consumption status in PrescriptionSchedule
						// table
						prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
						                                                                   deviceInfo.getId(), skipOrDelayDeviseInfo.getConsumptionStatus());
						// end update consumption status in PrescriptionSchedule
						// table
						biometricService.addGlucometer(glucometerModel);
						skipOrDelayeddeviceNameList.add(ApplicationConstants.GLUCO_METER);
					}

					break;
					case ApplicationConstants.SCALE: {
						ScaleModel scaleModel = biometricService
								.fetchScaleByPrescribedTimeAndDeviceInfoId(
										dateUtility.convertdatetimeStringToLocalDateTime(
												skipOrDelayDeviseInfo.getPrescribedTime()),
										Long.toString(deviceInfo.getId()));
						if (scaleModel == null) {
							scaleModel = new ScaleModel();
							scaleModel.setDeviceInfoId(Long.toString(deviceInfo.getId()));
							scaleModel.setPrescribedTime(skipOrDelayDeviseInfo.getPrescribedTime());
							scaleModel.setPatient(skipOrDelayDeviseInfo.getPatient());
							scaleModel.setPrescriptionID(skipOrDelayDeviseInfo.getPrescriptionID());
						}

						scaleModel.getConsumptionStatus()
						          .add(skipOrDelayDeviseInfo.getConsumptionStatus());
						scaleModel.setLocation(skipOrDelayDeviseInfo.getLocation());
						scaleModel.setObservedTime(skipOrDelayDeviseInfo.getObservedTime());
						scaleModel.setReading(null);
						// Fetching data based on AcualdateTime and dosageID
						jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC, deviceInfo.getId(),
						                                          skipOrDelayDeviseInfo.getConsumptionStatus());
						// To update consumption status in PrescriptionSchedule
						// table
						prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
						                                                                   deviceInfo.getId(), skipOrDelayDeviseInfo.getConsumptionStatus());
						// end update consumption status in PrescriptionSchedule
						// table
						biometricService.addScale(scaleModel);
						skipOrDelayeddeviceNameList.add(ApplicationConstants.SCALE);
					}
					break;
					case ApplicationConstants.STEPS: {
						StepsModel stepsModel = biometricService
								.fetchStepsByPrescribedTimeAndDeviceInfoId(
										dateUtility.convertdatetimeStringToLocalDateTime(
												skipOrDelayDeviseInfo.getPrescribedTime()),
										Long.toString(deviceInfo.getId()));
						if (stepsModel == null) {
							stepsModel = new StepsModel();
							stepsModel.setDeviceInfoId(Long.toString(deviceInfo.getId()));
							stepsModel.setPrescribedTime(skipOrDelayDeviseInfo.getPrescribedTime());
							stepsModel.setPatient(skipOrDelayDeviseInfo.getPatient());
							stepsModel.setPrescriptionID(skipOrDelayDeviseInfo.getPrescriptionID());
						}

						stepsModel.getConsumptionStatus()
						          .add(skipOrDelayDeviseInfo.getConsumptionStatus());
						stepsModel.setLocation(skipOrDelayDeviseInfo.getLocation());
						stepsModel.setObservedTime(skipOrDelayDeviseInfo.getObservedTime());
						stepsModel.setStepCount(0);
						// Fetching data based on AcualdateTime and dosageID
						jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC, deviceInfo.getId(),
						                                          skipOrDelayDeviseInfo.getConsumptionStatus());
						// To update consumption status in PrescriptionSchedule
						// table
						prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
						                                                                   deviceInfo.getId(), skipOrDelayDeviseInfo.getConsumptionStatus());
						// end update consumption status in PrescriptionSchedule
						// table
						biometricService.addSteps(stepsModel);
						skipOrDelayeddeviceNameList.add(ApplicationConstants.STEPS);
					}
					break;
					case ApplicationConstants.PULSE_OXIMETER: {
						skipOrDelayeddeviceNameList.add(ApplicationConstants.PULSE_OXIMETER);
					}
					break;
					case ApplicationConstants.HEART_RATE: {
						HeartRateModel heartRateModel = biometricService
								.fetchHeartRateByPrescribedTimeAndDeviceInfoId(
										dateUtility.convertdatetimeStringToLocalDateTime(
												skipOrDelayDeviseInfo.getPrescribedTime()),
										Long.toString(deviceInfo.getId()));
						if (heartRateModel == null) {
							heartRateModel = new HeartRateModel();
							heartRateModel.setDeviceInfoId(Long.toString(deviceInfo.getId()));
							heartRateModel.setPrescribedTime(skipOrDelayDeviseInfo.getPrescribedTime());
							heartRateModel.setPatient(skipOrDelayDeviseInfo.getPatient());
							heartRateModel.setPrescriptionID(skipOrDelayDeviseInfo.getPrescriptionID());
						}
						heartRateModel.getConsumptionStatus()
						              .add(skipOrDelayDeviseInfo.getConsumptionStatus());
						heartRateModel.setLocation(skipOrDelayDeviseInfo.getLocation());
						heartRateModel.setObservedTime(skipOrDelayDeviseInfo.getObservedTime());
						heartRateModel.setReadingValue(null);
						// Fetching data based on AcualdateTime and dosageID
						jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC, deviceInfo.getId(),
						                                          skipOrDelayDeviseInfo.getConsumptionStatus());
						// To update consumption status in PrescriptionSchedule
						// table
						prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
						                                                                   deviceInfo.getId(), skipOrDelayDeviseInfo.getConsumptionStatus());
						// end update consumption status in PrescriptionSchedule
						// table
						biometricService.addHeartRate(heartRateModel);
						skipOrDelayeddeviceNameList.add(ApplicationConstants.HEART_RATE);
					}
					break;
					case ApplicationConstants.SPIRO_METER: {
						skipOrDelayeddeviceNameList.add(ApplicationConstants.SPIRO_METER);
					}
					break;
					case ApplicationConstants.PT_INR: {
						skipOrDelayeddeviceNameList.add(ApplicationConstants.PT_INR);
					}
					break;
					default:
						logger.warn("Invalid device {}", deviceInfo.getDevice()
						                                           .getName());
				}
			}
			if (skipOrDelayDeviseInfo.getPatient() != null
					&& skipOrDelayDeviseInfo.getPatient()
					                        .getUserDetails() != null) {

				PatientModel patient = skipOrDelayDeviseInfo.getPatient();
				String userLangPreference = utility.getUserPreferedLang(patient.getId());
				Map<String, String> msgResourceReplaceKeyVal = messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonobj, "consumptionStatus",
				                                                                                                                skipOrDelayDeviseInfo.getConsumptionStatus(),
				                                                                                                                skipOrDelayeddeviceNameList,
				                                                                                                                userLangPreference);
				String bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "adherence.vital.status.you",
				                                                                               userLangPreference, null);
				//String subject = "Vital skipped/delayed";
				String subject =
						messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "emailsubject.vitalskipped",
						                                                              userLangPreference,
						                                                              null);

				List<String> recipientList = new ArrayList<>();
				recipientList.add(patient.getUserDetails()
				                         .getEmailId());
				emailAndSmsUtility.sendEmail(subject, bodyMsg, recipientList);
				recipientList.clear();
				emailAndSmsUtility.sendSms(bodyMsg, patient.getUserDetails()
				                                           .getCountryCode(), patient.getUserDetails()
				                                                                     .getMobileNumber());
				if (skipOrDelayDeviseInfo.getConsumptionStatus()
				                         .equalsIgnoreCase("delayed")) {
					notificationService.addWebNotification(patient.getUserDetails()
					                                              .getId(), bodyMsg, ApplicationConstants.DEVICE_STATUS_WIT_,
					                                       ApplicationConstants.VITAL_DELAYED_WIT);
				} else {
					notificationService.addWebNotification(patient.getUserDetails()
					                                              .getId(), bodyMsg, ApplicationConstants.DEVICE_STATUS_WIT_,
					                                       ApplicationConstants.VITAL_SKIPPED_WIT);
				}

				notificationService.sendMobileNotification(patient.getUserDetails()
				                                                  .getType(), bodyMsg, ApplicationConstants.UPDATED_PATIET_PROFILE_IN_DAYAMED_TITLE,
				                                           patient.getUserDetails()
				                                                  .getToken());
				//if patient has caregiver
				if (patient.getCaregivers() != null) {
					msgResourceReplaceKeyVal.put("patientName", patient.getUserDetails()
					                                                   .getFirstName() + " " + patient.getUserDetails()
					                                                                                  .getLastName());
					bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "adherence.vital.status.patientname",
					                                                                        userLangPreference, null);
					for (CaregiverModel caregiver : skipOrDelayDeviseInfo.getPatient()
					                                                     .getCaregivers()) {
						recipientList.add(caregiver.getUserDetails()
						                           .getEmailId());
						// sending sms too
						emailAndSmsUtility.sendSms(bodyMsg, caregiver.getUserDetails()
						                                             .getCountryCode()
								, caregiver.getUserDetails()
								           .getMobileNumber());
						if (caregiver.getUserDetails()
						             .getType() != null && (caregiver.getUserDetails()
						                                             .getType()
						                                             .equalsIgnoreCase(ApplicationConstants.ANDROID)
								|| caregiver.getUserDetails()
								            .getType()
								            .equalsIgnoreCase(ApplicationConstants.IOS))) {
							notificationService.sendMobileNotification(caregiver.getUserDetails()
							                                                    .getType(),
							                                           bodyMsg, ApplicationConstants.UPDATED_PATIET_PROFILE_IN_DAYAMED_TITLE,
							                                           caregiver.getUserDetails()
							                                                    .getToken());
						}
					}
					emailAndSmsUtility.sendEmail("emailsubject.vitalskipped", "adherence.vital.status.patientname", msgResourceReplaceKeyVal, recipientList);
					recipientList.clear();
				}
			}
		}
		return null;
	}

	// add PulseOximeter data
	@PostMapping(value = "/addpulseoximeter", produces = "application/json", consumes = "application/json")
	public ResponseEntity<PulseOximeterModel> addPulseOximeter(@RequestBody String pulseOximetermodel) throws JSONException {
		// {"datetime":"2015-07-16
		// 17:07","sp02":"95","pulse":"78","patientid":"1","zoneid":"Asia/Kolkata"}
		// datetime format: "yyyy-MM-dd HH:mm"
		JSONObject jsonobj = new JSONObject(pulseOximetermodel);
		if (!jsonobj.has("sp02") || !jsonobj.has("pulse") || !jsonobj.has("patientid") || !jsonobj.has("zoneid") || !jsonobj.has("datetime")) {
			logger.error("Missing json data while adding pulseoximeter");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		PulseOximeterModel pulseOximeterModel = new PulseOximeterModel();
		pulseOximeterModel.setSp02(jsonobj.getString("sp02"));
		pulseOximeterModel.setPulse(Integer.parseInt(jsonobj.getString("pulse")));
		PatientModel patientmodel = utility.convertPatientEntityTopatientModel(
				patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
		pulseOximeterModel.setPatient(patientmodel);

		LocalDateTime localDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("datetime"), jsonobj.getString("zoneid"));
		pulseOximeterModel.setTakenTime(localDateTimeInUTC);
		pulseOximeterModel = biometricService.addPulseOximeter(pulseOximeterModel);
		return new ResponseEntity<PulseOximeterModel>(pulseOximeterModel, HttpStatus.OK);
	}

	// add HeartRating data
	@PostMapping(value = "/addheartrate", produces = "application/json", consumes = "application/json")
	public ResponseEntity<HeartRateModel> addHeartrate(@RequestBody String heartrateModel) throws JSONException {
		/*
		 * { "observedtime": "2018-03-21 12:54", "reading": "82", "patientid":
		 * "97547", "zoneid": "Asia/Calcutta", "latitude": "56.130721",
		 * "longitude": "106.346337", "deviceinfoid": "2", "prescriptionid": 1,
		 * "prescribedtime": "2015-07-16 17:07", "consumptionstatus":
		 * "taken/delayed" }
		 */
		HeartRateModel hrmodel = null;
		JSONObject jsonobj = new JSONObject(heartrateModel);
		if (!jsonobj.has("prescribedtime") || !jsonobj.has("zoneid") || !jsonobj.has("patientid") || !jsonobj.has("prescriptionid")
				|| !jsonobj.has("reading") || !jsonobj.has("consumptionstatus") || !jsonobj.has("observedtime")) {
			logger.error("Missing required data while adding heartrate");
			return new ResponseEntity<HeartRateModel>(HttpStatus.BAD_REQUEST);
		}
		LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
				                                            .trim());
		if (jsonobj.has("deviceinfoid")) {
			//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(jsonobj.getString("deviceinfoid")));
			Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository.findById(Long.parseLong(jsonobj.getString("deviceinfoid")));
			if (optionaldeviceInfo.isPresent()) {
				if (!optionaldeviceInfo.get()
				                       .getDevice()
				                       .getName()
				                       .equalsIgnoreCase("Heart Rate")) {
					logger.error("DeviceInfo not related to HeartRate");
					return new ResponseEntity<HeartRateModel>(HttpStatus.BAD_REQUEST);
				}
			} else {
				logger.error("DeviceInfo not available in DB with provided deviceinfoid");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			if (optionaldeviceInfo.get()
			                      .getPrescription()
			                      .getId() != jsonobj.getLong("prescriptionid") || optionaldeviceInfo.get()
			                                                                                         .getPrescription()
			                                                                                         .getPatient()
			                                                                                         .getId() !=
					Long.parseLong(jsonobj.getString("patientid"))) {
				logger.error("prescriptionid not belongs to this patient");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}
		HeartRateModel heartRateModel = biometricService
				.fetchHeartRateByPrescribedTimeAndDeviceInfoId(
						dateUtility.convertdatetimeStringToLocalDateTime(
								prescribedLDTInUTC.format(dayamedGeneralFormatter)),
						jsonobj.getString("deviceinfoid"));
		if (heartRateModel == null) {
			heartRateModel = new HeartRateModel();

			heartRateModel.setDeviceInfoId(jsonobj.getString("deviceinfoid"));

			heartRateModel.setPrescriptionID(jsonobj.getLong("prescriptionid"));

			heartRateModel.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));
		}
		heartRateModel.setReadingValue(jsonobj.getString("reading"));
		heartRateModel.getConsumptionStatus()
		              .add(jsonobj.getString("consumptionstatus"));
		ArrayList<String> location = new ArrayList<String>();
		if (jsonobj.has("latitude")) {
			location.add(jsonobj.getString("latitude"));
		}
		if (jsonobj.has("longitude")) {
			location.add(jsonobj.getString("longitude"));
		}
		heartRateModel.setLocation(location);

		PatientModel patientModel = utility.convertPatientEntityTopatientModel(
				patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
		if (patientModel.getId() == 0) {
			return new ResponseEntity<HeartRateModel>(HttpStatus.BAD_REQUEST);
		}
		heartRateModel.setPatient(patientModel);
		LocalDateTime observedlocalDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("observedtime"), jsonobj.getString("zoneid"));
		heartRateModel.setObservedTime(observedlocalDateTimeInUTC.format(dayamedGeneralFormatter));

		// Fetching data based on AcualdateTime and dosageID from
		// SchedulerJobs.
		jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC,
		                                          Long.parseLong(jsonobj.getString("deviceinfoid")), jsonobj.getString("consumptionstatus")
		                                                                                                    .trim());
		// end update consumption status in schedulerjob
		// Fetching data based on AcualdateTime and dosageID
		prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
		                                                                   Long.parseLong(jsonobj.getString("deviceinfoid")),
		                                                                   jsonobj.getString("consumptionstatus")
		                                                                          .trim());
		// end Adding scale datapoint

		hrmodel = biometricService.addHeartRate(heartRateModel);
		String msg = ApplicationConstants.EMPTY_STRING;
		if (hrmodel != null) {
			//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(hrmodel.getDeviceInfoId()));
			Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository.findById(Long.parseLong(hrmodel.getDeviceInfoId()));
			if (optionaldeviceInfo.isPresent()) {
				if (optionaldeviceInfo.get()
				                      .getDeviceNotificationList() != null && optionaldeviceInfo.get()
				                                                                                .getDeviceNotificationList()
				                                                                                .size() > 0
						&& optionaldeviceInfo.get()
						                     .getDeviceNotificationList()
						                     .get(0) != null) {

					// handling greaterthan and lessthan readings
					if (optionaldeviceInfo.get()
					                      .getDeviceNotificationList()
					                      .get(0)
					                      .getGreaterThanReading1() != null
							&& !(Float.parseFloat(jsonobj.getString("reading")
							                             .trim()) > (Float.parseFloat(
							optionaldeviceInfo.get()
							                  .getDeviceNotificationList()
							                  .get(0)
							                  .getGreaterThanReading1())))) {
						msg = " Your HeartRate reading has gone lessthan the specified range "
								+ optionaldeviceInfo.get()
								                    .getDeviceNotificationList()
								                    .get(0)
								                    .getGreaterThanReading1();
					}
					if (optionaldeviceInfo.get()
					                      .getDeviceNotificationList()
					                      .get(0)
					                      .getLessThanReading1() != null
							&& !(Float.parseFloat(jsonobj.getString("reading")
							                             .trim()) < (Float.parseFloat(
							optionaldeviceInfo.get()
							                  .getDeviceNotificationList()
							                  .get(0)
							                  .getLessThanReading1())))) {
						msg = " HeartRate reading has gone beyond the specified range "
								+ optionaldeviceInfo.get()
								                    .getDeviceNotificationList()
								                    .get(0)
								                    .getLessThanReading1();
					}
				}
			}
		}

		// sending Mail to Patient
		if (hrmodel != null) {
			Long userId = patientModel.getUserDetails()
			                          .getId();
			String userLangPreference = utility.getUserPreferedLang(userId);
			Map<String, String> msgResourceReplaceKeyVal =
					messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonobj, "reading", jsonobj.getString("reading"));
			String bodyMsg =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.heart.you", userLangPreference,
					                                                              null);
			String emailSub =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "emailsubject.biometricstatus", userLangPreference,
					                                                              null);
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(patientModel.getUserDetails()
			                              .getEmailId());
			emailAndSmsUtility.sendEmail(emailSub, bodyMsg, recipientList);

			emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
			                                                .getCountryCode()
					, patientModel.getUserDetails()
					              .getMobileNumber());

			// Adding notification to Notification table. for history in
			// calender.
			notificationService.addWebNotification(patientModel.getUserDetails()
			                                                   .getId(), bodyMsg,
			                                       ApplicationConstants.DEVICE_STATUS_WIT_, ApplicationConstants.VITAL_TAKEN_WIT);
			//sending mobile notifation to patient
			notificationService.sendMobileNotification(patientModel.getUserDetails()
			                                                       .getType(), bodyMsg, ApplicationConstants.VITAL_TAKEN, patientModel.getUserDetails()
			                                                                                                                          .getToken());
			// sending notification and mails to caregiver
			String patientName = patientModel.getUserDetails()
			                                 .getFirstName() + " "
					+ patientModel.getUserDetails()
					              .getLastName();
			msgResourceReplaceKeyVal.put("reading", heartRateModel.getReadingValue());
			msgResourceReplaceKeyVal.put("patientName", patientName);
			bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.heart.patientname",
			                                                                        userLangPreference, null);
			List<String> caregiverMailList = new ArrayList<>();
			for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
				caregiverMailList.add(caregiverModel.getUserDetails()
				                                    .getEmailId());
				emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
				                                                  .getCountryCode(), caregiverModel.getUserDetails()
				                                                                                   .getMobileNumber());
				notificationService.addWebNotification(caregiverModel.getUserDetails()
				                                                     .getId(),
				                                       bodyMsg, ApplicationConstants.DEVICE_STATUS_WIT_, ApplicationConstants.VITAL_TAKEN_WIT);
				if (caregiverModel.getUserDetails()
				                  .getType() != null &&
						(caregiverModel.getUserDetails()
						               .getType()
						               .equalsIgnoreCase(ApplicationConstants.ANDROID) || caregiverModel.getUserDetails()
						                                                                                .getType()
						                                                                                .equalsIgnoreCase(ApplicationConstants.IOS))) {
					notificationService.sendMobileNotification(caregiverModel.getUserDetails()
					                                                         .getType(), bodyMsg, ApplicationConstants.VITAL_TAKEN,
					                                           caregiverModel.getUserDetails()
					                                                         .getToken());
				}
			}
			if (caregiverMailList.size() > 0) {
				emailAndSmsUtility.sendEmail("emailsubject.medicinestatus", "biometric.test.heart.patientname", msgResourceReplaceKeyVal, caregiverMailList);
			}
		}
		return new ResponseEntity<>(hrmodel, HttpStatus.OK);
	}

	// add Gloco Reading data
	@PostMapping(value = "/addglucose", produces = "application/json", consumes = "application/json")
	public ResponseEntity<GlucometerModel> addGlucoReading(@RequestBody String glocoreading) throws JSONException {
		/*
		 * { "observedtime": "2018-03-21 12:54", "reading": "128.0 mm/dL",
		 * "patientid": "97547", "zoneid": "Asia/Calcutta", "latitude":
		 * "56.130721", "longitude": "106.346337", "deviceinfoid": "2",
		 * "prescriptionid": 1, "prescribedtime": "2015-07-16 17:07",
		 * "consumptionstatus": "taken/delayed" }
		 */
		JSONObject jsonobj = new JSONObject(glocoreading);
		if (!jsonobj.has("prescribedtime") || !jsonobj.has("zoneid") || !jsonobj.has("patientid") || !jsonobj.has("prescriptionid") ||
				!jsonobj.has("deviceinfoid") || !jsonobj.has("consumptionstatus") || !jsonobj.has("reading")
				|| !jsonobj.has("observedtime")) {
			logger.error("Missing Json data");
			return new ResponseEntity<GlucometerModel>(HttpStatus.BAD_REQUEST);
		}
		LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
				                                            .trim());
		if (jsonobj.has("deviceinfoid")) {
			//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(jsonobj.getString("deviceinfoid")));
			Optional<DeviceInfo> optionalDeviceInfo = deviceInfoRepository.findById(Long.parseLong(jsonobj.getString("deviceinfoid")));
			if (optionalDeviceInfo.isPresent()) {
				if (!optionalDeviceInfo.get()
				                       .getDevice()
				                       .getName()
				                       .equalsIgnoreCase("Gluco Meter")) {
					logger.error("provided deviceinfi id is not glucometer");
					return new ResponseEntity<GlucometerModel>(HttpStatus.BAD_REQUEST);
				}
			} else {
				logger.error("deviceinfo not available in DB with provided deviceinfoid");
				return new ResponseEntity<GlucometerModel>(HttpStatus.BAD_REQUEST);
			}
			if (optionalDeviceInfo.get()
			                      .getPrescription()
			                      .getId() != jsonobj.getLong("prescriptionid") || optionalDeviceInfo.get()
			                                                                                         .getPrescription()
			                                                                                         .getPatient()
			                                                                                         .getId() !=
					Long.parseLong(jsonobj.getString("patientid"))) {
				logger.error("either patient id or deviceinfo id not beloging to this patient");
				return new ResponseEntity<GlucometerModel>(HttpStatus.BAD_REQUEST);
			}

		}
		GlucometerModel glucometerModel = biometricService
				.fetchGlucometerByPrescribedTimeAndDeviceInfoId(
						dateUtility.convertdatetimeStringToLocalDateTime(
								prescribedLDTInUTC.format(dayamedGeneralFormatter)),
						jsonobj.getString("deviceinfoid"));
		if (glucometerModel == null) {
			glucometerModel = new GlucometerModel();
			glucometerModel.setDeviceInfoId(jsonobj.getString("deviceinfoid"));

			glucometerModel.setPrescriptionID(jsonobj.getLong("prescriptionid"));

			glucometerModel.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));
		}
		glucometerModel.setReadingValue(jsonobj.getString("reading")
		                                       .trim());
		glucometerModel.getConsumptionStatus()
		               .add(jsonobj.getString("consumptionstatus"));

		ArrayList<String> location = new ArrayList<String>();
		if (jsonobj.has("latitude")) {
			location.add(jsonobj.getString("latitude"));
		}
		if (jsonobj.has("longitude")) {
			location.add(jsonobj.getString("longitude"));
		}
		glucometerModel.setLocation(location);

		PatientModel patientModel = utility.convertPatientEntityTopatientModel(
				patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
		if (patientModel.getId() == 0) {
			return new ResponseEntity<GlucometerModel>(HttpStatus.BAD_REQUEST);
		}
		glucometerModel.setPatient(patientModel);

		LocalDateTime observedLocalDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("observedtime")
				       .trim(), jsonobj.getString("zoneid")
				                       .trim());
		glucometerModel.setObservedTime(observedLocalDateTimeInUTC.format(dayamedGeneralFormatter));

		// Fetching data based on AcualdateTime and dosage, to update
		// consumption status in schedulerjob
		jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC,
		                                          Long.parseLong(jsonobj.getString("deviceinfoid")), jsonobj.getString("consumptionstatus")
		                                                                                                    .trim());
		// end update consumption status in schedulerjob
		// Fetching data based on AcualdateTime and dosageID from
		// PrescriptionSchedule
		prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
		                                                                   Long.parseLong(jsonobj.getString("deviceinfoid")),
		                                                                   jsonobj.getString("consumptionstatus")
		                                                                          .trim());
		// end Adding scale datapoint

		GlucometerModel persistedglucometerModel = biometricService.addGlucometer(glucometerModel);
		// sending Mail to Patient

		if (persistedglucometerModel != null) {
			Long userId = patientModel.getUserDetails()
			                          .getId();
			String userLangPreference = utility.getUserPreferedLang(userId);
			Map<String, String> msgResourceReplaceKeyVal =
					messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonobj, "reading", jsonobj.getString("reading"));
			String bodyMsg =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.glucose.you", userLangPreference,
					                                                              null);
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(patientModel.getUserDetails()
			                              .getEmailId());
			String emailSub =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "emailsubject.biometricstatus", userLangPreference,
					                                                              null);
			emailAndSmsUtility.sendEmail(emailSub, bodyMsg, recipientList);

			emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
			                                                .getCountryCode()
					, patientModel.getUserDetails()
					              .getMobileNumber());
			// Adding notification in Notification table. for history in calender.
			notificationService.addWebNotification(patientModel.getUserDetails()
			                                                   .getId(), bodyMsg,
			                                       ApplicationConstants.DEVICE_STATUS_WIT_, ApplicationConstants.VITAL_TAKEN_WIT);
			//sending notification to patient
			notificationService.sendMobileNotification(patientModel.getUserDetails()
			                                                       .getType(), bodyMsg, ApplicationConstants.VITAL_TAKEN, patientModel.getUserDetails()
			                                                                                                                          .getToken());
			// sending notification and mails to caregiver
			String patientName = patientModel.getUserDetails()
			                                 .getFirstName() + " "
					+ patientModel.getUserDetails()
					              .getLastName();
			msgResourceReplaceKeyVal.put("patientName", patientName);
			bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.glucose.patientname",
			                                                                        userLangPreference, null);
			List<String> caregiverMailList = new ArrayList<>();
			for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
				caregiverMailList.add(caregiverModel.getUserDetails()
				                                    .getEmailId());
				emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
				                                                  .getCountryCode(), caregiverModel.getUserDetails()
				                                                                                   .getMobileNumber());
				notificationService.addWebNotification(caregiverModel.getUserDetails()
				                                                     .getId(),
				                                       bodyMsg, ApplicationConstants.VITAL_TAKEN, ApplicationConstants.DEVICE_STATUS_WIT_);
				if (caregiverModel.getUserDetails()
				                  .getType() != null &&
						(caregiverModel.getUserDetails()
						               .getType()
						               .equalsIgnoreCase(ApplicationConstants.ANDROID) || caregiverModel.getUserDetails()
						                                                                                .getType()
						                                                                                .equalsIgnoreCase(ApplicationConstants.IOS))) {
					notificationService.sendMobileNotification(caregiverModel.getUserDetails()
					                                                         .getType(), bodyMsg, ApplicationConstants.VITAL_TAKEN,
					                                           caregiverModel.getUserDetails()
					                                                         .getToken());
				}
			}
			if (caregiverMailList.size() > 0) {
				//emailAndSmsUtility.sendEmail(ApplicationConstants.DEVICE_STATUS, bodyMsg, caregiverMailList);
				emailAndSmsUtility.sendEmail("emailsubject.devicestatus", "biometric.test.glucose.patientname", msgResourceReplaceKeyVal, caregiverMailList);
			}
		}
		return new ResponseEntity<GlucometerModel>(persistedglucometerModel, HttpStatus.OK);
	}

	@PostMapping(value = "/addscale", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ScaleModel> addScale(@RequestBody String scalemodel) throws JSONException {
		/*
		 * { "observedtime": "2018-03-21 12:54", "reading": "65", "patientid":
		 * "33256", "zoneid": "Asia/Calcutta", "latitude": "56.130721",
		 * "longitude": "106.346337", "deviceinfoid": "1", "prescriptionid": 1,
		 * "prescribedtime": "2015-07-16 17:07", "consumptionstatus":"taken" }
		 */
		JSONObject jsonobj = new JSONObject(scalemodel);
		if (!jsonobj.has("reading") || !jsonobj.has("prescribedtime") || !jsonobj.has("zoneid") || !jsonobj.has("deviceinfoid")
				|| !jsonobj.has("prescriptionid") || !jsonobj.has("patientid") || !jsonobj.has("observedtime") || !jsonobj.has("consumptionstatus")) {
			logger.error("missinfg required data while adding scale");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		String scaleReading = decimalFormat.format(Double.parseDouble(jsonobj.getString("reading")
		                                                                     .trim()));
		LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
				                                            .trim());

		//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(jsonobj.getString("deviceinfoid")));
		Optional<DeviceInfo> optionalDeviceInfo = deviceInfoRepository.findById(Long.parseLong(jsonobj.getString("deviceinfoid")));
		if (optionalDeviceInfo.isPresent()) {
			if (!optionalDeviceInfo.get()
			                       .getDevice()
			                       .getName()
			                       .equalsIgnoreCase("Scale")) {
				logger.error("provided devicceinfo id not scale");
				return new ResponseEntity<ScaleModel>(HttpStatus.BAD_REQUEST);
			}
		} else {
			logger.error("deviceinfo not available with provided deviceinfo id");
			return new ResponseEntity<ScaleModel>(HttpStatus.BAD_REQUEST);
		}
		if (optionalDeviceInfo.get()
		                      .getPrescription()
		                      .getId() != jsonobj.getLong("prescriptionid") || optionalDeviceInfo.get()
		                                                                                         .getPrescription()
		                                                                                         .getPatient()
		                                                                                         .getId() != Long.parseLong(jsonobj.getString("patientid"))) {
			return new ResponseEntity<ScaleModel>(HttpStatus.BAD_REQUEST);
		}

		ScaleModel scaleModel = biometricService
				.fetchScaleByPrescribedTimeAndDeviceInfoId(
						dateUtility.convertdatetimeStringToLocalDateTime(
								prescribedLDTInUTC.format(dayamedGeneralFormatter)),
						jsonobj.getString("deviceinfoid"));
		if (scaleModel == null) {
			scaleModel = new ScaleModel();
			scaleModel.setPrescriptionID(jsonobj.getLong("prescriptionid"));
			scaleModel.setDeviceInfoId(jsonobj.getString("deviceinfoid"));
			scaleModel.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));
		}
		PatientModel patientModel = utility.convertPatientEntityTopatientModel(
				patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
		if (patientModel.getId() == 0) {
			logger.error("patient not found with provided patient id");
			return new ResponseEntity<ScaleModel>(HttpStatus.BAD_REQUEST);
			//throw new ServiceException("patient not found");
		}
		scaleModel.setPatient(patientModel);
		scaleModel.setReading(scaleReading + " lbs");
		scaleModel.getConsumptionStatus()
		          .add(jsonobj.getString("consumptionstatus"));

		ArrayList<String> location = new ArrayList<String>();
		if (jsonobj.has("latitude")) {
			location.add(jsonobj.getString("latitude"));
		}
		if (jsonobj.has("longitude")) {
			location.add(jsonobj.getString("longitude"));
		}
		scaleModel.setLocation(location);

		LocalDateTime localDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("observedtime"), jsonobj.getString("zoneid"));
		scaleModel.setObservedTime(localDateTimeInUTC.format(dayamedGeneralFormatter));

		// Before Adding scale datapoint need to update schedular jobs

		// Fetching data based on AcualdateTime and dosage, to update
		// consumption status in schedulerjob
		jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC,
		                                          Long.parseLong(jsonobj.getString("deviceinfoid")), jsonobj.getString("consumptionstatus")
		                                                                                                    .trim());
		// end update consumption status in schedulerjob
		// Fetching data based on AcualdateTime and dosageID
		prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
		                                                                   Long.parseLong(jsonobj.getString("deviceinfoid")),
		                                                                   jsonobj.getString("consumptionstatus")
		                                                                          .trim());
		// end Adding scale datapoint

		ScaleModel persistedscaleModel = biometricService.addScale(scaleModel);

		//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(persistedscaleModel.getDeviceInfoId()));

		//have to check
		//Optional<DeviceInfo> optionalDeviceInfo =	deviceInfoRepository.findById(Long.parseLong(persistedscaleModel.getDeviceInfoId()));
		String msg = ApplicationConstants.EMPTY_STRING;
		Long userId = patientModel.getUserDetails()
		                          .getId();
		String userLangPreference = utility.getUserPreferedLang(userId);
		if (optionalDeviceInfo.isPresent()) {
			if (optionalDeviceInfo.get()
			                      .getDeviceNotificationList() != null
					&& optionalDeviceInfo.get()
					                     .getDeviceNotificationList()
					                     .size() > 0
					&& optionalDeviceInfo.get()
					                     .getDeviceNotificationList()
					                     .get(0) != null) {

				// handling greaterthan and lessthan readings
				if (optionalDeviceInfo.get()
				                      .getDeviceNotificationList()
				                      .get(0)
				                      .getGreaterThanReading1() != null
						&& !(Float.parseFloat(
						jsonobj.getString("reading")
						       .trim()) > (Float.parseFloat(optionalDeviceInfo.get()
						                                                      .getDeviceNotificationList()
						                                                      .get(0)
						                                                      .getGreaterThanReading1())))) {
					msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "scale.reading.less", userLangPreference,
					                                                                    null)
							+ optionalDeviceInfo.get()
							                    .getDeviceNotificationList()
							                    .get(0)
							                    .getGreaterThanReading1();
				}
				if (optionalDeviceInfo.get()
				                      .getDeviceNotificationList()
				                      .get(0)
				                      .getLessThanReading1() != null
						&& !(Float.parseFloat(
						jsonobj.getString("reading")
						       .trim()) < (Float.parseFloat(optionalDeviceInfo.get()
						                                                      .getDeviceNotificationList()
						                                                      .get(0)
						                                                      .getLessThanReading1())))) {
					msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "scale.reading.beyond", userLangPreference,
					                                                                    null)
							+ optionalDeviceInfo.get()
							                    .getDeviceNotificationList()
							                    .get(0)
							                    .getLessThanReading1();
				}
			}
		}

		if (persistedscaleModel != null) {
			List<String> recipientList = new ArrayList<String>();
			Map<String, String> msgResourceReplaceKeyVal =
					messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonobj, "reading", jsonobj.getString("reading"));
			String bodyMsg =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.scale.you", userLangPreference,
					                                                              null) + msg;
			recipientList.add(patientModel.getUserDetails()
			                              .getEmailId());
			emailAndSmsUtility.sendEmail(ApplicationConstants.BIOMETRIC_STATUS, bodyMsg, recipientList);

			emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
			                                                .getCountryCode()
					, patientModel.getUserDetails()
					              .getMobileNumber());
			// Adding notification to Notification table. for history in
			// calender.
			notificationService.addWebNotification(patientModel.getId(), bodyMsg, ApplicationConstants.DEVICE_STATUS_WIT_,
			                                       ApplicationConstants.VITAL_TAKEN_WIT);
			//sending notification to patient
			notificationService.sendMobileNotification(patientModel.getUserDetails()
			                                                       .getType(), bodyMsg, ApplicationConstants.VITAL_TAKEN, patientModel.getUserDetails()
			                                                                                                                          .getToken());
			// sending notification and mails to caregiver
			String patientName = patientModel.getUserDetails()
			                                 .getFirstName() + " "
					+ patientModel.getUserDetails()
					              .getLastName();
			bodyMsg = patientName + " have successfully taken his/her scale reading. count is: "
					+ persistedscaleModel.getReading() + ". prescribedTime is " + jsonobj.getString("prescribedtime")
					                                                                     .trim() + " and noticed at " + jsonobj.getString("observedtime")
					                                                                                                           .trim() + msg;
			List<String> caregiverMailList = new ArrayList<>();
			for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
				caregiverMailList.add(caregiverModel.getUserDetails()
				                                    .getEmailId());
				emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
				                                                  .getCountryCode(), caregiverModel.getUserDetails()
				                                                                                   .getMobileNumber());
				notificationService.addWebNotification(caregiverModel.getUserDetails()
				                                                     .getId(),
				                                       bodyMsg, "taken", "device_status");

			}
			if (caregiverMailList.size() > 0) {
				emailAndSmsUtility.sendEmail("emailsubject.biometricstatus", "biometric.test.scale.patientname", msgResourceReplaceKeyVal, recipientList);
			}
		}
		return new ResponseEntity<ScaleModel>(persistedscaleModel, HttpStatus.OK);
	}

	// add BP Reading
	@PostMapping(value = "/addbpreading", produces = "application/json", consumes = "application/json")
	public ResponseEntity<BpMonitorModel> addBpReading(@RequestBody String bpreading) throws JSONException {
		// sample json
		// {"observedtime":"2018-03-21 12:54","systolic":"128.0
		// mmHg","diastolic":"69.0
		// mmHg","patientid":"97547","zoneid":"Asia/Calcutta","latitude":
		// "56.130721",
		// "longitude": "106.346337","deviceinfoid":"2","prescriptionid":
		// 1,"prescribedtime":"2015-07-16
		// 17:07","consumptionstatus":"taken/delayed"}
		JSONObject jsonobj = new JSONObject(bpreading);
		if (!jsonobj.has("prescribedtime") || !jsonobj.has("zoneid") || !jsonobj.has("deviceinfoid") || !jsonobj.has("prescriptionid")
				|| !jsonobj.has("patientid") || !jsonobj.has("consumptionstatus")) {
			logger.error("Missing required data while adding bpreading");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
				                                            .trim());
		//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(jsonobj.getString("deviceinfoid")));
		Optional<DeviceInfo> optionalDeviceInfo = deviceInfoRepository.findById(Long.parseLong(jsonobj.getString("deviceinfoid")));
		if (optionalDeviceInfo.isPresent()) {
			if (!optionalDeviceInfo.get()
			                       .getDevice()
			                       .getName()
			                       .equalsIgnoreCase("BP Meter")) {
				logger.error("provided device info id not bpmeter");
				return new ResponseEntity<BpMonitorModel>(HttpStatus.BAD_REQUEST);
			}
		} else {
			logger.error("deviceInfo not availble with provided deviceinfo id while adding bpreading");
			return new ResponseEntity<BpMonitorModel>(HttpStatus.BAD_REQUEST);
		}
		if (optionalDeviceInfo.get()
		                      .getPrescription()
		                      .getId() != jsonobj.getLong("prescriptionid") || optionalDeviceInfo.get()
		                                                                                         .getPrescription()
		                                                                                         .getPatient()
		                                                                                         .getId() != Long.parseLong(jsonobj.getString("patientid"))) {
			logger.error("either deviceInfo or patient id not belonging to this");
			return new ResponseEntity<BpMonitorModel>(HttpStatus.BAD_REQUEST);
		}

		BpMonitorModel bpMonitorModel = biometricService
				.fetchBpMonitorByPrescribedTimeAndDeviceInfoId(
						dateUtility.convertdatetimeStringToLocalDateTime(
								prescribedLDTInUTC.format(dayamedGeneralFormatter)),
						jsonobj.getString("deviceinfoid"));
		if (bpMonitorModel == null) {
			bpMonitorModel = new BpMonitorModel();

			bpMonitorModel.setPrescriptionID(jsonobj.getLong("prescriptionid"));

			bpMonitorModel.setDeviceInfoId(jsonobj.getString("deviceinfoid"));

			bpMonitorModel.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));
		}
		bpMonitorModel.setSystolicpressureValue(jsonobj.getString("systolic")
		                                               .trim());
		bpMonitorModel.setDiastolicpressureValue(jsonobj.getString("diastolic")
		                                                .trim());

		bpMonitorModel.getConsumptionStatus()
		              .add(jsonobj.getString("consumptionstatus"));

		ArrayList<String> location = new ArrayList<String>();
		if (jsonobj.has("latitude")) {
			location.add(jsonobj.getString("latitude"));
		}
		if (jsonobj.has("longitude")) {
			location.add(jsonobj.getString("longitude"));
		}

		bpMonitorModel.setLocation(location);

		PatientModel patientModel = utility.convertPatientEntityTopatientModel(
				patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
		if (patientModel.getId() == 0) {
			return new ResponseEntity<BpMonitorModel>(HttpStatus.BAD_REQUEST);
		}
		bpMonitorModel.setPatient(patientModel);

		LocalDateTime localDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
				jsonobj.getString("observedtime")
				       .trim(), jsonobj.getString("zoneid")
				                       .trim());
		bpMonitorModel.setObservedTime(localDateTimeInUTC.format(dayamedGeneralFormatter));

		// Before Adding BP datapoint need to update schedular jobs

		// Fetching data based on AcualdateTime and dosage, to update
		// consumption status in schedulerjob
		jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC,
		                                          Long.parseLong(jsonobj.getString("deviceinfoid")), jsonobj.getString("consumptionstatus")
		                                                                                                    .trim());
		// end update consumption status in schedulerjob

		// To update consumption status in PrescriptionSchedule table
		prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
		                                                                   Long.parseLong(jsonobj.getString("deviceinfoid")),
		                                                                   jsonobj.getString("consumptionstatus")
		                                                                          .trim());
		// end update consumption status in PrescriptionSchedule table

		BpMonitorModel persistedbpMonitorModel = biometricService.addBpReading(bpMonitorModel);

		String msg = ApplicationConstants.EMPTY_STRING;
		String msg2 = ApplicationConstants.EMPTY_STRING;
		if (optionalDeviceInfo.isPresent()) {
			if (optionalDeviceInfo.get()
			                      .getDeviceNotificationList() != null
					&& optionalDeviceInfo.get()
					                     .getDeviceNotificationList()
					                     .size() > 0
					&& optionalDeviceInfo.get()
					                     .getDeviceNotificationList()
					                     .get(0) != null) {
				if (optionalDeviceInfo.get()
				                      .getDeviceNotificationList()
				                      .get(0)
				                      .getGreaterThanReading1() != null
						&& !(Double.parseDouble(jsonobj.getString("systolic")
						                               .trim()) > (Double.parseDouble(
						optionalDeviceInfo.get()
						                  .getDeviceNotificationList()
						                  .get(0)
						                  .getGreaterThanReading1())))) {
					msg = " Your systolic value has gone beyond the specified range "
							+ optionalDeviceInfo.get()
							                    .getDeviceNotificationList()
							                    .get(0)
							                    .getGreaterThanReading1();
				}
				if (optionalDeviceInfo.get()
				                      .getDeviceNotificationList()
				                      .get(0)
				                      .getGreaterThanReading2() != null
						&& !(Double.parseDouble(jsonobj.getString("diastolic")
						                               .trim()) > (Double.parseDouble(
						optionalDeviceInfo.get()
						                  .getDeviceNotificationList()
						                  .get(0)
						                  .getGreaterThanReading2())))) {
					msg2 = " Your diastolic value has gone beyond the specified range "
							+ optionalDeviceInfo.get()
							                    .getDeviceNotificationList()
							                    .get(0)
							                    .getGreaterThanReading2();
				}

				if (optionalDeviceInfo.get()
				                      .getDeviceNotificationList()
				                      .get(0)
				                      .getLessThanReading1() != null
						&& !(Double.parseDouble(jsonobj.getString("systolic")
						                               .trim()) < (Double
						.parseDouble(optionalDeviceInfo.get()
						                               .getDeviceNotificationList()
						                               .get(0)
						                               .getLessThanReading1())))) {
					msg = " Your systolic value has gone lessthan the specified range "
							+ optionalDeviceInfo.get()
							                    .getDeviceNotificationList()
							                    .get(0)
							                    .getLessThanReading1();
				}
				if (optionalDeviceInfo.get()
				                      .getDeviceNotificationList()
				                      .get(0)
				                      .getLessThanReading2() != null
						&& !(Double.parseDouble(jsonobj.getString("diastolic")
						                               .trim()) < (Double
						.parseDouble(optionalDeviceInfo.get()
						                               .getDeviceNotificationList()
						                               .get(0)
						                               .getLessThanReading2())))) {
					msg2 = " Your diastolic value has gone lessthan the specified range "
							+ optionalDeviceInfo.get()
							                    .getDeviceNotificationList()
							                    .get(0)
							                    .getLessThanReading2();
				}

			}
		}
		// sending mail to patient
		if (persistedbpMonitorModel != null) {
			List<String> recipientList = new ArrayList<String>();
			recipientList.add(patientModel.getUserDetails()
			                              .getEmailId());

			Long userId = patientModel.getUserDetails()
			                          .getId();
			String userLangPreference = utility.getUserPreferedLang(userId);
			Map<String, String> msgResourceReplaceKeyVal =
					messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonobj, "systolic", jsonobj.getString("systolic"));
			msgResourceReplaceKeyVal.put("diastolic", jsonobj.getString("diastolic")
			                                                 .trim());
			String bodyMsg =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.bp.you", userLangPreference,
					                                                              null);
			String emailSub = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "emailsubject.biometricstatus", userLangPreference,
			                                                                                null);
			emailAndSmsUtility.sendEmail(emailSub, bodyMsg, recipientList);

			emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
			                                                .getCountryCode()
					, patientModel.getUserDetails()
					              .getMobileNumber());
			// Adding notification to Notification table. for history in
			// calender.
			notificationService.addWebNotification(patientModel.getUserDetails()
			                                                   .getId(), bodyMsg,
			                                       ApplicationConstants.DEVICE_STATUS_WIT_, ApplicationConstants.VITAL_TAKEN_WIT);
			// sending notification and mails to caregiver
			String patientName = patientModel.getUserDetails()
			                                 .getFirstName() + " "
					+ patientModel.getUserDetails()
					              .getLastName();
			msgResourceReplaceKeyVal.put("patientName", patientName);
			msgResourceReplaceKeyVal.put("systolic", persistedbpMonitorModel.getDiastolicpressureValue());
			msgResourceReplaceKeyVal.put("diastolic", persistedbpMonitorModel.getSystolicpressureValue());
			bodyMsg =
					messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "biometric.test.bp.patientname",
					                                                              userLangPreference,
					                                                              null);
			List<String> caregiverMailList = new ArrayList<>();
			for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
				caregiverMailList.add(caregiverModel.getUserDetails()
				                                    .getEmailId());
				emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
				                                                  .getCountryCode(), caregiverModel.getUserDetails()
				                                                                                   .getMobileNumber());

				notificationService.addWebNotification(caregiverModel.getUserDetails()
				                                                     .getId(),
				                                       bodyMsg, "taken", "device_status");
			}
			if (caregiverMailList.size() > 0) {
				emailAndSmsUtility.sendEmail("emailsubject.biometricstatus", "biometric.test.bp.patientname", msgResourceReplaceKeyVal, recipientList);
			}
		}
		return new ResponseEntity<BpMonitorModel>(persistedbpMonitorModel, HttpStatus.OK);
	}

	// fetchGlocometerReadingsListBypatientId- Mobile
	@GetMapping(value = "/{patientid}/glucoreadings", produces = "application/json")
	@ApiIgnore
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<BiometricSuperJsonModel> fetchGlucoMtrReadingsBypatientId(
			@PathVariable(value = "patientid") String patientid) {
		// ideal readings <99 fasting, <140 post diet
		List<GlucometerModel> glucometerModels = biometricService
				.fetchGlocoReadingsByPatientId(Long.parseLong(patientid));
		if (glucometerModels.size() > 0) {
			List<BiometricJsonModel> glucoJsonvalues = new ArrayList<BiometricJsonModel>();
			BiometricSuperJsonModel glucoSuperJsonModel = new BiometricSuperJsonModel();
			glucometerModels.forEach(glucoModel -> {
				BiometricJsonModel glucojsonModel = new BiometricJsonModel();

				glucojsonModel.setName(
						dateUtility.getFormattedLocalDateTime(LocalDateTime.parse(glucoModel.getObservedTime())));
				glucojsonModel.setValue(glucoModel.getReadingValue());
				glucoJsonvalues.add(glucojsonModel);
			});

			glucoSuperJsonModel.setName("Gluco-Meter");
			glucoSuperJsonModel.setSeries(glucoJsonvalues);
			return new ResponseEntity<BiometricSuperJsonModel>(glucoSuperJsonModel, HttpStatus.OK);
		}
		return new ResponseEntity<BiometricSuperJsonModel>(HttpStatus.NO_CONTENT);
	}

	// New code
	@GetMapping(value = "/imantic/glucoreadings/{patientid}", produces = "application/json")
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<Map<String, Map<String, String>>> fetchImanticGlucoseReadingsBypatientIdFromImantics(
			@PathVariable(value = "patientid") String patientid, @RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate) {

		if (patientid == null || "".equals(patientid)) {
			logger.error("Missing Json data");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Patient patient = patientService.fetchPatientById(Long.parseLong(patientid));

		Map<String, String> valueAndTime = new HashMap<>();
		Map<String, Map<String, String>> glocoReadings = new HashMap<>();
		ArrayList<String> glocoBiometrics = new ArrayList<String>();
		glocoBiometrics.add("glucose");
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy,HH:mm:ss aaa");
		Date startDate = null;
		Date endDate = null;
		List<PidDataTable> biometricDataTableList = new ArrayList<PidDataTable>();
		try {
			if (startdate != null) {
				startDate = formatter.parse(startdate);
			} else {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -180);
				startDate = new Date(cal.getTimeInMillis());
			}
			if (enddate != null) {
				endDate = formatter.parse(enddate);
			} else {
				endDate = new Date();
			}
		} catch (ParseException e) {
			logger.error("Exception occurred when parsing dates", e);
		}
		if (patient.getUserDetails() != null) {
			biometricDataTableList = imanticService.fetchBiometricData(patient.getUserDetails()
			                                                                  .getImanticUserid(),
			                                                           startDate, endDate, glocoBiometrics);
			// glocoReadings
			for (PidDataTable pidDataTable : biometricDataTableList) {
				for (PidValue pidValue : pidDataTable.getValues()) {
					valueAndTime.put(pidValue.getValue(),
					                 DateUtils.toISO8601(new Date(Long.parseLong(pidValue.getTimestamp()))));
				}
				glocoReadings.put(pidDataTable.getPid(), valueAndTime);
			}
		}
		return new ResponseEntity<Map<String, Map<String, String>>>(glocoReadings, HttpStatus.OK);
	}

	@GetMapping(value = "imantic/scale/graph/bypatientid/{patientid}", produces = "application/json")
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<LatestGraphSuperJsonModel> fetchImanticScaleReadingsBypatientIdFromImantics(
			@PathVariable(value = "patientid") String patientid, @RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate, @RequestParam("zoneid") String zoneid) {
		// input: 62784, 2018-05-01 2018-08-06
		String startTime = " 00:00";
		String endTime = " 23:59";
		if (patientid == null || "".equals(patientid.trim()) || startdate == null || "".equals(startdate.trim()) ||
				enddate == null || "".equals(enddate.trim()) || zoneid == null || "".equals(zoneid.trim())) {
			logger.error("Missing required input data");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		LocalDateTime startLocalDateTimeObjInUTC = dateUtility
				.convertLocalTimeToUTCTimeObjectByzoneid(startdate + startTime, zoneid);
		LocalDateTime endLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(enddate + endTime,
		                                                                                             zoneid);

		Date strdateObjinUTC = java.util.Date
				.from(startLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
				                                .toInstant());

		Date enddateObjinUTC = java.util.Date.from(endLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
		                                                                   .toInstant());

		Patient patient = patientService.fetchPatientById(Long.parseLong(patientid));
		ArrayList<String> scaleBiometrics = new ArrayList<String>();
		scaleBiometrics.add("weight");

		List<PidDataTable> biometricDataTableList = new ArrayList<PidDataTable>();
		List<PidDataTable> predictedBiometricDataTableList = new ArrayList<PidDataTable>();

		if (patient != null && patient.getUserDetails() != null) {
			// String patinetid = "473ceda6c6bb43ef891b4c18e1c2cc8e";
			String patinetid = patient.getUserDetails()
			                          .getImanticUserid();

			biometricDataTableList = imanticService.fetchBiometricData(patinetid, strdateObjinUTC, enddateObjinUTC,
			                                                           scaleBiometrics);

			predictedBiometricDataTableList = imanticService.fetchPredictedBiometricData(patinetid, strdateObjinUTC,
			                                                                             enddateObjinUTC, scaleBiometrics);
			NumberFormat decimalFormat = new DecimalFormat("#0.00");
			// scale Readings
			Map<String, List<Map<String, String>>> bioMetricReadings = new HashMap<>();
			List<Map<String, String>> scaleValueMapList = new ArrayList<>();
			for (PidDataTable pidDataTable : biometricDataTableList) {
				for (PidValue pidValue : pidDataTable.getValues()) {
					Map<String, String> valueMap = new LinkedHashMap<>();
					valueMap.put("x", dateformatterForGraph
							.format(new Date(new Timestamp(Long.parseLong(pidValue.getTimestamp())).getTime())));
					valueMap.put("y", pidValue.getValue()
					                          .replace("lbs", "")
					                          .trim());
					scaleValueMapList.add(valueMap);
				}
				bioMetricReadings.put(pidDataTable.getPid(), scaleValueMapList);
			}
			Collections.sort(scaleValueMapList, new ListMapComparatorByDate());

			// pscale Readings
			Map<String, List<Map<String, String>>> predictedbioMetricReadings = new HashMap<>();
			List<Map<String, String>> pscalevalueMapList = new ArrayList<>();
			for (PidDataTable pidDataTable : predictedBiometricDataTableList) {
				for (PidValue pidValue : pidDataTable.getValues()) {
					Map<String, String> valueMap = new LinkedHashMap<>();
					valueMap.put("x", dateformatterForGraph
							.format(new Date(new Timestamp(Long.parseLong(pidValue.getTimestamp())).getTime())));
					valueMap.put("y", decimalFormat.format(Double.parseDouble(pidValue.getValue()
					                                                                  .replace("lbs", "")
					                                                                  .trim())));
					pscalevalueMapList.add(valueMap);
				}
				predictedbioMetricReadings.put(pidDataTable.getPid(), pscalevalueMapList);
			}
			Collections.sort(pscalevalueMapList, new ListMapComparatorByDate());
			// Analytics
			ArrayList<String> analyticNameList = new ArrayList<String>();
			analyticNameList.add("projectedAdherence");
			analyticNameList.add("currentAdherence");

			Map<String, List<Map<String, String>>> analyticsReadingsMap = imanticService.fetchAnalyticsMap(patinetid,
			                                                                                               strdateObjinUTC, enddateObjinUTC, analyticNameList);

			LatestGraphSuperJsonModel latestgraphSuperJsonModel = new LatestGraphSuperJsonModel();
			List<LatestGraphJsonModel> latestscaleDatasetvalues = new ArrayList<LatestGraphJsonModel>();

			// analyticsMap.forEach((k,v)->System.out.println("Key: " + k +
			// "Value: " + v));
			// projectedAdherence
			LatestGraphJsonModel projectedAnalyticGraphJsonModel = new LatestGraphJsonModel();
			projectedAnalyticGraphJsonModel.setLabel("PAdherence");
			List<Map<String, String>> pAdheranceDataMapList = new ArrayList<>();
			pAdheranceDataMapList = analyticsReadingsMap.get("projectedAdherence");
			projectedAnalyticGraphJsonModel.setData(pAdheranceDataMapList);
			latestscaleDatasetvalues.add(projectedAnalyticGraphJsonModel);

			// currentAdherence
			LatestGraphJsonModel currentAdherenceAnalyticGraphJsonModel = new LatestGraphJsonModel();
			currentAdherenceAnalyticGraphJsonModel.setLabel("CurrentAdherence");
			List<Map<String, String>> currentAdheranceDataMapList = new ArrayList<>();
			currentAdheranceDataMapList = analyticsReadingsMap.get("currentAdherence");
			currentAdherenceAnalyticGraphJsonModel.setData(currentAdheranceDataMapList);
			latestscaleDatasetvalues.add(currentAdherenceAnalyticGraphJsonModel);

			// Scale
			LatestGraphJsonModel ScalelatestGraphJsonModel = new LatestGraphJsonModel();
			ScalelatestGraphJsonModel.setLabel("Scale");
			List<Map<String, String>> scaleDataMapList = new LinkedList<Map<String, String>>();
			scaleDataMapList = bioMetricReadings.get("weight");
			ScalelatestGraphJsonModel.setData(scaleDataMapList);
			latestscaleDatasetvalues.add(ScalelatestGraphJsonModel);

			// PScale
			LatestGraphJsonModel PScaleGraphJsonModel = new LatestGraphJsonModel();
			PScaleGraphJsonModel.setLabel("Pscale");
			List<Map<String, String>> pscaleDataMapList = new LinkedList<Map<String, String>>();
			pscaleDataMapList = predictedbioMetricReadings.get("weight");
			PScaleGraphJsonModel.setData(pscaleDataMapList);
			latestscaleDatasetvalues.add(PScaleGraphJsonModel);

			latestgraphSuperJsonModel.setDatasets(latestscaleDatasetvalues);
			return new ResponseEntity<LatestGraphSuperJsonModel>(latestgraphSuperJsonModel, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	//has to change url pattern
	@GetMapping(value = "imantic/bpreadings/{patientid}", produces = "application/json")
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<LatestGraphSuperJsonModel> fetchImanticBpReadingsBypatientId(
			@PathVariable(value = "patientid") String patientid, @RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate, @RequestParam("zoneid") String zoneid) {
		// input: 62784, 2018-05-01 ,2018-08-06, Asia/Calcutta

		String startTime = " 00:00";
		String endTime = " 23:59";
		if (patientid == null || "".equals(patientid.trim()) || zoneid == null || "".equals(zoneid.trim()) || startdate == null || "".equals(startdate.trim())
				|| enddate == null || "".equals(enddate.trim())) {
			logger.error("Missing input data");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		LocalDateTime startLocalDateTimeObjInUTC = dateUtility
				.convertLocalTimeToUTCTimeObjectByzoneid(startdate + startTime, zoneid);
		LocalDateTime endLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(enddate + endTime,
		                                                                                             zoneid);

		Date strdateObjinUTC = java.util.Date
				.from(startLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
				                                .toInstant());

		Date enddateObjinUTC = java.util.Date.from(endLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
		                                                                   .toInstant());

		Patient patient = patientService.fetchPatientById(Long.parseLong(patientid));
		ArrayList<String> bpBiometrics = new ArrayList<String>();
		bpBiometrics.add("bpDia");
		bpBiometrics.add("bpSys");

		List<PidDataTable> biometricDataTableList = new ArrayList<PidDataTable>();
		List<PidDataTable> predictedBiometricDataTableList = new ArrayList<PidDataTable>();

		if (patient != null && patient.getUserDetails() != null) {
			biometricDataTableList = imanticService.fetchBiometricData(patient.getUserDetails()
			                                                                  .getImanticUserid(),
			                                                           strdateObjinUTC, enddateObjinUTC, bpBiometrics);

			predictedBiometricDataTableList = imanticService.fetchPredictedBiometricData(
					patient.getUserDetails()
					       .getImanticUserid(), strdateObjinUTC, enddateObjinUTC, bpBiometrics);

			// sys and dis data
			Map<String, List<Map<String, String>>> bioMetricReadings = new HashMap<>();
			List<Map<String, String>> bpValueMapList = new ArrayList<>();
			for (PidDataTable pidDataTable : biometricDataTableList) {
				for (PidValue pidValue : pidDataTable.getValues()) {
					Map<String, String> valueMap = new LinkedHashMap<>();
					valueMap.put("x", dateformatterForGraph
							.format(new Date(new Timestamp(Long.parseLong(pidValue.getTimestamp())).getTime())));
					valueMap.put("y", pidValue.getValue()
					                          .trim());
					bpValueMapList.add(valueMap);
				}
				bioMetricReadings.put(pidDataTable.getPid(), bpValueMapList);
			}
			Collections.sort(bpValueMapList, new ListMapComparatorByDate());
			// PData
			Map<String, List<Map<String, String>>> predictedbioMetricReadings = new HashMap<>();
			List<Map<String, String>> pvalueMapList = new ArrayList<>();
			for (PidDataTable pidDataTable : predictedBiometricDataTableList) {
				for (PidValue pidValue : pidDataTable.getValues()) {
					Map<String, String> valueMap = new LinkedHashMap<>();
					valueMap.put("x", dateformatterForGraph
							.format(new Date(new Timestamp(Long.parseLong(pidValue.getTimestamp())).getTime())));
					valueMap.put("y", decimalFormat.format(Double.parseDouble(pidValue.getValue()
					                                                                  .trim())));
					pvalueMapList.add(valueMap);
				}
				predictedbioMetricReadings.put(pidDataTable.getPid(), pvalueMapList);
			}
			Collections.sort(pvalueMapList, new ListMapComparatorByDate());

			ArrayList<String> analyticNameList = new ArrayList<String>();
			analyticNameList.add("projectedAdherence");
			analyticNameList.add("currentAdherence");
			Map<String, List<Map<String, String>>> analyticsReadingsMap = imanticService.fetchAnalyticsMap(
					patient.getUserDetails()
					       .getImanticUserid(), strdateObjinUTC, enddateObjinUTC, analyticNameList);

			LatestGraphSuperJsonModel graphSuperJsonModel = new LatestGraphSuperJsonModel();
			List<LatestGraphJsonModel> bpDatasetvalues = new ArrayList<LatestGraphJsonModel>();

			// analyticsMap.forEach((k,v)->System.out.println("Key: " + k +
			// "Value: " + v));
			// projectedAdherence
			LatestGraphJsonModel projectedAnalyticGraphJsonModel = new LatestGraphJsonModel();
			projectedAnalyticGraphJsonModel.setLabel("PAdherence");
			List<Map<String, String>> pAdheranceDataMapList = new ArrayList<>();
			pAdheranceDataMapList = analyticsReadingsMap.get("projectedAdherence");
			projectedAnalyticGraphJsonModel.setData(pAdheranceDataMapList);
			bpDatasetvalues.add(projectedAnalyticGraphJsonModel);

			// currentAdherence
			LatestGraphJsonModel currentAdherenceAnalyticGraphJsonModel = new LatestGraphJsonModel();
			currentAdherenceAnalyticGraphJsonModel.setLabel("CurrentAdherence");
			List<Map<String, String>> currentAdheranceDataMapList = new ArrayList<>();
			currentAdheranceDataMapList = analyticsReadingsMap.get("currentAdherence");
			currentAdherenceAnalyticGraphJsonModel.setData(currentAdheranceDataMapList);
			bpDatasetvalues.add(currentAdherenceAnalyticGraphJsonModel);

			// BP
			LatestGraphJsonModel bpSysAnalyticGraphJsonModel = new LatestGraphJsonModel();
			bpSysAnalyticGraphJsonModel.setLabel("Systolic");
			List<Map<String, String>> bpSysDataMapList = new LinkedList<Map<String, String>>();
			bpSysDataMapList = bioMetricReadings.get("bpSys");
			bpSysAnalyticGraphJsonModel.setData(bpSysDataMapList);
			bpDatasetvalues.add(bpSysAnalyticGraphJsonModel);

			LatestGraphJsonModel bpDiaAnalyticGraphJsonModel = new LatestGraphJsonModel();
			bpDiaAnalyticGraphJsonModel.setLabel("Diastolic");
			List<Map<String, String>> bpDiaDataMapList = new LinkedList<Map<String, String>>();
			bpDiaDataMapList = bioMetricReadings.get("bpDia");
			bpDiaAnalyticGraphJsonModel.setData(bpDiaDataMapList);
			bpDatasetvalues.add(bpDiaAnalyticGraphJsonModel);

			// Pbp
			LatestGraphJsonModel pbpSysAnalyticGraphJsonModel = new LatestGraphJsonModel();
			pbpSysAnalyticGraphJsonModel.setLabel("PSystolic");
			List<Map<String, String>> pbpSysDataMapList = new LinkedList<Map<String, String>>();
			pbpSysDataMapList = predictedbioMetricReadings.get("bpSys");
			pbpSysAnalyticGraphJsonModel.setData(pbpSysDataMapList);
			bpDatasetvalues.add(pbpSysAnalyticGraphJsonModel);

			LatestGraphJsonModel pbpDiaAnalyticGraphJsonModel = new LatestGraphJsonModel();
			pbpDiaAnalyticGraphJsonModel.setLabel("PDiastolic");
			List<Map<String, String>> pbpDiaDataMapList = new LinkedList<Map<String, String>>();
			pbpDiaDataMapList = predictedbioMetricReadings.get("bpDia");
			pbpDiaAnalyticGraphJsonModel.setData(pbpDiaDataMapList);
			bpDatasetvalues.add(pbpDiaAnalyticGraphJsonModel);

			graphSuperJsonModel.setDatasets(bpDatasetvalues);
			return new ResponseEntity<LatestGraphSuperJsonModel>(graphSuperJsonModel, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/fromexcel/bpreadings/{patientid}", produces = "application/json")
	@ApiIgnore
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<GraphSuperJsonModel> fetchFromExcelBPMoniterReadingsBypatientId(
			@PathVariable(value = "patientid") String patientid) {

		GraphSuperJsonModel graphSuperJsonModel = new GraphSuperJsonModel();
		List<GraphJsonModel> bpDatasetvalues = new ArrayList<GraphJsonModel>();
		List<Integer> labelCount = new ArrayList<>();
		// Reading Excel Data
		List<BPAdherenceFromExcelModel> bPAdherenceModels = getBPBiometricDataFromExcel(patientid);
		if (bPAdherenceModels.size() > 0) {
			// 1
			int i = 1;
			GraphJsonModel systolicbpjsonModel = new GraphJsonModel();
			systolicbpjsonModel.setLabel("Systolic");
			List<String> SystolicdataList = new ArrayList<>();
			for (BPAdherenceFromExcelModel bPAdherence : bPAdherenceModels) {
				labelCount.add(i);
				if (bPAdherence.getSystolicPressure() != null) {
					SystolicdataList.add(bPAdherence.getSystolicPressure());
				}
				i++;
			}
			systolicbpjsonModel.setData(SystolicdataList);
			bpDatasetvalues.add(systolicbpjsonModel);

			// 2
			GraphJsonModel psystolicbpjsonModel = new GraphJsonModel();
			psystolicbpjsonModel.setLabel("PSystolic");
			List<String> psystolicdataList = new ArrayList<>();
			for (BPAdherenceFromExcelModel bPAdherence : bPAdherenceModels) {
				if (bPAdherence.getSystolicPressureProjected() != null) {
					psystolicdataList.add(bPAdherence.getSystolicPressureProjected());
				}
			}
			psystolicbpjsonModel.setData(psystolicdataList);
			bpDatasetvalues.add(psystolicbpjsonModel);

			// 3
			GraphJsonModel diastolicbpjsonModel = new GraphJsonModel();
			diastolicbpjsonModel.setLabel("Diastolic");
			List<String> diastolicdataList = new ArrayList<>();
			for (BPAdherenceFromExcelModel bPAdherence : bPAdherenceModels) {
				if (bPAdherence.getDiastolicpressure() != null) {
					diastolicdataList.add(bPAdherence.getDiastolicpressure());
				}
			}
			diastolicbpjsonModel.setData(diastolicdataList);
			bpDatasetvalues.add(diastolicbpjsonModel);

			// 4
			GraphJsonModel pdiastolicbpjsonModel = new GraphJsonModel();
			pdiastolicbpjsonModel.setLabel("PDiastolic");
			List<String> pdiastolicdataList = new ArrayList<>();
			for (BPAdherenceFromExcelModel bPAdherence : bPAdherenceModels) {
				if (bPAdherence.getDiastolicpressureProjected() != null) {
					pdiastolicdataList.add(bPAdherence.getDiastolicpressureProjected());
				}
			}
			pdiastolicbpjsonModel.setData(pdiastolicdataList);
			bpDatasetvalues.add(pdiastolicbpjsonModel);

			// 5
			GraphJsonModel currentAdherencebpjsonModel = new GraphJsonModel();
			currentAdherencebpjsonModel.setLabel("currentAdherence");
			List<String> currentAdherencedataList = new ArrayList<>();
			for (BPAdherenceFromExcelModel bPAdherence : bPAdherenceModels) {
				if (bPAdherence.getCurrentAdherence() != null) {
					currentAdherencedataList.add(bPAdherence.getCurrentAdherence());
				}
			}
			currentAdherencebpjsonModel.setData(currentAdherencedataList);
			bpDatasetvalues.add(currentAdherencebpjsonModel);

			// 6
			GraphJsonModel padherencebpjsonModel = new GraphJsonModel();
			padherencebpjsonModel.setLabel("PAdherence");
			List<String> padherencedataList = new ArrayList<>();
			for (BPAdherenceFromExcelModel bPAdherence : bPAdherenceModels) {
				if (bPAdherence.getProjectedAdherence() != null) {
					padherencedataList.add(bPAdherence.getProjectedAdherence());
				}
			}
			padherencebpjsonModel.setData(padherencedataList);
			bpDatasetvalues.add(padherencebpjsonModel);

			// 7
			GraphJsonModel eventbpjsonModel = new GraphJsonModel();
			eventbpjsonModel.setLabel("Event");
			List<String> eventdataList = new ArrayList<>();
			for (BPAdherenceFromExcelModel bPAdherence : bPAdherenceModels) {
				if (bPAdherence.getAlert() != null) {
					eventdataList.add(bPAdherence.getAlert());
				}
			}
			eventbpjsonModel.setData(eventdataList);
			bpDatasetvalues.add(eventbpjsonModel);

		}

		graphSuperJsonModel.setLabels(labelCount);
		graphSuperJsonModel.setDatasets(bpDatasetvalues);

		return new ResponseEntity<GraphSuperJsonModel>(graphSuperJsonModel, HttpStatus.OK);
	}

	@PostMapping(value = "/post/imantics/analytics/{patientid}", produces = "application/json")
	@ApiOperation(value = "Fetch analytics By Id", notes = "Returns analytics")
	public ResponseEntity<LatestGraphSuperJsonModel> fetchhAnalyticsFromImanticsBypatientId(
			@PathVariable(value = "patientid") String patientid, @RequestBody String inputdata) throws JSONException {
		// input: 62784, 2018-05-01, 2018-08-06 ,Asia/Calcutta
		JSONObject jsonObject = new JSONObject(inputdata);
		String startdate = jsonObject.getString("startdate");
		String enddate = jsonObject.getString("enddate");
		String zoneid = jsonObject.getString("zoneid");
		String startTime = " 00:00";
		String endTime = " 23:59";

		LocalDateTime startLocalDateTimeObjInUTC = dateUtility
				.convertLocalTimeToUTCTimeObjectByzoneid(startdate + startTime, zoneid);
		LocalDateTime endLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(enddate + endTime,
		                                                                                             zoneid);

		Date strdateObjinUTC = java.util.Date
				.from(startLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
				                                .toInstant());

		Date enddateObjinUTC = java.util.Date.from(endLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
		                                                                   .toInstant());

		Patient patient = patientService.fetchPatientById(Long.parseLong(patientid));

		if (patient != null && patient.getUserDetails() != null) {
			String patinetid = patient.getUserDetails()
			                          .getImanticUserid();
			// Analytics
			ArrayList<String> analyticNameList = new ArrayList<String>();
			analyticNameList.add("projectedAdherence");
			analyticNameList.add("currentAdherence");

			Map<String, List<Map<String, String>>> analyticsReadingsMap = imanticService.fetchAnalyticsMap(patinetid,
			                                                                                               strdateObjinUTC, enddateObjinUTC, analyticNameList);
			LatestGraphSuperJsonModel graphSuperJsonModel = new LatestGraphSuperJsonModel();
			List<LatestGraphJsonModel> analyticsDatasetvalues = new ArrayList<LatestGraphJsonModel>();

			// projectedAdherence
			LatestGraphJsonModel projectedAnalyticGraphJsonModel = new LatestGraphJsonModel();
			projectedAnalyticGraphJsonModel.setLabel("PAdherence");
			List<Map<String, String>> pAdheranceDataMapList = new ArrayList<>();
			pAdheranceDataMapList = analyticsReadingsMap.get("projectedAdherence");
			projectedAnalyticGraphJsonModel.setData(pAdheranceDataMapList);
			analyticsDatasetvalues.add(projectedAnalyticGraphJsonModel);

			// currentAdherence
			LatestGraphJsonModel currentAdherenceAnalyticGraphJsonModel = new LatestGraphJsonModel();
			currentAdherenceAnalyticGraphJsonModel.setLabel("CurrentAdherence");
			List<Map<String, String>> currentAdheranceDataMapList = new ArrayList<>();
			currentAdheranceDataMapList = analyticsReadingsMap.get("currentAdherence");
			currentAdherenceAnalyticGraphJsonModel.setData(currentAdheranceDataMapList);
			analyticsDatasetvalues.add(currentAdherenceAnalyticGraphJsonModel);

			graphSuperJsonModel.setDatasets(analyticsDatasetvalues);
			return new ResponseEntity<LatestGraphSuperJsonModel>(graphSuperJsonModel, HttpStatus.OK);
		}
		return null;
	}

	@GetMapping(value = "/imantics/analytics/{patientid}", produces = "application/json")
	@ApiOperation(value = "Fetch analytics By Id", notes = "Returns analytics")
	public ResponseEntity<LatestGraphSuperJsonModel> fetchAnalyticsFromImanticsBypatientId(
			@PathVariable(value = "patientid") String patientid, @RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate, @RequestParam("zoneid") String zoneid) {
		// input: 62784, 2018-05-01, 2018-08-06 ,Asia/Calcutta

		String startTime = " 00:00";
		String endTime = " 23:59";

		LocalDateTime startLocalDateTimeObjInUTC = dateUtility
				.convertLocalTimeToUTCTimeObjectByzoneid(startdate + startTime, zoneid);
		LocalDateTime endLocalDateTimeObjInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(enddate + endTime,
		                                                                                             zoneid);

		Date strdateObjinUTC = java.util.Date
				.from(startLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
				                                .toInstant());

		Date enddateObjinUTC = java.util.Date.from(endLocalDateTimeObjInUTC.atZone(ZoneOffset.UTC)
		                                                                   .toInstant());

		Patient patient = patientService.fetchPatientById(Long.parseLong(patientid));

		if (patient != null && patient.getUserDetails() != null) {
			String patinetid = patient.getUserDetails()
			                          .getImanticUserid();
			// Analytics
			ArrayList<String> analyticNameList = new ArrayList<String>();
			analyticNameList.add("projectedAdherence");
			analyticNameList.add("currentAdherence");

			Map<String, List<Map<String, String>>> analyticsReadingsMap = imanticService.fetchAnalyticsMap(patinetid,
			                                                                                               strdateObjinUTC, enddateObjinUTC, analyticNameList);
			LatestGraphSuperJsonModel graphSuperJsonModel = new LatestGraphSuperJsonModel();
			List<LatestGraphJsonModel> analyticsDatasetvalues = new ArrayList<LatestGraphJsonModel>();

			// projectedAdherence
			LatestGraphJsonModel projectedAnalyticGraphJsonModel = new LatestGraphJsonModel();
			projectedAnalyticGraphJsonModel.setLabel("PAdherence");
			List<Map<String, String>> pAdheranceDataMapList = new ArrayList<>();
			pAdheranceDataMapList = analyticsReadingsMap.get("projectedAdherence");
			projectedAnalyticGraphJsonModel.setData(pAdheranceDataMapList);
			analyticsDatasetvalues.add(projectedAnalyticGraphJsonModel);

			// currentAdherence
			LatestGraphJsonModel currentAdherenceAnalyticGraphJsonModel = new LatestGraphJsonModel();
			currentAdherenceAnalyticGraphJsonModel.setLabel("CurrentAdherence");
			List<Map<String, String>> currentAdheranceDataMapList = new ArrayList<>();
			currentAdheranceDataMapList = analyticsReadingsMap.get("currentAdherence");
			currentAdherenceAnalyticGraphJsonModel.setData(currentAdheranceDataMapList);
			analyticsDatasetvalues.add(currentAdherenceAnalyticGraphJsonModel);

			graphSuperJsonModel.setDatasets(analyticsDatasetvalues);
			return new ResponseEntity<LatestGraphSuperJsonModel>(graphSuperJsonModel, HttpStatus.OK);
		}
		return null;
	}

	// fetchBPMoniterReadingsFromExcelBypatientId
	@GetMapping(value = "/fromexcel/glucoreadings/{patientid}", produces = "application/json")
	@ApiIgnore
	@ApiOperation(value = "Fetch Patient By Id", notes = "Returns patient")
	public ResponseEntity<GraphSuperJsonModel> newfetchglucoseReadingsFromExcelBypatientId(
			@PathVariable(value = "patientid") String patientid) {

		GraphSuperJsonModel graphSuperJsonModel = new GraphSuperJsonModel();
		List<GraphJsonModel> glucoseDatasetvalues = new ArrayList<GraphJsonModel>();
		List<Integer> labelCount = new ArrayList<>();
		// Reading Excel Data
		List<GlucoseAdherenceFromExcelModel> glucoseAdherenceModels = getGlucoseBiometricDataFromExcel(patientid);
		if (glucoseAdherenceModels.size() > 0) {
			// 1
			int i = 1;
			GraphJsonModel glucoseJsonModel = new GraphJsonModel();
			glucoseJsonModel.setLabel("Glucose");
			List<String> glucosedataList = new ArrayList<>();
			for (GlucoseAdherenceFromExcelModel glucoseAdherence : glucoseAdherenceModels) {
				labelCount.add(i);
				if (glucoseAdherence.getGlucose() != null) {
					glucosedataList.add(glucoseAdherence.getGlucose());
				}
				i++;
			}
			glucoseJsonModel.setData(glucosedataList);
			glucoseDatasetvalues.add(glucoseJsonModel);

			// 2
			GraphJsonModel pGlucoseJsonModel = new GraphJsonModel();
			pGlucoseJsonModel.setLabel("PGlucose");
			List<String> pGlucosedataList = new ArrayList<>();
			for (GlucoseAdherenceFromExcelModel glucoseAdherence : glucoseAdherenceModels) {
				if (glucoseAdherence.getGlucoseP() != null) {
					pGlucosedataList.add(glucoseAdherence.getGlucoseP());
				}
			}
			pGlucoseJsonModel.setData(pGlucosedataList);
			glucoseDatasetvalues.add(pGlucoseJsonModel);

			// 5
			GraphJsonModel currentAdherencebpjsonModel = new GraphJsonModel();
			currentAdherencebpjsonModel.setLabel("currentAdherence");
			List<String> currentAdherencedataList = new ArrayList<>();
			for (GlucoseAdherenceFromExcelModel glucoseAdherence : glucoseAdherenceModels) {
				if (glucoseAdherence.getCurrentAdherence() != null) {
					currentAdherencedataList.add(glucoseAdherence.getCurrentAdherence());
				}
			}
			currentAdherencebpjsonModel.setData(currentAdherencedataList);
			glucoseDatasetvalues.add(currentAdherencebpjsonModel);

			// 6
			GraphJsonModel padherencebpjsonModel = new GraphJsonModel();
			padherencebpjsonModel.setLabel("PAdherence");
			List<String> padherencedataList = new ArrayList<>();
			for (GlucoseAdherenceFromExcelModel glucoseAdherence : glucoseAdherenceModels) {
				if (glucoseAdherence.getProjectedAdherence() != null) {
					padherencedataList.add(glucoseAdherence.getProjectedAdherence());
				}
			}
			padherencebpjsonModel.setData(padherencedataList);
			glucoseDatasetvalues.add(padherencebpjsonModel);

			// 7
			GraphJsonModel eventbpjsonModel = new GraphJsonModel();
			eventbpjsonModel.setLabel("Event");
			List<String> eventdataList = new ArrayList<>();
			for (GlucoseAdherenceFromExcelModel glucoseAdherence : glucoseAdherenceModels) {
				if (glucoseAdherence.getAlert() != null) {
					eventdataList.add(glucoseAdherence.getAlert());
				}
			}
			eventbpjsonModel.setData(eventdataList);
			glucoseDatasetvalues.add(eventbpjsonModel);

			graphSuperJsonModel.setLabels(labelCount);
			graphSuperJsonModel.setDatasets(glucoseDatasetvalues);

			return new ResponseEntity<GraphSuperJsonModel>(graphSuperJsonModel, HttpStatus.OK);
		}

		return null;
	}

	public List<BPAdherenceFromExcelModel> getBPBiometricDataFromExcel(String patientId) {

		ClassLoader classLoader = getClass().getClassLoader();

		InputStream is = null;
		if (patientId.equalsIgnoreCase("2061")) {
			is = classLoader.getResourceAsStream("samplefiles/improving.xlsx");
		} else if (patientId.equalsIgnoreCase("5084")) {
			is = classLoader.getResourceAsStream("samplefiles/degrading.xlsx");

		} else if (patientId.equalsIgnoreCase("35545")) {
			is = classLoader.getResourceAsStream("samplefiles/flat.xlsx");

		} else if (patientId != null) {
			is = classLoader.getResourceAsStream("samplefiles/flat.xlsx");
		}

		List<BPAdherenceFromExcelModel> BPmodelList = new LinkedList<BPAdherenceFromExcelModel>();

		try {
			// XSSFSheet mySheet = new XSSFWorkbook(new
			// FileInputStream(myFile)).getSheetAt(0);
			XSSFSheet mySheet = new XSSFWorkbook(is).getSheetAt(0);
			Iterator<Row> rowIterator = mySheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Map<String, String> maps = new HashMap<String, String>();
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if (cell.getRowIndex() == 0) {
						break;
					}
					if (cell.getColumnIndex() == 0) {
						putValue(cell, "systolic", maps);
					} else if (cell.getColumnIndex() == 1) {
						putValue(cell, "systolicP", maps);
					} else if (cell.getColumnIndex() == 2) {
						putValue(cell, "diastolic", maps);
					} else if (cell.getColumnIndex() == 3) {
						putValue(cell, "diastolicP", maps);
					} else if (cell.getColumnIndex() == 4) {
						putValue(cell, "glucose", maps);
					} else if (cell.getColumnIndex() == 5) {
						putValue(cell, " glucoseP", maps);
					} else if (cell.getColumnIndex() == 6) {
						putValue(cell, "currentAdherence", maps);
					} else if (cell.getColumnIndex() == 7) {
						putValue(cell, "projectedAdherence", maps);
					} else if (cell.getColumnIndex() == 8) {
						putValue(cell, "event", maps);
					}

				}

				BPAdherenceFromExcelModel bpAdherence = new BPAdherenceFromExcelModel();
				bpAdherence.setSystolicPressure(maps.get("systolic"));
				bpAdherence.setSystolicPressureProjected(maps.get("systolicP"));
				bpAdherence.setDiastolicpressure(maps.get("diastolic"));
				bpAdherence.setDiastolicpressureProjected(maps.get("diastolicP"));
				bpAdherence.setCurrentAdherence(maps.get("currentAdherence"));
				bpAdherence.setProjectedAdherence(maps.get("projectedAdherence"));
				bpAdherence.setAlert(maps.get("event"));
				// bpAdherence.setTaken((maps.get("taken")));

				BPmodelList.add(bpAdherence);
			}
		} catch (IOException e) {
			e.getLocalizedMessage();

		}
		return BPmodelList;
	}

	public List<GlucoseAdherenceFromExcelModel> getGlucoseBiometricDataFromExcel(String patientId) {

		ClassLoader classLoader = getClass().getClassLoader();
		/*
		 * File myFile = null; if (patientId.equalsIgnoreCase("3")) { myFile =
		 * new
		 * File(classLoader.getResource("samplefiles/improving.xlsx").getFile())
		 * ; } else if (patientId.equalsIgnoreCase("4")) { myFile = new
		 * File(classLoader.getResource("samplefiles/degrading.xlsx").getFile())
		 * ; } else if (patientId.equalsIgnoreCase("1")) { myFile = new
		 * File(classLoader.getResource("samplefiles/flat.xlsx").getFile()); }
		 */

		InputStream is = null;
		if (patientId.equalsIgnoreCase("3")) {
			is = classLoader.getResourceAsStream("samplefiles/improving.xlsx");
		} else if (patientId.equalsIgnoreCase("2")) {
			is = classLoader.getResourceAsStream("samplefiles/degrading.xlsx");

		} else if (patientId.equalsIgnoreCase("1")) {
			is = classLoader.getResourceAsStream("samplefiles/flat.xlsx");
		} else if (patientId != null) {
			is = classLoader.getResourceAsStream("samplefiles/improving.xlsx");
		}

		List<GlucoseAdherenceFromExcelModel> glucomodelList = new LinkedList<GlucoseAdherenceFromExcelModel>();
		try {
			XSSFSheet mySheet = new XSSFWorkbook(is).getSheetAt(0);
			Iterator<Row> rowIterator = mySheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Map<String, String> maps = new HashMap<String, String>();
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if (cell.getRowIndex() == 0) {
						break;
					}

					if (cell.getColumnIndex() == 4) {
						putValue(cell, "glucose", maps);
					} else if (cell.getColumnIndex() == 5) {
						putValue(cell, "glucoseP", maps);
					} else if (cell.getColumnIndex() == 6) {
						putValue(cell, "currentAdherence", maps);
					} else if (cell.getColumnIndex() == 7) {
						putValue(cell, "projectedAdherence", maps);
					} else if (cell.getColumnIndex() == 8) {
						putValue(cell, "event", maps);
					}

				}

				GlucoseAdherenceFromExcelModel glucomodel = new GlucoseAdherenceFromExcelModel();
				glucomodel.setGlucose(maps.get("glucose"));
				glucomodel.setGlucoseP(maps.get("glucoseP"));
				glucomodel.setCurrentAdherence(maps.get("currentAdherence"));
				glucomodel.setProjectedAdherence(maps.get("projectedAdherence"));
				glucomodel.setAlert(maps.get("event"));
				// glucomodel.setTaken((maps.get("taken")));

				glucomodelList.add(glucomodel);

			}
		} catch (IOException e) {
			e.getLocalizedMessage();
		}
		return glucomodelList;
	}

	/**
	 * populate map
	 *
	 * @param cell
	 * @param key
	 * @param maps
	 */
	private void putValue(Cell cell, String key, Map<String, String> maps) {
		switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				maps.put(key, cell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				maps.put(key, String.valueOf((int) cell.getNumericCellValue()));
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				maps.put(key, String.valueOf(cell.getBooleanCellValue()));
				break;
			default:
		}
	}

	class ListMapComparatorByDate implements Comparator<Map<String, String>> {

		DateFormat f = new SimpleDateFormat("MM/dd/yyyy HH:mm");

		@Override
		public int compare(Map<String, String> o1, Map<String, String> o2) {
			try {
				return f.parse(o1.get("x"))
				        .compareTo(f.parse(o2.get("x")));
			} catch (ParseException e) {
				throw new IllegalArgumentException(e);
			}
		}

	}

}
