package com.sg.dayamed.controller;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.HeartRateModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.StepsModel;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.service.BiometricService;
import com.sg.dayamed.service.ImanticService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionScheduleService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;

@RestController
@Api(value = " No Jwt Biometric related Operations")
public class NoJwtBiometricController {

	@Autowired
	DateUtility dateUtility;

	@Autowired
	BiometricService biometricService;

	@Autowired
	PatientService patientService;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	ImanticService imanticService;

	@Autowired
	NotificationScheduleService jobService;

	@Autowired
	Utility utility;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	DeviceInfoRepository deviceInfoRepository;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	PrescriptionScheduleService prescriptionScheduleService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	private static final Logger logger = LoggerFactory.getLogger(NoJwtBiometricController.class);

	DateTimeFormatter dayamedGeneralFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	Format dateformatterForGraph = new SimpleDateFormat("MM/dd/yyyy HH:mm");// 04/01/2014

	NumberFormat decimalFormat = new DecimalFormat("#0.00");

	// add HeartRating data

	@PostMapping(value = "/addheartrate", produces = "application/json", consumes = "application/json")
	public ResponseEntity<HeartRateModel> addHeartrate(@RequestBody String heartrateModel) {
		/*
		 * { "observedtime": "2018-03-21 12:54", "reading": "82", "patientid":
		 * "97547", "zoneid": "Asia/Calcutta", "latitude": "56.130721",
		 * "longitude": "106.346337", "deviceinfoid": "2", "prescriptionid": 1,
		 * "prescribedtime": "2015-07-16 17:07", "consumptionstatus":
		 * "taken/delayed" }
		 */
		HeartRateModel hrmodel = null;
		try {
			JSONObject jsonobj = new JSONObject(heartrateModel);
			if (!jsonobj.has("prescribedtime") || !jsonobj.has("zoneid") || !jsonobj.has("patientid") || !jsonobj.has("prescriptionid")
					|| !jsonobj.has("reading") || !jsonobj.has("consumptionstatus") || !jsonobj.has("observedtime")) {
				logger.error("Missing required data while adding heartrate");
				return new ResponseEntity<HeartRateModel>(HttpStatus.BAD_REQUEST);
			}
			LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
					jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
					                                            .trim());
			if (jsonobj.has("deviceinfoid")) {
				//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(jsonobj.getString("deviceinfoid")));
				Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository.findById(Long.parseLong(jsonobj.getString("deviceinfoid")));
				if (optionaldeviceInfo.isPresent()) {
					if (!optionaldeviceInfo.get()
					                       .getDevice()
					                       .getName()
					                       .equalsIgnoreCase("Heart Rate")) {
						logger.error("DeviceInfo not related to HeartRate");
						return new ResponseEntity<HeartRateModel>(HttpStatus.BAD_REQUEST);
					}
				} else {
					logger.error("DeviceInfo not available in DB with provided deviceinfoid");
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				}
				if (optionaldeviceInfo.get()
				                      .getPrescription()
				                      .getId() != jsonobj.getLong("prescriptionid") || optionaldeviceInfo.get()
				                                                                                         .getPrescription()
				                                                                                         .getPatient()
				                                                                                         .getId() !=
						Long.parseLong(jsonobj.getString("patientid"))) {
					logger.error("prescriptionid not belongs to this patient");
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				}
			}
			HeartRateModel heartRateModel = biometricService
					.fetchHeartRateByPrescribedTimeAndDeviceInfoId(
							dateUtility.convertdatetimeStringToLocalDateTime(
									prescribedLDTInUTC.format(dayamedGeneralFormatter)),
							jsonobj.getString("deviceinfoid"));
			if (heartRateModel == null) {
				heartRateModel = new HeartRateModel();

				heartRateModel.setDeviceInfoId(jsonobj.getString("deviceinfoid"));

				heartRateModel.setPrescriptionID(jsonobj.getLong("prescriptionid"));

				heartRateModel.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));
			}
			heartRateModel.setReadingValue(jsonobj.getString("reading"));
			heartRateModel.getConsumptionStatus()
			              .add(jsonobj.getString("consumptionstatus"));
			ArrayList<String> location = new ArrayList<String>();
			if (jsonobj.has("latitude")) {
				location.add(jsonobj.getString("latitude"));
			}
			if (jsonobj.has("longitude")) {
				location.add(jsonobj.getString("longitude"));
			}
			heartRateModel.setLocation(location);

			PatientModel patientModel = utility.convertPatientEntityTopatientModel(
					patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
			if (patientModel.getId() == 0) {
				return new ResponseEntity<HeartRateModel>(HttpStatus.BAD_REQUEST);
			}
			heartRateModel.setPatient(patientModel);
			LocalDateTime observedlocalDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
					jsonobj.getString("observedtime"), jsonobj.getString("zoneid"));
			heartRateModel.setObservedTime(observedlocalDateTimeInUTC.format(dayamedGeneralFormatter));

			// Fetching data based on AcualdateTime and dosageID from
			// SchedulerJobs.
			jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC,
			                                          Long.parseLong(jsonobj.getString("deviceinfoid")), jsonobj.getString("consumptionstatus")
			                                                                                                    .trim());
			// end update consumption status in schedulerjob
			// Fetching data based on AcualdateTime and dosageID
			prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
			                                                                   Long.parseLong(jsonobj.getString("deviceinfoid")),
			                                                                   jsonobj.getString("consumptionstatus")
			                                                                          .trim());
			// end Adding scale datapoint

			hrmodel = biometricService.addHeartRate(heartRateModel);
			String msg = ApplicationConstants.EMPTY_STRING;
			Long userId = patientModel.getUserDetails()
			                          .getId();
			String userLangPreference = utility.getUserPreferedLang(userId);
			if (hrmodel != null) {
				//DeviceInfo deviceInfo = deviceInfoRepository.findOne(Long.parseLong(hrmodel.getDeviceInfoId()));
				Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository.findById(Long.parseLong(hrmodel.getDeviceInfoId()));
				if (optionaldeviceInfo.isPresent()) {
					if (optionaldeviceInfo.get()
					                      .getDeviceNotificationList() != null && optionaldeviceInfo.get()
					                                                                                .getDeviceNotificationList()
					                                                                                .size() > 0
							&& optionaldeviceInfo.get()
							                     .getDeviceNotificationList()
							                     .get(0) != null) {

						// handling greaterthan and lessthan readings
						if (optionaldeviceInfo.get()
						                      .getDeviceNotificationList()
						                      .get(0)
						                      .getGreaterThanReading1() != null
								&& !(Float.parseFloat(jsonobj.getString("reading")
								                             .trim()) > (Float.parseFloat(
								optionaldeviceInfo.get()
								                  .getDeviceNotificationList()
								                  .get(0)
								                  .getGreaterThanReading1())))) {
							msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "heartrate.range.less", userLangPreference,
							                                                                    null)
									+ optionaldeviceInfo.get()
									                    .getDeviceNotificationList()
									                    .get(0)
									                    .getGreaterThanReading1();
						}
						if (optionaldeviceInfo.get()
						                      .getDeviceNotificationList()
						                      .get(0)
						                      .getLessThanReading1() != null
								&& !(Float.parseFloat(jsonobj.getString("reading")
								                             .trim()) < (Float.parseFloat(
								optionaldeviceInfo.get()
								                  .getDeviceNotificationList()
								                  .get(0)
								                  .getLessThanReading1())))) {
							msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "heartrate.range.beyond", userLangPreference,
							                                                                    null)
									+ optionaldeviceInfo.get()
									                    .getDeviceNotificationList()
									                    .get(0)
									                    .getLessThanReading1();
						}
					}
				}
			}

			// sending Mail to Patient
			if (hrmodel != null) {
				Map<String, String> msgResourceReplaceKeyVal = new HashMap<String, String>();
				msgResourceReplaceKeyVal.put("reading", jsonobj.getString("reading"));
				String bodyMsg =
						messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "heartrate.success.taken", userLangPreference,
						                                                              null) + msg;
				String emailSub = messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "emailsubject.biometricstatus", userLangPreference,
				                                                                                null);
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(patientModel.getUserDetails()
				                              .getEmailId());
				//emailAndSmsUtility.sendEmail(ApplicationConstants.BIOMETRIC_STATUS, bodyMsg, recipientList);
				emailAndSmsUtility.sendEmail(emailSub, bodyMsg, recipientList);

				emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
				                                                .getCountryCode()
						, patientModel.getUserDetails()
						              .getMobileNumber());

				// sending notification and mails to caregiver
				String patientName = patientModel.getUserDetails()
				                                 .getFirstName() + " "
						+ patientModel.getUserDetails()
						              .getLastName();
				msgResourceReplaceKeyVal.put("patientName", patientName);
				msgResourceReplaceKeyVal.put("reading", heartRateModel.getReadingValue());
				bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "heartrate.success.patienent",
				                                                                        userLangPreference, null) + msg;
				List<String> caregiverMailList = new ArrayList<>();
				for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
					caregiverMailList.add(caregiverModel.getUserDetails()
					                                    .getEmailId());
					emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
					                                                  .getCountryCode(), caregiverModel.getUserDetails()
					                                                                                   .getMobileNumber());
				}
				if (caregiverMailList.size() > 0) {
					emailAndSmsUtility.sendEmail("emailsubject.biometricstatus", "heartrate.success.patienent", msgResourceReplaceKeyVal, recipientList);
				}
			}
			return new ResponseEntity<HeartRateModel>(hrmodel, HttpStatus.OK);
		} catch (JSONException e) {
			e.getLocalizedMessage();
			logger.error(e.getLocalizedMessage());
			return new ResponseEntity<HeartRateModel>(HttpStatus.BAD_REQUEST);
		}
	}

	// add steps data

	/**
	 * when adding vital need to add in respective vital table, and
	 * consumpton_status should be update in scheduler_jobs,prescrition_schedule
	 * and notification tables.
	 *
	 * @param stepsmodel
	 * @return StepsModel
	 */
	@PostMapping(value = "/addsteps", produces = "application/json", consumes = "application/json")
	public ResponseEntity<StepsModel> addSteps(@RequestBody String stepsmodel) {
		/*
		 * { "observedtime": "2018-03-21 12:54", "stepcount": "82", "patientid":
		 * "97547", "zoneid": "Asia/Calcutta", "latitude": "56.130721",
		 * "longitude": "106.346337", "deviceinfoid": "2", "prescriptionid": 1,
		 * "prescribedtime": "2015-07-16 17:07", "consumptionstatus":
		 * "taken/delayed" }
		 */
		try {
			logger.info("adding steps");
			if (stepsmodel == null || stepsmodel.isEmpty()) {
				logger.error("steps object must not be empty or null");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			JSONObject jsonobj = new JSONObject(stepsmodel);
			if (!jsonobj.has("zoneid") || !jsonobj.has("prescribedtime") || !jsonobj.has("stepcount")
					|| !jsonobj.has("consumptionstatus") || !jsonobj.has("observedtime") || !jsonobj.has("deviceinfoid") || !jsonobj.has("prescriptionid")) {
				logger.error("missing required data while adding steps");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			LocalDateTime prescribedLDTInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
					jsonobj.getString("prescribedtime"), jsonobj.getString("zoneid")
					                                            .trim());
			if (jsonobj.has("deviceinfoid")) {
				Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository
						.findById(Long.parseLong(jsonobj.getString("deviceinfoid")));

				if (optionaldeviceInfo.isPresent()) {
					if (!optionaldeviceInfo.get()
					                       .getDevice()
					                       .getName()
					                       .equalsIgnoreCase(ApplicationConstants.STEPS)) {
						logger.error("DeviceinfoId provided by you is not beloging to steps");
						return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
					}
				} else {
					logger.error("DeviceinfoId provided by you is not available in database");
					return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
				}
				if (optionaldeviceInfo.get()
				                      .getPrescription()
				                      .getId() != jsonobj.getLong("prescriptionid")
						|| optionaldeviceInfo.get()
						                     .getPrescription()
						                     .getPatient()
						                     .getId() != Long
						.parseLong(jsonobj.getString("patientid"))) {
					logger.error("prescription id provided by you is not belonging to this patient");
					return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
				}
			}
			StepsModel stepsModel = biometricService
					.fetchStepsByPrescribedTimeAndDeviceInfoId(
							dateUtility.convertdatetimeStringToLocalDateTime(
									prescribedLDTInUTC.format(dayamedGeneralFormatter)),
							jsonobj.getString("deviceinfoid"));
			if (stepsModel == null) {
				stepsModel = new StepsModel();
				if (jsonobj.has("prescriptionid")) {
					stepsModel.setPrescriptionID(jsonobj.getLong("prescriptionid"));
				}
				if (jsonobj.has("deviceinfoid")) {
					stepsModel.setDeviceInfoId(jsonobj.getString("deviceinfoid"));
				}
				stepsModel.setPrescribedTime(prescribedLDTInUTC.format(dayamedGeneralFormatter));
			}
			stepsModel.setStepCount(Integer.parseInt(jsonobj.getString("stepcount")));
			stepsModel.getConsumptionStatus()
			          .add(jsonobj.getString("consumptionstatus"));

			ArrayList<String> location = new ArrayList<String>();
			if (jsonobj.has("latitude")) {
				location.add(jsonobj.getString("latitude"));
			}
			if (jsonobj.has("longitude")) {
				location.add(jsonobj.getString("longitude"));
			}
			stepsModel.setLocation(location);
			PatientModel patientModel = utility.convertPatientEntityTopatientModel(
					patientService.fetchPatientById(Long.parseLong(jsonobj.getString("patientid"))));
			if (patientModel.getId() == 0) {
				return new ResponseEntity<StepsModel>(HttpStatus.BAD_REQUEST);
			}
			stepsModel.setPatient(patientModel);

			LocalDateTime observedlocalDateTimeInUTC = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid(
					jsonobj.getString("observedtime"), jsonobj.getString("zoneid"));
			stepsModel.setObservedTime(observedlocalDateTimeInUTC.format(dayamedGeneralFormatter));

			// Fetching data based on AcualdateTime and dosageID from
			// SchedulerJobs.
			jobService.updateSchedulerJobByDeviceInfo(prescribedLDTInUTC,
			                                          Long.parseLong(jsonobj.getString("deviceinfoid")), jsonobj.getString("consumptionstatus")
			                                                                                                    .trim());
			// end update consumption status in schedulerjob
			// Fetching data based on AcualdateTime and dosageID from
			// PrescriptionSchedule
			prescriptionScheduleService.updatePrescriptionScheduleByDeviceInfo(prescribedLDTInUTC,
			                                                                   Long.parseLong(jsonobj.getString("deviceinfoid")),
			                                                                   jsonobj.getString("consumptionstatus")
			                                                                          .trim());
			// end Adding steps datapoint
			StepsModel persistedStepsModel = biometricService.addSteps(stepsModel);
			String msg = ApplicationConstants.EMPTY_STRING;
			Long userId = patientModel.getUserDetails()
			                          .getId();
			String userLangPreference = utility.getUserPreferedLang(userId);
			if (persistedStepsModel != null) {
				Optional<DeviceInfo> optionaldeviceInfo = deviceInfoRepository
						.findById(Long.parseLong(persistedStepsModel.getDeviceInfoId()));
				DeviceInfo deviceInfo = null;
				if (optionaldeviceInfo.isPresent()) {
					deviceInfo = optionaldeviceInfo.get();
				}
				if (deviceInfo != null && deviceInfo.getDeviceNotificationList() != null
						&& deviceInfo.getDeviceNotificationList()
						             .size() > 0
						&& deviceInfo.getDeviceNotificationList()
						             .get(0) != null) {
					// handling greaterthan and lessthan readings
					if (deviceInfo.getDeviceNotificationList()
					              .get(0)
					              .getGreaterThanReading1() != null
							&& (Float.parseFloat(jsonobj.getString("stepcount")
							                            .trim()) > (Float.parseFloat(
							deviceInfo.getDeviceNotificationList()
							          .get(0)
							          .getGreaterThanReading1())))) {
						Map<String, String> keyVal = new HashMap<String, String>();
						keyVal.put("range", deviceInfo.getDeviceNotificationList()
						                              .get(0)
						                              .getGreaterThanReading1());
						msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal, "your.step.count.lessthan", userLangPreference,
						                                                                    null);
					}
					if (deviceInfo.getDeviceNotificationList()
					              .get(0)
					              .getLessThanReading1() != null
							&& !(Float.parseFloat(jsonobj.getString("stepcount")
							                             .trim()) < (Float.parseFloat(
							deviceInfo.getDeviceNotificationList()
							          .get(0)
							          .getLessThanReading1())))) {
						Map<String, String> keyVal = new HashMap<String, String>();
						keyVal.put("range", deviceInfo.getDeviceNotificationList()
						                              .get(0)
						                              .getLessThanReading1());
						msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal, "stepcount.reading.beyond.withVal", userLangPreference,
						                                                                    null);
					}
				}
			}

			// sending Mail to Patient
			if (persistedStepsModel != null) {
				Map<String, String> keyVal = new HashMap<String, String>();
				keyVal.put("range", jsonobj.getString("stepcount"));
				String bodyMsg =
						messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal, "you.have.successfully.withstepcount", userLangPreference,
						                                                              null) + "." + msg;
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(patientModel.getUserDetails()
				                              .getEmailId());
				emailAndSmsUtility.sendEmail(ApplicationConstants.BIOMETRIC_STATUS, bodyMsg, recipientList);

				emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
				                                                .getCountryCode()
						, patientModel.getUserDetails()
						              .getMobileNumber());
				// Adding notification to Notification table. for history in
				// calender
					/*notificationService.addWebNotification(patientModel.getUserDetails().getId(),
							bodyMsg, "taken", "device_status");*/
				// empty reciveruseridlist as first parameter
				// sending notification and mails to caregiver
				String patientName = patientModel.getUserDetails()
				                                 .getFirstName() + " "
						+ patientModel.getUserDetails()
						              .getLastName();
				bodyMsg = patientName + " have successfully taken his/her Step count. count is: "
						+ jsonobj.getString("stepcount") + "." + msg;
				List<String> caregiverMailList = new ArrayList<>();
				for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
					caregiverMailList.add(caregiverModel.getUserDetails()
					                                    .getEmailId());
					emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
					                                                  .getCountryCode()
							, caregiverModel.getUserDetails()
							                .getMobileNumber());
						/*notificationService.addWebNotification(caregiverModel.getUserDetails().getId(),
								bodyMsg, "taken", "device_status");*/
				}
				if (caregiverMailList.size() > 0) {
					emailAndSmsUtility.sendEmail("emailsubject.medicinestatus", "you.have.successfully.withstepcount", keyVal, caregiverMailList);
				}
			}
			return new ResponseEntity<StepsModel>(persistedStepsModel, HttpStatus.OK);

		} catch (Exception e) {
			e.getMessage();
			logger.error("Exception while adding steps. " + e.getLocalizedMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
