package com.sg.dayamed.controller;

import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.VideoAndVoiceService;
import com.sg.dayamed.util.ApplicationConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonarsource.scanner.api.internal.shaded.minimaljson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/rest/videoandvoice")
@Api(value = "Video and Voice Operations")
public class VideoAndVoiceController {

	private static final Logger logger = LoggerFactory.getLogger(VideoAndVoiceController.class);

	@Autowired
	VideoAndVoiceService videoAndVoiceService;

	@PostMapping(value = "/getvideocalltoken", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Get video call signature", notes = "Returns VidoeCallSignatureModel")
	public ResponseEntity<VidoeCallSignatureModel> getVideoCallSignature(HttpServletRequest request,
	                                                                     Authentication authentication,
	                                                                     @RequestBody String inputjson)
			throws JSONException {
		VidoeCallSignatureModel vidoeCallSignatureModel = null;
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		int role = 1;
		vidoeCallSignatureModel = videoAndVoiceService.getZoomVideoCallSignature(role, jwtUser);
		if (vidoeCallSignatureModel != null) {
			JSONObject jsonobj = new JSONObject(inputjson);
			vidoeCallSignatureModel.setRecevierUserID(jsonobj.getString("userid"));
			vidoeCallSignatureModel.setCallType(StringUtils.isEmpty(jsonobj.optString("calltype")) ?
					                                    ApplicationConstants.CallType.VideoCall.name() :
					                                    jsonobj.optString("calltype"));
			return new ResponseEntity<VidoeCallSignatureModel>(vidoeCallSignatureModel, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/sendnotification", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "send notification to recevier", notes = "Returns string")
	public ResponseEntity<String> sendCallNotification(HttpServletRequest request,
	                                                   Authentication authentication,
	                                                   @RequestBody VidoeCallSignatureModel vidoeCallSignatureModel) {
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		String recevierNotificationStatus = videoAndVoiceService.sendVideoCallNotificationToReciverByUserId(
				vidoeCallSignatureModel, jwtUser.getUserId());
		JsonObject responseJson = new JsonObject();
		if (ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION
				.equals(recevierNotificationStatus)) {
			responseJson.add("status", "Success");
			return new ResponseEntity<String>(responseJson.toString(), HttpStatus.OK);
		} else {
			responseJson.add("status", "fail");
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
