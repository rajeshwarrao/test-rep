package com.sg.dayamed.controller;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Adherence_DosageInfoMap;
import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.Glucometer;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.pojo.AdherenceDataPointsModel;
import com.sg.dayamed.pojo.AdherenceReportResModel;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.DosageInfoModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.adherence.DeviceAdheranceDataModel;
import com.sg.dayamed.pojo.adherence.MedAndDeviceAdheranceDataModel;
import com.sg.dayamed.repository.AdherenceRepository;
import com.sg.dayamed.repository.Adherence_DosageInfoMapRepository;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.GlucoMeterRepository;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.ScaleRepository;
import com.sg.dayamed.repository.StepsRepository;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.AdherenceReportService;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.DosageInfoService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionScheduleService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.ApplicationErrors;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author naresh.kamisetti
 */
@RestController
//@RequestMapping("/rest/adherence")
@Api(value = "Adherence Operations")
public class AdherenceController extends BaseRestApi {

	@Autowired
	DosageInfoService dosageInfoService;

	@Autowired
	AdherenceService adherenceService;

	@Autowired
	Utility utility;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	PatientService patientService;

	@Autowired
	PrescriptionScheduleService prescriptionScheduleService;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	NotificationService notificationService;

	@Autowired
	AdherenceRepository adherenceRepository;

	@Autowired
	BpMonitorRepository bpMonitorRepository;

	@Autowired
	ScaleRepository scaleRepository;

	@Autowired
	HeartRateRepository heartRateRepository;

	@Autowired
	GlucoMeterRepository glucoMeterRepository;

	@Autowired
	StepsRepository stepsRepository;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	Adherence_DosageInfoMapRepository adherence_DosageInfoMapRepository;

	@Autowired
	NotificationScheduleService notificationScheduleService;

	@Autowired
	BaseRestApiImpl baseRestApiImpl;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	private static final Logger logger = LoggerFactory.getLogger(AdherenceController.class);

	DateTimeFormatter dayamedGeneralFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	//BaseRestApiImpl baseRestApiImpl= new BaseRestApiImpl();

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	@PreAuthorize("hasRole('PATIENT')")
	@PostMapping(value = "/rest/adherence/add/multiple", produces = "application/json", consumes = "application/json")
	public ResponseEntity<List<AdherenceDataPointsModel>> addNewAdherence(@RequestBody String adherencedatapoints) {
		/*
		 * { "adherenceId":"1"/"", "how": [ "AR","NFC" ], "dosageinfoids": [ "8"
		 * ], "consumptionstatus": "consumed", "latitude": "56.130721",
		 * "longitude": "106.346337", "prescriptionid": 2, "prescribedTime":
		 * "2015-07-16 17:07", "observedtime":"2015-07-16 17:08",
		 * "zoneid":"Asia/Kolkata" }
		 *
		 * "adherenceId":[1,2]
		 */
		try {
			JSONObject jsonObject = new JSONObject(adherencedatapoints);
			if (!jsonObject.has("dosageinfoids") || !jsonObject.has("how") || !jsonObject.has("consumptionstatus")
					|| !jsonObject.has("prescribedTime") || !jsonObject.has("observedtime") || !jsonObject.has("zoneid")) {
				logger.error("required inputs are missing in JSON while adding adherence");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			JSONArray dosageinfoidArray = jsonObject.getJSONArray("dosageinfoids");
			JSONArray howArray = jsonObject.getJSONArray("how");
			JSONArray adherenceidarray = null;
			if (jsonObject.has("adherenceids") && !jsonObject.isNull("adherenceids")) {
				adherenceidarray = jsonObject.getJSONArray("adherenceids");
			}

			List<AdherenceDataPointsModel> adherenceDataPointsModelList = new ArrayList<AdherenceDataPointsModel>();
			PatientModel patientModel = null;

			//ArrayList<String> dosageinfoIdsList = new ArrayList<>();
			Set<Long> prescritionIdSet = new LinkedHashSet<Long>();
			List<DosageInfo> dosageInfoList = new ArrayList<DosageInfo>();
			Map<Long, List<DosageInfo>> prescriptionDosageInfoMap = new HashMap<Long, List<DosageInfo>>();
			for (int i = 0; i < dosageinfoidArray.length(); i++) {
				//dosageinfoIdsList.add(dosageinfoidArray.getString(i));
				DosageInfo dosageInfo = dosageInfoService.fetctDosageInfoByID(Long.valueOf(dosageinfoidArray.getString(i)));
				if (dosageInfo != null) {
					prescritionIdSet.add(dosageInfo.getPrescription()
					                               .getId());
					dosageInfoList.add(dosageInfo);
				}
			}
			//To know the patient.
			if (dosageInfoList != null && dosageInfoList.size() > 0) {
				DosageInfo dosageInfo = dosageInfoList.get(0);
				patientModel = utility.convertPatientEntityTopatientModel(dosageInfo.getPrescription()
				                                                                    .getPatient());
			}
			for (Long prescriptionId : prescritionIdSet) {
				List<DosageInfo> preslvlDosageInfoList = new ArrayList<>();
				for (DosageInfo dosageInfo : dosageInfoList) {
					if (dosageInfo.getPrescription()
					              .getId() == prescriptionId) {
						preslvlDosageInfoList.add(dosageInfo);
					}
				}
				prescriptionDosageInfoMap.put(prescriptionId, preslvlDosageInfoList);
			}

			//SDM17013-1211- Consolidate dosage information in notifications.
			Set<String> patientRecipientList = null;
			List<String> medicineNameList = null;
			String prescribedTime = jsonObject.getString("prescribedTime");
			List<String> consumptionStatus = null;

			//this is main loop
			for (Map.Entry<Long, List<DosageInfo>> entry : prescriptionDosageInfoMap.entrySet()) {
				Long prescriptionId = entry.getKey();
				List<DosageInfo> prescriptionLvlDosageinfoList = entry.getValue();

				//buzz logic here
				//this if cond only when 'HOW' parameter got updated.

				//change
				AdherenceDataPoints adherence = new AdherenceDataPoints();
				AdherenceDataPoints persistedAdherence = null;
				//adherence.setHow(jsonObject.getString("how"));
				HashSet<String> howList = new HashSet<>();
				for (int i = 0; i < howArray.length(); i++) {
					howList.add(howArray.getString(i));
				}
				adherence.setHow(howList);
				List<String> consumptionStatusList = new ArrayList<String>();
				consumptionStatusList.add(jsonObject.getString("consumptionstatus"));
				adherence.setConsumptionStatus(consumptionStatusList);
				ArrayList<String> location = new ArrayList<String>();
				if (jsonObject.has("latitude")) {
					location.add(jsonObject.getString("latitude"));
				}
				if (jsonObject.has("longitude")) {
					location.add(jsonObject.getString("longitude"));
				}
				/*String adherenceId = "";
				if (jsonObject.has("adherenceId")) {
					adherenceId = jsonObject.getString("adherenceId");
				}*/

				String zoneid = jsonObject.getString("zoneid");

				adherence.setLocation(location);
				LocalDateTime observedTimeInUTC = null;
				if (jsonObject.has("observedtime")) {
					observedTimeInUTC = dateUtility
							.convertLocalTimeToUTCTimeObjectByzoneid(jsonObject.getString("observedtime")
							                                                   .trim(), zoneid);
					adherence.setObservedTime(observedTimeInUTC);
				}
				//adherence.setPrescriptionID(Long.parseLong(jsonObject.getString("prescriptionid")));

				LocalDateTime prescribedTimeInUTC = dateUtility
						.convertLocalTimeToUTCTimeObjectByzoneid(prescribedTime.trim(), zoneid);
				adherence.setPrescribedTime(prescribedTimeInUTC);

				//
				AdherenceDataPointsModel adherenceModel = new AdherenceDataPointsModel();
				if (adherenceidarray != null && adherenceidarray.length() > 0) {
					for (int i = 0; i < adherenceidarray.length(); i++) {
						//adherenceIdsList.add(adherenceidarray.getLong(i));
						Optional<AdherenceDataPoints> optionaladherenceDataPoints = adherenceRepository.findById(adherenceidarray.getLong(i));
						if (optionaladherenceDataPoints.isPresent()) {
							optionaladherenceDataPoints.get()
							                           .getHow()
							                           .addAll(howList);
							adherenceModel = utility
									.convertAdherenceEntityToModel(adherenceRepository.save(optionaladherenceDataPoints.get()));
							adherenceDataPointsModelList.add(adherenceModel);
						}
					}

				} else {
					//for No_Action event triggered by Prescription_schedule Job.after it is coming from client
					//once network is up.
					AdherenceDataPoints adherenceDataPoints = adherenceService.findByPrescribedTimeAndprescriptionID(prescribedTimeInUTC, prescriptionId);
					if (adherenceDataPoints != null) {
						//if no_action we have to ovverride
						if (adherenceDataPoints.getConsumptionStatus()
						                       .contains("NO_Action")) {
							List<String> consumptionstatusList = new ArrayList<String>();
							consumptionstatusList.add(jsonObject.getString("consumptionstatus"));
							adherenceDataPoints.setConsumptionStatus(consumptionstatusList);
						} else {
							adherenceDataPoints.getConsumptionStatus()
							                   .add(jsonObject.getString("consumptionstatus"));
						}
						adherenceDataPoints.setObservedTime(observedTimeInUTC);
						adherenceDataPoints.setLocation(location);
						adherenceDataPoints.setHow(howList);

						persistedAdherence = adherenceService.addAdherence(adherenceDataPoints,
						                                                   prescribedTimeInUTC.format(dayamedGeneralFormatter));
						adherenceModel = utility.convertAdherenceEntityToModel(persistedAdherence);
						adherenceDataPointsModelList.add(adherenceModel);
						// New code: have to update here too notification schedule & prescripton schedule
						for (DosageInfo dosageInfo : prescriptionLvlDosageinfoList) {
							prescriptionScheduleService.updatePrescriptionScheduleByDosageInfo(prescribedTimeInUTC, dosageInfo.getId(),
							                                                                   jsonObject.getString("consumptionstatus")
							                                                                             .trim());
							/*update consumptionstatus in schedular jobs table,*/
							notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(prescribedTimeInUTC, dosageInfo.getId(),
							                                                                       jsonObject.getString("consumptionstatus")
							                                                                                 .trim());
							//end update consumption status in schedulerjob
						}
						//end
					} else {
						//new adherencedata points
						List<Adherence_DosageInfoMap> adherence_DosageInfoMapList = new ArrayList<>();
						ArrayList<String> dosageinfiIdsList = new ArrayList<>();
						for (DosageInfo dosageInfo : prescriptionLvlDosageinfoList) {

							PrescriptionModel pEescriptionModel = null;
							if (dosageInfo != null && dosageInfo.getPrescription() != null) {
								adherence.setPrescriptionID(dosageInfo.getPrescription()
								                                      .getId());
								pEescriptionModel = utility.convertPrescriptionEntityToModel(dosageInfo.getPrescription());
								patientModel = pEescriptionModel.getPatient();
							} else {
								logger.warn(ApplicationErrors.NOT_FOUND_PRESCRIPTION_WITH_ID);
								return new ResponseEntity<>(HttpStatus.NOT_FOUND);
							}

							prescriptionScheduleService.updatePrescriptionScheduleByDosageInfo(prescribedTimeInUTC, dosageInfo.getId(),
							                                                                   jsonObject.getString("consumptionstatus")
							                                                                             .trim());
							/*update consumptionstatus in schedular jobs table,*/
							notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(prescribedTimeInUTC, dosageInfo.getId(),
							                                                                       jsonObject.getString("consumptionstatus")
							                                                                                 .trim());
							//end update consumption status in schedulerjob

							//New code start
							if (StringUtils.equalsIgnoreCase(ApplicationConstants.CONSUMPTION_STATUS_SKIPPED, jsonObject.getString("consumptionstatus"))) {
								List<String> timeList = dosageInfo.getTime();
								int medDosagecount = timeList.size();
								PrescriptionSchedule prescriptionSchedule = new PrescriptionSchedule();
								prescriptionSchedule.setPrescribedTime(prescribedTimeInUTC);
								if (medDosagecount == 1) {
									prescriptionSchedule.setFireTime(prescribedTimeInUTC
											                                 .plusHours(3)
											                                 .plusMinutes(30));
								}
								if (medDosagecount == 2) {
									prescriptionSchedule.setFireTime(prescribedTimeInUTC
											                                 .plusHours(1)
											                                 .plusMinutes(30));
									// prescriptionSchedule.setFireTime(prescribedTimeInUTC.plusMinutes(3));
								}
								if (medDosagecount == 3) {
									prescriptionSchedule.setFireTime(prescribedTimeInUTC.plusHours(1));
									// prescriptionSchedule.setFireTime(prescribedTimeInUTC.plusMinutes(2));
								}
								if (medDosagecount > 3) {
									prescriptionSchedule.setFireTime(prescribedTimeInUTC
											                                 .plusMinutes(30));
								}
								prescriptionSchedule.setDosageInfo(dosageInfo);
								prescriptionSchedule.setUserId(pEescriptionModel.getPatient()
								                                                .getUserDetails()
								                                                .getId());
								List<String> consumptionStatuslist = new ArrayList<String>();
								consumptionStatuslist.add(ApplicationConstants.CONSUMPTION_STATUS_SECONDALERT_SKIPPED);
								prescriptionSchedule.setConsumptionStatus(consumptionStatuslist);
								prescriptionSchedule.setPrescriptionId(prescriptionId);
								//For notification msg need to add another column in table
								prescriptionScheduleRepository.save(prescriptionSchedule);
							}

							//New Code ends
							Adherence_DosageInfoMap adherence_DosageInfoMap = new Adherence_DosageInfoMap();
							adherence_DosageInfoMap.setDosageinfoId(dosageInfo.getId());
							adherence_DosageInfoMapList.add(adherence_DosageInfoMap);
						}
						adherence.setAdherenceDosageInfoMapList(adherence_DosageInfoMapList);
						adherence.getAdherenceDosageInfoMapList()
						         .forEach(adh_dosageMap -> {
							         adh_dosageMap.setAdherence(adherence);
						         });
						//adding adherence.
						persistedAdherence = adherenceService.addAdherence(adherence,
						                                                   prescribedTimeInUTC.format(dayamedGeneralFormatter));
						adherenceModel = utility.convertAdherenceEntityToModel(persistedAdherence);
						adherenceDataPointsModelList.add(adherenceModel);
					}

					if (patientModel == null) {
						logger.warn(ApplicationErrors.NOT_FOUND_PATIENT_IN_DB);
					}

					// sending mail to patient
					if (persistedAdherence != null && patientModel != null) {
						if (patientRecipientList == null) {
							patientRecipientList = new HashSet<String>();
						}
						patientRecipientList.add(patientModel.getUserDetails()
						                                     .getEmailId());
						medicineNameList = medicineNameList == null ? new ArrayList<String>() : medicineNameList;
						for (DosageInfoModel dosageInfo : adherenceModel.getDosageInfoList()) {
							medicineNameList.add(dosageInfo.getMedicine()
							                               .getName());
						}
						;
						consumptionStatus = persistedAdherence.getConsumptionStatus();
					}
				}
			}

			//SDM17013-1211- Consolidate dosage information in notifications.
			if (patientModel != null && CollectionUtils.isNotEmpty(medicineNameList)) {
				Long userId = patientModel.getUserDetails()
				                          .getId();
				//String subject = "Medication Status";
				String userLangPreference = utility.getUserPreferedLang(userId);
				String emailSubject =
						messageNotificationConstants.getUserPreferedLangTranslatedMsg(null, "emailsubject.medicinestatus", userLangPreference, null);
				Map<String, String> msgResourceReplaceKeyVal = buildAdherenceMsgForMessageResource(consumptionStatus,
				                                                                                   medicineNameList.stream()
				                                                                                                   .distinct()
				                                                                                                   .collect(Collectors.toList()),
				                                                                                   prescribedTime, jsonObject.getString("observedtime"),
				                                                                                   userLangPreference);
				String bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal,
				                                                                               "adherence.medicine.status.you", userLangPreference,
				                                                                               null);
				// sending mail and Sms to patient
				emailAndSmsUtility.sendEmail(emailSubject, bodyMsg, new ArrayList<String>(patientRecipientList));
				emailAndSmsUtility.sendSms(bodyMsg, patientModel.getUserDetails()
				                                                .getCountryCode(),
				                           patientModel.getUserDetails()
				                                       .getMobileNumber());
				// added into notification table for patient calander.
				notificationService.addWebNotification(patientModel.getUserDetails()
				                                                   .getId(), bodyMsg,
				                                       ApplicationConstants.CONSUMPTION_STATUS, jsonObject.getString("consumptionstatus"));

				//sending email and sms to caregiver
				String patientName = patientModel.getUserDetails()
				                                 .getFirstName() + " " + patientModel.getUserDetails()
				                                                                     .getLastName();
				msgResourceReplaceKeyVal.put("patientName", patientName);
				bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal, "adherence.medicine.status.patientname",
				                                                                        userLangPreference, null);
				List<String> caregiverMailList = new ArrayList<>();
				for (CaregiverModel caregiverModel : patientModel.getCaregivers()) {
					caregiverMailList.add(caregiverModel.getUserDetails()
					                                    .getEmailId());
					emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
					                                                  .getCountryCode(), caregiverModel.getUserDetails()
					                                                                                   .getMobileNumber());
					notificationService.addWebNotification(caregiverModel.getUserDetails()
					                                                     .getId(), bodyMsg, ApplicationConstants.MEDICATION_STATUS,
					                                       jsonObject.getString("consumptionstatus"));
				}
				if (caregiverMailList.size() > 0) {
					emailAndSmsUtility.sendEmail("Medication Status", bodyMsg, caregiverMailList);
				}
			}

			return new ResponseEntity<List<AdherenceDataPointsModel>>(adherenceDataPointsModelList, HttpStatus.OK);
		} catch (JSONException je) {
			logger.error("Error while parsing Json in adding adherence");
			je.getLocalizedMessage();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			logger.error("Error while adding adherence", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private Map<String, String> buildAdherenceMsgForMessageResource(List<String> consumptionStatus, List<String> medicineNameList, String prescribedTime,
	                                                                String observedTime,
	                                                                String userLangPreference) throws Exception {

		Map<String, String> keyVal = new HashMap<String, String>();
		keyVal.put("prescribedTime", prescribedTime);
		keyVal.put("observedTime", observedTime);
		keyVal.put("consumptionStatus", consumptionStatus.toString());
		String appendMedicineListWithAnd = messageNotificationConstants.prefixAndToList(medicineNameList, userLangPreference);
		keyVal.put("appendMedicineListWithAnd", appendMedicineListWithAnd);
		return keyVal;

	}

	/**
	 * While posting/updating adherence have to capture that data in Adherence Table,
	 * and consumpton_status should be update in
	 * scheduler_jobs,prescrition_schedule and notification tables
	 *
	 * @param adherencedatapoints
	 * @return AdherenceDataPointsModel
	 */

	@PreAuthorize("hasRole('PATIENT')")
	@PostMapping(value = "/rest/adherence/add", produces = "application/json", consumes = "application/json")
	public ResponseEntity<AdherenceDataPointsModel> addAdherence(@RequestBody String adherencedatapoints) {
		/*
		 * { "adherenceId":"1"/"", "how": [ "AR","NFC" ], "dosageinfoids": [ "8"
		 * ], "consumptionstatus": "consumed", "latitude": "56.130721",
		 * "longitude": "106.346337", "prescriptionid": 2, "prescribedTime":
		 * "2015-07-16 17:07", "observedtime":"2015-07-16 17:08",
		 * "zoneid":"Asia/Kolkata" }
		 */
		try {
			JSONObject jsonObject = new JSONObject(adherencedatapoints);
			if (!jsonObject.has("dosageinfoids") || !jsonObject.has("how") || !jsonObject.has("consumptionstatus")
					|| !jsonObject.has("prescriptionid") || !jsonObject.has("prescribedTime") || !jsonObject.has("observedtime") || !jsonObject.has("zoneid")) {
				logger.error("required inputs are missing in JSON while adding adherence");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			JSONArray dosageinfoidArray = jsonObject.getJSONArray("dosageinfoids");
			JSONArray howArray = jsonObject.getJSONArray("how");

			AdherenceDataPoints adherence = new AdherenceDataPoints();
			AdherenceDataPoints persistedAdherence = null;
			//adherence.setHow(jsonObject.getString("how"));
			HashSet<String> howList = new HashSet<>();
			for (int i = 0; i < howArray.length(); i++) {
				howList.add(howArray.getString(i));
			}
			adherence.setHow(howList);
			List<String> consumptionStatusList = new ArrayList<String>();
			consumptionStatusList.add(jsonObject.getString("consumptionstatus"));
			adherence.setConsumptionStatus(consumptionStatusList);
			ArrayList<String> location = new ArrayList<String>();
			if (jsonObject.has("latitude")) {
				location.add(jsonObject.getString("latitude"));
			}
			if (jsonObject.has("longitude")) {
				location.add(jsonObject.getString("longitude"));
			}
			String adherenceId = "";
			if (jsonObject.has("adherenceId")) {
				adherenceId = jsonObject.getString("adherenceId");
			}

			String prescribedTime = jsonObject.getString("prescribedTime");
			String zoneid = jsonObject.getString("zoneid");

			adherence.setLocation(location);
			LocalDateTime observedTimeInUTC = null;
			if (jsonObject.has("observedtime")) {
				observedTimeInUTC = dateUtility
						.convertLocalTimeToUTCTimeObjectByzoneid(jsonObject.getString("observedtime")
						                                                   .trim(), zoneid);
				adherence.setObservedTime(observedTimeInUTC);
			}
			adherence.setPrescriptionID(Long.parseLong(jsonObject.getString("prescriptionid")));

			LocalDateTime prescribedTimeInUTC = dateUtility
					.convertLocalTimeToUTCTimeObjectByzoneid(prescribedTime.trim(), zoneid);
			adherence.setPrescribedTime(prescribedTimeInUTC);

			PrescriptionModel prescriptionModel = prescriptionService
					.fetchPrescriptionByPrescriptionId(adherence.getPrescriptionID(), 0);

			AdherenceDataPointsModel adherenceModel = new AdherenceDataPointsModel();
			//Only when HOW parameter got updated.
			if (!StringUtils.isEmpty(adherenceId)) {
				Optional<AdherenceDataPoints> optionaladherenceDataPoints = adherenceRepository.findById(Long.parseLong(adherenceId));
				if (optionaladherenceDataPoints.isPresent()) {
					optionaladherenceDataPoints.get()
					                           .getHow()
					                           .addAll(howList);
					adherenceModel = utility
							.convertAdherenceEntityToModel(adherenceRepository.save(optionaladherenceDataPoints.get()));
				}
			} else {
				//for No_Action event triggered by Prescriptio_schedule Job.
				AdherenceDataPoints adherenceDataPoints =
						adherenceService.findByPrescribedTimeAndprescriptionID(prescribedTimeInUTC, Long.parseLong(jsonObject.getString("prescriptionid")));
				if (adherenceDataPoints != null) {
					if (adherenceDataPoints.getConsumptionStatus()
					                       .contains("NO_Action")) {
						List<String> consumptionstatusList = new ArrayList<String>();
						consumptionstatusList.add(jsonObject.getString("consumptionstatus"));
						adherenceDataPoints.setConsumptionStatus(consumptionstatusList);
					} else {
						adherenceDataPoints.getConsumptionStatus()
						                   .add(jsonObject.getString("consumptionstatus"));
					}
					adherenceDataPoints.setObservedTime(observedTimeInUTC);
					adherenceDataPoints.setLocation(location);
					adherenceDataPoints.setHow(howList);

					persistedAdherence = adherenceService.addAdherence(adherenceDataPoints,
					                                                   prescribedTimeInUTC.format(dayamedGeneralFormatter));
					adherenceModel = utility.convertAdherenceEntityToModel(persistedAdherence);

				} else {
					List<Adherence_DosageInfoMap> adherence_DosageInfoMapList = new ArrayList<>();
					ArrayList<String> dosageinfiIdsList = new ArrayList<>();
					for (int i = 0; i < dosageinfoidArray.length(); i++) {
						dosageinfiIdsList.add(dosageinfoidArray.getString(i));
						/*Before Adding adherence need to update consumptionstatus in prescription_schedule  schedular jobs tables
						Fetching data based on AcualdateTime and dosageID from PrescriptionSchedule*/
						prescriptionScheduleService.updatePrescriptionScheduleByDosageInfo(prescribedTimeInUTC, Long.parseLong(dosageinfoidArray.getString(i)),
						                                                                   jsonObject.getString("consumptionstatus")
						                                                                             .trim());
						/*update consumptionstatus in schedular jobs table,*/
						notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(prescribedTimeInUTC,
						                                                                       Long.parseLong(dosageinfoidArray.getString(i)),
						                                                                       jsonObject.getString("consumptionstatus")
						                                                                                 .trim());
						//end update consumption status in schedulerjob

						//New code start
						if (StringUtils.equalsIgnoreCase(ApplicationConstants.CONSUMPTION_STATUS_SKIPPED, jsonObject.getString("consumptionstatus"))) {
							DosageInfo dosageInfo = dosageInfoService.fetctDosageInfoByID(Long.parseLong(dosageinfoidArray.getString(i)));
							List<String> timeList = dosageInfo.getTime();
							int medDosagecount = timeList.size();

							PrescriptionSchedule prescriptionSchedule = new PrescriptionSchedule();

							prescriptionSchedule.setPrescribedTime(prescribedTimeInUTC);
							if (medDosagecount == 1) {
								prescriptionSchedule.setFireTime(prescribedTimeInUTC
										                                 .plusHours(3)
										                                 .plusMinutes(30));
							}
							if (medDosagecount == 2) {
								prescriptionSchedule.setFireTime(prescribedTimeInUTC
										                                 .plusHours(1)
										                                 .plusMinutes(30));
								// prescriptionSchedule.setFireTime(prescribedTimeInUTC.plusMinutes(3));
							}
							if (medDosagecount == 3) {
								prescriptionSchedule.setFireTime(prescribedTimeInUTC.plusHours(1));
								// prescriptionSchedule.setFireTime(prescribedTimeInUTC.plusMinutes(2));
							}
							if (medDosagecount > 3) {
								prescriptionSchedule.setFireTime(prescribedTimeInUTC
										                                 .plusMinutes(30));
							}
							prescriptionSchedule.setDosageInfo(dosageInfo);
							prescriptionSchedule.setUserId(prescriptionModel.getPatient()
							                                                .getUserDetails()
							                                                .getId());
							List<String> consumptionStatuslist = new ArrayList<String>();
							consumptionStatuslist.add(ApplicationConstants.CONSUMPTION_STATUS_SECONDALERT_SKIPPED);
							prescriptionSchedule.setConsumptionStatus(consumptionStatuslist);

							//For notification msg need to add another column in table
							prescriptionScheduleRepository.save(prescriptionSchedule);
						}

						//New Code ends
						Adherence_DosageInfoMap adherence_DosageInfoMap = new Adherence_DosageInfoMap();
						adherence_DosageInfoMap.setDosageinfoId(Long.parseLong(dosageinfoidArray.getString(i)));
						adherence_DosageInfoMapList.add(adherence_DosageInfoMap);
					}
					adherence.setAdherenceDosageInfoMapList(adherence_DosageInfoMapList);
					adherence.getAdherenceDosageInfoMapList()
					         .forEach(adh_dosageMap -> {
						         adh_dosageMap.setAdherence(adherence);
					         });
					//adding adherence.
					persistedAdherence = adherenceService.addAdherence(adherence,
					                                                   prescribedTimeInUTC.format(dayamedGeneralFormatter));
					adherenceModel = utility.convertAdherenceEntityToModel(persistedAdherence);
				}

				if (prescriptionModel == null) {
					logger.warn(ApplicationErrors.NOT_FOUND_PRESCRIPTION_WITH_ID);
				}
				// sending mail to patient
				if (persistedAdherence != null && prescriptionModel != null) {
					String subject = "Medication Status";
					List<String> recipientList = new ArrayList<String>();
					recipientList.add(prescriptionModel.getPatient()
					                                   .getUserDetails()
					                                   .getEmailId());
					List<String> medicineNameList = new ArrayList<>();
					adherenceModel.getDosageInfoList()
					              .forEach(dosageInfo -> {
						              medicineNameList.add(dosageInfo.getMedicine()
						                                             .getName());
					              });

					String bodyMsg = "You have " + persistedAdherence.getConsumptionStatus()
							+ " medication, Medication details : " + String.join(" and ", medicineNameList)
							+ " actual time :" + prescribedTime + " and noticed at " + jsonObject.getString("observedtime");

					/*String bodyMsg = "You have " + persistedAdherence.getConsumptionStatus()+ " medication, Medication details available in this link : "+
							"/"+ApplicationConstants.ADHERENCE_DEEPLINK +persistedAdherence.getId()+"&apn=com.seneca.dayamed"
							+ ". actual time is:" + prescribedTime + " and noticed at "+jsonObject.getString("observedtime");*/

					String notificationbodyMsg = "You have " + persistedAdherence.getConsumptionStatus()
							+ " medication, Medication details : " + String.join(" and ", medicineNameList)
							+ " actual time :" + prescribedTime + " and noticed at " + jsonObject.getString("observedtime");
					//sending mail and Sms to patient
					emailAndSmsUtility.sendEmail(subject, bodyMsg, recipientList);
					emailAndSmsUtility.sendSms(bodyMsg, prescriptionModel.getPatient()
					                                                     .getUserDetails()
					                                                     .getCountryCode(), prescriptionModel.getPatient()
					                                                                                         .getUserDetails()
					                                                                                         .getMobileNumber());
					// added into notification table for patient calander.
					notificationService.addWebNotification(prescriptionModel.getPatient()
					                                                        .getUserDetails()
					                                                        .getId(), notificationbodyMsg, ApplicationConstants.CONSUMPTION_STATUS,
					                                       jsonObject.getString("consumptionstatus"));
					recipientList.clear();
					//sending email and sms to caregiver

					String patientName = prescriptionModel.getPatient()
					                                      .getUserDetails()
					                                      .getFirstName() + " " + prescriptionModel.getPatient()
					                                                                               .getUserDetails()
					                                                                               .getLastName();
					bodyMsg = patientName + " have " + persistedAdherence.getConsumptionStatus()
							+ " medication, Medication details : " + String.join(" and ", medicineNameList)
							+ " actual time :" + prescribedTime + " and noticed at " + jsonObject.getString("observedtime");

					//if we keep deeplink while saving in notification table getting column length issue
					notificationbodyMsg = patientName + " have " + persistedAdherence.getConsumptionStatus()
							+ " medication, Medication details : " + String.join(" and ", medicineNameList)
							+ " actual time :" + prescribedTime + " and noticed at " + jsonObject.getString("observedtime");

					List<String> caregiverMailList = new ArrayList<>();
					for (CaregiverModel caregiverModel : prescriptionModel.getPatient()
					                                                      .getCaregivers()) {
						caregiverMailList.add(caregiverModel.getUserDetails()
						                                    .getEmailId());
						emailAndSmsUtility.sendSms(bodyMsg, caregiverModel.getUserDetails()
						                                                  .getCountryCode(), caregiverModel.getUserDetails()
						                                                                                   .getMobileNumber());
						// added into notification table for caregiver calander.
						notificationService.addWebNotification(caregiverModel.getUserDetails()
						                                                     .getId(), notificationbodyMsg, ApplicationConstants.MEDICATION_STATUS,
						                                       jsonObject.getString("consumptionstatus"));
					}
					if (caregiverMailList.size() > 0) {
						emailAndSmsUtility.sendEmail("Medication Status", bodyMsg, caregiverMailList);
					}
				}
			}
			return new ResponseEntity<AdherenceDataPointsModel>(adherenceModel, HttpStatus.OK);
		} catch (JSONException je) {
			logger.error("Error while parsing Json in adding adherence");
			je.getLocalizedMessage();
			return new ResponseEntity<AdherenceDataPointsModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			logger.error("Error while adding adherence");
			return new ResponseEntity<AdherenceDataPointsModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// fetch adherence datapoints from latest preescription of patientId
	@GetMapping(value = "/rest/adherence/{patientid}/multiple", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Id", notes = "Returns Adherence")
	public ResponseEntity<MedAndDeviceAdheranceDataModel> fetchNewMedAndDeviceAdherencesBypatientId(
			@PathVariable(value = "patientid") String patientid) {
		try {
			if (StringUtils.isBlank(patientid)) {
				logger.error("path variable patient id must not be null or empty.");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			//Prescription prescription = prescriptionService.fetchLatestPrescriptionByPatientId(Long.parseLong(patientid));
			Patient patient = patientService.directFetchPatientByPatientId(Long.valueOf(patientid));
			if (patient == null) {
				logger.error("patient not found in DB");
				return new ResponseEntity<MedAndDeviceAdheranceDataModel>(HttpStatus.NOT_FOUND);
			}
			MedAndDeviceAdheranceDataModel medAndDeviceAdheranceDataModel = new MedAndDeviceAdheranceDataModel();
			List<AdherenceDataPointsModel> adherenceModelList = new ArrayList<AdherenceDataPointsModel>();
			List<DeviceAdheranceDataModel> deviceAdheranceDataModelList = new ArrayList<DeviceAdheranceDataModel>();
			for (Prescription prescription : prescriptionService.fetchPrescriptionsByPatientId(Long.parseLong(patientid))) {
				Authentication authentication = SecurityContextHolder.getContext()
				                                                     .getAuthentication();

				/* int result = baseRestApiImpl.instanceOfPrescription(prescription, jwtUser);
				 if(result == 401){
	         		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	         	}*/

				if ((prescription != null)) {
					List<AdherenceDataPoints> adherenceList = adherenceService.findByprescriptionID(prescription.getId());
					adherenceList.forEach(adherence -> {
						adherenceModelList.add(utility.convertAdherenceEntityToModel(adherence));
					});

					//Sorting in decreasing order
					Collections.sort(adherenceModelList, new SortAdherencebyPriscribedTime());
					medAndDeviceAdheranceDataModel.setMedicineAdherenceList(adherenceModelList);
					DeviceAdheranceDataModel deviceAdheranceDataModel = null;
					for (BpMonitor bpMonitor : bpMonitorRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (bpMonitor.getLocation() != null && bpMonitor.getLocation()
						                                                .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();
							deviceAdheranceDataModel.setConsumptionStatus(bpMonitor.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(bpMonitor.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(bpMonitor.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(bpMonitor.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(bpMonitor.getSystolicpressureValue() + " mmHg");
							deviceAdheranceDataModel.setReading2(bpMonitor.getDiastolicpressureValue() + " mmHg");
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.BP_METER);
							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					for (Scale scale : scaleRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (scale.getLocation() != null && scale.getLocation()
						                                        .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();
							deviceAdheranceDataModel.setConsumptionStatus(scale.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(scale.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(scale.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(scale.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(scale.getReading());
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.SCALE);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					for (HeartRate heartRate : heartRateRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (heartRate.getLocation() != null && heartRate.getLocation()
						                                                .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();

							deviceAdheranceDataModel.setConsumptionStatus(heartRate.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(heartRate.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(heartRate.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(heartRate.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(heartRate.getReadingValue() + " bpm");
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.HEART_RATE);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					for (Glucometer glucometer : glucoMeterRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (glucometer.getLocation() != null && glucometer.getLocation()
						                                                  .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();
							deviceAdheranceDataModel.setConsumptionStatus(glucometer.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(glucometer.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(glucometer.getObservedTime()));
							deviceAdheranceDataModel.setReading1(glucometer.getReadingValue());
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(glucometer.getPrescribedTime()));
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.GLUCO_METER);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}

					for (Steps steps : stepsRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (steps.getLocation() != null && steps.getLocation()
						                                        .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();

							deviceAdheranceDataModel.setConsumptionStatus(steps.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(steps.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(steps.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(steps.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(String.valueOf(steps.getStepCount()));
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.STEPS);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					//Sorting in decreasing order
					Collections.sort(deviceAdheranceDataModelList, new SortDeviceAdherancebyPriscribedTime());
					medAndDeviceAdheranceDataModel.setDeviceAdherenceList(deviceAdheranceDataModelList);
				} else {
					logger.warn("prescription not found for this patient");
					return new ResponseEntity<MedAndDeviceAdheranceDataModel>(HttpStatus.NOT_FOUND);
				}
			}
			return new ResponseEntity<MedAndDeviceAdheranceDataModel>(medAndDeviceAdheranceDataModel, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error while fetching adherence and vitals");
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// fetch adherence datapoints from latest preescription of patientId
	@GetMapping(value = "/rest/adherence/{patientid}", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Id", notes = "Returns Adherence")
	public ResponseEntity<MedAndDeviceAdheranceDataModel> fetchMedAndDeviceAdherencesBypatientId(
			@PathVariable(value = "patientid") String patientid) {

		try {
			if (StringUtils.isBlank(patientid)) {
				logger.error("path variable patient id must not be null or empty.");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			//Prescription prescription = prescriptionService.fetchLatestPrescriptionByPatientId(Long.parseLong(patientid));
			Patient patient = patientService.directFetchPatientByPatientId(Long.valueOf(patientid));
			if (patient == null) {
				logger.error("patient not found in DB");
				return new ResponseEntity<MedAndDeviceAdheranceDataModel>(HttpStatus.NOT_FOUND);
			}
			MedAndDeviceAdheranceDataModel medAndDeviceAdheranceDataModel = new MedAndDeviceAdheranceDataModel();
			List<AdherenceDataPointsModel> adherenceModelList = new ArrayList<AdherenceDataPointsModel>();
			List<DeviceAdheranceDataModel> deviceAdheranceDataModelList = new ArrayList<DeviceAdheranceDataModel>();
			for (Prescription prescription : prescriptionService.fetchPrescriptionsByPatientId(Long.parseLong(patientid))) {
			/* int result = baseRestApiImpl.instanceOfPrescription(prescription, jwtUser);
			 if(result == 401){
         		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
         	}*/
				if ((prescription != null)) {
					List<AdherenceDataPoints> adherenceList = adherenceService.findByprescriptionID(prescription.getId());
					adherenceList.forEach(adherence -> {
						adherenceModelList.add(utility.convertAdherenceEntityToModel(adherence));
					});
					//Sorting in decreasing order
					Collections.sort(adherenceModelList, new SortAdherencebyPriscribedTime());
					medAndDeviceAdheranceDataModel.setMedicineAdherenceList(adherenceModelList);
					DeviceAdheranceDataModel deviceAdheranceDataModel = null;
					for (BpMonitor bpMonitor : bpMonitorRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (bpMonitor.getLocation() != null && bpMonitor.getLocation()
						                                                .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();
							deviceAdheranceDataModel.setConsumptionStatus(bpMonitor.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(bpMonitor.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(bpMonitor.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(bpMonitor.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(bpMonitor.getSystolicpressureValue() + " mmHg");
							deviceAdheranceDataModel.setReading2(bpMonitor.getDiastolicpressureValue() + " mmHg");
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.BP_METER);
							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					for (Scale scale : scaleRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (scale.getLocation() != null && scale.getLocation()
						                                        .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();
							deviceAdheranceDataModel.setConsumptionStatus(scale.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(scale.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(scale.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(scale.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(scale.getReading());
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.SCALE);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					for (HeartRate heartRate : heartRateRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (heartRate.getLocation() != null && heartRate.getLocation()
						                                                .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();

							deviceAdheranceDataModel.setConsumptionStatus(heartRate.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(heartRate.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(heartRate.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(heartRate.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(heartRate.getReadingValue() + " bpm");
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.HEART_RATE);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					for (Glucometer glucometer : glucoMeterRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (glucometer.getLocation() != null && glucometer.getLocation()
						                                                  .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();
							deviceAdheranceDataModel.setConsumptionStatus(glucometer.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(glucometer.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(glucometer.getObservedTime()));
							deviceAdheranceDataModel.setReading1(glucometer.getReadingValue());
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(glucometer.getPrescribedTime()));
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.GLUCO_METER);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}

					for (Steps steps : stepsRepository.findByPatient_id(Long.parseLong(patientid))) {
						if (steps.getLocation() != null && steps.getLocation()
						                                        .size() > 0) {
							deviceAdheranceDataModel = new DeviceAdheranceDataModel();

							deviceAdheranceDataModel.setConsumptionStatus(steps.getConsumptionStatus());
							deviceAdheranceDataModel.setLocation(steps.getLocation());
							deviceAdheranceDataModel.setObservedTime(
									dateUtility.convertLocalDateTimeToDateObject(steps.getObservedTime()));
							deviceAdheranceDataModel.setPrescribedTime(dateUtility.convertLocalDateTimeToDateObject(steps.getPrescribedTime()));
							deviceAdheranceDataModel.setReading1(String.valueOf(steps.getStepCount()));
							deviceAdheranceDataModel.setDeviceName(ApplicationConstants.STEPS);

							deviceAdheranceDataModelList.add(deviceAdheranceDataModel);
						}
					}
					//Sorting in decreasing order
					Collections.sort(deviceAdheranceDataModelList, new SortDeviceAdherancebyPriscribedTime());
					medAndDeviceAdheranceDataModel.setDeviceAdherenceList(deviceAdheranceDataModelList);
				} else {
					logger.warn("prescription not found for this patient");
					return new ResponseEntity<MedAndDeviceAdheranceDataModel>(HttpStatus.NOT_FOUND);
				}
			}
			return new ResponseEntity<MedAndDeviceAdheranceDataModel>(medAndDeviceAdheranceDataModel, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error while fetching adherence and vitals");
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @author naresh.kamisetti
	 * Used for sorting in descending order of id
	 * number
	 */
	class SortAdherencebyPriscribedTime implements Comparator<AdherenceDataPointsModel> {

		public int compare(AdherenceDataPointsModel o1, AdherenceDataPointsModel o2) {
			if (o1.getPrescribedTime() == null || o2.getPrescribedTime() == null) {
				return 0;
			}
			return o2.getPrescribedTime()
			         .compareTo(o1.getPrescribedTime());
		}

	}

	class SortDeviceAdherancebyPriscribedTime implements Comparator<DeviceAdheranceDataModel> {

		public int compare(DeviceAdheranceDataModel a, DeviceAdheranceDataModel b) {
			return (int) (b.getPrescribedTime()
			               .compareTo(a.getPrescribedTime()));//desending order
		}

	}

	// fetchAdherence By PatientId
	@GetMapping(value = "/rest/adherence/patient/analytics/{patientId}", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Patient Id", notes = "Returns Adherence")
	public ResponseEntity<PatientAdherencePojo> fetchPatientAnalyticsById(@PathVariable(value = "patientId") String patientId) {
		try {
			if (StringUtils.isBlank(patientId)) {
				logger.error(ApplicationErrors.PATIENT_ID_SHOULD_NOT_NULL);
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			PatientAdherencePojo patientAdherencePojo = null;
			//PatientAdherencePojo patientAdherencePojo = patientService.fetchAdherenceByPatientId(Long.parseLong(patientId));
			if (StringUtils.isNotBlank(patientId)) {
				Patient patient = patientService.fetchPatientById(Long.parseLong(patientId));
				Authentication authentication = SecurityContextHolder.getContext()
				                                                     .getAuthentication();
				JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
				int result = utility.instanceOfPatient(patient, jwtUser);
				if (result == 401) {
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
				PatientModel patientModel = utility.setLatestAdherenceValues(utility.convertPatientEntityTopatientModel(patient));
				if (patientModel != null && StringUtils.isNotBlank(patientModel.getCurrentAdherence()) &&
						StringUtils.isNotBlank(patientModel.getPredictedAdherence()) && StringUtils.isNotBlank(patientModel.getProjectedAdherence()) &&
						StringUtils.isNotBlank(patientModel.getEmoji())) {
					//updating patient analytics
					//patientService.updatePatientAlone(utility.convertPatientModelTopatientEntity(patientModel));
					patientAdherencePojo = new PatientAdherencePojo(patientModel.getCurrentAdherence(), patientModel.getPredictedAdherence(),
					                                                patientModel.getProjectedAdherence(), patientModel.getEmoji());
				}
			}
			if (patientAdherencePojo == null) {
				logger.warn("adherence data not found for this patient");
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<PatientAdherencePojo>(patientAdherencePojo, HttpStatus.OK);
		} catch (Exception e) {
			e.getMessage();
			return new ResponseEntity<PatientAdherencePojo>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/rest/adherence/byadherenceid/{adherenceID}", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Patient Id", notes = "Returns Adherence")
	public ResponseEntity<AdherenceDataPointsModel> fetchAdherenceDatapointsById(@PathVariable(value = "adherenceID") String adherenceID) {
		try {
			if (StringUtils.isBlank(adherenceID)) {
				logger.error("Adherence Id should not null");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			AdherenceDataPoints adherenceDataPoints = adherenceService.fetchAdherenceById(Long.parseLong(adherenceID));
			AdherenceDataPointsModel adherenceDataPointsModel = utility.convertAdherenceEntityToModel(adherenceDataPoints);
			if (adherenceDataPoints == null) {
				logger.warn("adherence data not found for this Id");
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<AdherenceDataPointsModel>(adherenceDataPointsModel, HttpStatus.OK);
		} catch (Exception e) {
			e.getMessage();
			return new ResponseEntity<AdherenceDataPointsModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/rest/adherence/dosageinfo/{dosageInfoId}", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By dosageinfo Id", notes = "Returns Adherence")
	public ResponseEntity<AdherenceDataPointsModel> fetchAdherenceByDosageInfoId(@PathVariable(value = "adherenceID") String adherenceID) {
		return null;

	}

	@Autowired
	AdherenceReportService adherenceReportService;

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping(value = "/rest/utility/getAdherenceReport", produces = "application/json")
	public ResponseEntity<Object> getAdherenceReport(@CurrentUser JwtUserDetails jwtUser,
	                                                 @RequestParam(value = "providerId", required = false) List<Long> providerIds) throws Exception {
		return inboundServiceCallGeneric(jwtUser, service -> {
			AdherenceReportResModel adherenceReportResModel = adherenceReportService.getAdherenceReport(providerIds,
			                                                                                            jwtUser.getUserEmailId());
			return new ResponseEntity<Object>(adherenceReportResModel, HttpStatus.OK);
		});
	}


}