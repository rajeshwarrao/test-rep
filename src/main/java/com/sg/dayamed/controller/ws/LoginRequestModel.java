package com.sg.dayamed.controller.ws;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class LoginRequestModel {

	@NotBlank(message = "{username.is.required}")
	@Email(message = "{email.address.is.not.valid.format}")
	@JsonProperty("username")
	private String userName;

	@NotBlank(message = "{password.is.required}")
	private String password;

	@NotBlank(message = "{typeofdevice.is.required}")
	@JsonProperty("typeofdevice")
	private String typeOfDevice;

	@JsonProperty("osversion")
	private String osVersion;

	@JsonProperty("appversion")
	private String appVersion;

	@JsonProperty("deviceid")
	private String deviceId;
}
