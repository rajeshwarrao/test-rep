package com.sg.dayamed.controller.ws;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 02/Sep/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class WSUserEncryptionRequest {
	private String decryptedPath;
	private String encryptedPath;
}
