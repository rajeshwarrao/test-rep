package com.sg.dayamed.controller.ws;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
public class ImanticNotificationReqModel {

	@NotBlank(message = "{imantic.notification.imanticid.required}")
	@JsonProperty("imanticid")
	private String imanticId;

	@NotBlank(message = "{imantic.notification.message.required}")
	private String message;

	@NotBlank(message = "{imantic.notification.tolist.required}")
	@JsonProperty("tolist")
	private List<String> toList;

	@NotBlank(message = "{imantic.notification.alerttype.required}")
	@JsonProperty("alerttype")
	private List<String> alertType;
}
