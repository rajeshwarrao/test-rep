package com.sg.dayamed.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.commons.rest.RestRequest;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.helper.pojo.DeletePrescriptionVO;
import com.sg.dayamed.helper.pojo.GenericIDAndZoneVO;
import com.sg.dayamed.pojo.DiagnosisModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.DiagnosisService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Patient Operations")
public class PrescriptionController extends BaseRestApiImpl {

	@Autowired
	PatientService patientService;

	@Autowired
	PrescriptionStatusRepository prescriptionStatusRepository;

	@Autowired
	PrescriptionService prescriptionService;

	@Autowired
	Utility utility;

	@Autowired
	DiagnosisService diagnosisService;
	
	
	@PreAuthorize("hasRole('PROVIDER')")
	@GetMapping(value = "/rest/prescriptions/diagnosis/intelligence/{name}", produces = "application/json")
	@ApiOperation(value = "Fetch diagnosis intelligence By diagnosis name", notes = "Returns Diagnosis List")
	public ResponseEntity<Object> searchDiagnosesByName(@PathVariable(value = "name") String name) throws
	                                                                                                RestApiException {
		return inboundServiceCallGeneric(name, service -> {
			List<DiagnosisModel> diagnosisModelList =
					diagnosisService.fetchDiagnosisModelIntelligenceByDescription(name);
			return ResponseEntity.ok(diagnosisModelList);
		});
	}

	// fetchPrescriptionlistByPatientId(patient with all prescriptions)
	@GetMapping(value = "/rest/patients/{patientid}/prescriptions", produces = "application/json")
	@ApiOperation(value = "Fetch Prescriptionlist ByPatientId", notes = "Returns Prescriptionlist")
	public ResponseEntity<Object> getPrescriptionListByPatientId(
			@PathVariable(value = "patientid") Long id, Authentication authentication)
			throws Exception {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			List<PrescriptionModel> prescriptionModelList =
					prescriptionService.fetchPrescriptionListByPatientId(id, authentication);
			return ResponseEntity.ok(prescriptionModelList);
		});
	}

	//work Pending
	@PreAuthorize("hasAnyRole('PROVIDER','CAREGIVER')")
	@PostMapping(value = "/rest/patients/{id}/prescriptions/add", produces = "application/json", consumes =
			"application/json")
	public ResponseEntity<Object> addPrescription(@PathVariable(value = "id") Long patientid,
	                                              @RequestBody PrescriptionModel prescriptionmodel,
	                                              @CurrentUser JwtUserDetails jwtUser) throws RestApiException {

		return inboundServiceCallGeneric(prescriptionmodel, service -> {
			PrescriptionModel prescriptionModel =
					prescriptionService.addPrescription(prescriptionmodel, patientid,
							jwtUser);
			return ResponseEntity.ok(prescriptionModel);
		});
	}

	@GetMapping(value = "/rest/prescriptions/latest/{patientid}", produces = "application/json")
	@ApiOperation(value = "Fetch LatestPrescriptionByPatientId", notes = "Returns Prescription")
	public ResponseEntity<Object> fetchLatestPrescriptionByPatientId(
			@PathVariable(value = "patientid") Long id, Authentication authentication)
			throws RestApiException {
		return inboundServiceCallGeneric(id, service -> {
			PrescriptionModel prescriptionModel =
					prescriptionService.validateFetchLatestPrescriptionByPatientId(id, authentication);
			return ResponseEntity.ok(prescriptionModel);
		});
	}

	@PostMapping(value = "/rest/prescriptions/patient/active", produces = "application/json")
	@ApiOperation(value = "Fetch LatestPrescriptionByPatientId", notes = "Returns Prescription")
	public ResponseEntity<Object> fetchActivePrescriptionListByPatientId(
			@RequestBody GenericIDAndZoneVO genericIDAndZoneVO,
			Authentication authentication) throws RestApiException {
		return inboundServiceCallGeneric(genericIDAndZoneVO, service -> {
			List<PrescriptionModel> prescriptionModelList =
					prescriptionService.fetchActivePrescriptionListByPatientId(genericIDAndZoneVO, authentication);
			return ResponseEntity.ok(prescriptionModelList);
		});
	}

	// fetchPrescriptionByPrescriptionId
	@GetMapping(value = "/rest/prescriptions/{preid}", produces = "application/json")
	@ApiOperation(value = "Fetch Prescription ByPrescriptionId", notes = "Returns Prescription")
	public ResponseEntity<Object> fetchPrescriptionByPrescriptionId(
			@PathVariable(value = "preid") Long preid, Authentication authentication)
			throws RestApiException {

		return inboundServiceCallGeneric(preid, service -> {
			PrescriptionModel prescriptionModel =
					prescriptionService.validateFetchPrescriptionByPrescriptionId(preid,
					                                                              authentication);
			return ResponseEntity.ok(prescriptionModel);
		});
	}

	// fetchPrescriptionByNotificationstatus
	@GetMapping(value = "/rest/prescriptions/status", produces = "application/json")
	@ApiOperation(value = "Fetch Prescription Status", notes = "Returns Prescription Status")
	public ResponseEntity<Object> fetchPrescriptionStatusList() throws RestApiException {
		return inboundServiceCallGeneric(new RestRequest(), service -> {
			List<PrescriptionStatus> PrescriptionStatusList = new ArrayList<>();
			prescriptionStatusRepository.findAll()
			                            .forEach(PrescriptionStatusList::add);
			return ResponseEntity.ok(PrescriptionStatusList);
		});
	}

	// Update PrescriptionBystatus
	@PostMapping(value = "/rest/prescriptions/updatestatus", produces = "application/json", consumes = "application" +
			"/json")
	public ResponseEntity<Object> updatePrescriptionStatus(@RequestBody PrescriptionModel prescriptionModel
			, Authentication authentication) throws RestApiException {
		return inboundServiceCallGeneric(prescriptionModel, service -> {
			PrescriptionModel PrescriptionModel =
					prescriptionService.updatePrescriptionStatus(prescriptionModel,authentication);
			return ResponseEntity.ok(PrescriptionModel);
		});
	}

	@PreAuthorize("hasAnyRole('PROVIDER','ADMIN')")
	@DeleteMapping(value = "/rest/prescriptions/delete/{prescrId}", produces = "application/json")
	public ResponseEntity<Object> deletePrescriptionById(@PathVariable(value = "prescrId") Long prescrId)
			throws  NumberFormatException, RestApiException {
		return inboundServiceCallGeneric(prescrId, service -> {
			DeletePrescriptionVO deletePrescriptionVO =
					prescriptionService.deletePrescriptionById(prescrId);
			return ResponseEntity.ok(deletePrescriptionVO);
		});
	}

}
