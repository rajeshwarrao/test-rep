package com.sg.dayamed.controller;

import com.sg.dayamed.entity.ConsentDocument;
import com.sg.dayamed.entity.UserConsent;
import com.sg.dayamed.helper.pojo.ConcentDocResponse;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.ConsentDocumentRerpository;
import com.sg.dayamed.repository.UserConsentRepository;
import com.sg.dayamed.service.RediseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author naresh.kamisetti
 */
@RestController
//@RequestMapping("")
@Api(value = "User Consent")
public class UserConsentController {

	@Autowired
	ConsentDocumentRerpository consentDocumentRerpository;

	@Autowired
	UserConsentRepository userConsentRepository;

	@Value("${dayamed.userconsent.update.fields}")
	String dayamedUserconsentUpdateFields;

	@Autowired
	RediseService rediseService;

	private static final Logger logger = LoggerFactory.getLogger(UserConsentController.class);

	//fetchLatestVersionOfUserConcent(long userid); API-1
	@GetMapping(value = "/rest/userconsent/{userid}", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Patient Id", notes = "Returns Adherence")
	public ResponseEntity<UserConsent> fetchLatestVersionOfUserConcent(@PathVariable(value = "userid") String userid) {
		try {
			String userConcentEnable =
					(String) rediseService.getDataInRedisByKey(RedisKeyConstants.USERCONSENT_ENABLE);
			logger.info("User Concent Enable Feature.." + userConcentEnable);
			if (!userConcentEnable.equalsIgnoreCase("true")) {
				UserConsent newUserConsent = new UserConsent();
				newUserConsent.setStatus(true);
				newUserConsent.setUserId(Long.parseLong(userid));
				return new ResponseEntity<UserConsent>(newUserConsent, HttpStatus.OK);
			}

			UserConsent userConsent = null;
			String latestversion = "";
			ConsentDocument consentDocument = consentDocumentRerpository.findFirst1ByOrderByPublicationDateDesc();
			if (consentDocument != null) {
				latestversion = consentDocument.getVersion();
				if (!"".equalsIgnoreCase(latestversion)) {
					userConsent = userConsentRepository.findByUserIdAndVersion(Long.parseLong(userid), latestversion);
					if (userConsent == null) {
						// if userconcent not availble then need to insert one
						// record with status false.
						UserConsent newUserConsent = new UserConsent();
						newUserConsent.setStatus(false);
						newUserConsent.setUserId(Long.parseLong(userid));
						newUserConsent.setVersion(latestversion);

						userConsent = userConsentRepository.save(newUserConsent);
						logger.info("userconcesnt created");
						return new ResponseEntity<UserConsent>(userConsent, HttpStatus.OK);
					} else {
						logger.info("return existed userconcesnt");
						return new ResponseEntity<UserConsent>(userConsent, HttpStatus.OK);
					}
				}
			}
			return new ResponseEntity<UserConsent>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.getMessage();
			return new ResponseEntity<UserConsent>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//API - 2
	@GetMapping(value = "/rest/html/consentdocument", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Patient Id", notes = "Returns Adherence")
	public ResponseEntity<ConcentDocResponse> htmlConsent() {
		try {
			ConsentDocument consentDocument = consentDocumentRerpository.findFirst1ByOrderByPublicationDateDesc();
			String response = "";
			if (consentDocument != null) {
				response = consentDocument.getDocBody();
			}
			logger.info("consent documet returned");
			return new ResponseEntity<ConcentDocResponse>(
					new ConcentDocResponse(response, consentDocument != null ? consentDocument.getVersion() : ""),
					HttpStatus.OK);
		} catch (Exception e) {
			e.getMessage();
			return new ResponseEntity<ConcentDocResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//API -3
	//@PreAuthorize("hasRole('PATIENT')")
	@PostMapping(value = "/rest/userconcent/status/update", produces = "application/json", consumes = "application" +
			"/json")
	public ResponseEntity<UserConsent> updateUserConsentStatus(@RequestBody String userConcentInfo) {
			/*{
				"userid":"24",
				"status":true,
				"version":"7"
				}*/
		try {
			JSONObject jsonObject = new JSONObject(userConcentInfo);
			UserConsent userConsent = null;

			List<String> errorStringList = new ArrayList<String>();
			for (String field : dayamedUserconsentUpdateFields.split(",")) {
				if (!jsonObject.has(field.trim())) {
					errorStringList.add(field);
				}
			}
			if (errorStringList.size() > 0) {
				String errorString = String.join(" and ", errorStringList);
				logger.error(errorString + " missing in input json");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			ConsentDocument consentDocument = consentDocumentRerpository.findFirst1ByOrderByPublicationDateDesc();
			String version =
					consentDocument.getVersion(); //latest version is taking by default if not provided from input
			// json.
			userConsent = userConsentRepository.findByUserIdAndVersion(Long.parseLong(jsonObject.optString("userid")),
			                                                           version);

			if (jsonObject.has("version")) {
				version = jsonObject.optString("version"); //update version if provides
			}
			if (jsonObject.getBoolean("status")) {
				userConsent.setStatus(true);
				userConsent.setVersion(version);
				userConsent = userConsentRepository.save(userConsent);
			}
			logger.info("userconcent has been updated");
			return new ResponseEntity<UserConsent>(userConsent, HttpStatus.OK);

		} catch (JSONException e) {
			logger.error("error while updating aggriment status in Userconsent");
			e.getMessage();
			return new ResponseEntity<UserConsent>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/addconsentdoc", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Patient Id", notes = "Returns Adherence")
	public ResponseEntity<ConsentDocument> addConsentDoc() {
		try {
			ConsentDocument consentDocument = new ConsentDocument();

			String body =
					"<h4>Employing a digital health platform to enhance medication adherence and satisfaction in " +
							"patients from a" +
							"          membership pharmacy for the uninsured </h4>" +
							"        <h3 style=\"text-align:center\">INFORMATION AND CONSENT FORM</h3>" +
							"        <h5>Introduction:</h5>" +
							"        <p> You are invited to take part in a research study trying to find out how " +
							"well" +
							" " +
							"people take their medicine. If" +
							"          you decide to take part, you will use a smartphone application or ‘app’ to " +
							"record your doses and tell us" +
							"          yourself. This study is being led by Bridget Lynch, a post graduate year 2 " +
							"(PGY2) pharmacy resident at" +
							"          Lipscomb University College of Pharmacy under the direction of Kevin Clauson " +
							"and Beth Breeden, faculty" +
							"          members in the Pharmacy Department. You were selected as a possible " +
							"participant" +
							" in this research because" +
							"          you are a current patient of Good Shepherd Pharmacy and own a smart phone. " +
							"Please read this form and ask" +
							"          questions before you agree to be in the study.</p>" +
							"        <h5>Background Information:</h5>" +
							"        <p> The purpose of this study is to measure how well patients take their " +
							"medications with the smartphone app" +
							"          DayaMed. We also want to try and find out: how people use the smartphone app " +
							"DayaMed to see how well" +
							"          Real-time Medication Adherence (RMA) works as way of measuring how well " +
							"patients take their medications" +
							"          compared to Medication Possession Ratio (MPR), and how much patients like " +
							"using" +
							" the DayaMed app." +
							"          We think about 200 people will be involved in this study.</p>" +
							"        <h5>Procedures:</h5>" +
							"        <p>If you decide to join, you will be asked to:</p>" +
							"        <p>" +
							"          1. Provide informed consent and obtain signed HIPAA Authorization form (20 " +
							"minutes) <br>" +
							"          2. Complete the first survey with patient characteristics and habits (15 min)" +
							" " +
							"<br>" +
							"          3. Download DayaMed digital health platform to smartphone device (5 min)<br>" +
							"          4. Complete second survey through app, paper, or phone (15 min)<br>" +
							"          5. Complete final survey through app, paper, or phone (15 min)<br>" +
							"        </p>" +
							"        <p>This study will take about six months and include three phases. The three " +
							"phases are first, middle and" +
							"          final, they will include surveys to be completed by participants." +
							"          Risks and Benefits:" +
							"          The study has a small amount of risk, first subjects will be affected if " +
							"privacy is broken. Second, the" +
							"          mental impact of sharing opinions about medications and how well a subject is" +
							" " +
							"taking them." +
							"          While there are no direct benefits to participants, possible benefits to " +
							"participants may include a" +
							"          positive impact on how well they take their medication. We will try and " +
							"minimize the above risks by coding" +
							"          data, and confidential treatment of survey responses, and expect a positive " +
							"benefit-to-risk ratio for" +
							"          participants.</p>" +
							"        <h5>Compensation:</h5>" +
							"        <p>If you participate, you will receive a token for a free delivery for each " +
							"survey completed. With a total of" +
							"          three surveys participants are eligible to receive a total of 3 tokens (3 " +
							"free" +
							" " +
							"deliveries) if all three" +
							"          surveys are completed.</p>" +
							"        <h4>Confidentiality:</h4>" +
							"        <p>• Any information obtained in connection with this" +
							"          research study that can be identified with you will be released only with " +
							"your" +
							" " +
							"permission; your results" +
							"          will be kept confidential. In any written reports or publications, no one " +
							"will" +
							" " +
							"be identified or" +
							"          identifiable and only group data will be presented. Appropriate safeguards " +
							"for" +
							" " +
							"protected health" +
							"          information include: User Access control, tracking and automated alerts, data" +
							" " +
							"security at-rest using" +
							"          encryption of all PHI data, data masking at user role level, data scrubbing " +
							"and" +
							" de-identification when" +
							"          data is used for analysis.</p>" +
							"        <p>• We will keep the research results in an encrypted" +
							"          computer located on campus and at Good Shepherd Pharmacy and only I, Good " +
							"Shepherd pharmacy and my" +
							"          advisor (Kevin Clauson) will have access to the records while we work on this" +
							" " +
							"project. We will finish" +
							"          analyzing the data by June 2019. We will then destroy all original reports " +
							"and" +
							" " +
							"identifying information" +
							"          that can be linked back to you.</p>" +
							"        <h5>Voluntary Participation:</h5>" +
							"        <p> Participation in this research study is voluntary. You are free to stop at " +
							"any time. Your decision whether" +
							"          or not to participate will not affect your current or future relations with " +
							"Good Shepherd Pharmacy or" +
							"          Lipscomb University in any way.</p>" +
							"        <h5>Contacts and Questions:</h5>" +
							"        <p> If you have any questions, please feel free to contact me, Bridget Lynch, " +
							"at" +
							" " +
							"615-966-1000," +
							"          balynch@lipscomb.edu or one of the others researchers, Kevin Clauson, " +
							"615-966-7001," +
							"          kevin.clauson@lipscomb.edu or Elizabeth Breeden, 615-7112, beth" +
							".breeden@lipscomb.edu." +
							"          You may ask questions now or later and I or the other researchers will be " +
							"happy" +
							" to answer them. If you have" +
							"          other questions or concerns regarding the study and would like to talk to " +
							"someone other than the" +
							"          researcher(s), you may also contact Dr. Justin Briggs. Chair of the Lipscomb " +
							"University Institutional" +
							"          Review Board at jgbriggs@lipscomb.edu.</p>" +
							"        <p>You may keep a copy of this form for your records.</p>" +
							"        <h5>Statement of Consent:</h5>" +
							"        <p>You are making a decision whether or not to participate. Your signature " +
							"indicates that you have read this" +
							"          information and your questions have been answered. Even after signing this " +
							"form, please know that you may" +
							"          withdraw from the study at any time.</p>" +
							"        <p> I consent to participate in the study</p>";

			consentDocument.setDocBody(body);
			LocalDateTime rightNow = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
			//LocalDateTime.ofInstant(instant, ZoneOffset.UTC)
			consentDocument.setPublicationDate(rightNow);
			String version = "1";
			ConsentDocument existedConsentDocument =
					consentDocumentRerpository.findFirst1ByOrderByPublicationDateDesc();
			if (existedConsentDocument != null) {
				int intVersion = Integer.parseInt(existedConsentDocument.getVersion());
				version = Integer.toString(intVersion + 1);
			}
			consentDocument.setVersion(version);
			consentDocument = consentDocumentRerpository.save(consentDocument);
			return new ResponseEntity<ConsentDocument>(consentDocument, HttpStatus.OK);
		} catch (NumberFormatException ne) {
			ne.getMessage();
			logger.error("NumbeFormatExc exception while converting string to float value");
			return new ResponseEntity<ConsentDocument>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			e.getMessage();
			return new ResponseEntity<ConsentDocument>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiIgnore
	@GetMapping(value = "/consentdocument/latest", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Patient Id", notes = "Returns Adherence")
	public ResponseEntity<ConsentDocument> fetchlatestConsentDocument() {
		try {
			ConsentDocument consentDocument = consentDocumentRerpository.findFirst1ByOrderByPublicationDateDesc();
			return new ResponseEntity<ConsentDocument>(consentDocument, HttpStatus.OK);
		} catch (Exception e) {
			e.getMessage();
			return new ResponseEntity<ConsentDocument>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//long userid,String version
	@ApiIgnore
	@GetMapping(value = "/userconsent/{userid}/version/{version}", produces = "application/json")
	@ApiOperation(value = "Fetch Adherence By Patient Id", notes = "Returns Adherence")
	public ResponseEntity<UserConsent> fetchuserconsent(@PathVariable(value = "userid") String userid,
	                                                    @PathVariable(value = "version") String version) {
		try {
			UserConsent userConsent = userConsentRepository.findByUserIdAndVersion(Long.parseLong(userid), version);
			return new ResponseEntity<UserConsent>(userConsent, HttpStatus.OK);
		} catch (Exception e) {
			e.getMessage();
			return new ResponseEntity<UserConsent>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
