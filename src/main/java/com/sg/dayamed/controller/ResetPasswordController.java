package com.sg.dayamed.controller;

import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.ApplicationConstants;

import io.swagger.annotations.Api;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pwdupdate")
@Api(value = "Reset Password Operations")
public class ResetPasswordController {

	@Autowired
	UserDetailsService userDetailsService;

	@Value("${dayamed.password.expire.days}")
	int pwdExpireDays;

	private static final Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);

	@PostMapping(value = "/resetpwd", consumes = "application/json")
	public ResponseEntity<String> resetPassword(@RequestBody String inputObj) throws JSONException {
		JSONObject respJson = new JSONObject();
		JSONObject reqJson = new JSONObject(inputObj);
		if (inputObj != null && !inputObj.isEmpty()) {
			String tokenId = reqJson.optString("tokenId");
			String password = reqJson.optString("password");
			//logger.info(password+"<pass==resetPassword==tokenId=>"+tokenId);
			if (tokenId != null && !tokenId.isEmpty() && password != null && !password.isEmpty()) {
				String responseStr = userDetailsService.resetPassword(tokenId, password);
				if (ApplicationConstants.SUCCESS.equals(responseStr)) {
					return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.SUCCESS)
					                                    .put(ApplicationConstants.MESSAGE,
					                                         "Password updated successfully")
					                                    .toString(), HttpStatus.OK);
				} else if (ApplicationConstants.EXPIRED_TOKEN.equals(responseStr)) {
					return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.FAILED)
					                                    .put(ApplicationConstants.MESSAGE,
					                                         ApplicationConstants.EXPIRED_TOKEN)
					                                    .toString(), HttpStatus.OK);
				} else if (ApplicationConstants.INVALID_TOKEN.equals(responseStr)) {
					return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.FAILED)
					                                    .put(ApplicationConstants.MESSAGE,
					                                         ApplicationConstants.INVALID_TOKEN)
					                                    .toString(), HttpStatus.OK);
				} else {
					return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.FAILED)
					                                    .put(ApplicationConstants.MESSAGE,
					                                         ApplicationConstants.INVALID_JSON)
					                                    .toString(), HttpStatus.OK);
				}
			} else {
				//logger.error("<=password=Invalid details tokenId=="+tokenId);
				return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.FAILED)
				                                    .put(ApplicationConstants.MESSAGE,
				                                         ApplicationConstants.INVALID_JSON)
				                                    .toString(), HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.FAILED)
			                                    .put(ApplicationConstants.MESSAGE, ApplicationConstants.INVALID_JSON)
			                                    .toString(), HttpStatus.OK);
		}
	}

	@PostMapping(value = "/forgotpwd", consumes = "application/json")
	public ResponseEntity<String> forgotPassword(@RequestBody String inputObj) throws JSONException {
		JSONObject respJson = new JSONObject();
		//logger.info("<=forgotPassword==inputObj==>"+inputObj);
		if (inputObj != null && !inputObj.isEmpty()) {
			JSONObject reqJson = new JSONObject(inputObj);
			String emailId = reqJson.getString("emailId");
			if (emailId != null && !emailId.isEmpty()) {
				String status = userDetailsService.forgotPassword(emailId);
				if (ApplicationConstants.SUCCESS.equals(status)) {
					return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.SUCCESS)
					                                    .put(ApplicationConstants.MESSAGE,
					                                         "Password reset mail sent successfully.")
					                                    .toString(), HttpStatus.OK);
				} else {
					return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.FAILED)
					                                    .put(ApplicationConstants.MESSAGE,
					                                         ApplicationConstants.INVALID_EMAILID)
					                                    .toString(), HttpStatus.OK);
				}
			}
			//logger.info("<=forgotPassword==emailId==>"+emailId);
		}
		return new ResponseEntity<>(respJson.put(ApplicationConstants.STATUS, ApplicationConstants.FAILED)
		                                    .put(ApplicationConstants.MESSAGE, ApplicationConstants.INVALID_JSON)
		                                    .toString(), HttpStatus.OK);
	}

}
