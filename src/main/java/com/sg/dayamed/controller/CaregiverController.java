package com.sg.dayamed.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.coyote.Request;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.pojo.PatientEmailVO;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.security.jwtsecurity.security.CurrentUser;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/rest/caregivers")
@Api(value = "Caregiver Operations")
public class CaregiverController extends BaseRestApi {

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	Utility utility;
	
	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	DosageNotificationRepository dosageNotificationRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(CaregiverController.class);

	//fetchPatientsByProviderId from Cookie

	@PreAuthorize("hasRole('CAREGIVER')")
	@GetMapping(value = "/patients", produces = "application/json")
	@ApiOperation(value = "Fetch Patient list ByCaregiverId", notes = "Returns patient list")
	public ResponseEntity<Object> fetchPatietnsByCaregiverIdFromJwtToken(
			Authentication authentication) throws RestApiException {

		return inboundServiceCallGeneric(authentication, service -> {
			return ResponseEntity.ok(caregiverService.fetchPatietnsByCaregiverIdFromJwtToken(authentication));
		});
	}

	/**
	 * data which has only patient data , it won't include
	 * patients providers list pharmacists list ,prescriptions list
	 *
	 * @param authentication
	 * @return
	 * @throws RestApiException
	 */
	@PreAuthorize("hasRole('CAREGIVER')")
	@GetMapping(value = "/associated/patients", produces = "application/json")
	@ApiOperation(value = "Fetch Patient list ByCaregiverId", notes = "Returns patient list")
	public ResponseEntity<Object> fetchPatietnsAloneByCaregiver(Authentication authentication) throws RestApiException {
		return inboundServiceCallGeneric(authentication, service -> {
			List<PatientModel> patientModelList = caregiverService.fetchPatietnsAloneByCaregiver(authentication);
			return ResponseEntity.ok(patientModelList);
		});
	}

	//Adding caregiver by provider or Admin from web
	@PreAuthorize("hasAnyRole('PROVIDER','ADMIN','CAREGIVER')")
	@PostMapping(value = "/add", produces = "application/json", consumes = {"application/json", "multipart/form-data",
			"application/octet-stream"})
	@ApiOperation(value = "Add only Caregiver", notes = "Returns Caregiver")
	public ResponseEntity<Object> addCaregiverFromWeb(@RequestPart("caregiverInfo") String strCaregiverInfo,
	                                                  @RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage,
	                                                  @CurrentUser JwtUserDetails jwtUser) throws RestApiException {
		return inboundServiceCallGeneric(strCaregiverInfo, service -> {
			CaregiverModel caregiverModel = caregiverService.addCaregiverFromWebValidate(objectMapper.readValue(strCaregiverInfo, CaregiverModel.class), profileImage, jwtUser);
			return ResponseEntity.ok(caregiverModel);
		});
	}

	// Delete mapping between provider and patinet and delete provider
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public ResponseEntity<Object> deleteCaregiver(@PathVariable(value = "id") Long id) throws RestApiException {
		return inboundServiceCallGeneric(id, service -> {
			caregiverService.deleteCaregiver(id);
			return ResponseEntity.ok(new StringResponse(ApplicationConstants.SUCCESS));
		});
	}

	//caregiver can associated to patient by patient emailID
	@PostMapping(value = "/{id}/mapwithpatient", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Add Caregiver", notes = "Returns Caregiver")
	public ResponseEntity<Object> mapCaregiverwithPatient(@RequestBody PatientEmailVO patientEmailVO,
	                                                      @PathVariable(value = "id") Long caregiverId)
			throws JSONException, DataValidationException, RestApiException {
		//{"patientemailid":"naresh@dayamed.com"}
		return inboundServiceCallGeneric(caregiverId, service -> {
			return ResponseEntity.ok(new StringResponse(
					caregiverService.mapPatientToCaegiver(patientEmailVO.getPatientEmailId(), caregiverId)));
		});
	}

	//register/update caregiver from APP
	@PostMapping(value = "/app/update", produces = "application/json", consumes = {"application/json",
			"multipart/form-data", "application/octet-stream"})
	@ApiOperation(value = "Add Caregiver", notes = "Returns Caregiver")
	public ResponseEntity<Object> updateCaregiverFromApp(
			@RequestPart(value = "caregiverInfo") String strCaregiverInfo,
			@RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage)
			throws DataValidationException, RestApiException {
		return inboundServiceCallGeneric(strCaregiverInfo, service -> {
			CaregiverModel caregiverModel = caregiverService.updateCaregiverFromApp(objectMapper.readValue(strCaregiverInfo, CaregiverModel.class), profileImage);
			return ResponseEntity.ok(caregiverModel);
		});
	}

	// add caregiver By patient from patient login from mobile
	@PreAuthorize("hasRole('PATIENT')")
	@PostMapping(value = "/addtopatient/{patientid}", produces = "application/json", consumes = {"application/json",
			"multipart/form-data", "application/octet-stream"})
	@ApiOperation(value = "Add Caregiver", notes = "Returns Caregiver")
	public ResponseEntity<Object> createCaregiverAssociatedWithPatient(
			@RequestPart(value = "caregiverInfo") String strCaregiverInfo,
			@RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage,
			@PathVariable(value = "patientid") Long patientid) throws DataValidationException, RestApiException {
		return inboundServiceCallGeneric(strCaregiverInfo, service -> {
			Set<CaregiverModel> caregiverModelSet =
					caregiverService.createCaregiverAssociatedwithPatient(objectMapper.readValue(strCaregiverInfo, CaregiverModel.class), patientid, profileImage);
			return ResponseEntity.ok(caregiverModelSet);
		});

	}

	// fetchCaregiverById
	@GetMapping(value = "/{id}", produces = "application/json")
	@ApiOperation(value = "Fetch Caregiver By Id", notes = "Returns Caregiver")
	public ResponseEntity<Object> fetchCaregiverById(@PathVariable(value = "id") Long id)
			throws DataValidationException, RestApiException {
		return inboundServiceCallGeneric(id, service -> {
			Caregiver caregiver = caregiverService.fetchCaregiverById(id);
			return ResponseEntity.ok(caregiver);
		});
	}

	@GetMapping(value = "/byuser/{userid}", produces = "application/json")
	@ApiOperation(value = "Fetch Caregiver By Id", notes = "Returns Caregiver")
	public ResponseEntity<Object> fetchCaregiverByUserId(@PathVariable(value = "userid") Long userid)
			throws DataValidationException, RestApiException {
		return inboundServiceCallGeneric(userid, service -> {
			Caregiver caregiver = caregiverService.fetchCaregiverByUserId(userid);
			return ResponseEntity.ok(caregiver);
		});
	}

	//fetch all caregivers
	@GetMapping(value = "/providerandadmin/{userid}", produces = "application/json")
	@ApiOperation(value = "Fetch Caregivers", notes = "Returns Caregiver list")
	public ResponseEntity<Object> fetchAdminAndSpecificProviderCaregivers(
			@PathVariable(value = "userid") String userid, Authentication authentication) throws RestApiException {
		return inboundServiceCallGeneric(userid, service -> {
			Long userId = StringUtils.isBlank(userid) || "NaN".equalsIgnoreCase(userid) ? 0l : Long.parseLong(userid);
			List<CaregiverModel> caregiverModelList =
					caregiverService.fetchAdminAndSpecificProviderCargivers(userId, authentication);
			return ResponseEntity.ok(caregiverModelList);
		});
	}

	//fetch all caregivers
	@GetMapping(produces = "application/json")
	@ApiOperation(value = "Fetch Caregivers", notes = "Returns Caregiver list")
	public ResponseEntity<Object> fetchCaregivers() throws RestApiException {
		return inboundServiceCallGeneric(new Request(), service -> {
			List<CaregiverModel> caregiverModelList = caregiverService.fetchCaregivers();
			return ResponseEntity.ok(caregiverModelList);
		});
	}

	// fetch all associated patients by caregiverId
	@GetMapping(value = "/{id}/patients", produces = "application/json")
	@ApiOperation(value = "Fetch PatientsByCaregiverId", notes = "Returns PatientsBy CaregiverId ")
	public ResponseEntity<Object> fetchPatientsByCaregiverId(@PathVariable(value = "id") Long cargiverid,
	                                                         HttpServletRequest request,
	                                                         Authentication authentication)
			throws DataValidationException, RestApiException {
		return inboundServiceCallGeneric(cargiverid, service -> {
			List<PatientModel> patientModelList =
					caregiverService.fetchPatientsByCaregiverId(cargiverid, authentication);
			return ResponseEntity.ok(patientModelList);
		});
	}

	// fetch all associated patients prescriptions by caregiverId
	@GetMapping(value = "/{id}/prescriptions", produces = "application/json")
	@ApiOperation(value = "Fetch PrescriptionsByCaregiverId", notes = "Returns PrescriptionsBy CaregiverId ")
	public ResponseEntity<Object> fetchPrescriptionsByCaregiverId(
			@PathVariable(value = "id") Long cargiverid) throws RestApiException {
		return inboundServiceCallGeneric(cargiverid, service -> {
			List<PrescriptionModel> PrescriptionModelList =
					caregiverService.fetchPrescriptionsByCaregiverId(cargiverid);
			return ResponseEntity.ok(PrescriptionModelList);
		});
	}

	@GetMapping(value = "/bypatient/{patientid}", produces = "application/json")
	@ApiOperation(value = "fetch Caregivers By PatientId", notes = "Returns Caregivers List")
	public ResponseEntity<Object> fetchCaregiversByPatientId(
			@PathVariable(value = "patientid") Long patientid) throws RestApiException {
		return inboundServiceCallGeneric(patientid, service -> {
			List<CaregiverModel> caregiverModelList =
					caregiverService.fetchCaregiversByPatientId(patientid);
			return ResponseEntity.ok(caregiverModelList);
		});
	}

}
