package com.sg.dayamed.controller;

import com.sg.dayamed.component.UserDetailsMigrationComponent;
import com.sg.dayamed.controller.ws.ImanticNotificationReqModel;
import com.sg.dayamed.controller.ws.WSUserEncryptionRequest;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingReqModel;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.PaitentAdherenceWeeklyPDFReport;
import com.sg.dayamed.service.TokenNotValidationService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.WeeklyEmailNotificationToPatient;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@Api(value = "Token Not Validation Operations")
public class TokenNotValidationController extends BaseRestApi {

	@Autowired
	Utility utility;

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	TokenNotValidationService tokenNotValidationService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	ObjectMapper objectMapper;

	@PostMapping(value = "/app/register", produces = "application/json", consumes = {"application/json",
			"multipart/form-data", "application/octet-stream"})
	@ApiOperation(value = "Add Caregiver", notes = "Returns Caregiver")
	public ResponseEntity<Object> addCaregiverFromApp(
			@RequestPart(value = "caregiverInfo")  String caregiverInfo,
			@RequestPart(value = "displayPicture", required = false) final MultipartFile profileImage)
			throws RestApiException {
		return inboundServiceCallObjectResponse(caregiverInfo, service -> {
			CaregiverModel caregiverModel = objectMapper.readValue(caregiverInfo, CaregiverModel.class);
			Object caregiverModelSet = caregiverService.addCaregiverFromApp(caregiverModel, profileImage);
			return new ResponseEntity<>(caregiverModelSet, HttpStatus.OK);
		});
	}

	@GetMapping(value = "/images/profilepic/{imgname:.+}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<RestResponse> getprofileImagetest(@PathVariable(value = "imgname") String imgname,
	                                                        HttpServletResponse response) throws RestApiException {
		return inboundServiceCall(imgname, service -> {
			tokenNotValidationService.getDeviceImageOrmedicineImageOrProfileImagetestOrImage(imgname, response);
			return ResponseEntity.ok()
			                     .body(null);
		});
	}

	@GetMapping(value = "/images/ndcimages/{imgname:.+}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<RestResponse> fetchNDCImages(@PathVariable(value = "imgname") String imgname,
	                                                   HttpServletResponse response) throws RestApiException {
		return inboundServiceCall(imgname, service -> {
			tokenNotValidationService.fetchNDCImages(imgname, response);
			return new ResponseEntity<>(HttpStatus.OK);
		});
	}

	@GetMapping(value = "/images/devices/{imgname}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<RestResponse> getDeviceImage(@PathVariable(value = "imgname") String imgname,
	                                                   HttpServletResponse response) throws RestApiException {
		return inboundServiceCall(imgname, service -> {
			tokenNotValidationService.getDeviceImageOrmedicineImageOrProfileImagetestOrImage(imgname, response);
			return ResponseEntity.ok()
			                     .body(null);
		});
	}

	@GetMapping(value = "/images/medicines/{imgname}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<RestResponse> getmedicineImage(@PathVariable(value = "imgname") String imgname,
	                                                     HttpServletResponse response) throws RestApiException {
		return inboundServiceCall(imgname, service -> {
			tokenNotValidationService.getDeviceImageOrmedicineImageOrProfileImagetestOrImage(imgname, response);
			return ResponseEntity.ok()
			                     .body(null);
		});
	}

	@GetMapping(value = "/images/{imgname}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<RestResponse> getImage(@PathVariable(value = "imgname") String imgname,
	                                             HttpServletResponse response) throws RestApiException {
		return inboundServiceCall(imgname, service -> {
			tokenNotValidationService.getDeviceImageOrmedicineImageOrProfileImagetestOrImage(imgname, response);
			return ResponseEntity.ok()
			                     .body(null);
		});
	}

	@PostMapping(value = "/notifications/imantics/add", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Add Imantic Notification", notes = "Returns Imantic Notification")
	public ResponseEntity<RestResponse> addImanticNotification(
			@RequestBody @Valid ImanticNotificationReqModel notificationModel) throws RestApiException {
		return inboundServiceCall(notificationModel, service -> {
			tokenNotValidationService.addImanticNotification(notificationModel);
			return new ResponseEntity<>(HttpStatus.OK);
		});
	}

	@GetMapping(value = "/getpatientattributes", produces = "application/json")
	@ApiOperation(value = "Fetch data for imantics by patientid", notes = "Returns dataforimantics")
	public ResponseEntity<Object> fetchPatientAttributes(@RequestParam("patientId") Optional<String> patientId)
			throws RestApiException {
		return inboundServiceCallObjectResponse(patientId, service -> {
			Object listDataForImantics = tokenNotValidationService.fetchPatientAttributes(patientId);
			return new ResponseEntity<>(listDataForImantics, HttpStatus.OK);
		});
	}

	@PostMapping(value = "/weberrorlogs", produces = "application/json")
	public ResponseEntity<RestResponse> webErrorLogsSave(@RequestBody String inputJson) throws RestApiException {
		return inboundServiceCall(inputJson, service -> {
			RestResponse restResponse = tokenNotValidationService.webErrorLogsSave(inputJson);
			return new ResponseEntity<>(restResponse, HttpStatus.OK);
		});
	}

	@PostMapping(value = "/vucaurlgstoken", produces = "application/json")
	public ResponseEntity<RestResponse> getVUCAUrlbyGSToken(@RequestParam("token") String gsToken)
			throws RestApiException {
		return inboundServiceCall(gsToken, service -> {
			RestResponse restResponse = tokenNotValidationService.getVUCAUrlbyGSToken(gsToken);
			return new ResponseEntity<>(restResponse, HttpStatus.OK);
		});
	}

	@PostMapping(value = "/endmeeting", produces = "application/json")
	@ApiOperation(value = "End meeting")
	public ResponseEntity<Object> endMeeting(HttpServletRequest request,
	                                         @RequestBody ZoomEndMeetingReqModel zoomEndMeetingReqModel)
			throws RestApiException {
		return inboundServiceCallObjectResponse(zoomEndMeetingReqModel, service -> {
			Object statusCode = tokenNotValidationService.endMeeting(zoomEndMeetingReqModel,
			                                                         request.getHeader("authorization"));
			return new ResponseEntity<>(statusCode, HttpStatus.OK);
		});
	}

	@Autowired
	UserDetailsMigrationComponent userDetailsMigrationComponent;
	@PostMapping("/user/details/encryption")
	public ResponseEntity<RestResponse> testEncryption(@RequestBody WSUserEncryptionRequest request) throws Exception {
		userDetailsMigrationComponent.updateUserDetails(request.getDecryptedPath(), request.getEncryptedPath());
		return  null;
	}
	
	@Autowired
	WeeklyEmailNotificationToPatient weeklyEmailNotificationToPatient;

	@GetMapping("/notificationTest")
	public ResponseEntity<String> notificationTest(@RequestParam("emailId") String emailId,@RequestParam("notificationType") String notificationType) throws Exception {
		if (!StringUtils.isEmpty(emailId)) {
			weeklyEmailNotificationToPatient.sendNotificationToPatient(notificationType, emailId);
			return new ResponseEntity<>("Sent mail success", HttpStatus.OK);
		}
		return new ResponseEntity<>("Sent mail Fail", HttpStatus.OK);
	}
	
	@Autowired
	PaitentAdherenceWeeklyPDFReport paitentAdherenceWeeklyPDFReport;

	@GetMapping("/patientWeeklyPdf")
	public ResponseEntity<String> patientWeeklyPdf(@RequestParam("emailId") String emailId) throws Exception {
		if (!StringUtils.isEmpty(emailId)) {
			paitentAdherenceWeeklyPDFReport.getPatientRecoreds(null,  null, emailId);
			return new ResponseEntity<>("Weekly Sent mail success", HttpStatus.OK);
		}
		return new ResponseEntity<>("Weekly PDF Sent mail Fail", HttpStatus.OK);
	}
}
