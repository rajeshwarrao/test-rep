package com.sg.dayamed.controller;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.security.JwtGenerator;
import com.sg.dayamed.security.jwtsecurity.security.SecurityConstants;
import com.sg.dayamed.service.LoginValidationService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.StringResponse;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class TokenController extends BaseRestApi {

	@Autowired
	LoginValidationService loginValidationService;

	private JwtGenerator jwtGenerator;

	@Autowired
	RediseService rediseService;

	public TokenController(JwtGenerator jwtGenerator) {
		this.jwtGenerator = jwtGenerator;
	}

	@PostMapping(value = "/login", produces = "application/json", consumes = "application/json")
	public ResponseEntity<RestResponse> generate(@RequestBody @Valid LoginRequestModel loginRequestModel,
	                                             HttpServletRequest request) throws RestApiException {
		return inboundServiceCall(loginRequestModel, service -> {
			RestResponse userDetailsWithJwtModel = loginValidationService
					.loginValidation(loginRequestModel, request, jwtGenerator);
			return new ResponseEntity<>(userDetailsWithJwtModel, HttpStatus.OK);
		});
	}

	@PostMapping(value = "/user/login", produces = "application/json", consumes = "application/json")
	public ResponseEntity<RestResponse> login(@RequestBody @Valid LoginRequestModel loginRequestModel,
	                                          HttpServletRequest request) throws RestApiException {
		return inboundServiceCall(loginRequestModel, service -> {
			RestResponse userDetailsWithJwtModel = loginValidationService
					.loginValidation(loginRequestModel, request, jwtGenerator);
			return new ResponseEntity<>(userDetailsWithJwtModel, HttpStatus.OK);
		});
	}

	@GetMapping(value = "/rest/logout", produces = "application/json")
	@ApiOperation(value = "logout", notes = "Seesion will be Expire")
	public ResponseEntity<StringResponse> logout(HttpServletRequest request) {
		String jwtTokenKey = request.getHeader(SecurityConstants.HEADER_STRING);
		JwtUser jwtUser = (JwtUser) rediseService.getDataInRedisByKey(jwtTokenKey);
		rediseService.removeTokenFromCache(RedisKeyConstants.userLatestJWTActiveToken + jwtUser.getUserId());
		rediseService.removeTokenFromCache(jwtTokenKey);
		return new ResponseEntity<>(new StringResponse("Successfully loggedout"), HttpStatus.OK);
	}
}
