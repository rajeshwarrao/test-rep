package com.sg.dayamed.controller;

import com.sg.dayamed.entity.DeviceStatus;
import com.sg.dayamed.pojo.DeviceStatusModel;
import com.sg.dayamed.pojo.DeviceStatusRequestModel;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.DeviceStatusService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.Utility;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(value = "Device Operations")
public class DeviceController extends BaseRestApi {

	@Autowired
	NotificationService notificationService;

	@Autowired
	Utility utility;

	@Autowired
	DeviceStatusService deviceStatusService;

	@GetMapping(value = "/rest/utility/getdevicestatus/{userid}", produces = "application/json")
	public ResponseEntity<DeviceStatusModel> getDeviceStatus(@PathVariable(value = "userid") Long userId) throws Exception {
		notificationService.sendMobileNotification(userId, ApplicationConstants.DEVICE_STATUS, ApplicationConstants.DEVICE_STATUS);
		Thread.sleep(1000);
		return new ResponseEntity<DeviceStatusModel>(utility.convertDosageDeviceEntityToModel(deviceStatusService.fetchDeviceStatusByUserID(userId)), HttpStatus.OK);
	}

	@PostMapping(value = "/rest/utility/adddevicestatus", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "add device status", notes = "Returns deviceStatus")
	public ResponseEntity<Object> addDeviceStatus(@RequestBody DeviceStatusRequestModel deviceStatusRequestModel) throws RestApiException {
		return inboundServiceCallGeneric(deviceStatusRequestModel, service -> {
			DeviceStatus deviceStatus = deviceStatusService.addDeviceStatus(deviceStatusRequestModel);
			return new ResponseEntity<Object>(deviceStatus ,HttpStatus.OK);
		});
	}

}
