package com.sg.dayamed.component;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

/**
 * Created By Gorantla, Eresh on 18/Sep/2019
 **/
@Component
public class AmazonClient {

	private AmazonS3 s3client;

	@Value("${s3.endpoint.url}")
	private String endpointUrl;

	@Value("${s3.profile.bucket.name}")
	private String bucketName;

	@Value("${s3.access.key}")
	private String accessKey;

	@Value("${s3.secret.key}")
	private String secretKey;

	@PostConstruct
	private void initializeAmazon() {
		AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
		this.s3client = new AmazonS3Client(credentials);
	}

	public String uploadFile(MultipartFile multipartFile) throws ApplicationException {
		String fileUrl;
		try {
			if (!(MediaType.IMAGE_JPEG_VALUE.equalsIgnoreCase(multipartFile.getContentType()) ||
					MediaType.IMAGE_GIF_VALUE.equalsIgnoreCase(multipartFile.getContentType()) ||
					MediaType.IMAGE_PNG_VALUE.equalsIgnoreCase(multipartFile.getContentType()))) {
				throw new DataValidationException(APIErrorFields.FILE_UPLOAD, APIErrorKeys.ERROR_AWS_UPLOAD_INVALID_FILE_FORMAT,
				                                  new Object[]{"jgp, jpeg, png, gif"});
			}
			File file = convertMultiPartToFile(multipartFile);
			String fileName = generateFileName(multipartFile);
			fileUrl = endpointUrl + "/" + fileName;
			uploadFileTos3bucket(fileName, file);
			Files.delete(Paths.get(file.getPath()));
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
		return fileUrl;
	}

	private File convertMultiPartToFile(MultipartFile file) throws IOException {
		File convertToFile = new File(file.getOriginalFilename());
		try (FileOutputStream fos = new FileOutputStream(convertToFile)) {
			fos.write(file.getBytes());
		}

		return convertToFile;
	}

	private String generateFileName(MultipartFile multiPart) {
		return new Date().getTime() + "-" + multiPart.getOriginalFilename()
		                                             .replace(" ", "_");
	}

	private void uploadFileTos3bucket(String fileName, File file) {
		s3client.putObject(new PutObjectRequest(bucketName, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
	}

	public String deleteFileFromS3Bucket(String fileUrl) {
		String fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
		s3client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
		return "Successfully deleted";
	}

}