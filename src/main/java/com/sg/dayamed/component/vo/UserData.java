package com.sg.dayamed.component.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Gorantla, Eresh on 29/Aug/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserData {
	private Long id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String emailId;
	private String mobileNumber;
	private String location;
	private String zipCode;
	private String city;
	private String country;
	private String state;
}
