package com.sg.dayamed.component.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created By Gorantla, Eresh on 29/Aug/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserDataResponse {
	private List<UserData> users;
}
