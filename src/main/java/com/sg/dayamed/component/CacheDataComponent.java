package com.sg.dayamed.component;

import com.sg.dayamed.dao.UserDetailsCacheVO;
import com.sg.dayamed.dao.UserDetailsDao;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created By Gorantla, Eresh on 19/Sep/2019
 **/
@Component
public class CacheDataComponent {

	@Autowired
	UserDetailsDao userDetailsDao;

	public UserDetailsCacheVO validateUserDetails(Long userId) {
		List<UserDetailsCacheVO> userDetailsCacheVOS = userDetailsDao.loadUserCacheDetails();
		return userDetailsCacheVOS.stream()
		                          .filter(user -> user.getId() != null)
		                          .filter(user -> user.getId()
		                                              .equals(userId))
		                          .findFirst()
		                          .orElse(null);
	}

}
