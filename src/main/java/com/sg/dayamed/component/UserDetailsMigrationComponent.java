package com.sg.dayamed.component;

import com.sg.dayamed.component.vo.UserData;
import com.sg.dayamed.component.vo.UserDataResponse;
import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;
import com.sg.dayamed.entity.Address;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.UserDetailsRepository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By Gorantla, Eresh on 29/Aug/2019
 **/
@Component
public class UserDetailsMigrationComponent {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Autowired
	PHIDataEncryption phiDataEncryption;

	@Autowired
	ObjectMapper objectMapper;

	private String keyText = "1234567890ABCD";

	public void updateUserDetails(String decryptedPath, String encryptedPath) throws ApplicationException {
		try {
			List<UserDetails> userDetails = userDetailsRepository.findAll();
			generateDecryptedDetails(encryptedPath, decryptedPath);
			updateDecryptedDetails(decryptedPath, userDetails);
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	private void generateDecryptedDetails(String encryptedPath, String decryptedPath) throws ApplicationException {

		List<UserData> userDataS = new ArrayList<>();
		UserDataResponse userDataResponse = new UserDataResponse();
		try (
				Reader reader = Files.newBufferedReader(Paths.get(encryptedPath));
				CSVReader csvReader = new CSVReader(reader);
		) {
			SecretKey secretKey = phiDataEncryption.getSecretEncryptionKey(keyText);
			String[] nextRecord;
			while ((nextRecord = csvReader.readNext()) != null) {
				UserData userData = new UserData();
				userData.setId(NumberUtils.toLong(nextRecord[0]));
				userData.setFirstName(getDecryptedText(nextRecord[1], secretKey));
				userData.setMiddleName(getDecryptedText(nextRecord[2], secretKey));
				userData.setLastName(getDecryptedText(nextRecord[3], secretKey));
				userData.setEmailId(getDecryptedText(nextRecord[4], secretKey));
				userData.setMobileNumber(getDecryptedText(nextRecord[5], secretKey));
				userData.setLocation(getDecryptedText(nextRecord[6], secretKey));
				userData.setCity(getDecryptedText(nextRecord[7], secretKey));
				userData.setState(getDecryptedText(nextRecord[8], secretKey));
				userData.setCountry(getDecryptedText(nextRecord[9], secretKey));
				userData.setZipCode(getDecryptedText(nextRecord[10], secretKey));
				userDataS.add(userData);
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
		userDataResponse.setUsers(userDataS);
		try {
			File file = Paths.get(decryptedPath).toFile();
			if (!file.exists()) {
				Boolean createFlag = file.createNewFile();
				logger.debug("New File Created", createFlag);
			}
			objectMapper.writerWithDefaultPrettyPrinter()
			            .writeValue(file, userDataResponse);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void updateDecryptedDetails(String decryptedPath, List<UserDetails> userDetails) throws Exception {
		File file = ResourceUtils.getFile(decryptedPath);
		UserDataResponse response = objectMapper.readValue(file, UserDataResponse.class);
		List<UserData> userDataList = response.getUsers();
		userDetails.stream()
		           .forEach(user -> {
			           UserData userData = userDataList.stream()
			                                           .filter(data -> data.getId()
			                                                               .equals(user.getId()))
			                                           .findFirst()
			                                           .orElse(null);
			           if (userData != null) {
				           user.setEmailId(userData.getEmailId());
				           user.setFirstName(userData.getFirstName());
				           user.setLastName(userData.getLastName());
				           user.setMobileNumber(userData.getMobileNumber());
				           user.setMiddleName(userData.getMiddleName());

				           Address address = new Address();
				           address.setState(userData.getState());
				           address.setZipCode(userData.getZipCode());
				           address.setLocation(userData.getLocation());
				           address.setCountry(userData.getCountry());
				           address.setCity(userData.getCity());
				           user.setAddress(address);
			           }
		           });
		userDetailsRepository.saveAll(userDetails);
	}

	private String getDecryptedText(String text, SecretKey secretKey) {
		try {
			return phiDataEncryption.decryptText(text, secretKey);
		} catch (Exception e) {
			try {
				return phiDataEncryption.decryptText(text, secretKey);
			} catch (Exception e1) {
				return text;
			}
		}
	}
}
