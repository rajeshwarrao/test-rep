package com.sg.dayamed.component;

import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Created By Gorantla, Eresh on 26/Jun/2019
 **/
@Component
public class RoleCustomAuthorization {

	public Boolean isAdmin(Authentication authentication) {
		JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();
		return UserRoleEnum.ADMIN.getRole()
		                         .equalsIgnoreCase(jwtUser.getRole());
	}
}
