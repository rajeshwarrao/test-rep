package com.sg.dayamed.pdfhelper;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class PatientPdfPageEventHelper extends PdfPageEventHelper {

	private Map<String, String> patientDetails;

	private String imagePath;

	private String footerLinks;

	public PatientPdfPageEventHelper(Map<String, String> patientDetails, String imagePath, String footerLinks) {
		this.patientDetails = patientDetails;
		this.imagePath = imagePath;
		this.footerLinks = footerLinks;
	}

	public void onStartPage(PdfWriter pdfWriter, Document document) {
		try {
			Image img = Image.getInstance(imagePath);
			img.scaleToFit(80, 80);
			img.setAbsolutePosition(35, 755);
			pdfWriter.getDirectContent()
			         .addImage(img);
			PdfPTable myTable = new PdfPTable(1);
			myTable.setWidthPercentage(100);

			PdfPCell cellOne = new PdfPCell(new Paragraph(""));
			cellOne.setBorder(Rectangle.BOTTOM);
			cellOne.setBorderColor(new BaseColor(44, 67, 144));
			cellOne.setBorderWidth(2f);
			myTable.addCell(cellOne);
			document.add(myTable);
			document.add(new Paragraph(" "));
		} catch (Exception x) {
			log.error("Exception occurred writing into PDF", x);
		}
	}

	public void onEndPage(PdfWriter pdfWriter, Document document) {
	}

}
