package com.sg.dayamed.pdfhelper;

import com.sg.dayamed.util.ApplicationConstants;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.FontSelector;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Slf4j
public class PatientPdfBody {

	public void patientPdfBody(ResultSet rs, Document document) throws IOException, DocumentException, SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		PdfPTable table = new PdfPTable(columns);
		table.setWidthPercentage(100);
		table.setHeaderRows(1);
		PdfPCell cell;
		BaseFont bf = BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, BaseFont.EMBEDDED);
		Font font = new Font(bf, 12);
		font.setColor(BaseColor.BLUE);
		for (int x = 1; x <= columns; x++) {
			String columName = rsmd.getColumnLabel(x);
			if ("Prescribed_Time".equalsIgnoreCase(columName)) {
				columName = columName + "(UTC)";
			}
			columName = columName != null ? columName.replace("_", " ") : columName;
			cell = new PdfPCell(new Phrase(columName, font));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorder(PdfPCell.BOX);
			cell.setFixedHeight(27);
			table.addCell(cell);
		}

		int bodyFontSize = 11;

		FontSelector selector = new FontSelector();
		Font blueFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
		blueFont.setColor(BaseColor.BLUE);
		Font redFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
		redFont.setColor(BaseColor.RED);
		Font greenFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
		greenFont.setColor(BaseColor.GREEN);
		Font blackFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
		blackFont.setColor(BaseColor.BLACK);

		while (rs.next()) {
			for (int x = 1; x <= columns; x++) {
				String columName = rsmd.getColumnLabel(x);
				String columValue = StringUtils.isEmpty(rs.getString(columName)) ? "" : rs.getString(columName);
				if (ApplicationConstants.CONSUMPTION_STATUS_AUTODELAY.equalsIgnoreCase(columValue)
						|| ApplicationConstants.CONSUMPTION_STATUS_DELAY.equalsIgnoreCase(columValue)) {
					selector = new FontSelector();
					selector.addFont(blueFont);
					cell = new PdfPCell(selector.process(columValue.toUpperCase()));
				} else if (ApplicationConstants.CONSUMPTION_STATUS_SKIPPED.equalsIgnoreCase(columValue)
						|| ApplicationConstants.CONSUMPTION_STATUS_NOACTION.equalsIgnoreCase(columValue)) {
					selector = new FontSelector();
					selector.addFont(redFont);
					cell = new PdfPCell(selector.process(columValue.toUpperCase()));
				} else if (ApplicationConstants.CONSUMPTION_STATUS_CONSUMED.equalsIgnoreCase(columValue)) {
					selector = new FontSelector();
					selector.addFont(greenFont);
					cell = new PdfPCell(selector.process(columValue.toUpperCase()));
				} else {
					selector = new FontSelector();
					selector.addFont(blackFont);
					cell = new PdfPCell();
					// Phrase phr = selector.process(columValue);
					// Paragraph phrg = new Paragraph(columValue, blackFont);
					// cell.setCellEvent(new
					// PatientPdfBody.CenterContent(phrg));
					cell = new PdfPCell(selector.process(columValue));
				}
				// cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				// cell.setBorder(PdfPCell.BOX);
				cell.setFixedHeight(22);
				table.addCell(cell);
			}
		}
		document.add(table);
	}

	public void patientPdfBody(List<Map<String, String>> rs, Document document)
			throws IOException, DocumentException, SQLException {
		if (rs != null && rs.size() > 0) {
			Map<String, String> headers = rs.get(0);
			PdfPTable table = new PdfPTable(headers.size());
			table.setWidthPercentage(100);
			table.setHeaderRows(1);
			BaseFont bf = BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, BaseFont.EMBEDDED);
			Font font = new Font(bf, 12);
			font.setColor(BaseColor.BLUE);
			headers.forEach((columName, v) -> {
				PdfPCell cell = null;
				if ("Prescribed_Time".equalsIgnoreCase(columName)) {
					columName = columName + " (UTC)";
				}
				columName = columName != null ? columName.replace("_", " ") : columName;
				cell = new PdfPCell(new Phrase(columName, font));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
				cell.setBorder(PdfPCell.BOX);
				cell.setFixedHeight(27);
				table.addCell(cell);
			});
			int bodyFontSize = 11;
			// FontSelector selector = new FontSelector();
			Font blueFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
			blueFont.setColor(BaseColor.BLUE);
			Font redFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
			redFont.setColor(BaseColor.RED);
			Font greenFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
			greenFont.setColor(BaseColor.GREEN);
			Font blackFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, bodyFontSize);
			blackFont.setColor(BaseColor.BLACK);

			rs.forEach(map -> {
				map.forEach((columName, columValue) -> {
					PdfPCell cellIn = null;
					columValue = StringUtils.isEmpty(columValue) ? " " : columValue;
					if ("Mobile_Number".equalsIgnoreCase(columName) || "Email".equalsIgnoreCase(columName) ||
							"Country".equalsIgnoreCase(columName) || "ZipCode".equalsIgnoreCase(columName) ||
							"Name".equalsIgnoreCase(columName)) {
						if ("Name".equalsIgnoreCase(columName)) {
							StringBuffer strName = new StringBuffer();
							for (String strNameVal : columValue.split(" ")) {
								strName.append(strNameVal);
							}
							columValue = strName.toString();
						}
					}
					FontSelector selector = null;
					columValue = columValue.replace("_", " ");
					if (ApplicationConstants.CONSUMPTION_STATUS_AUTODELAY.equalsIgnoreCase(columValue)
							|| ApplicationConstants.CONSUMPTION_STATUS_DELAY.equalsIgnoreCase(columValue)) {
						selector = new FontSelector();
						selector.addFont(blueFont);
						cellIn = new PdfPCell(selector.process(columValue.toUpperCase()));
					} else if (ApplicationConstants.CONSUMPTION_STATUS_SKIPPED.equalsIgnoreCase(columValue)
							|| ApplicationConstants.CONSUMPTION_STATUS_NOACTION.equalsIgnoreCase(columValue)) {
						selector = new FontSelector();
						selector.addFont(redFont);
						cellIn = new PdfPCell(selector.process(columValue.toUpperCase()));
					} else if (ApplicationConstants.CONSUMPTION_STATUS_CONSUMED.equalsIgnoreCase(columValue)) {
						selector = new FontSelector();
						selector.addFont(greenFont);
						cellIn = new PdfPCell(selector.process(columValue.toUpperCase()));
					} else {
						selector = new FontSelector();
						selector.addFont(blackFont);
						cellIn = new PdfPCell();
						cellIn = new PdfPCell(selector.process(columValue));
					}
					//System.out.println(selector+"=="+blackFont+"blackFont======columValue==="+columValue);
					cellIn.setFixedHeight(22);
					table.addCell(cellIn);
				});
			});
			document.add(table);
		}
	}

	public class CenterContent implements PdfPCellEvent {

		protected Paragraph content;

		public CenterContent(Paragraph content) {
			this.content = content;
		}

		public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
			try {
				PdfContentByte canvas = canvases[PdfPTable.TEXTCANVAS];
				ColumnText ct = new ColumnText(canvas);
				ct.setSimpleColumn(0, 0, position.getWidth(), -1000);
				ct.addElement(content);
				ct.go(true);
				float spaceneeded = 0 - ct.getYLine();
				log.debug("The content requires {} pt whereas the height is {} pt.", spaceneeded,
				          position.getHeight());
				float offset = (position.getHeight() - spaceneeded) / 2;
				log.debug("The difference is {} pt; we'll need an offset of {} pt.", -2f * offset, offset);
				PdfTemplate tmp = canvas.createTemplate(position.getWidth(), position.getHeight());
				ct = new ColumnText(tmp);
				ct.setSimpleColumn(0, offset, position.getWidth(), offset + spaceneeded);
				ct.addElement(content);
				ct.go();
				canvas.addTemplate(tmp, position.getLeft(), position.getBottom());
			} catch (DocumentException e) {
				throw new ExceptionConverter(e);
			}
		}
	}
}
