package com.sg.dayamed.redis;

import redis.clients.jedis.JedisPoolConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.net.UnknownHostException;
import java.time.Duration;

@Configuration
public class RedisDataConfig {

	private static final Logger logger = LoggerFactory.getLogger(RedisDataConfig.class);

	@Value("${dayamed.cache.redis.host}")
	String strRedisHost;

	@Value("${dayamed.cache.redis.port}")
	int strRedisPort;

	@Value("${dayamed.cache.redis.password}")
	String strRedisPassword;

	@Bean
	public RedisConnectionFactory redisConnectionFactory() throws UnknownHostException {
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(250);
		poolConfig.setMinIdle(15);
		poolConfig.setMaxIdle(40);
		poolConfig.setJmxEnabled(true);
		poolConfig.setJmxNamePrefix("Dayamed-Jedis");
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
		redisStandaloneConfiguration.setHostName(strRedisHost);
		redisStandaloneConfiguration.setPort(strRedisPort);
		redisStandaloneConfiguration.setPassword(RedisPassword.of(strRedisPassword));

		JedisClientConfiguration.JedisClientConfigurationBuilder jedisClientConfiguration = JedisClientConfiguration.builder();
		jedisClientConfiguration.connectTimeout(Duration.ofSeconds(60));// 60s connection timeout
		jedisClientConfiguration.usePooling().poolConfig(poolConfig).build();

		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory(redisStandaloneConfiguration,
		                                                                    jedisClientConfiguration.build());

		return jedisConFactory;
	}

	@Bean(name = "redisUserTemplate")
	public RedisTemplate<String, Object> redisTemplateUser(RedisConnectionFactory connectionFactory) {
		RedisTemplate<String, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(connectionFactory);
		template.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
		template.setKeySerializer(new StringRedisSerializer());
		template.setHashKeySerializer(new GenericJackson2JsonRedisSerializer());
		template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		return template;
	}
}
