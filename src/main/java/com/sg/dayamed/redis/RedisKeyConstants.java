package com.sg.dayamed.redis;

public class RedisKeyConstants {

	public static final String patientDetailsQueryForWeeklyReoprt = "dayamed.query.patinet.weeklyreport.userdetails";

	public static final String patientDetailsQueryForRecipienEamils =
			"dayamed.query.patinet.weekly.report.recipientemails";

	public static final String patientAdherenceDataQueryForWeeklyReport =
			"dayamed.query.patinet.weekly.report.adherencedata";

	public static final String patientAdherenceDataForWeeklyReportImagePath = "dayamed.weekly.report.imagepath";

	public static final String patientAdherenceDataForWeeklyReportFooterLinks = "dayamed.weekly.report.footerlinks";

	public static final String patientNoOfDaysAdherenceDataForWeeklyReportFooter = "dayamed.weekly.report.noofdays";

	public static final String emailHost = "dayamed.email.host";

	public static final String emailPort = "dayamed.email.port";

	public static final String emailUsername = "dayamed.email.sender.username";

	public static final String emailCredentials = "dayamed.email.sender.password";

	public static final String weeklyReportEmailContent = "dayamed.weekly.report.email.content";

	public static final String emailSender = "dayamed.weekly.report.email.sender";

	public static final String DayamedAlertProtocalNotification =
			"dayamed.alert.protocal.userdetails.qry";

	public static final String DayamedAlertProtocalNotificationRecepientEmailqry =
			"dayamed.alert.protocal.recepient.email.qry";

	public static final String DayamedAlertProtocalNotificationRecepientEmailBody =
			"dayamed.alert.protocal.email.body";

	public static final String DayamedAlertProtocalNotificationRecepientSMSBody =
			"dayamed.alert.protocal.sms.body";

	public static final String DayamedAlertProtocalNotificationRecepientSMSEnabled =
			"dayamed.alert.protocal.sms.enable";

	public static final String DayamedAlertProtocalNotifAdharenceUpdatestatsQry =
			"dayamed.alert.protocal.adharance.status.update.qry";

	public static final String videoCallRecevierNotificationKey = "dayamed.videocall.recevier.userid.";

	public static final String userLatestJWTActiveToken = "dayamed.active.jwttoken.userid.";

	public static final String vucavediotoken = "dayamed.vuca.vedio.token";

	public static final String vucavediopartnerkey = "dayamed.vuca.vedio.partnerkey";

	public static final String MEDS_ONCUE_BASE_URL = "dayamed.meds.oncue.baseurl";

	public static final String MEDS_ONCUE_WIDGET_URL = "dayamed.meds.oncue.widgeturl";

	public static final String dbencryptionenable = "dayamed.db.encryption.enable";

	public static final String dbencryptionFeilds = "dayamed.db.encryption.fileds";

	public static final String vucapharmacyID = "dayamed.vuca.pharmacyid";

	public static final String USERCONSENT_ENABLE = "dayamed.demo.userconsent.enable";

	public static final String WEB_ERRORLOG_FILEPATH = "dayamed.web.errorlog.filepath";

	public static final String WEEKLY_EMAIL_TO_PATIENT_USERDETAILS_QRY =
			"dayamed.weekly.email.patient.userdetails.qury";

	public static final String DAILY_EMAIL_TO_PATIENT_USERDETAILS_QRY = "dayamed.daily.email.patient.medications.qury";

	public static final String WEEKLY_EMAIL_TO_PATIENT_MEDICATION_QRY = "dayamed.weekly.email.patient.medications" +
			".qury";

	public static final String WEEKLY_EMAIL_TO_PATIENT_MEDICATION_NOOF_LASTDAYS =
			"dayamed.weekly.email.patient.medications.noof.days";

	public static final String WEEKLY_EMAIL_TO_PATIENT_MEDICATION_EMAIL_CONTENT =
			"dayamed.weekly.email.patient.medications.email.content";

	public static final String WEEKLY_EMAIL_TO_PATIENT_MEDICATION_APPURL =
			"dayamed.weekly.email.patient.medications.app.url";

	public static final String DAILY_EMAIL_TO_PATIENT_MEDICATION_EMAIL_CONTENT =
			"dayamed.daily.email.patient.medications.email.content";

	public static final String ENABLE_SEND_EMAIL_NOTIFICATION_FOR_PRESCRIPTION_CREATION_UPDATION =
			"dayamed.notification.pres.creation.updation.enable";

	public static final String CONSUMPTION_FREQUENCY_JSON_FOR_UI = "dayamed.consumption.frequency.json";

	public static final String CONSUMPTION_FREQUENCY_LIST_JSON_FOR_UI = "dayamed.frequencylist.json";

	public static final String FIRST_CONSUMPTION_JSON_FOR_UI = "dayamed.first.consumption.json";

	public static final String GS_GLOBAL_SMS_ENABLE = "dayamed.global.sms.enable";

	public static final String ZOOM_NOTIFICATION_TOKEN = "dayamed.zoom.notification.token";

	public static final String VELOCITY_EMAIL_TEMPLATE = "dayamed.dosage.custom.notification.template";
	
	public static final String MISSED_MEDICATION_VELOCITY_EMAIL_TEMPLATE = "dayamed.missed.medication.notification.template";
}
