package com.sg.dayamed.encryption.passwordencryption.impl;

import com.sg.dayamed.controller.PatientController;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

@Component
public class PasswordSHA256Encryption implements PasswordEncryption {

	private static final Logger logger = LoggerFactory.getLogger(PatientController.class);

	@Override
	public String encrypt(String strToEncrypt) {
		// TODO Auto-generated method stub
		if (strToEncrypt != null) {
			String encrptPWD = msgDigest(strToEncrypt);
			if (encrptPWD != null && !encrptPWD.isEmpty()) {
				return encrptPWD;
			}
		}
		return null;
	}

	public String msgDigest(String strPwd) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(strPwd.getBytes(StandardCharsets.UTF_8));
			return bytesToHex(encodedhash);
		} catch (Exception e) {
			logger.error("Exception occurred when creating message digest", e);
		}
		return null;
	}

	private static String bytesToHex(byte[] hash) {
		StringBuffer hexString = new StringBuffer();
		// bytes to hex
		if (hash != null) {
			for (byte b : hash) {
				hexString.append(String.format("%02x", b));
			}
		}
		return hexString.toString();
	}

}
