package com.sg.dayamed.encryption.passwordencryption;

public interface PasswordEncryption {

	String encrypt(String strToEncrypt);
}
