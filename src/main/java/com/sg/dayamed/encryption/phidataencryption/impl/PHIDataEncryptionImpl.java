package com.sg.dayamed.encryption.phidataencryption.impl;

import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;
import com.sg.dayamed.util.ApplicationConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.Arrays;

@Component
public class PHIDataEncryptionImpl implements PHIDataEncryption {

	private static final Logger logger = LoggerFactory.getLogger(PHIDataEncryptionImpl.class);

	/**
	 * Encrypts plainText in AES using the secret key
	 *
	 * @param plainText
	 * @param secKey
	 * @return
	 * @throws Exception
	 */
	@Override
	public String encryptText(String plainText, SecretKey secKey) throws Exception {
		try {
			Cipher aesCipher = Cipher.getInstance(ApplicationConstants.ENCRYPTION_ALGORITHM_TYPE);
			aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
			byte[] byteCipherText = aesCipher.doFinal(plainText.getBytes());
			return DatatypeConverter.printHexBinary(byteCipherText);
		} catch (Exception e) {
			logger.error("Exception occurred in encryptText", e);
			return plainText;
		}
	}

	/**
	 * Decrypts encrypted byte array using the key used for encryption.
	 *
	 * @param byteCipherText
	 * @param secKey
	 * @return
	 * @throws Exception
	 */
	@Override
	public String decryptText(byte[] byteCipherText, SecretKey secKey) throws Exception {
		// AES defaults to AES/ECB/PKCS5Padding in Java 7
		Cipher aesCipher = Cipher.getInstance(ApplicationConstants.ENCRYPTION_ALGORITHM_TYPE);
		aesCipher.init(Cipher.DECRYPT_MODE, secKey);
		byte[] bytePlainText = aesCipher.doFinal(byteCipherText);
		return new String(bytePlainText);
	}

	/**
	 * Decrypts encrypted string using the key used for encryption.
	 *
	 * @param byteCipherText
	 * @param secKey
	 * @return
	 * @throws Exception
	 */
	@Override
	public String decryptText(String strCipherText, SecretKey secKey) throws Exception {
		byte[] byteCipherText = DatatypeConverter.parseHexBinary(strCipherText);
		Cipher aesCipher = Cipher.getInstance(ApplicationConstants.ENCRYPTION_ALGORITHM_TYPE);
		aesCipher.init(Cipher.DECRYPT_MODE, secKey);
		byte[] bytePlainText = aesCipher.doFinal(byteCipherText);
		return new String(bytePlainText);
	}

	/**
	 * gets the AES encryption key. In your actual programs, this should be
	 * safely stored.
	 *
	 * @return
	 * @throws Exception
	 */
	@Override
	public SecretKey getSecretEncryptionKey(String keyText) throws Exception {
		if (keyText != null && !keyText.trim()
		                               .equals("")) {
			SecretKeySpec keySpec = new SecretKeySpec(getKey(keyText), ApplicationConstants.ENCRYPTION_ALGORITHM_TYPE);
			return keySpec;
		} else {
			logger.error("Exception occurred when generating secret key for {}", keyText);
			return null;
		}
	}

	public byte[] getKey(String keyStr) {
		byte[] key = null;
		try {
			key = (keyStr).getBytes(ApplicationConstants.ENCRYPTION_BYTE_FORMAT);
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
		} catch (Exception e) {
			logger.error("Exception occurred when getting the key {}", keyStr, e);
		}
		return key;
	}

}
