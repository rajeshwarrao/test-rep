package com.sg.dayamed.encryption.phidataencryption;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserDetailsModel;

import java.util.List;

public interface PHIDataDBEncryption {

	public List<PatientModel> encryptPatientList(List<PatientModel> listPatinetModel, String keyText);

	public PatientModel encryptPatient(PatientModel patientModel, String keyText);

	public PatientModel decryptPatient(PatientModel patientModel, String keyText);

	public UserDetailsModel encryptUserDetails(UserDetailsModel userDetailsModel, String keyText);

	public UserDetailsModel decryptUserDetails(UserDetailsModel userDetailsModel, String keyText);

	public String getDBEncryKey();

	public String filedLevelEncryption(String keyText, String encryptText, String role);

	public String filedLevelDecryption(String keyText, String encryptText, String role);

	public UserDetails encryptUserDetailsEntitiy(UserDetails userDetails, String keyText);

	public UserDetails decryptUserDetailsEntity(UserDetails userDetails, String keyText);
}
