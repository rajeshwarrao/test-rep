package com.sg.dayamed.encryption.phidataencryption.impl;

import com.sg.dayamed.encryption.phidataencryption.PHIDataDBEncryption;
import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.Utility;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service("PHIDataDBEncryption")
public class PHIDataDBEncryptionImpl implements PHIDataDBEncryption {

	@Value("${dayamed.phidata.db.encrypt.key}")
	String strDBEncryptionKey;

	@Autowired
	RediseService rediseService;

	@Autowired
	PHIDataEncryptionService pHIDataEncryptionService;

	@Autowired
	Utility utility;
	
	@Autowired
	PHIDataEncryption pHIDataEncryption;

	public List<PatientModel> encryptPatientList(List<PatientModel> listPatinetModel, String keyText) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable)) {
			keyText = keyText != null ? keyText.trim() : keyText;
			List<PatientModel> encrypPatientList = null;
			if (listPatinetModel != null && listPatinetModel.size() > 0) {
				encrypPatientList = new ArrayList<PatientModel>();
				for (PatientModel patientModel : listPatinetModel) {
					encrypPatientList.add(encryptPatient(patientModel, keyText));
				}
			}
			return encrypPatientList;
		}
		return listPatinetModel;
	}

	public PatientModel encryptPatient(PatientModel patientModel, String keyText) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) && ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(
				patientModel.getUserDetails()
				            .getRole()
				            .getName())) {
			String dbEncrptionFileds =
					(String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionFeilds);
			return pHIDataEncryptionService.encryptPatient(patientModel, keyText, dbEncrptionFileds);
		}
		return patientModel;
	}

	public PatientModel decryptPatient(PatientModel patientModel, String keyText) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) && ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(
				patientModel.getUserDetails()
				            .getRole()
				            .getName())) {
			String dbEncrptionFileds =
					(String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionFeilds);
			return pHIDataEncryptionService.decryptPatient(patientModel, keyText, dbEncrptionFileds);
		}
		return patientModel;
	}

	public UserDetailsModel encryptUserDetails(UserDetailsModel userDetailsModel, String keyText) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) && userDetailsModel != null &&
				ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(userDetailsModel.getRole()
				                                                                           .getName())) {
			String dbEncrptionFileds =
					(String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionFeilds);
			return pHIDataEncryptionService.encryptUserDetails(userDetailsModel, keyText, dbEncrptionFileds);
		}
		return userDetailsModel;
	}

	public UserDetails decryptUserDetailsEntity(UserDetails userDetails, String keyText) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) && userDetails != null &&
				ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(userDetails.getRole()
				                                                                      .getName())) {
			UserDetailsModel userDetailsModel = utility.convertUserDetailsEntityToModel(userDetails);
			UserDetailsModel decryUserDetails = decryptUserDetails(userDetailsModel, keyText);
			return utility.convertUserDetailsModelToEntity(decryUserDetails);
		}
		return userDetails;
	}

	public UserDetails encryptUserDetailsEntitiy(UserDetails userDetails, String keyText) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) && userDetails != null &&
				ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(userDetails.getRole()
				                                                                      .getName())) {
			UserDetailsModel userDetailsModel = utility.convertUserDetailsEntityToModel(userDetails);
			UserDetailsModel decryUserDetails = encryptUserDetails(userDetailsModel, keyText);
			return utility.convertUserDetailsModelToEntity(decryUserDetails);
		}
		return userDetails;
	}

	public UserDetailsModel decryptUserDetails(UserDetailsModel userDetailsModel, String keyText) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) && userDetailsModel != null &&
				ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(userDetailsModel.getRole()
				                                                                           .getName())) {
			String dbEncrptionFileds =
					(String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionFeilds);
			return pHIDataEncryptionService.decryptPatientUserDetails(userDetailsModel, keyText, dbEncrptionFileds);
		}
		return userDetailsModel;
	}

	public String filedLevelEncryption(String keyText, String encryptText, String role) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) &&
				ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(role) && encryptText != null) {
			try {
				SecretKey secretKey = pHIDataEncryption.getSecretEncryptionKey(keyText);
				return pHIDataEncryption.encryptText(encryptText, secretKey);
			} catch (Exception e) {
				log.error("Exception occurred during field level encryption", e);
			}
		}
		return encryptText;
	}

	public String filedLevelDecryption(String keyText, String encryptText, String role) {
		String dbEncrptionEnable = (String) rediseService.getDataInRedisByKey(RedisKeyConstants.dbencryptionenable);
		if ("true".equalsIgnoreCase(dbEncrptionEnable) &&
				ApplicationConstants.DAYAMED_PATIENT_ROLE.equalsIgnoreCase(role) && encryptText != null) {
			try {
				SecretKey secretKey = pHIDataEncryption.getSecretEncryptionKey(keyText);
				return pHIDataEncryption.decryptText(encryptText, secretKey);
			} catch (Exception e) {
				log.error("Exception occurred during field level decryption", e);
			}
		}
		return encryptText;
	}

	public String getDBEncryKey() {
		return strDBEncryptionKey;
	}

}
