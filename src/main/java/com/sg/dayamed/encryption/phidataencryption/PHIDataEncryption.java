package com.sg.dayamed.encryption.phidataencryption;

import javax.crypto.SecretKey;

public interface PHIDataEncryption {

	public String encryptText(String plainText, SecretKey secKey) throws Exception;

	public String decryptText(byte[] byteCipherText, SecretKey secKey) throws Exception;

	public String decryptText(String strCipherText, SecretKey secKey) throws Exception;

	public SecretKey getSecretEncryptionKey(String keyText) throws Exception;
}
