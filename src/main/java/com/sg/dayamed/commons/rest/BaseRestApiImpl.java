package com.sg.dayamed.commons.rest;

import com.sg.dayamed.entity.*;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.rest.commons.BaseRestApi;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Gorantla, Eresh
 * @created 06-02-2019
 */
@Component
public class BaseRestApiImpl extends BaseRestApi{

	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	Utility utility;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public int instanceOfPatientModel(PatientModel patientModel, JwtUserDetails jwtUser) {
		if (jwtUser != null && "provider".equalsIgnoreCase(jwtUser.getRole())) {
			Set<ProviderModel> providerModelSet = patientModel.getProviders();
			boolean underProvider = false;
			for (ProviderModel providerModel : providerModelSet) {
				if (providerModel.getUserDetails()
				                 .getEmailId()
				                 .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underProvider = true;
				}
			}
			if (!underProvider) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "caregiver".equalsIgnoreCase(jwtUser.getRole())) {
			Set<CaregiverModel> caregiverModelSet = patientModel.getCaregivers();
			boolean underCaregiver = false;
			for (CaregiverModel caregiverModel : caregiverModelSet) {
				if (caregiverModel.getUserDetails()
				                  .getEmailId()
				                  .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underCaregiver = true;
				}
			}
			if (!underCaregiver) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "pharmacist".equalsIgnoreCase(jwtUser.getRole())) {
			Set<PharmacistModel> pharmacistModelSet = patientModel.getPharmacists();
			boolean underPharmacist = false;
			for (PharmacistModel pharmacistModel : pharmacistModelSet) {
				if (pharmacistModel.getUserDetails()
				                   .getEmailId()
				                   .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underPharmacist = true;
				}
			}
			if (!underPharmacist) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		return HttpStatus.OK.value();
	}

	public int instanceOfPrescription(Prescription prescription, JwtUserDetails jwtUser) {
		Patient patient = prescription.getPatient();
		if (jwtUser != null && "provider".equalsIgnoreCase(jwtUser.getRole())) {
			Set<Provider> providerSet = patient.getProviders();
			boolean underProvider = false;
			for (Provider provider : providerSet) {
				if (provider.getUserDetails()
				            .getEmailId()
				            .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underProvider = true;
				}
			}
			if (!underProvider) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "caregiver".equalsIgnoreCase(jwtUser.getRole())) {
			Set<Caregiver> caregiverSet = patient.getCaregivers();
			boolean underCaregiver = false;
			for (Caregiver caregiver : caregiverSet) {
				if (caregiver.getUserDetails()
				             .getEmailId()
				             .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underCaregiver = true;
				}
			}
			if (!underCaregiver) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		if (jwtUser != null && "pharmacist".equalsIgnoreCase(jwtUser.getRole())) {
			Set<Pharmacist> PharmacistSet = patient.getPharmacists();
			boolean underPharmacist = false;
			for (Pharmacist pharmacist : PharmacistSet) {
				if (pharmacist.getUserDetails()
				              .getEmailId()
				              .equalsIgnoreCase(jwtUser.getUserEmailId())) {
					underPharmacist = true;
				}
			}
			if (!underPharmacist) {
				return HttpStatus.UNAUTHORIZED.value();
			}
		}
		return HttpStatus.OK.value();
	}


	protected <I> ResponseEntity<Object> inboundServiceCall(I request,
	                                                        IServiceMethod<I, ResponseEntity<Object>> service,
	                                                        Class<? extends Object> clazz) throws Exception {
		try {
			ResponseEntity<Object> response = service.execute(request);
			if (response.getStatusCode()
			            .isError()) {
				return new ResponseEntity<>(response.getStatusCode());
			}
			Authentication authentication = SecurityContextHolder.getContext()
			                                                     .getAuthentication();
			Object respObject = response.getBody();
			JwtUserDetails jwtUser = (JwtUserDetails) authentication.getPrincipal();

			if (respObject != null && respObject instanceof List) {
				if (((List) respObject).size() > 0 && (((List) respObject).get(0) instanceof Patient)) {
					Patient patient = (Patient) respObject;
					int result = utility.instanceOfPatient(patient, jwtUser);
					if (result == 401) {
						return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
					}
				}
				if (((List) respObject).size() > 0 && (((List) respObject).get(0) instanceof PatientModel)) {
					Patient patient = (Patient) respObject;
					int result = utility.instanceOfPatient(patient, jwtUser);
					if (result == 401) {
						return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
					}
				}
				if (((List) respObject).size() > 0 && (((List) respObject).get(0) instanceof Prescription)) {
					int result = instanceOfPrescription(((Prescription) respObject), jwtUser);
					if (result == 401) {
						return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
					}
				}
				if (((List) respObject).size() > 0 && (((List) respObject).get(0) instanceof PrescriptionModel)) {
					int result = utility.instanceOfPrescriptionModel(((PrescriptionModel) ((List) respObject).get(0)),
					                                         jwtUser);
					if (result == 401) {
						return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
					}
				}
			}

			//patient
			if (respObject instanceof Patient) {
				Patient patient = (Patient) respObject;
				int result = utility.instanceOfPatient(patient, jwtUser);
				if (result == 401) {
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
			}
			//patient Model
			if (respObject instanceof PatientModel) {
				int result = instanceOfPatientModel((PatientModel) respObject, jwtUser);
				if (result == 401) {
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
			}
			// Prescription
			if (respObject instanceof Prescription) {
				int result = instanceOfPrescription(((Prescription) respObject), jwtUser);
				if (result == 401) {
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
			}
			//PrescriptionModel
			if (respObject instanceof PrescriptionModel) {
				int result = utility.instanceOfPrescriptionModel(((PrescriptionModel) respObject), jwtUser);
				if (result == 401) {
					return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
				}
			}
			return response;
		} catch (Exception e) {
			logger.error("Error processing the " + e);
			//throw processException(e);
			throw e;
		}
	}

}
