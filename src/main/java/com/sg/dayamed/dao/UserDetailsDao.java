package com.sg.dayamed.dao;

import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By Gorantla, Eresh on 19/Sep/2019
 **/
@Component
@CacheConfig(cacheNames = EhCacheConstants.USER_DETAILS_CACHE)
public class UserDetailsDao {

	@Autowired
	UserPreferencesVService userPreferencesVService;

	@Cacheable
	public List<UserDetailsCacheVO> loadUserCacheDetails() {
		List<UserPreferenceVO> preferenceVOS = userPreferencesVService.getAllUserPreferences();
		return CollectionUtils.isNotEmpty(preferenceVOS) ? preferenceVOS.stream()
		                                                                .map(UserDetailsCacheVO::new)
		                                                                .collect(Collectors.toList()) : Collections.emptyList();
	}

}