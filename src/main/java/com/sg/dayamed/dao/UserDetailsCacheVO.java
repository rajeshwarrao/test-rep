package com.sg.dayamed.dao;

import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created By Gorantla, Eresh on 19/Sep/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class UserDetailsCacheVO implements Serializable {

	private Long id;

	private String role;

	private Integer roleId;

	public UserDetailsCacheVO(UserPreferenceVO preferenceVO) {
		this.id = preferenceVO.getUserId();
		this.role = preferenceVO.getRole();
		this.roleId = preferenceVO.getRoleId() != null ? preferenceVO.getRoleId()
		                                                             .intValue() : null;
	}
}