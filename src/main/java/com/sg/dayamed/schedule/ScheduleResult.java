package com.sg.dayamed.schedule;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eresh Gorantla on 03/Jun/2019
 **/
@Getter
@Setter
public class ScheduleResult {

	private String notificationValue;

	private String notificationType;

	private List<String> dosages = new ArrayList<>();
}
