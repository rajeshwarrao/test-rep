package com.sg.dayamed.schedule;

import com.sg.dayamed.adapter.notifications.vo.DosageDetailsVO;
import com.sg.dayamed.adapter.notifications.vo.MedicineStatusInfoVO;
import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Adherence_DosageInfoMap;
import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.BpMonitorModel;
import com.sg.dayamed.pojo.HeartRateModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.ScaleModel;
import com.sg.dayamed.pojo.StepsModel;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.BiometricService;
import com.sg.dayamed.service.BpMonitorService;
import com.sg.dayamed.service.GoodShapredAlertProtocal;
import com.sg.dayamed.service.HeartRateService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PaitentAdherenceWeeklyPDFReport;
import com.sg.dayamed.service.ScaleService;
import com.sg.dayamed.service.StepsService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.WeeklyEmailNotificationToPatient;
import com.sg.dayamed.service.velocity.VelocityServices;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.Utility;

import io.jsonwebtoken.lang.Collections;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ScheduleJobs {

	@Autowired
	NotificationScheduleRepository notificationScheduleRepository;

	@Autowired
	DateUtility dateUtility;

	@Autowired
	NotificationService notificationService;

	@Autowired
	PrescriptionRepository prescriptionRepository;

	@Autowired
	PrescriptionScheduleRepository prescriptionScheduleRepository;

	@Autowired
	EmailAndSmsUtility emailAndSmsUtility;

	@Autowired
	BpMonitorService bpMonitorService;

	@Autowired
	ScaleService scaleService;

	@Autowired
	StepsService stepsService;

	@Autowired
	HeartRateService heartRateService;

	@Autowired
	AdherenceService adherenceService;

	@Autowired
	BiometricService biometricService;

	@Autowired
	Utility utility;

	@Autowired
	DosageInfoRepository dosageInfoRepository;

	@Autowired
	UserDetailsService userDetailsService;

	@Value("${dayamed.deeplink.call}")
	String callDeeplink;

	@Value("${dayamed.deeplink.dosageinfo}")
	String dosageInfoDeeplink;

	@Value("${dayamed.deeplink.deviceinfo}")
	String deviceInfoDeeplink;

	@Autowired
	NotificationScheduleService notificationScheduleService;

	@Autowired
	EmailService emailService;

	@Autowired
	MessageNotificationConstants messageNotificationConstants;

	private static final Logger logger = LoggerFactory.getLogger(ScheduleJobs.class);

	final String DATE_FORMAT = "yyyy-MM-dd HH:mm";

	DateTimeFormatter dayamedGeneralFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

	@Scheduled(cron = "*/59 * * * * *")
	public void executeDosageAndDeviceSchedlerJobsByFireTime() {
		String localDateTimeStr = dateUtility
				.getFormattedLocalDateTime(LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC));
		LocalDateTime ldt = LocalDateTime.parse(localDateTimeStr, DateTimeFormatter.ofPattern(DATE_FORMAT));
		List<BigInteger> nsIds = notificationScheduleRepository.getNotificationScheduleIdsNotInDosageInfoTable();
		if (nsIds.size() > 0) {
			notificationScheduleService.deletebulkNotificationSchdule(nsIds);
		}
		try {
			List<NotificationSchedule> scheduleList = notificationScheduleRepository.findByFireTimeLessThanEqual(ldt);
			List<Long> userIds = scheduleList.stream()
			                                 .map(NotificationSchedule::getUserId)
			                                 .distinct()
			                                 .collect(Collectors.toList());
			StringBuilder emailBuilder = new StringBuilder(128);
			StringBuilder smsBuilder = new StringBuilder(128);
			List<ScheduleResult> results = new ArrayList<>();
			Map<String, ScheduleResult> listMap = new HashMap<>();
			// P005631713-63-Notifications template and text
			List<DosageDetailsVO> dosageDetailsVOS = new ArrayList<>();
			Map<String, List<DosageDetailsVO>> dosageDetailVoMap = new HashMap<>();
			for (Long userId : userIds) {
				ScheduleResult scheduleResult;
				List<NotificationSchedule> notificationSchedules = scheduleList.stream()
				                                                               .filter(sch -> sch.getUserId() ==
						                                                               userId.longValue())
				                                                               .collect(Collectors.toList());
				Patient patient = notificationSchedules.get(0)
				                                       .getDosageInfo()
				                                       .getPrescription()
				                                       .getPatient();
				UserDetails patientUserDetails = patient.getUserDetails();
				String patientName = patientUserDetails.getFirstName() + " " + patientUserDetails.getLastName();
				String mobileNumber = patientUserDetails.getMobileNumber();
				List<String> notificationValues = notificationSchedules.stream()
				                                                       .map(NotificationSchedule::getNotificationValue)
				                                                       .distinct()
				                                                       .collect(Collectors.toList());
				notificationValueLoop:
				for (String notificationValue : notificationValues) {
					scheduleResult = results.stream()
					                        .filter(sch -> sch.getNotificationValue()
					                                          .equalsIgnoreCase(notificationValue))
					                        .findFirst()
					                        .orElse(null);
					if (scheduleResult == null) {
						scheduleResult = new ScheduleResult();
						scheduleResult.setNotificationValue(notificationValue);
					}
					List<String> messages = new ArrayList<>();
					List<NotificationSchedule> schedules = notificationSchedules.stream()
					                                                            .filter(sch -> notificationValue.equalsIgnoreCase(
							                                                            sch.getNotificationValue()))
					                                                            .collect(Collectors.toList());
					List<NotificationSchedule> emailSchedules = schedules.stream()
					                                                     .filter(sch -> "Email".equalsIgnoreCase(
							                                                     sch.getNotificationType())
							                                                     || "Video".equalsIgnoreCase(
							                                                     sch.getNotificationType()))
					                                                     .filter(sch -> sch.getNotificationValue()
					                                                                       .contains("@"))
					                                                     .collect(Collectors.toList());
					List<NotificationSchedule> smsSchedules = schedules.stream()
					                                                   .filter(sch -> "Sms".equalsIgnoreCase(
							                                                   sch.getNotificationType())
							                                                   || "Video".equalsIgnoreCase(
							                                                   sch.getNotificationType()))
					                                                   .filter(sch -> !sch.getNotificationValue()
					                                                                      .contains("@"))
					                                                   .collect(Collectors.toList());
					if (CollectionUtils.isNotEmpty(emailSchedules)) {
						if (!messages.contains(patientName)) {
							messages.add("</br><b>Patient : " + patientName + " </b>");
						}
					}
					if (CollectionUtils.isNotEmpty(smsSchedules)) {
						if (!messages.contains(patientName)) {
							messages.add("Patient : " + patientName);
						}
					}
					// P005631713-63-Notifications template and text
					List<MedicineStatusInfoVO> medicineStatusList = new ArrayList<>();
					MedicineStatusInfoVO medicineStatusInfoVO;
					for (NotificationSchedule emailData : emailSchedules) {
						if (emailData.getDosageInfo() != null) {
							String medicineName = emailData.getDosageInfo()
							                               .getMedicine()
							                               .getName();
							List<String> consumptionStatuses = emailData.getConsumptionStatus();
							if (CollectionUtils.isNotEmpty(consumptionStatuses)) {
								medicineStatusInfoVO = new MedicineStatusInfoVO(medicineName,
								                                                consumptionStatuses.stream()
								                                                                   .collect(
										                                                                   Collectors.joining(
												                                                                   ", "
										                                                                   )));
								emailBuilder.append(medicineName)
								            .append(" ")
								            .append(": ")
								            .append(consumptionStatuses.stream()
								                                       .collect(Collectors.joining(", ")));
							} else {
								emailBuilder.append(medicineName)
								            .append(" ")
								            .append(": ")
								            .append("No Action");
								medicineStatusInfoVO = new MedicineStatusInfoVO(medicineName, "No Action");
							}
							messages.add(emailBuilder.toString());
							emailBuilder = new StringBuilder(128);
							medicineStatusList.add(medicineStatusInfoVO);
						}

					}
					for (NotificationSchedule smsData : smsSchedules) {
						if (smsData.getDosageInfo() != null) {
							String medicineName = smsData.getDosageInfo()
							                             .getMedicine()
							                             .getName();
							List<String> consumptionStatuses = smsData.getConsumptionStatus();
							if (CollectionUtils.isNotEmpty(consumptionStatuses)) {
								smsBuilder.append(medicineName)
								          .append(" ")
								          .append(": ")
								          .append(consumptionStatuses.stream()
								                                     .collect(Collectors.joining(", ")));
							} else {
								smsBuilder.append(medicineName)
								          .append(" ")
								          .append(": ")
								          .append("No Action");
							}
							messages.add(smsBuilder.toString());
							smsBuilder = new StringBuilder(128);
						}
					}
					List<Long> lostOfOwnerIds = notificationSchedules.stream()
					                                                 .map(NotificationSchedule::getOwner_userId)
					                                                 .distinct()
					                                                 .collect(Collectors.toList());
					List<UserDetails> caregiverUsersDetails = userDetailsService
							.findUserDetailsByIdInAndRole(lostOfOwnerIds, ApplicationConstants.CAREGIVER);
					for (UserDetails userDetails : caregiverUsersDetails) {
						List<NotificationSchedule> notificationSchedulesByOwner = notificationSchedules.stream()
						                                                                               .filter(sch -> sch.getOwner_userId() ==
								                                                                               userDetails.getId())
						                                                                               .collect(
								                                                                               Collectors.toList());
						List<NotificationSchedule> emailSchedulesOfOwner = notificationSchedulesByOwner.stream()
						                                                                               .filter(sch ->
								                                                                                       "Email".equalsIgnoreCase(
								                                                                               sch.getNotificationType())
								                                                                               ||
								                                                                               "Video".equalsIgnoreCase(
										                                                                               sch.getNotificationType()))
						                                                                               .filter(sch -> sch.getNotificationValue()
						                                                                                                 .contains(
								                                                                                                 "@"))
						                                                                               .collect(
								                                                                               Collectors.toList());
						List<NotificationSchedule> smsSchedulesOfOwner = notificationSchedulesByOwner.stream()
						                                                                             .filter(sch ->
								                                                                                     "Sms".equalsIgnoreCase(
								                                                                             sch.getNotificationType())
								                                                                             ||
								                                                                             "Video".equalsIgnoreCase(
										                                                                             sch.getNotificationType()))
						                                                                             .filter(sch -> !sch.getNotificationValue()
						                                                                                                .contains(
								                                                                                                "@"))
						                                                                             .collect(
								                                                                             Collectors.toList());
						if (addCaregiverNotification(listMap, userDetails, emailSchedulesOfOwner, patientName)
								|| addCaregiverNotification(listMap, userDetails, smsSchedulesOfOwner, patientName)) {
							continue notificationValueLoop;
						}
					}
					StringBuilder deepLink = new StringBuilder();
					StringBuffer callDeepLinkSb = new StringBuffer(callDeeplink);
					if (callDeepLinkSb != null && !callDeepLinkSb.toString()
					                                             .isEmpty()) {
						String strPatientId = String.valueOf(patientUserDetails.getId());
						callDeepLinkSb = new StringBuffer(callDeepLinkSb.toString()
						                                                .replace("$userId",
						                                                         strPatientId != null ? strPatientId :
								                                                         " "));
						deepLink.append(callDeepLinkSb.toString());
					}
					NotificationSchedule emailSchedule = emailSchedules.stream()
					                                                   .filter(sch -> "Video".equalsIgnoreCase(
							                                                   sch.getNotificationType()))
					                                                   .findFirst()
					                                                   .orElse(null);
					String dosageMobileNumber = null;
					String dosageDeepLink = null;
					if (emailSchedule != null) {
						dosageMobileNumber = mobileNumber;
						dosageDeepLink = deepLink.toString();
						messages.add(new StringBuilder(64).append("Contact:")
						                                  .append(mobileNumber + " OR ")
						                                  .append(deepLink.toString())
						                                  .toString());
					}
					NotificationSchedule smsSchedule = smsSchedules.stream()
					                                               .filter(sch -> "Video".equalsIgnoreCase(
							                                               sch.getNotificationType()))
					                                               .findFirst()
					                                               .orElse(null);
					if (smsSchedule != null) {
						messages.add(new StringBuilder(64).append("Contact:")
						                                  .append(mobileNumber + " OR ")
						                                  .append(deepLink.toString())
						                                  .toString());
					}
					scheduleResult.getDosages()
					              .addAll(messages);
					if (listMap.containsKey(notificationValue)) {
						ScheduleResult result = listMap.get(notificationValue);
						result.getDosages()
						      .addAll(messages);
						scheduleResult.setDosages(result.getDosages());
						listMap.put(notificationValue, scheduleResult);
					} else {
						listMap.put(notificationValue, scheduleResult);
					}
					// P005631713-63-Notifications template and text
					if (notificationValue.contains("@")) {
						if (dosageDetailVoMap.containsKey(notificationValue)) {
							List<DosageDetailsVO> listDosageDetailsVo = (List<DosageDetailsVO>) dosageDetailVoMap
									.get(notificationValue);
							listDosageDetailsVo.add(new DosageDetailsVO(patientName, medicineStatusList,
							                                            dosageMobileNumber, dosageDeepLink));
							dosageDetailVoMap.put(notificationValue, listDosageDetailsVo);
						} else {
							List<DosageDetailsVO> listDosageDetailsVo = new ArrayList<>();
							listDosageDetailsVo.add(new DosageDetailsVO(patientName, medicineStatusList,
							                                            dosageMobileNumber, dosageDeepLink));
							dosageDetailVoMap.put(notificationValue, listDosageDetailsVo);
						}
					}
				}

			}
			dosageDetailVoMap.forEach((k, v) -> {
				velocityServices.sendCustomEmailForDosages(v, k);
			});

			listMap.forEach((k, v) -> {
				ScheduleResult result = v;
				String message;
				if (k.contains("@")) {
					Email obj = new Email();
					message = result.getDosages()
					                .stream()
					                .filter(mes -> StringUtils.isNotBlank(mes))
					                .collect(Collectors.joining("</br>"));
					obj.setBody(message);
					obj.setRecipientList(Stream.of(k)
					                           .collect(Collectors.toList()));
					obj.setSubject("Medication Status");
					// if notification value email or video link we are sending
					// through velocity.
					// emailService.sendEmail(obj);
				} else {
					message = result.getDosages()
					                .stream()
					                .filter(mes -> StringUtils.isNotBlank(mes))
					                .collect(Collectors.joining(System.getProperty("line.separator")));
					emailAndSmsUtility.sendSms(message, k);
				}
			});
			notificationScheduleRepository.deleteAll(scheduleList);
		} catch (Exception e) {
			logger.error("Error while executing notfifications schedule", e);
		}
	}

	@Autowired
	VelocityServices velocityServices;

	public boolean addCaregiverNotification(Map<String, ScheduleResult> listMap, UserDetails userDetails,
	                                        List<NotificationSchedule> schedulesOfOwner, String patientName) {
		for (NotificationSchedule schedulerJob : schedulesOfOwner) {
			if (schedulerJob.getConsumptionStatus()
			                .size() == 0
					|| schedulerJob.getConsumptionStatus()
					               .contains(ApplicationConstants.CONSUMPTION_STATUS_SKIPPED)
					|| schedulerJob.getConsumptionStatus()
					               .contains(ApplicationConstants.CONSUMPTION_STATUS_NOACTION)
					|| (schedulerJob.getConsumptionStatus()
					                .size() == 1 && schedulerJob.getConsumptionStatus()
					                                            .contains(
							                                            ApplicationConstants.CONSUMPTION_STATUS_DELAY))
					|| (schedulerJob.getConsumptionStatus()
					                .size() == 1 && schedulerJob.getConsumptionStatus()
					                                            .contains(
							                                            ApplicationConstants.CONSUMPTION_STATUS_AUTODELAY))) {
				String msgKey = "caregiver.notification.patient.missed.medication";
				Map<String, String> msgResourceReplaceKeyVal = new HashMap<>();
				msgResourceReplaceKeyVal.put("patientName", patientName);
				String userLangPreference = utility.getUserPreferedLang(userDetails.getId());
				String bodyMsg =
						messageNotificationConstants.getUserPreferedLangTranslatedMsg(msgResourceReplaceKeyVal,
				                                                                               msgKey,
				                                                                               userLangPreference,
				                                                                               null);
				ScheduleResult scheduleResultCar = new ScheduleResult();
				scheduleResultCar.setNotificationValue(schedulerJob.getNotificationValue());
				scheduleResultCar.getDosages()
				                 .add(bodyMsg);
				if (schedulerJob.getNotificationValue()
				                .contains("@")) {
					Email obj = new Email();
					obj.setBody(bodyMsg);
					obj.setRecipientList(Stream.of(schedulerJob.getNotificationValue())
					                           .collect(Collectors.toList()));
					obj.setSubject("Medication Status");
					emailService.sendEmail(obj);
				} else {
					emailAndSmsUtility.sendSms(bodyMsg, schedulerJob.getNotificationValue());
				}
				return true;
			}
		}
		return false;
	}

	public void sendEmail(Map<String, String> notificationMap) {
		if (!Collections.isEmpty(notificationMap)) {
			notificationMap.forEach((mailid, body) -> {
				List<String> recipientList = new ArrayList<String>();
				recipientList.add(mailid);
				emailAndSmsUtility.sendEmail("Medicine Status", body, recipientList);
			});
		}
	}

	@Scheduled(cron = "*/59 * * * * *")
	public void executeDosageAndDevicePrescriptionScheduleByPrescribedTime() {

		// fetching schedulerJobs for every 1min based on firing time
		String localDateTimeStr = dateUtility
				.getFormattedLocalDateTime(LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC));
		LocalDateTime ldtInUTC = LocalDateTime.parse(localDateTimeStr, DateTimeFormatter.ofPattern(DATE_FORMAT));

		try {
			List<PrescriptionSchedule> dosagePrescriptionScheduleList = prescriptionScheduleRepository
					.findByFireTimeLessThanEqualAndDosageInfoNotNull(ldtInUTC);
			List<PrescriptionSchedule> deviceprescriptionScheduleList = prescriptionScheduleRepository
					.findByFireTimeLessThanEqualAndDeviceInfoNotNull(ldtInUTC);
			logger.info("prescriptionSchedule jobs loaded");
			Set<Long> userIdsSet = new LinkedHashSet<Long>();

			Set<LocalDateTime> prescribedTimeSet = new TreeSet<LocalDateTime>();

			// creating prescribedtime unique set.
			dosagePrescriptionScheduleList.forEach(prescriptionSchedule -> {
				prescribedTimeSet.add(prescriptionSchedule.getPrescribedTime());
				userIdsSet.add(prescriptionSchedule.getUserId());

			});
			// creating userids unique set
			/*
			 * dosagePrescriptionScheduleList.forEach(prescriptionSchedule ->{
			 * userIdsSet.add(prescriptionSchedule.getUserId()) ; });
			 */
			// we can get different prescribed times of PrescriptionSchedules.
			for (LocalDateTime prescribedTime : prescribedTimeSet) {
				List<PrescriptionSchedule> dosagePrescriptionScheduleListByPrescribedTime = new ArrayList<>();
				for (PrescriptionSchedule prescriptionSchedule : dosagePrescriptionScheduleList) {
					if (prescriptionSchedule.getPrescribedTime()
					                        .toString()
					                        .equals(prescribedTime.toString())) {
						dosagePrescriptionScheduleListByPrescribedTime.add(prescriptionSchedule);
					}
				}
				// P005631713-63-Notifications template and text
				List<DosageDetailsVO> listDosageDetailsVO = new ArrayList<>();
				MedicineStatusInfoVO medicineStatusInfoVO;
				// Not taken any action should be saved in medicine Aherenace
				// table
				// userIdsSet.forEach(userid ->{
				for (Long userid : userIdsSet) {
					String patientName = ApplicationConstants.EMPTY_STRING;
					String patientMobileNumber = ApplicationConstants.EMPTY_STRING;
					Patient patient = null;

					AdherenceDataPoints adherenceDataPoints = new AdherenceDataPoints();
					List<Adherence_DosageInfoMap> adherence_DosageInfoMapList = new ArrayList<>();
					adherenceDataPoints.setObservedTime(ldtInUTC);
					List<String> medicineNameList = new ArrayList<>();
					// subPrescriptionScheduleList is at same prescribed time
					// and for one patient.
					List<PrescriptionSchedule> noConsumptionStutsSubPrescriptionScheduleList = new ArrayList<>();
					List<PrescriptionSchedule> skipStutsSubPrescriptionScheduleList = new ArrayList<>();

					for (PrescriptionSchedule prescriptionSchedule : dosagePrescriptionScheduleListByPrescribedTime) {
						if (prescriptionSchedule.getUserId() == userid) {
							medicineNameList.add(prescriptionSchedule.getDosageInfo()
							                                         .getMedicine()
							                                         .getName());
							if (prescriptionSchedule.getConsumptionStatus()
							                        .size() <= 0) {
								noConsumptionStutsSubPrescriptionScheduleList.add(prescriptionSchedule);
								// if
								// (prescriptionSchedule.getConsumptionStatus().size()
								// <= 0) {
								Adherence_DosageInfoMap adherence_DosageInfoMap = new Adherence_DosageInfoMap();
								adherence_DosageInfoMap.setDosageinfoId(prescriptionSchedule.getDosageInfo()
								                                                            .getId());
								adherence_DosageInfoMap.setAdherence(adherenceDataPoints);
								adherence_DosageInfoMapList.add(adherence_DosageInfoMap);
								// }
								adherenceDataPoints.setPrescribedTime(prescriptionSchedule.getPrescribedTime());// this
								// is
								// wrong

							} else if (prescriptionSchedule.getConsumptionStatus()
							                               .contains(
									                               ApplicationConstants.CONSUMPTION_STATUS_SECONDALERT_SKIPPED)) {
								skipStutsSubPrescriptionScheduleList.add(prescriptionSchedule);
							}
							// deleting whether it has consumption status or not
							prescriptionScheduleRepository.delete(prescriptionSchedule);
						}
					}
					adherenceDataPoints.setAdherenceDosageInfoMapList(adherence_DosageInfoMapList);
					String medicineNameListCommaSeparated = medicineNameList.stream()
					                                                        .collect(Collectors.joining(","));

					PrescriptionSchedule prescriptionSchedule = null;
					if (noConsumptionStutsSubPrescriptionScheduleList.size() > 0) {
						prescriptionSchedule = noConsumptionStutsSubPrescriptionScheduleList.get(0);
					}
					if (skipStutsSubPrescriptionScheduleList.size() > 0) {
						prescriptionSchedule = skipStutsSubPrescriptionScheduleList.get(0);
					}
					if (prescriptionSchedule != null) {

						// }
						patient = prescriptionSchedule.getDosageInfo()
						                              .getPrescription()
						                              .getPatient();
						UserDetails patientUserdetails = patient.getUserDetails();
						patientName = patientUserdetails.getFirstName() + " " + patientUserdetails.getLastName();
						StringBuffer callDeeplinkSb = new StringBuffer(callDeeplink);
						if (callDeeplinkSb != null && !callDeeplinkSb.toString()
						                                             .isEmpty()) {
							String strPatientId = String.valueOf(patientUserdetails.getId());
							callDeeplinkSb = new StringBuffer(callDeeplinkSb.toString()
							                                                .replace("$userId",
							                                                         strPatientId != null ?
									                                                         strPatientId : " "));
						}
						patientMobileNumber = patientUserdetails.getCountryCode()
								+ patientUserdetails.getMobileNumber();
						String fromatedPatientMobileNumber = patientMobileNumber.replace("(", "")
						                                                        .replace(")", "")
						                                                        .replace("-", "")
						                                                        .replaceAll("\\s", "");

						String userLangPreference = utility.getUserPreferedLang(userid);
						Map<String, String> keyVal = new HashMap<String, String>();
						keyVal.put("medicineNameListCommaSeparated", medicineNameListCommaSeparated);
						String patientBodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal,
						                                                                                      "care.about.you",
						                                                                                      userLangPreference,
						                                                                                      null);
						keyVal.put("patientName", patientName);
						keyVal.put("fromatedPatientMobileNumber", fromatedPatientMobileNumber);
						keyVal.put("callDeeplinkSb", callDeeplinkSb.toString());
						String bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal,
						                                                                               "no.action" +
								                                                                               ".perform",
						                                                                               userLangPreference,
						                                                                               null);

						if (noConsumptionStutsSubPrescriptionScheduleList.size() > 0) {

							List<String> consumptionStatusList = new ArrayList<String>();
							consumptionStatusList.add("NO_Action");
							adherenceDataPoints.setConsumptionStatus(consumptionStatusList);
							adherenceDataPoints.setHow(null);
							adherenceDataPoints
									.setPrescriptionID(prescriptionSchedule.getDosageInfo()
									                                       .getPrescription()
									                                       .getId());
							adherenceDataPoints.setLocation(new ArrayList<String>());

							// Adding adherence
							AdherenceDataPoints persistedMedAdherence = adherenceService.addAdherence(
									adherenceDataPoints,
									prescriptionSchedule.getPrescribedTime()
									                    .format(dayamedGeneralFormatter));
							String notificationMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(
									keyVal, "not.taken.anyacton", userLangPreference, null);

							// saving in Notification table
							notificationService.addWebNotification(patientUserdetails.getId(), notificationMsg,
							                                       "Medicine no action",
							                                       ApplicationConstants.MedicineNoAction);
						}
						// if( skipStutsSubPrescriptionScheduleList.size()>0){

						// sending Mail and Sms to respective provider and
						// caregiver & Patient
						// To patient
						List<String> recipientList = new ArrayList<String>();
						recipientList.add(patient.getUserDetails()
						                         .getEmailId());
						emailAndSmsUtility.sendEmail("Medicine Status", patientBodyMsg, recipientList);
						emailAndSmsUtility.sendSms(patientBodyMsg, patient.getUserDetails()
						                                                  .getCountryCode(),
						                           patient.getUserDetails()
						                                  .getMobileNumber());
						recipientList.clear();

						// sending sms to provider
						for (Provider provider : patient.getProviders()) {
							recipientList.add(provider.getUserDetails()
							                          .getEmailId());
							// emailAndSmsUtility.sendSms(bodyMsg,
							// provider.getUserDetails().getCountryCode(),provider
							// .getUserDetails().getMobileNumber());
						}
						// emailAndSmsUtility.sendEmail("Medicine
						// Status",bodyMsg, recipientList);
						// Sending Eamil and Sms to respective caregivers
						Set<Caregiver> caregiverSet = patient.getCaregivers();
						List<String> caregiverMailList = new ArrayList<>();
						for (Caregiver caregiver : caregiverSet) {
							caregiverMailList.add(caregiver.getUserDetails()
							                               .getEmailId());
							emailAndSmsUtility.sendSms(bodyMsg, caregiver.getUserDetails()
							                                             .getCountryCode(),
							                           caregiver.getUserDetails()
							                                    .getMobileNumber());
						}
						if (caregiverMailList.size() > 0) {
							emailAndSmsUtility.sendEmail("emailsubject.medicinestatus", "no.action.perform", keyVal,
							                             caregiverMailList);
						}

					}

				}
				dosagePrescriptionScheduleListByPrescribedTime.clear();
			}

			// For Device Info
			for (PrescriptionSchedule prescriptionSchedule : deviceprescriptionScheduleList) {
				String patientName = ApplicationConstants.EMPTY_STRING;
				String patientMobileNumber = ApplicationConstants.EMPTY_STRING;
				Patient patient = null;
				if (prescriptionSchedule.getDeviceInfo() != null) {
					DeviceInfo deviceInfo = prescriptionSchedule.getDeviceInfo();
					Prescription prescription = prescriptionSchedule.getDeviceInfo()
					                                                .getPrescription();
					patient = prescription.getPatient();
					UserDetails patientUserdetails = patient.getUserDetails();
					PatientModel patientModel = utility.convertPatientEntityTopatientModel(patient);
					patientName = patientUserdetails.getFirstName() + " " + patientUserdetails.getLastName();
					patientMobileNumber = patientUserdetails.getCountryCode() + patientUserdetails.getMobileNumber();

					long providerId = 0L;
					Set<Provider> providerset = patient.getProviders();
					List<Provider> providerList = new ArrayList<Provider>(providerset);
					if (providerList.size() > 0) {
						providerId = providerList.get(0)
						                         .getUserDetails()
						                         .getId();
					}
					StringBuffer callDeeplinkSb = new StringBuffer(callDeeplink);
					if (callDeeplinkSb != null && !callDeeplinkSb.toString()
					                                             .isEmpty()) {
						String strPatientId = String.valueOf(patientUserdetails.getId());
						callDeeplinkSb = new StringBuffer(callDeeplinkSb.toString()
						                                                .replace("$userId",
						                                                         strPatientId != null ? strPatientId :
								                                                         " "));
					}
					String fromatedMobileNumber = patientMobileNumber.replace("(", "")
					                                                 .replace(")", "")
					                                                 .replace("-", "")
					                                                 .replaceAll("\\s", "");

					String userLangPreference = utility.getUserPreferedLang(providerId);
					Map<String, String> keyVal = new HashMap<String, String>();
					keyVal.put("patientName", patientName);
					keyVal.put("medicineNameListCommaSeparated",
					           prescriptionSchedule.getDeviceInfo()
					                               .getDevice()
					                               .getName());
					keyVal.put("callDeeplinkSb", callDeeplinkSb.toString());
					String bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal,
					                                                                               "no.action.perform",
					                                                                               userLangPreference,
					                                                                               null);

					// Range for BP
					String msg = ApplicationConstants.EMPTY_STRING;
					String msg2 = ApplicationConstants.EMPTY_STRING;

					if (prescriptionSchedule.getConsumptionStatus() != null) {

						if (prescriptionSchedule.getConsumptionStatus()
						                        .contains("taken")) {

							Map<String, String> keyValNew = new HashMap<String, String>();
							keyVal.put("patientName", patientName);
							keyVal.put("deviceName", prescriptionSchedule.getDeviceInfo()
							                                             .getDevice()
							                                             .getName());
							keyVal.put("fromatedPatientMobileNumber", fromatedMobileNumber);
							keyVal.put("callDeeplinkSb", callDeeplinkSb.toString());

							bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyValNew,
							                                                                        "device.reading" +
									                                                                        ".taken" +
									                                                                        ".patient",
							                                                                        userLangPreference,
							                                                                        null);
							// Bp Meter
							if (deviceInfo.getDevice()
							              .getName()
							              .equalsIgnoreCase(ApplicationConstants.BP_METER)) {
								if (deviceInfo != null && deviceInfo.getDeviceNotificationList() != null
										&& deviceInfo.getDeviceNotificationList()
										             .size() > 0
										&& deviceInfo.getDeviceNotificationList()
										             .get(0) != null) {
									List<BpMonitor> bpMonitorList = bpMonitorService
											.findByPrescribedTime(prescriptionSchedule.getPrescribedTime());

									if (bpMonitorList.size() > 0) {
										BpMonitor bpMonitor = bpMonitorList
												.get(0);/*
										 * devices some hitting
										 * more times when
										 * taking readings
										 */
										if (bpMonitor != null
												&& deviceInfo.getDeviceNotificationList()
												             .get(0)
												             .getGreaterThanReading1() != null
												&& !(Double
												.parseDouble(
														bpMonitor.getSystolicpressureValue()
														         .replace("mmHg", "")
														         .trim()) > (Double.parseDouble(deviceInfo
																                                        .getDeviceNotificationList()
																                                        .get(0)
																                                        .getGreaterThanReading1())))) {
											Map<String, String> keyValsysto = new HashMap<String, String>();
											keyValsysto.put("range", deviceInfo.getDeviceNotificationList()
											                                   .get(0)
											                                   .getGreaterThanReading1());
											msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(
													keyValsysto, "systolic.value.beyond", userLangPreference, null);
										}

										if (bpMonitor != null
												&& deviceInfo.getDeviceNotificationList()
												             .get(0)
												             .getGreaterThanReading2() != null
												&& !(Double
												.parseDouble(
														bpMonitor.getDiastolicpressureValue()
														         .replace("mmHg", "")
														         .trim()) > ((Double.parseDouble(deviceInfo
																                                         .getDeviceNotificationList()
																                                         .get(0)
																                                         .getGreaterThanReading2()))))) {
											Map<String, String> keyValsysto = new HashMap<String, String>();
											keyValsysto.put("range", deviceInfo.getDeviceNotificationList()
											                                   .get(0)
											                                   .getGreaterThanReading2());
											msg2 = messageNotificationConstants.getUserPreferedLangTranslatedMsg(
													keyValsysto, "diastolic.value.beyond ", userLangPreference, null);
										}

										// //for diastolic

										if (bpMonitor != null
												&& deviceInfo.getDeviceNotificationList()
												             .get(0)
												             .getLessThanReading1() != null
												&& !(Double
												.parseDouble(
														bpMonitor.getSystolicpressureValue()
														         .replace("mmHg", "")
														         .trim()) < (Double.parseDouble(deviceInfo
																                                        .getDeviceNotificationList()
																                                        .get(0)
																                                        .getLessThanReading1())))) {
											Map<String, String> keyValsysto = new HashMap<String, String>();
											keyValsysto.put("range", deviceInfo.getDeviceNotificationList()
											                                   .get(0)
											                                   .getLessThanReading1());
											msg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(
													keyValsysto, "systolic.value.less", userLangPreference, null);
										}
										if (bpMonitor != null
												&& deviceInfo.getDeviceNotificationList()
												             .get(0)
												             .getLessThanReading2() != null
												&& !(Double.parseDouble(bpMonitor.getDiastolicpressureValue()
												                                 .replace("mmHg", "")
												                                 .trim()) < (Double
												.parseDouble(deviceInfo.getDeviceNotificationList()
												                       .get(0)
												                       .getLessThanReading2())))) {
											Map<String, String> keyValsysto = new HashMap<String, String>();
											keyValsysto.put("range", deviceInfo.getDeviceNotificationList()
											                                   .get(0)
											                                   .getLessThanReading2());
											msg2 = messageNotificationConstants.getUserPreferedLangTranslatedMsg(
													keyValsysto, "diastolic.value.less", userLangPreference, null);
										}

										Map<String, String> keyValsysto = new HashMap<String, String>();
										keyValsysto.put("patientName", patientName);
										keyValsysto.put("systolicpressureValue", bpMonitor.getSystolicpressureValue());
										keyValsysto.put("diastolicpressureValue",
										                bpMonitor.getDiastolicpressureValue()
										                         .replace("mmHg", ""));
										keyValsysto.put("msg", msg);
										keyValsysto.put("msg2", msg2);
										keyValsysto.put("fromatedMobileNumber", fromatedMobileNumber);
										keyValsysto.put("callDeeplinkSb", callDeeplinkSb.toString());
										bodyMsg = messageNotificationConstants.getUserPreferedLangTranslatedMsg(
												keyValsysto, "BP.readings.and.readingsare", userLangPreference, null);
									}
								}

							}
							// Scale
							if (deviceInfo.getDevice()
							              .getName()
							              .equalsIgnoreCase(ApplicationConstants.SCALE)) {
								List<Scale> scaleList = scaleService
										.findByPrescribedTime(prescriptionSchedule.getPrescribedTime());
								if (scaleList.size() > 0) {
									Scale scale = scaleList.get(0);
									if (deviceInfo != null && deviceInfo.getDeviceNotificationList() != null
											&& deviceInfo.getDeviceNotificationList()
											             .size() > 0
											&& deviceInfo.getDeviceNotificationList()
											             .get(0) != null) {

										if (deviceInfo.getDeviceNotificationList()
										              .get(0)
										              .getGreaterThanReading1() != null
												&& !(Float.parseFloat(scale.getReading()
												                           .replace("lbs", "")) > (Float
												.parseFloat(deviceInfo.getDeviceNotificationList()
												                      .get(0)
												                      .getGreaterThanReading1())))) {
											msg = " Scale reading has gone lessthan the specified range " + deviceInfo
													.getDeviceNotificationList()
													.get(0)
													.getGreaterThanReading1();
										}
										if (deviceInfo.getDeviceNotificationList()
										              .get(0)
										              .getLessThanReading1() != null
												&& !(Float.parseFloat(scale.getReading()
												                           .replace("lbs", "")) < (Float
												.parseFloat(deviceInfo.getDeviceNotificationList()
												                      .get(0)
												                      .getLessThanReading1())))) {
											msg = " Scale reading has gone beyond the specified range " + deviceInfo
													.getDeviceNotificationList()
													.get(0)
													.getLessThanReading1();
										}
										NumberFormat decimalFormat = new DecimalFormat("#0.00");
										bodyMsg = patientName + " have  taken your Scale readings and reading is: "
												+ decimalFormat.format(
												Double.parseDouble(scale.getReading()
												                        .replace("lbs", "")))
												+ " lbs. " + msg + " contact +" + fromatedMobileNumber + "/" + " "
												+ callDeeplinkSb.toString();
									}
								}
							}
							// HeartRate
							if (deviceInfo.getDevice()
							              .getName()
							              .equalsIgnoreCase(ApplicationConstants.HEART_RATE)) {
								List<HeartRate> heartRateList = heartRateService
										.findByPrescribedTime(prescriptionSchedule.getPrescribedTime());
								if (heartRateList.size() > 0) {
									HeartRate heartRate = heartRateList.get(0);
									if (deviceInfo != null && deviceInfo.getDeviceNotificationList() != null
											&& deviceInfo.getDeviceNotificationList()
											             .size() > 0
											&& deviceInfo.getDeviceNotificationList()
											             .get(0) != null) {
										if (deviceInfo.getDeviceNotificationList()
										              .get(0)
										              .getGreaterThanReading1() != null
												&& !(Float.parseFloat(heartRate.getReadingValue()) > (Float
												.parseFloat(deviceInfo.getDeviceNotificationList()
												                      .get(0)
												                      .getGreaterThanReading1())))) {
											msg = " HeartRate reading has gone lessthan the specified range "
													+ deviceInfo.getDeviceNotificationList()
													            .get(0)
													            .getGreaterThanReading1();
										}
										if (deviceInfo.getDeviceNotificationList()
										              .get(0)
										              .getLessThanReading1() != null
												&& !(Float.parseFloat(heartRate.getReadingValue()) < (Float
												.parseFloat(deviceInfo.getDeviceNotificationList()
												                      .get(0)
												                      .getLessThanReading1())))) {
											msg = " HeartRate reading has gone beyond the specified range " + deviceInfo
													.getDeviceNotificationList()
													.get(0)
													.getLessThanReading1();
										}
										bodyMsg = patientName + " have taken your HeartRate readings and reading is: "
												+ Double.parseDouble(heartRate.getReadingValue()) + msg + " contact +"
												+ fromatedMobileNumber + "/" + " " + callDeeplinkSb.toString();

									}

								}
							}
							// Steps
							if (deviceInfo.getDevice()
							              .getName()
							              .equalsIgnoreCase(ApplicationConstants.STEPS)) {
								List<Steps> stepsList = stepsService
										.findByPrescribedTime(prescriptionSchedule.getPrescribedTime());
								if (stepsList.size() > 0) {
									Steps steps = stepsList.get(0);
									if (deviceInfo != null && deviceInfo.getDeviceNotificationList() != null
											&& deviceInfo.getDeviceNotificationList()
											             .size() > 0
											&& deviceInfo.getDeviceNotificationList()
											             .get(0) != null) {
										if (deviceInfo.getDeviceNotificationList()
										              .get(0)
										              .getGreaterThanReading1() != null
												&& !(steps.getStepCount() > (Integer
												.parseInt(deviceInfo.getDeviceNotificationList()
												                    .get(0)
												                    .getGreaterThanReading1())))) {
											msg = " Step Count has gone lessthan the specified range " + deviceInfo
													.getDeviceNotificationList()
													.get(0)
													.getGreaterThanReading1();
										}
										if (deviceInfo.getDeviceNotificationList()
										              .get(0)
										              .getLessThanReading1() != null
												&& !(steps.getStepCount() < (Integer.parseInt(deviceInfo
														                                              .getDeviceNotificationList()
														                                              .get(0)
														                                              .getLessThanReading1())))) {
											msg = " Step Count has gone beyond the specified range " + deviceInfo
													.getDeviceNotificationList()
													.get(0)
													.getLessThanReading1();
										}
										bodyMsg = patientName + " have taken your Step Count and reading is: "
												+ steps.getStepCount() + "." + msg + " contact +" + fromatedMobileNumber
												+ "/" + " " + callDeeplinkSb.toString();
									}
								}
							}
						}

					} else {
						// for skiped devices
						// Bp Meter

						List<String> consumptionStatusList = new ArrayList<String>();
						consumptionStatusList.add(ApplicationConstants.No_Action);

						if (deviceInfo.getDevice()
						              .getName()
						              .equalsIgnoreCase(ApplicationConstants.BP_METER)) {
							BpMonitorModel bpMonitorModel = new BpMonitorModel();

							bpMonitorModel.setConsumptionStatus(consumptionStatusList);
							bpMonitorModel.setDeviceInfoId(Long.toString(deviceInfo.getDevice()
							                                                       .getId()));
							bpMonitorModel.setDiastolicpressureValue(null);
							bpMonitorModel.setSystolicpressureValue(null);
							bpMonitorModel.setPatient(patientModel);
							bpMonitorModel.setPrescriptionID(prescription.getId());
							bpMonitorModel.setObservedTime(localDateTimeStr);// In
							// UTC
							// time
							bpMonitorModel.setPrescribedTime(
									prescriptionSchedule.getPrescribedTime()
									                    .format(dayamedGeneralFormatter));

							biometricService.addBpReading(bpMonitorModel);
							logger.info("No_Action BP job has been added in imantic DB");
						}
						// Scale
						if (deviceInfo.getDevice()
						              .getName()
						              .equalsIgnoreCase(ApplicationConstants.SCALE)) {
							ScaleModel scaleModel = new ScaleModel();
							scaleModel.setConsumptionStatus(consumptionStatusList);
							scaleModel.setDeviceInfoId(Long.toString(deviceInfo.getDevice()
							                                                   .getId()));
							scaleModel.setObservedTime(localDateTimeStr);
							scaleModel.setPrescribedTime(
									prescriptionSchedule.getPrescribedTime()
									                    .format(dayamedGeneralFormatter));
							scaleModel.setPatient(patientModel);
							scaleModel.setPrescriptionID(prescription.getId());
							scaleModel.setReading("0");

							biometricService.addScale(scaleModel);
							logger.info("No_Action Scale job has been added in imantic DB");
						}
						// HeartRate
						if (deviceInfo.getDevice()
						              .getName()
						              .equalsIgnoreCase(ApplicationConstants.HEART_RATE)) {
							HeartRateModel hrmodel = new HeartRateModel();
							hrmodel.setConsumptionStatus(consumptionStatusList);
							hrmodel.setDeviceInfoId(Long.toString(deviceInfo.getDevice()
							                                                .getId()));
							hrmodel.setObservedTime(localDateTimeStr);
							hrmodel.setPrescribedTime(
									prescriptionSchedule.getPrescribedTime()
									                    .format(dayamedGeneralFormatter));
							hrmodel.setPrescriptionID(prescription.getId());
							hrmodel.setPatient(patientModel);
							hrmodel.setReadingValue("0");

							biometricService.addHeartRate(hrmodel);
							logger.info("No_Action HR job has been added in imantic DB");
						}
						// steps
						if (deviceInfo.getDevice()
						              .getName()
						              .equalsIgnoreCase(ApplicationConstants.STEPS)) {
							StepsModel stepsModel = new StepsModel();
							stepsModel.setConsumptionStatus(consumptionStatusList);
							stepsModel.setDeviceInfoId(Long.toString(deviceInfo.getDevice()
							                                                   .getId()));
							stepsModel.setObservedTime(localDateTimeStr);// in
							// UTC
							// time
							stepsModel.setPrescribedTime(
									prescriptionSchedule.getPrescribedTime()
									                    .format(dayamedGeneralFormatter));
							stepsModel.setPrescriptionID(prescription.getId());
							stepsModel.setStepCount(0);
							stepsModel.setPatient(patientModel);

							biometricService.addSteps(stepsModel);
							logger.info("No_Action Steps job has been added in imantic DB");
						}
					}

					String deviceStatusCommaSeparated = "Metrics taken";

					// Sending Eamil and Sms to respective providers
					List<String> recipienprovidertList = new ArrayList<String>();
					// sending sms to provider
					for (Provider provider : patient.getProviders()) {
						recipienprovidertList.add(provider.getUserDetails()
						                                  .getEmailId());
						emailAndSmsUtility.sendSms(bodyMsg, provider.getUserDetails()
						                                            .getCountryCode(),
						                           provider.getUserDetails()
						                                   .getMobileNumber());
					}
					emailAndSmsUtility.sendEmail("Device Status", bodyMsg, recipienprovidertList);
					// Sending Eamil and Sms to respective caregivers
					Set<Caregiver> caregiverSet = patient.getCaregivers();
					List<String> caregiverMailList = new ArrayList<>();
					for (Caregiver caregiver : caregiverSet) {
						caregiverMailList.add(caregiver.getUserDetails()
						                               .getEmailId());
						emailAndSmsUtility.sendSms(bodyMsg, caregiver.getUserDetails()
						                                             .getCountryCode(),
						                           caregiver.getUserDetails()
						                                    .getMobileNumber());
					}
					if (caregiverMailList.size() > 0) {
						emailAndSmsUtility.sendEmail("Device Status", bodyMsg, caregiverMailList);
					}
					String title = deviceStatusCommaSeparated.split(",")[0];
					String type = "device_status";
					// only showing not taken metrics events in calender
					if (prescriptionSchedule.getConsumptionStatus() == null) {
						// sending notifications Notification Table.
						if (patient != null) {
							String notificationMsg = "Not taken any action on "
									+ prescriptionSchedule.getDeviceInfo()
									                      .getDevice()
									                      .getName();

							bodyMsg = notificationMsg;

							title = "Metrics skipped";
							type = ApplicationConstants.DeviceNoAction;
						}
						notificationService.addWebNotification(patientUserdetails.getId(), bodyMsg, title, type);
					}
					prescriptionScheduleRepository.delete(prescriptionSchedule);
				}
			}
		} catch (Exception e) {
			logger.error("Problem while loading prescriptionSchedule");
		}
	}

	@Autowired
	PaitentAdherenceWeeklyPDFReport paitentAdherenceWeeklyPDFReport;

	@Scheduled(cron = "00 00 12 * * SUN")
	public void scheduleWeeklyPatientAdherence() {
		try {
			paitentAdherenceWeeklyPDFReport.getPatientRecoreds();
		} catch (Exception e) {
			logger.error("Exception occurred when scheduling weekly patient adherence", e);
		}
	}

	@Autowired
	GoodShapredAlertProtocal goodShapredAlertProtocal;

	// @Scheduled(cron = "*/59 * * * * *")
	/*
	 * public void goodShepardAlertProtocolNotification() {
	 * goodShapredAlertProtocal.sendMissedAdherenceNotification(); }
	 */

	@Autowired
	WeeklyEmailNotificationToPatient weeklyEmailNotificationToPatient;

	@Scheduled(cron = "00 00 6 * * SUN")
	public void weeklyEmailNotificationToPatient() {
		try {
			weeklyEmailNotificationToPatient
					.sendNotificationToPatient(ApplicationConstants.PATIENT_MISSED_NOTIIFCATION_TYPE_WEEKLY, null);
		} catch (Exception e) {
			logger.error("Exception occurred when sending weekly email to patient", e);
		}
	}

	@Scheduled(cron = "0 0 0 * * *")
	public void dailyEmailNotificationToPatient() {
		try {
			weeklyEmailNotificationToPatient
					.sendNotificationToPatient(ApplicationConstants.PATIENT_MISSED_NOTIIFCATION_TYPE_DAILY, null);
		} catch (Exception e) {
			logger.error("Exception occurred when sending daily email to patient", e);
		}
	}
}
