package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NDCImageModel {

	private String imagePath;

	private String urlPathInDB;

}
