package com.sg.dayamed.pojo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ScaleModel {

	private long id;

	private String reading;

	private String observedTime;

	private PatientModel patient;

	private long prescriptionID;

	private String deviceInfoId;

	private ArrayList<String> location;

	private String prescribedTime;

	//private String consumptionStatus;
	private List<String> consumptionStatus = new LinkedList<String>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

	public String getDeviceInfoId() {
		return deviceInfoId;
	}

	public void setDeviceInfoId(String deviceInfoId) {
		this.deviceInfoId = deviceInfoId;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public String getReading() {
		return reading;
	}

	public void setReading(String reading) {
		this.reading = reading;
	}

	public PatientModel getPatient() {
		return patient;
	}

	public void setPatient(PatientModel patient) {
		this.patient = patient;
	}

	public String getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(String observedTime) {
		this.observedTime = observedTime;
	}

	public String getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(String prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

}
