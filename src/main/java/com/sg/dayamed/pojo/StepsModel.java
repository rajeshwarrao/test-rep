package com.sg.dayamed.pojo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class StepsModel {

	private long id;

	private int stepCount;

	private PatientModel patient;

	private String prescribedTime;

	private String observedTime;

	//private String consumptionStatus;
	private List<String> consumptionStatus = new LinkedList<String>();

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	private ArrayList<String> location;

	private long prescriptionID;

	private String deviceInfoId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getStepCount() {
		return stepCount;
	}

	public void setStepCount(int stepCount) {
		this.stepCount = stepCount;
	}

	public PatientModel getPatient() {
		return patient;
	}

	public void setPatient(PatientModel patient) {
		this.patient = patient;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public String getDeviceInfoId() {
		return deviceInfoId;
	}

	public void setDeviceInfoId(String deviceInfoId) {
		this.deviceInfoId = deviceInfoId;
	}

	public String getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(String prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	public String getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(String observedTime) {
		this.observedTime = observedTime;
	}

}
