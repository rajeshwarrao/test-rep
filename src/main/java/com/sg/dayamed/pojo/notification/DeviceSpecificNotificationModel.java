package com.sg.dayamed.pojo.notification;

public class DeviceSpecificNotificationModel {

	private long id;

	private String expectedTime;

	private String notificationType;

	private String notificationValue;

	private String countryCode;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getExpectedTime() {
		return expectedTime;
	}

	public void setExpectedTime(String expectedTime) {
		this.expectedTime = expectedTime;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getNotificationValue() {
		return notificationValue;
	}

	public void setNotificationValue(String notificationValue) {
		this.notificationValue = notificationValue;
	}

}
