package com.sg.dayamed.pojo.notification;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FCMNotificationRequestModel {

	private String to;

	private FCMNotificationDataRequestModel data;
}
