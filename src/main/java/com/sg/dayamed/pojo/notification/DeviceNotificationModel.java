package com.sg.dayamed.pojo.notification;

import java.util.List;

public class DeviceNotificationModel {

	private GreaterThanNotificationModel greaterThan;

	private LessThanNotificationModel lessThan;

	private List<DeviceSpecificNotificationModel> notificationList;

	public GreaterThanNotificationModel getGreaterThan() {
		return greaterThan;
	}

	public void setGreaterThan(GreaterThanNotificationModel greaterThan) {
		this.greaterThan = greaterThan;
	}

	public LessThanNotificationModel getLessThan() {
		return lessThan;
	}

	public void setLessThan(LessThanNotificationModel lessThan) {
		this.lessThan = lessThan;
	}

	public List<DeviceSpecificNotificationModel> getNotificationList() {
		return notificationList;
	}

	public void setNotificationList(List<DeviceSpecificNotificationModel> notificationList) {
		this.notificationList = notificationList;
	}

}
	
