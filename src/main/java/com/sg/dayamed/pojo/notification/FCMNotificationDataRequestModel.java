package com.sg.dayamed.pojo.notification;

import com.sg.dayamed.pojo.VidoeCallSignatureModel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FCMNotificationDataRequestModel {

	private String title;

	private String message;

	private String callerName;

	private String roomName;

	private VidoeCallSignatureModel vidoeCallSignatureModel;
}
