package com.sg.dayamed.pojo.adherence;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class DeviceAdheranceDataModel {

	private ArrayList<String> location;

	private String reading1;

	private String reading2;

	private Date observedTime;

	//private String consumptionStatus;
	private List<String> consumptionStatus = new LinkedList<String>();

	private Date prescribedTime;

	private String deviceName;

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public String getReading1() {
		return reading1;
	}

	public void setReading1(String reading1) {
		this.reading1 = reading1;
	}

	public String getReading2() {
		return reading2;
	}

	public void setReading2(String reading2) {
		this.reading2 = reading2;
	}

	public Date getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(Date observedTime) {
		this.observedTime = observedTime;
	}
	
	/*public String getPrescribedTime() {
		return prescribedTime;
	}
	public void setPrescribedTime(String prescribedTime) {
		this.prescribedTime = prescribedTime;
	}*/

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Date getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(Date prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

}
