package com.sg.dayamed.pojo.adherence;

import com.sg.dayamed.pojo.AdherenceDataPointsModel;

import java.util.List;

public class MedAndDeviceAdheranceDataModel {

	private List<AdherenceDataPointsModel> medicineAdherenceList;

	private List<DeviceAdheranceDataModel> deviceAdherenceList;

	public List<AdherenceDataPointsModel> getMedicineAdherenceList() {
		return medicineAdherenceList;
	}

	public void setMedicineAdherenceList(List<AdherenceDataPointsModel> medicineAdherenceList) {
		this.medicineAdherenceList = medicineAdherenceList;
	}

	public List<DeviceAdheranceDataModel> getDeviceAdherenceList() {
		return deviceAdherenceList;
	}

	public void setDeviceAdherenceList(List<DeviceAdheranceDataModel> deviceAdherenceList) {
		this.deviceAdherenceList = deviceAdherenceList;
	}

}
