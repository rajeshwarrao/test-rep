package com.sg.dayamed.pojo.adherence;

public class BPAdherenceFromExcelModel {

	private String systolicPressure;

	private String systolicPressureProjected;

	private String diastolicpressure;

	private String diastolicpressureProjected;

	private String currentAdherence;

	private String projectedAdherence;

	private String alert;

	public String getSystolicPressure() {
		return systolicPressure;
	}

	public void setSystolicPressure(String systolicPressure) {
		this.systolicPressure = systolicPressure;
	}

	public String getSystolicPressureProjected() {
		return systolicPressureProjected;
	}

	public void setSystolicPressureProjected(String systolicPressureProjected) {
		this.systolicPressureProjected = systolicPressureProjected;
	}

	public String getDiastolicpressure() {
		return diastolicpressure;
	}

	public void setDiastolicpressure(String diastolicpressure) {
		this.diastolicpressure = diastolicpressure;
	}

	public String getDiastolicpressureProjected() {
		return diastolicpressureProjected;
	}

	public void setDiastolicpressureProjected(String diastolicpressureProjected) {
		this.diastolicpressureProjected = diastolicpressureProjected;
	}

	public String getCurrentAdherence() {
		return currentAdherence;
	}

	public void setCurrentAdherence(String currentAdherence) {
		this.currentAdherence = currentAdherence;
	}

	public String getProjectedAdherence() {
		return projectedAdherence;
	}

	public void setProjectedAdherence(String projectedAdherence) {
		this.projectedAdherence = projectedAdherence;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

}
