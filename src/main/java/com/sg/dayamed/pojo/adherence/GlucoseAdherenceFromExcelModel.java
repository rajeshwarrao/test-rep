package com.sg.dayamed.pojo.adherence;

public class GlucoseAdherenceFromExcelModel {

	private String glucose;

	private String glucoseP;

	private String currentAdherence;

	private String projectedAdherence;

	private String alert;

	public String getGlucose() {
		return glucose;
	}

	public void setGlucose(String glucose) {
		this.glucose = glucose;
	}

	public String getGlucoseP() {
		return glucoseP;
	}

	public void setGlucoseP(String glucoseP) {
		this.glucoseP = glucoseP;
	}

	public String getCurrentAdherence() {
		return currentAdherence;
	}

	public void setCurrentAdherence(String currentAdherence) {
		this.currentAdherence = currentAdherence;
	}

	public String getProjectedAdherence() {
		return projectedAdherence;
	}

	public void setProjectedAdherence(String projectedAdherence) {
		this.projectedAdherence = projectedAdherence;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

}
