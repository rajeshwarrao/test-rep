package com.sg.dayamed.pojo;

public class DayamedException extends SuperClass {

	private String error;

	public DayamedException() {
	}

	public DayamedException(String error) {
		super();
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
