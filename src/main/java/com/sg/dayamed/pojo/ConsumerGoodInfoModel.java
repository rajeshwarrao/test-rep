package com.sg.dayamed.pojo;

import java.util.List;

public class ConsumerGoodInfoModel {

	private long id;

	private String Comment;

	//private int qunatity;
	private List<String> time;

	private ConsumerGoodsModel commodity;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public ConsumerGoodsModel getCommodity() {
		return commodity;
	}

	public void setCommodity(ConsumerGoodsModel commodity) {
		this.commodity = commodity;
	}

	public List<String> getTime() {
		return time;
	}

	public void setTime(List<String> time) {
		this.time = time;
	}

}
