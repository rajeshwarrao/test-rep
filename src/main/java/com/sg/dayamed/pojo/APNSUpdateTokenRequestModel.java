package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class APNSUpdateTokenRequestModel {

	@NotBlank(message = "{apns.update.token.emailId.is.required}")
	@JsonProperty("emailid")
	private String emailId;

	@NotBlank(message = "{apns.update.token.token.is.required}")
	@JsonProperty("token")
	private String token;

	@NotBlank(message = "{apns.update.token.typeOfDevice.is.required}")
	@JsonProperty("typeofdevice")
	private String typeOfDevice;

	@JsonProperty("voiptoken")
	private String voIpToken;
}
