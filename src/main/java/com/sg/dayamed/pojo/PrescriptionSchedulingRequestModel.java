package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class PrescriptionSchedulingRequestModel {

	@NotBlank(message = "{prescription.scheduling.zoneId.is.required}")
	@JsonProperty("zoneid")
	private String zoneId;

	@NotBlank(message = "{prescription.scheduling.prescriptionId.is.required}")
	@JsonProperty("prescriptionid")
	private Long prescriptionId;

	@NotBlank(message = "{prescription.scheduling.flag.is.required}")
	@JsonProperty("flag")
	private Integer flag;

}
