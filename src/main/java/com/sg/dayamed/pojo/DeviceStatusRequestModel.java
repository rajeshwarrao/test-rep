package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class DeviceStatusRequestModel {

	private String latitude;

	private String longitude;

	@JsonProperty("msg")
	private String message;

	private String status;

	@JsonProperty("batterystatus")
	private String batteryStatus;

	@JsonProperty("belonginguserid")
	@NotBlank(message = "{device.status.belonging.userId.required}")
	private Long belongingUserId;
}
