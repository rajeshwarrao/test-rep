package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdherenceReportModel {

	private String providerName;

	private String patientName;

	private String patientEmailAddress;

	private Long patientId;

	private Long prescriptionId;

	private String observedTime;

	private String eventTime;

	// adherence action (none, skipped, delayed, etc.)
	private String adherenceAction;

	// adherence action type (scan, button, tap, etc.)
	private String adherenceActionType;

	private String medicationName;

	private String medicationQuantity;

	private String dosageTimeOfTheDay;

	private String zone;

	private String dosageOfDay;
}
