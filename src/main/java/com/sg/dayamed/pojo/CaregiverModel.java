package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CaregiverModel extends UserInfo {

	private String typeofDevice;

	private boolean adminflag;

	private Set<ProviderModel> providers;
}
