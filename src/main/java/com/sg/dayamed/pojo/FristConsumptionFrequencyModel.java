package com.sg.dayamed.pojo;

public class FristConsumptionFrequencyModel {

	private Long id;

	private Long frequencyId;

	private String frequencyName;

	private String consumptionsKey;

	private String consumptionsDesc;

	private Long consumptionsId;

	private String consumptionsTimes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFrequencyId() {
		return frequencyId;
	}

	public void setFrequencyId(Long frequencyId) {
		this.frequencyId = frequencyId;
	}

	public String getFrequencyName() {
		return frequencyName;
	}

	public void setFrequencyName(String frequencyName) {
		this.frequencyName = frequencyName;
	}

	public String getConsumptionsKey() {
		return consumptionsKey;
	}

	public void setConsumptionsKey(String consumptionsKey) {
		this.consumptionsKey = consumptionsKey;
	}

	public String getConsumptionsDesc() {
		return consumptionsDesc;
	}

	public void setConsumptionsDesc(String consumptionsDesc) {
		this.consumptionsDesc = consumptionsDesc;
	}

	public Long getConsumptionsId() {
		return consumptionsId;
	}

	public void setConsumptionsId(Long consumptionsId) {
		this.consumptionsId = consumptionsId;
	}

	public String getConsumptionsTimes() {
		return consumptionsTimes;
	}

	public void setConsumptionsTimes(String consumptionsTimes) {
		this.consumptionsTimes = consumptionsTimes;
	}

}
