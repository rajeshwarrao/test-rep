package com.sg.dayamed.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PatientModel extends UserInfo {

	private String deviceId;

	private List<PrescriptionModel> prescriptions = new ArrayList<PrescriptionModel>();

	private Set<CaregiverModel> caregiverList;

	private String predictedAdherence;

	private String currentAdherence;

	private String projectedAdherence;

	private String emoji;

	private Set<CaregiverModel> caregivers;

	private Set<ProviderModel> providers;

	private Set<PharmacistModel> pharmacists;

	private boolean smsFlag;

	private boolean emailFlag;

	public boolean isSmsFlag() {
		return smsFlag;
	}

	public void setSmsFlag(boolean smsFlag) {
		this.smsFlag = smsFlag;
	}

	public boolean isEmailFlag() {
		return emailFlag;
	}

	public void setEmailFlag(boolean emailFlag) {
		this.emailFlag = emailFlag;
	}

	public Set<CaregiverModel> getCaregiverList() {
		return caregiverList;
	}

	public void setCaregiverList(Set<CaregiverModel> caregiverList) {
		this.caregiverList = caregiverList;
	}

	public String getPredictedAdherence() {
		return predictedAdherence;
	}

	public void setPredictedAdherence(String predictedAdherence) {
		this.predictedAdherence = predictedAdherence;
	}

	public String getCurrentAdherence() {
		return currentAdherence;
	}

	public void setCurrentAdherence(String currentAdherence) {
		this.currentAdherence = currentAdherence;
	}

	public String getProjectedAdherence() {
		return projectedAdherence;
	}

	public void setProjectedAdherence(String projectedAdherence) {
		this.projectedAdherence = projectedAdherence;
	}

	public Set<ProviderModel> getProviders() {
		return providers;
	}

	public void setProviders(Set<ProviderModel> providers) {
		this.providers = providers;
	}

	public Set<PharmacistModel> getPharmacists() {
		return pharmacists;
	}

	public void setPharmacists(Set<PharmacistModel> pharmacists) {
		this.pharmacists = pharmacists;
	}

	public Set<CaregiverModel> getCaregivers() {
		return caregivers;
	}

	public void setCaregivers(Set<CaregiverModel> caregivers) {
		this.caregivers = caregivers;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public List<PrescriptionModel> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(List<PrescriptionModel> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public String getEmoji() {
		return emoji;
	}

	public void setEmoji(String emoji) {
		this.emoji = emoji;
	}

}
