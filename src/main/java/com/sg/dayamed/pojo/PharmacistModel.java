package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class PharmacistModel extends UserInfo {

	private String companyName;
}
