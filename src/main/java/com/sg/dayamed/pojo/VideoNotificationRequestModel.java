package com.sg.dayamed.pojo;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VideoNotificationRequestModel {

	@NotBlank(message = "{video.notification.roomName.is.required}")
	private String roomName;

	@JsonProperty("belongingid")
	private Long belongingId;

	@NotBlank(message = "{video.notification.userId.is.required}")
	@JsonProperty("userid")
	private Long userId;

}
