package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsumptionResponseModel {

	private String description;

	private String name;

	private Long id;

}
