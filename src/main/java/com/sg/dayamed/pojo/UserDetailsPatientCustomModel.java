package com.sg.dayamed.pojo;

public class UserDetailsPatientCustomModel {

	private UserDetailsModel userDetails;

	public UserDetailsModel getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetailsModel userDetails) {
		this.userDetails = userDetails;
	}

	public PrescriptionModel getPrescription() {
		return Prescription;
	}

	public void setPrescription(PrescriptionModel prescription) {
		Prescription = prescription;
	}

	private PrescriptionModel Prescription;
}
