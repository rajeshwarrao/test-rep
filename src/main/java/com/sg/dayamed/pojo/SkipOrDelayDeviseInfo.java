package com.sg.dayamed.pojo;

import java.util.ArrayList;
import java.util.List;

public class SkipOrDelayDeviseInfo {

	private long id;

	private String reading1;

	private String reading2;

	private String observedTime;

	private String prescribedTime;

	private PatientModel patient;

	private long prescriptionID;

	private List<DeviceInfoModel> deviceInfoList;

	private String consumptionStatus;

	private ArrayList<String> location;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getReading1() {
		return reading1;
	}

	public void setReading1(String reading1) {
		this.reading1 = reading1;
	}

	public String getReading2() {
		return reading2;
	}

	public void setReading2(String reading2) {
		this.reading2 = reading2;
	}

	public String getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(String observedTime) {
		this.observedTime = observedTime;
	}

	public String getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(String prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	public PatientModel getPatient() {
		return patient;
	}

	public void setPatient(PatientModel patient) {
		this.patient = patient;
	}

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

	public List<DeviceInfoModel> getDeviceInfoList() {
		return deviceInfoList;
	}

	public void setDeviceInfoList(List<DeviceInfoModel> deviceInfoList) {
		this.deviceInfoList = deviceInfoList;
	}

	public String getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(String consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

}

