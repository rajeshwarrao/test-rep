package com.sg.dayamed.pojo;

import com.sg.dayamed.pojo.notification.DeviceNotificationModel;

import java.util.List;

public class DeviceInfoModel {

	private long id;

	private String Comment;

	private List<String> time;

	//private String time;
	private DeviceModel device;

	private int duration;

	private String status;

	private String serialNo;

	private DeviceNotificationModel deviceNotification;
	//private PrescriptionModel Prescription;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public DeviceModel getDevice() {
		return device;
	}

	public void setDevice(DeviceModel device) {
		this.device = device;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public DeviceNotificationModel getDeviceNotification() {
		return deviceNotification;
	}

	public void setDeviceNotification(DeviceNotificationModel deviceNotification) {
		this.deviceNotification = deviceNotification;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public List<String> getTime() {
		return time;
	}

	public void setTime(List<String> time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

}
