package com.sg.dayamed.pojo;

import java.util.ArrayList;
import java.util.Date;

public class TriggerJobsModel {

	private long id;

	private Date actualTime;

	private Date fireTime;

	private ArrayList<String> consumptionStatus;

	private String notificationType;

	private String notificationValue;

	private DosageInfoModel dosageInfo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getActualTime() {
		return actualTime;
	}

	public void setActualTime(Date actualTime) {
		this.actualTime = actualTime;
	}

	public Date getFireTime() {
		return fireTime;
	}

	public void setFireTime(Date fireTime) {
		this.fireTime = fireTime;
	}

	public ArrayList<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(ArrayList<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getNotificationValue() {
		return notificationValue;
	}

	public void setNotificationValue(String notificationValue) {
		this.notificationValue = notificationValue;
	}

	public DosageInfoModel getDosageInfo() {
		return dosageInfo;
	}

	public void setDosageInfo(DosageInfoModel dosageInfo) {
		this.dosageInfo = dosageInfo;
	}

}
