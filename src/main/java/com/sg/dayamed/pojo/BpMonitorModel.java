package com.sg.dayamed.pojo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BpMonitorModel {

	private long id;

	private String systolicpressureValue;

	private String diastolicpressureValue;

	private String observedTime;

	private String prescribedTime;

	private PatientModel patient;

	private long prescriptionID;

	private String deviceInfoId;

	private List<String> consumptionStatus = new LinkedList<String>();

	private ArrayList<String> location;

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

	public String getDeviceInfoId() {
		return deviceInfoId;
	}

	public void setDeviceInfoId(String deviceInfoId) {
		this.deviceInfoId = deviceInfoId;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PatientModel getPatient() {
		return patient;
	}

	public void setPatient(PatientModel patient) {
		this.patient = patient;
	}

	public String getDiastolicpressureValue() {
		return diastolicpressureValue;
	}

	public void setDiastolicpressureValue(String diastolicpressureValue) {
		this.diastolicpressureValue = diastolicpressureValue;
	}

	public String getSystolicpressureValue() {
		return systolicpressureValue;
	}

	public void setSystolicpressureValue(String systolicpressureValue) {
		this.systolicpressureValue = systolicpressureValue;
	}

	public String getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(String prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

	public String getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(String observedTime) {
		this.observedTime = observedTime;
	}

}
