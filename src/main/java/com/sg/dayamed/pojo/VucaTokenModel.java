package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VucaTokenModel {

	@JsonProperty("BaseUrl")
	private String baseUrl;

	@JsonProperty("ExpirationDateTime_UTC")
	private String expirationDateTime_UTC;

	@JsonProperty("StatusCode")
	private String statusCode;

	@JsonProperty("StatusMessage")
	private String statusMessage;
}
