package com.sg.dayamed.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class AdherenceDataPointsModel {

	private long id;

	private ArrayList<String> location;

	private HashSet<String> how;

	private Date observedTime;

	private Date prescribedTime;

	//private LocalDateTime prescribedTime;
	private List<String> consumptionStatus = new ArrayList<String>();

	//private String consumptionStatus;
	private long prescriptionID;

	private List<DosageInfoModel> dosageInfoList;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public long getPrescriptionID() {
		return prescriptionID;
	}

	public void setPrescriptionID(long prescriptionID) {
		this.prescriptionID = prescriptionID;
	}

	public List<DosageInfoModel> getDosageInfoList() {
		return dosageInfoList;
	}

	public void setDosageInfoList(List<DosageInfoModel> dosageInfoList) {
		this.dosageInfoList = dosageInfoList;
	}

	public HashSet<String> getHow() {
		return how;
	}

	public List<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(List<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public void setHow(HashSet<String> how) {
		this.how = how;
	}

	public Date getObservedTime() {
		return observedTime;
	}

	public void setObservedTime(Date observedTime) {
		this.observedTime = observedTime;
	}

	public Date getPrescribedTime() {
		return prescribedTime;
	}

	public void setPrescribedTime(Date prescribedTime) {
		this.prescribedTime = prescribedTime;
	}

}
