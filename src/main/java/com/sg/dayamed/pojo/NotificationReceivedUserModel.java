package com.sg.dayamed.pojo;

public class NotificationReceivedUserModel {

	private long id;

	private UserDetailsModel userDetails;

	private NotificationModel notification;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserDetailsModel getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetailsModel userDetails) {
		this.userDetails = userDetails;
	}

	public NotificationModel getNotification() {
		return notification;
	}

	public void setNotification(NotificationModel notification) {
		this.notification = notification;
	}

}
