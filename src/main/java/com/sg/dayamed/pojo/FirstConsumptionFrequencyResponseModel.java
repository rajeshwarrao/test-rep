package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FirstConsumptionFrequencyResponseModel {
	private Long id;
	private String name;
}
