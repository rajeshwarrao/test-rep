package com.sg.dayamed.pojo;

import java.util.List;

public class BpMonitorSuperJsonModel {

	private String name;

	private List<BpMonitorJsonModel> series;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BpMonitorJsonModel> getSeries() {
		return series;
	}

	public void setSeries(List<BpMonitorJsonModel> series) {
		this.series = series;
	}
}
