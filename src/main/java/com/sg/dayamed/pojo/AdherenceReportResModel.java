package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class AdherenceReportResModel {

	private String status;

	private String message;

	private Map<String, String> adherenceReportHeaders;

	private List<AdherenceReportModel> adherenceReportModelList;
}
