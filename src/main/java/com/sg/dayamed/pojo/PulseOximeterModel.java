package com.sg.dayamed.pojo;

import java.time.LocalDateTime;

public class PulseOximeterModel {

	private long id;

	private String sp02;

	private int pulse;

	private LocalDateTime takenTime;

	private PatientModel patient;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSp02() {
		return sp02;
	}

	public void setSp02(String sp02) {
		this.sp02 = sp02;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public LocalDateTime getTakenTime() {
		return takenTime;
	}

	public void setTakenTime(LocalDateTime takenTime) {
		this.takenTime = takenTime;
	}

	public PatientModel getPatient() {
		return patient;
	}

	public void setPatient(PatientModel patient) {
		this.patient = patient;
	}

}
