package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class PasswordChangeReqModel {

	@NotBlank(message = "{errors.invalid.current.password}")
	private String currentPassword;

	@NotBlank(message = "{errors.invalid.new.password}")
	private String newPassword;

}
