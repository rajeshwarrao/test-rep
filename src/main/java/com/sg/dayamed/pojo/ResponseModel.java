package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@Getter
@Setter
@NoArgsConstructor
public class ResponseModel {

	private String status;

	private String message;
}
