package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FristConsumptionTypeModel {

	FrequencyPerDayModel frequencyPerDay;

	FirstConsumptionTimeModel firstConsumptionTime;
}
