package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class VidoeCallSignatureModel implements Serializable {

	private static final long serialVersionUID = -7890805173537851683L;

	private String meetingNumber;

	private String leaveUrl;

	private int role;

	private String apiKey;

	private String signature;

	private String passwd;

	private String apiSercet;

	private String userEmail;

	private long time;

	private String userName;

	private String email;

	private String zoomJWTToken;

	private String zoomAccessToken;

	private String zoomUserId;

	private String callType;

	private String recevierUserID;

	private String join_url;

	private String zoomMeetingResponse;

	private String pstn_password;

	private String password;

	private String h323_password;

	private String encrypted_password;

	private String host_id;

	private String uuid;

}
