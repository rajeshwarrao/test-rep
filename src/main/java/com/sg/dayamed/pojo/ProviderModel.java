package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ProviderModel extends UserInfo {

	private String specialization;

	private String licence;

	@JsonProperty
	private boolean isPharmacist;

	private Set<PatientModel> patients;
}
