package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ConsumptionFrequencyResponseModel {

	private List<FirstConsumptionFrequencyResponseModel> frequencies;
}
