package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
public class NotificationRequestModel {

	@NotBlank(message = "{notification.request.userIds.is.required}")
	@JsonProperty("userids")
	private List<Long> userIds;

	@NotBlank(message = "{notification.request.userIds.is.message}")
	private String message;

	@NotBlank(message = "{notification.request.userIds.is.type}")
	private String type;

	@NotBlank(message = "{notification.request.userIds.is.belongingUserId}")
	@JsonProperty("belonginguserid")
	private Long belongingUserId;

}
