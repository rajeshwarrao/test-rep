package com.sg.dayamed.pojo;

import com.sg.dayamed.entity.DosageInfo;

import java.util.ArrayList;
import java.util.List;

public class SchedlerJobsModel {

	private long id;

	private ArrayList<String> actualIntakeTime;

	private ArrayList<String> fireTime;

	private ArrayList<String> consumptionStatus;

	private String notificationType;

	private String notificationValue;

	private List<DosageInfo> dosageInfoList;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNotificationValue() {
		return notificationValue;
	}

	public void setNotificationValue(String notificationValue) {
		this.notificationValue = notificationValue;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public ArrayList<String> getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(ArrayList<String> consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

	public ArrayList<String> getActualIntakeTime() {
		return actualIntakeTime;
	}

	public void setActualIntakeTime(ArrayList<String> actualIntakeTime) {
		this.actualIntakeTime = actualIntakeTime;
	}

	public ArrayList<String> getFireTime() {
		return fireTime;
	}

	public void setFireTime(ArrayList<String> fireTime) {
		this.fireTime = fireTime;
	}

}
