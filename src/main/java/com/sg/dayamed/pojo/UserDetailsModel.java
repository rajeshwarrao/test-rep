package com.sg.dayamed.pojo;

import com.sg.dayamed.util.enums.UserRoleEnum;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

@Getter
@Setter
public class UserDetailsModel implements Serializable {

	private static final long serialVersionUID = 4989445561656957204L;

	private long id;

	private String firstName;

	private String lastName;

	private String middleName;

	private String mobileNumber;

	private String countryCode;

	private String emailId;

	private String gender;

	private LocalDate dateOfBirth;

	private String age;

	private String imageUrl;

	private AddressModel address;

	private RoleModel role;

	private PatientModel patient;

	private CaregiverModel caregiver;

	private PharmacistModel pharmasist;

	private String type;

	private String token;

	private String race;

	private String imanticUserid;

	private String password;

	private String voipToken;

	private String appVersion;

	private String osVersion;

	private Date lastLoginTime;

	private Date lastActiveTime;

	private String userZoneId;

	private UserPreferenceModel userPreferenceModel;

	private boolean emailFlag;

	private boolean smsFlag;

	public Boolean validDateOfBirth(UserRoleEnum userRoleEnum) {
		if (this.dateOfBirth != null) {
			Period period = Period.between(this.dateOfBirth, LocalDate.now());
			this.age = String.valueOf(period.getYears());
		}
		if (UserRoleEnum.PATIENT.equals(userRoleEnum)) {
			return !(this.dateOfBirth != null && (this.dateOfBirth.isBefore(LocalDate.now()) || this.dateOfBirth.isEqual(LocalDate.now())));
		}
		return this.dateOfBirth != null ? this.dateOfBirth.isAfter(LocalDate.now()) : false;
	}
}
