package com.sg.dayamed.pojo;

import java.util.List;

public class BiometricSuperJsonModel {

	private String name;

	private List<BiometricJsonModel> series;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BiometricJsonModel> getSeries() {
		return series;
	}

	public void setSeries(List<BiometricJsonModel> series) {
		this.series = series;
	}
}
