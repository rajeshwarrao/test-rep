package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserInfo {

	private long id;

	private UserDetailsModel userDetails;
}
