package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicineModel {

	private long id;

	private String name;

	private String type;

	private String description;

	private String category;

	private String imageURL;

	private Long ndcCode;

	private String ndcType;

	private Long genCode;

	private String packager;

	private boolean vucaVideoAvailable;

}
