package com.sg.dayamed.pojo;

import com.sg.dayamed.util.ApplicationConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserPreferenceModel {

	private String languagePreference = ApplicationConstants.LANG_PREF_DEFAULT;

	private String firstConsumption;

	private Boolean smsFlag = false;

	private Boolean emailFlag = false;

	private Boolean pushNotificationFlag = true;

}
