package com.sg.dayamed.pojo;

import java.util.List;
import java.util.Set;

public class CalendarNotificationModel {

	private List<CalenderEventModel> events;

	private UserDetailsModel userDetails;

	private Set<CaregiverModel> caregivers;

	private Set<ProviderModel> providers;

	public List<CalenderEventModel> getEvents() {
		return events;
	}

	public void setEvents(List<CalenderEventModel> events) {
		this.events = events;
	}

	public UserDetailsModel getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetailsModel userDetails) {
		this.userDetails = userDetails;
	}

	public Set<CaregiverModel> getCaregivers() {
		return caregivers;
	}

	public void setCaregivers(Set<CaregiverModel> caregivers) {
		this.caregivers = caregivers;
	}

	public Set<ProviderModel> getProviders() {
		return providers;
	}

	public void setProviders(Set<ProviderModel> providers) {
		this.providers = providers;
	}

}
