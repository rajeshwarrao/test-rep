package com.sg.dayamed.pojo;

public class Adherence_DosageInfoMapModel {

	private long id;

	private AdherenceDataPointsModel adherenceId;

	private DosageInfoModel dosageinfoId;

	private String consumptionStatus;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public AdherenceDataPointsModel getAdherenceId() {
		return adherenceId;
	}

	public void setAdherenceId(AdherenceDataPointsModel adherenceId) {
		this.adherenceId = adherenceId;
	}

	public DosageInfoModel getDosageinfoId() {
		return dosageinfoId;
	}

	public void setDosageinfoId(DosageInfoModel dosageinfoId) {
		this.dosageinfoId = dosageinfoId;
	}

	public String getConsumptionStatus() {
		return consumptionStatus;
	}

	public void setConsumptionStatus(String consumptionStatus) {
		this.consumptionStatus = consumptionStatus;
	}

}
