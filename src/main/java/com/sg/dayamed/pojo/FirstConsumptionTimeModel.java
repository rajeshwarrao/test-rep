package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FirstConsumptionTimeModel {

	String name;

	String description;

	String times;

	long id;
}
