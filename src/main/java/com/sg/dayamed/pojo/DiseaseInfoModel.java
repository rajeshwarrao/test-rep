package com.sg.dayamed.pojo;

public class DiseaseInfoModel {

	private long id;

	private DiseaseModel disease;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public DiseaseModel getDisease() {
		return disease;
	}

	public void setDisease(DiseaseModel disease) {
		this.disease = disease;
	}

}
