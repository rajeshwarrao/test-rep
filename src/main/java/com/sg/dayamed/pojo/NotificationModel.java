package com.sg.dayamed.pojo;

import java.util.Date;
import java.util.List;

public class NotificationModel {

	private long id;

	private String message;

	// private UserDetailsModel userDetails;
	private UserDetailsModel belongigngUserDetails;

	private boolean deleteflag;

	private String roomName;

	private Date time;

	private String title;

	private String type;

	private List<NotificationReceivedUserModel> notificationReceivedUserList;

	private VidoeCallSignatureModel vidoeCallSignatureModel;

	public VidoeCallSignatureModel getVidoeCallSignatureModel() {
		return vidoeCallSignatureModel;
	}

	public void setVidoeCallSignatureModel(VidoeCallSignatureModel vidoeCallSignatureModel) {
		this.vidoeCallSignatureModel = vidoeCallSignatureModel;
	}

	public List<NotificationReceivedUserModel> getNotificationReceivedUserList() {
		return notificationReceivedUserList;
	}

	public void setNotificationReceivedUserList(List<NotificationReceivedUserModel> notificationReceivedUserList) {
		this.notificationReceivedUserList = notificationReceivedUserList;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/*
	 * public UserDetailsModel getUserDetails() { return userDetails; } public
	 * void setUserDetails(UserDetailsModel userDetails) { this.userDetails =
	 * userDetails; }
	 */
	public boolean isDeleteflag() {
		return deleteflag;
	}

	public void setDeleteflag(boolean deleteflag) {
		this.deleteflag = deleteflag;
	}

	public UserDetailsModel getBelongigngUserDetails() {
		return belongigngUserDetails;
	}

	public void setBelongigngUserDetails(UserDetailsModel belongigngUserDetails) {
		this.belongigngUserDetails = belongigngUserDetails;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
