package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class CalenderNotificationRequestModel {

	@NotBlank(message = "{calenderNotification.request.zoneId.is.required}")
	@JsonProperty("zoneid")
	private String zoneId;

	@NotBlank(message = "{calenderNotification.request.startDate.is.required}")
	@JsonProperty("start")
	private String startDate;

	@NotBlank(message = "{calenderNotification.request.endDate.is.required}")
	@JsonProperty("end")
	private String endDate;
}
