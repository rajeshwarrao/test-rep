package com.sg.dayamed.pojo;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Length;

import java.util.List;

@Getter
@Setter
public class DosageInfoModel {

	private long id;

	private List<String> time;

	//private String time;
	private int duration;

	private String comment;

	private String pcomment;

	private Integer consumeDuration;

	// have to check this relation
	private MedicineModel medicine;

	private String identityCode;

	private List<DosageNotificationModel> dosageNotificationList;

	private String adherenceType;

	private String frequency;

	private List<String> alaramscheduleList;

	private FristConsumptionTypeModel fristConsumptionType;

	private FristConsumptionFrequencyModel fristConsumption;

	@Length(message = "{prescription.dosage.custom.video.url.link.invalid.length}", max = 500)
	private String customVideoLink;
	
	@Length(message = "{prescription.dosage.dosage.time.image.url.invalid.length}", max = 500)
	private String dosageTimeImageUrl;

}
