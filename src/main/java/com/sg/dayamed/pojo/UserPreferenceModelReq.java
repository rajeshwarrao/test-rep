package com.sg.dayamed.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserPreferenceModelReq {

	private String languagePreference;

	private String firstConsumption;

	private Boolean smsFlag;

	private Boolean emailFlag;

	private Boolean pushNotificationFlag;

}
