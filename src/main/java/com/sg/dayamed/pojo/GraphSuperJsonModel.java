package com.sg.dayamed.pojo;

import java.util.List;

public class GraphSuperJsonModel {

	private List<Integer> labels;

	private List<GraphJsonModel> datasets;

	public List<Integer> getLabels() {
		return labels;
	}

	public void setLabels(List<Integer> labels) {
		this.labels = labels;
	}

	public List<GraphJsonModel> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<GraphJsonModel> datasets) {
		this.datasets = datasets;
	}
}
