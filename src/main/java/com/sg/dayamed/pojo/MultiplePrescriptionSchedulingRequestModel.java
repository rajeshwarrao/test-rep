package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
public class MultiplePrescriptionSchedulingRequestModel {

	@NotBlank(message = "{prescription.scheduling.zoneId.is.required}")
	@JsonProperty("zoneid")
	private String zoneId;

	@NotBlank(message = "{prescription.scheduling.prescriptionId.is.required}")
	@JsonProperty("prescriptions")
	private List<Long> prescriptionIds;
}
