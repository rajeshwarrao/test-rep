package com.sg.dayamed.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
public class FcmNotificationRequestModel {

	@NotBlank(message = "{fcm.notification.emailId.is.required}")
	@JsonProperty("emailid")
	private List<String> emailIds;

	@NotBlank(message = "{fcm.notification.title.is.required}")
	@JsonProperty("title")
	private String title;

	@NotBlank(message = "{fcm.notification.message.is.required}")
	@JsonProperty("msg")
	private String message;

	@JsonProperty("belongingid")
	private Long belongingId;

}

