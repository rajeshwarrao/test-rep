package com.sg.dayamed.pojo;

import java.util.List;

public class PrescriptionModel {

	private long id;

	private String problem;

	private String solution;

	private List<DiagnosisModel> DiagnosisList;
	//private DiagnosisModel diagnosis;

	//private String updatedDate;
	
	/*public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}*/

	private String date;

	// private String diseaseId;

	private String comment;

	private PatientModel patient;

	private PrescriptionStatusModel prescriptionStatus;

	private List<DiseaseInfoModel> diseaseInfoList;

	private List<DosageInfoModel> dosageInfoList;

	private List<ConsumerGoodInfoModel> commodityInfoList;

	private List<DosageDeviceModel> dosageDevices;

	private List<DeviceInfoModel> deviceInfoList;

	private String expectedAdherence;

	private boolean canPatientModify;

	public boolean isCanPatientModify() {
		return canPatientModify;
	}

	public void setCanPatientModify(boolean canPatientModify) {
		this.canPatientModify = canPatientModify;
	}

	public PatientModel getPatient() {
		return patient;
	}

	public void setPatient(PatientModel patient) {
		this.patient = patient;
	}

	public List<DosageInfoModel> getDosageInfoList() {
		return dosageInfoList;
	}

	public void setDosageInfoList(List<DosageInfoModel> dosageInfoList) {
		this.dosageInfoList = dosageInfoList;
	}

	public String getExpectedAdherence() {
		return expectedAdherence;
	}

	public void setExpectedAdherence(String expectedAdherence) {
		this.expectedAdherence = expectedAdherence;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PrescriptionStatusModel getPrescriptionStatus() {
		return prescriptionStatus;
	}

	public void setPrescriptionStatus(PrescriptionStatusModel prescriptionStatus) {
		this.prescriptionStatus = prescriptionStatus;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<ConsumerGoodInfoModel> getCommodityInfoList() {
		return commodityInfoList;
	}

	public void setCommodityInfoList(List<ConsumerGoodInfoModel> commodityInfoList) {
		this.commodityInfoList = commodityInfoList;
	}

	public List<DeviceInfoModel> getDeviceInfoList() {
		return deviceInfoList;
	}

	public void setDeviceInfoList(List<DeviceInfoModel> deviceInfoList) {
		this.deviceInfoList = deviceInfoList;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public List<DiseaseInfoModel> getDiseaseInfoList() {
		return diseaseInfoList;
	}

	public void setDiseaseInfoList(List<DiseaseInfoModel> diseaseInfoList) {
		this.diseaseInfoList = diseaseInfoList;
	}

	public List<DosageDeviceModel> getDosageDevices() {
		return dosageDevices;
	}

	public void setDosageDevices(List<DosageDeviceModel> dosageDevices) {
		this.dosageDevices = dosageDevices;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<DiagnosisModel> getDiagnosisList() {
		return DiagnosisList;
	}

	public void setDiagnosisList(List<DiagnosisModel> diagnosisList) {
		DiagnosisList = diagnosisList;
	}

}
