package com.sg.dayamed.entity;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/

public class TestEntityUtilities {

	@Test
	public void testConsumptionTemplate() {
		ConsumptionTemplate template = TestResponseGenerator.generateConsumptionTemplate();
		assertNotNull(template);
		assertNotNull(template.getConsumptionsKey());
		assertNotNull(template.getFrequencyName());
		assertNotNull(template.getConsumptionsTimes());
		assertNotNull(template.getConsumptionsDesc());
		assertNotNull(template.getConsumptionsId());
		assertNotNull(template.getFrequencyId());
		assertNotNull(template.getId());
	}

	@Test
	public void testDosageNotification() {
		DosageNotification notification = TestResponseGenerator.generateDosageNotification();
		assertNotNull(notification);
		assertNotNull(notification.getNotificationValue());
		assertNotNull(notification.getId());
		assertNotNull(notification.getUserId());
		assertNotNull(notification.getNotificationType());
		assertNotNull(notification.getExpectedTime());
		assertNotNull(notification.getCountryCode());
		assertNotNull(notification.getDosageInfo());
	}

	@Test
	public void testMedicine() {
		Medicine medicine = TestResponseGenerator.generateMedicine();
		assertNotNull(medicine);
		assertNotNull(medicine.getName());
		assertNotNull(medicine.getImageURL());
		assertNotNull(medicine.getNdcCode());
		assertNotNull(medicine.getType());
		assertNotNull(medicine.getCategory());
		assertNotNull(medicine.getDescription());
		assertNotNull(medicine.getGenCode());
		assertNotNull(medicine.getId());
		assertNotNull(medicine.getItemCategoryType());
		assertNotNull(medicine.getNdcType());
		assertNotNull(medicine.getPackager());
	}
}
