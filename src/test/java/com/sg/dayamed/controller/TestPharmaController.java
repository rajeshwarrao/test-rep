package com.sg.dayamed.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.entity.Device;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.service.PharmaService;
import com.sg.dayamed.util.TestResponseGenerator;

@RunWith(MockitoJUnitRunner.class)
public class TestPharmaController extends BaseRestApiTest {

	@InjectMocks
	private PharmaController pharmaController;

	@Mock
	PharmaService pharmaService;

	@Test
	public void testFetchMedicines() throws Exception {
		List<Medicine> listMedicine = Arrays.asList(TestResponseGenerator.generateMedicine());
		Mockito.when(pharmaService.fetchMedicines())
		       .thenReturn(listMedicine);
		ResponseEntity<Object> responseEntity = pharmaController.fetchMedicines();
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(listMedicine, responseEntity.getBody());
	}

	@Test
	public void testFetchConsumergoods() throws Exception {
		List<ConsumerGoods> listConsumerGoods = Arrays.asList(TestResponseGenerator.generateConsumerGoods());
		Mockito.when(pharmaService.fetchConsumergoods())
		       .thenReturn(listConsumerGoods);
		ResponseEntity<Object> responseEntity = pharmaController.fetchConsumergoods();
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(listConsumerGoods, responseEntity.getBody());
	}

	@Test
	public void testFetchDevices() throws Exception {
		List<Device> listDevice = Arrays.asList(TestResponseGenerator.generateDevice());
		Mockito.when(pharmaService.fetchAllDevices())
		       .thenReturn(listDevice);
		ResponseEntity<Object> responseEntity = pharmaController.fetchDevices();
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(listDevice, responseEntity.getBody());
	}

	@Test
	public void testFetchDeceases() throws Exception {
		List<Disease> listDisease = Arrays.asList(TestResponseGenerator.generateDisease());
		Mockito.when(pharmaService.fetchDiseases())
		       .thenReturn(listDisease);
		ResponseEntity<Object> responseEntity = pharmaController.fetchDeceases();
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(listDisease, responseEntity.getBody());
	}
}
