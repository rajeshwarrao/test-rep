package com.sg.dayamed.controller;

import com.sg.dayamed.service.TwilioVoiceService;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.TwilioVoiceModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Naresh Kamisetti on 23/July/2019
 **/
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.security.*")
public class TestTwilioVoiceController {

	@InjectMocks
	TwilioVoiceController mockTwilioVoiceController;

	@Mock
	TwilioVoiceService mockTwilioVoiceService;

	@Before
	public void init() throws Exception {
	}

	@Test
	public void fetchTwilioVoiceToken() throws Exception {
		TwilioVoiceModel twilioVoiceModel = TestResponseGenerator.generateTwilioVoiceModel();
		Mockito.when(mockTwilioVoiceService.generateVoiceToken(Mockito.anyString()))
		       .thenReturn(twilioVoiceModel);
		ResponseEntity responseEntity = mockTwilioVoiceController.fetchTwilioVoiceToken(twilioVoiceModel);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void getVoiceXMLResponse() throws Exception {
		TwilioVoiceModel twilioVoiceModel = TestResponseGenerator.generateTwilioVoiceModel();
		Mockito.when(mockTwilioVoiceService.connectVoiceCall(Mockito.anyString()))
		       .thenReturn("abcd");
		ResponseEntity responseEntity = mockTwilioVoiceController.getVoiceXMLResponse(twilioVoiceModel);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

}
