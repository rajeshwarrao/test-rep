package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.pojo.AdherenceReportResModel;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.AdherenceReportService;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.DosageInfoService;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.PrescriptionScheduleService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Naresh Kamisetti on 18/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestAdherenceController extends BaseRestApiTest {

	@InjectMocks
	AdherenceController adherenceController = new AdherenceController();

	@Mock
	DosageInfoService mockDosageInfoService;

	@Mock
	Utility mockUtility;

	@Mock
	PrescriptionScheduleService mockPrescriptionScheduleService;

	@Mock
	PrescriptionScheduleRepository mockpPrescriptionScheduleRepository;

	@Mock
	NotificationScheduleService mockNotificationScheduleService;

	@Mock
	DateUtility mockDateUtility;

	@Mock
	AdherenceService mockAdherenceService;

	@Mock
	AdherenceReportService mockAdherenceReportService;

	@Before
	public void setUp() {
		LocalDateTime dateTime = LocalDateTime.now();
		Mockito.when(mockDateUtility.convertLocalTimeToUTCTimeObjectByzoneid(Mockito.any(), Mockito.any()))
		       .thenReturn(dateTime);
	}

	@Test
	public void testAddNewAdherence() throws Exception {

		String request = "{\"adherenceId\": \"\",\"how\": [\"AR\",\"NFC\"],\"dosageinfoids\": [\"24\",\"25\"],"
				+ "  \"consumptionstatus\": \"skipped\","
				+ " \"latitude\": \"56.130721\","
				+ " \"longitude\": \"106.346337\","
				+ " \"prescriptionid\": 13,"
				+ " \"prescribedTime\": \"2019-05-31 14:00\","
				+ "  \"observedtime\": \" 2019-05-31 14:05\","
				+ " \"zoneid\": \"Asia/Kolkata\"}";

		DosageInfo dosageInfo = TestResponseGenerator.generateDosageInfo(false);
		dosageInfo.setPrescription(TestResponseGenerator.generatePrescription());
		Mockito.when(mockDosageInfoService.fetctDosageInfoByID(Mockito.anyLong()))
		       .thenReturn(dosageInfo);

		List<String> consumptionStatusList = Stream.of("delayed")
		                                           .collect(Collectors.toList());
		AdherenceDataPoints adherenceDataPoints =
				TestResponseGenerator.generateAdherenceDataPoints(consumptionStatusList);
		Mockito.when(mockAdherenceService.findByPrescribedTimeAndprescriptionID(Mockito.any(), Mockito.anyLong()))
		       .thenReturn(adherenceDataPoints);
		ResponseEntity responseEntity = adherenceController.addNewAdherence(request);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testGetAdherenceReport() throws Exception {
		AdherenceReportResModel adherenceReportResModel = new AdherenceReportResModel();
		adherenceReportResModel.setStatus(ApplicationConstants.SUCCESS);
		adherenceReportResModel.setMessage(ApplicationConstants.SUCCESS);
		Mockito.when(mockAdherenceReportService.getAdherenceReport(Mockito.any(), Mockito.anyString())).thenReturn(adherenceReportResModel);
		JwtUserDetails jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(TestResponseGenerator.generateUserDetails("Male","Asian",ApplicationConstants.PATIENT),
		                                                                             true, ApplicationConstants.PATIENT);
		List<Long> providerIds = new ArrayList<>();
		providerIds.add(1l);
		ResponseEntity responseEntity = adherenceController.getAdherenceReport(jwtUserDetails, providerIds);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(adherenceReportResModel, responseEntity.getBody());
	}
}
