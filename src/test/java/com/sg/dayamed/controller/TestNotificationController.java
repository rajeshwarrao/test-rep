package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.helper.pojo.CaregiverDosageNotificationModel;
import com.sg.dayamed.pojo.APNSUpdateTokenRequestModel;
import com.sg.dayamed.pojo.CalendarNotificationModel;
import com.sg.dayamed.pojo.CalenderNotificationRequestModel;
import com.sg.dayamed.pojo.DosageNotificationModel;
import com.sg.dayamed.pojo.FcmNotificationRequestModel;
import com.sg.dayamed.pojo.NotificationModel;
import com.sg.dayamed.pojo.NotificationRequestModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.VideoNotificationRequestModel;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 18/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestNotificationController extends BaseRestApiTest {

	@InjectMocks
	NotificationController notificationController = new NotificationController();

	@Mock
	private UserDetailsRepository mockUserDetailsRepository;

	@Mock
	private NotificationService mockNotificationService;

	@Mock
	private Utility mockUtility;

	@Test
	public void testAddNotification() throws Exception {
		NotificationRequestModel notificationRequestModel = TestResponseGenerator.generateNotificationRequestModel();
		NotificationModel notificationModel =
				generateRequestObject("notificationModel", NotificationModel.class, ADHERENCE_DATA_POINTS_FILE);
		Mockito.when(mockNotificationService.addNotificationFromController(Mockito.any()))
		       .thenReturn(notificationModel);
		ResponseEntity responseEntity = notificationController.addNotification(notificationRequestModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(notificationModel, responseEntity.getBody());
	}

	@Test
	public void testFetchNotificationByType() throws Exception {
		List<NotificationModel>
				listNotificationModel = Stream.of(
				generateRequestObject("notificationModel", NotificationModel.class, ADHERENCE_DATA_POINTS_FILE))
				                              .collect(Collectors.toList());
		Mockito.when(mockNotificationService.fetchNotificationModelsByNotificationType(Mockito.anyString()))
		       .thenReturn(listNotificationModel);
		ResponseEntity responseEntity = notificationController.fetchNotificationByType("type");
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(listNotificationModel, responseEntity.getBody());
	}

	@Test
	public void testFetchNotificationByBelongingUserId() throws Exception {
		List<Notification> listNotification = Stream.of(TestResponseGenerator.generateNotification())
		                                            .collect(Collectors.toList());
		NotificationModel notificationModel =
				generateRequestObject("notificationModel", NotificationModel.class, ADHERENCE_DATA_POINTS_FILE);
		Mockito.when(mockUtility.convertNotificationEntityToModel(Mockito.any()))
		       .thenReturn(notificationModel);
		Mockito.when(mockNotificationService.fetchNotificationsByBelongingUserId(Mockito.anyLong()))
		       .thenReturn(listNotification);
		List<NotificationModel> listNotificationModel = Stream.of(notificationModel)
		                                                      .collect(Collectors.toList());
		ResponseEntity responseEntity = notificationController.fetchNotificationByBelongingUserId(1l);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(listNotificationModel, responseEntity.getBody());
	}

	@Test
	public void testDeleteNotifications() throws Exception {
		Notification notification = TestResponseGenerator.generateNotification();
		Mockito.when(mockNotificationService.fetchNotificationById(Mockito.anyLong()))
		       .thenReturn(notification);
		ResponseEntity responseEntity = notificationController.deleteNotifications(1l);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals("deleted", responseEntity.getBody());
	}

	@Test
	public void testCalenderNotificationsWithOutScheduledByPatientId() throws Exception {
		CalenderNotificationRequestModel calenderNotificationRequestModel = new CalenderNotificationRequestModel();
		CalendarNotificationModel calendarNotificationModel = new CalendarNotificationModel();
		Mockito.when(mockNotificationService.calenderNotificationsWithOutScheduledByPatientId(Mockito.anyLong(),
		                                                                                      Mockito.any()))
		       .thenReturn(calendarNotificationModel);
		ResponseEntity<Object> responseEntity =
				notificationController.calenderNotificationsWithOutScheduledByPatientId(1l,
				                                                                        calenderNotificationRequestModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(calendarNotificationModel, responseEntity.getBody());
	}

	@Test
	public void testCalenderNotificationsByPatientId() throws Exception {
		CalenderNotificationRequestModel calenderNotificationRequestModel = new CalenderNotificationRequestModel();
		CalendarNotificationModel calendarNotificationModel = new CalendarNotificationModel();
		Mockito.when(mockNotificationService.calenderNotificationsByPatientId(Mockito.anyLong(), Mockito.any()))
		       .thenReturn(calendarNotificationModel);
		ResponseEntity<Object> responseEntity =
				notificationController.calenderNotificationsByPatientId(1l, calenderNotificationRequestModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(calendarNotificationModel, responseEntity.getBody());
	}

	@Test
	public void testSendVideoNotificationByUserId() throws Exception {
		VideoNotificationRequestModel videoNotificationRequestModel =
				TestResponseGenerator.generateVideoNotificationRequestModel();
		Mockito.when(mockNotificationService.sendVideoNotificationByUserId(Mockito.any()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<Object> responseEntity =
				notificationController.sendVideoNotificationByUserId(videoNotificationRequestModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(ApplicationConstants.SUCCESS, responseEntity.getBody());
	}

	@Test
	public void testSendFcmNotification() throws Exception {
		FcmNotificationRequestModel fcmNotificationRequestModel =
				TestResponseGenerator.generateFcmNotificationRequestModel();
		Mockito.when(mockNotificationService.sendFcmNotification(Mockito.any()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<Object> responseEntity =
				notificationController.sendFcmNotification(fcmNotificationRequestModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(ApplicationConstants.SUCCESS, responseEntity.getBody());
	}

	@Test
	public void testUpdateTokenInUserDetails() throws Exception {
		APNSUpdateTokenRequestModel aPNSUpdateTokenRequestModel =
				TestResponseGenerator.generateAPNSUpdateTokenRequestModel();
		UserDetailsModel userDetails =
				TestResponseGenerator.generateUserDetailsModel("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockNotificationService.updateTokenInUserDetails(Mockito.any()))
		       .thenReturn(userDetails);
		ResponseEntity<Object> responseEntity =
				notificationController.updateTokenInUserDetails(aPNSUpdateTokenRequestModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(userDetails, responseEntity.getBody());
	}

	@Test
	public void testAddCaregiverNotifications() throws Exception{
		CaregiverDosageNotificationModel caregiverDosageNotificationModel = TestResponseGenerator.generateCaregiverDosageNotificationModel();
		Mockito.when(mockNotificationService.addCaregiverNotifications(Mockito.any())).thenReturn(caregiverDosageNotificationModel);
		ResponseEntity<Object> responseEntity =
				notificationController.addCaregiverNotifications(caregiverDosageNotificationModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(caregiverDosageNotificationModel, (CaregiverDosageNotificationModel)responseEntity.getBody());
	}

	@Test
	public void testFetchDosageNotificationsByCaregiverId() throws Exception{
		List<DosageNotificationModel> dosageNotificationModelList = Stream.of(TestResponseGenerator.generateDosageNotificationModel()).collect(
				Collectors.toList());
		Mockito.when(mockNotificationService.fetchDosageNotificationsByCaregiverId(Mockito.anyLong(),Mockito.anyLong())).thenReturn(dosageNotificationModelList);
		ResponseEntity<Object> responseEntity =
				notificationController.fetchDosageNotificationsByCaregiverId(1L,1L);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(dosageNotificationModelList, (List)responseEntity.getBody());
	}
}
