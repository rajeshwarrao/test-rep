package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.security.SecurityConstants;
import com.sg.dayamed.service.LoginValidationService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestTokenController extends BaseRestApiTest {

	@InjectMocks
	private TokenController tokenController;

	@Mock
	RediseService mockRediseService;

	@Mock
	private HttpServletRequest httpRequest;

	@Mock
	LoginValidationService loginValidationService;

	@Test
	public void testLogout() {
		when(httpRequest.getHeader(SecurityConstants.HEADER_STRING)).thenReturn("jwttoken");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.anyString()))
		       .thenReturn(generateJwtUserDetailsByJwtToken());
		when(mockRediseService.removeTokenFromCache(Mockito.anyString())).thenReturn(true);
		ResponseEntity<StringResponse> responseEntity = tokenController.logout(httpRequest);
		assertNotNull(responseEntity);
		StringResponse responseModel = (StringResponse) responseEntity.getBody();
		assertNotNull(responseModel);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals("Successfully loggedout", responseModel.getResponse());
	}

	@Test
	public void testLogin() throws Exception {
		when(loginValidationService.loginValidation(any(), any(), any())).thenReturn(getRestResponse());
		ResponseEntity<RestResponse> responseEntity =
				tokenController.login(TestResponseGenerator.getLoginRequestModel(), httpRequest);
		assertNotNull(responseEntity);
		RestResponse responseModel = (RestResponse) responseEntity.getBody();
		assertNotNull(responseModel);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testGenerate() throws Exception {
		when(loginValidationService.loginValidation(any(), any(), any())).thenReturn(getRestResponse());
		ResponseEntity<RestResponse> responseEntity =
				tokenController.generate(TestResponseGenerator.getLoginRequestModel(), httpRequest);
		assertNotNull(responseEntity);
		RestResponse responseModel = (RestResponse) responseEntity.getBody();
		assertNotNull(responseModel);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	public UserDetailsWithJwtModel getRestResponse() {
		UserDetailsWithJwtModel restResponse = new UserDetailsWithJwtModel();
		return restResponse;
	}

	public JwtUser generateJwtUserDetailsByJwtToken() {
		String role = ApplicationConstants.PROVIDER;
		JwtUser jwtUserDetails = new JwtUser();
		jwtUserDetails.setUserName("p1@pr.com");
		jwtUserDetails.setUserId(3l);
		jwtUserDetails.setPassword("8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918");
		jwtUserDetails.setRole("provider");
		jwtUserDetails.setGsPharmacistEnable(true);
		return jwtUserDetails;
	}
}
