package com.sg.dayamed.controller;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.VucaService;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.StringResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class VucaControllerTest {

	@InjectMocks
	private VucaController vucaController;

	@Mock
	private VucaService vucaService;

	private static final String NDC = "088909001";

	@Test
	public void testFetchVucaMedicineVideoURL() throws Exception {
		final String videoUrl = "http://medoncues.com/video?code=" + NDC;
		Mockito.when(vucaService.fetchVucaVideoURL(anyString()))
		       .thenReturn(videoUrl);
		final ResponseEntity<Object> objectResponseEntity = vucaController.fetchVucaMedicineVideoURL(NDC);
		assertEquals(HttpStatus.OK, objectResponseEntity.getStatusCode());
		assertEquals(videoUrl, ((StringResponse) objectResponseEntity.getBody()).getResponse());
	}

	@Test
	public void testFetchVucaMedicineVideoUrlWhenServiceThrowsException() throws Exception {
		Mockito.when(vucaService.fetchVucaVideoURL(anyString()))
		       .thenThrow(new ApplicationException(null, null, APIErrorKeys.ERROR_UNABLE_TO_GET_VUCA_TOKEN));
		try {
			vucaController.fetchVucaMedicineVideoURL(NDC);
		} catch (RestApiException apiException) {
			assertEquals(APIErrorKeys.ERROR_UNABLE_TO_GET_VUCA_TOKEN, apiException.getFaultInfo()
			                                                                      .getErrorKey());
		}
	}
}
