package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.helper.PatientHelper;
import com.sg.dayamed.helper.pojo.PatientAssociatedActorsPojo;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.service.PatientInfoService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Naresh Kamisetti on 18/May/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientController extends BaseRestApiTest {

	@InjectMocks
	private PatientController mockPatientController;

	@Mock
	private PatientInfoService mockPatientInfoService;

	@Mock
	private PatientService mockPatientService;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	SecurityContextHolder securityContextHolder;

	@Mock
	Authentication mockAuthentication;

	@Mock
	PHIDataEncryptionService mockPHIDataEncryptionService;


	@Mock
	private Utility mockUtility;

	@Mock
	PatientHelper mockPatientHelper;

	@Mock
	private PasswordEncryption mockPasswordEncryption;


	@Mock
	UserDetailsService mockUserDetailsService;

	private MockHttpServletRequest request = new MockHttpServletRequest();

	@Test
	public void testUpdatePatients() throws Exception {
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockPatientInfoService.updatePatient(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(patientModel);
		ResponseEntity<Object> responseEntity = mockPatientController.updatePatient("testData", null, request,
		                                                                            mockAuthentication);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), patientModel);
	}

	@Test
	public void testAddPatient() throws Exception {
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockPatientInfoService.addPatient(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(patientModel);
		ResponseEntity<Object> responseEntity = mockPatientController.addPatient("testdata", profileImage,
		                                                                         mockRequest, mockAuthentication);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), patientModel);
	}

	@Test
	public void testFetchPatietnsByProviderId() throws Exception {
		List<PatientModel> listPatientModel = Arrays.asList(TestResponseGenerator.generatePatientModel());
		Mockito.when(
				mockPatientInfoService.fetchPatietnsByProviderId(Mockito.any(), Mockito.anyString(), Mockito.any()))
		       .thenReturn(listPatientModel);
		ResponseEntity<Object> responseEntity = mockPatientController.fetchPatientsByProviderId(mockRequest, "1",
		                                                                                        mockAuthentication);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), listPatientModel);
	}

	@Test
	public void testFetchMissedAdherencePatietnsByProviderId() throws Exception {
		List<PatientModel> listPatientModel = Arrays.asList(TestResponseGenerator.generatePatientModel());
		Mockito.when(mockPatientInfoService.fetchMissedAdherencePatietnsByProviderId(Mockito.any(), Mockito.any()))
		       .thenReturn(listPatientModel);
		ResponseEntity<Object> responseEntity = mockPatientController
				.fetchMissedAdherencePatientsByProviderId(mockAuthentication, mockRequest);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), listPatientModel);
	}

	@Test
	public void testFetchPatietnsByProviderIdFromCookie() throws Exception {
		List<PatientModel> listPatientModel = Arrays.asList(TestResponseGenerator.generatePatientModel());
		Mockito.when(mockPatientInfoService.fetchPatietnsByProviderIdFromCookie(Mockito.any(), Mockito.any()))
		       .thenReturn(listPatientModel);
		ResponseEntity<Object> responseEntity = mockPatientController
				.fetchPatientsByProviderIdFromCookie(mockAuthentication, mockRequest);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), listPatientModel);
	}

	@Test
	public void testFetchAllPatients() throws Exception {
		List<PatientModel> listPatientModel = Arrays.asList(TestResponseGenerator.generatePatientModel());
		Mockito.when(mockPatientInfoService.fetchAllPatients(Mockito.any(), Mockito.any()))
		       .thenReturn(listPatientModel);
		ResponseEntity<Object> responseEntity = mockPatientController.fetchAllPatients(mockAuthentication,
		                                                                               mockRequest);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), listPatientModel);
	}

	@Test
	public void testFetchPatientById() throws Exception {
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockPatientInfoService.fetchPatientById(Mockito.anyString(), Mockito.any(), Mockito.any()))
		       .thenReturn(patientModel);
		ResponseEntity<Object> responseEntity = mockPatientController.fetchPatientById("1", mockAuthentication,
		                                                                               mockRequest);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), patientModel);
	}

	@Test
	public void testDeletePatient() throws Exception {
		Mockito.when(mockPatientInfoService.deletePatient(Mockito.anyString(), Mockito.any(), Mockito.any()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<Object> responseEntity = mockPatientController.deletePatient("1", mockRequest,
		                                                                            mockAuthentication);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		StringResponse stringResponse = (StringResponse) responseEntity.getBody();
		assertEquals(stringResponse.getResponse(), ApplicationConstants.SUCCESS);
	}

	@Test
	public void testFetchProvidersByPatientId() throws Exception {
		PatientAssociatedActorsPojo patientAssociatedActorsPojo = new PatientAssociatedActorsPojo();
		Mockito.when(mockPatientInfoService.fetchProvidersByPatientId(Mockito.anyString()))
		       .thenReturn(patientAssociatedActorsPojo);
		ResponseEntity<Object> responseEntity = mockPatientController.fetchProvidersByPatientId("1");
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testAddPatientWithoutDateOfBirth() throws Exception {
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		patientModel.getUserDetails()
		            .setDateOfBirth(null);
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		Mockito.when(mockPatientInfoService.addPatient(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(patientModel);
		ResponseEntity<Object> responseEntity = mockPatientController.addPatient("test", profileImage,
		                                                                         mockRequest, mockAuthentication);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals(responseEntity.getBody(), patientModel);
	}
}
