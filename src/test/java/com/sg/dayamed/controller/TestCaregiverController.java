package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.pojo.PatientEmailVO;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.repository.CaregiverRepository;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestCaregiverController extends BaseRestApiTest {

	@InjectMocks
	private CaregiverController caregiverController;

	@Mock
	CaregiverRepository mockCaregiverRepository;

	@Mock
	private UserDetailsService mockUserDetailsService;

	@Mock
	private PatientService mockPatientService;

	@Mock
	UserPreferenceService mockUserPreferenceService;

	@Mock
	EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	CaregiverService mockCaregiverService;

	@Mock
	Utility mockUtility;
	
	@Mock
	MockMultipartFile mockMultipartFile;

	@Mock
	MessageNotificationConstants mockMessageNotificationConstants;

	@Mock
	Authentication mockAuthentication;
	
	@Mock
	JwtUserDetails jwtUserDetails;
	
	

	
	@Test
	public void fetchPatietnsByCaregiverIdFromJwtToken() throws RestApiException, DataValidationException{
		List<PatientModel> patietnModelList = new ArrayList<>();
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		patietnModelList.add(patientModel);
		Mockito.when(mockCaregiverService.fetchPatietnsByCaregiverIdFromJwtToken( Mockito.any(Authentication.class))).thenReturn(patietnModelList);
		ResponseEntity<Object> responseEntity = caregiverController.fetchPatietnsByCaregiverIdFromJwtToken(mockAuthentication);
		assertNotNull(responseEntity);
	}
	
	@Test
	public void fetchPatietnsAloneByCaregiver() throws RestApiException, DataValidationException{
		List<PatientModel> patietnModelList = new ArrayList<>();
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		patietnModelList.add(patientModel);
		Mockito.when(mockCaregiverService.fetchPatietnsAloneByCaregiver(Mockito.any(Authentication.class))).thenReturn(patietnModelList);
		ResponseEntity<Object> responseEntity = caregiverController.fetchPatietnsAloneByCaregiver(mockAuthentication);
		assertNotNull(responseEntity);
	}
	
	@Test
	public void addCaregiverFromWeb() throws RestApiException, IOException, DataValidationException{
		
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
                inputFile);
		//Mockito.when(mockCaregiverService.addCaregiverFromWebValidate(Mockito.any(CaregiverModel.class), Mockito.any(MultipartFile.class), Mockito.any(JwtUserDetails.class))).thenReturn(caregiverModel);
		ResponseEntity<Object> responseEntity = caregiverController.addCaregiverFromWeb("testdata", profileImage, jwtUserDetails);
		assertNotNull(responseEntity);
	}
		
	@Test
	public void deleteCaregiver() throws RestApiException{
		ResponseEntity<Object> responseEntity = caregiverController.deleteCaregiver(1L);
		assertNotNull(responseEntity);
	}
	
	@Test
	public void mapCaregiverwithPatient() throws DataValidationException, JSONException, RestApiException{
		PatientEmailVO patientEmailVO = new PatientEmailVO();
		patientEmailVO.setPatientEmailId("abcd@gmail.con");
		Mockito.when(mockCaregiverService.mapPatientToCaegiver(Mockito.anyString(), Mockito.anyLong())).thenReturn("success");
		ResponseEntity<Object> responseEntity = caregiverController.mapCaregiverwithPatient(patientEmailVO, 1L);
		assertNotNull(responseEntity);
	}
	
	@Test
	public void testUpdateCaregiverFromApp() throws Exception {
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("male", "Asian");
		Mockito.when(mockCaregiverService.updateCaregiverFromApp(Mockito.any(), Mockito.any())).thenReturn(caregiverModel);
		ResponseEntity<Object> responseEntity = caregiverController.updateCaregiverFromApp("testdata", null);
		assertNotNull(responseEntity);
		assertEquals(caregiverModel, (CaregiverModel) responseEntity.getBody());
	}

	@Test
	public void testCreateCaregiverAssociatedWithPatient() throws Exception{
		Set<CaregiverModel> setCaregiverModel = new HashSet<CaregiverModel>();
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("male", "Asian");
		setCaregiverModel.add(caregiverModel);
		Mockito.when(mockCaregiverService.createCaregiverAssociatedwithPatient(Mockito.any(),  Mockito.anyLong(),Mockito.any())).thenReturn(setCaregiverModel);
		ResponseEntity<Object> responseEntity = caregiverController.createCaregiverAssociatedWithPatient("testdata", null,1L);
		assertNotNull(responseEntity);
		assertEquals(setCaregiverModel, (Set) responseEntity.getBody());
	}

	@Test
	public void testFetchCaregiverById() throws  Exception{
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("male","Asian",ApplicationConstants.CAREGIVER,true);
		Mockito.when(mockCaregiverService.fetchCaregiverById( Mockito.anyLong())).thenReturn(caregiver);
		ResponseEntity<Object> responseEntity = caregiverController.fetchCaregiverById(1L);
		assertNotNull(responseEntity);
		assertEquals(caregiver, (Caregiver) responseEntity.getBody());
	}

	@Test
	public void testFetchCaregiverByUserId() throws Exception{
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("male","Asian",ApplicationConstants.CAREGIVER,true);
		Mockito.when(mockCaregiverService.fetchCaregiverByUserId( Mockito.anyLong())).thenReturn(caregiver);
		ResponseEntity<Object> responseEntity = caregiverController.fetchCaregiverByUserId(1L);
		assertNotNull(responseEntity);
		assertEquals(caregiver, (Caregiver) responseEntity.getBody());
	}

	@Test
	public void testFetchAdminAndSpecificProviderCaregivers() throws  Exception{
		List<CaregiverModel> setCaregiverModel = new ArrayList<CaregiverModel>();
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("male", "Asian");
		setCaregiverModel.add(caregiverModel);
		Mockito.when(mockCaregiverService.fetchAdminAndSpecificProviderCargivers(Mockito.anyLong(),Mockito.any())).thenReturn(setCaregiverModel);
		ResponseEntity<Object> responseEntity = caregiverController.fetchAdminAndSpecificProviderCaregivers("1",null);
		assertNotNull(responseEntity);
		assertEquals(setCaregiverModel, (List) responseEntity.getBody());
	}

	@Test
	public void testFetchCaregivers() throws  Exception{
		List<CaregiverModel> setCaregiverModel = new ArrayList<CaregiverModel>();
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("male", "Asian");
		setCaregiverModel.add(caregiverModel);
		Mockito.when(mockCaregiverService.fetchCaregivers()).thenReturn(setCaregiverModel);
		ResponseEntity<Object> responseEntity = caregiverController.fetchCaregivers();
		assertNotNull(responseEntity);
		assertEquals(setCaregiverModel, (List) responseEntity.getBody());
	}

	@Test
	public void testFetchPatientsByCaregiverId() throws  Exception{
		List<PatientModel> listPatientModels = Stream.of(TestResponseGenerator.generatePatientModel()).collect(
				Collectors.toList());
		Mockito.when(mockCaregiverService.fetchPatientsByCaregiverId(Mockito.anyLong(), Mockito.any())).thenReturn(listPatientModels);
		ResponseEntity<Object> responseEntity = caregiverController.fetchPatientsByCaregiverId(1L,null,null);
		assertNotNull(responseEntity);
		assertEquals(listPatientModels, (List) responseEntity.getBody());
	}

	@Test
	public void testFetchPrescriptionsByCaregiverId() throws Exception{
		List<PrescriptionModel> listPrescriptionModel = Stream.of(TestResponseGenerator.generatePrescriptionModel()).collect(
				Collectors.toList());
		Mockito.when(mockCaregiverService.fetchPrescriptionsByCaregiverId(Mockito.anyLong())).thenReturn(listPrescriptionModel);
		ResponseEntity<Object> responseEntity = caregiverController.fetchPrescriptionsByCaregiverId(1L);
		assertNotNull(responseEntity);
		assertEquals(listPrescriptionModel, (List) responseEntity.getBody());
	}

	@Test
	public void testFetchCaregiversByPatientId() throws Exception{
		List<CaregiverModel> listCaregiverModel = Stream.of(TestResponseGenerator.generateCaregiverModel("male", "Asian")).collect(
				Collectors.toList());
		Mockito.when(mockCaregiverService.fetchCaregiversByPatientId(Mockito.anyLong())).thenReturn(listCaregiverModel);
		ResponseEntity<Object> responseEntity = caregiverController.fetchCaregiversByPatientId(1L);
		assertNotNull(responseEntity);
		assertEquals(listCaregiverModel, (List) responseEntity.getBody());
	}

}
