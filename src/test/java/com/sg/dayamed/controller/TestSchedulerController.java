package com.sg.dayamed.controller;

import com.sg.dayamed.pojo.ConsumptionFrequencyResponseModel;
import com.sg.dayamed.pojo.FirstConsumptionResponseModel;
import com.sg.dayamed.pojo.MultiplePrescriptionSchedulingRequestModel;
import com.sg.dayamed.pojo.PrescriptionSchedulingRequestModel;
import com.sg.dayamed.service.ConsumptionTemplateService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.UtilityService;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestSchedulerController {

	@Mock
	PrescriptionService prescriptionService;

	@InjectMocks
	private SchedulerController schedulerController;

	@Mock
	UtilityService utilityService;

	@Mock
	ConsumptionTemplateService consumptionTemplateService;

	@Test
	public void testStartPrescriptionScheduling() throws  Exception{
		PrescriptionSchedulingRequestModel prescriptionSchedulingRequestModel= new PrescriptionSchedulingRequestModel();
		prescriptionSchedulingRequestModel.setFlag(1);
		prescriptionSchedulingRequestModel.setPrescriptionId(1l);
		prescriptionSchedulingRequestModel.setZoneId("Asian/Kolkata");
		Mockito.when(prescriptionService.generatePrescriptionSchedule(Mockito.anyLong(), Mockito.anyString())).thenReturn(true);
		Mockito.when(prescriptionService.createSchedulerJobsByPrescription(Mockito.anyLong(), Mockito.anyString())).thenReturn(true);
		boolean isValid = schedulerController.startPrescriptionScheduling(prescriptionSchedulingRequestModel);
		assertTrue(isValid);
	}

	@Test
	public void testStartMultiplePrescriptionScheduling() throws  Exception{
		MultiplePrescriptionSchedulingRequestModel multiplePrescriptionSchedulingRequestModel= TestResponseGenerator.generateMultiplePrescriptionSchedulingRequestModel();
		Mockito.when(utilityService.startMultiplePrescriptionScheduling(Mockito.any())).thenReturn(true);
		boolean isValid = schedulerController.startMultiplePrescriptionScheduling(multiplePrescriptionSchedulingRequestModel);
		assertTrue(isValid);
	}

	@Test
	public void testGetFrequencyList() throws  Exception{
		ConsumptionFrequencyResponseModel consumptionFrequencyResponseModel = new ConsumptionFrequencyResponseModel();
		Mockito.when(consumptionTemplateService.getFrequencyList()).thenReturn(consumptionFrequencyResponseModel);
		ResponseEntity<Object> responseEntity = schedulerController.getFrequencyList();
		assertTrue(responseEntity != null);
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), consumptionFrequencyResponseModel);
	}

	@Test
	public void testGetFirstConsumption() throws  Exception{
		FirstConsumptionResponseModel firstConsumptionResponseModel = TestResponseGenerator.generateFirstConsumptionResponseModel();
		Mockito.when(consumptionTemplateService.getFirstConsumption(Mockito.anyLong())).thenReturn(firstConsumptionResponseModel);
		ResponseEntity<Object> responseEntity = schedulerController.getFirstConsumption(1l);
		assertTrue(responseEntity != null);
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), firstConsumptionResponseModel);
	}

}
