package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.ApplicationConstants;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(PowerMockRunner.class)
public class TestResetPasswordController extends BaseRestApiTest {

	@InjectMocks
	ResetPasswordController resetPasswordController = new ResetPasswordController();

	@Mock
	UserDetailsService mockuserDetailsService;

	@Test
	public void testResetPassword() throws JSONException {

		/**
		 * Case 1 : assert if the request object is empty.
		 */

		String request = "{}";
		ResponseEntity responseEntity = resetPasswordController.resetPassword(request);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals("{\"message\":\"invalid JSON Request\",\"status\":\"fail\"}", responseEntity.getBody());

		/**
		 * Case 2 : success case with proper Json string and but pass expired token
		 */

		String request1 = "{\"tokenId\": \"xyzxyz1234\",\"password\": \"seneca123\"}";
		Mockito.when(mockuserDetailsService.resetPassword(Mockito.eq("xyzxyz1234"), Mockito.eq("seneca123")))
		       .thenReturn(ApplicationConstants.EXPIRED_TOKEN);
		responseEntity = resetPasswordController.resetPassword(request1);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 3 : TokenId empty and password available.
		 */
		String request2 = "{\"tokenId\": \"\",\"password\": \"seneca123\"}";
		responseEntity = resetPasswordController.resetPassword(request2);
		assertEquals("{\"message\":\"invalid JSON Request\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 4 : TokenId available and password empty.
		 */
		String request3 = "{\"tokenId\": \"xyzxyz1234\",\"password\": \"\"}";
		responseEntity = resetPasswordController.resetPassword(request3);
		assertEquals("{\"message\":\"invalid JSON Request\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 5 : TokenId empty and password empty.
		 */
		String request4 = "{\"tokenId\": \"\",\"password\": \"\"}";
		responseEntity = resetPasswordController.resetPassword(request4);
		assertEquals("{\"message\":\"invalid JSON Request\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 6 : Check for invalid token with user null
		 */
		String request5 = "{\"tokenId\": \"xyzxyz1234\",\"password\": \"seneca123\"}";
		Mockito.when(mockuserDetailsService.resetPassword(Mockito.eq("xyzxyz1234"), Mockito.eq("seneca123")))
		       .thenReturn(ApplicationConstants.INVALID_TOKEN);
		responseEntity = resetPasswordController.resetPassword(request5);
		assertEquals("{\"message\":\"invalid token\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 7 : Check for success for password change event
		 */
		String request6 = "{\"tokenId\": \"xyzxyz1234\",\"password\": \"seneca123\"}";
		Mockito.when(mockuserDetailsService.resetPassword(Mockito.eq("xyzxyz1234"), Mockito.eq("seneca123")))
		       .thenReturn(ApplicationConstants.SUCCESS);
		responseEntity = resetPasswordController.resetPassword(request6);
		assertEquals("{\"message\":\"Password updated successfully\",\"status\":\"success\"}",
		             responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testForgotPassword() throws JSONException {

		/**
		 * Case 1 : check input string is empty.
		 */
		String request = "";
		ResponseEntity responseEntity = resetPasswordController.forgotPassword(request);
		assertEquals("{\"message\":\"invalid JSON Request\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 2 : check input string is empty.
		 */
		String request1 = null;
		responseEntity = resetPasswordController.forgotPassword(request1);
		assertEquals("{\"message\":\"invalid JSON Request\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 3 : check emailId is empty.
		 */
		String request2 = "{\"emailId\": \"\"}";
		responseEntity = resetPasswordController.forgotPassword(request2);
		assertEquals("{\"message\":\"invalid JSON Request\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 4 : check emailId is null.
		 */
		String request3 = "{\"emailId\": null}";
		responseEntity = resetPasswordController.forgotPassword(request3);
		assertEquals("{\"message\":\"Email id does not exist\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 5 : check password reset success.
		 */
		String request4 = "{\"emailId\": \"test@gmail.com\"}";
		Mockito.when(mockuserDetailsService.forgotPassword(Mockito.eq("test@gmail.com")))
		       .thenReturn(ApplicationConstants.SUCCESS);
		responseEntity = resetPasswordController.forgotPassword(request4);
		assertEquals("{\"message\":\"Password reset mail sent successfully.\",\"status\":\"success\"}",
		             responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		/**
		 * Case 6 : check password reset fail.
		 */
		String request5 = "{\"emailId\": \"test@gmail.com\"}";
		Mockito.when(mockuserDetailsService.forgotPassword(Mockito.eq("test@gmail.com")))
		       .thenReturn(ApplicationConstants.INVALID_EMAILID);
		responseEntity = resetPasswordController.forgotPassword(request5);
		assertEquals("{\"message\":\"Email id does not exist\",\"status\":\"fail\"}", responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

	}
}
