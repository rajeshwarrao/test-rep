package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.ProviderService;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 01/May/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestProviderController extends BaseRestApiTest {

	private MockMvc mockMvc;

	@InjectMocks
	private ProviderController mockProviderController;

	@Mock
	private ProviderService mockProviderService;

	@Mock
	HttpServletRequest mockRequest;

	@Mock
	private Utility mockUtility;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders
				.standaloneSetup(mockProviderController)
				.build();
	}

	@Test
	public void createProvider() throws Exception {
		ProviderModel providerInfo = TestResponseGenerator.generateProviderModel();
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		ResponseEntity responseEntity = mockProviderController.createProvider("abcd", profileImage);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	/*@Test
	public void deleteProvider() throws RestApiException, DataValidationException {
		ResponseEntity responseEntity = mockProviderController.deleteProvider(1L);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}*/

	@Test
	public void listProvider() throws RestApiException {
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		List<ProviderModel> providerModelList = new ArrayList<ProviderModel>();
		providerModelList.add(providerModel);
		Mockito.when(mockProviderService.fetchProviders())
		       .thenReturn(providerModelList);
		ResponseEntity<Object> responseEntity = mockProviderController.listProvider();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void fetchProviderByUserId() throws Exception {
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Mockito.when(mockProviderService.fetchProviderByUserId(Mockito.anyLong()))
		       .thenReturn(providerModel);
		ResponseEntity responseEntity = mockProviderController.fetchProviderByUserId(1L);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void fetchProviderById() throws Exception {
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Mockito.when(mockProviderService.fetchProviderById(Mockito.anyLong()))
		       .thenReturn(providerModel);
		ResponseEntity responseEntity = mockProviderController.fetchProviderById(1L);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void fetchProviderByIdNOT_FOUND() throws Exception {
		Mockito.when(mockProviderService.fetchProviderById(Mockito.anyLong()))
		       .thenReturn(null);
		ResponseEntity responseEntity = mockProviderController.fetchProviderById(1L);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	public void fetchProvidersByPatientId() throws RestApiException, DataValidationException {
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		List<ProviderModel> providerModelList = new ArrayList<ProviderModel>();
		providerModelList.add(providerModel);

		Mockito.when(mockProviderService.fetchProvidersByPatientId(Mockito.anyLong()))
		       .thenReturn(providerModelList);
		ResponseEntity responseEntity = mockProviderController.fetchProvidersByPatientId(1L);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}
}
