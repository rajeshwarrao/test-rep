package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.pojo.DeletePrescriptionVO;
import com.sg.dayamed.helper.pojo.GenericIDAndZoneVO;
import com.sg.dayamed.helper.pojo.UpdatePrescriptionStatusVO;
import com.sg.dayamed.pojo.DiagnosisModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.DiagnosisService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.TestResponseGenerator;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Created By Kamisetti, Naresh on 01/May/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPrescriptionController extends BaseRestApiTest {


	@InjectMocks
	private PrescriptionController prescriptionController;

	@Mock
	PrescriptionStatusRepository prescriptionStatusRepository;

	@Mock
	PrescriptionService prescriptionService;

	@Mock
	DiagnosisService diagnosisService;

	@Mock
	private Authentication mockAuthentication;

	@Mock
	private HttpServletRequest mockHttpRequest;
	
	@Mock
	JwtUserDetails mockJwtUserDetails;


	@Test
	public void fetchDiagnosisByString() throws RestApiException, DataValidationException {
		DiagnosisModel diagnosisModel = TestResponseGenerator.generateDiagnosisModel();
		List<DiagnosisModel> diagnosisModelList = new ArrayList<>();
		diagnosisModelList.add(diagnosisModel);
		Mockito.when(diagnosisService.fetchDiagnosisModelIntelligenceByDescription(Mockito.anyString()))
		       .thenReturn(diagnosisModelList);
		ResponseEntity<Object> responseEntity = prescriptionController.searchDiagnosesByName("abc");
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
         assertEquals(Stream.of(diagnosisModel)
		                   .collect(Collectors.toList()), responseEntity.getBody());
	}

	@Test
	public void fetchPrescriptionListByPatientId() throws Exception {

		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		List<PrescriptionModel> prescriptionModelList = new ArrayList<>();
		prescriptionModelList.add(prescriptionModel);
		Mockito.when(prescriptionService.fetchPrescriptionListByPatientId(Mockito.anyLong(),
		                                                                  Mockito.any(Authentication.class)
		                                                                 ))
		       .thenReturn(prescriptionModelList);
		
		ResponseEntity<Object> responseEntity =
				prescriptionController.getPrescriptionListByPatientId(1L, mockAuthentication);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(Stream.of(prescriptionModel)
                .collect(Collectors.toList()), responseEntity.getBody());
	}

	@Test
	public void addPrescription() throws DataValidationException, RestApiException {
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		Mockito.when(prescriptionService.addPrescription(Mockito.any(PrescriptionModel.class), Mockito.anyLong(),
		                                                 Mockito.any(JwtUserDetails.class)))
		       .thenReturn(prescriptionModel);
		ResponseEntity<Object> responseEntity =
				prescriptionController.addPrescription(1123L, prescriptionModel, mockJwtUserDetails);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(prescriptionModel, responseEntity.getBody());
	}

	@Test
	public void fetchLatestPrescriptionByPatientId() throws DataValidationException, ParseException, RestApiException {
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		Mockito.when(prescriptionService.validateFetchLatestPrescriptionByPatientId(Mockito.anyLong(),
		                                                                            Mockito.any(Authentication.class)
		                                                                            ))
		       .thenReturn(prescriptionModel);
		ResponseEntity<Object> responseEntity =
				prescriptionController.fetchLatestPrescriptionByPatientId(123L, mockAuthentication);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(prescriptionModel, responseEntity.getBody());
	}

	@Test
	public void fetchActivePrescriptionListByPatientId() throws JSONException, ParseException, RestApiException, ApplicationException, DataValidationException {
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		List<PrescriptionModel> prescriptionModelList = new ArrayList<>();
		prescriptionModelList.add(prescriptionModel);
		GenericIDAndZoneVO genericIDAndZoneVO = new GenericIDAndZoneVO();
		genericIDAndZoneVO.setPatientId("147");
		genericIDAndZoneVO.setZoneId("Asia/Kolkata");
		Mockito.when(prescriptionService.fetchActivePrescriptionListByPatientId(Mockito.any(GenericIDAndZoneVO.class),
		                                                                        Mockito.any(Authentication.class)
		                                                                       ))
		       .thenReturn(prescriptionModelList);
		ResponseEntity<Object> responseEntity =
				prescriptionController.fetchActivePrescriptionListByPatientId(genericIDAndZoneVO, mockAuthentication);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(Stream.of(prescriptionModel)
                .collect(Collectors.toList()), responseEntity.getBody());
	}

	@Test
	public void fetchPrescriptionByPrescriptionId() throws DataValidationException, RestApiException {
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		Mockito.when(prescriptionService.validateFetchPrescriptionByPrescriptionId(Mockito.anyLong(), Mockito.any(Authentication.class)))
		       .thenReturn(prescriptionModel);
		ResponseEntity<Object> responseEntity =
				prescriptionController.fetchPrescriptionByPrescriptionId(123L, mockAuthentication);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(prescriptionModel, responseEntity.getBody());
	}

	/*@Test
	public void fetchPrescriptionlistBynotificationStatus() throws RestApiException {
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		List<PrescriptionModel> prescriptionModelList = new ArrayList<>();
		prescriptionModelList.add(prescriptionModel);
		Mockito.when(prescriptionService.fetchPrescriptionListByStatus(Mockito.any(Authentication.class),
		                                                                Mockito.anyString()))
		       .thenReturn(prescriptionModelList);
		ResponseEntity<Object> responseEntity =
				prescriptionController.fetchPrescriptionlistBynotificationStatus(mockAuthentication);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(Stream.of(prescriptionModel)
                .collect(Collectors.toList()), responseEntity.getBody());
	}*/

	/*@Test
	public void fetchPrescriptionlistByStatus() throws RestApiException {
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		List<PrescriptionModel> prescriptionModelList = new ArrayList<>();
		prescriptionModelList.add(prescriptionModel);
		Mockito.when(prescriptionService.fetchPrescriptionListByStatus(Mockito.any(Authentication.class),
		                                                                      Mockito.anyString()))
		       .thenReturn(prescriptionModelList);
		ResponseEntity<Object> responseEntity =
				prescriptionController.getPrescriptionListByStatus(mockAuthentication);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(Stream.of(prescriptionModel)
                .collect(Collectors.toList()), responseEntity.getBody());
	}*/

	@Test
	public void fetchPrescriptionStatusList() throws RestApiException {
		PrescriptionStatus prescriptionStatus = new PrescriptionStatus();
		prescriptionStatus.setId(1L);
		prescriptionStatus.setName("NEW");

		List<PrescriptionStatus> prescriptionStatusList = new ArrayList<>();
		prescriptionStatusList.add(prescriptionStatus);

		Mockito.when(prescriptionStatusRepository.findAll())
		       .thenReturn(prescriptionStatusList);
		ResponseEntity<Object> responseEntity = prescriptionController.fetchPrescriptionStatusList();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(Stream.of(prescriptionStatus)
                .collect(Collectors.toList()), responseEntity.getBody());
	}

	@Test
	public void updatePrescriptionStatus() throws RestApiException {
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		Mockito.when(prescriptionService.updatePrescriptionStatus(Mockito.any(PrescriptionModel.class),
		                                                          Mockito.any(Authentication.class)))
		       .thenReturn(prescriptionModel);
		ResponseEntity<Object> responseEntity =
				prescriptionController.updatePrescriptionStatus(prescriptionModel, mockAuthentication);                                             
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(prescriptionModel, responseEntity.getBody());
	}

	@Test
	public void deletePrescriptionById() throws NumberFormatException, JSONException, RestApiException {

		DeletePrescriptionVO deletePrescriptionVO = new DeletePrescriptionVO();
		deletePrescriptionVO.setMessage("sample message");
		deletePrescriptionVO.setStatus("fail");
		Mockito.when(prescriptionService.deletePrescriptionById(Mockito.anyLong()))
		       .thenReturn(deletePrescriptionVO);
		ResponseEntity<Object> responseEntity = prescriptionController.deletePrescriptionById(123L);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(deletePrescriptionVO, responseEntity.getBody());
	}
}