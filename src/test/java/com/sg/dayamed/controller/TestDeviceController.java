package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.DeviceStatus;
import com.sg.dayamed.pojo.DeviceStatusModel;
import com.sg.dayamed.pojo.DeviceStatusRequestModel;
import com.sg.dayamed.service.DeviceStatusService;
import com.sg.dayamed.util.Utility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.sg.dayamed.service.NotificationService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestDeviceController extends BaseRestApiTest {

	@InjectMocks
	private DeviceController mockDeviceController;

	@Mock
	NotificationService notificationService;

	@Mock
	Utility utility;

	@Mock
	DeviceStatusService deviceStatusService;

	@Test
	public void testGetDeviceStatus() throws  Exception{
		DeviceStatus deviceStatus=new DeviceStatus();
		DeviceStatusModel deviceStatusModel = new DeviceStatusModel();
		Mockito.when(deviceStatusService.fetchDeviceStatusByUserID(Mockito.anyLong())).thenReturn(deviceStatus);
		Mockito.when(utility.convertDosageDeviceEntityToModel(deviceStatus)).thenReturn(deviceStatusModel);
		ResponseEntity<DeviceStatusModel> responseEntity=mockDeviceController.getDeviceStatus(1l);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(deviceStatusModel, responseEntity.getBody());
	}

	@Test
	public void testAddDeviceStatus() throws  Exception{
		DeviceStatusRequestModel deviceStatusRequestModel= new DeviceStatusRequestModel();
		DeviceStatus deviceStatus = new DeviceStatus();
		Mockito.when(deviceStatusService.addDeviceStatus(Mockito.any())).thenReturn(deviceStatus);
		ResponseEntity<Object> responseEntity=mockDeviceController.addDeviceStatus(deviceStatusRequestModel);
		assertTrue(responseEntity != null);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(deviceStatus, responseEntity.getBody());
	}
}
