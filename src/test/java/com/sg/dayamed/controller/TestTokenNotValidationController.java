package com.sg.dayamed.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingReqModel;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.rest.commons.IResponseCodes;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.TokenNotValidationService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DataForImantics;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.security.*")
public class TestTokenNotValidationController extends BaseRestApiImpl {

	@InjectMocks
	private TokenNotValidationController tokenNotValidationController;

	@Mock
	private MultipartFile profileImage;

	@Mock
	private CaregiverService caregiverService;

	@Mock
	private TokenNotValidationService tokenNotValidationService;

	@Mock
	private HttpServletResponse response;

	@Mock
	private HttpServletRequest request;

	@Mock
	private Optional<String> patientId;
	
	@Mock
	ObjectMapper mockObjectMapper;

	@Test
	public void testAddCaregiverFromApp() throws Exception {
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Set<CaregiverModel> setCaregiverModel = new HashSet<>();
		setCaregiverModel.add(caregiverModel);
		Mockito.when(caregiverService.addCaregiverFromApp(Mockito.any(), Mockito.any()))
		       .thenReturn(setCaregiverModel);
		ResponseEntity<Object> responseEntity = tokenNotValidationController.addCaregiverFromApp("testData",
		                                                                                         profileImage);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(setCaregiverModel, responseEntity.getBody());
	}

	@Test
	public void testGetprofileImagetest() throws Exception {
		Mockito.when(tokenNotValidationService
				             .getDeviceImageOrmedicineImageOrProfileImagetestOrImage(Mockito.anyString(),
				                                                                     Mockito.eq(response)))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.getprofileImagetest("Test.png",
		                                                                                               response);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testFetchNDCImages() throws Exception {
		Mockito.when(tokenNotValidationService.fetchNDCImages(Mockito.anyString(), Mockito.eq(response)))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.fetchNDCImages("Test.png",
		                                                                                          response);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testGetmedicineImage() throws Exception {
		Mockito.when(tokenNotValidationService
				             .getDeviceImageOrmedicineImageOrProfileImagetestOrImage(Mockito.anyString(),
				                                                                     Mockito.eq(response)))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.getmedicineImage("Test.png",
		                                                                                            response);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testGetDeviceImage() throws Exception {
		Mockito.when(tokenNotValidationService
				             .getDeviceImageOrmedicineImageOrProfileImagetestOrImage(Mockito.anyString(),
				                                                                     Mockito.eq(response)))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.getDeviceImage("Test.png",
		                                                                                          response);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testGetImage() throws Exception {
		Mockito.when(tokenNotValidationService
				             .getDeviceImageOrmedicineImageOrProfileImagetestOrImage(Mockito.anyString(),
				                                                                     Mockito.eq(response)))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.getImage("Test.png", response);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testFetchPatientAttributes() throws Exception {
		List<DataForImantics> listDataForImantics = new ArrayList<>();
		DataForImantics dataForImantics = new DataForImantics();
		listDataForImantics.add(dataForImantics);
		Mockito.when(tokenNotValidationService.fetchPatientAttributes(Mockito.any()))
		       .thenReturn(listDataForImantics);
		ResponseEntity<Object> responseEntity = tokenNotValidationController.fetchPatientAttributes(patientId);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(listDataForImantics, responseEntity.getBody());
	}

	@Test
	public void testWebErrorLogsSave() throws Exception {
		Mockito.when(tokenNotValidationService.webErrorLogsSave(Mockito.any()))
		       .thenReturn(new RestResponse());
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.webErrorLogsSave("test");
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		RestResponse responseModel = (RestResponse) responseEntity.getBody();
		assertEquals(responseModel.getResult(), IResponseCodes.SUCCESSFUL);
	}

	@Test
	public void testGetVUCAUrlbyGSToken() throws Exception {
		Mockito.when(tokenNotValidationService.getVUCAUrlbyGSToken(Mockito.any()))
		       .thenReturn(new RestResponse());
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.getVUCAUrlbyGSToken("test");
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		RestResponse vUCAURestResponse = (RestResponse) responseEntity.getBody();
		assertEquals(vUCAURestResponse.getResult(), IResponseCodes.SUCCESSFUL);
	}

	@Test
	public void testEndMeeting() throws Exception {
		ZoomEndMeetingReqModel zoomEndMeetingReqModel = new ZoomEndMeetingReqModel();
		Mockito.when(tokenNotValidationService.endMeeting(Mockito.any(), Mockito.anyString()))
		       .thenReturn(new Integer(200));
		ResponseEntity<Object> responseEntity = tokenNotValidationController.endMeeting(request,
		                                                                                zoomEndMeetingReqModel);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testAddImanticNotification() throws Exception {
		Mockito.when(tokenNotValidationService.addImanticNotification(Mockito.any()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<RestResponse> responseEntity = tokenNotValidationController.addImanticNotification(
				TestResponseGenerator.genrateImanticNotificationReqModel());
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}
}
