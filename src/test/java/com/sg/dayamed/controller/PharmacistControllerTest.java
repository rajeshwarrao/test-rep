package com.sg.dayamed.controller;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.PharmaService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.StringResponse;
import com.sg.dayamed.util.TestResponseGenerator;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class PharmacistControllerTest extends BaseRestApiTest {

	@Mock
	private PharmacistService mockPharmacistService;

	@Mock
	private PharmaService pharmaService;

	@InjectMocks
	private PharmacistController pharmacistController;

	@Test
	public void testFetchPatientsByPharmacistIdFromCookie() throws Exception {
		List<PatientModel> listPatientModel = Stream.of(TestResponseGenerator.generatePatientModel())
		                                            .collect(Collectors.toList());
		Mockito.when(mockPharmacistService.getPatientsByPharmacistIdentifier(Mockito.any()))
		       .thenReturn(listPatientModel);
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(TestResponseGenerator.generateUserDetails("Male", "Asian",
				                                                                                       ApplicationConstants.PATIENT),
				                                             true, ApplicationConstants.PATIENT);
		ResponseEntity<Object> responseEntity =
				pharmacistController.getPatientsByPharmacistIdentifier(jwtUserDetails);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), listPatientModel);
	}

	@Test
	public void testFetchPharmacists() throws Exception{
		List<PharmacistModel> listPharmacistModel = Stream.of(TestResponseGenerator.generatePharmacistModel())
		                                                  .collect(Collectors.toList());
		Mockito.when(pharmaService.fetchPharmasists()).thenReturn(listPharmacistModel);
		ResponseEntity<Object> responseEntity =
				pharmacistController.fetchPharmacists();
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), listPharmacistModel);
	}

	@Test
	public void testDeletePharmacist()throws  Exception {
		Mockito.when(pharmaService.deletePharmacistValidation(Mockito.anyLong())).thenReturn(ApplicationConstants.SUCCESS);
		ResponseEntity<Object> responseEntity =
				pharmacistController.deletePharmacist(1l);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		StringResponse actualResponse =(StringResponse) responseEntity.getBody();
		assertEquals(actualResponse.getResponse(), ApplicationConstants.SUCCESS);
	}

	@Test
	public void testFetchPharmacistsById()throws  Exception {
		PharmacistModel pharmacistModel= TestResponseGenerator.generatePharmacistModel();
		Mockito.when(pharmaService.fetchPharmacistById(Mockito.anyLong())).thenReturn(pharmacistModel);
		ResponseEntity<Object> responseEntity =
				pharmacistController.fetchPharmacistsById(1l);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), pharmacistModel);
	}

	@Test
	public void testFetchPharmacistsByUserId()throws  Exception {
		PharmacistModel pharmacistModel= TestResponseGenerator.generatePharmacistModel();
		Mockito.when(mockPharmacistService.fetchPharmacistByUserId(Mockito.anyLong())).thenReturn(pharmacistModel);
		ResponseEntity<Object> responseEntity =
				pharmacistController.fetchPharmacistsByUserId(1l);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), pharmacistModel);
	}


	@Test
	public void testAddPharmacist() throws Exception {
		PharmacistModel pharmacistModel= TestResponseGenerator.generatePharmacistModel();
		Mockito.when(pharmaService.addPharmacistValidation(Mockito.any(), Mockito.any())).thenReturn(pharmacistModel);
		ResponseEntity<Object> responseEntity =
				pharmacistController.addPharmacist("test", null);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), pharmacistModel);
	}

	@Test
	public void testFetchPharmacistsByPatientId() throws Exception {
		List<PharmacistModel> listPharmacistModel = Stream.of(TestResponseGenerator.generatePharmacistModel())
		                                                  .collect(Collectors.toList());
		Mockito.when(mockPharmacistService.fetchPharmacistListByPatientId(Mockito.anyLong())).thenReturn(listPharmacistModel);
		ResponseEntity<Object> responseEntity =
				pharmacistController.fetchPharmacistsByPatientId(1l);
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
		assertEquals(responseEntity.getBody(), listPharmacistModel);
	}
}
