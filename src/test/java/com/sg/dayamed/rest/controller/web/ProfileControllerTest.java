package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PasswordChangeReqModel;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestFault;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSPasswordChangeResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class ProfileControllerTest {

	@InjectMocks
	private ProfileController profileController;

	@Mock
	private UserDetailsService mockUserDetailsService;

	@Test
	public void testChangePasswordFailInvalidCurrentPassword() throws Exception {
		/**
		 * Case 1 : Fail with Invalid current password case.
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("male", "Black", "patient");
		userDetails.setPassword("8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918");
		JwtUserDetails jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(userDetails, false, "patient");
		PasswordChangeReqModel reqModel = new PasswordChangeReqModel();
		reqModel.setCurrentPassword("8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A9181324");
		reqModel.setNewPassword("");
		doThrow(new DataValidationException(APIErrorFields.CURRENT_CREDENTIAL,
		                                    APIErrorKeys.ERRORS_INVALID_CURRENT_CREDENTIALS))
				.when(mockUserDetailsService)
				.changePassword(any(), anyLong());
		try {
			profileController.changePassword(jwtUserDetails, reqModel);
			fail();
		} catch (RestApiException apiException) {
			final RestFault faultInfo = apiException.getFaultInfo();
			assertEquals(APIErrorFields.CURRENT_CREDENTIAL, faultInfo.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_CURRENT_CREDENTIALS, faultInfo.getErrorKey());
		}
	}

	@Test
	public void testChangePasswordSuccess() throws RestApiException {
		/**
		 * Case 1 : Success case.
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("male", "Black", "patient");
		userDetails.setPassword("8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918");
		JwtUserDetails jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(userDetails, false, "patient");
		PasswordChangeReqModel reqModel = new PasswordChangeReqModel();
		reqModel.setCurrentPassword("8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918");
		reqModel.setNewPassword("yyyyyyyyy");
		ResponseEntity<RestResponse> responseEntity = profileController.changePassword(jwtUserDetails, reqModel);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		WSPasswordChangeResponse responseModel = (WSPasswordChangeResponse) responseEntity.getBody();
		assertNotNull(responseModel);
		assertEquals(ApplicationConstants.SUCCESS, responseModel.getStatus());
		assertEquals("Password changed successfully.", responseModel.getMessage());
	}

}
