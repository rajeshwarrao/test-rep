package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static com.sg.dayamed.util.TestResponseGenerator.generateJwtUserDetails;
import static com.sg.dayamed.util.TestResponseGenerator.generateUserDetails;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;

@RunWith(MockitoJUnitRunner.class)
public class UserPreferenceControllerTest {

	@InjectMocks
	private UserPreferenceController userPreferenceController;

	@Mock
	private UserPreferenceService userPreferenceService;

	private JwtUserDetails userDetails;

	@Before
	public void setUp() throws Exception {
		final UserDetails userDetails = generateUserDetails("male", "Black", "patient");
		this.userDetails = generateJwtUserDetails(userDetails, false, "patient");
	}

	@Test
	public void testGetUserPreferences() throws Exception {
		final UserPreferenceModel userSettings = TestResponseGenerator.generateUserPreferenceModel();
		Mockito.when(userPreferenceService.findUserSettingsByUserId(anyLong()))
		       .thenReturn(userSettings);

		final ResponseEntity<Object> userPreferencesResponse =
				userPreferenceController.getUserPreferences(userDetails);

		final UserPreferenceModel userSettingsResponse = (UserPreferenceModel) userPreferencesResponse.getBody();
		assertTrue(userSettingsResponse.getEmailFlag());
		assertTrue(userSettingsResponse.getSmsFlag());
		assertTrue(userSettingsResponse.getPushNotificationFlag());
		assertEquals("EAR", userSettingsResponse.getFirstConsumption());
		assertEquals(ApplicationConstants.LANG_PREF_DEFAULT, userSettingsResponse.getLanguagePreference());
	}

	@Test
	public void testUpdateUserPreferences() throws Exception {
		final UserPreferenceModel userSettings = TestResponseGenerator.generateUserPreferenceModel();
		Mockito.when(userPreferenceService.updateUserPreference(anyLong(), any()))
		       .thenReturn(userSettings);

		final ResponseEntity<Object> userPreferencesResponse =
				userPreferenceController.updateUserPreferences(userDetails, new UserPreferenceModel());

		final UserPreferenceModel userSettingsResponse = (UserPreferenceModel) userPreferencesResponse.getBody();
		assertTrue(userSettingsResponse.getEmailFlag());
		assertTrue(userSettingsResponse.getSmsFlag());
		assertTrue(userSettingsResponse.getPushNotificationFlag());
		assertEquals("EAR", userSettingsResponse.getFirstConsumption());
		assertEquals(ApplicationConstants.LANG_PREF_DEFAULT, userSettingsResponse.getLanguagePreference());
	}
}
