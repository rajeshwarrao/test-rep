package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSAddress;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetails;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetailsResponse;
import com.sg.dayamed.rest.controller.ws.users.WSUserDetails;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.v1.PharmacistService;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsVO;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import ma.glasnost.orika.MapperFacade;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 03/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPharmacistController extends BaseRestApiTest {

	@InjectMocks
	private PharmacistController pharmacistController;

	@Mock
	private PharmacistService mockPharmacistService;

	@Mock
	private MapperFacade mockMapperFacade;

	private UserDetails userDetails;

	private JwtUserDetails jwtUserDetails;

	@Before
	public void setup() {
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PHARMACIST.getRole());
	}

	@Test
	public void testLoadPatients() throws Exception {
		PatientDetailsResponseVO responseVO = TestResponseGenerator.generaPatientDetailsResponseVO(10, 20);
		for (PatientDetailsVO patientDetailsVO : responseVO.getPatientDetails()) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(patientDetailsVO), Mockito.any()))
			       .thenReturn(toWsPatientDetails(patientDetailsVO));
		}
		Mockito.when(mockPharmacistService.getPatientsForGSEnabled(Mockito.any(), Mockito.anyString(), Mockito.any()))
		       .thenReturn(responseVO);
		WSPatientDetailsResponse response = new WSPatientDetailsResponse();
		response.setTotalRecords(responseVO.getTotalRecords());
		response.setPatients(responseVO.getPatientDetails()
		                               .stream()
		                               .map(detail -> toWsPatientDetails(detail))
		                               .collect(Collectors.toList()));
		String responseText = objectMapper.writeValueAsString(response);
		Mockito.when(mockObjectMapper.writeValueAsString(Mockito.any(WSPatientDetailsResponse.class)))
		       .thenReturn(responseText);
		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(WSPatientDetailsResponse.class)))
		       .thenReturn(response);
		ResponseEntity<RestResponse> responseEntity =
				pharmacistController.loadPatients(jwtUserDetails, 0, 10, PatientSortByEnum.FIRST_NAME.getSortBy(),
				                                  SortOrderEnum.DESC.getType(), "new", "query");
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		WSPatientDetailsResponse actualResponse = (WSPatientDetailsResponse) responseEntity.getBody();
		assertEquals(20, actualResponse.getTotalRecords()
		                               .intValue());
		assertEquals(10, actualResponse.getPatients()
		                               .size());
		assertEquals(response.getPatients(), actualResponse.getPatients());
	}

	private WSPatientDetails toWsPatientDetails(PatientDetailsVO detailsVO) {
		WSPatientDetails details = new WSPatientDetails();
		details.setId(detailsVO.getId());
		details.setEmoji(detailsVO.getEmoji());
		details.setPatientStatus(detailsVO.getPatientStatus());
		UserDetailsVO userDetailsVO = detailsVO.getUserDetails();
		if (userDetailsVO != null) {
			WSUserDetails wsUserDetails = new WSUserDetails();
			wsUserDetails.setAge(userDetailsVO.getAge() != null ? String.valueOf(userDetailsVO.getAge()) : null);
			wsUserDetails.setFirstName(userDetailsVO.getFirstName());
			wsUserDetails.setLastName(userDetailsVO.getLastName());
			wsUserDetails.setMiddleName(userDetailsVO.getMiddleName());
			wsUserDetails.setEmailFlag(userDetailsVO.getEmailFlag());
			wsUserDetails.setSmsFlag(userDetailsVO.getSmsFlag());
			wsUserDetails.setEmailId(userDetailsVO.getEmailId());
			wsUserDetails.setCreatedOn(userDetailsVO.getUserCreatedOn());
			wsUserDetails.setLastActiveTime(userDetailsVO.getLastActiveTime());
			WSAddress address = new WSAddress();
			address.setCity(userDetailsVO.getCity());
			address.setCountry(userDetailsVO.getCountry());
			address.setLocation(userDetailsVO.getLocation());
			address.setState(userDetailsVO.getState());
			address.setZipCode(userDetailsVO.getZipCode());
			wsUserDetails.setAddress(address);
			details.setUserDetails(wsUserDetails);
		}
		return details;
	}
}
