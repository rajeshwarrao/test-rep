package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.rest.commons.IResponseCodes;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestFault;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSAddress;
import com.sg.dayamed.rest.controller.ws.users.WSPatientAssociationDetailsResponse;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetails;
import com.sg.dayamed.rest.controller.ws.users.WSPatientDetailsResponse;
import com.sg.dayamed.rest.controller.ws.users.WSUserDetails;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.service.v1.PatientAssociationDetailsService;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import ma.glasnost.orika.MapperFacade;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPatientController extends BaseRestApiTest {

	@InjectMocks
	private PatientController patientController = new PatientController();

	@Mock
	private PatientAssociationDetailsService mockAssociationDetailsService;

	@Mock
	private MapperFacade mockMapperFacade;

	private JwtUserDetails jwtUserDetails;

	@Before
	public void setUp() {
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
	}

	@Test
	public void testGetPatientsException() throws Exception {
		DataValidationException dve =
				new DataValidationException(APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_INVALID_ROLE);
		ApplicationException applicationException = new ApplicationException(dve);

		try {
			Mockito.when(mockAssociationDetailsService.getPatients(Mockito.any()))
			       .thenThrow(applicationException);
			patientController.loadPatients(jwtUserDetails, 0, 10, PatientSortByEnum.ACTIVE_TIME.getSortBy(),
			                               SortOrderEnum.ASC.getType(), false, 1L, 1L, "test");
		} catch (RestApiException e) {
			RestFault restFault = e.getFaultInfo();
			assertEquals(IResponseCodes.DATA_VALIDATION_ERROR, restFault.getResult());
			assertEquals(APIErrorFields.AUTHORIZATION, restFault.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_ROLE, restFault.getErrorKey());
		}
	}

	@Test
	public void testGetPatients() throws Exception {
		PatientDetailsResponseVO responseVO = TestResponseGenerator.generaPatientDetailsResponseVO(10, 20);
		for (PatientDetailsVO patientDetailsVO : responseVO.getPatientDetails()) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(patientDetailsVO), Mockito.any()))
			       .thenReturn(toWsPatientDetails(patientDetailsVO));
		}
		Mockito.when(mockAssociationDetailsService.getPatients(Mockito.any()))
		       .thenReturn(responseVO);
		WSPatientDetailsResponse response = new WSPatientDetailsResponse();
		response.setTotalRecords(responseVO.getTotalRecords());
		response.setPatients(responseVO.getPatientDetails()
		                               .stream()
		                               .map(detail -> toWsPatientDetails(detail))
		                               .collect(Collectors.toList()));
		String responseText = objectMapper.writeValueAsString(response);
		Mockito.when(mockObjectMapper.writeValueAsString(Mockito.any(WSPatientDetailsResponse.class)))
		       .thenReturn(responseText);
		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(WSPatientDetailsResponse.class)))
		       .thenReturn(response);
		ResponseEntity responseEntity =
				patientController.loadPatients(jwtUserDetails, 0, 10, PatientSortByEnum.ACTIVE_TIME.getSortBy(),
				                               SortOrderEnum.ASC.getType(), false, 1L, 1L, "test");
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		WSPatientDetailsResponse actualResponse = (WSPatientDetailsResponse) responseEntity.getBody();
		assertEquals(20, actualResponse.getTotalRecords()
		                               .intValue());
		assertEquals(10, actualResponse.getPatients()
		                               .size());
		assertEquals(response.getPatients(), actualResponse.getPatients());
	}

	@Test
	public void testSearchPatientsErrorCase() throws Exception {
		DataValidationException validationException =
				new DataValidationException(APIErrorFields.QUERY_STRING, APIErrorKeys.ERRORS_INVALID_LENGTH_FOR_QUERY,
				                            new Object[]{3});
		ApplicationException applicationException = new ApplicationException(validationException);
		try {
			Mockito.when(mockAssociationDetailsService.searchPatients(Mockito.any(), Mockito.anyString()))
			       .thenThrow(applicationException);
			patientController.searchPatients(jwtUserDetails, 0, 10, "text", PatientSortByEnum.ACTIVE_TIME.getSortBy(),
			                                 SortOrderEnum.ASC.getType(), 1L, 1L);
		} catch (RestApiException e) {
			RestFault restFault = e.getFaultInfo();
			assertEquals(IResponseCodes.DATA_VALIDATION_ERROR, restFault.getResult());
			assertEquals(APIErrorFields.QUERY_STRING, restFault.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_LENGTH_FOR_QUERY, restFault.getErrorKey());
		}
	}

	@Test
	public void testSearchPatients() throws Exception {
		PatientDetailsResponseVO responseVO = TestResponseGenerator.generaPatientDetailsResponseVO(10, 20);
		for (PatientDetailsVO patientDetailsVO : responseVO.getPatientDetails()) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(patientDetailsVO), Mockito.any()))
			       .thenReturn(toWsPatientDetails(patientDetailsVO));
		}
		Mockito.when(mockAssociationDetailsService.searchPatients(Mockito.any(), Mockito.anyString()))
		       .thenReturn(responseVO);
		WSPatientDetailsResponse response = new WSPatientDetailsResponse();
		response.setTotalRecords(responseVO.getTotalRecords());
		response.setPatients(responseVO.getPatientDetails()
		                               .stream()
		                               .map(detail -> toWsPatientDetails(detail))
		                               .collect(Collectors.toList()));
		String responseText = objectMapper.writeValueAsString(response);
		Mockito.when(mockObjectMapper.writeValueAsString(Mockito.any(WSPatientDetailsResponse.class)))
		       .thenReturn(responseText);
		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(WSPatientDetailsResponse.class)))
		       .thenReturn(response);
		ResponseEntity responseEntity = patientController.searchPatients(jwtUserDetails, 0, 10, "text",
		                                                                 PatientSortByEnum.ACTIVE_TIME.getSortBy(),
		                                                                 SortOrderEnum.ASC.getType(), 1L, 1L);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		WSPatientDetailsResponse actualResponse = (WSPatientDetailsResponse) responseEntity.getBody();
		assertEquals(20, actualResponse.getTotalRecords()
		                               .intValue());
		assertEquals(10, actualResponse.getPatients()
		                               .size());
		assertEquals(response.getPatients(), actualResponse.getPatients());
	}

	@Test
	public void testGetPatientAssociationsErrors() throws Exception {
		DataValidationException validationException =
				new DataValidationException(APIErrorFields.QUERY_STRING, APIErrorKeys.ERRORS_INVALID_LENGTH_FOR_QUERY,
				                            new Object[]{3});
		ApplicationException applicationException = new ApplicationException(validationException);
		try {
			Mockito.when(mockAssociationDetailsService.getPatientAssociations(Mockito.any()))
			       .thenThrow(applicationException);
			patientController.getPatientAssociations(jwtUserDetails, 1L, 1L, 1L);
		} catch (RestApiException e) {
			RestFault restFault = e.getFaultInfo();
			assertEquals(IResponseCodes.DATA_VALIDATION_ERROR, restFault.getResult());
			assertEquals(APIErrorFields.QUERY_STRING, restFault.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_LENGTH_FOR_QUERY, restFault.getErrorKey());
		}
	}

	@Test
	public void testGetPatientAssociations() throws Exception {
		PatientDetailsResponseVO responseVO = TestResponseGenerator.generaPatientDetailsResponseVO(10, 20);
		PatientAssociationsVO associationsVO = new PatientAssociationsVO();
		associationsVO.setPatient(responseVO.getPatientDetails()
		                                    .get(0));
		for (PatientDetailsVO patientDetailsVO : responseVO.getPatientDetails()) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(patientDetailsVO), Mockito.any()))
			       .thenReturn(toWsPatientDetails(patientDetailsVO));
		}
		Mockito.when(mockAssociationDetailsService.getPatientAssociations(Mockito.any()))
		       .thenReturn(associationsVO);
		WSPatientAssociationDetailsResponse response = new WSPatientAssociationDetailsResponse();
		response.setPatient(toWsPatientDetails(responseVO.getPatientDetails()
		                                                 .get(0)));
		String responseText = objectMapper.writeValueAsString(response);
		Mockito.when(mockObjectMapper.writeValueAsString(Mockito.any(WSPatientDetails.class)))
		       .thenReturn(responseText);
		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(WSPatientDetails.class)))
		       .thenReturn(response.getPatient());
		ResponseEntity<RestResponse> responseEntity =
				patientController.getPatientAssociations(jwtUserDetails, 1L, 1L, 1L);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	private WSPatientDetails toWsPatientDetails(PatientDetailsVO detailsVO) {
		WSPatientDetails details = new WSPatientDetails();
		details.setId(detailsVO.getId());
		details.setEmoji(detailsVO.getEmoji());
		details.setPatientStatus(detailsVO.getPatientStatus());
		UserDetailsVO userDetailsVO = detailsVO.getUserDetails();
		if (userDetailsVO != null) {
			WSUserDetails wsUserDetails = new WSUserDetails();
			wsUserDetails.setAge(userDetailsVO.getAge() != null ? String.valueOf(userDetailsVO.getAge()) : null);
			wsUserDetails.setFirstName(userDetailsVO.getFirstName());
			wsUserDetails.setLastName(userDetailsVO.getLastName());
			wsUserDetails.setMiddleName(userDetailsVO.getMiddleName());
			wsUserDetails.setEmailFlag(userDetailsVO.getEmailFlag());
			wsUserDetails.setSmsFlag(userDetailsVO.getSmsFlag());
			wsUserDetails.setEmailId(userDetailsVO.getEmailId());
			wsUserDetails.setCreatedOn(userDetailsVO.getUserCreatedOn());
			wsUserDetails.setLastActiveTime(userDetailsVO.getLastActiveTime());
			WSAddress address = new WSAddress();
			address.setCity(userDetailsVO.getCity());
			address.setCountry(userDetailsVO.getCountry());
			address.setLocation(userDetailsVO.getLocation());
			address.setState(userDetailsVO.getState());
			address.setZipCode(userDetailsVO.getZipCode());
			wsUserDetails.setAddress(address);
			details.setUserDetails(wsUserDetails);
		}
		return details;
	}

}
