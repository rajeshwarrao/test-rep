package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.NDCImageModel;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.MedicineService;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MedicineControllerTest {

	@InjectMocks
	private MedicineController medicineController;

	@Mock
	private MedicineService medicineService;

	@Mock
	private JwtUserDetails jwtUserDetails;

	@Test
	public void testImageUpdatedSuccessfully() throws Exception {
		doNothing().when(medicineService)
		           .updateImages(any());
		final ResponseEntity<Object> updateImageResponse =
				medicineController.updateMedicineImageURL(new NDCImageModel());
		assertEquals(HttpStatus.NO_CONTENT, updateImageResponse.getStatusCode());
	}

	@Test
	public void testFetchMedicines() throws Exception {
		final Medicine medicine = TestResponseGenerator.generateMedicine();
		final Page<Medicine> resultPage = new PageImpl<>(Stream.of(medicine)
		                                                       .collect(Collectors.toList()));
		when(medicineService.getMedicines(medicine.getName(), 0, 10)).thenReturn(resultPage);
		final ResponseEntity<Object> responses =
				medicineController.fetchMedicines(jwtUserDetails, medicine.getName(), 0, 10);
		Page<Medicine> medicineList = (Page<Medicine>) responses.getBody();
		assertEquals(1, medicineList.getTotalElements());
		assertEquals(medicine.getName(), medicineList.getContent()
		                                             .get(0)
		                                             .getName());
	}

	@Test
	public void testImageUpdateThrowsError() throws IOException, DataValidationException {
		doThrow(new DataValidationException(APIErrorFields.IMAGE_PATH, APIErrorKeys.ERROR_NO_IMAGE_IN_IMAGE_DIRECTORY,
		                                    new String[]{"/temp/path"}))
				.when(medicineService)
				.updateImages(any());
		try {
			medicineController.updateMedicineImageURL(new NDCImageModel());
			fail();
		} catch (RestApiException exception) {
			assertEquals(APIErrorFields.IMAGE_PATH, exception.getFaultInfo()
			                                                 .getFieldName());
			assertEquals(APIErrorKeys.ERROR_NO_IMAGE_IN_IMAGE_DIRECTORY, exception.getFaultInfo()
			                                                                      .getErrorKey());
		}
	}
}
