package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.util.TwilioVideoModel;
import com.sg.dayamed.util.TwilioVideoService;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static com.sg.dayamed.util.TestResponseGenerator.generateJwtUserDetails;
import static com.sg.dayamed.util.TestResponseGenerator.generateUserDetails;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class TwilioVideoControllerTest {

	@InjectMocks
	private TwilioVideoController twilioVideoController;

	@Mock
	private TwilioVideoService videoService;

	private JwtUserDetails jwtUserDetails;

	private TwilioVideoModel videoModel = new TwilioVideoModel();

	private static final String TOKEN = RandomStringUtils.randomAlphabetic(20);

	@Before
	public void setUp() throws Exception {
		final UserDetails userDetails = generateUserDetails("male", "Black", "patient");
		jwtUserDetails = generateJwtUserDetails(userDetails, false, "patient");
		videoModel.setToken(TOKEN);
		videoModel.setIdentity(jwtUserDetails.getUserEmailId());
	}

	@Test
	public void testFetchTwilioAuthToken() throws RestApiException {
		Mockito.when(videoService.generateAuthToken(anyString()))
		       .thenReturn(videoModel);
		final ResponseEntity<Object> twilioAuthTokenResponse =
				twilioVideoController.fetchTwilioAuthToken(jwtUserDetails);
		final TwilioVideoModel generatedToken = (TwilioVideoModel) twilioAuthTokenResponse.getBody();
		assertEquals(videoModel.getToken(), generatedToken.getToken());
		assertEquals(videoModel.getIdentity(), jwtUserDetails.getUserEmailId());
	}
}
