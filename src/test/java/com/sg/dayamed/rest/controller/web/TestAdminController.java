package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestFault;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.controller.ws.users.WSUserDetailsResponse;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.vo.users.ProviderVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsResponseVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 01/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestAdminController extends BaseRestApiTest {

	@InjectMocks
	private AdminController adminController;

	@Mock
	private UserPreferencesVService mockUserPreferencesVService;

	@Mock
	private UserValidator mockUserValidator;

	private UserDetails userDetails;

	private JwtUserDetails jwtUserDetails;

	@Before
	public void setup() {
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.ADMIN.getRole());
		jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.ADMIN.getRole());
	}

	@Test
	public void testLoadUsers() throws Exception {
		/**
		 * Case 1 : Only has provider data and requesting for provider
		 */
		Mockito.doNothing()
		       .when(mockUserValidator)
		       .validateUserRole(Mockito.anyString());
		List<ProviderVO> providerVOS = IntStream.range(0, 15)
		                                        .mapToObj(index -> TestResponseGenerator.generateProviderVO())
		                                        .collect(Collectors.toList());
		UserDetailsResponseVO responseVO = new UserDetailsResponseVO();
		responseVO.setTotalRecords(providerVOS.size());
		responseVO.setProviders(providerVOS);
		Mockito.when(mockUserPreferencesVService.getUserPreferences(Mockito.eq(UserRoleEnum.PROVIDER), Mockito.any()))
		       .thenReturn(responseVO);
		ResponseEntity<RestResponse> responseEntity =
				adminController.loadUsers(jwtUserDetails, 0, 1, PatientSortByEnum.FIRST_NAME.getSortBy(),
				                          SortOrderEnum.DESC.getType(),
				                          "Test", UserRoleEnum.PROVIDER.getRole());
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		WSUserDetailsResponse response = (WSUserDetailsResponse) responseEntity.getBody();
		assertNotNull(response);
		assertEquals(15, response.getTotalRecords()
		                         .intValue());
		assertTrue(CollectionUtils.isNotEmpty(response.getProviders()));
		assertTrue(CollectionUtils.isEmpty(response.getCaregivers()));
		assertTrue(CollectionUtils.isEmpty(response.getPharmacists()));

		/**
		 * Case 2 : Only has provider data requesting for caregiver
		 */
		Mockito.when(mockUserPreferencesVService.getUserPreferences(Mockito.eq(UserRoleEnum.CAREGIVER), Mockito.any()))
		       .thenReturn(new UserDetailsResponseVO());
		responseEntity = adminController.loadUsers(jwtUserDetails, 0, 1, PatientSortByEnum.FIRST_NAME.getSortBy(),
		                                           SortOrderEnum.DESC.getType(),
		                                           "Test", UserRoleEnum.CAREGIVER.getRole());
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		response = (WSUserDetailsResponse) responseEntity.getBody();
		assertNotNull(response);
		assertEquals(0, response.getTotalRecords()
		                        .intValue());
		assertTrue(CollectionUtils.isEmpty(response.getProviders()));
		assertTrue(CollectionUtils.isEmpty(response.getCaregivers()));
		assertTrue(CollectionUtils.isEmpty(response.getPharmacists()));
	}

	@Test
	public void testLoadUsersErrorCase() throws Exception {
		DataValidationException exception =
				new DataValidationException(APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_INVALID_ROLE);
		;
		Mockito.doThrow(exception)
		       .when(mockUserValidator)
		       .validateUserRole(Mockito.anyString());
		try {
			adminController.loadUsers(jwtUserDetails, 0, 1, PatientSortByEnum.FIRST_NAME.getSortBy(),
			                          SortOrderEnum.DESC.getType(),
			                          "Test", UserRoleEnum.CAREGIVER.getRole());
			Assert.fail();
		} catch (RestApiException e) {
			assertNotNull(e);
			RestFault restFault = (RestFault) e.getFaultInfo();
			assertNotNull(restFault);
			assertEquals(APIErrorFields.AUTHORIZATION, restFault.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_ROLE, restFault.getErrorKey());
		}
	}

}
