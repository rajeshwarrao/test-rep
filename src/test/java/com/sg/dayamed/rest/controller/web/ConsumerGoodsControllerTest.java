package com.sg.dayamed.rest.controller.web;

import com.sg.dayamed.pojo.ConsumerGoodsModel;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.service.ConsumerGoodsService;
import com.sg.dayamed.util.TestResponseGenerator;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConsumerGoodsControllerTest {

	@Mock
	private ConsumerGoodsService service;

	@InjectMocks
	private ConsumerGoodsController consumerGoodsController;

	@Test
	public void testFetchConsumerGoodsByType() throws RestApiException {
		final ConsumerGoodsModel glucoseMonitor =
				TestResponseGenerator.generateConsumerGoodsModel(NumberUtils.LONG_ONE, "glucose monitor");
		final ConsumerGoodsModel bpMonitor = TestResponseGenerator.generateConsumerGoodsModel(2L, "bpMonitor");
		when(service.getConsumerGoodsByType(anyString())).thenReturn(Stream.of(glucoseMonitor, bpMonitor)
		                                                                   .collect(Collectors.toList()));
		final ResponseEntity<Object> goods = consumerGoodsController.fetchConsumerGoodsByType("electronic");
		assertEquals(HttpStatus.OK, goods.getStatusCode());
		final List<ConsumerGoodsModel> goodsResponse = (List<ConsumerGoodsModel>) goods.getBody();

		assertEquals(2, goodsResponse.size());

		final ConsumerGoodsModel firstItem = goodsResponse.get(0);
		assertEquals(glucoseMonitor.getId(), firstItem.getId());
		assertEquals(glucoseMonitor.getName(), firstItem.getName());

		final ConsumerGoodsModel secondItem = goodsResponse.get(1);
		assertEquals(bpMonitor.getId(), secondItem.getId());
		assertEquals(bpMonitor.getName(), secondItem.getName());
	}
}
