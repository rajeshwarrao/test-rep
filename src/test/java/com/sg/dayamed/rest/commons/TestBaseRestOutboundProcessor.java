package com.sg.dayamed.rest.commons;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.rest.commons.BaseRestOutboundProcessor;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DayamedObjectMapper;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.OptionalInt;
import java.util.UUID;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class TestBaseRestOutboundProcessor extends BaseRestApiTest {

	@InjectMocks
	private BaseRestOutboundProcessor baseRestOutboundProcessor;

	private RestTemplate restTemplate = null;

	@Rule
	public WireMockRule wireMockRule = new WireMockRule(8901);

	@Before
	public void setup() {
		restTemplate = new RestTemplate();
		OptionalInt index = IntStream.range(0, restTemplate.getMessageConverters()
		                                                   .size())
		                             .filter(i -> restTemplate.getMessageConverters()
		                                                      .get(i) instanceof MappingJackson2HttpMessageConverter)
		                             .findFirst();

		if (index.isPresent()) {
			restTemplate.getMessageConverters()
			            .set(index.getAsInt(), new MappingJackson2HttpMessageConverter(new DayamedObjectMapper()));
		}
		restTemplate.getMessageConverters()
		            .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		ReflectionTestUtils.setField(baseRestOutboundProcessor, "restTemplate", restTemplate);
	}

	@Test
	public void testRestOutboundCalls() {
		wireMockRule.stubFor(WireMock.post(WireMock.urlEqualTo("/test"))
		                             .withRequestBody(WireMock.equalTo("reqBody"))
		                             .willReturn(WireMock.aResponse()
		                                                 .withBody(ApplicationConstants.SUCCESS)
		                                                 .withStatus(HttpStatus.OK.value())
		                                                 .withHeader("Content-Type",
		                                                             "application/json;charset=UTF-8")));
		Map<String, String> headers = new HashMap<>();
		headers.put("Authorization", "key=" + UUID.randomUUID().toString());
		ResponseEntity<String> result = baseRestOutboundProcessor.post("http://localhost:8901/test",
		                                                               "reqBody", String.class, headers);
		assertNotNull(result);
		assertEquals(HttpStatus.OK.value(), result.getStatusCode().value());
		assertEquals(result.getBody(), ApplicationConstants.SUCCESS);
	}
}
