package com.sg.dayamed.rest.commons;

import com.sg.dayamed.exceptions.ApplicationException;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 21/Jun/2019
 **/

public class TestRestApiException {

	@Test
	public void testRestApiException() {
		assertNotNull(RestApiException.createNoPrivilegeFault());
		assertNotNull(RestApiException.createApplicationExceptionFault(new ApplicationException()));
		assertNotNull(new RestApiException("string", new RestFault()));
		assertNotNull(new RestApiException());
		RestApiException apiException = new RestApiException();
		apiException.getMethod();
	}
}
