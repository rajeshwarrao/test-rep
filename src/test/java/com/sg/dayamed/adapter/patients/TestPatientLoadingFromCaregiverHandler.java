package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;
import com.sg.dayamed.repository.view.PatientCaregiverAssociationsVRepository;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import ma.glasnost.orika.MapperFacade;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/

@RunWith(MockitoJUnitRunner.class)
public class TestPatientLoadingFromCaregiverHandler {

	@InjectMocks
	private PatientLoadingFromCaregiverHandler loadingFromCaregiverHandler;

	@Mock
	private PatientLoadingFromCaregiverHandler mockSelf;

	@Mock
	private MapperFacade mockMapperFacade;

	@Mock
	private PatientCaregiverAssociationsVRepository mockCaregiverAssociationsVRepository;

	@After
	public void tearDown() {
		Mockito.reset(mockCaregiverAssociationsVRepository, mockMapperFacade, mockSelf);
	}

	@Test
	public void testGetUserTypeHandler() {
		BasePatientInitLoadingHandler handler = loadingFromCaregiverHandler.getUserTypeHandler();
		assertNotNull(handler);
		assertTrue(handler instanceof PatientLoadingFromCaregiverHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] actuals = loadingFromCaregiverHandler.getUserTypes();
		assertEquals(UserRoleEnum.CAREGIVER.getRole(), actuals[0]);
	}

	@Test
	public void testGetPatientAssociationDetails() throws Exception {
		/**
		 * Case 1 : Success response
		 */
		List<PatientCaregiverAssociationsV> associationDetailsVS = IntStream.range(0, 15)
		                                                                    .mapToObj(
				                                                                    index -> TestResponseGenerator.generatePatientCaregiverAssociationsV())
		                                                                    .collect(Collectors.toList());
		Mockito.when(mockCaregiverAssociationsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(associationDetailsVS);
		for (PatientCaregiverAssociationsV detailsV : associationDetailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.any(PatientCaregiverAssociationsV.class), Mockito.any()))
			       .thenReturn(toPatientAssociationDetailsV(detailsV));
		}
		List<PatientAssociationDetailsV> detailsVS =
				loadingFromCaregiverHandler.getPatientAssociationDetails(UserRoleEnum.CAREGIVER,
				                                                         TestResponseGenerator.generateLoadPatientRequest(
						                                                         UserRoleEnum.CAREGIVER));
		assertNotNull(detailsVS);
		assertEquals(associationDetailsVS.size(), detailsVS.size());

		/**
		 * Case 2 : Exception occurred
		 */
		Mockito.reset(mockCaregiverAssociationsVRepository);
		Mockito.when(mockCaregiverAssociationsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenThrow(new RuntimeException());
		try {
			loadingFromCaregiverHandler.getPatientAssociationDetails(UserRoleEnum.CAREGIVER,
			                                                         TestResponseGenerator.generateLoadPatientRequest(
					                                                         UserRoleEnum.CAREGIVER));
			Assert.fail();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	private PatientAssociationDetailsV toPatientAssociationDetailsV(PatientCaregiverAssociationsV detailsV) {
		PatientAssociationDetailsV associationDetailsV = new PatientAssociationDetailsV();
		associationDetailsV.setFirstName(detailsV.getFirstName());
		associationDetailsV.setLastName(detailsV.getLastName());
		associationDetailsV.setAge(detailsV.getAge());
		associationDetailsV.setCity(detailsV.getCity());
		associationDetailsV.setCountry(detailsV.getCountry());
		associationDetailsV.setEmailId(detailsV.getEmailId());
		associationDetailsV.setMobileNumber(detailsV.getMobileNumber());
		associationDetailsV.setCaregiverId(detailsV.getCaregiverId());
		associationDetailsV.setPatientId(detailsV.getPatientId());
		return associationDetailsV;
	}
}
