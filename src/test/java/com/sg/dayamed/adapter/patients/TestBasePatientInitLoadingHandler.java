package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.repository.view.PatientAssociationDetailsVRepository;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.util.CommonAsserts;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import ma.glasnost.orika.MapperFacade;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 01/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestBasePatientInitLoadingHandler {

	private BasePatientInitLoadingHandler basePatientInitLoadingHandler;

	private PatientLoadingAdapter mockAdapter;

	private UserValidator mockUserValidator;

	private PatientAssociationDetailsVRepository mockPatientAssociationDetailsVRepository;

	private MapperFacade mockMapperFacade;

	private PatientLoadingFromCaregiverHandler mockLoadingFromCaregiverHandler;

	@Before
	public void setup() throws Exception {
		basePatientInitLoadingHandler = Mockito.mock(BasePatientInitLoadingHandler.class);
		mockAdapter = Mockito.mock(PatientLoadingAdapter.class);
		mockUserValidator = Mockito.mock(UserValidator.class);
		mockPatientAssociationDetailsVRepository = Mockito.mock(PatientAssociationDetailsVRepository.class);
		mockMapperFacade = Mockito.mock(MapperFacade.class);
		mockLoadingFromCaregiverHandler = Mockito.mock(PatientLoadingFromCaregiverHandler.class);
		ReflectionTestUtils.setField(basePatientInitLoadingHandler, "adapter", mockAdapter);
		ReflectionTestUtils.setField(basePatientInitLoadingHandler, "userValidator", mockUserValidator);
		ReflectionTestUtils.setField(basePatientInitLoadingHandler, "associationDetailsVRepository",
		                             mockPatientAssociationDetailsVRepository);
		ReflectionTestUtils.setField(basePatientInitLoadingHandler, "mapperFacade", mockMapperFacade);
		Mockito.doCallRealMethod()
		       .when(basePatientInitLoadingHandler)
		       .init();
		Mockito.doCallRealMethod()
		       .when(basePatientInitLoadingHandler)
		       .getPatients(Mockito.any());
		Mockito.doCallRealMethod()
		       .when(basePatientInitLoadingHandler)
		       .getPatientAssociationDetails(Mockito.any(), Mockito.any());
		Mockito.doCallRealMethod()
		       .when(basePatientInitLoadingHandler)
		       .constructPagesForResult(Mockito.any(), Mockito.any(), Mockito.anyList());
	}

	@Test
	public void testInit() {
		basePatientInitLoadingHandler.init();
	}

	@Test
	public void testGetPatients() throws Exception {
		/**
		 * Case 1 : sort by first name ascending, offset=0, limit = 10
		 */
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.CAREGIVER);
		Mockito.doNothing()
		       .when(mockUserValidator)
		       .validateGetPatients(Mockito.any(), Mockito.any());
		List<PatientAssociationDetailsV> detailsVS = IntStream.range(0, 20)
		                                                      .mapToObj(
				                                                      index -> TestResponseGenerator.generatePatientAssociationDetailsV())
		                                                      .collect(Collectors.toList());
		for (PatientAssociationDetailsV detailsV : detailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(detailsV), Mockito.any()))
			       .thenReturn(TestResponseGenerator.toPatientDetailsVO(detailsV));
		}
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(detailsVS);
		PatientDetailsResponseVO responseVO = basePatientInitLoadingHandler.getPatients(requestVO);
		List<PatientAssociationDetailsV> sortedPatientDetails = detailsVS.stream()
		                                                                 .sorted(Comparator.comparing(
				                                                                 PatientAssociationDetailsV::getFirstName,
				                                                                 String::compareToIgnoreCase))
		                                                                 .limit(10)
		                                                                 .collect(Collectors.toList());
		assertNotNull(responseVO);
		assertEquals(detailsVS.size(), responseVO.getTotalRecords()
		                                         .intValue());
		assertTrue(CollectionUtils.isNotEmpty(responseVO.getPatientDetails()));
		assertEquals(sortedPatientDetails.size(), responseVO.getPatientDetails()
		                                                    .size());
		for (Integer index = 0; index < sortedPatientDetails.size(); index++) {
			PatientAssociationDetailsV associationDetailsV = sortedPatientDetails.get(index);
			PatientDetailsVO patientDetailsVO = responseVO.getPatientDetails()
			                                              .get(index);
			CommonAsserts.assertPatientDetailsVO(associationDetailsV, patientDetailsVO);
		}

		/**
		 * Case 2 : sort by first name ascending, offset=1, limit = 10
		 */
		Mockito.reset(mockMapperFacade);
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		requestVO.setOffset(1);
		detailsVS = IntStream.range(0, 20)
		                     .mapToObj(index -> TestResponseGenerator.generatePatientAssociationDetailsV())
		                     .collect(Collectors.toList());
		for (PatientAssociationDetailsV detailsV : detailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(detailsV), Mockito.any()))
			       .thenReturn(TestResponseGenerator.toPatientDetailsVO(detailsV));
		}
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(detailsVS);
		sortedPatientDetails = detailsVS.stream()
		                                .sorted(Comparator.comparing(PatientAssociationDetailsV::getFirstName,
		                                                             String::compareToIgnoreCase)
		                                                  .reversed())
		                                .collect(Collectors.toList());
		sortedPatientDetails = sortedPatientDetails.subList(10, sortedPatientDetails.size());
		responseVO = basePatientInitLoadingHandler.getPatients(requestVO);
		assertNotNull(responseVO);
		assertEquals(detailsVS.size(), responseVO.getTotalRecords()
		                                         .intValue());
		assertTrue(CollectionUtils.isNotEmpty(responseVO.getPatientDetails()));
		assertEquals(sortedPatientDetails.size(), responseVO.getPatientDetails()
		                                                    .size());
		for (Integer index = 0; index < sortedPatientDetails.size(); index++) {
			PatientAssociationDetailsV associationDetailsV = sortedPatientDetails.get(index);
			PatientDetailsVO patientDetailsVO = responseVO.getPatientDetails()
			                                              .get(index);
			CommonAsserts.assertPatientDetailsVO(associationDetailsV, patientDetailsVO);
		}

		/**
		 * Case 3 : search term is included.
		 */
		Mockito.reset(mockMapperFacade);
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		requestVO.setOffset(0);
		detailsVS = IntStream.range(0, 20)
		                     .mapToObj(index -> TestResponseGenerator.generatePatientAssociationDetailsV())
		                     .collect(Collectors.toList());
		for (PatientAssociationDetailsV detailsV : detailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(detailsV), Mockito.any()))
			       .thenReturn(TestResponseGenerator.toPatientDetailsVO(detailsV));
		}
		String query = detailsVS.get(10)
		                        .getFirstName();
		requestVO.setQuery(query);
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(detailsVS);
		responseVO = basePatientInitLoadingHandler.getPatients(requestVO);
		assertNotNull(responseVO);
		assertEquals(1, responseVO.getTotalRecords()
		                          .intValue());
		assertTrue(CollectionUtils.isNotEmpty(responseVO.getPatientDetails()));
		assertEquals(1, responseVO.getPatientDetails()
		                          .size());
		CommonAsserts.assertPatientDetailsVO(detailsVS.get(10), responseVO.getPatientDetails()
		                                                                  .get(0));
	}

	@Test
	public void testGetPatientsErrorCase() {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.CAREGIVER);
		/**
		 * Case 1 : userValidation exception
		 */
		try {

			DataValidationException exception =
					new DataValidationException(APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_INVALID_ROLE);
			Mockito.doThrow(exception)
			       .when(mockUserValidator)
			       .validateGetPatients(Mockito.any(), Mockito.any());
			basePatientInitLoadingHandler.getPatients(requestVO);
			Assert.fail();
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			DataValidationException exception = (DataValidationException) e.getCause();
			assertEquals(APIErrorFields.AUTHORIZATION, exception.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_ROLE, exception.getErrorKey());
		}

		/**
		 * Case 2 : Exception while retrieving data of patient associations.
		 */
		try {
			Mockito.doNothing()
			       .when(mockUserValidator)
			       .validateGetPatients(Mockito.any(), Mockito.any());
			List<PatientAssociationDetailsV> detailsVS = IntStream.range(0, 20)
			                                                      .mapToObj(
					                                                      index -> TestResponseGenerator.generatePatientAssociationDetailsV())
			                                                      .collect(Collectors.toList());
			Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
			       .thenThrow(new RuntimeException("Unexpected Error"));
			basePatientInitLoadingHandler.getPatients(requestVO);
			Assert.fail();
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}

		/**
		 * Case 3 :
		 */
		ReflectionTestUtils.setField(basePatientInitLoadingHandler, "mapperFacade", null);
		try {
			TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.CAREGIVER);
			Mockito.doNothing()
			       .when(mockUserValidator)
			       .validateGetPatients(Mockito.any(), Mockito.any());
			List<PatientAssociationDetailsV> detailsVS = IntStream.range(0, 20)
			                                                      .mapToObj(
					                                                      index -> TestResponseGenerator.generatePatientAssociationDetailsV())
			                                                      .collect(Collectors.toList());
			Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
			       .thenReturn(detailsVS);
			basePatientInitLoadingHandler.getPatients(requestVO);
			Assert.fail();
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
	}

}
