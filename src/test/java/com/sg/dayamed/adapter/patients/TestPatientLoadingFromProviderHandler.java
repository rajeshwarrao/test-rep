package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientProviderAssociationsV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.view.PatientProviderAssociationsVRepository;
import com.sg.dayamed.service.AdherenceService;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import ma.glasnost.orika.MapperFacade;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 02/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientLoadingFromProviderHandler {

	@InjectMocks
	private PatientLoadingFromProviderHandler loadingFromProviderHandler;

	@Mock
	private PatientLoadingFromProviderHandler mockSelf;

	@Mock
	private PrescriptionRepository mockPrescriptionRepository;

	@Mock
	private PatientProviderAssociationsVRepository mockProviderAssociationsVRepository;

	@Mock
	private DateUtility mockDateUtility;

	@Mock
	private AdherenceService mockAdherenceService;

	@Mock
	private MapperFacade mockMapperFacade;

	@Mock
	private UserValidator mockUserValidator;

	@Test
	public void testGetUserTypeHandler() {
		BasePatientInitLoadingHandler handler = loadingFromProviderHandler.getUserTypeHandler();
		assertNotNull(handler);
		assertTrue(handler instanceof PatientLoadingFromProviderHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] strings = loadingFromProviderHandler.getUserTypes();
		assertEquals(UserRoleEnum.PROVIDER.getRole(), strings[0]);
	}

	@Test
	public void testGetPatients() throws Exception {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PROVIDER);
		List<PatientProviderAssociationsV> providerAssociationsVList = IntStream.range(0, 15)
		                                                                        .mapToObj(
				                                                                        index -> TestResponseGenerator.generatePatientProviderAssociationsV())
		                                                                        .collect(Collectors.toList());
		List<PatientAssociationDetailsV> detailsVS = providerAssociationsVList.stream()
		                                                                      .map(detail -> TestResponseGenerator.toPatientAssociationDetailsV(
				                                                                      detail))
		                                                                      .collect(Collectors.toList());
		Mockito.when(mockProviderAssociationsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(providerAssociationsVList);
		for (PatientProviderAssociationsV associationsV : providerAssociationsVList) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(associationsV), Mockito.any()))
			       .thenReturn(TestResponseGenerator.toPatientAssociationDetailsV(associationsV));
		}
		List<Prescription> prescriptions = IntStream.range(0, 5)
		                                            .mapToObj(index -> TestResponseGenerator.generatePrescription())
		                                            .collect(Collectors.toList());
		Mockito.when(mockPrescriptionRepository.findByPatient_idIn(Mockito.anyList()))
		       .thenReturn(prescriptions);
		List<Long> patientDetails = providerAssociationsVList.stream()
		                                                     .map(PatientProviderAssociationsV::getPatientId)
		                                                     .limit(5)
		                                                     .collect(Collectors.toList());
		List<Long> prescriptionIds = prescriptions.stream()
		                                          .map(Prescription::getId)
		                                          .collect(Collectors.toList());
		for (Integer index = 0; index < 5; index++) {
			prescriptions.get(index)
			             .getPatient()
			             .setId(patientDetails.get(index));
		}
		List<AdherenceDataPoints> adherenceDataPoints = IntStream.range(0, 5)
		                                                         .mapToObj(
				                                                         index -> TestResponseGenerator.generateAdherenceDataPoints(
						                                                         Stream.of("consumed")
						                                                               .collect(Collectors.toList())))
		                                                         .collect(Collectors.toList());
		Mockito.when(mockAdherenceService.findAdherencesBetweenPrescribedTime(Mockito.any(), Mockito.any(),
		                                                                      Mockito.anyList()))
		       .thenReturn(adherenceDataPoints);
		List<PatientAssociationDetailsV> patientAssociationDetailsVS = new ArrayList<>();
		PatientAssociationDetailsV detailsV1 = detailsVS.get(0);
		detailsV1.setPatientId(prescriptions.get(0)
		                                    .getPatient()
		                                    .getId());
		PatientAssociationDetailsV detailsV2 = detailsVS.get(3);
		detailsV2.setPatientId(prescriptions.get(3)
		                                    .getPatient()
		                                    .getId());
		patientAssociationDetailsVS.add(detailsV1);
		patientAssociationDetailsVS.add(detailsV2);
		adherenceDataPoints.get(0)
		                   .setPrescriptionID(prescriptionIds.get(0));
		adherenceDataPoints.get(0)
		                   .setConsumptionStatus(Stream.of("NO_Action")
		                                               .collect(Collectors.toList()));
		adherenceDataPoints.get(3)
		                   .setPrescriptionID(prescriptionIds.get(3));
		adherenceDataPoints.get(3)
		                   .setConsumptionStatus(Stream.of("NO_Action")
		                                               .collect(Collectors.toList()));
		Mockito.when(mockAdherenceService.findAdherencesBetweenPrescribedTime(Mockito.any(), Mockito.any(),
		                                                                      Mockito.anyList()))
		       .thenReturn(adherenceDataPoints);
		PatientDetailsResponseVO responseVO = loadingFromProviderHandler.getPatients(requestVO);
		assertNotNull(responseVO);
		assertEquals(2, responseVO.getTotalRecords()
		                          .intValue());
		assertEquals(2, responseVO.getPatientDetails()
		                          .size());
	}

	@Test
	public void testGetPatientsErrorCase() throws Exception {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PROVIDER);
		DataValidationException exception =
				new DataValidationException(APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_INVALID_ROLE);
		Mockito.doThrow(exception)
		       .when(mockUserValidator)
		       .validateGetPatients(Mockito.any(), Mockito.any());
		try {
			loadingFromProviderHandler.getPatients(requestVO);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			DataValidationException cause = (DataValidationException) e.getCause();
			assertEquals(APIErrorFields.AUTHORIZATION, cause.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_ROLE, cause.getErrorKey());
		}

		Mockito.reset(mockUserValidator);
		Mockito.when(mockProviderAssociationsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenThrow(new RuntimeException());
		try {
			loadingFromProviderHandler.getPatients(requestVO);
			Assert.fail();
		} catch (Exception e) {
			assertTrue(true);
		}
	}
}
