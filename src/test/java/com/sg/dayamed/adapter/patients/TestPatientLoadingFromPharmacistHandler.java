package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientPharmacistAssociationsV;
import com.sg.dayamed.repository.view.PatientPharmacistAssociationsVRepository;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import ma.glasnost.orika.MapperFacade;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 02/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientLoadingFromPharmacistHandler {

	@InjectMocks
	private PatientLoadingFromPharmacistHandler loadingFromPharmacistHandler;

	@Mock
	private PatientLoadingFromPharmacistHandler mockSelf;

	@Mock
	private PatientPharmacistAssociationsVRepository mockPharmacistAssociationsVRepository;

	@Mock
	private MapperFacade mockMapperFacade;

	@Test
	public void testGetUserTypeHandler() {
		BasePatientInitLoadingHandler handler = loadingFromPharmacistHandler.getUserTypeHandler();
		assertNotNull(handler);
		assertTrue(handler instanceof PatientLoadingFromPharmacistHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] strings = loadingFromPharmacistHandler.getUserTypes();
		assertEquals(UserRoleEnum.PHARMACIST.getRole(), strings[0]);
	}

	@Test
	public void testGetPatientAssociationDetails() throws Exception {
		/**
		 * Case 1 : Success response
		 */
		List<PatientPharmacistAssociationsV> associationDetailsVS = IntStream.range(0, 15)
		                                                                     .mapToObj(
				                                                                     index -> TestResponseGenerator.generatePatientPharmacistAssociationsV())
		                                                                     .collect(Collectors.toList());
		Mockito.when(mockPharmacistAssociationsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(associationDetailsVS);
		for (PatientPharmacistAssociationsV detailsV : associationDetailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(detailsV), Mockito.any()))
			       .thenReturn(TestResponseGenerator.toPatientAssociationDetailsV(detailsV));
		}
		List<PatientAssociationDetailsV> detailsVS =
				loadingFromPharmacistHandler.getPatientAssociationDetails(UserRoleEnum.PHARMACIST,
				                                                          TestResponseGenerator.generateLoadPatientRequest(
						                                                          UserRoleEnum.PHARMACIST));
		assertNotNull(detailsVS);
		assertEquals(associationDetailsVS.size(), detailsVS.size());

		/**
		 * Case 2 : Exception occurred
		 */
		Mockito.reset(mockPharmacistAssociationsVRepository);
		Mockito.when(mockPharmacistAssociationsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenThrow(new RuntimeException());
		try {
			loadingFromPharmacistHandler.getPatientAssociationDetails(UserRoleEnum.PHARMACIST,
			                                                          TestResponseGenerator.generateLoadPatientRequest(
					                                                          UserRoleEnum.PHARMACIST));
			Assert.fail();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

}
