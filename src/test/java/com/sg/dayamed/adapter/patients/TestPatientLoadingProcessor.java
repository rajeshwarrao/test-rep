package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientLoadingProcessor {

	@InjectMocks
	private PatientLoadingProcessor patientLoadingProcessor;

	@Mock
	private PatientLoadingAdapter mockAdapter;

	@Mock
	private PatientLoadingFromProviderHandler mockLoadingFromProviderHandler;

	@Test
	public void testGetPatients() throws Exception {

		/**
		 * Case 1 : Error while retrieving patients.
		 */
		Mockito.when(mockAdapter.getHandler(Mockito.eq(UserRoleEnum.CAREGIVER.getRole())))
		       .thenThrow(new RuntimeException());
		try {
			patientLoadingProcessor.getPatients(UserRoleEnum.CAREGIVER,
			                                    TestResponseGenerator.generateLoadPatientRequest(
					                                    UserRoleEnum.CAREGIVER));
		} catch (Exception e) {
			assertTrue(true);
		}

		/**
		 * Case 2 : Success response
		 */
		Mockito.when(mockAdapter.getHandler(Mockito.eq(UserRoleEnum.PROVIDER.getRole())))
		       .thenReturn(mockLoadingFromProviderHandler);
		PatientDetailsResponseVO expectedResponse = new PatientDetailsResponseVO();
		expectedResponse.setTotalRecords(10);
		Mockito.when(mockLoadingFromProviderHandler.getPatients(Mockito.any()))
		       .thenReturn(expectedResponse);
		PatientDetailsResponseVO actualResponse = patientLoadingProcessor.getPatients(UserRoleEnum.PROVIDER,
		                                                                              TestResponseGenerator.generateLoadPatientRequest(
				                                                                              UserRoleEnum.PROVIDER));
		assertNotNull(actualResponse);
		assertEquals(expectedResponse.getTotalRecords(), actualResponse.getTotalRecords());
	}
}
