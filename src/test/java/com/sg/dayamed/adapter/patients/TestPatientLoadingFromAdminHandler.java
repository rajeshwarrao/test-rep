package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 02/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientLoadingFromAdminHandler {

	@Mock
	private PatientLoadingFromAdminHandler mockPatientLoadingFromAdminHandler;

	@InjectMocks
	private PatientLoadingFromAdminHandler patientLoadingFromAdminHandler;

	@Test
	public void testGetUserTypeHandler() {
		BasePatientInitLoadingHandler userTypeHandler = patientLoadingFromAdminHandler.getUserTypeHandler();
		assertNotNull(userTypeHandler);
		assertTrue(userTypeHandler instanceof PatientLoadingFromAdminHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] results = patientLoadingFromAdminHandler.getUserTypes();
		assertTrue(ArrayUtils.isNotEmpty(results));
		assertEquals(UserRoleEnum.ADMIN.getRole(), results[0]);
	}
}
