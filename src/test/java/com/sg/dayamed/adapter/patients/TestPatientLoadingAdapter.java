package com.sg.dayamed.adapter.patients;

import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 25/Jun/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientLoadingAdapter {

	@InjectMocks
	private PatientLoadingAdapter patientLoadingAdapter;

	@Test
	public void testRegisterHandler() {
		/**
		 * Case 1 : Handler is null
		 */
		try {
			patientLoadingAdapter.registerHandler(null, UserRoleEnum.PROVIDER.getRole());
			Assert.fail();
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}

		/**
		 * Case 2 : User types is empty
		 */
		try {
			patientLoadingAdapter.registerHandler(new PatientLoadingFromProviderHandler(), new String[]{});
			Assert.fail();
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}

		/**
		 * Case 3 : Registering handler successful
		 */
		patientLoadingAdapter.registerHandler(new PatientLoadingFromCaregiverHandler(),
		                                      UserRoleEnum.CAREGIVER.getRole());
		BasePatientInitLoadingHandler handler = patientLoadingAdapter.getHandler(UserRoleEnum.CAREGIVER.getRole());
		assertNotNull(handler);
		assertTrue(handler instanceof PatientLoadingFromCaregiverHandler);

		/**
		 * Case 4 : Invalid Handler requested.
		 */
		handler = patientLoadingAdapter.getHandler(UserRoleEnum.PROVIDER.getRole());
		assertNull(handler);
	}

}
