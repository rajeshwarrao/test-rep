package com.sg.dayamed.adapter.users;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserOptions;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.security.jwtsecurity.security.JwtGenerator;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class TestUserInfoLoadingHandler {

	private UserInfoLoadingHandler mockUserInfoLoadingHandler;

	private JwtGenerator jwtGenerator;

	private UserPreferenceRepository userPreferenceRepository;

	private UserDetailsRepository userDetailsRepository;


	private UserLoadingAdapter adapter;

	private Utility utility;

	private ObjectMapper userPreferenceModelMapper;

	@Before
	public void setup() throws Exception {
		mockUserInfoLoadingHandler = Mockito.mock(UserInfoLoadingHandler.class);
		adapter = Mockito.mock(UserLoadingAdapter.class);
		jwtGenerator = Mockito.mock(JwtGenerator.class);
		utility = Mockito.mock(Utility.class);
		userPreferenceRepository = Mockito.mock(UserPreferenceRepository.class);
		userDetailsRepository = Mockito.mock(UserDetailsRepository.class);
		userPreferenceModelMapper = Mockito.mock(ObjectMapper.class);
		ReflectionTestUtils.setField(mockUserInfoLoadingHandler, "adapter", adapter);
		ReflectionTestUtils.setField(mockUserInfoLoadingHandler, "utility", utility);
		ReflectionTestUtils.setField(mockUserInfoLoadingHandler, "userPreferenceModelMapper",
		                             userPreferenceModelMapper);
		ReflectionTestUtils.setField(mockUserInfoLoadingHandler, "userDetailsRepository", userDetailsRepository);
		ReflectionTestUtils.setField(mockUserInfoLoadingHandler, "userPreferenceRepository", userPreferenceRepository);
		Mockito.doCallRealMethod()
		       .when(mockUserInfoLoadingHandler)
		       .init();
		Mockito.doCallRealMethod()
		       .when(mockUserInfoLoadingHandler)
		       .getUserDetailsWithModel(Mockito.any(),
		                                Mockito.any(), Mockito.any());
	}

	@Test
	public void testInit() {
		mockUserInfoLoadingHandler.init();
	}

	@Test
	public void testGetUserDetailsWithModelFailer() throws Exception {
		/*
		 * Case 1 : if userdetails is null throw exception
		 */
		try {
			mockUserInfoLoadingHandler.getUserDetailsWithModel(TestResponseGenerator.getLoginRequestModel(),
			                                                   jwtGenerator, null);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
	}

	@Test
	public void testGetUserDetailsWithModelSuccess() throws Exception {
		/*
		 * Case 1 : response success
		 */
		UserOptions userOptions = new UserOptions();
		userOptions.setGsPharmacistEnable(1);
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
		                                                                    ApplicationConstants.PROVIDER);
		userDetails.setUserOptions(userOptions);
		Mockito.when(userPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
		       .thenReturn(TestResponseGenerator.generateUserPreference());
		Mockito.when(utility.convertUserDetailsEntityToModel(any()))
		       .thenReturn(
				       TestResponseGenerator.generateUserDetailsModel("Male", "Asian", ApplicationConstants.PROVIDER));
		Mockito.when(userDetailsRepository.save(any()))
		       .thenReturn(userDetails);
		UserDetailsWithJwtModel userDetailsWithJwtModel = mockUserInfoLoadingHandler
				.getUserDetailsWithModel(TestResponseGenerator.getLoginRequestModel(), jwtGenerator, userDetails);
		assertNotNull(userDetailsWithJwtModel);
	}
}
