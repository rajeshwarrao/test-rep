package com.sg.dayamed.adapter.users;

import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.security.jwtsecurity.security.JwtGenerator;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestUserInfoLoadingProcessor {

	@InjectMocks
	private UserInfoLoadingProcessor mockUserInfoLoadingProcessor;

	@Mock
	private UserLoadingAdapter mockUserLoadingAdapter;

	@Mock
	private JwtGenerator jwtGenerator;

	@Mock
	private ProviderLoadingHandler mockProviderLoadingHandler;

	@Test
	public void testGetUserDetailsWithModel() throws Exception {
		/*
		 * Case 1: throws ApplicationException
		 */
		Mockito.when(mockUserLoadingAdapter.getHandler(Mockito.eq(UserRoleEnum.PHARMACIST.getRole())))
		       .thenThrow(new RuntimeException());
		try {
			mockUserInfoLoadingProcessor.getUserDetailsWithModel(UserRoleEnum.PHARMACIST,
			                                                     TestResponseGenerator.getLoginRequestModel(),
			                                                     jwtGenerator,
			                                                     TestResponseGenerator.generateUserDetails("Male",
			                                                                                               "Asian",
			                                                                                               ApplicationConstants.PHARMACIST));
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetUserDetailsWithSuccess() throws Exception {
		/*
		 * Case 2: Success response
		 */
		Mockito.when(mockUserLoadingAdapter.getHandler(Mockito.eq(UserRoleEnum.PROVIDER.getRole())))
		       .thenReturn(mockProviderLoadingHandler);
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		Mockito.when(mockProviderLoadingHandler.getUserDetailsWithModel(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(userDetailsWithJwtModel);
		Mockito.when(mockProviderLoadingHandler.getUserDetailsWithModelResponse(Mockito.any(), Mockito.any()))
		       .thenReturn(userDetailsWithJwtModel);
		userDetailsWithJwtModel = mockUserInfoLoadingProcessor.getUserDetailsWithModel(UserRoleEnum.PROVIDER,
		                                                                               TestResponseGenerator.getLoginRequestModel(),
		                                                                               jwtGenerator,
		                                                                               TestResponseGenerator.generateUserDetails(
				                                                                               "Male", "Asian",
				                                                                               ApplicationConstants.PROVIDER));
		assertNotNull(userDetailsWithJwtModel);

	}

}
