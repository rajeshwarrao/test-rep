package com.sg.dayamed.adapter.users;

import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestPharmacistLoadingHandler {

	@InjectMocks
	private PharmacistLoadingHandler pharmacistLoadingHandler;

	@Mock
	private PharmacistLoadingHandler mockPharmacistLoadingHandler;

	@Mock
	private PharmacistRepository pharmacistRepository;

	@Mock
	private Utility utility;

	@Test
	public void testGetUserTypeHandler() {
		UserInfoLoadingHandler userTypeHandler = pharmacistLoadingHandler.getUserTypeHandler();
		assertNotNull(userTypeHandler);
		assertTrue(userTypeHandler instanceof PharmacistLoadingHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] results = pharmacistLoadingHandler.getUserTypes();
		assertTrue(ArrayUtils.isNotEmpty(results));
		assertEquals(UserRoleEnum.PHARMACIST.getRole(), results[0]);
	}

	@Test
	public void testGetUserDetailsWithModelResponse() throws Exception {
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		userDetailsWithJwtModel.setUserDetails(
				TestResponseGenerator.generateUserDetailsModel("Male", "Asian", ApplicationConstants.PHARMACIST));
		Mockito.when(pharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(TestResponseGenerator.generatePharmacist("Male", "Asian", ApplicationConstants.PHARMACIST));
		Mockito.when(utility.convertPharmacistEntityToModel(Mockito.any()))
		       .thenReturn(TestResponseGenerator.generatePharmacistModel());
		userDetailsWithJwtModel = pharmacistLoadingHandler.getUserDetailsWithModelResponse(userDetailsWithJwtModel,
		                                                                                   TestResponseGenerator.getLoginRequestModel());
		assertNotNull(userDetailsWithJwtModel);
	}

}
