package com.sg.dayamed.adapter.users;

import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestUserLoadingAdapter {

	@InjectMocks
	private UserLoadingAdapter userLoadingAdapter;

	@Test
	public void testRegisterHandler() {
		/**
		 * Case 1 : Handler is null
		 */
		try {
			userLoadingAdapter.registerHandler(null, UserRoleEnum.PROVIDER.getRole());
			Assert.fail();
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}

		/**
		 * Case 2 : User types is empty
		 */
		try {
			userLoadingAdapter.registerHandler(new ProviderLoadingHandler(), new String[]{});
			Assert.fail();
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}

		/**
		 * Case 3 : Registering handler successful
		 */
		userLoadingAdapter.registerHandler(new ProviderLoadingHandler(), UserRoleEnum.PROVIDER.getRole());
		UserInfoLoadingHandler handler = userLoadingAdapter.getHandler(UserRoleEnum.PROVIDER.getRole());
		assertNotNull(handler);
		assertTrue(handler instanceof ProviderLoadingHandler);

		/**
		 * Case 4 : Invalid Handler requested.
		 */
		handler = userLoadingAdapter.getHandler(UserRoleEnum.PHARMACIST.getRole());
		assertNull(handler);
	}

}
