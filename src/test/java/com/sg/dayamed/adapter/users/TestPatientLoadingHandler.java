package com.sg.dayamed.adapter.users;

import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserOptions;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestPatientLoadingHandler {

	@InjectMocks
	private PatientLoadingHandler patientLoadingHandler;

	@Mock
	private PatientLoadingHandler mockPatientLoadingHandler;

	@Mock
	private PatientRepository patientRepository;

	@Mock
	private PHIDataEncryptionService pHIDataEncryptionService;

	@Mock
	private Utility utility;

	@Test
	public void testGetUserTypeHandler() {
		UserInfoLoadingHandler userTypeHandler = patientLoadingHandler.getUserTypeHandler();
		assertNotNull(userTypeHandler);
		assertTrue(userTypeHandler instanceof PatientLoadingHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] results = patientLoadingHandler.getUserTypes();
		assertTrue(ArrayUtils.isNotEmpty(results));
		assertEquals(UserRoleEnum.PATIENT.getRole(), results[0]);
	}

	@Test
	public void testGetPatient() throws Exception {
		Mockito.when(patientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT));
		Patient patient = patientLoadingHandler.getPatient(1l);
		assertNotNull(patient);
	}

	@Test
	public void testGetUserDetailsWithModelResponse() throws Exception {
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		userDetailsWithJwtModel.setUserDetails(
				TestResponseGenerator.generateUserDetailsModel("Male", "Asian", ApplicationConstants.PATIENT));
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", ApplicationConstants.PROVIDER);
		UserDetails providerUserDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PROVIDER);
		UserOptions userOptions = new UserOptions();
		userOptions.setGsPharmacistEnable(1);
		providerUserDetails.setUserOptions(userOptions);
		provider.setUserDetails(providerUserDetails);
		Set<Provider> providerSet = new HashSet<>();
		providerSet.add(provider);
		patient.setProviders(providerSet);
		Mockito.when(patientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		Mockito.when(utility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(TestResponseGenerator.generatePatientModel());
		Mockito.when(pHIDataEncryptionService.getKeyText(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(RandomStringUtils.randomAlphanumeric(9));
		Mockito.when(pHIDataEncryptionService.encryptPatient(Mockito.any(), Mockito.any()))
		       .thenReturn(TestResponseGenerator.generatePatientModel());
		LoginRequestModel loginRequestModel = TestResponseGenerator.getLoginRequestModel();
		loginRequestModel.setTypeOfDevice(ApplicationConstants.ANDROID);
		userDetailsWithJwtModel =
				patientLoadingHandler.getUserDetailsWithModelResponse(userDetailsWithJwtModel, loginRequestModel);
		assertNotNull(userDetailsWithJwtModel);
	}

}
