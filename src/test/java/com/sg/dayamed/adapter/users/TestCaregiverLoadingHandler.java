package com.sg.dayamed.adapter.users;

import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.repository.CaregiverRepository;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestCaregiverLoadingHandler {

	@InjectMocks
	private CaregiverLoadingHandler caregiverLoadingHandler;

	@Mock
	private CaregiverLoadingHandler mockCaregiverLoadingHandler;

	@Mock
	private CaregiverRepository caregiverRepository;

	@Mock
	Utility utility;

	@Test
	public void testGetUserTypeHandler() {
		UserInfoLoadingHandler userTypeHandler = caregiverLoadingHandler.getUserTypeHandler();
		assertNotNull(userTypeHandler);
		assertTrue(userTypeHandler instanceof CaregiverLoadingHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] results = caregiverLoadingHandler.getUserTypes();
		assertTrue(ArrayUtils.isNotEmpty(results));
		assertEquals(UserRoleEnum.CAREGIVER.getRole(), results[0]);
	}

	@Test
	public void testGetUserDetailsWithModelResponse() throws Exception {
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		userDetailsWithJwtModel.setUserDetails(
				TestResponseGenerator.generateUserDetailsModel("Male", "Asian", ApplicationConstants.CAREGIVER));
		Mockito.when(caregiverRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(
				       TestResponseGenerator.generateCaregiver("Male", "Asian", ApplicationConstants.CAREGIVER,
				                                               false));
		Mockito.when(utility.convertCaregiverEntityToModel(Mockito.any()))
		       .thenReturn(TestResponseGenerator.generateCaregiverModel("Male", "Asian"));
		userDetailsWithJwtModel = caregiverLoadingHandler.getUserDetailsWithModelResponse(userDetailsWithJwtModel,
		                                                                                  TestResponseGenerator.getLoginRequestModel());
		assertNotNull(userDetailsWithJwtModel);
	}
}
