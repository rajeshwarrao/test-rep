package com.sg.dayamed.adapter.users;

import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestAdminLoadingHandler {

	@InjectMocks
	private AdminLoadingHandler adminLoadingHandler;

	@Mock
	private AdminLoadingHandler mockAdminLoadingHandler;

	@Test
	public void testGetUserTypeHandler() {
		UserInfoLoadingHandler userTypeHandler = adminLoadingHandler.getUserTypeHandler();
		assertNotNull(userTypeHandler);
		assertTrue(userTypeHandler instanceof AdminLoadingHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] results = adminLoadingHandler.getUserTypes();
		assertTrue(ArrayUtils.isNotEmpty(results));
		assertEquals(UserRoleEnum.ADMIN.getRole(), results[0]);
	}

	@Test
	public void testGetUserDetailsWithModelResponse() throws Exception {
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		userDetailsWithJwtModel = adminLoadingHandler.getUserDetailsWithModelResponse(userDetailsWithJwtModel,
		                                                                              TestResponseGenerator.getLoginRequestModel());
		assertNotNull(userDetailsWithJwtModel);
	}
}
