package com.sg.dayamed.adapter.users;

import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestProviderLoadingHandler {

	@InjectMocks
	private ProviderLoadingHandler providerLoadingHandler;

	@Mock
	private ProviderLoadingHandler mockProviderLoadingHandler;

	@Test
	public void testGetUserTypeHandler() {
		UserInfoLoadingHandler userTypeHandler = providerLoadingHandler.getUserTypeHandler();
		assertNotNull(userTypeHandler);
		assertTrue(userTypeHandler instanceof ProviderLoadingHandler);
	}

	@Test
	public void testGetUserTypes() {
		String[] results = providerLoadingHandler.getUserTypes();
		assertTrue(ArrayUtils.isNotEmpty(results));
		assertEquals(UserRoleEnum.PROVIDER.getRole(), results[0]);
	}

	@Test
	public void testGetUserDetailsWithModelResponse() throws Exception {
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		userDetailsWithJwtModel = providerLoadingHandler.getUserDetailsWithModelResponse(userDetailsWithJwtModel,
		                                                                                 TestResponseGenerator.getLoginRequestModel());
		assertNotNull(userDetailsWithJwtModel);
	}
}
