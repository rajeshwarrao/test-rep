package com.sg.dayamed.exceptions;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/

public class TestApplicationException {

	@Test
	public void testApplicationException() {
		assertNotNull(new ApplicationException("error"));
		assertNotNull(new ApplicationException("error", new RuntimeException()));
		assertNotNull(new ApplicationException(new Throwable()));
		assertNotNull(new ApplicationException("error", new Throwable()));
		assertNotNull(new ApplicationException("error", new Throwable(), "key"));
		assertNotNull(new ApplicationException("error", new Throwable(), "key", new Object[]{1}));
		new ApplicationException().equals(new ApplicationException());
		assertNotNull(new ApplicationException("error", new Throwable()).hashCode());
		assertNotNull(new ApplicationException().toString());
		assertNotNull(new ApplicationException(new RuntimeException()));

	}
}
