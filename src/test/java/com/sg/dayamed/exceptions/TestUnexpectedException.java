package com.sg.dayamed.exceptions;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/

public class TestUnexpectedException {

	@Test
	public void testUnexpectedException() {
		assertNotNull(new UnexpectedException());
		assertNotNull(new UnexpectedException("error", new RuntimeException(), true, true));
		assertNotNull(new UnexpectedException("error", new RuntimeException()));
		assertNotNull(new UnexpectedException(new RuntimeException()));
		assertNotNull(new UnexpectedException("error"));
	}
}
