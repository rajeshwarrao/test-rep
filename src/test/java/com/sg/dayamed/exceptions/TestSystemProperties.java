package com.sg.dayamed.exceptions;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/

public class TestSystemProperties {

	@Test
	public void testSystemProperties() {
		assertNotNull(SystemProperties.getUserName());
		assertNotNull(SystemProperties.getUserDir());
		assertNotNull(SystemProperties.getUserHome());
		assertNotNull(SystemProperties.FILE_SEPARATOR);
		assertNotNull(SystemProperties.JAVA_CLASS_PATH);
		assertNotNull(SystemProperties.JAVA_CLASS_PATH);
		assertNotNull(SystemProperties.JAVA_EXT_DIRS);
		assertNotNull(SystemProperties.JAVA_HOME);
		assertNotNull(SystemProperties.JAVA_SPECIFICATION_NAME);
		assertNotNull(SystemProperties.JAVA_SPECIFICATION_VENDOR);
		assertNotNull(SystemProperties.JAVA_SPECIFICATION_VERSION);
		assertNotNull(SystemProperties.JAVA_VENDOR);
		assertNotNull(SystemProperties.JAVA_VENDOR_URL);
		assertNotNull(SystemProperties.JAVA_VERSION);
		assertNotNull(SystemProperties.JAVA_EXT_DIRS);
		assertNotNull(SystemProperties.JAVA_CLASS_PATH);
		assertNotNull(SystemProperties.JAVA_CLASS_VERSION);
		assertNotNull(SystemProperties.JAVA_VM_NAME);
		assertNotNull(SystemProperties.JAVA_VM_SPECIFICATION_VENDOR);
		assertNotNull(SystemProperties.JAVA_VM_SPECIFICATION_NAME);
		assertNotNull(SystemProperties.JAVA_VM_SPECIFICATION_VERSION);
		assertNotNull(SystemProperties.LINE_SEPARATOR);
		assertNotNull(SystemProperties.OS_ARCH);
		assertNotNull(SystemProperties.OS_NAME);
		assertNotNull(SystemProperties.OS_VERSION);
		assertNotNull(SystemProperties.PATH_SEPARATOR);
	}
}
