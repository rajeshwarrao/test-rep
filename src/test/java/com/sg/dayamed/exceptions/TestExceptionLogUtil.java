package com.sg.dayamed.exceptions;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import java.rmi.RemoteException;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
public class TestExceptionLogUtil {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Test
	public void testExceptionLogUtil() {
		ExceptionLogUtil.getRootCause(new RuntimeException());
		ExceptionLogUtil.log(new RuntimeException(), logger);
		ExceptionLogUtil.log(new RemoteException(), "error", logger);
		ExceptionLogUtil.log(new RuntimeException(), "error", logger);
		ExceptionLogUtil.logDBError(logger, Level.DEBUG, "error", new RuntimeException());
	}
}
