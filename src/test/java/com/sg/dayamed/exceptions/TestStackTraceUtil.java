package com.sg.dayamed.exceptions;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/

public class TestStackTraceUtil {

	@Test
	public void testStackTrace() {
		assertNull(StackTraceUtil.getStackTrace(null));
		String stacktrace = StackTraceUtil.getStackTrace(new RuntimeException());
		assertNotNull(stacktrace);
		assertNull(StackTraceUtil.getAnalysisStackTrace(null));
		stacktrace = StackTraceUtil.getAnalysisStackTrace(new RuntimeException());
		assertNotNull(stacktrace);
		StackTraceUtil.getAbbreviatedCubicStackTrace(new RuntimeException());
	}
}
