package com.sg.dayamed.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.apache.commons.lang.StringUtils;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PowerMockIgnore;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Eresh Gorantla on 03/May/2019
 **/
@PowerMockIgnore("javax.security.*")
public class BaseTestCase {

	protected final String ADHERENCE_DATA_POINTS_FILE = "entity-input/adherence-data-points.json";

	protected final String USER_DETAILS_FILE = "entity-input/user-details.json";

	protected final String BIOMETRIC_SERVICE_FILE = "entity-input/biometric-service.json";

	protected final String PRESCRIPTION_DETAILS_FILE = "entity-input/prescription-details.json";

	protected final String WS_USER_DETAILS_FILE = "request-json/ws-user-details.json";

	protected ObjectMapper objectMapper;

	public BaseTestCase() {
		objectMapper = new ObjectMapper();
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		objectMapper.configure(DeserializationFeature.EAGER_DESERIALIZER_FETCH, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
		objectMapper.registerModule(new JavaTimeModule());
	}

	protected String getRequestJson(String nodeName, String fileName) throws Exception {
		if (StringUtils.isBlank(fileName)) {
			return null;
		}
		final JsonNode requestJsonNode = getJsonNode(nodeName, fileName);
		return requestJsonNode.toString();
	}

	private JsonNode getJsonNode(String nodeName, String fileName) throws IOException {
		try (InputStream is = getClass().getClassLoader()
		                                .getResourceAsStream(fileName)) {
			final JsonNode node = objectMapper.readTree(is);
			return node.path(nodeName);
		}
	}

	protected <T> T generateRequestObject(String nodeName, Class<T> clazz, String fileName) throws Exception {
		if (StringUtils.isBlank(fileName)) {
			return null;
		}
		JsonNode requestNode = getJsonNode(nodeName, fileName);
		return objectMapper.readValue(requestNode.toString(), clazz);
	}
}
