package com.sg.dayamed.base;

import com.sg.dayamed.component.CacheDataComponent;
import com.sg.dayamed.dao.UserDetailsCacheVO;
import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
public class BaseRestApiTest extends BaseTestCase {

	@Mock
	protected ObjectMapper mockObjectMapper;

	@Mock
	protected PHIDataEncryption mockPhiDataEncryption;

	@Mock
	protected CacheDataComponent mockCacheDataComponent;

	@Before
	public void init() {
		Mockito.lenient()
		       .when(mockCacheDataComponent.validateUserDetails(Mockito.anyLong()))
		       .thenReturn(new UserDetailsCacheVO());
	}

}
