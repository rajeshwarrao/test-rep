package com.sg.dayamed.util;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 04/May/2019
 **/

public class TestUtil {

	private static final Supplier<String> mobileSupplier =
			() -> String.format("%01d%01d%01d%01d%01d%01d%01d0%01d%01d%01d", ThreadLocalRandom.current()
			                                                                                  .nextInt(6, 9),
			                    ThreadLocalRandom.current()
			                                     .nextInt(9), ThreadLocalRandom.current()
			                                                                   .nextInt(9), ThreadLocalRandom.current()
			                                                                                                 .nextInt(
					                                                                                                 9),
			                    ThreadLocalRandom.current()
			                                     .nextInt(9),
			                    ThreadLocalRandom.current()
			                                     .nextInt(9), ThreadLocalRandom.current()
			                                                                   .nextInt(9), ThreadLocalRandom.current()
			                                                                                                 .nextInt(
					                                                                                                 9),
			                    ThreadLocalRandom.current()
			                                     .nextInt(9), ThreadLocalRandom.current()
			                                                                   .nextInt(9));

	private static final Supplier<String> imanticUserSupplier = () -> String.format("%01d", ThreadLocalRandom.current()
	                                                                                                         .nextInt(
			                                                                                                         1000));

	private static final Supplier<String> ageSupplier = () -> String.format("%01d", ThreadLocalRandom.current()
	                                                                                                 .nextInt(1, 90));

	private static final Supplier<String> idSupplier = () -> String.format("%01d", ThreadLocalRandom.current()
	                                                                                                .nextInt(1,
	                                                                                                         100000));

	private TestUtil() {

	}

	public static String generateMobileNumber() {
		return Stream.generate(TestUtil.mobileSupplier)
		             .limit(1)
		             .findFirst()
		             .orElse(null);
	}

	public static String generateImanticUserid() {
		return Stream.generate(TestUtil.imanticUserSupplier)
		             .limit(1)
		             .findFirst()
		             .orElse(null);
	}

	public static Integer generateAge() {
		return Stream.generate(TestUtil.ageSupplier)
		             .limit(1)
		             .map(Integer::valueOf)
		             .findFirst()
		             .orElse(null);
	}

	public static Long generateId() {
		return Stream.generate(TestUtil.idSupplier)
		             .limit(1)
		             .map(Long::new)
		             .findFirst()
		             .orElse(null);
	}

	public static Double generateLatitude() {
		return (ThreadLocalRandom.current()
		                         .nextDouble() * -180.0) + 90.0;
	}

	public static Double generateLongitude() {
		return (ThreadLocalRandom.current()
		                         .nextDouble() * -360.0) + 180.0;
	}

}
