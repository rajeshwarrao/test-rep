package com.sg.dayamed.util;

import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.repository.UserPreferenceRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 19/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestUtility {

	@InjectMocks
	private Utility utility;

	@Mock
	private UserPreferenceRepository mockUserPreferenceRepository;

	@Test
	public void testGetUserPreferredLang() {
		/**
		 * Case 1 : No exception
		 */
		UserPreference userPreference = TestResponseGenerator.generateUserPreference();
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.eq(1L)))
		       .thenReturn(userPreference);
		String actualResult = utility.getUserPreferedLang(1L);
		assertNotNull(actualResult);
		assertEquals("en", actualResult);

		/**
		 * Case 2 : Exception
		 */
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.eq(0L)))
		       .thenThrow(new RuntimeException());
		actualResult = utility.getUserPreferedLang(0L);
		assertNotNull(actualResult);
		assertEquals("en", actualResult);
	}
}
