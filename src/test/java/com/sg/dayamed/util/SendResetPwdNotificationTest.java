package com.sg.dayamed.util;

import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class SendResetPwdNotificationTest {

	@InjectMocks
	private SendResetPwdNotification sendResetPwdNotification;

	@Mock
	private PasswordEncryption mockPasswordEncryption;

	@Mock
	private EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	private EmailService mockEmailService;

	@Before
	public void init() {
		ReflectionTestUtils.setField(sendResetPwdNotification, "emailSubject", "test");
		ReflectionTestUtils.setField(sendResetPwdNotification, "emailBody", "test");
		ReflectionTestUtils.setField(sendResetPwdNotification, "smsBody", "test");
		ReflectionTestUtils.setField(sendResetPwdNotification, "pwdExpireDays", 100);
		ReflectionTestUtils.setField(sendResetPwdNotification, "downloadAndroidApp", "test");
		ReflectionTestUtils.setField(sendResetPwdNotification, "downloadIosApp", "test");
	}

	@Test
	public void testSendPwdResetNotification() {
		/**
		 * With out errors
		 */
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PATIENT.getRole());
		sendResetPwdNotification.sendPwdResetNotificationWhenRegst(userDetails, UserRoleEnum.PATIENT.getRole());
		Mockito.verify(mockEmailService, Mockito.times(1))
		       .sendEmail(Mockito.any());
		Mockito.verify(mockEmailAndSmsUtility, Mockito.times(1))
		       .sendSms(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());

		/**
		 * With errors
		 */
		Mockito.reset(mockEmailAndSmsUtility, mockEmailService);
		Mockito.doThrow(new RuntimeException())
		       .when(mockEmailService)
		       .sendEmail(Mockito.any());
		Mockito.when(mockEmailAndSmsUtility.sendSms(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
		       .thenThrow(new RuntimeException());
		sendResetPwdNotification.sendPwdResetNotificationWhenRegst(userDetails, UserRoleEnum.PATIENT.getRole());
		Mockito.verify(mockEmailService, Mockito.times(1))
		       .sendEmail(Mockito.any());
		Mockito.verify(mockEmailAndSmsUtility, Mockito.times(1))
		       .sendSms(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
	}

	@Test
	public void testSendEmailNotificationByRecipients() {
		/**
		 * Case 1: Without Exceptions
		 */
		List<Map<String, String>> recipientMap = new ArrayList<>();
		Map<String, String> stringMap = new HashMap<>();
		stringMap.put("emailId", "test@gmail.com");
		recipientMap.add(stringMap);
		Boolean result = sendResetPwdNotification.sendEmailNotificationByRecepients(recipientMap,
		                                                                            RandomStringUtils.randomAlphabetic(
				                                                                            10),
		                                                                            RandomStringUtils.randomAlphabetic(
				                                                                            10));
		assertNotNull(result);
		assertTrue(result);

		/**
		 * Case 2 : With Exceptions
		 */
		Mockito.doThrow(new RuntimeException())
		       .when(mockEmailService)
		       .sendEmail(Mockito.any());
		result = sendResetPwdNotification.sendEmailNotificationByRecepients(recipientMap,
		                                                                    RandomStringUtils.randomAlphabetic(10),
		                                                                    RandomStringUtils.randomAlphabetic(10));
		assertNotNull(result);
		assertFalse(result);
	}

	@Test
	public void testSendSMSNotification() {
		/**
		 * Case 1 : Without Exceptions
		 */
		List<Map<String, String>> recipientMap = new ArrayList<>();
		Map<String, String> stringMap = new HashMap<>();
		stringMap.put("mobileNumber", RandomStringUtils.randomNumeric(10));
		stringMap.put("countryCode", "91");
		recipientMap.add(stringMap);
		Boolean result =
				sendResetPwdNotification.sendSMSNotification(recipientMap, RandomStringUtils.randomAlphabetic(20));
		assertNotNull(result);
		assertTrue(result);

		/**
		 * Case 2 : With Exceptions
		 */
		Mockito.when(mockEmailAndSmsUtility.sendSms(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
		       .thenThrow(new RuntimeException());
		result = sendResetPwdNotification.sendSMSNotification(recipientMap, RandomStringUtils.randomAlphabetic(20));
		assertNotNull(result);
		assertFalse(result);
	}
}
