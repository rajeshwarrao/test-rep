package com.sg.dayamed.util;

import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({BeanUtil.class})
public class TestStringCryptoConverter {

	@InjectMocks
	private StringCryptoConverter stringCryptoConverter;

	private BeanUtil mockBeanUtil;

	private PHIDataEncryption mockPhiDataEncryption;

	@Before
	public void setup() {
		mockBeanUtil = PowerMockito.mock(BeanUtil.class);
		PowerMockito.mockStatic(BeanUtil.class);
	}

	@Test
	public void testGetPhiDataEncryption() {
		mockPhiDataEncryption = Mockito.mock(PHIDataEncryption.class);
		PowerMockito.when(BeanUtil.getBean(Mockito.any()))
		            .thenReturn(mockPhiDataEncryption);
		PHIDataEncryption dataEncryption = stringCryptoConverter.getPhiDataEncryption();
		assertNotNull(dataEncryption);

		ReflectionTestUtils.setField(stringCryptoConverter, "phiDataEncryption", mockPhiDataEncryption);
		dataEncryption = stringCryptoConverter.getPhiDataEncryption();
		assertNotNull(dataEncryption);
	}

	@Test
	public void testConvertToDatabaseColumn() throws Exception {
		mockPhiDataEncryption = Mockito.mock(PHIDataEncryption.class);
		PowerMockito.when(BeanUtil.getBean(Mockito.any()))
		            .thenReturn(mockPhiDataEncryption);
		Mockito.when(mockPhiDataEncryption.encryptText(Mockito.anyString(), Mockito.any()))
		       .thenReturn("value");
		String response = stringCryptoConverter.convertToDatabaseColumn(RandomStringUtils.randomAlphabetic(20));
		assertNotNull(response);
		assertEquals("value", response);

		String encryptedText = RandomStringUtils.randomAlphabetic(20);
		Mockito.when(mockPhiDataEncryption.encryptText(Mockito.eq(encryptedText), Mockito.any()))
		       .thenThrow(new RuntimeException());
		response = stringCryptoConverter.convertToDatabaseColumn(encryptedText);
		assertNotNull(response);
		assertEquals(encryptedText, response);
	}

	@Test
	public void testConvertToEntityAttribute() throws Exception {
		mockPhiDataEncryption = Mockito.mock(PHIDataEncryption.class);
		PowerMockito.when(BeanUtil.getBean(Mockito.any()))
		            .thenReturn(mockPhiDataEncryption);
		Mockito.when(mockPhiDataEncryption.decryptText(Mockito.anyString(), Mockito.any()))
		       .thenReturn("value");
		String response = stringCryptoConverter.convertToEntityAttribute(RandomStringUtils.randomAlphabetic(20));
		assertNotNull(response);
		assertEquals("value", response);

		String encryptedText = RandomStringUtils.randomAlphabetic(20);
		Mockito.when(mockPhiDataEncryption.decryptText(Mockito.eq(encryptedText), Mockito.any()))
		       .thenThrow(new RuntimeException());
		response = stringCryptoConverter.convertToEntityAttribute(encryptedText);
		assertNotNull(response);
		assertEquals(encryptedText, response);
	}
}
