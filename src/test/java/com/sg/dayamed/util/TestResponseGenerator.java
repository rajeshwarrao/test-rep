package com.sg.dayamed.util;

import com.sg.dayamed.controller.ws.ImanticNotificationReqModel;
import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.dao.UserDetailsCacheVO;
import com.sg.dayamed.entity.Address;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Adherence_DosageInfoMap;
import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.ConfigParams;
import com.sg.dayamed.entity.ConsumerGoodInfo;
import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.entity.Device;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.DeviceNotification;
import com.sg.dayamed.entity.DeviceStatus;
import com.sg.dayamed.entity.Diagnosis;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.DiseaseInfo;
import com.sg.dayamed.entity.DosageDevice;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.GSVUCAToken;
import com.sg.dayamed.entity.Glucometer;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.entity.ImanticNotification;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.NotificationReceivedUser;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.PulseOximeter;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserOptions;
import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;
import com.sg.dayamed.entity.view.PatientPharmacistAssociationsV;
import com.sg.dayamed.entity.view.PatientProviderAssociationsV;
import com.sg.dayamed.entity.view.PrescriptionInfoV;
import com.sg.dayamed.entity.view.UserPreferencesV;
import com.sg.dayamed.helper.pojo.CaregiverDosageNotificationModel;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingObjectReqModel;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingPayloadReqModel;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingReqModel;
import com.sg.dayamed.pojo.APNSUpdateTokenRequestModel;
import com.sg.dayamed.pojo.AddressModel;
import com.sg.dayamed.pojo.AdherenceReportModel;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.ConsumerGoodsModel;
import com.sg.dayamed.pojo.ConsumptionResponseModel;
import com.sg.dayamed.pojo.DiagnosisModel;
import com.sg.dayamed.pojo.DosageNotificationModel;
import com.sg.dayamed.pojo.FcmNotificationRequestModel;
import com.sg.dayamed.pojo.FirstConsumptionResponseModel;
import com.sg.dayamed.pojo.FirstConsumptionTimeModel;
import com.sg.dayamed.pojo.FrequencyPerDayModel;
import com.sg.dayamed.pojo.FristConsumptionTypeModel;
import com.sg.dayamed.pojo.MultiplePrescriptionSchedulingRequestModel;
import com.sg.dayamed.pojo.NotificationRequestModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.RoleModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.pojo.VideoNotificationRequestModel;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.ProviderVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.enums.DosageTimeOfTheDay;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imantics.akp.sdk.common.model.MetaData;
import com.imantics.akp.sdk.pid.model.PidDataTable;
import com.imantics.akp.sdk.pid.model.PidValue;
import com.imantics.dm.api.DmPatientDataModel;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.sg.dayamed.util.TestUtil;


/**
 * Created by Eresh Gorantla on 04/May/2019
 **/

public class TestResponseGenerator {

	public static final DayamedObjectMapper OBJECT_MAPPER = new DayamedObjectMapper();

	private TestResponseGenerator() {

	}

	public static Caregiver generateCaregiver(String gender, String race, String roleName, Boolean adminFlag) {
		Caregiver caregiver = new Caregiver();
		caregiver.setId(TestUtil.generateId());
		caregiver.setAdminflag(adminFlag);
		UserDetails userDetails = generateUserDetails(gender, race, roleName);
		caregiver.setUserDetails(userDetails);
		return caregiver;
	}

	public static Pharmacist generatePharmacist(String gender, String race, String roleName) {
		Pharmacist pharmacist = new Pharmacist();
		pharmacist.setId(TestUtil.generateId());
		UserDetails userDetails = generateUserDetails(gender, race, roleName);
		pharmacist.setUserDetails(userDetails);
		return pharmacist;
	}

	public static String generateUserCredentials(String username, String password, String typeOfDevice)
			throws JsonProcessingException {
		Map<String, String> userDetailsAsMap = new HashMap<>();
		if (username != null) {
			userDetailsAsMap.put("username", username);
		}
		if (password != null) {
			userDetailsAsMap.put("password", password);
		}
		if (typeOfDevice != null) {
			userDetailsAsMap.put("typeofdevice", typeOfDevice);
		}
		return new ObjectMapper().writeValueAsString(userDetailsAsMap);
	}

	public static CaregiverModel toCaregiverModel(Caregiver caregiver) {
		CaregiverModel caregiverModel = new CaregiverModel();
		caregiverModel.setId(caregiver.getId());
		final UserDetails userDetails = caregiver.getUserDetails();
		if (userDetails != null) {
			UserDetailsModel userDetailsModel = new UserDetailsModel();
			userDetailsModel.setImageUrl(userDetails
					                             .getImageUrl());
			RoleModel roleModel = new RoleModel();
			roleModel.setId(userDetails
					                .getRole() != null ? userDetails
					.getId() : null);
			roleModel.setName(userDetails
					                  .getRole() != null ? userDetails
					.getRole()
					.getName() : null);
			AddressModel addressModel = new AddressModel();
			final Address address = userDetails.getAddress();
			if (address != null) {
				BeanUtils.copyProperties(address, addressModel);
				userDetailsModel.setAddress(addressModel);
			}
			userDetailsModel.setRole(roleModel);
			userDetailsModel.setPassword(userDetails
					                             .getPassword());
			userDetailsModel.setType(userDetails
					                         .getType());
			userDetailsModel.setAge(String.valueOf(userDetails
					                                       .getAge()));
			userDetailsModel.setToken(userDetails
					                          .getToken());
			userDetailsModel.setEmailId(userDetails
					                            .getEmailId());
			userDetailsModel.setMiddleName(userDetails
					                               .getMiddleName());
			userDetailsModel.setFirstName(userDetails
					                              .getFirstName());
			userDetailsModel.setLastName(userDetails
					                             .getLastName());
			userDetailsModel.setMobileNumber(userDetails
					                                 .getMobileNumber());
			userDetailsModel.setCountryCode(userDetails
					                                .getCountryCode());
			userDetailsModel.setGender(userDetails
					                           .getGender());
			userDetailsModel.setImanticUserid(userDetails
					                                  .getImanticUserid());
			userDetailsModel.setId(userDetails
					                       .getId());
			userDetailsModel.setRace(userDetails
					                         .getRace());
			caregiverModel.setUserDetails(userDetailsModel);
		}
		return caregiverModel;
	}

	public static UserDetails generateUserDetails(String gender, String race, String roleName) {
		Role role = new Role();
		role.setName(roleName);
		role.setId(1);
		UserDetails userDetails = new UserDetails();
		userDetails.setId(TestUtil.generateId());
		userDetails.setRole(role);
		userDetails.setFirstName(RandomStringUtils.randomAlphabetic(20));
		userDetails.setMiddleName(RandomStringUtils.randomAlphabetic(20));
		userDetails.setLastName(RandomStringUtils.randomAlphabetic(20));
		userDetails.setPassword(RandomStringUtils.randomAlphanumeric(15));
		userDetails.setAge(TestUtil.generateAge());
		userDetails.setDateOfBirth(LocalDate.of(2000, 1, 1));
		userDetails.setAddress(generateAddress());
		userDetails.setPwdChangeToken(RandomStringUtils.randomAlphanumeric(50));
		userDetails.setPwdTokenExpireDate(new Date(System.currentTimeMillis() + 3600 * 1000));
		userDetails.setEmailId(RandomStringUtils.randomAlphanumeric(15) + "@dayamed.com");
		userDetails.setCountryCode("+91");
		userDetails.setGender(gender);
		userDetails.setRace(race);
		userDetails.setImanticUserid(TestUtil.generateImanticUserid());
		userDetails.setMobileNumber(TestUtil.generateMobileNumber());
		userDetails.setType(ApplicationConstants.ANDROID);
		userDetails.setToken(RandomStringUtils.randomAlphanumeric(50));
		return userDetails;
	}

	public static Patient generatePatient(String gender, String race, String roleName) {
		Patient patient = new Patient();
		patient.setId(TestUtil.generateId());
		patient.setUserDetails(generateUserDetails(gender, race, roleName));
		patient.setDeviceId("JUnit");
		patient.setEmoji(RandomStringUtils.randomAlphanumeric(20));
		patient.setStatus(1);
		patient.setProviders(
				new HashSet<>(Arrays.asList(generateProvider("Male", "Asian", ApplicationConstants.PROVIDER))));
		patient.setCaregivers(new HashSet<>(
				Arrays.asList(generateCaregiver("Male", "Asian", ApplicationConstants.CAREGIVER, false))));
		patient.setPharmacists(
				new HashSet<>(Arrays.asList(generatePharmacist("Male", "Asian", ApplicationConstants.PHARMACIST))));
		return patient;
	}

	public static Provider generateProvider(String gender, String race, String roleName) {
		Provider provider = new Provider();
		provider.setId(TestUtil.generateId());
		provider.setUserDetails(generateUserDetails(gender, race, roleName));
		provider.setLicence(RandomStringUtils.randomAlphanumeric(10));
		provider.setSpecialization(RandomStringUtils.randomAlphanumeric(20));
		return provider;
	}

	public static Role generateRole(String name) {
		Role role = new Role();
		role.setId(1);
		role.setName(name);
		return role;
	}

	public static JwtUserDetails generateJwtUserDetails(UserDetails userDetails, Boolean isPharamcistEnabled,
	                                                    String role) {
		JwtUserDetails jwtUserDetails = new JwtUserDetails(userDetails.getEmailId(), role, userDetails.getId(),
		                                                   UUID.randomUUID()
		                                                       .toString(),
		                                                   Stream.of(new SimpleGrantedAuthority(role))
		                                                         .collect(Collectors.toList()), "Junit", "UnitTest",
		                                                   userDetails.getEmailId(),
		                                                   isPharamcistEnabled);
		return jwtUserDetails;
	}

	public static DeviceInfo generateDeviceInfo(Integer duration, String status, Boolean deleteFlag) {
		DeviceInfo info = new DeviceInfo();
		info.setId(TestUtil.generateId());
		info.setDuration(duration);
		ArrayList<String> times = new ArrayList<>();
		times.add("10:00 AM");
		info.setTime(times);
		info.setSerialNo(RandomStringUtils.randomAlphanumeric(20));
		info.setComment(RandomStringUtils.randomAlphanumeric(50));
		info.setStatus(status);
		info.setDeleteflag(deleteFlag);
		Device device = new Device();
		device.setName(RandomStringUtils.randomAlphanumeric(10));
		device.setImageURL(RandomStringUtils.randomAlphanumeric(10));
		info.setDevice(device);
		return info;
	}

	public static DeviceStatus generateDeviceStatus(String batteryStatus) {
		DeviceStatus status = new DeviceStatus();
		status.setBatteryStatus(batteryStatus);
		status.setLatitude(TestUtil.generateLatitude()
		                           .toString());
		status.setLongitude(TestUtil.generateLongitude()
		                            .toString());
		status.setMsg(RandomStringUtils.randomAlphanumeric(50));
		status.setId(TestUtil.generateId());
		status.setUserDetails(generateUserDetails("Male", "Asian", "Patient"));
		return status;
	}

	public static ConsumerGoods generateConsumerGoods() {
		ConsumerGoods consumerGoods = new ConsumerGoods();
		consumerGoods.setDescription(RandomStringUtils.randomAlphanumeric(10));
		consumerGoods.setName(RandomStringUtils.randomAlphanumeric(10));
		consumerGoods.setImageURL(RandomStringUtils.randomAlphanumeric(10));
		return consumerGoods;
	}

	public static Device generateDevice() {
		Device device = new Device();
		device.setDescription(RandomStringUtils.randomAlphanumeric(10));
		device.setName(RandomStringUtils.randomAlphanumeric(10));
		return device;
	}

	public static DosageInfo generateDosageInfo(Boolean deleteFlag) {
		DosageInfo dosageInfo = new DosageInfo();
		dosageInfo.setDeleteflag(deleteFlag);
		dosageInfo.setId(TestUtil.generateId());
		dosageInfo.setDuration(TestUtil.generateAge());
		dosageInfo.setComment(RandomStringUtils.randomAlphanumeric(50));
		dosageInfo.setPcomment(RandomStringUtils.randomAlphanumeric(50));
		dosageInfo.setMedicine(generateMedicine());
		dosageInfo.setFrequency("{\"type\":\"daily\",\"startDate\":\"08/26/2019\",\"value\":20,\"recurrence\":null," +
				                        "\"endAfterOccurrences\":null}");
		ArrayList<String> timesList = new ArrayList<>();
		timesList.add("10:00 AM");
		timesList.add("07:00 PM");
		dosageInfo.setTime(timesList);
		dosageInfo.setDosageNotificationList(Stream.of(generateDosageNotification())
		                                           .collect(Collectors.toList()));
		return dosageInfo;
	}

	public static Medicine generateMedicine() {
		Medicine medicine = new Medicine();
		medicine.setName(RandomStringUtils.randomAlphanumeric(10));
		medicine.setId(TestUtil.generateId());
		medicine.setImageURL(RandomStringUtils.randomAlphabetic(10));
		medicine.setType(RandomStringUtils.randomAlphabetic(10));
		medicine.setDescription(RandomStringUtils.randomAlphabetic(10));
		medicine.setCategory(RandomStringUtils.randomAlphabetic(10));
		medicine.setItemCategoryType(RandomStringUtils.randomAlphabetic(10));
		medicine.setGenCode(TestUtil.generateId());
		medicine.setNdcCode(TestUtil.generateId());
		medicine.setNdcType(RandomStringUtils.randomAlphabetic(10));
		medicine.setPackager(RandomStringUtils.randomAlphabetic(10));
		return medicine;
	}

	public static HeartRate generHeartRate() {
		HeartRate heartRate = new HeartRate();
		heartRate.setId(TestUtil.generateId());
		heartRate.setConsumptionStatus(Stream.of("Consumed")
		                                     .collect(Collectors.toList()));
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		LocalDateTime obsDateTime = LocalDateTime.now();
		heartRate.setPrescribedTime(presDateTime);
		heartRate.setObservedTime(obsDateTime);
		heartRate.setPatient(generatePatient("Male", "Asian", "Patient"));
		heartRate.setPrescriptionID(TestUtil.generateId());
		heartRate.setDeviceInfoId(RandomStringUtils.randomAlphanumeric(10));
		heartRate.setReadingValue(RandomStringUtils.randomAlphanumeric(20));
		return heartRate;
	}

	public static Prescription generatePrescription() {
		Prescription prescription = new Prescription();
		prescription.setPatient(generatePatient("Male", "Asian", "Patient"));
		prescription.setDisease(RandomStringUtils.randomAlphanumeric(10));
		prescription.setComment(RandomStringUtils.randomAlphanumeric(50));
		prescription.setId(TestUtil.generateId());
		prescription.setDiseaseInfoList(Stream.of(generateDiseaseInfo(prescription))
		                                      .collect(Collectors.toList()));
		DosageInfo dosageInfo = generateDosageInfo(false);
		DosageNotification notification = generateDosageNotification(dosageInfo);
		dosageInfo.setDosageNotificationList(Stream.of(notification)
		                                           .collect(Collectors.toList()));
		dosageInfo.setPrescription(prescription);
		prescription.setDosageInfoList(Stream.of(dosageInfo)
		                                     .collect(Collectors.toList()));
		DeviceInfo deviceInfo = generateDeviceInfo(1, "1", false);
		DeviceNotification deviceNotification = generateDeviceNotification(deviceInfo);
		deviceInfo.setDeviceNotificationList(Stream.of(deviceNotification)
		                                           .collect(Collectors.toList()));
		prescription.setDeviceInfoList(Stream.of(deviceInfo)
		                                     .collect(Collectors.toList()));
		ConsumerGoodInfo consumerGoodInfo = generateConsumerGoodInfo(prescription);
		prescription.setCommodityInfoList(Stream.of(consumerGoodInfo)
		                                        .collect(Collectors.toList()));
		return prescription;
	}

	public static PrescriptionModel generatePrescriptionModel() {
		PrescriptionModel prescription = new PrescriptionModel();
		prescription.setPatient(generatePatientModel());
		prescription.setComment(RandomStringUtils.randomAlphanumeric(50));
		prescription.setId(TestUtil.generateId());
		return prescription;
	}

	public static DeviceNotification generateDeviceNotification(DeviceInfo deviceInfo) {
		DeviceNotification deviceNotification = new DeviceNotification();
		deviceNotification.setCountryCode("+91");
		deviceNotification.setDeviceInfo(deviceInfo);
		deviceNotification.setGreaterThanReading1(RandomStringUtils.randomAlphanumeric(1));
		deviceNotification.setGreaterThanReading2(RandomStringUtils.randomAlphanumeric(5));
		deviceNotification.setExpectedTime("10");
		deviceNotification.setId(TestUtil.generateId());
		deviceNotification.setLessThanReading1(RandomStringUtils.randomAlphanumeric(5));
		deviceNotification.setLessThanReading2(RandomStringUtils.randomAlphanumeric(5));
		deviceNotification.setNotificationType("EMAIL");
		deviceNotification.setNotificationValue(RandomStringUtils.randomAlphanumeric(10));
		return deviceNotification;
	}

	public static DosageNotification generateDosageNotification(DosageInfo dosageInfo) {
		DosageNotification notification = new DosageNotification();
		notification.setDosageInfo(dosageInfo);
		notification.setUserId(TestUtil.generateId());
		notification.setCountryCode("+91");
		notification.setExpectedTime("10:00");
		notification.setNotificationType("EMAIL");
		notification.setNotificationValue(RandomStringUtils.randomAlphanumeric(10));
		return notification;
	}

	public static ConsumerGoodInfo generateConsumerGoodInfo(Prescription prescription) {
		ConsumerGoodInfo info = new ConsumerGoodInfo();
		info.setDeleteflag(false);
		info.setPrescription(prescription);
		info.setComment(RandomStringUtils.randomAlphanumeric(40));
		info.setId(TestUtil.generateId());
		return info;
	}

	public static PrescriptionSchedule generatePrescriptionSchedule() {
		PrescriptionSchedule schedule = new PrescriptionSchedule();
		schedule.setFireTime(LocalDateTime.now());
		schedule.setPrescribedTime(LocalDateTime.now());
		schedule.setUserId(TestUtil.generateId());
		schedule.setConsumptionStatus(Collections.emptyList());
		schedule.setId(TestUtil.generateId());
		return schedule;
	}

	public static NotificationSchedule generateEmailNotificationSchedule() {
		NotificationSchedule schedule = new NotificationSchedule();
		schedule.setNotificationValue(RandomStringUtils.randomAlphabetic(10) + "@gmail.com");
		schedule.setActualDate(LocalDateTime.now());
		schedule.setFireTime(LocalDateTime.now());
		schedule.setUserId(TestUtil.generateId());
		schedule.setId(TestUtil.generateId());
		schedule.setNotificationType("EMAIL");
		schedule.setConsumptionStatus(Collections.emptyList());
		schedule.setPrescriptionId(TestUtil.generateId());
		schedule.setOwner_userId(TestUtil.generateId());
		return schedule;
	}

	public static NotificationSchedule generateSMSNotificationSchedule() {
		NotificationSchedule schedule = new NotificationSchedule();
		schedule.setNotificationValue(RandomStringUtils.randomNumeric(10));
		schedule.setActualDate(LocalDateTime.now());
		schedule.setFireTime(LocalDateTime.now());
		schedule.setUserId(TestUtil.generateId());
		schedule.setId(TestUtil.generateId());
		schedule.setNotificationType("SMS");
		schedule.setConsumptionStatus(Collections.emptyList());
		schedule.setPrescriptionId(TestUtil.generateId());
		schedule.setOwner_userId(TestUtil.generateId());
		return schedule;
	}

	public static DiseaseInfo generateDiseaseInfo(Prescription prescription) {
		DiseaseInfo diseaseInfo = new DiseaseInfo();
		diseaseInfo.setPrescription(prescription);
		diseaseInfo.setId(TestUtil.generateId());
		diseaseInfo.setDeleteflag(false);
		diseaseInfo.setDisease(generateDisease());
		return diseaseInfo;
	}

	public static Disease generateDisease() {
		Disease disease = new Disease();
		disease.setDescription(RandomStringUtils.randomAlphanumeric(100));
		disease.setName(RandomStringUtils.randomAlphanumeric(25));
		disease.setId(TestUtil.generateId());
		return disease;
	}

	public static Diagnosis generateDiagnosis() {
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.setName(RandomStringUtils.randomAlphanumeric(25));
		diagnosis.setId(TestUtil.generateId());
		return diagnosis;
	}

	public static DiagnosisModel generateDiagnosisModel() {
		DiagnosisModel diagnosisModel = new DiagnosisModel();
		diagnosisModel.setName(RandomStringUtils.randomAlphanumeric(25));
		diagnosisModel.setId(TestUtil.generateId());
		return diagnosisModel;
	}

	public static PrescriptionStatus generatePrescriptionStatus() {
		PrescriptionStatus prescriptionStatus = new PrescriptionStatus();
		prescriptionStatus.setName("New");
		prescriptionStatus.setId(1L);
		return prescriptionStatus;

	}

	public static DosageDevice generateDosageDevice(Prescription prescription) {
		DosageDevice dosageDevice = new DosageDevice();
		dosageDevice.setDevice("JUnit");
		dosageDevice.setPrescription(prescription);
		dosageDevice.setId(TestUtil.generateId());
		dosageDevice.setSerialNo(RandomStringUtils.randomAlphanumeric(10));
		dosageDevice.setStatus("Consumed");
		return dosageDevice;
	}

	public static DmPatientDataModel generateDmPatientDataModel(Patient patient) {
		DmPatientDataModel patientDataModel = new DmPatientDataModel();
		patientDataModel.setId(TestUtil.generateId()
		                               .toString());
		patientDataModel.setPatientName(patient.getUserDetails() != null ? patient.getUserDetails()
		                                                                          .getFirstName() : null);
		patientDataModel.setDescription(RandomStringUtils.randomAlphanumeric(100));
		patientDataModel.setSerialNumber(RandomStringUtils.randomAlphanumeric(50));
		patientDataModel.setName(RandomStringUtils.randomAlphanumeric(20));
		patientDataModel.set_id(TestUtil.generateImanticUserid());
		patientDataModel.setAppId("JUnit");
		patientDataModel.setUserIds(Stream.of(String.valueOf(patient.getId()))
		                                  .collect(Collectors.toList()));
		patientDataModel.setMetadata(Stream.of(generateMetaData())
		                                   .collect(Collectors.toList()));
		return patientDataModel;
	}

	public static MetaData generateMetaData() {
		MetaData metaData = new MetaData();
		metaData.setKey(RandomStringUtils.randomAlphanumeric(10));
		metaData.setValue(RandomStringUtils.randomAlphanumeric(10));
		return metaData;
	}

	public static AdherenceDataPoints generateAdherenceDataPoints(List<String> consumptionStatus) {
		AdherenceDataPoints dataPoints = new AdherenceDataPoints();
		dataPoints.setId(TestUtil.generateId());
		dataPoints.setConsumptionStatus(consumptionStatus);
		ArrayList<String> locations = new ArrayList<>();
		locations.add(TestUtil.generateLatitude()
		                      .toString());
		locations.add(TestUtil.generateLongitude()
		                      .toString());
		dataPoints.setLocation(locations);
		dataPoints.setPrescribedTime(LocalDateTime.now());
		dataPoints.setAdherenceDosageInfoMapList(Stream.of(generateAdherence_dosageInfoMap(dataPoints))
		                                               .collect(Collectors.toList()));
		dataPoints.setObservedTime(LocalDateTime.now());
		return dataPoints;
	}

	public static Adherence_DosageInfoMap generateAdherence_dosageInfoMap(AdherenceDataPoints adherenceDataPoints) {
		Adherence_DosageInfoMap dosageInfoMap = new Adherence_DosageInfoMap();
		dosageInfoMap.setDosageinfoId(TestUtil.generateId());
		dosageInfoMap.setAdherence(adherenceDataPoints);
		dosageInfoMap.setDosageinfoId(TestUtil.generateId());
		return dosageInfoMap;
	}

	public static PidDataTable generatePidDataTable() {
		PidDataTable dataTable = new PidDataTable();
		dataTable.setDeviceId("Junit");
		dataTable.setModuleId(RandomStringUtils.randomAlphanumeric(10));
		dataTable.setPid(RandomStringUtils.randomAlphanumeric(10));
		dataTable.setSubmoduleId(RandomStringUtils.randomAlphanumeric(10));
		dataTable.setValues(Stream.of(generatePidValue())
		                          .collect(Collectors.toList()));
		return dataTable;
	}

	public static PidValue generatePidValue() {
		PidValue pidValue = new PidValue();
		pidValue.setValue(RandomStringUtils.randomAlphanumeric(10));
		Date date = new Date();
		pidValue.setTimestamp(String.valueOf(date.getTime()));
		return pidValue;
	}

	public static Glucometer generateGlucometer() {
		Glucometer glucometer = new Glucometer();
		glucometer.setReadingValue(RandomStringUtils.randomAlphanumeric(20));
		glucometer.setDeviceInfoId("Junit");
		glucometer.setPrescriptionID(TestUtil.generateId());
		glucometer.setId(TestUtil.generateId());
		glucometer.setPrescribedTime(LocalDateTime.now()
		                                          .plusMinutes(-10));
		glucometer.setObservedTime(LocalDateTime.now());
		glucometer.setConsumptionStatus(Stream.of("Consumed")
		                                      .collect(Collectors.toList()));
		glucometer.setPatient(generatePatient("Male", "Asian", "Patient"));
		ArrayList<String> locations = new ArrayList<>();
		locations.add(TestUtil.generateLatitude()
		                      .toString());
		locations.add(TestUtil.generateLongitude()
		                      .toString());
		glucometer.setLocation(locations);
		return glucometer;
	}

	public static Scale generateScale() {
		Scale scale = new Scale();
		scale.setReading(RandomStringUtils.randomAlphanumeric(20));
		scale.setId(TestUtil.generateId());
		scale.setDeviceInfoId("JUnit");
		scale.setConsumptionStatus(Stream.of("Consumed")
		                                 .collect(Collectors.toList()));
		scale.setPatient(generatePatient("Male", "Asian", "Patient"));
		scale.setPrescribedTime(LocalDateTime.now()
		                                     .plusMinutes(-10));
		scale.setObservedTime(LocalDateTime.now());
		ArrayList<String> locations = new ArrayList<>();
		locations.add(TestUtil.generateLatitude()
		                      .toString());
		locations.add(TestUtil.generateLongitude()
		                      .toString());
		scale.setLocation(locations);
		return scale;
	}

	public static BpMonitor generateBpMonitor() {
		BpMonitor bpMonitor = new BpMonitor();
		bpMonitor.setPrescribedTime(LocalDateTime.now()
		                                         .plusMinutes(-10));
		bpMonitor.setObservedTime(LocalDateTime.now());
		bpMonitor.setId(TestUtil.generateId());
		bpMonitor.setConsumptionStatus(Stream.of("Consumed")
		                                     .collect(Collectors.toList()));
		bpMonitor.setPatient(generatePatient("Male", "Asian", "Patient"));
		ArrayList<String> locations = new ArrayList<>();
		locations.add(TestUtil.generateLatitude()
		                      .toString());
		locations.add(TestUtil.generateLongitude()
		                      .toString());
		bpMonitor.setLocation(locations);
		bpMonitor.setDeviceInfoId("Junit");
		bpMonitor.setDiastolicpressureValue(RandomStringUtils.randomAlphanumeric(20));
		bpMonitor.setSystolicpressureValue(RandomStringUtils.randomAlphanumeric(20));
		return bpMonitor;
	}

	public static Steps generateSteps() {
		Steps steps = new Steps();
		steps.setId(TestUtil.generateId());
		steps.setPrescriptionID(TestUtil.generateId());
		steps.setObservedTime(LocalDateTime.now());
		steps.setId(TestUtil.generateId());
		steps.setConsumptionStatus(Stream.of("Consumed")
		                                 .collect(Collectors.toList()));
		steps.setPatient(generatePatient("Male", "Asian", "Patient"));
		ArrayList<String> locations = new ArrayList<>();
		locations.add(TestUtil.generateLatitude()
		                      .toString());
		locations.add(TestUtil.generateLongitude()
		                      .toString());
		steps.setLocation(locations);
		steps.setDeviceInfoId("Junit");
		steps.setStepCount(TestUtil.generateAge());
		return steps;
	}

	public static PulseOximeter generatePulseOximeter() {
		PulseOximeter pulseOximeter = new PulseOximeter();
		pulseOximeter.setTakenTime(LocalDateTime.now());
		pulseOximeter.setSp02(RandomStringUtils.randomAlphanumeric(10));
		pulseOximeter.setPulse(TestUtil.generateAge());
		pulseOximeter.setId(TestUtil.generateId());
		pulseOximeter.setPatient(generatePatient("Male", "Asian", "Patient"));
		return pulseOximeter;
	}

	public static ConfigParams generateConfigParams() {
		ConfigParams configParams = new ConfigParams();
		configParams.setId(TestUtil.generateId());
		configParams.setKeyParam(RandomStringUtils.randomAlphanumeric(10));
		configParams.setUpdateDate(new Date());
		configParams.setValueParam(RandomStringUtils.randomAlphanumeric(10));
		return configParams;
	}

	public static Notification generateNotification() {
		Notification notification = new Notification();
		notification.setDeleteflag(false);
		notification.setMessage(RandomStringUtils.randomAlphanumeric(100));
		notification.setTime(new Date());
		notification.setTitle(RandomStringUtils.randomAlphanumeric(20));
		notification.setBelongigngUserDetails(generateUserDetails("Male", "Asian", "Patient"));
		notification.setType(RandomStringUtils.randomAlphanumeric(5));
		notification.setId(TestUtil.generateId());
		notification.setNotificationReceivedUserList(
				Stream.of(generateNotificationReceivedUser(notification, notification.getBelongigngUserDetails()))
				      .
						      collect(Collectors.toList()));
		return notification;
	}

	public static VidoeCallSignatureModel generateVidoeCallSignatureModel() {
		VidoeCallSignatureModel model = new VidoeCallSignatureModel();
		model.setCallType(RandomStringUtils.randomAlphanumeric(10));
		model.setApiKey(UUID.randomUUID()
		                    .toString());
		model.setRole(TestUtil.generateAge());
		model.setSignature(RandomStringUtils.randomAlphanumeric(50));
		model.setMeetingNumber(TestUtil.generateMobileNumber());
		model.setPasswd(RandomStringUtils.randomAlphanumeric(20));
		model.setRecevierUserID(RandomStringUtils.randomAlphanumeric(40));
		return model;
	}

	public static NotificationReceivedUser generateNotificationReceivedUser(Notification notification,
	                                                                        UserDetails userDetails) {
		NotificationReceivedUser receivedUser = new NotificationReceivedUser();
		receivedUser.setNotification(notification);
		receivedUser.setUserDetails(userDetails);
		receivedUser.setId(TestUtil.generateId());
		return receivedUser;
	}

	public static Map<String, String> generateImaticMap() {
		Map<String, String> imaticMap = new HashMap<>();
		imaticMap.put("imanticid", TestUtil.generateImanticUserid());
		return imaticMap;
	}

	public static Map<String, String> generateImaticAnalyticMap() {
		Map<String, String> imaticMap = new HashMap<>();
		imaticMap.put("predictedAdherence", RandomStringUtils.randomAlphanumeric(20));
		imaticMap.put("currentAdherence", RandomStringUtils.randomAlphanumeric(20));
		imaticMap.put("projectedAdherence", RandomStringUtils.randomAlphanumeric(20));
		imaticMap.put("emoji", RandomStringUtils.randomAlphanumeric(20));
		return imaticMap;
	}

	public static PatientAdherencePojo generatePatientAdherencePojo() {
		return new PatientAdherencePojo(RandomStringUtils.randomAlphanumeric(10),
		                                RandomStringUtils.randomAlphanumeric(10),
		                                RandomStringUtils.randomAlphanumeric(10),
		                                RandomStringUtils.randomAlphanumeric(10));
	}

	public static PatientAssociationDetailsV generatePatientAssociationDetailsV() {
		PatientAssociationDetailsV associationDetailsV = new PatientAssociationDetailsV();
		associationDetailsV.setAge(TestUtil.generateAge());
		associationDetailsV.setFirstName(RandomStringUtils.randomAlphabetic(15));
		associationDetailsV.setLastName(RandomStringUtils.randomAlphabetic(10));
		associationDetailsV.setEmailFlag(false);
		associationDetailsV.setEmailId(RandomStringUtils.randomAlphanumeric(10) + "@gmail.com");
		associationDetailsV.setMobileNumber(TestUtil.generateMobileNumber());
		associationDetailsV.setSmsFlag(false);
		associationDetailsV.setCity(RandomStringUtils.randomAlphabetic(10));
		associationDetailsV.setLocation(RandomStringUtils.randomAlphabetic(15));
		associationDetailsV.setState(RandomStringUtils.randomAlphabetic(12));
		associationDetailsV.setCountry("India");
		associationDetailsV.setZipCode(RandomStringUtils.randomNumeric(6));
		associationDetailsV.setGender("Male");
		associationDetailsV.setUserId(TestUtil.generateId());
		associationDetailsV.setUserCreatedOn(LocalDateTime.now()
		                                                  .plusDays(-TestUtil.generateAge()));
		associationDetailsV.setLastActiveTime(LocalDateTime.now()
		                                                   .plusDays(-TestUtil.generateAge()));
		associationDetailsV.setPatientId(TestUtil.generateId());
		return associationDetailsV;
	}

	public static PatientCaregiverAssociationsV generatePatientCaregiverAssociationsV() {
		PatientCaregiverAssociationsV associationsV = new PatientCaregiverAssociationsV();
		associationsV.setAge(TestUtil.generateAge());
		associationsV.setFirstName(RandomStringUtils.randomAlphabetic(15));
		associationsV.setLastName(RandomStringUtils.randomAlphabetic(10));
		associationsV.setEmailFlag(false);
		associationsV.setEmailId(RandomStringUtils.randomAlphanumeric(10) + "@gmail.com");
		associationsV.setMobileNumber(TestUtil.generateMobileNumber());
		associationsV.setSmsFlag(false);
		associationsV.setCity(RandomStringUtils.randomAlphabetic(10));
		associationsV.setLocation(RandomStringUtils.randomAlphabetic(15));
		associationsV.setState(RandomStringUtils.randomAlphabetic(12));
		associationsV.setCountry("India");
		associationsV.setZipCode(RandomStringUtils.randomNumeric(6));
		associationsV.setGender("Male");
		associationsV.setUserId(TestUtil.generateId());
		associationsV.setUserCreatedOn(LocalDateTime.now()
		                                            .plusDays(-TestUtil.generateAge()));
		associationsV.setLastActiveTime(LocalDateTime.now()
		                                             .plusDays(-TestUtil.generateAge()));
		associationsV.setPatientId(TestUtil.generateId());
		return associationsV;
	}

	public static PatientPharmacistAssociationsV generatePatientPharmacistAssociationsV() {
		PatientPharmacistAssociationsV associationsV = new PatientPharmacistAssociationsV();
		associationsV.setAge(TestUtil.generateAge());
		associationsV.setFirstName(RandomStringUtils.randomAlphabetic(15));
		associationsV.setLastName(RandomStringUtils.randomAlphabetic(10));
		associationsV.setEmailFlag(false);
		associationsV.setEmailId(RandomStringUtils.randomAlphanumeric(10) + "@gmail.com");
		associationsV.setMobileNumber(TestUtil.generateMobileNumber());
		associationsV.setSmsFlag(false);
		associationsV.setCity(RandomStringUtils.randomAlphabetic(10));
		associationsV.setLocation(RandomStringUtils.randomAlphabetic(15));
		associationsV.setState(RandomStringUtils.randomAlphabetic(12));
		associationsV.setCountry("India");
		associationsV.setZipCode(RandomStringUtils.randomNumeric(6));
		associationsV.setGender("Male");
		associationsV.setUserId(TestUtil.generateId());
		associationsV.setUserCreatedOn(LocalDateTime.now()
		                                            .plusDays(-TestUtil.generateAge()));
		associationsV.setLastActiveTime(LocalDateTime.now()
		                                             .plusDays(-TestUtil.generateAge()));
		associationsV.setPatientId(TestUtil.generateId());
		return associationsV;
	}

	public static PatientDetailsVO generatePatientDetailsVO() {
		PatientDetailsVO detailsVO = new PatientDetailsVO();
		detailsVO.setId(TestUtil.generateId());
		detailsVO.setEmoji(RandomStringUtils.randomAlphabetic(10));
		detailsVO.setPatientStatus(0);
		UserDetailsVO userDetailsVO = new UserDetailsVO();
		userDetailsVO.setRace("Asian");
		userDetailsVO.setMobileNumber(TestUtil.generateMobileNumber());
		userDetailsVO.setId(TestUtil.generateId());
		userDetailsVO.setImanticUserId(TestUtil.generateImanticUserid());
		userDetailsVO.setEmailId(RandomStringUtils.randomAlphanumeric(15) + "@gmail.com");
		userDetailsVO.setMobileNumber(TestUtil.generateMobileNumber());
		userDetailsVO.setLocation(RandomStringUtils.randomAlphabetic(10));
		userDetailsVO.setFirstName(RandomStringUtils.randomAlphabetic(10));
		userDetailsVO.setLastName(RandomStringUtils.randomAlphabetic(10));
		userDetailsVO.setMiddleName(RandomStringUtils.randomAlphabetic(5));
		userDetailsVO.setCountry("India");
		userDetailsVO.setState(RandomStringUtils.randomAlphabetic(10));
		userDetailsVO.setZipCode(RandomStringUtils.randomNumeric(6));
		userDetailsVO.setCity(RandomStringUtils.randomAlphabetic(10));
		userDetailsVO.setUserCreatedOn(LocalDateTime.now()
		                                            .plusDays(-TestUtil.generateAge()));
		userDetailsVO.setLastActiveTime(LocalDateTime.now()
		                                             .plusHours(-TestUtil.generateAge()));
		userDetailsVO.setEmailFlag(false);
		userDetailsVO.setSmsFlag(false);
		detailsVO.setUserDetails(userDetailsVO);
		return detailsVO;
	}

	public static UserPreferenceVO generateUserPreferenceVO() {
		UserPreferenceVO preferenceVO = new UserPreferenceVO();
		preferenceVO.setAdminCaregiver(true);
		preferenceVO.setAge(TestUtil.generateAge());
		preferenceVO.setCaregiverId(TestUtil.generateId());
		preferenceVO.setRace("Asian");
		preferenceVO.setMobileNumber(TestUtil.generateMobileNumber());
		preferenceVO.setId(UUID.randomUUID()
		                       .toString());
		preferenceVO.setImanticUserId(TestUtil.generateImanticUserid());
		preferenceVO.setEmailId(RandomStringUtils.randomAlphanumeric(15) + "@gmail.com");
		preferenceVO.setMobileNumber(TestUtil.generateMobileNumber());
		preferenceVO.setLocation(RandomStringUtils.randomAlphabetic(10));
		preferenceVO.setFirstName(RandomStringUtils.randomAlphabetic(10));
		preferenceVO.setLastName(RandomStringUtils.randomAlphabetic(10));
		preferenceVO.setMiddleName(RandomStringUtils.randomAlphabetic(5));
		preferenceVO.setCountry("India");
		preferenceVO.setState(RandomStringUtils.randomAlphabetic(10));
		preferenceVO.setZipCode(RandomStringUtils.randomNumeric(6));
		preferenceVO.setCity(RandomStringUtils.randomAlphabetic(10));
		preferenceVO.setUserCreatedOn(LocalDateTime.now()
		                                           .plusDays(-TestUtil.generateAge()));
		preferenceVO.setLastActiveTime(LocalDateTime.now()
		                                            .plusHours(-TestUtil.generateAge()));
		preferenceVO.setEmailFlag(false);
		preferenceVO.setSmsFlag(false);
		return preferenceVO;
	}

	public static PatientDetailsResponseVO generaPatientDetailsResponseVO(Integer limit, Integer totalRecords) {
		PatientDetailsResponseVO responseVO = new PatientDetailsResponseVO();
		responseVO.setTotalRecords(totalRecords);
		List<PatientDetailsVO> detailsVOS = new ArrayList<>();
		for (Integer index = 0; index < limit; index++) {
			detailsVOS.add(generatePatientDetailsVO());
		}
		responseVO.setPatientDetails(detailsVOS);
		return responseVO;
	}

	public static LoadPatientRequestVO generateLoadPatientRequest(UserRoleEnum userRoleEnum) {
		LoadPatientRequestVO requestVO = new LoadPatientRequestVO();
		requestVO.setSortOrderEnum(SortOrderEnum.ASC);
		requestVO.setSortByEnum(PatientSortByEnum.FIRST_NAME);
		requestVO.setNonAdherentFlag(true);
		requestVO.setOffset(0);
		requestVO.setLimit(10);
		requestVO.setUserDetails(TestResponseGenerator.generateJwtUserDetails(
				TestResponseGenerator.generateUserDetails("Male", "Asian", userRoleEnum.getRole()),
				true, userRoleEnum.getRole()));
		return requestVO;
	}

	public static PatientAssociationsRequestVO generatePatientAssociationsRequest(UserRoleEnum userRoleEnum) {
		PatientAssociationsRequestVO requestVO = new PatientAssociationsRequestVO();
		requestVO.setPatientId(TestUtil.generateId());
		requestVO.setProviderId(TestUtil.generateId());
		requestVO.setUserDetails(TestResponseGenerator.generateJwtUserDetails(
				TestResponseGenerator.generateUserDetails("Male", "Asian", userRoleEnum.getRole()),
				true, userRoleEnum.getRole()));
		requestVO.setCaregiverId(TestUtil.generateId());
		return requestVO;
	}

	public static UserPreferencesV generateUserPreferencesV() {
		UserPreferencesV preferencesV = new UserPreferencesV();
		preferencesV.setAdminCaregiver(true);
		preferencesV.setAge(TestUtil.generateAge());
		preferencesV.setAppVersion(RandomStringUtils.random(1));
		preferencesV.setCaregiverId(TestUtil.generateId());
		preferencesV.setCity(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setCountry(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setCountryCode(RandomStringUtils.randomNumeric(2));
		preferencesV.setEmailFlag(true);
		preferencesV.setFirstName(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setEmailId(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setGender("Male");
		preferencesV.setLastActiveTime(LocalDateTime.now());
		preferencesV.setLastLoginTime(LocalDateTime.now());
		preferencesV.setId(UUID.randomUUID()
		                       .toString());
		preferencesV.setImanticUserId(TestUtil.generateImanticUserid());
		preferencesV.setLastName(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setMiddleName(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setMobileNumber(TestUtil.generateMobileNumber());
		preferencesV.setLocation(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setZipCode(RandomStringUtils.randomNumeric(6));
		preferencesV.setUserCreatedDate(LocalDateTime.now());
		preferencesV.setUserCreatedDate(LocalDateTime.now());
		preferencesV.setUserDeviceType(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setToken(UUID.randomUUID()
		                          .toString());
		preferencesV.setVoipToken(UUID.randomUUID()
		                              .toString());
		preferencesV.setAppVersion("1");
		preferencesV.setOsVersion("1");
		preferencesV.setPatientId(TestUtil.generateId());
		preferencesV.setZoneId(RandomStringUtils.randomAlphanumeric(10));
		preferencesV.setUserZoneId(RandomStringUtils.randomAlphanumeric(10));
		preferencesV.setUserUpdatedOn(LocalDateTime.now());
		preferencesV.setUserUpdatedDate(LocalDateTime.now());
		preferencesV.setUserToken(RandomStringUtils.randomAlphabetic(40));
		preferencesV.setUserPrefId(TestUtil.generateId());
		preferencesV.setUserPreference(new UserPreferenceModel());
		preferencesV.setLocation(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setUserImageUrl(RandomStringUtils.randomAlphabetic(100));
		preferencesV.setUserId(TestUtil.generateId());
		preferencesV.setUserCreatedOn(LocalDateTime.now());
		preferencesV.setState(RandomStringUtils.randomAlphabetic(10));
		preferencesV.setSmsFlag(false);
		preferencesV.setRoleId(NumberUtils.LONG_ONE);
		preferencesV.setRace("Asian");
		preferencesV.setPwdTokenExpireDate(LocalDateTime.now());
		preferencesV.setPwdChangeToken(RandomStringUtils.randomAlphanumeric(100));
		preferencesV.setProviderId(TestUtil.generateId());
		preferencesV.setPharmacistId(TestUtil.generateId());
		preferencesV.setPharmacistEnable(false);
		preferencesV.setPassword(RandomStringUtils.randomAlphanumeric(100));
		preferencesV.setObjectMapper(new ObjectMapper());
		preferencesV.setImaticUserId(UUID.randomUUID()
		                                 .toString());
		return preferencesV;
	}

	public static UserOptions generateUserOptions(Integer gsPharmaEnable, UserDetails userDetails) {
		UserOptions userOptions = new UserOptions();
		userOptions.setGsPharmacistEnable(gsPharmaEnable);
		userOptions.setUserDetails(userDetails);
		userOptions.setId(TestUtil.generateId());
		return userOptions;
	}

	public static ProviderVO generateProviderVO() {
		ProviderVO providerVO = new ProviderVO();
		providerVO.setSpecialization(RandomStringUtils.randomAlphabetic(10));
		providerVO.setLicense(RandomStringUtils.randomAlphabetic(30));
		providerVO.setId(TestUtil.generateId());
		UserPreferenceVO userPreferenceVO = generateUserPreferenceVO();
		providerVO.setUserDetails(new UserDetailsVO(userPreferenceVO));
		return providerVO;
	}

	public static PatientDetailsVO toPatientDetailsVO(PatientAssociationDetailsV detailsV) {
		PatientDetailsVO patientDetailsVO = new PatientDetailsVO();
		patientDetailsVO.setCurrentAdherence(detailsV.getCurrentAdherence());
		patientDetailsVO.setEmoji(detailsV.getEmoji());
		patientDetailsVO.setId(detailsV.getPatientId());
		UserDetailsVO userDetailsVO = new UserDetailsVO();
		userDetailsVO.setAge(detailsV.getAge());
		userDetailsVO.setCity(detailsV.getCity());
		userDetailsVO.setCountry(detailsV.getCountry());
		userDetailsVO.setEmailFlag(detailsV.getEmailFlag());
		userDetailsVO.setEmailId(detailsV.getEmailId());
		userDetailsVO.setFirstName(detailsV.getFirstName());
		userDetailsVO.setMiddleName(detailsV.getMiddleName());
		userDetailsVO.setLastActiveTime(detailsV.getLastActiveTime());
		userDetailsVO.setUserCreatedOn(detailsV.getUserCreatedOn());
		userDetailsVO.setLastName(detailsV.getLastName());
		userDetailsVO.setLocation(detailsV.getLocation());
		userDetailsVO.setId(detailsV.getUserId());
		userDetailsVO.setMobileNumber(detailsV.getMobileNumber());
		userDetailsVO.setRace(detailsV.getRace());
		userDetailsVO.setSmsFlag(detailsV.getSmsFlag());
		userDetailsVO.setState(detailsV.getState());
		userDetailsVO.setZipCode(detailsV.getZipCode());
		patientDetailsVO.setUserDetails(userDetailsVO);
		return patientDetailsVO;
	}

	public static PatientAssociationDetailsV toPatientAssociationDetailsV(PatientPharmacistAssociationsV detailsV) {
		PatientAssociationDetailsV associationDetailsV = new PatientAssociationDetailsV();
		associationDetailsV.setFirstName(detailsV.getFirstName());
		associationDetailsV.setLastName(detailsV.getLastName());
		associationDetailsV.setAge(detailsV.getAge());
		associationDetailsV.setCity(detailsV.getCity());
		associationDetailsV.setCountry(detailsV.getCountry());
		associationDetailsV.setEmailId(detailsV.getEmailId());
		associationDetailsV.setMobileNumber(detailsV.getMobileNumber());
		associationDetailsV.setPharmacistId(detailsV.getPharmacistId());
		associationDetailsV.setPatientId(detailsV.getPatientId());
		return associationDetailsV;
	}

	public static PatientProviderAssociationsV generatePatientProviderAssociationsV() {
		PatientProviderAssociationsV associationsV = new PatientProviderAssociationsV();
		associationsV.setAge(TestUtil.generateAge());
		associationsV.setFirstName(RandomStringUtils.randomAlphabetic(15));
		associationsV.setLastName(RandomStringUtils.randomAlphabetic(10));
		associationsV.setEmailFlag(false);
		associationsV.setEmailId(RandomStringUtils.randomAlphanumeric(10) + "@gmail.com");
		associationsV.setMobileNumber(TestUtil.generateMobileNumber());
		associationsV.setSmsFlag(false);
		associationsV.setCity(RandomStringUtils.randomAlphabetic(10));
		associationsV.setLocation(RandomStringUtils.randomAlphabetic(15));
		associationsV.setState(RandomStringUtils.randomAlphabetic(12));
		associationsV.setCountry("India");
		associationsV.setZipCode(RandomStringUtils.randomNumeric(6));
		associationsV.setGender("Male");
		associationsV.setUserId(TestUtil.generateId());
		associationsV.setUserCreatedOn(LocalDateTime.now()
		                                            .plusDays(-TestUtil.generateAge()));
		associationsV.setLastActiveTime(LocalDateTime.now()
		                                             .plusDays(-TestUtil.generateAge()));
		associationsV.setPatientId(TestUtil.generateId());
		return associationsV;
	}

	public static PatientAssociationDetailsV toPatientAssociationDetailsV(PatientProviderAssociationsV detailsV) {
		PatientAssociationDetailsV associationDetailsV = new PatientAssociationDetailsV();
		associationDetailsV.setFirstName(detailsV.getFirstName());
		associationDetailsV.setLastName(detailsV.getLastName());
		associationDetailsV.setAge(detailsV.getAge());
		associationDetailsV.setCity(detailsV.getCity());
		associationDetailsV.setCountry(detailsV.getCountry());
		associationDetailsV.setEmailId(detailsV.getEmailId());
		associationDetailsV.setMobileNumber(detailsV.getMobileNumber());
		associationDetailsV.setProviderId(detailsV.getProviderId());
		associationDetailsV.setPatientId(detailsV.getPatientId());
		return associationDetailsV;
	}

	public static PrescriptionInfoV generatePrescriptionInfoV() {
		PrescriptionInfoV infoV = new PrescriptionInfoV();
		infoV.setCaregiverId(TestUtil.generateId());
		infoV.setCaregiverUserId(TestUtil.generateId());
		;
		infoV.setCurrentAdherence(RandomStringUtils.randomNumeric(2));
		infoV.setDiagnosis(RandomStringUtils.randomAlphabetic(10));
		infoV.setDisease(RandomStringUtils.randomAlphabetic(10));
		infoV.setDiagnosisId(TestUtil.generateId());
		infoV.setDiseaseDeleteFlag(false);
		infoV.setDiseaseId(TestUtil.generateId());
		infoV.setDiseaseDescription(RandomStringUtils.randomAlphabetic(100));
		infoV.setDiseaseInfoId(TestUtil.generateId());
		infoV.setEmoji(RandomStringUtils.randomAlphabetic(10));
		infoV.setId(UUID.randomUUID()
		                .toString());
		infoV.setPatientId(TestUtil.generateId());
		infoV.setPatientStatus(1);
		infoV.setPatientUserId(TestUtil.generateId());
		infoV.setPharmacistId(TestUtil.generateId());
		infoV.setPharmacistUserId(TestUtil.generateId());
		infoV.setPresCreatedDate(LocalDateTime.now()
		                                      .plusDays(-TestUtil.generateAge()));
		infoV.setPrescriptionId(TestUtil.generateId());
		infoV.setProviderComment(RandomStringUtils.randomAlphabetic(50));
		infoV.setPresUpdatedDate(LocalDateTime.now());
		return infoV;
	}

	public static String generateCaregiverModelAsString(String gender, String race) {
		try {
			Caregiver caregiver = generateCaregiver("Male", "Asian", "Caregiver", false);
			CaregiverModel caregiverModel = new Utility().convertCaregiverEntityToModel(caregiver);
			return OBJECT_MAPPER.writeValueAsString(caregiverModel);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static CaregiverModel generateCaregiverModel(String gender, String race) {
		try {
			Caregiver caregiver = generateCaregiver(gender, race, "Caregiver", false);
			CaregiverModel caregiverModel = new Utility().convertCaregiverEntityToModel(caregiver);
			return caregiverModel;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static UserDetailsModel generateUserDetailsModel(String gender, String race, String roleName) {
		RoleModel role = new RoleModel();
		role.setName(roleName);
		role.setId(1);
		UserDetailsModel userDetails = new UserDetailsModel();
		AddressModel addressModel = new AddressModel();
		addressModel.setLocation("hyderabad");
		addressModel.setZipCode("HYD080");
		userDetails.setAddress(addressModel);
		userDetails.setId(TestUtil.generateId());
		userDetails.setRole(role);
		userDetails.setFirstName(RandomStringUtils.randomAlphabetic(20));
		userDetails.setMiddleName(RandomStringUtils.randomAlphabetic(20));
		userDetails.setLastName(RandomStringUtils.randomAlphabetic(20));
		userDetails.setPassword(RandomStringUtils.randomAlphanumeric(15));
		userDetails.setAge(TestUtil.generateAge()
		                           .toString());
		userDetails.setDateOfBirth(LocalDate.of(1981, 1, 10));
		// userDetails.setPwdChangeToken(RandomStringUtils.randomAlphanumeric(50));
		userDetails.setEmailId(RandomStringUtils.randomAlphanumeric(15) + "@dayamed.com");
		userDetails.setCountryCode("+91");
		userDetails.setGender(gender);
		userDetails.setRace(race);
		userDetails.setImanticUserid(TestUtil.generateImanticUserid());
		userDetails.setMobileNumber(TestUtil.generateMobileNumber());
		userDetails.setType(ApplicationConstants.ANDROID);
		return userDetails;
	}

	public static PatientModel generatePatientModel() {
		PatientModel patientModel = new PatientModel();
		patientModel.setId(TestUtil.generateId());
		patientModel.setUserDetails(generateUserDetailsModel("Male", "Asian", "Patient"));
		return patientModel;
	}

	public static PharmacistModel generatePharmacistModel() {
		PharmacistModel pharmacistModel = new PharmacistModel();
		pharmacistModel.setId(TestUtil.generateId());
		pharmacistModel.setUserDetails(generateUserDetailsModel("Male", "Asian", UserRoleEnum.PHARMACIST.getRole()));
		return pharmacistModel;
	}

	public static ProviderModel generateProviderModel() {
		ProviderModel providerModel = new ProviderModel();
		providerModel.setId(TestUtil.generateId());
		providerModel.setUserDetails(generateUserDetailsModel("Male", "Asian", UserRoleEnum.PROVIDER.getRole()));
		return providerModel;
	}

	public static UserPreferenceModel generateUserPreferenceModel() {
		UserPreferenceModel userPreferenceModel = new UserPreferenceModel();
		//userPreferenceModel.setId(TestUtil.generateId());
		userPreferenceModel.setEmailFlag(true);
		userPreferenceModel.setFirstConsumption("EAR");
		userPreferenceModel.setLanguagePreference("en");
		userPreferenceModel.setSmsFlag(true);
		userPreferenceModel.setPushNotificationFlag(true);
		return userPreferenceModel;
	}

	public static UserPreference generateUserPreference() {
		UserPreference userPreference = new UserPreference();
		//userPreferenceModel.setId(TestUtil.generateId());
		userPreference.setId(TestUtil.generateId());
		userPreference.setUserDetails(generateUserDetails("Male", "Asian", UserRoleEnum.PATIENT.getRole()));
		userPreference.setUserPrefSetting(generatePreferenceModelAsString());
		return userPreference;
	}

	public static String toJSONString(Object model) throws JsonProcessingException {
		return OBJECT_MAPPER.writeValueAsString(model);
	}

	public static String generatePreferenceModelAsString() {
		try {
			return OBJECT_MAPPER.writeValueAsString(generateUserPreferenceModel());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return StringUtils.EMPTY;
	}

	public static String generatePatientModelAsString() {
		try {
			PatientModel patientModel = generatePatientModel();
			return OBJECT_MAPPER.writeValueAsString(patientModel);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static FristConsumptionTypeModel generateFristConsumptionTypeModel() {
		FristConsumptionTypeModel model = new FristConsumptionTypeModel();
		model.setFrequencyPerDay(generateFrequencyPerDayModel("Twice"));
		model.setFirstConsumptionTime(generateFirstConsumptionTimeModel("Early", "6:00 AM, 3:00 PM"));
		return model;
	}

	public static FrequencyPerDayModel generateFrequencyPerDayModel(String name) {
		FrequencyPerDayModel frequency = new FrequencyPerDayModel();
		frequency.setId(1);
		frequency.setName(name);
		return frequency;
	}

	public static FirstConsumptionTimeModel generateFirstConsumptionTimeModel(String name, String times) {
		FirstConsumptionTimeModel model = new FirstConsumptionTimeModel();
		model.setId(1);
		model.setName(name);
		model.setTimes(times);
		return model;
	}

	public static LoginRequestModel getLoginRequestModel() {
		LoginRequestModel loginRequestModel = new LoginRequestModel();
		loginRequestModel.setUserName(RandomStringUtils.randomAlphanumeric(15) + "@dayamed.com");
		loginRequestModel.setPassword(RandomStringUtils.randomAlphanumeric(15));
		loginRequestModel.setAppVersion(RandomStringUtils.randomAlphanumeric(5));
		loginRequestModel.setDeviceId(RandomStringUtils.randomAlphanumeric(5));
		loginRequestModel.setOsVersion(RandomStringUtils.randomAlphanumeric(5));
		return loginRequestModel;
	}

	public static AdherenceReportModel generateAdherenceReportModel() {
		AdherenceReportModel reportModel = new AdherenceReportModel();
		reportModel.setDosageOfDay(DosageTimeOfTheDay.getTimeOfTheDay(1)
		                                             .getValue());
		reportModel.setDosageTimeOfTheDay("10:00 AM");
		reportModel.setZone("Central");
		reportModel.setEventTime("2019-06-12 10:00");
		reportModel.setObservedTime("2019-06-12 10:10");
		reportModel.setAdherenceAction("Consumed");
		reportModel.setAdherenceActionType("How");
		reportModel.setMedicationName("M1");
		reportModel.setMedicationQuantity("1");
		reportModel.setPatientEmailAddress("test@gmail.com");
		reportModel.setPatientId(TestUtil.generateId());
		reportModel.setPatientName(RandomStringUtils.randomAlphabetic(10));
		reportModel.setPrescriptionId(TestUtil.generateId());
		reportModel.setProviderName(RandomStringUtils.randomAlphabetic(10));
		return reportModel;
	}

	public static ZoomEndMeetingReqModel generateZoomEndMeetingReqModel() {
		ZoomEndMeetingObjectReqModel zoomEndMeetingObjectReqModel = new ZoomEndMeetingObjectReqModel();
		zoomEndMeetingObjectReqModel.setDuration(33l);
		zoomEndMeetingObjectReqModel.setHost_id(RandomStringUtils.randomAlphabetic(10));
		zoomEndMeetingObjectReqModel.setId(RandomStringUtils.randomNumeric(8));
		ZoomEndMeetingPayloadReqModel zoomEndMeetingPayloadReqModel = new ZoomEndMeetingPayloadReqModel();
		zoomEndMeetingPayloadReqModel.setObject(zoomEndMeetingObjectReqModel);
		ZoomEndMeetingReqModel zoomEndMeetingReqModel = new ZoomEndMeetingReqModel();
		zoomEndMeetingReqModel.setPayload(zoomEndMeetingPayloadReqModel);
		zoomEndMeetingReqModel.setEvent(RandomStringUtils.randomAlphabetic(5));
		return zoomEndMeetingReqModel;
	}

	public static GSVUCAToken genrateGSVUCAToken() {
		GSVUCAToken gSVUCAToken = new GSVUCAToken();
		gSVUCAToken.setId(5l);
		gSVUCAToken.setNdcCode(RandomStringUtils.randomAlphabetic(5));
		gSVUCAToken.setToken(RandomStringUtils.randomAlphabetic(8));
		return gSVUCAToken;
	}

	public static ServletOutputStream genrateServletOutputStream() {
		return new ServletOutputStream() {
			@Override
			public void write(int b) throws IOException {
			}

			@Override
			public void setWriteListener(WriteListener listener) {
			}

			@Override
			public boolean isReady() {
				return false;
			}
		};
	}

	public static ImanticNotification genrateImanticNotification() {
		ImanticNotification imanticNotification = new ImanticNotification();
		return imanticNotification;
	}

	public static ImanticNotificationReqModel genrateImanticNotificationReqModel() {
		ImanticNotificationReqModel imanticNotificationReqModel = new ImanticNotificationReqModel();
		imanticNotificationReqModel.setAlertType(Arrays.asList("email", "sms"));
		imanticNotificationReqModel.setToList(Arrays.asList(ApplicationConstants.PROVIDER,
		                                                    ApplicationConstants.PATIENT,
		                                                    ApplicationConstants.PHARMACIST,
		                                                    ApplicationConstants.CAREGIVER));
		imanticNotificationReqModel.setMessage(RandomStringUtils.randomAlphabetic(15));
		imanticNotificationReqModel.setImanticId(RandomStringUtils.randomAlphabetic(10));
		return imanticNotificationReqModel;
	}

	public static TwilioVoiceModel generateTwilioVoiceModel() {
		TwilioVoiceModel twilioVoiceModel = new TwilioVoiceModel();
		twilioVoiceModel.setIdentity("NGF45Rt");
		return twilioVoiceModel;
	}

	public static ConsumerGoods generateConsumerGoods(long id, String name) {
		ConsumerGoods consumerGoods = new ConsumerGoods();
		consumerGoods.setType("electronic");
		consumerGoods.setId(id);
		consumerGoods.setPurpose(RandomStringUtils.randomAlphabetic(100));
		consumerGoods.setName(name);
		consumerGoods.setImageURL(RandomStringUtils.randomAlphabetic(10));
		consumerGoods.setDescription(RandomStringUtils.randomAlphabetic(20));
		return consumerGoods;
	}

	public static ConsumerGoodsModel generateConsumerGoodsModel(long id, String name) {
		ConsumerGoodsModel consumerGoodsModel = new ConsumerGoodsModel();
		BeanUtils.copyProperties(generateConsumerGoods(id, name), consumerGoodsModel);
		return consumerGoodsModel;

	}

	public static Address generateAddress() {
		Address address = new Address();
		address.setCity(RandomStringUtils.randomAlphabetic(10));
		address.setCountry(RandomStringUtils.randomAlphabetic(10));
		address.setLocation(RandomStringUtils.randomAlphabetic(10));
		address.setZipCode(RandomStringUtils.randomNumeric(6));
		address.setState(RandomStringUtils.randomAlphabetic(10));
		return address;
	}

	public static JwtUser generateJwtUser(UserRoleEnum roleEnum) {
		JwtUser jwtUser = new JwtUser();
		jwtUser.setRole(roleEnum.getRole());
		jwtUser.setGsPharmacistEnable(true);
		jwtUser.setUserId(TestUtil.generateId());
		jwtUser.setUserName(RandomStringUtils.randomAlphabetic(10) + "@gmail.com");
		jwtUser.setUserEmailId(RandomStringUtils.randomAlphabetic(10) + "@gmail.com");
		return jwtUser;
	}

	public static ConsumptionTemplate generateConsumptionTemplate() {
		ConsumptionTemplate template = new ConsumptionTemplate();
		template.setConsumptionsTimes(RandomStringUtils.randomAlphabetic(10));
		template.setFrequencyName(RandomStringUtils.randomAlphabetic(10));
		template.setConsumptionsId(TestUtil.generateId());
		template.setConsumptionsKey(RandomStringUtils.randomAlphabetic(10));
		template.setConsumptionsDesc(RandomStringUtils.randomAlphabetic(10));
		template.setId(TestUtil.generateId());
		template.setFrequencyId(TestUtil.generateId());
		return template;
	}

	public static DosageNotification generateDosageNotification() {
		DosageNotification notification = new DosageNotification();
		notification.setDosageInfo(new DosageInfo());
		notification.setUserId(TestUtil.generateId());
		notification.setCountryCode("91");
		notification.setExpectedTime("10");
		notification.setNotificationType("Email");
		notification.setNotificationValue(RandomStringUtils.randomAlphabetic(10));
		notification.setId(TestUtil.generateId());
		return notification;
	}

	public static UserDetailsCacheVO generateUserDetailsCacheVO() {
		UserDetailsCacheVO cacheVO = new UserDetailsCacheVO();
		cacheVO.setId(TestUtil.generateId());
		cacheVO.setRole(UserRoleEnum.PROVIDER.getRole());
		cacheVO.setRoleId(UserRoleEnum.PROVIDER.getId());
		return cacheVO;
	}

	public static APNSUpdateTokenRequestModel generateAPNSUpdateTokenRequestModel() {
		APNSUpdateTokenRequestModel aPNSUpdateTokenRequestModel = new APNSUpdateTokenRequestModel();
		aPNSUpdateTokenRequestModel.setEmailId(RandomStringUtils.randomAlphabetic(10)+"@test.com");
		aPNSUpdateTokenRequestModel.setToken(RandomStringUtils.randomAlphabetic(10));
		aPNSUpdateTokenRequestModel.setTypeOfDevice(ApplicationConstants.IOS);
		aPNSUpdateTokenRequestModel.setVoIpToken(RandomStringUtils.randomAlphabetic(10));
		return aPNSUpdateTokenRequestModel;
	}

	public static FcmNotificationRequestModel generateFcmNotificationRequestModel() {
		FcmNotificationRequestModel fcmNotificationRequestModel = new FcmNotificationRequestModel();
		fcmNotificationRequestModel.setBelongingId(1l);
		fcmNotificationRequestModel.setEmailIds(Stream.of(RandomStringUtils.randomAlphabetic(10) + "@test.com")
		                                              .collect(
				                                              Collectors.toList()));
		fcmNotificationRequestModel.setMessage(RandomStringUtils.randomAlphabetic(10));
		fcmNotificationRequestModel.setTitle(RandomStringUtils.randomAlphabetic(10));
		return fcmNotificationRequestModel;
	}

	public static VideoNotificationRequestModel generateVideoNotificationRequestModel() {
		VideoNotificationRequestModel videoNotificationRequestModel = new VideoNotificationRequestModel();
		videoNotificationRequestModel.setBelongingId(1l);
		videoNotificationRequestModel.setRoomName(RandomStringUtils.randomAlphabetic(10));
		videoNotificationRequestModel.setUserId(1l);
		return videoNotificationRequestModel;
	}

	public static MultiplePrescriptionSchedulingRequestModel generateMultiplePrescriptionSchedulingRequestModel() {
		MultiplePrescriptionSchedulingRequestModel multiplePrescriptionSchedulingRequestModel =
				new MultiplePrescriptionSchedulingRequestModel();
		multiplePrescriptionSchedulingRequestModel.setPrescriptionIds(Stream.of(1l)
		                                                                    .collect(
				                                                                    Collectors.toList()));
		multiplePrescriptionSchedulingRequestModel.setZoneId("Asian/kolkat");
		return multiplePrescriptionSchedulingRequestModel;
	}

	public static FirstConsumptionResponseModel generateFirstConsumptionResponseModel() {
		FirstConsumptionResponseModel firstConsumptionResponseModel =
				new FirstConsumptionResponseModel();
		ConsumptionResponseModel consumptionResponseModel = new ConsumptionResponseModel();
		consumptionResponseModel.setId(1l);
		consumptionResponseModel.setName(RandomStringUtils.randomAlphabetic(10));
		consumptionResponseModel.setDescription(RandomStringUtils.randomAlphabetic(10));
		firstConsumptionResponseModel.setConsumptions(Stream.of(consumptionResponseModel)
		                                                    .collect(
				                                                    Collectors.toList()));
		return firstConsumptionResponseModel;
	}

	public static NotificationRequestModel generateNotificationRequestModel(){
		NotificationRequestModel notificationRequestModel = new NotificationRequestModel();
		notificationRequestModel.setBelongingUserId(1l);
		notificationRequestModel.setMessage(RandomStringUtils.randomAlphabetic(15));
		notificationRequestModel.setType(RandomStringUtils.randomAlphabetic(10));
		notificationRequestModel.setUserIds(Stream.of(1l).collect(Collectors.toList()));
		return notificationRequestModel;
	}

	public static DosageNotificationModel generateDosageNotificationModel() {
		DosageNotificationModel notification = new DosageNotificationModel();
		notification.setId(1L);
		notification.setUserId(TestUtil.generateId());
		notification.setCountryCode("91");
		notification.setExpectedTime("10");
		notification.setNotificationType("Email");
		notification.setNotificationValue(RandomStringUtils.randomAlphabetic(10));
		notification.setId(TestUtil.generateId());
		return notification;
	}


	public static CaregiverDosageNotificationModel generateCaregiverDosageNotificationModel(){
		CaregiverDosageNotificationModel caregiverDosageNotificationModel = new CaregiverDosageNotificationModel();
		caregiverDosageNotificationModel.setDosageInfoID(1L);
		caregiverDosageNotificationModel.setOwneruserid(1L);
		caregiverDosageNotificationModel.setPatientId(1L);
		caregiverDosageNotificationModel.setPrescriptionId(1L);
		caregiverDosageNotificationModel.setDosageNotificationList(
				Stream.of(generateDosageNotificationModel()).collect(Collectors.toList()));
		return caregiverDosageNotificationModel;
	}
}
