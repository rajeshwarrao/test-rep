package com.sg.dayamed.util;

import com.sg.dayamed.base.BaseTestCase;

import org.apache.velocity.app.Velocity;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Velocity.class})
public class TestMessageNotificationConstants extends BaseTestCase {

	@InjectMocks
	MessageNotificationConstants messageNotificationConstants;

	@Mock
	MessageSource messageSource;

	@Mock
	Utility mockUtility;

	JSONObject jsonObj = new JSONObject();

	@Before
	public void init() throws Exception {
		PowerMockito.mockStatic(Velocity.class);
		ReflectionTestUtils.setField(messageNotificationConstants, "locales", SimpleDateFormat.getAvailableLocales());
		jsonObj.put("prescribedtime", "testPTime");
		jsonObj.put("observedtime", "testOTime");
		jsonObj.put("testPropKey", "jsonPropVal");
	}

	@Test
	public void test_init() throws Exception {
		messageNotificationConstants.init();
	}

	@Test
	public void test_getUserPreferedLangTranslatedMsg() throws Exception {
		Map<String, String> keyVal = new HashMap<String, String>();
		String checkLocale =
				messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal, "patient.not.found", "en", null);
		assertEquals(checkLocale, "");
	}

	@Test
	public void test_getUserPreferedLangTranslatedMsgNull() throws Exception {
		Map<String, String> keyVal = new HashMap<String, String>();
		String checkLocale =
				messageNotificationConstants.getUserPreferedLangTranslatedMsg(keyVal, "patient.not.found", "dummy",
				                                                              null);
		assertNull(checkLocale);
	}

	@Test
	public void test_prefixAndToList() throws Exception {
		List<String> medicineList = new ArrayList<String>();
		medicineList.add("m1");
		medicineList.add("m2");
		String checkAppended = messageNotificationConstants.prefixAndToList(medicineList, "en");
		assertEquals(checkAppended, "m1, m2");
	}

	@Test
	public void test_empty_prefixAndToList() throws Exception {
		List<String> medicineList = new ArrayList<String>();
		String checkIsEmpty = messageNotificationConstants.prefixAndToList(medicineList, "en");
		assertEquals(checkIsEmpty, "");
	}

	@Test
	public void test_buildAdherenceMsgForMessageResource() throws Exception {

		Map<String, String> aderencehMap = messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonObj,
		                                                                                                    "mapPropKey",
		                                                                                                    "testPropKey");

		Map<String, String> expectedMap = new HashMap<>();
		expectedMap.put("prescribedTime", "testPTime");
		expectedMap.put("observedTime", "testOTime");
		expectedMap.put("mapPropKey", "jsonPropVal");
		assertEquals(aderencehMap, expectedMap);

	}

	@Test
	public void test_buildAdherenceMsgForMessageResourceOverLoaded() throws Exception {

		List<String> medicineNameList = new ArrayList<String>();
		medicineNameList.add("m1");
		medicineNameList.add("m2");
		Map<String, String> aderencehMap =
				messageNotificationConstants.buildAdherenceMsgForMessageResource(jsonObj, "mapPropKey", "testPropKey",
				                                                                 medicineNameList, "en");
		Map<String, String> expectedMap = new HashMap<>();
		expectedMap.put("prescribedTime", "testPTime");
		expectedMap.put("observedTime", "testOTime");
		expectedMap.put("mapPropKey", "jsonPropVal");
		expectedMap.put("appendMedicineListWithAnd", "m1, m2");
		assertEquals(aderencehMap, expectedMap);

	}
}
