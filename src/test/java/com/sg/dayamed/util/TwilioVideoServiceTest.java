package com.sg.dayamed.util;

import com.twilio.jwt.accesstoken.AccessToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.test.util.ReflectionTestUtils;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
@PrepareForTest(AccessToken.class)
public class TwilioVideoServiceTest {

	private static final String KEY_SID = randomAlphabetic(10);

	private static final String ACCOUNT_SID = randomAlphabetic(10);

	private static final String SECRET = randomAlphabetic(10);

	@InjectMocks
	private TwilioVideoService twilioVideoService;

	@Before
	public void setUp() throws Exception {
		ReflectionTestUtils.setField(twilioVideoService, "accountSid", ACCOUNT_SID);
		ReflectionTestUtils.setField(twilioVideoService, "keySid", KEY_SID);
		ReflectionTestUtils.setField(twilioVideoService, "secret", SECRET);
	}

	@Test
	public void testGenerateToken() throws Exception {
		final String clientIdentity = randomAlphabetic(10);
		final TwilioVideoModel twilioVideoModel = twilioVideoService.generateAuthToken(clientIdentity);
		assertEquals(clientIdentity, twilioVideoModel.getIdentity());
		assertNotNull(twilioVideoModel.getToken());
	}
}
