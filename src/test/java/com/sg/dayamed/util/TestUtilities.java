package com.sg.dayamed.util;

import com.sg.dayamed.entity.Address;
import com.sg.dayamed.errorhandling.ValidationError;
import com.sg.dayamed.errorhandling.ValidationFieldError;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.AdherenceReportModel;
import com.sg.dayamed.rest.commons.IAPIResponseCode;
import com.sg.dayamed.rest.commons.IResponseCodes;
import com.sg.dayamed.rest.commons.RestRequest;
import com.sg.dayamed.rest.commons.RestResponse;
import com.sg.dayamed.rest.commons.ServiceMethod;
import com.sg.dayamed.rest.controller.ws.users.WSPharmacist;
import com.sg.dayamed.rest.controller.ws.users.WSRole;
import com.sg.dayamed.rest.controller.ws.users.WSUserDetails;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.PharmacistVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 21/Jun/2019
 **/

public class TestUtilities {

	@Test
	public void testIResponseCodes() {
		assertNotNull(IResponseCodes.DATA_VALIDATION_ERROR);
		assertNotNull(IResponseCodes.ACCESS_DENIED_EXCEPTION);
		assertNotNull(IResponseCodes.AUTHENTICATION_FAILED);
		assertNotNull(IResponseCodes.AUTHORIZATION_FAILED);
		assertNotNull(IResponseCodes.SERVICE_UNAVAILABLE);
		assertNotNull(IResponseCodes.SUCCESSFUL);
		assertNotNull(IResponseCodes.UNEXPECTED_SYSTEM_ERROR);
	}

	@Test
	public void testPOJOObjects() {
		WSRole role = new WSRole();
		assertNotNull(role);
		role.setId(NumberUtils.LONG_ONE);
		role.setName(UserRoleEnum.PATIENT.getRole());
		assertNotNull(role.getId());
		assertNotNull(role.getName());
		assertNotNull(new ServiceMethod());
		assertNotNull(new RestRequest());
		RestResponse restResponse = new RestResponse();
		restResponse.setResult(IResponseCodes.SUCCESSFUL);
		assertEquals(IResponseCodes.SUCCESSFUL, restResponse.getResult());
	}

	@Test
	public void testPharmacistVO() {
		PharmacistVO pharmacistVO = new PharmacistVO();
		pharmacistVO.setCompany(RandomStringUtils.random(10));
		pharmacistVO.setId(TestUtil.generateId());
		pharmacistVO.setUserDetails(new UserDetailsVO());

		assertNotNull(pharmacistVO);
		assertNotNull(pharmacistVO.getCompany());
		assertNotNull(pharmacistVO.getId());
		assertNotNull(pharmacistVO.getUserDetails());

		pharmacistVO.toWsPharmacist();
		new PharmacistVO(new UserPreferenceVO());
	}

	@Test
	public void testPatientDetailsVO() {
		PatientDetailsVO detailsVO = new PatientDetailsVO();
		detailsVO.setPatientStatus(1);
		detailsVO.setEmoji(RandomStringUtils.randomAlphanumeric(10));
		detailsVO.setId(TestUtil.generateId());
		detailsVO.setCurrentAdherence(RandomStringUtils.randomAlphanumeric(10));
		detailsVO.setPatientDeviceId(RandomStringUtils.randomAlphanumeric(10));
		detailsVO.setPredictedAdherence(RandomStringUtils.randomAlphanumeric(10));
		detailsVO.setProjectedAdherence(RandomStringUtils.randomAlphanumeric(10));
		detailsVO.setUserDetails(new UserDetailsVO());

		assertNotNull(detailsVO);
		assertNotNull(detailsVO.getCurrentAdherence());
		assertNotNull(detailsVO.getEmoji());
		assertNotNull(detailsVO.getId());
		assertNotNull(detailsVO.getPatientDeviceId());
		assertNotNull(detailsVO.getPatientStatus());
		assertNotNull(detailsVO.getPredictedAdherence());
		assertNotNull(detailsVO.getProjectedAdherence());
		assertNotNull(detailsVO.getUserDetails());
	}

	@Test
	public void testWSPharmacist() {
		WSPharmacist pharmacist = new WSPharmacist();
		pharmacist.setUserDetails(new WSUserDetails());
		pharmacist.setId(1L);
		pharmacist.setCompany(RandomStringUtils.randomAlphanumeric(10));

		assertNotNull(pharmacist);
		assertNotNull(pharmacist.getCompany());
		assertNotNull(pharmacist.getId());
		assertNotNull(pharmacist.getUserDetails());
	}

	@Test
	public void testIAPIResponseCode() {
		assertNotNull(IAPIResponseCode.AUTHENTICATION_FAILED);
		assertNotNull(IAPIResponseCode.FAILED);
		assertNotNull(IAPIResponseCode.PARAMETER_VALIDATION_FAILED);
		assertNotNull(IAPIResponseCode.SUCCESS);
		assertNotNull(IAPIResponseCode.UNEXPECTED_SYSTEM_ERROR);
	}

	@Test
	public void testDataValidationException() {
		DataValidationException exception = new DataValidationException("error", "error");
		exception.toString();
	}

	@Test
	public void testAddress() {
		Address address = TestResponseGenerator.generateAddress();
		assertNotNull(address);
		assertNotNull(address.getCity());
		assertNotNull(address.getLocation());
		assertNotNull(address.getState());
		assertNotNull(address.getZipCode());
		assertNotNull(address.getCountry());
	}

}
