package com.sg.dayamed.util;

import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.MessageModel;
import com.sg.dayamed.helper.pojo.ResultVO;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.rabbitmq.RabbitMQSender;
import com.sg.dayamed.service.UserDetailsService;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class EmailAndSmsUtilityTest {

	@InjectMocks
	private EmailAndSmsUtility emailAndSmsUtility;

	@Mock
	private RabbitMQSender mockRabbitMQSender;

	@Mock
	private UserDetailsService mockUserDetailsService;

	@Mock
	private MessageNotificationConstants mockMessageNotificationConstants;

	@Test
	public void testSendEmailWithoutFlag() {
		/**
		 * Case 1 : No Exception
		 */
		List<String> emails = Stream.of(RandomStringUtils.randomAlphabetic(10) + "@gmail.com")
		                            .collect(Collectors.toList());
		Mockito.when(mockUserDetailsService.findByEmailIdIn(Mockito.eq(emails)))
		       .thenReturn(emails);
		Boolean result = emailAndSmsUtility.sendEmail(RandomStringUtils.randomAlphabetic(10),
		                                              RandomStringUtils.randomAlphabetic(10), emails);
		assertNotNull(result);
		assertTrue(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(1))
		       .sendEmail(Mockito.any(Email.class));

		/**
		 * Case 2 : With Error
		 */
		Mockito.reset(mockUserDetailsService, mockRabbitMQSender);
		Mockito.when(mockUserDetailsService.findByEmailIdIn(Mockito.eq(emails)))
		       .thenThrow(new RuntimeException());
		result = emailAndSmsUtility.sendEmail(RandomStringUtils.randomAlphabetic(10),
		                                      RandomStringUtils.randomAlphabetic(10), emails);
		assertNotNull(result);
		assertFalse(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(0))
		       .sendEmail(Mockito.any(Email.class));
	}

	@Test
	public void testSendEmailWithDefaultFlag() {
		/**
		 * Case 1 : No Exception
		 */
		List<String> emails = Stream.of(RandomStringUtils.randomAlphabetic(10) + "@gmail.com")
		                            .collect(Collectors.toList());
		Boolean result = emailAndSmsUtility.sendEmail(RandomStringUtils.randomAlphabetic(10),
		                                              RandomStringUtils.randomAlphabetic(10), emails, true);
		assertNotNull(result);
		assertTrue(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(1))
		       .sendEmail(Mockito.any(Email.class));

		/**
		 * Case 2 : With Exception
		 */
		Mockito.reset(mockRabbitMQSender);
		Mockito.doThrow(new RuntimeException())
		       .when(mockRabbitMQSender)
		       .sendEmail(Mockito.any());
		result = emailAndSmsUtility.sendEmail(RandomStringUtils.randomAlphabetic(10),
		                                      RandomStringUtils.randomAlphabetic(10), emails, true);
		assertNotNull(result);
		assertFalse(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(1))
		       .sendEmail(Mockito.any(Email.class));
	}

	@Test
	public void sendSMSWithoutCountryCode() {
		/**
		 * Case 1 : With out exception
		 */
		Boolean result =
				emailAndSmsUtility.sendSms(RandomStringUtils.randomAlphabetic(30),
				                           RandomStringUtils.randomNumeric(10));
		assertNotNull(result);
		assertTrue(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(1))
		       .sendSms(Mockito.any(MessageModel.class));

		/**
		 * Case 2 : With Exception
		 */
		Mockito.reset(mockRabbitMQSender);
		Mockito.doThrow(new RuntimeException())
		       .when(mockRabbitMQSender)
		       .sendSms(Mockito.any());
		result =
				emailAndSmsUtility.sendSms(RandomStringUtils.randomAlphabetic(30),
				                           RandomStringUtils.randomNumeric(10));
		assertNotNull(result);
		assertFalse(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(1))
		       .sendSms(Mockito.any(MessageModel.class));
	}

	@Test
	public void sendSMSWithCountryCode() {
		/**
		 * Case 1 : With out exception
		 */
		Mockito.when(mockUserDetailsService.findByPhoneNumber(Mockito.anyString()))
		       .thenReturn(true);
		Boolean result = emailAndSmsUtility.sendSms(RandomStringUtils.randomAlphabetic(30), "91",
		                                            RandomStringUtils.randomNumeric(10));
		assertNotNull(result);
		assertTrue(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(1))
		       .sendSms(Mockito.any(MessageModel.class));

		/**
		 * Case 2 : With Exception
		 */
		Mockito.reset(mockRabbitMQSender, mockUserDetailsService);
		Mockito.when(mockUserDetailsService.findByPhoneNumber(Mockito.anyString()))
		       .thenThrow(new RuntimeException());
		result = emailAndSmsUtility.sendSms(RandomStringUtils.randomAlphabetic(30), "91",
		                                    RandomStringUtils.randomNumeric(10));
		assertNotNull(result);
		assertFalse(result);
		Mockito.verify(mockRabbitMQSender, Mockito.times(0))
		       .sendSms(Mockito.any(MessageModel.class));
	}

	@Test
	public void testSendEmailWithDynamic() {
		/**
		 * No Exception
		 */
		UserPreferenceModel preferenceModel = TestResponseGenerator.generateUserPreferenceModel();
		String emailId = RandomStringUtils.randomAlphabetic(10) + "@gmail.com";
		ResultVO resultVO = new ResultVO();
		resultVO.setEmailId(emailId);
		resultVO.setModel(preferenceModel);
		List<String> emailIds = Stream.of(emailId)
		                              .collect(Collectors.toList());
		List<ResultVO> resultVOS = Stream.of(resultVO)
		                                 .collect(Collectors.toList());
		Mockito.when(mockUserDetailsService.findByEmailIdInResultVo(Mockito.anyList()))
		       .thenReturn(resultVOS);
		Boolean result =
				emailAndSmsUtility.sendEmail(RandomStringUtils.randomAlphabetic(10), "en", new HashMap<>(), emailIds);
		assertNotNull(result);
		assertTrue(result);

		/**
		 * Exception occurred
		 */
		Mockito.reset(mockUserDetailsService);
		Mockito.when(mockUserDetailsService.findByEmailIdInResultVo(Mockito.anyList()))
		       .thenThrow(new RuntimeException());
		result = emailAndSmsUtility.sendEmail(RandomStringUtils.randomAlphabetic(10), "en", new HashMap<>(), emailIds);
		assertNotNull(result);
		assertFalse(result);
	}
}
