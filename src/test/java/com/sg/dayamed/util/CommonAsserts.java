package com.sg.dayamed.util;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.entity.view.UserPreferencesV;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 02/Jul/2019
 **/

public class CommonAsserts {

	public static void assertPatientDetailsVO(PatientAssociationDetailsV detailsV, PatientDetailsVO detailsVO) {
		assertEquals(detailsV.getPatientId(), detailsVO.getId());
		assertEquals(detailsV.getEmoji(), detailsVO.getEmoji());
		assertEquals(detailsV.getPatientStatus(), detailsVO.getPatientStatus());
		assertNotNull(detailsVO.getUserDetails());
		assertEquals(detailsV.getUserId(), detailsVO.getUserDetails()
		                                            .getId());
		assertEquals(detailsV.getEmailFlag(), detailsVO.getUserDetails()
		                                               .getEmailFlag());
		assertEquals(detailsV.getAge(), detailsVO.getUserDetails()
		                                         .getAge());
		assertEquals(detailsV.getFirstName(), detailsVO.getUserDetails()
		                                               .getFirstName());
		assertEquals(detailsV.getLastName(), detailsVO.getUserDetails()
		                                              .getLastName());
		assertEquals(detailsV.getMiddleName(), detailsVO.getUserDetails()
		                                                .getMiddleName());
		assertEquals(detailsV.getLastActiveTime(), detailsVO.getUserDetails()
		                                                    .getLastActiveTime());
		assertEquals(detailsV.getEmailId(), detailsVO.getUserDetails()
		                                             .getEmailId());
		assertEquals(detailsV.getMobileNumber(), detailsVO.getUserDetails()
		                                                  .getMobileNumber());

		assertEquals(detailsV.getSmsFlag(), detailsVO.getUserDetails()
		                                             .getSmsFlag());
		assertEquals(detailsV.getCity(), detailsVO.getUserDetails()
		                                          .getCity());
		assertEquals(detailsV.getLocation(), detailsVO.getUserDetails()
		                                              .getLocation());
		assertEquals(detailsV.getCountry(), detailsVO.getUserDetails()
		                                             .getCountry());
		assertEquals(detailsV.getState(), detailsVO.getUserDetails()
		                                           .getState());
		assertEquals(detailsV.getZipCode(), detailsVO.getUserDetails()
		                                             .getZipCode());
	}

	public static void assertUserPreferenceVO(UserPreferencesV userPreferencesV, UserPreferenceVO preferenceVO) {
		assertEquals(userPreferencesV.getPharmacistId(), preferenceVO.getPharmacistId());
		assertEquals(userPreferencesV.getRoleId(), preferenceVO.getRoleId());
		assertEquals(userPreferencesV.getRole(), preferenceVO.getRole());
		assertEquals(userPreferencesV.getCaregiverId(), preferenceVO.getCaregiverId());
		assertEquals(userPreferencesV.getProviderId(), preferenceVO.getProviderId());
		assertEquals(userPreferencesV.getPatientId(), preferenceVO.getPatientId());
		assertEquals(userPreferencesV.getUserId(), preferenceVO.getUserId());
		assertEquals(userPreferencesV.getUserPrefId(), preferenceVO.getUserPrefId());
		assertEquals(userPreferencesV.getUserPref(), preferenceVO.getUserPref());
		assertEquals(userPreferencesV.getAge(), preferenceVO.getAge());
		assertEquals(userPreferencesV.getFirstName(), preferenceVO.getFirstName());
		assertEquals(userPreferencesV.getImaticUserId(), preferenceVO.getImanticUserId());
		assertEquals(userPreferencesV.getLocation(), preferenceVO.getLocation());
		assertEquals(userPreferencesV.getAdminCaregiver(), preferenceVO.getAdminCaregiver());
		assertEquals(userPreferencesV.getAppVersion(), preferenceVO.getAppVersion());
		assertEquals(userPreferencesV.getCity(), preferenceVO.getCity());
		assertEquals(userPreferencesV.getCountry(), preferenceVO.getCountry());
		assertEquals(userPreferencesV.getCountryCode(), preferenceVO.getCountryCode());
		assertEquals(userPreferencesV.getMiddleName(), preferenceVO.getMiddleName());
		assertEquals(userPreferencesV.getLastName(), preferenceVO.getLastName());
		assertEquals(userPreferencesV.getEmailFlag(), preferenceVO.getEmailFlag());
		assertEquals(userPreferencesV.getEmailId(), preferenceVO.getEmailId());
		assertEquals(userPreferencesV.getMobileNumber(), preferenceVO.getMobileNumber());
		assertEquals(userPreferencesV.getZipCode(), preferenceVO.getZipCode());
		assertEquals(userPreferencesV.getLocation(), preferenceVO.getLocation());
		assertEquals(userPreferencesV.getLastActiveTime(), preferenceVO.getLastActiveTime());
		assertEquals(userPreferencesV.getLastLoginTime(), preferenceVO.getLastLoginTime());

	}
}