package com.sg.dayamed.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class DateUtilityTest {

	@InjectMocks
	DateUtility dateUtility;

	@Test
	public void testConvertLocalTimeToUTCTimeObjectByZoneId() {
		LocalDateTime resultDateTime =
				dateUtility.convertLocalTimeToUTCTimeObjectByzoneid("2019-06-01 05:30", "Asia/Kolkata");
		LocalDateTime expectedDateTime = LocalDateTime.of(2019, 6, 1, 0, 0);
		assertEquals(expectedDateTime, resultDateTime);
	}

	@Test
	public void testConvertLocalTimeToUTCWithZoneId() {
		LocalDateTime resultDateTime = dateUtility.convertLocalTimeToUTCWithZoneId("2019-06-01 05:30", "Asia/Kolkata");
		LocalDateTime expectedDateTime = LocalDateTime.of(2019, 6, 1, 5, 30);
		assertEquals(expectedDateTime, resultDateTime);
	}

	@Test
	public void testConvertLocalTimeToUTCWithZoneIdWithoutDateFormat() {
		LocalDateTime resultDateTime = dateUtility.convertLocalTimeToUTCWithZoneId("2019-06-01 05:30", "");
		assertNull(resultDateTime);
	}

	@Test
	public void testConvertLocalTimeToUTCWithZoneIdWithoutDate() {
		LocalDateTime resultDateTime = dateUtility.convertLocalTimeToUTCWithZoneId("", "Asia/Kolkata");
		assertNull(resultDateTime);
	}

	@Test
	public void testConvertLocalTimeToUTCTimeObjectByZoneIdWithoutZoneId() {
		LocalDateTime resultDateTime = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid("2019-06-01 05:30", "");
		assertNull(resultDateTime);
	}

	@Test
	public void testConvertLocalTimeToUTCTimeObjectByZonedIdWithoutDate() {
		LocalDateTime resultDateTime = dateUtility.convertLocalTimeToUTCTimeObjectByzoneid("", "Asia/Kolkata");
		assertNull(resultDateTime);
	}

	@Test
	public void testStaticContents() {
		Date fromLocalDate = DateUtility.asDate(LocalDate.now());
		assertNotNull(fromLocalDate);
	}

	@Test
	public void testConvertUTCLocalDateTimeStringToObject() {
		String dateTime = "2019-08-04 11:00";
		LocalDateTime localDateTime = dateUtility.convertUTCLocalDateTimeStringToObject(dateTime);
		assertNotNull(localDateTime);
		assertEquals(4, localDateTime.getDayOfMonth());
	}

	@Test
	public void testConvertUTCDateToLocalDateByZoneID() {

		/**
		 * Case 1 : No Exception.
		 */
		String dateTime = "2019-08-04 11:00";
		String datePattern = "yyyy-MM-dd";
		String result = dateUtility.convertUTCDateToLocalDateByZoneID(dateTime, ZoneId.systemDefault()
		                                                                              .getId(), datePattern);
		assertNotNull(result);
		assertEquals("2019-08-04", result);

		/**
		 * Case 2 : Exception
		 */
		dateTime = "2019/08/04 11:00";
		result = dateUtility.convertUTCDateToLocalDateByZoneID(dateTime, ZoneId.systemDefault()
		                                                                       .getId(), datePattern);
		assertTrue(StringUtils.isBlank(result));
	}

	@Test
	public void testConvertUTCLocalDateTimeObjectToZonedLocalDateTimeByZoneID() {
		/**
		 * Case 1 : No Exception.
		 */
		String dateTime = "2019-08-04 11:00";
		String datePattern = "yyyy-MM-dd";
		String result =
				dateUtility.convertUTCLocalDateTimeObjectToZonedLocalDateTimeByZoneID(dateTime, ZoneId.systemDefault()
				                                                                                      .getId(),
				                                                                      datePattern);
		assertNotNull(result);
		assertEquals("2019-08-04", result);

		/**
		 * Case 2 : Exception
		 */
		dateTime = "2019/08/04 11:00";
		result = dateUtility.convertUTCLocalDateTimeObjectToZonedLocalDateTimeByZoneID(dateTime, ZoneId.systemDefault()
		                                                                                               .getId(),
		                                                                               datePattern);
		assertTrue(StringUtils.isBlank(result));
	}

	@Test
	public void testGetDateTimeBasedOnZoneId() {
		/**
		 * Case 1 : With out Exception
		 */
		String result = dateUtility.getDateTimeBasedOnZoneId(ZoneId.systemDefault()
		                                                           .getId());
		assertNotNull(result);

		/**
		 * Case 2 : Exception
		 */
		result = dateUtility.getDateTimeBasedOnZoneId("Invalid");
		assertTrue(StringUtils.isBlank(result));
	}

	@Test
	public void testGetFormattedLocalDateTime() {
		assertNotNull(dateUtility.getFormattedLocalDateTime(LocalDateTime.now()));
	}

	@Test
	public void testAddDaysToLocalDate() {

		/**
		 * Case 1 : With out Exception
		 */
		LocalDate result = dateUtility.addDaysToLocalDate(LocalDate.now(), 10);
		assertNotNull(result);

		/**
		 * Case 2 : With Exception
		 */
		result = dateUtility.addDaysToLocalDate(null, 10);
		assertNull(result);
	}

	@Test
	public void testConvertDatetimeStringToLocalDateTime() {
		String dateTime = "2019-08-04 11:00";
		LocalDateTime localDateTime = dateUtility.convertdatetimeStringToLocalDateTime(dateTime);
		assertNotNull(localDateTime);
	}

	@Test
	public void testConvertDateObjectToLocalDateTimeObject() {
		assertNotNull(dateUtility.convertDateObjectToLocalDateTimeObject(new Date()));
	}

	@Test
	public void testAddTimeStringToLocalDate() {
		assertNotNull(dateUtility.addTimestringToLacalDate("10:00 am", LocalDate.now()));
		assertNotNull(dateUtility.addTimestringToLacalDate("10:00 pm", LocalDate.now()));
		assertNull(dateUtility.addTimestringToLacalDate("10:00 xx", LocalDate.now()));
	}

	@Test
	public void testConvertDateStringToDateObject() {
		assertNotNull(dateUtility.convertdateStringToDateObject("2019-08-04", "yyyy-MM-dd"));
	}

	@Test
	public void testConvertLocalDateTimeToDateObject() {
		assertNotNull(dateUtility.convertLocalDateTimeToDateObject(LocalDateTime.now()));
		assertNull(dateUtility.convertLocalDateTimeToDateObject(null));
	}
	@Test
	public void testConvertDate24hoursFormatTo12hourForamat() throws ParseException{
		assertNotNull(dateUtility.convertDate24hoursFormatTo12hourForamat("28.12.1987 - 11.30.27"));
		/*SimpleDateFormat _24HourSDF = new SimpleDateFormat("dd.MM.yyyy - HH.mm.ss");
		SimpleDateFormat _12HourSDF = new SimpleDateFormat("dd.MM.yyyy - hh.mm.ss aa");
		Date _24HourDt = _24HourSDF.parse(_24HourTime);
		return _12HourSDF.format(_24HourDt);*/
		
	}
}
