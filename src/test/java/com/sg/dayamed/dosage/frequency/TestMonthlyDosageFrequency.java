package com.sg.dayamed.dosage.frequency;

import org.apache.commons.collections4.CollectionUtils;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestMonthlyDosageFrequency {

	@InjectMocks
	private MonthlyDosageFrequency monthlyDosageFrequency;

	@Test(expected = NullPointerException.class)
	public void testFrequencyCalculationWithFrequencyAsNull() throws JSONException, ParseException {
		monthlyDosageFrequency.frequencyCalculation(null, Collections.emptyList());
	}

	@Test
	public void testFrequencyCalculationWithFrequency() throws Exception {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String currentDate = ZonedDateTime.now()
		                                  .format(dateTimeFormatter);
		String frequencyJson = "{\"endAfterOccurrences\" : 6, \"recurrence\" : 1, \"startDate\" : \"" + currentDate +
				"\", \"type\" : \"monthly\", " +
				"\"value\" : 10, \"patientTimeZone\" : \"Asia/Kolkata\"}";
		List<String> alarms = monthlyDosageFrequency.frequencyCalculation(frequencyJson, Stream.of("10:00 am")
		                                                                                       .collect(
				                                                                                       Collectors.toList()));
		assertTrue(CollectionUtils.isNotEmpty(alarms));
	}
}
