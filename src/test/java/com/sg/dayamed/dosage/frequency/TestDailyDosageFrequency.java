package com.sg.dayamed.dosage.frequency;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestDailyDosageFrequency {

	@InjectMocks
	private DailyDosageFrequency dailyDosageFrequency;

	@Test(expected = NullPointerException.class)
	public void testFrequencyCalculationWhenFrequencyIsNull() throws Exception {
		dailyDosageFrequency.frequencyCalculation(null, new ArrayList<>());
	}

	@Test
	public void testFrequencyCalculationWithFrequency() throws Exception {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String currentDate = ZonedDateTime.now()
		                                  .format(dateTimeFormatter);
		String frequencyJson =
				"{\"endAfterOccurrences\" : null, \"recurrence\" : null, \"startDate\" : \"" + currentDate +
						"\", \"type\" : \"daily\", " +
						"\"value\" : 120, \"patientTimeZone\" : \"Asia/Kolkata\"}";
		List<String> alarms = dailyDosageFrequency.frequencyCalculation(frequencyJson, Stream.of("10:00 am")
		                                                                                     .collect(
				                                                                                     Collectors.toList()));
		assertTrue(CollectionUtils.isNotEmpty(alarms));
	}
}
