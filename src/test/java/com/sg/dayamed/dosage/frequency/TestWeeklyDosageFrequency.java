package com.sg.dayamed.dosage.frequency;

import static org.junit.Assert.assertTrue;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(MockitoJUnitRunner.class)
public class TestWeeklyDosageFrequency {

	@InjectMocks
	private WeeklyDosageFrequency weeklyDosageFrequency;

	@Test(expected = NullPointerException.class)
	public void testFrequencyCalculationWithFrequencyJsonAsNull() throws Exception {
		weeklyDosageFrequency.frequencyCalculation(null, Collections.emptyList());
	}

	@Test
	public void testFrequencyCalculationWithFrequencyJson() throws Exception {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String currentDate = ZonedDateTime.now()
		                                  .format(dateTimeFormatter);
		String frequencyJson = "{\"endAfterOccurrences\" : 6, \"recurrence\" : 1, \"startDate\" : \"" + currentDate +
				"\", \"type\" : \"weekly\", " +
				"\"value\" : [\"MON\", \"TUE\", \"WED\", \"THU\", \"FRI\", \"SAT\", \"SUN\"], \"patientTimeZone\" : " +
				"\"Asia/Kolkata\"}";
		List<String> alarms = weeklyDosageFrequency.frequencyCalculation(frequencyJson, Stream.of("10:00 am")
		                                                                                      .collect(
				                                                                                      Collectors.toList()));
		assertTrue(CollectionUtils.isNotEmpty(alarms));
	}
}
