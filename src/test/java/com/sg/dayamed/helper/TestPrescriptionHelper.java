package com.sg.dayamed.helper;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Diagnosis;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.DiagnosisModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.DiagnosisService;
import com.sg.dayamed.service.NotificationService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PrescriptionService;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Naresh Kamisetti on 19/July/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPrescriptionHelper extends BaseTestCase {

	@InjectMocks
	private PrescriptionHelper mockPrescriptionHelper = new PrescriptionHelper();

	@Mock
	PatientRepository mockPatientRepository;

	@Mock
	ProviderRepository mockProviderRepository;

	@Mock
	DosageInfoRepository mockDosageInfoRepository;

	@Mock
	PatientService mockPatientService;

	@Mock
	PrescriptionRepository mockPrescriptionRepository;

	@Mock
	UserDetailsRepository mockUserDetailsRepository;

	@Mock
	UserDetailsService mockUserDetailsService;

	@Mock
	PrescriptionService mockPrescriptionService;

	@Mock
	Utility mockUtility;

	@Mock
	EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	DiagnosisService mockDiagnosisService;

	@Mock
	PrescriptionStatusRepository mockPrescriptionStatusRepository;

	@Mock
	MessageNotificationConstants mockMessageNotificationConstants;

	@Mock
	NotificationService notificationService;

	UserDetails userDetails = null;

	Patient patient = null;

	Prescription prescription = null;

	PrescriptionModel prescriptionModel = null;

	DiagnosisModel diagnosisModel = null;

	Diagnosis diagnosis = null;

	PrescriptionStatus prescriptionStatus = null;

	@Before
	public void setUp() throws Exception {
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		prescriptionStatus = TestResponseGenerator.generatePrescriptionStatus();
		prescription = TestResponseGenerator.generatePrescription();
		diagnosis = TestResponseGenerator.generateDiagnosis();
		diagnosisModel = TestResponseGenerator.generateDiagnosisModel();
		prescriptionModel = generateRequestObject("PrescriptionModel-1", PrescriptionModel.class,
		                                          PRESCRIPTION_DETAILS_FILE);
		patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
	}

	@Test
	public void addPrescriptionUpdate() throws Exception {
		//case1 : update
		Mockito.when(mockDiagnosisService.addDiagnosis(Mockito.any(Diagnosis.class)))
		       .thenReturn(diagnosis);
		Mockito.when(mockUtility.convertDiagnosisEntityToModel(diagnosis))
		       .thenReturn(diagnosisModel);

		Mockito.when(mockUtility.convertPrescriptionModelToEntity(prescriptionModel))
		       .thenReturn(prescription);
		Mockito.when(mockPrescriptionStatusRepository.findByName(Mockito.anyString()))
		       .thenReturn(prescriptionStatus);
		Mockito.when(mockPatientService.addPrescription(Mockito.any(Prescription.class), Mockito.anyLong(),
		                                                Mockito.anyLong()))
		       .thenReturn(prescriptionModel);

		Mockito.when(mockUtility.getUserPreferedLang(Mockito.anyLong()))
		       .thenReturn("en");

		Mockito.when(mockDiagnosisService.addDiagnosis(Mockito.any(Diagnosis.class)))
		       .thenReturn(diagnosis);

		List<DiagnosisModel> diagnosisModelList = new ArrayList<DiagnosisModel>();
		diagnosisModelList.add(diagnosisModel);
		prescriptionModel.setDiagnosisList(diagnosisModelList);
		//setting caregiver
		Set<Caregiver> caregiverSet = new HashSet<Caregiver>();
		caregiverSet.add(TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false));
		patient.setCaregivers(caregiverSet);

		PrescriptionModel actualprescriptionModel = mockPrescriptionHelper.addPrescription(prescriptionModel, patient,
		                                                                                   1L);
		assertNotNull(actualprescriptionModel);
	}

	@Test
	public void addPrescriptionSave() throws Exception {
		//case2 : save
		Mockito.when(mockDiagnosisService.addDiagnosis(Mockito.any(Diagnosis.class)))
		       .thenReturn(diagnosis);
		diagnosisModel.setId(null);
		Mockito.when(mockUtility.convertDiagnosisEntityToModel(diagnosis))
		       .thenReturn(diagnosisModel);
		prescription.setId(0L);
		prescription.setCretedDate(null);
		Mockito.when(mockUtility.convertPrescriptionModelToEntity(prescriptionModel))
		       .thenReturn(prescription);
		Mockito.when(mockPrescriptionStatusRepository.findByName(Mockito.anyString()))
		       .thenReturn(prescriptionStatus);
		Mockito.when(mockPatientService.addPrescription(Mockito.any(Prescription.class), Mockito.anyLong(),
		                                                Mockito.anyLong()))
		       .thenReturn(prescriptionModel);

		Mockito.when(mockUtility.getUserPreferedLang(Mockito.anyLong()))
		       .thenReturn("en");

		Mockito.when(mockDiagnosisService.addDiagnosis(Mockito.any(Diagnosis.class)))
		       .thenReturn(diagnosis);

		List<DiagnosisModel> diagnosisModelList = new ArrayList<DiagnosisModel>();
		diagnosisModelList.add(diagnosisModel);
		prescriptionModel.setDiagnosisList(diagnosisModelList);
		prescriptionModel.setId(0);

		//setting caregiver
		Set<Caregiver> caregiverSet = new HashSet<Caregiver>();
		caregiverSet.add(TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false));
		patient.setCaregivers(caregiverSet);
		//setting pharmacist
		Pharmacist pharmacist = TestResponseGenerator.generatePharmacist("Male", "Asian", "Pharmacist");
		Set<Pharmacist> pharmacistSet = new HashSet<Pharmacist>();
		pharmacistSet.add(pharmacist);
		patient.setPharmacists(pharmacistSet);

		patient.getUserDetails()
		       .setEmailFlag(true);
		patient.getUserDetails()
		       .setSmsFlag(true);

		PrescriptionModel actualprescriptionModel = mockPrescriptionHelper.addPrescription(prescriptionModel, patient,
		                                                                                   1L);
		assertNotNull(actualprescriptionModel);
	}

}
