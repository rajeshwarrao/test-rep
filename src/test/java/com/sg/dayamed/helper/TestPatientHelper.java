package com.sg.dayamed.helper;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.CaregiverService;
import com.sg.dayamed.service.PatientService;
import com.sg.dayamed.service.PharmacistService;
import com.sg.dayamed.service.ProviderService;
import com.sg.dayamed.service.UserPreferenceService;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 06/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPatientHelper extends BaseTestCase {

	@InjectMocks
	private PatientHelper mockPatientHelper = new PatientHelper();

	@Mock
	PatientService mockPatientService;

	@Mock
	UserPreferenceService mockUserPreferenceService;

	@Mock
	Authentication mockAuthentication;

	@Mock
	Utility mockUtility;

	@Mock
	ProviderService mockProviderService;

	@Mock
	RoleRepository mockRoleRepository;

	@Mock
	CaregiverService mockCaregiverService;

	@Mock
	PharmacistService mockPharmacistService;

	@Test
	public void testAddPatient() throws DataValidationException {

		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		patientModel.getUserDetails()
		            .setImanticUserid(null);

		CaregiverModel cargiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Set<CaregiverModel> caregiverSet = new HashSet<CaregiverModel>();
		caregiverSet.add(cargiverModel);
		patientModel.setCaregiverList(caregiverSet);
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(jwtUserDetails);
		//Patient patient = utility.convertPatientModelTopatientEntity(patientModel);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", UserRoleEnum.PATIENT.getRole());
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any(PatientModel.class)))
		       .thenReturn(patient);
		Pharmacist pharmacist =
				TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		Mockito.when(mockPharmacistService.directFetchPharmacistByUserId(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		//Pharmacist pharmacist = pharmacistService.directFetchPharmacistByUserId(jwtUser.getUserId());
		Caregiver caregiver =
				TestResponseGenerator.generateCaregiver("Male", "Asian", UserRoleEnum.CAREGIVER.getRole(), false);
		Mockito.when(mockCaregiverService.directFetchCaregiverById(Mockito.anyLong()))
		       .thenReturn(caregiver);
		Mockito.when(mockPatientService.addPatient(Mockito.any()))
		       .thenReturn(patientModel);
		//persistedpatientmodel = patientService.addPatient(patient);
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockProviderService.directFetchProviderByUserId(Mockito.anyLong()))
		       .thenReturn(provider);

		Role role = TestResponseGenerator.generateRole("Patient");
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString()))
		       .thenReturn(role);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		PatientModel actualPatientModel = mockPatientHelper.addPatient(patientModel, mockAuthentication);
		assertNotNull(actualPatientModel);
		assertEquals(patientModel.getId(), actualPatientModel.getId());
	}

	@Test //case 2
	public void testAddPatient2() throws DataValidationException {

		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		//patientModel.getUserDetails().setImanticUserid(null);

		CaregiverModel cargiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Set<CaregiverModel> caregiverSet = new HashSet<CaregiverModel>();
		caregiverSet.add(cargiverModel);
		patientModel.setCaregiverList(caregiverSet);
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(jwtUserDetails);
		//Patient patient = utility.convertPatientModelTopatientEntity(patientModel);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", UserRoleEnum.PATIENT.getRole());
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any(PatientModel.class)))
		       .thenReturn(patient);
		Pharmacist pharmacist =
				TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		Mockito.when(mockPharmacistService.directFetchPharmacistByUserId(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		//Pharmacist pharmacist = pharmacistService.directFetchPharmacistByUserId(jwtUser.getUserId());
		Caregiver caregiver =
				TestResponseGenerator.generateCaregiver("Male", "Asian", UserRoleEnum.CAREGIVER.getRole(), false);
		Mockito.when(mockCaregiverService.directFetchCaregiverById(Mockito.anyLong()))
		       .thenReturn(caregiver);
		Mockito.when(mockPatientService.addPatient(Mockito.any()))
		       .thenReturn(patientModel);
		//persistedpatientmodel = patientService.addPatient(patient);
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockProviderService.directFetchProviderByUserId(Mockito.anyLong()))
		       .thenReturn(provider);
		Mockito.when(mockPatientService.noDecrypCargiverfetchPatientById(Mockito.anyLong()))
		       .thenReturn(patient);
		//Patient existedPatient = patientService.noDecrypCargiverfetchPatientById(patient.getId());
		//persistedpatientmodel = patientService.updatePatient(existedPatient);
		Mockito.when(mockPatientService.updatePatient(Mockito.any()))
		       .thenReturn(patientModel);
		Role role = TestResponseGenerator.generateRole("Patient");
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString()))
		       .thenReturn(role);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		PatientModel actualPatientModel = mockPatientHelper.addPatient(patientModel, mockAuthentication);
		assertNotNull(actualPatientModel);
		assertEquals(patientModel.getId(), actualPatientModel.getId());
	}

}
