package com.sg.dayamed.pdfhelper;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class PatientPdfPageEventHelperTest {

	@InjectMocks
	private PatientPdfPageEventHelper patientPdfPageEventHelper =
			new PatientPdfPageEventHelper(new HashMap<>(), "classpath:/images/sample.png", "test");

	@Mock
	private PdfWriter mockPdfWriter;

	@Mock
	private Document mockDocument;

	@Test
	public void testOnStartPageError() {
		patientPdfPageEventHelper.onStartPage(mockPdfWriter, mockDocument);
		patientPdfPageEventHelper.onEndPage(mockPdfWriter, mockDocument);
	}
}
