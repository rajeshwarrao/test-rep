package com.sg.dayamed.validators;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.TestUtil;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 19/Jun/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestUserValidator extends BaseTestCase {

	@InjectMocks
	private UserValidator userValidator;

	@Test
	public void testValidateGetPatients() throws Exception {
		LoadPatientRequestVO requestVO = new LoadPatientRequestVO();
		requestVO.setSortOrderEnum(SortOrderEnum.ASC);
		requestVO.setSortByEnum(PatientSortByEnum.FIRST_NAME);
		requestVO.setNonAdherentFlag(true);
		requestVO.setOffset(0);
		requestVO.setLimit(10);
		requestVO.setUserDetails(TestResponseGenerator.generateJwtUserDetails(
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole()),
				true, UserRoleEnum.PROVIDER.getRole()));

		/**
		 * Case 1 : Invalid Role
		 */
		validateGetPatients(null, requestVO, APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_INVALID_ROLE);

		/**
		 * Case 2 : Admin role and both caregiver id and provider id not provided
		 */
		validateGetPatients(UserRoleEnum.ADMIN, requestVO, APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
		                    APIErrorKeys.ERRORS_EITHER_CAREGIVER_ID_OR_PROVIDER_ID_IS_REQUIRED);

		/**
		 * Case 3 : Admin role and both caregiver id and provider id are provided
		 */
		requestVO.setProviderId(TestUtil.generateId());
		requestVO.setCaregiverId(TestUtil.generateId());
		validateGetPatients(UserRoleEnum.ADMIN, requestVO, APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
		                    APIErrorKeys.ERRORS_BOTH_CAREGIVER_ID_OR_PROVIDER_ID_IS_NOT_EXPECTED);

		/**
		 * Case 4 : Success case
		 */
		userValidator.validateGetPatients(UserRoleEnum.PROVIDER, requestVO);
	}

	@Test
	public void testValidateGetPatientAssociations() throws Exception {
		PatientAssociationsRequestVO requestVO = new PatientAssociationsRequestVO();
		requestVO.setPatientId(TestUtil.generateId());
		requestVO.setUserDetails(TestResponseGenerator.generateJwtUserDetails(
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole()),
				true, UserRoleEnum.PROVIDER.getRole()));

		/**
		 * Case 1 : Invalid Role
		 */
		validateGetPatientAssociations(null, requestVO, APIErrorFields.AUTHORIZATION,
		                               APIErrorKeys.ERRORS_INVALID_ROLE);

		/**
		 * Case 2 : Admin role and both caregiver id and provider id not provided
		 */
		validateGetPatientAssociations(UserRoleEnum.ADMIN, requestVO, APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
		                               APIErrorKeys.ERRORS_EITHER_CAREGIVER_ID_OR_PROVIDER_ID_IS_REQUIRED);

		/**
		 * Case 3 : Admin role and both caregiver id and provider id are provided
		 */
		requestVO.setProviderId(TestUtil.generateId());
		requestVO.setCaregiverId(TestUtil.generateId());
		validateGetPatientAssociations(UserRoleEnum.ADMIN, requestVO, APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
		                               APIErrorKeys.ERRORS_BOTH_CAREGIVER_ID_OR_PROVIDER_ID_IS_NOT_EXPECTED);

		/**
		 * Case 4 : Success case
		 */
		userValidator.validateGetPatientAssociations(UserRoleEnum.PROVIDER, requestVO);
	}

	@Test
	public void testValidateSearchPatients() throws Exception {
		LoadPatientRequestVO requestVO = new LoadPatientRequestVO();
		requestVO.setSortOrderEnum(SortOrderEnum.ASC);
		requestVO.setSortByEnum(PatientSortByEnum.FIRST_NAME);
		requestVO.setNonAdherentFlag(true);
		requestVO.setOffset(0);
		requestVO.setLimit(10);
		requestVO.setUserDetails(TestResponseGenerator.generateJwtUserDetails(
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole()),
				true, UserRoleEnum.PROVIDER.getRole()));

		/**
		 * Case 1 : Invalid Role
		 */
		validateSearchPatients(null, requestVO, "test", APIErrorFields.AUTHORIZATION,
		                       APIErrorKeys.ERRORS_INVALID_ROLE);

		/**
		 * Case 2 : Admin role and both caregiver id and provider id not provided
		 */
		validateSearchPatients(UserRoleEnum.ADMIN, requestVO, "text", APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
		                       APIErrorKeys.ERRORS_EITHER_CAREGIVER_ID_OR_PROVIDER_ID_IS_REQUIRED);

		/**
		 * Case 3 : Admin role and both caregiver id and provider id are provided
		 */
		requestVO.setProviderId(TestUtil.generateId());
		requestVO.setCaregiverId(TestUtil.generateId());
		validateSearchPatients(UserRoleEnum.ADMIN, requestVO, "text", APIErrorFields.ADMIN_LOAD_PATENT_ERROR_FIELD,
		                       APIErrorKeys.ERRORS_BOTH_CAREGIVER_ID_OR_PROVIDER_ID_IS_NOT_EXPECTED);

		/**
		 * Case 4 : query string less than 3 characters
		 */
		requestVO.setCaregiverId(null);
		validateSearchPatients(UserRoleEnum.ADMIN, requestVO, "t", APIErrorFields.QUERY_STRING,
		                       APIErrorKeys.ERRORS_INVALID_LENGTH_FOR_QUERY);

		/**
		 * Case 5 : Success case
		 */
		userValidator.validateSearchPatients(UserRoleEnum.PROVIDER, requestVO, "text");
	}

	@Test
	public void testValidateUserRole() {
		try {
			userValidator.validateUserRole("xxx");
		} catch (DataValidationException dve) {
			assertEquals(APIErrorFields.AUTHORIZATION, dve.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_ROLE, dve.getErrorKey());
		}

	}

	@Test
	public void testValidatePharmacistPatients() throws Exception {
		/**
		 * Case 1 : provider with gsenable flag false
		 */
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, false, UserRoleEnum.PROVIDER.getRole());
		validatePharmacistPatients(jwtUserDetails, APIErrorFields.AUTHORIZATION,
		                           IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);

		/**
		 * Case 2 : Invalid role other than Provider or Pharmacist
		 */
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PATIENT.getRole());
		jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, false, UserRoleEnum.PATIENT.getRole());
		validatePharmacistPatients(jwtUserDetails, APIErrorFields.AUTHORIZATION,
		                           IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);

		/**
		 * Case 3 : Success case
		 */
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
		UserRoleEnum userRoleEnum = userValidator.validatePharmacistPatients(jwtUserDetails);
		assertNotNull(userRoleEnum);
		assertEquals(UserRoleEnum.PROVIDER, userRoleEnum);
	}

	private void validateGetPatients(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO, String fieldName,
	                                 String errorKey) {
		try {
			userValidator.validateGetPatients(userRoleEnum, requestVO);
			Assert.fail();
		} catch (DataValidationException dve) {
			assertEquals(fieldName, dve.getFieldName());
			assertEquals(errorKey, dve.getErrorKey());
		}
	}

	private void validateGetPatientAssociations(UserRoleEnum userRoleEnum, PatientAssociationsRequestVO requestVO,
	                                            String fieldName, String errorKey) {
		try {
			userValidator.validateGetPatientAssociations(userRoleEnum, requestVO);
			Assert.fail();
		} catch (DataValidationException dve) {
			assertEquals(fieldName, dve.getFieldName());
			assertEquals(errorKey, dve.getErrorKey());
		}
	}

	private void validateSearchPatients(UserRoleEnum userRoleEnum, LoadPatientRequestVO requestVO, String query,
	                                    String fieldName, String errorKey) {
		try {
			userValidator.validateSearchPatients(userRoleEnum, requestVO, query);
			Assert.fail();
		} catch (DataValidationException dve) {
			assertEquals(fieldName, dve.getFieldName());
			assertEquals(errorKey, dve.getErrorKey());
		}
	}

	private void validatePharmacistPatients(JwtUserDetails userDetails, String fieldName, String errorKey) {
		try {
			userValidator.validatePharmacistPatients(userDetails);
			Assert.fail();
		} catch (DataValidationException dve) {
			assertEquals(fieldName, dve.getFieldName());
			assertEquals(errorKey, dve.getErrorKey());
		}
	}
}
