package com.sg.dayamed.commons.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

/**
 * Created By Gorantla, Eresh on 04/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class BaseRestApiImplTest extends BaseTestCase {

	@InjectMocks
	private BaseRestApiImpl baseRestApi;
	
	@Mock
	Utility utility;

	private final String commonEmail = "test@gmail.com";
	
	@Test
	public void testInstanceOfPatientModelForProvider() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		ProviderModel providerModel = generateRequestObject("ProviderModel-1", ProviderModel.class, USER_DETAILS_FILE);
		patientModel.setProviders(Stream.of(providerModel)
		                                .collect(Collectors.toSet()));
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());

		/**
		 * Case 1 : Unauthorized.
		 */
		Integer result = baseRestApi.instanceOfPatientModel(patientModel, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.UNAUTHORIZED.value(), result.intValue());

		/**
		 * Case 2 : Authorized.
		 */
		patientModel.getProviders()
		            .forEach(provider -> provider.getUserDetails()
		                                         .setEmailId(commonEmail));
		jwtUserDetails.setUserEmailId(commonEmail);
		result = baseRestApi.instanceOfPatientModel(patientModel, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.OK.value(), result.intValue());
	}

	@Test
	public void testInstanceOfPatientModelForCaregiver() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		CaregiverModel caregiverModel =
				generateRequestObject("CaregiverModel-1", CaregiverModel.class, USER_DETAILS_FILE);
		patientModel.setCaregivers(Stream.of(caregiverModel)
		                                 .collect(Collectors.toSet()));
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());

		/**
		 * Case 1 : Unauthorized.
		 */
		Integer result = baseRestApi.instanceOfPatientModel(patientModel, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.UNAUTHORIZED.value(), result.intValue());

		/**
		 * Case 2 : Authorized.
		 */
		patientModel.getCaregivers()
		            .forEach(caregiver -> caregiver.getUserDetails()
		                                           .setEmailId(commonEmail));
		jwtUserDetails.setUserEmailId(commonEmail);
		result = baseRestApi.instanceOfPatientModel(patientModel, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.OK.value(), result.intValue());
	}

	@Test
	public void testInstanceOfPatientModelForPharmacist() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		PharmacistModel pharmacistModel =
				generateRequestObject("PharmacistModel-1", PharmacistModel.class, USER_DETAILS_FILE);
		patientModel.setPharmacists(Stream.of(pharmacistModel)
		                                  .collect(Collectors.toSet()));
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PHARMACIST.getRole());

		/**
		 * Case 1 : Unauthorized.
		 */
		Integer result = baseRestApi.instanceOfPatientModel(patientModel, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.UNAUTHORIZED.value(), result.intValue());

		/**
		 * Case 2 : Authorized.
		 */
		patientModel.getPharmacists()
		            .forEach(pharmacist -> pharmacist.getUserDetails()
		                                             .setEmailId(commonEmail));
		jwtUserDetails.setUserEmailId(commonEmail);
		result = baseRestApi.instanceOfPatientModel(patientModel, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.OK.value(), result.intValue());
	}

	@Test
	public void testInstanceOfPrescriptionForProvider() {
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", UserRoleEnum.PATIENT.getRole());
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
		Prescription prescription = TestResponseGenerator.generatePrescription();
		prescription.setPatient(patient);

		/**
		 * Case 1 : Unauthorized
		 */
		Integer result = baseRestApi.instanceOfPrescription(prescription, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.UNAUTHORIZED.value(), result.intValue());

		/**
		 * Case 2 : Authorized
		 */
		prescription.getPatient()
		            .getProviders()
		            .forEach(provider -> provider.getUserDetails()
		                                         .setEmailId(commonEmail));
		jwtUserDetails.setUserEmailId(commonEmail);
		result = baseRestApi.instanceOfPrescription(prescription, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.OK.value(), result.intValue());
	}

	@Test
	public void testInstanceOfPrescriptionForCaregiver() {
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", UserRoleEnum.PATIENT.getRole());
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		Prescription prescription = TestResponseGenerator.generatePrescription();
		prescription.setPatient(patient);

		/**
		 * Case 1 : Unauthorized
		 */
		Integer result = baseRestApi.instanceOfPrescription(prescription, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.UNAUTHORIZED.value(), result.intValue());

		/**
		 * Case 2 : Authorized
		 */
		prescription.getPatient()
		            .getCaregivers()
		            .forEach(caregiver -> caregiver.getUserDetails()
		                                           .setEmailId(commonEmail));
		jwtUserDetails.setUserEmailId(commonEmail);
		result = baseRestApi.instanceOfPrescription(prescription, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.OK.value(), result.intValue());
	}

	@Test
	public void testInstanceOfPrescriptionForPharmacist() {
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", UserRoleEnum.PATIENT.getRole());
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PHARMACIST.getRole());
		Prescription prescription = TestResponseGenerator.generatePrescription();
		prescription.setPatient(patient);

		/**
		 * Case 1 : Unauthorized
		 */
		Integer result = baseRestApi.instanceOfPrescription(prescription, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.UNAUTHORIZED.value(), result.intValue());

		/**
		 * Case 2 : Authorized
		 */
		prescription.getPatient()
		            .getPharmacists()
		            .forEach(pharmacist -> pharmacist.getUserDetails()
		                                             .setEmailId(commonEmail));
		jwtUserDetails.setUserEmailId(commonEmail);
		result = baseRestApi.instanceOfPrescription(prescription, jwtUserDetails);
		assertNotNull(result);
		assertEquals(HttpStatus.OK.value(), result.intValue());
	}

}
