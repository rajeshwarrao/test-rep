package com.sg.dayamed.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.sg.dayamed.dao.UserDetailsCacheVO;
import com.sg.dayamed.dao.UserDetailsDao;
import com.sg.dayamed.util.TestResponseGenerator;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created By Gorantla, Eresh on 12/Sep/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class CacheDataComponentTest {

	@InjectMocks
	private CacheDataComponent cacheDataComponent;

	@Mock
	private UserDetailsDao mockUserDetailsDao;

	@Test
	public void testValidateUserDetails() {


		List<UserDetailsCacheVO> cacheVOS = IntStream.range(0, 10)
		                                             .mapToObj(index -> TestResponseGenerator.generateUserDetailsCacheVO())
		                                             .collect(Collectors.toList());
		Mockito.when(mockUserDetailsDao.loadUserCacheDetails()).thenReturn(cacheVOS);

		/**
		 * Case 1 : Data exists
		 */
		Long userId = cacheVOS.get(0).getId();
		UserDetailsCacheVO cacheVO = cacheDataComponent.validateUserDetails(userId);
		assertNotNull(cacheVO);
		assertEquals(cacheVOS.get(0).getId(), cacheVO.getId());
		assertEquals(cacheVOS.get(0).getRole(), cacheVO.getRole());
		assertEquals(cacheVOS.get(0).getRoleId(), cacheVO.getRoleId());

		/**
		 * Case 2 : Data not exists
		 */
		cacheVO = cacheDataComponent.validateUserDetails(NumberUtils.LONG_MINUS_ONE);
		assertNull(cacheVO);
	}
}
