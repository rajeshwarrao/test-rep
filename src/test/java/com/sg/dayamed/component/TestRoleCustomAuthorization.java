package com.sg.dayamed.component;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 01/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestRoleCustomAuthorization {

	@Mock
	private Authentication mockAuthentication;

	@InjectMocks
	private RoleCustomAuthorization roleCustomAuthorization;

	@Test
	public void testIsAdmin() {
		/**
		 * Case 1 : Role is admin. True will be returned
		 */
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.ADMIN.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.ADMIN.getRole());
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(jwtUserDetails);
		assertTrue(roleCustomAuthorization.isAdmin(mockAuthentication));

		/**
		 * Case 2 : Role is caregiver. false is returned.
		 */
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(jwtUserDetails);
		assertFalse(roleCustomAuthorization.isAdmin(mockAuthentication));
	}

}
