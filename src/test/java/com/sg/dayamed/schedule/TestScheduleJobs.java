package com.sg.dayamed.schedule;

import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.service.NotificationScheduleService;
import com.sg.dayamed.service.PaitentAdherenceWeeklyPDFReport;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.service.WeeklyEmailNotificationToPatient;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 21/Jun/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestScheduleJobs {

	@InjectMocks
	private ScheduleJobs scheduleJobs;

	@Mock
	private DateUtility mockDateUtility;

	@Mock
	private NotificationScheduleRepository mockNotificationScheduleRepository;

	@Mock
	private NotificationScheduleService mockNotificationScheduleService;

	@Mock
	private EmailService emailService;

	@Mock
	private EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	private UserDetailsService userDetailsService;

	@Mock
	private PaitentAdherenceWeeklyPDFReport mockPaitentAdherenceWeeklyPDFReport;

	@Mock
	private Utility utility;

	@Mock
	MessageNotificationConstants messageNotificationConstants;

	@Mock
	private WeeklyEmailNotificationToPatient mockWeeklyEmailNotificationToPatient;

	@Before
	public void setUp() {
		ReflectionTestUtils.setField(scheduleJobs, "callDeeplink", "http://test.com");
	}

	@Test
	public void testExecuteDosageAndDeviceSchedulerJobsByFireTime() {
		Mockito.when(mockDateUtility.getFormattedLocalDateTime(Mockito.any()))
		       .thenReturn("2019-06-06 10:00");
		Mockito.when(mockNotificationScheduleRepository.getNotificationScheduleIdsNotInDosageInfoTable())
		       .thenReturn(Stream.of(new BigInteger("1"))
		                         .collect(Collectors.toList()));
		NotificationSchedule emailnotificationSchedule = TestResponseGenerator.generateEmailNotificationSchedule();
		List<UserDetails> listUserDetails = new ArrayList<>();
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		listUserDetails.add(userDetails);
		Mockito.when(userDetailsService.findUserDetailsByIdInAndRole(Mockito.any(), Mockito.anyString()))
		       .thenReturn(listUserDetails);
		Mockito.when(utility.getUserPreferedLang(Mockito.any()))
		       .thenReturn("en");
		/*Mockito.when(messageNotificationConstants.getUserPreferedLangTranslatedMsg(Mockito.any(), Mockito.anyString(),
		                                                                           Mockito.anyString(),Mockito.any(Object[].class)))
		       .thenReturn(RandomStringUtils.randomAlphanumeric(15));*/
		NotificationSchedule smsnotificationSchedule = TestResponseGenerator.generateSMSNotificationSchedule();
		emailnotificationSchedule.setDosageInfo(TestResponseGenerator.generateDosageInfo(false));
		emailnotificationSchedule.getDosageInfo()
		                         .setPrescription(TestResponseGenerator.generatePrescription());
		emailnotificationSchedule.getDosageInfo()
		                         .getPrescription()
		                         .setPatient(TestResponseGenerator.generatePatient("Male", "Asian",
		                                                                           UserRoleEnum.PATIENT.getRole()));
		emailnotificationSchedule.setConsumptionStatus(Stream.of("delayed")
		                                                     .collect(Collectors.toList()));
		emailnotificationSchedule.setOwner_userId(userDetails.getId());
		smsnotificationSchedule.setDosageInfo(TestResponseGenerator.generateDosageInfo(false));
		smsnotificationSchedule.getDosageInfo()
		                       .setPrescription(TestResponseGenerator.generatePrescription());
		smsnotificationSchedule.getDosageInfo()
		                       .getPrescription()
		                       .setPatient(TestResponseGenerator.generatePatient("Male", "Asian",
		                                                                         UserRoleEnum.PATIENT.getRole()));
		smsnotificationSchedule.setConsumptionStatus(Stream.of("delayed")
		                                                   .collect(Collectors.toList()));
		smsnotificationSchedule.setOwner_userId(userDetails.getId());
		Mockito.when(mockNotificationScheduleRepository.findByFireTimeLessThanEqual(Mockito.any()))
		       .thenReturn(Stream.of(emailnotificationSchedule, smsnotificationSchedule)
		                         .collect(Collectors.toList()));
		scheduleJobs.executeDosageAndDeviceSchedlerJobsByFireTime();

	}

	@Test
	public void testScheduleWeeklyPatientAdherence() {
		Mockito.doNothing().when(mockPaitentAdherenceWeeklyPDFReport).getPatientRecoreds();
		scheduleJobs.scheduleWeeklyPatientAdherence();

		/**
		 * Error case
 		 */
		Mockito.reset(mockPaitentAdherenceWeeklyPDFReport);
		Mockito.doThrow(new RuntimeException()).when(mockPaitentAdherenceWeeklyPDFReport).getPatientRecoreds();
		scheduleJobs.scheduleWeeklyPatientAdherence();
	}

	@Test
	public void testWeeklyEmailNotificationToPatient() {
		scheduleJobs.weeklyEmailNotificationToPatient();

		/**
		 * Error case
		 */
		Mockito.doThrow(new RuntimeException()).when(mockWeeklyEmailNotificationToPatient).sendNotificationToPatient(Mockito.anyString(),Mockito.any());
		scheduleJobs.weeklyEmailNotificationToPatient();
	}

	@Test
	public void testDailyEmailNotificationToPatient() {
		scheduleJobs.dailyEmailNotificationToPatient();

		/**
		 * Error case
		 */
		Mockito.doThrow(new RuntimeException()).when(mockWeeklyEmailNotificationToPatient).sendNotificationToPatient(Mockito.anyString(), Mockito.any());
		scheduleJobs.weeklyEmailNotificationToPatient();
	}

}
