package com.sg.dayamed.repository.specification;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.TestUtil;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created by Eresh Gorantla on 18/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPatientLoadingSpecification {

	@InjectMocks
	private PatientLoadingSpecification patientLoadingSpecification;

	@Mock
	private Root<PatientAssociationDetailsV> mockRoot;

	@Mock
	private CriteriaQuery<?> mockCriteriaQuery;

	@Mock
	private CriteriaBuilder mockCriteriaBuilder;

	@Test
	public void testPredicates() {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PROVIDER);
		requestVO.setSortByEnum(PatientSortByEnum.ACTIVE_TIME);
		patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.PROVIDER, requestVO);
		patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.PROVIDER, requestVO);
		patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.CAREGIVER);
		patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.CAREGIVER, requestVO);
		patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PHARMACIST);
		patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.PHARMACIST, requestVO);
		patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.ADMIN);
		requestVO.setCaregiverId(TestUtil.generateId());
		patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.ADMIN, requestVO);
		patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.ADMIN);
		requestVO.setProviderId(TestUtil.generateId());
		patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.ADMIN, requestVO);
		patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

        /*patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.CAREGIVER, PatientSortByEnum
        .ACTIVE_TIME, SortOrderEnum.DESC, NumberUtils.LONG_ONE);
        patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);
        patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.PHARMACIST, PatientSortByEnum
        .FIRST_NAME, SortOrderEnum.DESC, NumberUtils.LONG_ONE);
        patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);
        patientLoadingSpecification = new PatientLoadingSpecification(UserRoleEnum.ADMIN, PatientSortByEnum
        .FIRST_NAME, SortOrderEnum.DESC, NumberUtils.LONG_ONE);
        patientLoadingSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);*/
	}
}
