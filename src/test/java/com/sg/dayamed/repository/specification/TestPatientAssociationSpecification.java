package com.sg.dayamed.repository.specification;

import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.TestUtil;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientAssociationSpecification {

	@InjectMocks
	private PatientAssociationSpecification patientAssociationSpecification;

	@Mock
	private Root<PatientAssociationDetailsV> mockRoot;

	@Mock
	private CriteriaQuery<?> mockCriteriaQuery;

	@Mock
	private CriteriaBuilder mockCriteriaBuilder;

	@Test
	public void testPredicates() {
		PatientAssociationsRequestVO requestVO =
				TestResponseGenerator.generatePatientAssociationsRequest(UserRoleEnum.PROVIDER);
		PatientAssociationSpecification patientAssociationSpecification =
				new PatientAssociationSpecification(UserRoleEnum.PROVIDER, requestVO);
		patientAssociationSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generatePatientAssociationsRequest(UserRoleEnum.CAREGIVER);
		patientAssociationSpecification = new PatientAssociationSpecification(UserRoleEnum.CAREGIVER, requestVO);
		patientAssociationSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generatePatientAssociationsRequest(UserRoleEnum.PHARMACIST);
		patientAssociationSpecification = new PatientAssociationSpecification(UserRoleEnum.PHARMACIST, requestVO);
		patientAssociationSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generatePatientAssociationsRequest(UserRoleEnum.ADMIN);
		requestVO.setProviderId(TestUtil.generateId());
		patientAssociationSpecification = new PatientAssociationSpecification(UserRoleEnum.ADMIN, requestVO);
		patientAssociationSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO = TestResponseGenerator.generatePatientAssociationsRequest(UserRoleEnum.ADMIN);
		requestVO.setProviderId(null);
		requestVO.setCaregiverId(TestUtil.generateId());
		patientAssociationSpecification = new PatientAssociationSpecification(UserRoleEnum.ADMIN, requestVO);
		patientAssociationSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);
	}
}
