package com.sg.dayamed.repository.specification;

import com.sg.dayamed.entity.view.PatientCaregiverAssociationsV;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created By Gorantla, Eresh on 01/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPatientCaregiverSpecification {

	private UserRoleEnum userRoleEnum;

	private LoadPatientRequestVO requestVO;

	@Mock
	private Root<PatientCaregiverAssociationsV> mockRoot;

	@Mock
	private CriteriaQuery<?> mockCriteriaQuery;

	@Mock
	private CriteriaBuilder mockCriteriaBuilder;

	@InjectMocks
	private PatientCaregiverSpecification patientCaregiverSpecification;

	@Before
	public void setup() {
		userRoleEnum = UserRoleEnum.PHARMACIST;
		requestVO = TestResponseGenerator.generateLoadPatientRequest(userRoleEnum);
	}

	@Test
	public void testToPredicate() {
		requestVO.setSortByEnum(PatientSortByEnum.ACTIVE_TIME);
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		patientCaregiverSpecification = new PatientCaregiverSpecification(userRoleEnum, requestVO);
		patientCaregiverSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);

		requestVO.setSortOrderEnum(SortOrderEnum.ASC);
		patientCaregiverSpecification = new PatientCaregiverSpecification(userRoleEnum, requestVO);
		patientCaregiverSpecification.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);
	}
}
