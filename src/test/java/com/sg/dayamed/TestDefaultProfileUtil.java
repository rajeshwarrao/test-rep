package com.sg.dayamed;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created By Gorantla, Eresh on 04/Jul/2019
 **/

public class TestDefaultProfileUtil {

	private org.springframework.boot.SpringApplication mockSpringBootApplication;

	@Test
	public void testAddDefaultProfile() {
		mockSpringBootApplication = Mockito.mock(org.springframework.boot.SpringApplication.class);
		DefaultProfileUtil.addDefaultProfile(mockSpringBootApplication);
		Mockito.verify(mockSpringBootApplication, Mockito.times(1))
		       .setDefaultProperties(Mockito.anyMap());
	}

}
