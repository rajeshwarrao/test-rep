package com.sg.dayamed;

import com.sg.dayamed.service.LoadConfigParamsOnRedis;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 04/Jul/2019
 **/
@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor("com.sg.dayamed.DayaMedApplication")
@PrepareForTest({org.springframework.boot.SpringApplication.class, DefaultProfileUtil.class})
public class TestDayamedGSApplication {

	@InjectMocks
	private DayaMedApplication dayaMedGSApplication;

	@Mock
	private LoadConfigParamsOnRedis mockLoadConfigParamsOnRedis;

	@Mock
	private ConfigurableEnvironment mockEnvironment;

	@Mock
	private ConfigurableApplicationContext mockConfigurableApplicationContext;

	@Mock
	private Logger mockLogger;

	private org.springframework.boot.SpringApplication mockSpringBootApplication;

	@Before
	public void setup() throws Exception {
		mockSpringBootApplication = Mockito.mock(org.springframework.boot.SpringApplication.class);
		PowerMockito.whenNew(org.springframework.boot.SpringApplication.class)
		            .withArguments(Mockito.eq(DayaMedApplication.class))
		            .thenReturn(mockSpringBootApplication);
		PowerMockito.mock(DefaultProfileUtil.class);
		PowerMockito.mockStatic(DefaultProfileUtil.class);
		Mockito.when(mockSpringBootApplication.run(Mockito.anyString()))
		       .thenReturn(mockConfigurableApplicationContext);
		Mockito.when(mockConfigurableApplicationContext.getEnvironment())
		       .thenReturn(mockEnvironment);
		ReflectionTestUtils.setField(dayaMedGSApplication, "logger", mockLogger);
		ReflectionTestUtils.setField(dayaMedGSApplication, "loadConfigParamsOnRedis", mockLoadConfigParamsOnRedis);
	}

	@After
	public void tearDown() {
		Mockito.reset(mockLoadConfigParamsOnRedis);
	}

	@Test
	public void testLoadConfigParametersIntoRedis() {
		Integer result;
		dayaMedGSApplication.loadConfigParametersIntoRedis();
		result = NumberUtils.INTEGER_ONE;
		assertTrue(NumberUtils.INTEGER_ONE.equals(result));
	}

	@Test
	public void testMainMethod() throws Exception {
		Integer result;
		Mockito.when(mockEnvironment.getProperty(Mockito.eq("server.ssl.key-store")))
		       .thenReturn("value");
		DayaMedApplication.main(new String[]{"a"});
		result = NumberUtils.INTEGER_ONE;
		assertTrue(NumberUtils.INTEGER_ONE.equals(result));
	}

}
