package com.sg.dayamed.security.jwtsecurity.security;

import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class JwtGeneratorTest {

	@InjectMocks
	private JwtGenerator jwtGenerator;

	@Mock
	private RediseService mockRediseService;

	@Test
	public void testGenerateJwt() {
		JwtUser jwtUser = TestResponseGenerator.generateJwtUser(UserRoleEnum.PROVIDER);
		String jwtToken = jwtGenerator.generate(jwtUser);
		assertNotNull(jwtToken);
		Mockito.verify(mockRediseService, Mockito.times(1))
		       .saveDataInRedis(Mockito.eq(jwtToken), Mockito.eq(jwtUser));
	}
}
