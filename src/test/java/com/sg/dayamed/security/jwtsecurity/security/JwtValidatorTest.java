package com.sg.dayamed.security.jwtsecurity.security;

import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class JwtValidatorTest {

	@InjectMocks
	private JwtValidator jwtValidator;

	@Mock
	private RediseService mockRediseService;

	@Test
	public void testValidate() {
		JwtUser expectedJwtUser = TestResponseGenerator.generateJwtUser(UserRoleEnum.PROVIDER);
		Mockito.when(mockRediseService.getDataInRedis(Mockito.anyString(), Mockito.anyBoolean()))
		       .thenReturn(expectedJwtUser);
		JwtUser actualUser = jwtValidator.validate(RandomStringUtils.randomAlphanumeric(10));
		assertNotNull(actualUser);
		assertEquals(expectedJwtUser, actualUser);
	}

	@Test
	public void testValidateErrors() {
		Mockito.when(mockRediseService.getDataInRedis(Mockito.anyString(), Mockito.anyBoolean()))
		       .thenReturn(null);
		try {
			jwtValidator.validate(RandomStringUtils.randomAlphanumeric(10));
		} catch (UsernameNotFoundException e) {
			assertEquals("Token expired", e.getMessage());
		}

		Mockito.reset(mockRediseService);
		Mockito.when(mockRediseService.getDataInRedis(Mockito.anyString(), Mockito.anyBoolean()))
		       .thenThrow(new RuntimeException());
		try {
			jwtValidator.validate(RandomStringUtils.randomAlphanumeric(10));
		} catch (UsernameNotFoundException e) {
			assertEquals("Token expired", e.getMessage());
		}
	}

}
