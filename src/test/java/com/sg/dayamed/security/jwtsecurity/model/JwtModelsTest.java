package com.sg.dayamed.security.jwtsecurity.model;

import org.junit.Test;

import java.util.UUID;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created By Gorantla, Eresh on 14/Aug/2019
 **/

public class JwtModelsTest {

	@Test
	public void testJwtAuthenticationToken() {
		JwtAuthenticationToken token = new JwtAuthenticationToken(UUID.randomUUID().toString());
		token.setToken(UUID.randomUUID().toString());
		assertNotNull(token.getToken());
		assertNull(token.getCredentials());
		assertNull(token.getPrincipal());
	}
}
