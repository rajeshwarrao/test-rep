package com.sg.dayamed.security.jwtsecurity.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.sg.dayamed.security.jwtsecurity.model.JwtAuthenticationToken;
import com.sg.dayamed.security.jwtsecurity.model.JwtUser;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class JwtAuthenticationProviderTest {

	@InjectMocks
	private JwtAuthenticationProvider authenticationProvider;

	@Mock
	private JwtAuthenticationToken mockJwtAuthenticationToken;

	@Mock
	private JwtValidator mockJwtValidator;

	@Test
	public void testAdditionalAuthenticationChecks() {
		authenticationProvider.additionalAuthenticationChecks(null, null);
	}

	@Test
	public void testRetrieveUserErrors() {
		String userName = RandomStringUtils.randomAlphabetic(10);
		String jwt = RandomStringUtils.randomAlphabetic(50);
		try {
			authenticationProvider.retrieveUser(userName, mockJwtAuthenticationToken);
		} catch (UsernameNotFoundException e) {
			assertEquals("JWT Token is incorrect", e.getMessage());
		}
		Mockito.when(mockJwtAuthenticationToken.getToken())
		       .thenReturn(jwt);
		Mockito.when(mockJwtValidator.validate(Mockito.anyString()))
		       .thenThrow(new UsernameNotFoundException("Token expired"));
		try {
			authenticationProvider.retrieveUser(userName, mockJwtAuthenticationToken);
		} catch (UsernameNotFoundException e) {
			assertEquals("Token expired", e.getMessage());
		}
	}

	@Test
	public void testRetrieveUser() {
		String userName = RandomStringUtils.randomAlphabetic(10);
		String jwt = RandomStringUtils.randomAlphabetic(50);
		Mockito.when(mockJwtAuthenticationToken.getToken())
		       .thenReturn(jwt);
		JwtUser jwtUser = TestResponseGenerator.generateJwtUser(UserRoleEnum.PROVIDER);
		Mockito.when(mockJwtValidator.validate(Mockito.anyString()))
		       .thenReturn(jwtUser);
		UserDetails userDetails = authenticationProvider.retrieveUser(userName, mockJwtAuthenticationToken);
		assertNotNull(userDetails);
		assertEquals(jwtUser.getRole(), userDetails.getAuthorities()
		                                           .stream()
		                                           .map(auth -> auth.getAuthority())
		                                           .findFirst()
		                                           .orElse(null));
	}

}
