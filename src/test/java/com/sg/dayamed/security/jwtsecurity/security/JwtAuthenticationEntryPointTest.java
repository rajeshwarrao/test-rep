package com.sg.dayamed.security.jwtsecurity.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created By Gorantla, Eresh on 14/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class JwtAuthenticationEntryPointTest {

	@InjectMocks
	private JwtAuthenticationEntryPoint authenticationEntryPoint;

	@Mock
	private HttpServletRequest mockServletRequest;

	@Mock
	private HttpServletResponse mockServletResponse;

	@Mock
	private AuthenticationException mockAuthenticationException;

	@Test
	public void testCommence() throws Exception {
		authenticationEntryPoint.commence(mockServletRequest, mockServletResponse, mockAuthenticationException);
		Mockito.verify(mockServletResponse, Mockito.times(1))
		       .sendError(Mockito.eq(HttpServletResponse.SC_UNAUTHORIZED), Mockito.eq("UNAUTHORIZED"));
	}
}
