package com.sg.dayamed.security.jwtsecurity.security;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestJwtAuthenticationTokenFilter extends BaseTestCase {

	@InjectMocks
	private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private FilterChain mockFilterChain;

	@Mock
	private Authentication mockAuthentication;

	@Test
	public void testSuccessfulAuthentication() throws Exception {
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(userDetails, false, "Patient");
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(jwtUserDetails);
		jwtAuthenticationTokenFilter.successfulAuthentication(mockHttpServletRequest, mockHttpServletResponse,
		                                                      mockFilterChain, mockAuthentication);
	}
}
