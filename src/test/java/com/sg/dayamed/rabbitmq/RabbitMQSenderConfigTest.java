package com.sg.dayamed.rabbitmq;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class RabbitMQSenderConfigTest {

	@InjectMocks
	private RabbitMQSenderConfig rabbitMQSenderConfig;

	@Mock
	private Queue mockQueue;

	@Mock
	private DirectExchange mockDirectExchange;

	@Mock
	private ConnectionFactory mockConnectionFactory;

	@Before
	public void init() {
		ReflectionTestUtils.setField(rabbitMQSenderConfig, "emailQueue", "test");
		ReflectionTestUtils.setField(rabbitMQSenderConfig, "smsQueue", "test");
		ReflectionTestUtils.setField(rabbitMQSenderConfig, "exchange", "test");
		ReflectionTestUtils.setField(rabbitMQSenderConfig, "routingkey", "test");
	}

	@Test
	public void testExchange() {
		assertNotNull(rabbitMQSenderConfig.exchange());
	}

	@Test
	public void testEmailQueue() {
		assertNotNull(rabbitMQSenderConfig.emailQueue());
	}

	@Test
	public void testEmailBinding() {
		assertNotNull(rabbitMQSenderConfig.emailbinding(mockQueue, mockDirectExchange));
	}

	@Test
	public void testSmsQueue() {
		assertNotNull(rabbitMQSenderConfig.smsQueue());
	}

	@Test
	public void testSmsBinding() {
		assertNotNull(rabbitMQSenderConfig.smsbinding(mockQueue, mockDirectExchange));
	}

	@Test
	public void testJsonMessageConverter() {
		assertNotNull(rabbitMQSenderConfig.jsonMessageConverter());
	}

	@Test
	public void testRabbitTemplate() {
		assertNotNull(rabbitMQSenderConfig.rabbitTemplate(mockConnectionFactory));
	}
}
