package com.sg.dayamed.rabbitmq;

import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.email.MessageModel;
import com.sg.dayamed.email.SmsService;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class RabbitMQConsumerTest {

	@InjectMocks
	private RabbitMQConsumer rabbitMQConsumer;

	@Mock
	private SmsService mockSmsService;

	@Mock
	private EmailService mockEmailService;

	@Before
	public void init() {
		Mockito.when(mockSmsService.sendSMSNotification(Mockito.any()))
		       .thenReturn(true);
		Mockito.doNothing()
		       .when(mockEmailService)
		       .sendEmail(Mockito.any());
	}

	@Test
	public void testEmailReceiver() {
		Email email = new Email();
		email.setRecipientList(Stream.of("test@gmail.com")
		                             .collect(Collectors.toList()));
		rabbitMQConsumer.emailReciever(email);
		Mockito.verify(mockEmailService, Mockito.times(1))
		       .sendEmail(Mockito.eq(email));
	}

	@Test
	public void testSmsReceiver() {
		MessageModel model = new MessageModel();
		model.setTocontact(RandomStringUtils.randomNumeric(10));
		rabbitMQConsumer.smsReciever(model);
		Mockito.verify(mockSmsService, Mockito.times(1))
		       .sendSMSNotification(Mockito.eq(model));
	}
}
