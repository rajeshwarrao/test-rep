package com.sg.dayamed.rabbitmq;

import com.sg.dayamed.email.Email;
import com.sg.dayamed.email.MessageModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.service.RediseService;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class RabbitMQSenderTest {

	@InjectMocks
	private RabbitMQSender rabbitMQSender;

	@Mock
	private AmqpTemplate mockAMQPTemplate;

	@Mock
	private RediseService mockRedisService;

	@Before
	public void init() {
		Mockito.reset(mockAMQPTemplate);
		ReflectionTestUtils.setField(rabbitMQSender, "exchange", "test");
		ReflectionTestUtils.setField(rabbitMQSender, "routingkey", "test");
	}

	@Test
	public void testSendEmail() {
		Email email = new Email();
		email.setRecipientList(Stream.of("test@gmail.com")
		                             .collect(Collectors.toList()));
		email.setSubject(RandomStringUtils.randomAlphanumeric(10));
		email.setBody(RandomStringUtils.randomAlphanumeric(20));
		rabbitMQSender.sendEmail(email);
	}

	@Test
	public void testSendSms() {
		MessageModel model = new MessageModel();
		model.setTocontact(RandomStringUtils.randomNumeric(10));
		model.setCountryPhoneCode("91");
		model.setMessage(RandomStringUtils.randomAlphanumeric(10));
		Mockito.when(mockRedisService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.GS_GLOBAL_SMS_ENABLE)))
		       .thenReturn("true");
		rabbitMQSender.sendSms(model);
	}

}
