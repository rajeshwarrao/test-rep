package com.sg.dayamed;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestDayaConfiguration {

	@InjectMocks
	private DayaConfiguration dayaConfiguration;

	@Mock
	private CorsRegistry mockCorsRegistry;

	@Test
	public void testAddCorsMappings() {
		CorsRegistration registration = new CorsRegistration("test");
		Mockito.when(mockCorsRegistry.addMapping(Mockito.anyString()))
		       .thenReturn(registration);
		dayaConfiguration.addCorsMappings(mockCorsRegistry);
	}

	@Test
	public void testApi() {
		dayaConfiguration.api();
	}

	@Test
	public void testMessageSource() {
		dayaConfiguration.messageSource();
	}

	@Test
	public void testJsonMapper() {
		dayaConfiguration.jsonMapper();
	}

	@Test
	public void testGetValidator() {
		LocalValidatorFactoryBean validatorFactoryBean = dayaConfiguration.getValidator();
		assertNotNull(validatorFactoryBean);
		dayaConfiguration.restTemplate();
	}

}
