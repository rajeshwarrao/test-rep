package com.sg.dayamed.errorhandling;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/

public class ErrorHandlingUtilitiesTest {

	@Test
	public void testValidationError() {
		ValidationFieldError fieldError = new ValidationFieldError(RandomStringUtils.randomAlphanumeric(10),
		                                                           RandomStringUtils.randomAlphanumeric(10));
		ValidationError error = new ValidationError(Stream.of(fieldError)
		                                                  .collect(Collectors.toList()));
		assertNotNull(error);
		assertNotNull(error.getMessage());
		assertNotNull(error.getErrorType());
		assertNotNull(error.getStatusCode());
		assertNotNull(error.getTimestamp());
		assertNotNull(error.getValidationErrors());
	}

	@Test
	public void testServiceError() {
		ServiceError serviceError = new ServiceError(RandomStringUtils.randomAlphanumeric(10));
		assertNotNull(serviceError);
		assertNotNull(serviceError.getMessage());
		assertNotNull(serviceError.getStatusCode());

		serviceError =
				new ServiceError(RandomStringUtils.randomAlphanumeric(10), 1,
				                 RandomStringUtils.randomAlphanumeric(10));
		assertNotNull(serviceError);
		assertNotNull(serviceError.getMessage());
		assertNotNull(serviceError.getStatusCode());
		assertNotNull(serviceError.getErrorType());

		serviceError =
				new ServiceError(RandomStringUtils.randomAlphanumeric(10), 1, RandomStringUtils.randomAlphanumeric(10),
				                 HttpStatus.OK);
		serviceError.setMessage(RandomStringUtils.randomAlphanumeric(10));
		serviceError.setErrorType(RandomStringUtils.randomAlphanumeric(10));
		serviceError.setStatusCode(HttpStatus.OK.value());
		assertNotNull(serviceError);
		assertNotNull(serviceError.getMessage());
		assertNotNull(serviceError.getStatusCode());
		assertNotNull(serviceError.getErrorType());
		assertNotNull(serviceError.getTimestamp());
	}

}
