package com.sg.dayamed.errorhandling;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.rest.commons.IResponseCodes;
import com.sg.dayamed.rest.commons.RestApiException;
import com.sg.dayamed.rest.commons.RestFault;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.nio.file.AccessDeniedException;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestRestExceptionProcessor extends BaseRestApiTest {

	@InjectMocks
	private RestExceptionProcessor restExceptionProcessor;

	@Mock
	private MessageSource mockMessageSource;

	@Mock
	private ObjectMapper mockObjectMapper;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private ConstraintViolationException mockConstraintViolationException;

	@Mock
	private MethodArgumentNotValidException mockMethodArgumentNotValidException;

	@Mock
	private WebRequest mockWebRequest;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String SERVICE_EXCEPTION = "A service exception occurred";

	@Before
	public void setUp() throws Exception {
		ReflectionTestUtils.setField(restExceptionProcessor, "mapper", mockObjectMapper);
		Mockito.when(mockObjectMapper.writeValueAsString(Mockito.anyString()))
		       .thenReturn("some error");
	}

	@Test
	public void testHandleServiceException() {
		/**
		 * Http status is null
		 */
		Mockito.when(mockMessageSource.getMessage(Mockito.anyString(), Mockito.any(), Mockito.any()))
		       .thenReturn(SERVICE_EXCEPTION);
		ServiceException serviceException = new ServiceException(SERVICE_EXCEPTION, logger);
		ServiceError serviceError = restExceptionProcessor.handleServiceException(serviceException);
		assertNotNull(serviceError);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), serviceError.getStatusCode());
		assertEquals(SERVICE_EXCEPTION, serviceError.getMessage());

		/**
		 * Http status is 400
		 */
		serviceException = new ServiceException(SERVICE_EXCEPTION, HttpStatus.BAD_REQUEST, logger);
		serviceError = restExceptionProcessor.handleServiceException(serviceException);
		assertNotNull(serviceError);
		assertEquals(HttpStatus.BAD_REQUEST.value(), serviceError.getStatusCode());
		assertEquals(SERVICE_EXCEPTION, serviceError.getMessage());
	}

	@Test
	public void testRestApiException() throws Exception {
		/**
		 * Case 1 : RestApi Exception
		 */
		DataValidationException dve =
				new DataValidationException(APIErrorFields.AUTHORIZATION, APIErrorKeys.ERRORS_INVALID_ROLE);
		RestApiException restApiException = new RestApiException(dve);
		ResponseEntity<RestFault> responseEntity =
				restExceptionProcessor.handleException(restApiException, Locale.ENGLISH, mockHttpServletRequest);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		RestFault restFault = responseEntity.getBody();
		assertEquals(IResponseCodes.DATA_VALIDATION_ERROR, restFault.getResult());
		assertEquals(APIErrorKeys.ERRORS_INVALID_ROLE, restFault.getErrorKey());
		assertEquals(APIErrorFields.AUTHORIZATION, restFault.getFieldName());
		assertNotNull(restFault.getUid());
	}

	@Test
	public void testConstraintViolationException() {
		ResponseEntity<RestFault> responseEntity =
				restExceptionProcessor.handleConstraintViolation(mockConstraintViolationException,
				                                                 mockHttpServletRequest);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
		RestFault restFault = responseEntity.getBody();
		assertNotNull(restFault.getUid());
	}

	@Test
	public void testMissingServletRequestParameterException() {
		MissingServletRequestParameterException parameterException =
				new MissingServletRequestParameterException("key", "value");
		ResponseEntity<RestFault> responseEntity =
				restExceptionProcessor.handleConflict(parameterException, mockHttpServletRequest);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		RestFault restFault = responseEntity.getBody();
		assertEquals(IResponseCodes.DATA_VALIDATION_ERROR, restFault.getResult());
		assertEquals(IWSGlobalApiErrorKeys.ERRORS_MISSING_REQUEST_PARAMETER_FIELD, restFault.getErrorKey());
		assertTrue(StringUtils.containsIgnoreCase(restFault.getFieldName(), "key"));
		assertNotNull(restFault.getUid());
	}

	@Test
	public void testMethodArgumentNotValidException() {
		Mockito.when(mockMethodArgumentNotValidException.getBindingResult())
		       .thenReturn(new DirectFieldBindingResult(new Object(), "text"));
		ResponseEntity<RestFault> responseEntity =
				restExceptionProcessor.handleConflict(mockMethodArgumentNotValidException, mockHttpServletRequest);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
		RestFault restFault = responseEntity.getBody();
		assertNotNull(restFault.getUid());
	}

	@Test
	public void testAuthenticationException() {
		AuthenticationException exception = new AccountExpiredException("error", new RuntimeException());
		ServiceError serviceError = restExceptionProcessor.handleCustomException(exception, mockWebRequest);
		assertNotNull(serviceError);
		assertEquals(HttpStatus.FORBIDDEN.value(), serviceError.getStatusCode());
	}

	@Test
	public void testAccessDeniedException() {
		AccessDeniedException deniedException = new AccessDeniedException("error");
		ServiceError serviceError = restExceptionProcessor.handleCustomException(deniedException, mockWebRequest);
		assertNotNull(serviceError);
		assertEquals(HttpStatus.FORBIDDEN.value(), serviceError.getStatusCode());
	}

	@Test
	public void testThrowable() {
		RuntimeException runtimeException = new RuntimeException();
		ServiceError serviceError = restExceptionProcessor.unhandledError(runtimeException);
		assertNotNull(serviceError);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), serviceError.getStatusCode());
	}
}
