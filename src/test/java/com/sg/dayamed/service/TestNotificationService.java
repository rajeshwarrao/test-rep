package com.sg.dayamed.service;

import com.sg.dayamed.FcmNotification;
import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.NotificationReceivedUser;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.helper.PrescriptionHelper;
import com.sg.dayamed.helper.pojo.CaregiverDosageNotificationModel;
import com.sg.dayamed.notification.apns.ApnsNotification;
import com.sg.dayamed.pojo.APNSUpdateTokenRequestModel;
import com.sg.dayamed.pojo.CalendarNotificationModel;
import com.sg.dayamed.pojo.CalenderNotificationRequestModel;
import com.sg.dayamed.pojo.DosageNotificationModel;
import com.sg.dayamed.pojo.FcmNotificationRequestModel;
import com.sg.dayamed.pojo.NotificationModel;
import com.sg.dayamed.pojo.NotificationRequestModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.VideoNotificationRequestModel;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.repository.NotificationReceivedUserRepository;
import com.sg.dayamed.repository.NotificationRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.impl.NotificationServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import shaded.apache.commons.lang3.StringUtils;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({FcmNotification.class})
public class TestNotificationService extends BaseTestCase {

	@InjectMocks
	private NotificationService notificationService = new NotificationServiceImpl();

	@Mock
	private UserDetailsRepository mockUserDetailsRepository;

	@Mock
	private ApnsNotification mockApnsNotification;

	@Mock
	private NotificationRepository mockNotificationRepository;

	@Mock
	private SimpMessagingTemplate mockSimpMessagingTemplate;

	@Mock
	private NotificationReceivedUserRepository mockNotificationReceivedUserRepository;

	@Mock
	private DateUtility mockDateUtility;

	@Mock
	private FcmNotification mockFcmNotification;

	@Mock
	UserDetailsService userDetailsService;

	@Mock
	PatientService patientService;

	@Mock
	Utility mockUtility;

	@Mock
	PrescriptionHelper prescriptionHelper;

	@Mock
	CaregiverService mockCaregiverService;

	@Test
	public void testAddNotification() throws Exception {
		/**
		 * Case 1 : Android push notification.
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		userDetails.setToken(RandomStringUtils.randomAlphanumeric(30));
		userDetails.setType(ApplicationConstants.ANDROID);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		Boolean result = notificationService.addNotification(Stream.of(NumberUtils.LONG_ONE)
		                                                           .collect(Collectors.toList()), 1, "Test Message",
		                                                     "Test Title",
		                                                     ApplicationConstants.ANDROID);
		assertTrue(result);

		/**
		 * Case 2 : iOS push notification
		 */
		userDetails.setType(ApplicationConstants.IOS);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		result = notificationService.addNotification(Stream.of(NumberUtils.LONG_ONE)
		                                                   .collect(Collectors.toList()), 1, "Test Message",
		                                             "Test Title",
		                                             ApplicationConstants.IOS);
		assertTrue(result);

		/**
		 * Case 3 : WEB notification with recievingUseridlist not null.
		 */
		userDetails.setType(ApplicationConstants.WEB);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		result = notificationService.addNotification(Stream.of(NumberUtils.LONG_ONE)
		                                                   .collect(Collectors.toList()), 1, "Test Message",
		                                             "Test Title",
		                                             ApplicationConstants.WEB);
		assertTrue(result);

		/**
		 * Case 4 : WEB notification with recievingUseridlist null.
		 */
		result = notificationService.addNotification(null, 1, "Test Message", "Test Title",
		                                             ApplicationConstants.WEB);
		assertTrue(result);

		/**
		 * Case 5 : UserDetails not found
		 */
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		result = notificationService.addNotification(Stream.of(NumberUtils.LONG_ONE)
		                                                   .collect(Collectors.toList()), 1, "Test Message",
		                                             "Test Title",
		                                             ApplicationConstants.WEB);
		assertFalse(result);

		/**
		 * Case 6 : Exception is thrown
		 */
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		result = notificationService.addNotification(Stream.of(NumberUtils.LONG_ONE)
		                                                   .collect(Collectors.toList()), 1, "Test Message",
		                                             "Test Title",
		                                             ApplicationConstants.WEB);
		assertFalse(result);
	}

	@Test
	public void testSendMobileNotification() {
		/**
		 * Case 1 : device type Android
		 */
		String response =
				notificationService.sendMobileNotification(ApplicationConstants.ANDROID, "TestMessage", "Title",
				                                           UUID.randomUUID()
				                                               .toString());
		assertNotNull(response);
		assertEquals("Sent_Notification", response);

		/**
		 * Case 2 : device type iOS
		 */
		response = notificationService.sendMobileNotification(ApplicationConstants.IOS, "TestMessage", "Title",
		                                                      UUID.randomUUID()
		                                                          .toString());
		assertNotNull(response);
		assertEquals("Sent_Notification", response);

		/**
		 * Case 3 : Exception case
		 */
		Mockito.doThrow(new RuntimeException())
		       .when(mockApnsNotification)
		       .pushApnsNotification(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
		response = notificationService.sendMobileNotification(ApplicationConstants.IOS, "TestMessage", "Title",
		                                                      UUID.randomUUID()
		                                                          .toString());
		assertEquals(StringUtils.EMPTY, response);
	}

	@Test
	public void testSendMobileNotificationForUserId() {
		/**
		 * Case 1 : For Android
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		userDetails.setToken(RandomStringUtils.randomAlphanumeric(30));
		userDetails.setType(ApplicationConstants.ANDROID);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		String response = notificationService.sendMobileNotification(1, "Test Message", "Title");
		assertNotNull(response);
		assertEquals("Sent_Notification", response);

		/**
		 * Case 2 : For iOS
		 */
		userDetails.setType(ApplicationConstants.IOS);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		response = notificationService.sendMobileNotification(1, "Test Message", "Title");
		assertNotNull(response);
		assertEquals("Sent_Notification", response);

		/**
		 * Case 3 : For empty token
		 */
		userDetails.setType(null);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		response = notificationService.sendMobileNotification(1, "Test Message", "Title");
		assertNotNull(response);
		assertEquals("Token_is_Empty", response);

		/**
		 * Case 4 : Exception is occurred.
		 */
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		response = notificationService.sendMobileNotification(1, "Test Message", "Title");
		assertNotNull(response);
		assertEquals(StringUtils.EMPTY, response);
	}

	@Test
	public void testSendMobileVideoCallNotification() {
		/**
		 * Case 1 : Video calling for Android
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		userDetails.setToken(RandomStringUtils.randomAlphanumeric(30));
		userDetails.setType(ApplicationConstants.ANDROID);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		String response =
				notificationService.sendMobileVidoeCallNotification("1", "Test Message", ApplicationConstants.CALLING,
				                                                    "Test Room",
				                                                    "Junit", null);
		assertNotNull(response);
		assertEquals(ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION, response);

		/**
		 * Case 2 : Video Calling for IOS
		 */
		userDetails.setToken(RandomStringUtils.randomAlphanumeric(30));
		userDetails.setType(ApplicationConstants.IOS);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		response =
				notificationService.sendMobileVidoeCallNotification("1", "Test Message", ApplicationConstants.CALLING,
				                                                    "Test Room",
				                                                    "Junit", null);
		assertNotNull(response);
		assertEquals(ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION, response);

		/**
		 * Case 3 : Token is null.
		 */
		userDetails.setToken(null);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		response =
				notificationService.sendMobileVidoeCallNotification("1", "Test Message", ApplicationConstants.CALLING,
				                                                    "Test Room",
				                                                    "Junit", null);
		assertNotNull(response);
		assertEquals(ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_TOKEN_IS_EMPTY, response);

		/**
		 * Case 4 : Exception is thrown
		 */
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		response =
				notificationService.sendMobileVidoeCallNotification("1", "Test Message", ApplicationConstants.CALLING,
				                                                    "Test Room",
				                                                    "Junit", null);
		assertEquals(StringUtils.EMPTY, response);

		/**
		 * Case 5 : Not video call
		 */
		Mockito.reset(mockUserDetailsRepository);
		userDetails.setToken(RandomStringUtils.randomAlphanumeric(30));
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		response = notificationService.sendMobileVidoeCallNotification("1", "Test Message", "Test", "Test Room",
		                                                               "Junit", null);
		assertNotNull(response);
		assertEquals(ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION, response);
	}

	@Test
	public void testFetchNotificationModelsByNotificationType() throws Exception {
		Notification notification = TestResponseGenerator.generateNotification();
		NotificationModel notificationModel =
				generateRequestObject("notificationModel", NotificationModel.class, ADHERENCE_DATA_POINTS_FILE);
		List<Notification> expectedNotifications = Stream.of(notification)
		                                                 .collect(Collectors.toList());
		Mockito.when(mockNotificationRepository.findBytype(Mockito.anyString()))
		       .thenReturn(expectedNotifications);
		Mockito.when(mockUtility.convertNotificationEntityToModel(Mockito.any()))
		       .thenReturn(notificationModel);
		List<NotificationModel> notificationModelList =
				notificationService.fetchNotificationModelsByNotificationType("test");
		assertNotNull(notificationModelList);
	}

	@Test
	public void testFetchByNotificationType() {
		Notification notification = TestResponseGenerator.generateNotification();
		List<Notification> expectedNotifications = Stream.of(notification)
		                                                 .collect(Collectors.toList());
		Mockito.when(mockNotificationRepository.findBytype(Mockito.anyString()))
		       .thenReturn(expectedNotifications);
		List<Notification> actualNotifications = notificationService.fetchByNotificationType("1");
		assertTrue(CollectionUtils.isNotEmpty(actualNotifications));
		assertEquals(expectedNotifications, actualNotifications);
	}

	@Test
	public void testAddWebNotificationWithNotification() {
		/**
		 * Case 1 : Video call signature is passed
		 */
		Notification notification = TestResponseGenerator.generateNotification();
		VidoeCallSignatureModel model = TestResponseGenerator.generateVidoeCallSignatureModel();
		notification.setVidoeCallSignatureModel(model);
		Mockito.when(mockNotificationRepository.save(Mockito.any()))
		       .thenReturn(notification);
		notificationService.addWebNotification(notification);
		//assertNotNull(actualNotification);

		/**
		 * Case 2 : Video call signature is not passed
		 */
		notification.setVidoeCallSignatureModel(null);
		Notification actualNotification = notificationService.addWebNotification(notification);
		assertNotNull(actualNotification);
		assertEquals(notification, actualNotification);
	}

	@Test
	public void testAddWebNotificationWithUserList() {
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		Mockito.when(mockNotificationRepository.save(Mockito.any()))
		       .thenReturn(expectedNotification);
		Notification notification = notificationService.addWebNotification(Stream.of(NumberUtils.LONG_ONE)
		                                                                         .collect(Collectors.toList()), 2,
		                                                                   "Test Message", "JUnit",
		                                                                   ApplicationConstants.ANDROID);
		assertNotNull(notification);
		assertEquals(expectedNotification, notification);
	}

	@Test
	public void testAddWebNotificationWithUserId() {
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		Mockito.when(mockNotificationRepository.save(Mockito.any()))
		       .thenReturn(expectedNotification);
		Notification notification =
				notificationService.addWebNotification(2, "Test Message", "JUnit", ApplicationConstants.ANDROID);
		assertNotNull(notification);
		assertEquals(expectedNotification, notification);
	}

	@Test
	public void testFetchAllNotifications() {
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		List<Notification> expectedNotifications = Stream.of(expectedNotification)
		                                                 .collect(Collectors.toList());
		Mockito.when(mockNotificationRepository.findAll())
		       .thenReturn(expectedNotifications);
		List<Notification> actualNotifications = notificationService.fetchAllNotifications();
		assertTrue(CollectionUtils.isNotEmpty(actualNotifications));
		assertEquals(expectedNotifications, actualNotifications);
	}

	@Test
	public void testDeleteNotificationById() {
		/**
		 * Case 1: Not found
		 */
		String response = notificationService.deleteNotificationById(1);
		assertNotNull(response);
		assertEquals("notfound", response);

		/**
		 * Case 2 : success case
		 */
		Mockito.when(mockNotificationRepository.existsById(Mockito.anyLong()))
		       .thenReturn(true);
		response = notificationService.deleteNotificationById(1);
		assertNotNull(response);
		assertEquals("success", response);
	}

	@Test
	public void testSendTwilioVideoFCMNotification() {
		/**
		 * Case 1 : Title As Calling with User Token
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		userDetails.setToken(RandomStringUtils.randomAlphanumeric(30));
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		String response =
				notificationService.sendTwilioVideoFCMNotification("1", "TestMessage", ApplicationConstants.CALLING,
				                                                   "Test Room", "Junit");
		assertNotNull(response);
		assertEquals(ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION, response);

		/**
		 * Case 2 : Title as Not Calling with User Token
		 */
		response = notificationService.sendTwilioVideoFCMNotification("1", "TestMessage", ApplicationConstants.ANDROID,
		                                                              "Test Room", "Junit");
		assertNotNull(response);
		assertEquals(ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION, response);

		/**
		 * Case 3 : With out User Token.
		 */
		userDetails.setToken(null);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		response = notificationService.sendTwilioVideoFCMNotification("1", "TestMessage", ApplicationConstants.ANDROID,
		                                                              "Test Room", "Junit");
		assertNotNull(response);
		assertEquals(ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_TOKEN_IS_EMPTY, response);

		/**
		 * Case 4 : Exception is thrown
		 */
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		response = notificationService.sendTwilioVideoFCMNotification("1", "TestMessage", ApplicationConstants.ANDROID,
		                                                              "Test Room", "Junit");
		assertNotNull(response);
		assertEquals(ApplicationConstants.EMPTY_STRING, response);
	}

	@Test
	public void testFetchNotificationsByUserIdAndType() {
		/**
		 * Case 1 : <60000 millis
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Notification notification = TestResponseGenerator.generateNotification();
		NotificationReceivedUser receivedUser =
				TestResponseGenerator.generateNotificationReceivedUser(notification, userDetails);
		Mockito.when(
				mockNotificationReceivedUserRepository.findTop1ByuserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc(
						Mockito.anyLong(),
						Mockito.anyString(), Mockito.anyBoolean()))
		       .thenReturn(receivedUser);
		Mockito.when(
				mockNotificationReceivedUserRepository.findByuserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc(
						Mockito.anyLong(),
						Mockito.anyString(), Mockito.anyBoolean()))
		       .thenReturn(Stream.of(receivedUser)
		                         .collect(Collectors.toList()));
		LocalDateTime dateTime = LocalDateTime.now()
		                                      .plusHours(-5)
		                                      .plusMinutes(-10);
		Mockito.when(mockDateUtility.convertDateObjectToLocalDateTimeObject(Mockito.any()))
		       .thenReturn(dateTime);
		Notification actualNotification = notificationService.fetchNotificationsByUseridAndType(1, "Test");
		assertNotNull(actualNotification);
		assertEquals(notification, actualNotification);

		/**
		 * Case 2 : > 60000 millis
		 */
		dateTime = LocalDateTime.now()
		                        .plusHours(-8);
		Mockito.when(mockDateUtility.convertDateObjectToLocalDateTimeObject(Mockito.any()))
		       .thenReturn(dateTime);
		actualNotification = notificationService.fetchNotificationsByUseridAndType(1, "Test");
		assertNull(actualNotification);

		/**
		 * Case 3 : Exception is thrown.
		 */
		Mockito.when(
				mockNotificationReceivedUserRepository.findTop1ByuserDetails_idAndNotification_typeAndNotification_deleteflagOrderByIdDesc(
						Mockito.anyLong(),
						Mockito.anyString(), Mockito.anyBoolean()))
		       .thenThrow(new RuntimeException());
		actualNotification = notificationService.fetchNotificationsByUseridAndType(1, "Test");
		assertNull(actualNotification);
	}

	@Test
	public void testFetchNotificationsByUserId() {
		//notificationService.fetchNotificationsByUserId(1);
	}

	@Test
	public void testFetchNotificationById() {
		/**
		 * Case 1 : Non null response from repository
		 */
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		Mockito.when(mockNotificationRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(expectedNotification));
		Notification actualNotification = notificationService.fetchNotificationById(1);
		assertNotNull(actualNotification);
		;
		assertEquals(expectedNotification, actualNotification);

		/**
		 * Case 2 : Null response from repository
		 */
		Mockito.when(mockNotificationRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualNotification = notificationService.fetchNotificationById(1);
		assertNull(actualNotification);
		;
	}

	@Test
	public void testFetchNotificationsByBelongigngUserId() {
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		List<Notification> expectedNotifications = Stream.of(expectedNotification)
		                                                 .collect(Collectors.toList());
		Mockito.when(mockNotificationRepository.findBybelongigngUserDetails_id(Mockito.anyLong()))
		       .thenReturn(expectedNotifications);
		List<Notification> actualNotifications = notificationService.fetchNotificationsByBelongingUserId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualNotifications));
		assertEquals(expectedNotifications, actualNotifications);
	}

	@Test
	public void testFetchNotificationsByUserIdBetweenDates() {
		/**
		 * Case 1 : With out exception.
		 */
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		List<Notification> expectedNotifications = Stream.of(expectedNotification)
		                                                 .collect(Collectors.toList());
		Mockito.when(
				mockNotificationRepository.findByTimeBetweenAndBelongigngUserDetails_id(Mockito.any(), Mockito.any(),
				                                                                        Mockito.anyLong()))
		       .thenReturn(expectedNotifications);
		List<Notification> actualNotifications =
				notificationService.fetchNotificationsByUseridBetweenDates(new Date(), new Date(), 1);
		assertTrue(CollectionUtils.isNotEmpty(actualNotifications));
		assertEquals(expectedNotifications, actualNotifications);

		/**
		 * Case 2 : With Exception
		 */
		Mockito.when(
				mockNotificationRepository.findByTimeBetweenAndBelongigngUserDetails_id(Mockito.any(), Mockito.any(),
				                                                                        Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		actualNotifications = notificationService.fetchNotificationsByUseridBetweenDates(new Date(), new Date(), 1);
		assertFalse(CollectionUtils.isNotEmpty(actualNotifications));
	}

	@Test
	public void testDeleteNotifications() {
		notificationService.deleteNotifications(Collections.emptyList());
	}

	@Test
	public void testGetNotificationByRoomName() {
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		Mockito.when(mockNotificationRepository.findByroomName(Mockito.eq("test")))
		       .thenReturn(expectedNotification);
		Notification actualNotification = notificationService.getNotificationByRoomName("test");
		assertNotNull(actualNotification);
		assertEquals(expectedNotification, actualNotification);
	}

	@Test
	public void testSaveNotification() {
		Notification expectedNotification = TestResponseGenerator.generateNotification();
		Mockito.when(mockNotificationRepository.save(Mockito.any()))
		       .thenReturn(expectedNotification);
		Notification actualNotification = notificationService.saveNotification(expectedNotification);
		assertNotNull(actualNotification);
		assertEquals(expectedNotification, actualNotification);
	}

	@Test
	public void testCalenderNotificationsWithOutScheduledByPatientId() throws Exception {
		CalenderNotificationRequestModel calenderNotificationRequestModel = new CalenderNotificationRequestModel();
		calenderNotificationRequestModel.setZoneId("Asia/Kolkata");
		calenderNotificationRequestModel.setStartDate("03/15/2019");
		calenderNotificationRequestModel.setEndDate("09/15/2019");
		Mockito.when(mockDateUtility.convertLocalTimeToUTCTimeObjectByzoneid(Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(LocalDateTime.now());
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(userDetailsService.fetchUserDetailEntitiysById(Mockito.anyLong()))
		       .thenReturn(userDetails);
		List<Notification> notificationList = Stream.of(TestResponseGenerator.generateNotification())
		                                            .collect(
				                                            Collectors.toList());
		Mockito.when(notificationService
				             .fetchNotificationsByUseridBetweenDates(Mockito.any(), Mockito.any(), Mockito.anyLong()))
		       .thenReturn(notificationList);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(patientService.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(TestResponseGenerator.generatePatientModel());
		CalendarNotificationModel calendarNotificationModel =
				notificationService.calenderNotificationsWithOutScheduledByPatientId(1l,
				                                                                     calenderNotificationRequestModel);
		assertTrue(calendarNotificationModel != null);
	}

	@Test
	public void testCalenderNotificationsByPatientId() throws Exception {
		CalenderNotificationRequestModel calenderNotificationRequestModel = new CalenderNotificationRequestModel();
		calenderNotificationRequestModel.setZoneId("Asia/Kolkata");
		calenderNotificationRequestModel.setStartDate("03/15/2019");
		calenderNotificationRequestModel.setEndDate("09/15/2019");
		Mockito.when(mockDateUtility.convertLocalTimeToUTCTimeObjectByzoneid(Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(LocalDateTime.now());
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(userDetailsService.fetchUserDetailEntitiysById(Mockito.anyLong()))
		       .thenReturn(userDetails);
		List<Notification> notificationList = Stream.of(TestResponseGenerator.generateNotification())
		                                            .collect(
				                                            Collectors.toList());
		Mockito.when(notificationService
				             .fetchNotificationsByUseridBetweenDates(Mockito.any(), Mockito.any(), Mockito.anyLong()))
		       .thenReturn(notificationList);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(patientService.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(TestResponseGenerator.generatePatientModel());
		List<PrescriptionSchedule> prescriptionScheduleList =
				Stream.of(TestResponseGenerator.generatePrescriptionSchedule())
				      .collect(
						      Collectors.toList());
		Mockito.when(
				prescriptionHelper.fetchPrescriptionSchedulesBetweenActaulDatesByUserId(Mockito.any(), Mockito.any(),
				                                                                        Mockito.anyLong()))
		       .thenReturn(prescriptionScheduleList);
		Mockito.when(mockDateUtility.convertUTCLocalDateTimeObjectToZonedLocalDateTimeByZoneID(Mockito.anyString(),
		                                                                                       Mockito.anyString(),
		                                                                                       Mockito.anyString()))
		       .thenReturn(new Date().toString());
		CalendarNotificationModel calendarNotificationModel =
				notificationService.calenderNotificationsByPatientId(1l, calenderNotificationRequestModel);
		assertTrue(calendarNotificationModel != null);
	}

	@Test
	public void testSendVideoNotificationByUserId() throws Exception {
		VideoNotificationRequestModel videoNotificationRequestModel =
				TestResponseGenerator.generateVideoNotificationRequestModel();
		UserDetails belongingUserDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT);
		Optional<UserDetails> optionalUserDetails = Optional.of(belongingUserDetails);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalUserDetails);
		String status = notificationService.sendVideoNotificationByUserId(videoNotificationRequestModel);
		assertNotNull(status);
		assertEquals(status, ApplicationConstants.SUCCESS);
		/*
		 * if user cargiver
		 */
		UserDetails belongingUserDetailsCar =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.CAREGIVER);
		Optional<UserDetails> optionalUserDetailsCar = Optional.of(belongingUserDetailsCar);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalUserDetailsCar);
		Caregiver caregiver =
				TestResponseGenerator.generateCaregiver("Male", "Asian", ApplicationConstants.CAREGIVER, true);
		caregiver.getUserDetails()
		         .setType(ApplicationConstants.ANDROID);
		Mockito.when(mockCaregiverService.fetchCaregiverByUserId(Mockito.anyLong()))
		       .thenReturn(caregiver);
		String statusCar = notificationService.sendVideoNotificationByUserId(videoNotificationRequestModel);
		assertNotNull(statusCar);
		assertEquals(statusCar, ApplicationConstants.SUCCESS);
	}

	@Test
	public void testSendFcmNotification() throws Exception {
		FcmNotificationRequestModel fcmNotificationRequestModel =
				TestResponseGenerator.generateFcmNotificationRequestModel();
		UserDetails belongingUserDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT);
		belongingUserDetails.setType(ApplicationConstants.ANDROID);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(belongingUserDetails);
		Optional<UserDetails> optionalUserDetailsCar = Optional.of(belongingUserDetails);
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalUserDetailsCar);
		String statusCar = notificationService.sendFcmNotification(fcmNotificationRequestModel);
		assertNotNull(statusCar);
		assertEquals(statusCar, ApplicationConstants.SUCCESS);
	}

	@Test
	public void testUpdateTokenInUserDetails() throws Exception {
		APNSUpdateTokenRequestModel aPNSUpdateTokenRequestModel =
				TestResponseGenerator.generateAPNSUpdateTokenRequestModel();
		UserDetailsModel userDetailsModel =
				TestResponseGenerator.generateUserDetailsModel("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(userDetailsService.fetchUserDetailsByToken(Mockito.anyString()))
		       .thenReturn(Stream.of(userDetailsModel)
		                         .collect(
				                         Collectors.toList()));
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any()))
		       .thenReturn(userDetailsModel);
		Mockito.when(userDetailsService.updateUserDetails(Mockito.any()))
		       .thenReturn(userDetailsModel);
		UserDetailsModel resUserDetailsModel =
				notificationService.updateTokenInUserDetails(aPNSUpdateTokenRequestModel);
		assertNotNull(resUserDetailsModel);
		assertEquals(resUserDetailsModel, userDetailsModel);
	}

	@Test
	public void testAddNotificationFromController() throws Exception {
		Optional<UserDetails> optionalBelongingUserDetails =
				Optional.of(TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT));
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalBelongingUserDetails);
		NotificationRequestModel notificationRequestModel = TestResponseGenerator.generateNotificationRequestModel();
		Notification notification = TestResponseGenerator.generateNotification();
		Mockito.when(mockNotificationRepository.save(Mockito.any()))
		       .thenReturn(notification);
		NotificationModel notificationModel =
				generateRequestObject("notificationModel", NotificationModel.class, ADHERENCE_DATA_POINTS_FILE);
		Mockito.when(mockUtility.convertNotificationEntityToModel(Mockito.any()))
		       .thenReturn(notificationModel);
		NotificationModel acctualNotificationModel =
				notificationService.addNotificationFromController(notificationRequestModel);
		assertNotNull(acctualNotificationModel);
		assertEquals(acctualNotificationModel, notificationModel);
	}

	@Mock
	DosageInfoService mockDosageInfoService;

	@Mock
	DosageNotificationRepository mockDosageNotificationRepository;

	@Test
	public void testAddCaregiverNotifications() throws Exception {
		CaregiverDosageNotificationModel caregiverDosageNotificationModel =
				TestResponseGenerator.generateCaregiverDosageNotificationModel();
		Mockito.when(mockDosageInfoService
				             .fetctDosageInfoByID(Mockito.anyLong()))
		       .thenReturn(TestResponseGenerator.generateDosageInfo(true));
		DosageNotification dosageNotification = TestResponseGenerator.generateDosageNotification();
		Mockito.when(mockDosageNotificationRepository
				             .findByDosageInfo_idAndUserId(Mockito.anyLong(), Mockito.anyLong()))
		       .thenReturn(Stream.of(dosageNotification)
		                         .collect(
				                         Collectors.toList()));
		DosageNotification dosageNotification2 = TestResponseGenerator.generateDosageNotification();
		dosageNotification2.setId(0);
		Mockito.when(mockUtility.convertDosageNotificationModelToEntity(Mockito.any()))
		       .thenReturn(dosageNotification2);
		CaregiverDosageNotificationModel resCaregiverDosageNotificationModel =
				notificationService.addCaregiverNotifications(caregiverDosageNotificationModel);
		assertNotNull(resCaregiverDosageNotificationModel);
		assertEquals(resCaregiverDosageNotificationModel, caregiverDosageNotificationModel);
	}

	@Test
	public void testFetchDosageNotificationsByCaregiverId() throws Exception {
		List<DosageNotification> dosageNotifications = Stream.of(TestResponseGenerator.generateDosageNotification())
		                                                     .collect(
				                                                     Collectors.toList());
		Mockito.when(mockDosageNotificationRepository
				             .findByDosageInfo_idAndUserId(Mockito.anyLong(), Mockito.anyLong()))
		       .thenReturn(dosageNotifications);
		DosageNotificationModel dosageNotificationModel = TestResponseGenerator.generateDosageNotificationModel();
		Mockito.when(mockUtility.convertDosageNotificationEntityToModel(Mockito.any()))
		       .thenReturn(dosageNotificationModel);
		List<DosageNotificationModel> dosageNotificationModelList = Stream.of(dosageNotificationModel)
		                                                                  .collect(Collectors.toList());
		List<DosageNotificationModel> resultdosageNotificationModelList =
				notificationService.fetchDosageNotificationsByCaregiverId(1L, 1L);
		assertNotNull(resultdosageNotificationModelList);
		assertEquals(resultdosageNotificationModelList, dosageNotificationModelList);
	}

}
