package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.service.impl.NotificationScheduleServiceImpl;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestNotificationScheduleService extends BaseTestCase {

	@InjectMocks
	private NotificationScheduleService notificationScheduleService = new NotificationScheduleServiceImpl();

	@Mock
	private NotificationScheduleRepository mockNotificationScheduleRepository;

	@Mock
	private DateUtility mockDateUtility;

	@Mock
	private DataSource mockDataSource;

	@Mock
	private JdbcTemplate mockJdbcTemplate;

	@Test
	public void testDeleteBulkNotificationSchedule() {
		//notificationScheduleService.deletebulkNotificationSchdule(Stream.of(new BigInteger("1")).collect(Collectors
		// .toList()));
	}

	@Test
	public void testupdateNotificationSchedulerJobByDosageInfo() {
		NotificationSchedule expectedNotficationSchedule = TestResponseGenerator.generateEmailNotificationSchedule();
		NotificationSchedule expectedNotficationSchedulewitStatus =
				TestResponseGenerator.generateEmailNotificationSchedule();
		expectedNotficationSchedulewitStatus.setConsumptionStatus(Stream.of("delayed", "consumed")
		                                                                .collect(Collectors.toList()));
		List<NotificationSchedule> expectedNotificationScheduleList =
				Stream.of(expectedNotficationSchedule, expectedNotficationSchedulewitStatus)
				      .collect(Collectors.toList());
		Mockito.when(
				mockNotificationScheduleRepository.findByActualDateAndDosageInfo_id(Mockito.any(), Mockito.anyLong()))
		       .thenReturn(expectedNotificationScheduleList);
		//Mockito.when(mockNotificationScheduleRepository.findByActualDateAndDosageInfo_id(Mockito.any(),Mockito
		// .anyLong())).thenThrow(new RuntimeException());
		LocalDateTime ldtPrescribedDatetimeInUTC = LocalDateTime.now();
		//notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(ldtPrescribedDatetimeInUTC, Mockito
		// .anyLong(), Mockito.anyString());
		assertEquals(true,
		             notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(ldtPrescribedDatetimeInUTC,
		                                                                                    Mockito.anyLong(),
		                                                                                    Mockito.anyString()));
	}

	@Test
	public void testupdateNotificationSchedulerJobByDosageInfoError() {
		Mockito.when(
				mockNotificationScheduleRepository.findByActualDateAndDosageInfo_id(Mockito.any(), Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		LocalDateTime ldtPrescribedDatetimeInUTC = LocalDateTime.now();
		assertEquals(false,
		             notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(ldtPrescribedDatetimeInUTC,
		                                                                                    Mockito.anyLong(),
		                                                                                    Mockito.anyString()));
	}
}
