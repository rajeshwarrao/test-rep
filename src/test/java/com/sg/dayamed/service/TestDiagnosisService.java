package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Diagnosis;
import com.sg.dayamed.repository.DiagnosisRepository;
import com.sg.dayamed.service.impl.DiagnosisServiceImpl;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class TestDiagnosisService extends BaseTestCase {

	@InjectMocks
	DiagnosisService mockDiagnosisService = new DiagnosisServiceImpl();

	@Mock
	DiagnosisRepository mockDiagnosisRepository;

	@Test
	public void testFetchDiagnosisIntelligenceByDescription() throws  Exception {
		List<Diagnosis> diagnosisList = Stream.of(TestResponseGenerator.generateDiagnosis())
		                                      .collect(Collectors.toList());
		Mockito.when(mockDiagnosisRepository.findByNameIgnoreCaseContaining(Mockito.anyString()))
		       .thenReturn(diagnosisList);
		List<Diagnosis> actualDiagnosisList = mockDiagnosisService.fetchDiagnosisIntelligenceByDescription("testDa");
		assertNotNull(actualDiagnosisList);
		assertEquals(actualDiagnosisList, diagnosisList);
	}

	@Test
	public void testAddDiagnosis() {
		Diagnosis diagnosis = TestResponseGenerator.generateDiagnosis();
		Mockito.when(mockDiagnosisRepository.save(Mockito.any()))
		       .thenReturn(diagnosis);
		Diagnosis responseDiagnosis = mockDiagnosisService.addDiagnosis(diagnosis);
		assertNotNull(responseDiagnosis);
		assertEquals(responseDiagnosis, diagnosis);
	}
}
