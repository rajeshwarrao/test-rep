package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.ConfigParams;
import com.sg.dayamed.repository.ConfigParamRepository;
import com.sg.dayamed.service.impl.LoadConfigParamsOnRedisImpl;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestLoadConfigParamsOnRedis extends BaseTestCase {

	@InjectMocks
	private LoadConfigParamsOnRedis loadConfigParamsOnRedis = new LoadConfigParamsOnRedisImpl();

	@Mock
	private ConfigParamRepository mockConfigParamRepository;

	@Mock
	private RediseService mockRediseService;

	@Test
	public void testLoadConfigParamsOnRedis() throws Exception {
		List<ConfigParams> expectedConfigParams =
				Stream.of(TestResponseGenerator.generateConfigParams(), TestResponseGenerator.generateConfigParams())
				      .collect(Collectors.toList());
		Mockito.when(mockConfigParamRepository.findAll())
		       .thenReturn(expectedConfigParams);
		loadConfigParamsOnRedis.loadConfigParamsOnRedis();
	}
}
