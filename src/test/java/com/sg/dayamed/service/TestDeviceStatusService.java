package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.DeviceStatus;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.DeviceStatusRequestModel;
import com.sg.dayamed.repository.DeviceStatusRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.impl.DeviceStatusServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestDeviceStatusService extends BaseTestCase {

	@InjectMocks
	private DeviceStatusService deviceStatusService = new DeviceStatusServiceImpl();

	@Mock
	private DeviceStatusRepository mockDeviceStatusRepository;

	@Mock
	UserDetailsRepository userDetailsRepository;

	@Test
	public void testAddDeviceStatus() throws Exception {
		DeviceStatusRequestModel deviceStatusRequestModel = new DeviceStatusRequestModel();
		deviceStatusRequestModel.setBelongingUserId(1l);
		DeviceStatus deviceStatus = TestResponseGenerator.generateDeviceStatus("Active");
		Mockito.when(mockDeviceStatusRepository.save(Mockito.any()))
		       .thenReturn(deviceStatus);
		Mockito.when(mockDeviceStatusRepository.findByUserDetails_id(Mockito.anyLong())).thenReturn(null);
		Optional<UserDetails> optionalUserDetails = Optional.of(TestResponseGenerator.generateUserDetails("Male",
		                                                                                                  "Asian",
		                                                                                                  ApplicationConstants.PATIENT));
		Mockito.when(userDetailsRepository.findById(Mockito.anyLong())).thenReturn(optionalUserDetails);
		DeviceStatus actualDeviceStatus = deviceStatusService.addDeviceStatus(deviceStatusRequestModel);
		assertNotNull(actualDeviceStatus);
		assertEquals(deviceStatus, actualDeviceStatus);
	}

	@Test
	public void testFetchDeviceStatusById() {
		/**
		 * Case 1 : Non null from repository
		 */
		DeviceStatus deviceStatus = TestResponseGenerator.generateDeviceStatus("Active");
		Mockito.when(mockDeviceStatusRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(deviceStatus));
		DeviceStatus actualDeviceStatus = deviceStatusService.fetchDeviceStatusById(1);
		assertNotNull(actualDeviceStatus);
		assertEquals(deviceStatus, actualDeviceStatus);

		/**
		 * Case 2 : null from repository
		 */
		Mockito.when(mockDeviceStatusRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualDeviceStatus = deviceStatusService.fetchDeviceStatusById(1);
		assertNull(actualDeviceStatus);
	}

	@Test
	public void testDeleteDeviceStatus() {
		deviceStatusService.deleteDeviceStatus(1);
	}

	@Test
	public void testFetchDeviceStatusByUserID() {
		DeviceStatus deviceStatus = TestResponseGenerator.generateDeviceStatus("Active");
		Mockito.when(mockDeviceStatusRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(deviceStatus);
		DeviceStatus actualDeviceStatus = deviceStatusService.fetchDeviceStatusByUserID(1);
		assertNotNull(actualDeviceStatus);
		assertEquals(deviceStatus, actualDeviceStatus);
	}
}
