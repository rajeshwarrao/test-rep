package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.DiseaseInfo;
import com.sg.dayamed.entity.DosageDevice;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionSchedule;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.repository.ConsumerGoodsInfoRepository;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.repository.DeviceNotificationRepository;
import com.sg.dayamed.repository.DiseaseInfoRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.DosageDeviceRepository;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.service.impl.PatientServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 06/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPatientService extends BaseTestCase {

	@InjectMocks
	private PatientService patientService = new PatientServiceImpl();

	@Mock
	private PatientRepository mockPatientRepository;

	@Mock
	private ProviderRepository mockProviderRepository;

	@Mock
	private ImanticService mockImanticService;

	@Mock
	private PasswordEncryption mockPasswordEncryption;

	@Mock
	private SendResetPwdNotification mockSendResetPwdNotification;

	@Mock
	private NotificationService mockNotificationService;

	@Mock
	private EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	private DiseaseRepository mockDiseaseRepository;

	@Mock
	private DosageInfoRepository mockDosageInfoRepository;

	@Mock
	private DeviceInfoRepository mockDeviceInfoRepository;

	@Mock
	private DiseaseInfoRepository mockDiseaseInfoRepository;

	@Mock
	private ConsumerGoodsInfoRepository mockConsumerGoodsInfoRepository;

	@Mock
	private DosageNotificationRepository mockDosageNotificationRepository;

	@Mock
	private DeviceNotificationRepository mockDeviceNotificationRepository;

	@Mock
	private PrescriptionRepository mockPrescriptionRepository;

	@Mock
	private PrescriptionScheduleService mockPrescriptionScheduleService;

	@Mock
	private NotificationScheduleService mockNotificationScheduleService;

	@Mock
	private PrescriptionScheduleRepository mockPrescriptionScheduleRepository;

	@Mock
	private NotificationScheduleRepository mockNotificationScheduleRepository;

	@Mock
	private DosageDeviceRepository mockDosageDeviceRepository;

	@Mock
	private Utility mockUtility;

	@Mock
	MessageNotificationConstants mockMessageNotificationConstants;

	private Patient patient;

	UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");

	@Before
	public void setUp() {
		patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn(UUID.randomUUID()
		                       .toString());
		Mockito.when(mockEmailAndSmsUtility.sendEmail(Mockito.anyString(), Mockito.anyString(), Mockito.anyList()))
		       .thenReturn(true);
		Mockito.when(mockEmailAndSmsUtility.sendSms(Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(true);
		Mockito.doNothing()
		       .when(mockSendResetPwdNotification)
		       .sendPwdResetNotificationWhenRegst(Mockito.any(), Mockito.anyString());
		Mockito.when(
				mockNotificationService.sendMobileNotification(Mockito.any(), Mockito.anyString(), Mockito.anyString(),
				                                               Mockito.any()))
		       .thenReturn("Success");
	}

	@Test
	public void testDeletePatientByProcedure() {
		/**
		 * Case 1 : Success in deletion of Imantic user.
		 */
		Mockito.when(mockPatientRepository.deletePatient(Mockito.anyLong()))
		       .thenReturn("success");
		Mockito.when(mockImanticService.deletePatientByImanticId(Mockito.anyString()))
		       .thenReturn(true);
		String result = patientService.deletePatientByProcedure(1);
		assertNotNull(result);
		assertEquals("success", result);

		/**
		 * Case 2 : Failed to delete imatic data
		 */
		Mockito.when(mockImanticService.deletePatientByImanticId(Mockito.anyString()))
		       .thenReturn(false);
		result = patientService.deletePatientByProcedure(1);
		assertNotNull(result);
		assertEquals("success", result);
	}

	@Test
	public void testDeletePatient() {
		/**
		 * Case 1 : Success in deletion of Imantic user.
		 */
		Mockito.when(mockImanticService.deletePatientByImanticId(Mockito.anyString()))
		       .thenReturn(true);
		String result = patientService.deletePatient(1);
		assertNotNull(result);
		assertEquals("success", result);

		/**
		 * Case 2 : Failed to delete imatic data
		 */
		Mockito.when(mockImanticService.deletePatientByImanticId(Mockito.anyString()))
		       .thenReturn(false);
		result = patientService.deletePatient(1);
		assertNotNull(result);
		assertEquals("success", result);
	}

	// @Test
	// public void testAddPatient() throws Exception {
	//     /**
	//      * Case 1 : Imatic user found.
	//      */
	//     Map<String, String> imaticsMap = TestResponseGenerator.generateImaticMap();
	//     Map<String, String> analyticsMap = TestResponseGenerator.generateImaticAnalyticMap();
	//     Mockito.when(mockImanticService.createPatientInImantics(Mockito.any())).thenReturn(imaticsMap);
	//     Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString())).thenReturn(analyticsMap);
	//     Mockito.when(mockUtility.addDaysToCurrentDate(Mockito.anyInt())).thenReturn(new Date());
	//     Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
	//     caregiver.getUserDetails().setType(ApplicationConstants.ANDROID);
	//     patient.setCaregivers(Stream.of(caregiver).collect(Collectors.toSet()));
	//     Mockito.when(mockPatientRepository.save(Mockito.any())).thenReturn(patient);
	//     PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
	//     Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any())).thenReturn(patientModel);
	//     PatientModel actualPatientModel = patientService.addPatient(patient);
	//     assertNotNull(actualPatientModel);
	//     assertEquals(patientModel, actualPatientModel);
	//     Mockito.verify(mockSendResetPwdNotification, Mockito.times(1)).sendPwdResetNotificationWhenRegst(Mockito
	//     .any(), Mockito.anyString());
	//     Mockito.verify(mockNotificationService, Mockito.times(2)).sendMobileNotification(Mockito.any(), Mockito
	//     .anyString(), Mockito.anyString(), Mockito.any());
	//     Mockito.verify(mockEmailAndSmsUtility, Mockito.times(1)).sendSms(Mockito.any(), Mockito.anyString());
	//     Mockito.verify(mockEmailAndSmsUtility, Mockito.times(1)).sendEmail(Mockito.any(), Mockito.anyString(),
	//     Mockito.anyList());

	//     /**
	//      * Case 2 : Imantic user not found
	//      */
	//     Mockito.when(mockImanticService.createPatientInImantics(Mockito.any())).thenReturn(null);
	//     actualPatientModel = patientService.addPatient(patient);
	//     assertNull(actualPatientModel);
	// }

	@Test
	public void testAddPrescription() throws Exception {
		/**
		 * Case 1 : Non null response from Imatics
		 */
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Disease disease = TestResponseGenerator.generateDisease();
		Mockito.when(mockDiseaseRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(disease));
		DiseaseInfo diseaseInfo = TestResponseGenerator.generateDiseaseInfo(prescription);
		Mockito.when(mockDiseaseInfoRepository.findByPrescription_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(diseaseInfo)
		                         .collect(Collectors.toList()));
		DosageInfo dosageInfo = TestResponseGenerator.generateDosageInfo(false);
		Mockito.when(mockDosageInfoRepository.findByPrescription_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(dosageInfo)
		                         .collect(Collectors.toList()));
		DeviceInfo deviceInfo = TestResponseGenerator.generateDeviceInfo(10, "1", false);
		Mockito.when(mockDeviceInfoRepository.findByPrescription_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(deviceInfo)
		                         .collect(Collectors.toList()));
		Mockito.when(mockImanticService.createPrescription(Mockito.any()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		Map<String, String> analyticsMap = TestResponseGenerator.generateImaticAnalyticMap();
		Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString()))
		       .thenReturn(analyticsMap);
		Mockito.when(mockPrescriptionRepository.save(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel =
				generateRequestObject("PrescriptionModel-1", PrescriptionModel.class, PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		PrescriptionSchedule schedule = TestResponseGenerator.generatePrescriptionSchedule();
		//Mockito.when(mockPrescriptionScheduleService.fetchJobsByUserId(Mockito.anyLong())).thenReturn(Stream.of
		// (schedule).collect(Collectors.toList()));
		NotificationSchedule notificationSchedule = TestResponseGenerator.generateEmailNotificationSchedule();
		//Mockito.when(mockNotificationScheduleService.fetchJobsByUserId(Mockito.anyLong())).thenReturn(Stream.of
		// (notificationSchedule).collect(Collectors.toList()));
		PrescriptionModel actualPrescriptionModel = patientService.addPrescription(prescription, 1, 1);
		assertNotNull(actualPrescriptionModel);
		assertEquals(prescriptionModel, actualPrescriptionModel);

		/**
		 * Case 2 : null response from imatics.
		 */
		Mockito.when(mockImanticService.createPrescription(Mockito.any()))
		       .thenReturn(ApplicationConstants.FAILED);
		actualPrescriptionModel = patientService.addPrescription(prescription, 1, 1);
		assertNull(actualPrescriptionModel);

		/**
		 * case 3 : Exception occurred
		 */
		Mockito.when(mockDiseaseRepository.findById(Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		actualPrescriptionModel = patientService.addPrescription(prescription, 1, 1);
		assertNull(actualPrescriptionModel);
	}

	@Test
	public void testUpdatePrescription() throws Exception {
		Prescription prescription = TestResponseGenerator.generatePrescription();
		DosageDevice dosageDevice = TestResponseGenerator.generateDosageDevice(prescription);
		Mockito.when(mockDosageDeviceRepository.findByPrescription_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(dosageDevice)
		                         .collect(Collectors.toList()));
		Mockito.when(mockPrescriptionRepository.save(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel =
				generateRequestObject("PrescriptionModel-1", PrescriptionModel.class, PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		PrescriptionModel actualPrescriptionModel = patientService.updatePrescription(prescription);
		assertNotNull(actualPrescriptionModel);
		assertEquals(prescriptionModel, actualPrescriptionModel);
	}

	@Test
	public void testFetchPatientsByProviderId() throws Exception {
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findByProviders_idAndStatus(Mockito.anyLong(), Mockito.anyInt()))
		       .thenReturn(patients);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		CaregiverModel caregiverModel =
				generateRequestObject("CaregiverModel-1", CaregiverModel.class, USER_DETAILS_FILE);
		ProviderModel providerModel = generateRequestObject("ProviderModel-1", ProviderModel.class, USER_DETAILS_FILE);
		PharmacistModel pharmacistModel =
				generateRequestObject("PharmacistModel-1", PharmacistModel.class, USER_DETAILS_FILE);
		patientModel.setPharmacists(Stream.of(pharmacistModel)
		                                  .collect(Collectors.toSet()));
		patientModel.setProviders(Stream.of(providerModel)
		                                .collect(Collectors.toSet()));
		patientModel.setCaregivers(Stream.of(caregiverModel)
		                                 .collect(Collectors.toSet()));
		patientModel.setCaregiverList(Stream.of(caregiverModel)
		                                    .collect(Collectors.toSet()));
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel =
				generateRequestObject("PrescriptionModel-1", PrescriptionModel.class, PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		UserDetailsModel userDetailsModel =
				generateRequestObject("userDetails-Caregiver1", UserDetailsModel.class, USER_DETAILS_FILE);
		List<PatientModel> patientModels = patientService.fetchPatientsByProviderId(1);
		assertTrue(CollectionUtils.isNotEmpty(patientModels));
		assertEquals(patientModels, Stream.of(patientModel)
		                                  .collect(Collectors.toList()));
	}

	@Test
	public void testFetchPrescriptionListByPatientId() throws Exception {
		/**
		 * Case 1 : Without Exception
		 */
		Prescription prescription = TestResponseGenerator.generatePrescription();
		List<Prescription> prescriptions = Stream.of(prescription)
		                                         .collect(Collectors.toList());
		Mockito.when(mockPrescriptionRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(prescriptions);
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel =
				generateRequestObject("PrescriptionModel-1", PrescriptionModel.class, PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		List<PrescriptionModel> actualPrescriptionModels = patientService.fetchPrescriptionlistByPatientId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualPrescriptionModels));
		assertEquals(Stream.of(prescriptionModel)
		                   .collect(Collectors.toList()), actualPrescriptionModels);

		/**
		 * Case 2 : Exception occurred.
		 */
		Mockito.when(mockPrescriptionRepository.findByPatient_id(Mockito.anyLong()))
		       .thenThrow(new RuntimeException());
		actualPrescriptionModels = patientService.fetchPrescriptionlistByPatientId(1);
		assertTrue(CollectionUtils.isEmpty(actualPrescriptionModels));
	}

	@Test
	public void testFetchPrescriptionListByStatus() throws Exception {
		Prescription prescription = TestResponseGenerator.generatePrescription();
		List<Prescription> prescriptions = Stream.of(prescription)
		                                         .collect(Collectors.toList());
		Mockito.when(mockPrescriptionRepository.findByPrescriptionStatus_nameIn(Mockito.any()))
		       .thenReturn(prescriptions);
		PrescriptionModel prescriptionModel =
				generateRequestObject("PrescriptionModel-1", PrescriptionModel.class, PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		List<PrescriptionModel> actualPrescriptionModels = patientService.fetchPrescriptionlistByStatus(
				Stream.of("Junit")
				      .collect(Collectors.toList()));
		assertTrue(CollectionUtils.isNotEmpty(actualPrescriptionModels));
		assertEquals(Stream.of(prescriptionModel)
		                   .collect(Collectors.toList()), actualPrescriptionModels);
	}

	@Test
	public void testFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Patient actualPatient = patientService.fetchPatientById(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id does not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.fetchPatientById(1);
		assertNull(actualPatient);
	}

	@Test
	public void testDirectFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		Patient actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNull(actualPatient);
	}

	@Test
	public void testDecryptPatientProviderFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");

		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		patient.setCaregivers(Stream.of(caregiver)
		                            .collect(Collectors.toSet()));
		patient.setProviders(Stream.of(provider)
		                           .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Patient actualPatient = patientService.decryptPatientProviderfetchPatientById(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.decryptPatientProviderfetchPatientById(1);
		assertNull(actualPatient);
	}

	@Test
	public void testNoDecryptCargiverFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		patient.setCaregivers(Stream.of(caregiver)
		                            .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Patient actualPatient = patientService.noDecrypCargiverfetchPatientById(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.noDecrypCargiverfetchPatientById(1);
		assertNull(actualPatient);
	}

	@Test
	public void testDirectFetchPatientByPatientId() {
		/**
		 * Case 1 : Id exists
		 */
		Patient actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNull(actualPatient);
	}

	@Test
	public void testFindByUserDetails_id() {
		Mockito.when(mockPatientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		Patient actualPatient = patientService.findByUserDetails_id(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);
	}

	// @Test
	// public void testUpdatePatient() throws Exception {
	//     Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
	//     patient.setCaregivers(Stream.of(caregiver).collect(Collectors.toSet()));
	//     Map<String, String> analyticsMap = TestResponseGenerator.generateImaticAnalyticMap();
	//     Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString())).thenReturn(analyticsMap);
	//     UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
	//     Mockito.when(mockPhiDataDBEncryption.decryptUserDetailsEntity(Mockito.any(), Mockito.anyString()))
	//     .thenReturn(userDetails);
	//     Mockito.when(mockPatientRepository.save(Mockito.any())).thenReturn(patient);
	//     PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
	//     Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any())).thenReturn(patientModel);
	//     PatientModel actualPatientModel = patientService.updatePatient(patient);
	//     assertNotNull(actualPatientModel);
	//     assertEquals(patientModel, actualPatientModel);
	//     Mockito.verify(mockEmailAndSmsUtility, Mockito.times(2)).sendEmail(Mockito.anyString(), Mockito.anyString()
	//     , Mockito.any());
	//     Mockito.verify(mockEmailAndSmsUtility, Mockito.times(2)).sendSms(Mockito.anyString(), Mockito.anyString());
	//     Mockito.verify(mockNotificationService, Mockito.times(2)).sendMobileNotification(Mockito.anyString(),
	//     Mockito.anyString(), Mockito.anyString(), Mockito.any());
	// }

	@Test
	public void testFetchPatientsByCaregiverId() throws Exception {
		Mockito.when(mockPatientRepository.findByCaregivers_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(patient)
		                         .collect(Collectors.toList()));
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		List<PatientModel> actualPatientModels = patientService.fetchPatientsByCaregiverId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualPatientModels));
		assertEquals(Stream.of(patientModel)
		                   .collect(Collectors.toList()), actualPatientModels);
	}

	@Test
	public void testDirectFetchPatientsByCaregiverId() {
		Mockito.when(mockPatientRepository.findByCaregivers_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(patient)
		                         .collect(Collectors.toList()));
		List<Patient> actualPatients = patientService.directFetchPatientsByCaregiverId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualPatients));
		assertEquals(Stream.of(patient)
		                   .collect(Collectors.toList()), actualPatients);
	}

	@Test
	public void testUpdatePatientAlone() throws Exception {
		Mockito.when(mockPatientRepository.save(Mockito.any()))
		       .thenReturn(patient);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		UserDetailsModel userDetailsModel =
				generateRequestObject("userDetails-Caregiver1", UserDetailsModel.class, USER_DETAILS_FILE);
		PatientModel actualPatientModel = patientService.updatePatientAlone(patient);
		assertNotNull(actualPatientModel);
		assertEquals(patientModel, actualPatientModel);
	}

	@Test
	public void testFetchByDeviceId() {
		Mockito.when(mockPatientRepository.findByDeviceId(Mockito.any()))
		       .thenReturn(patient);
		Patient actualPatient = patientService.fetchByDeviceId("JUnit");
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);
	}

	@Test
	public void testFetchAdherenceByPatientId() {
		/**
		 * Case 1 : Null response from repository
		 */
		Mockito.when(mockPatientRepository.findAdherenceById(Mockito.anyLong()))
		       .thenReturn(null);
		PatientAdherencePojo actualPojo = patientService.fetchAdherenceByPatientId(1);
		assertNull(actualPojo);

		/**
		 * Case 2 : non null response from repository
		 */
		PatientAdherencePojo expectedPojo = TestResponseGenerator.generatePatientAdherencePojo();
		Mockito.when(mockPatientRepository.findAdherenceById(Mockito.anyLong()))
		       .thenReturn(expectedPojo);
		actualPojo = patientService.fetchAdherenceByPatientId(1);
		assertNotNull(actualPojo);
		assertEquals(expectedPojo, actualPojo);
	}

	@Test
	public void testFetchPatientByUserId() throws Exception {
		/**
		 * Case 1 : Non null result
		 */
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		patient.setCaregivers(Stream.of(caregiver)
		                            .collect(Collectors.toSet()));
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		patient.setProviders(Stream.of(provider)
		                           .collect(Collectors.toSet()));
		Pharmacist pharmacist = TestResponseGenerator.generatePharmacist("Male", "Asian", "Pharmacist");
		patient.setPharmacists(Stream.of(pharmacist)
		                             .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		UserDetailsModel userDetailsModel =
				generateRequestObject("userDetails-Caregiver1", UserDetailsModel.class, USER_DETAILS_FILE);
		Patient actualPatient = patientService.fetchPatientByUserId(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Null result
		 */
		Mockito.when(mockPatientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(null);
		actualPatient = patientService.fetchPatientByUserId(1);
		assertNull(actualPatient);
	}

	@Test
	public void addPatient() {

		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");

		Map<String, String> imanticMap = new HashMap<String, String>();
		imanticMap.put("imanticid", "12345");
		Map<String, String> analyticMap = new HashMap<String, String>();
		analyticMap.put("emoji", "happy");

		Mockito.when(mockImanticService.createPatientInImantics(Mockito.any(Patient.class)))
		       .thenReturn(imanticMap);
		Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString()))
		       .thenReturn(analyticMap);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("abcd");

		//pHIDataDBEncryption.getDBEncryKey()
		Mockito.when(mockPatientRepository.save(Mockito.any(Patient.class)))
		       .thenReturn(patient);
		mockSendResetPwdNotification.sendPwdResetNotificationWhenRegst(Mockito.any(UserDetails.class),
		                                                               Mockito.anyString());

		mockNotificationService.sendMobileNotification(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
		                                               Mockito.anyString());

		PatientModel patienTModel = TestResponseGenerator.generatePatientModel();

		Mockito.when(mockUtility.convertPatientEntityTopatientModel(patient))
		       .thenReturn(patienTModel);

		Mockito.when(mockUtility.getUserPreferedLang(Mockito.anyLong()))
		       .thenReturn("abcd");

		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Set<Caregiver> CaregiverSet = new HashSet<Caregiver>();
		CaregiverSet.add(caregiver);
		patient.setCaregivers(CaregiverSet);
		PatientModel patientModel = patientService.addPatient(patient);
		assertNotNull(patientModel);
	}

	@Test
	public void updatePatient() {

		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Set<Caregiver> CaregiverSet = new HashSet<Caregiver>();
		CaregiverSet.add(caregiver);
		patient.setCaregivers(CaregiverSet);
		PatientModel patienTModel = TestResponseGenerator.generatePatientModel();
		Map<String, String> analyticMap = new HashMap<String, String>();
		analyticMap.put("emoji", "happy");
		Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString()))
		       .thenReturn(analyticMap);

		//pHIDataDBEncryption.encryptUserDetailsEntitiy(patient.getUserDetails(), pHIDataDBEncryption.getDBEncryKey())
		Mockito.when(mockPatientRepository.save(Mockito.any(Patient.class)))
		       .thenReturn(patient);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(patient))
		       .thenReturn(patienTModel);
		Mockito.when(mockUtility.getUserPreferedLang(Mockito.anyLong()))
		       .thenReturn("abcd");

		Mockito.when(
				mockMessageNotificationConstants.getUserPreferedLangTranslatedMsg(Mockito.any(), Mockito.anyString(),
				                                                                  Mockito.anyString(), Mockito.any(Object[].class)))
		       .thenReturn("abcd");
		PatientModel actualPatientModel = patientService.updatePatient(patient);
		assertNotNull(actualPatientModel);
	}

}









