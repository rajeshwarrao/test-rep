package com.sg.dayamed.service;

import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserOptions;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.repository.UserOptionsRepository;
import com.sg.dayamed.service.impl.ProviderServiceImpl;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestProviderService {

	@InjectMocks
	ProviderServiceImpl providerService;

	@Mock
	NotificationScheduleRepository mockNotificationScheduleRepository;

	@Mock
	ProviderRepository mockProviderRepository;

	@Mock
	Utility mockUtility;

	@Mock
	PatientService mockPatientService;

	@Mock
	EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	RoleRepository mockRoleRepository;

	@Mock
	UserPreferenceService mockUserPreferenceService;

	@Mock
	private PasswordEncryption mockPasswordEncryption;

	@Mock
	MessageNotificationConstants mockMessageNotificationConstants;

	@Mock
	SendResetPwdNotification mockSendPwdResetNtf;

	@Mock
	UserOptionsRepository mockUserOptionsRepository;

	@Mock
	PharmacistRepository mockPharmacistRepository;

	@Before
	public void setUp() {
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
	}

	@Test
	public void testAddProvider() {
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		provider.setId(0);
		Mockito.when(mockUtility.convertProviderModelToEntity(Mockito.any(ProviderModel.class)))
		       .thenReturn(provider);
		Role role = TestResponseGenerator.generateRole(UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString()))
		       .thenReturn(role);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		Mockito.when(mockProviderRepository.save(Mockito.any(Provider.class)))
		       .thenReturn(provider);
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		providerModel.setPharmacist(true);
		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);
		providerModel.setPharmacist(true);
		ProviderModel actualProviderModel = providerService.addProvider(providerModel);
		assertEquals(actualProviderModel.getId(), providerModel.getId());
	}

	@Test
	public void testAddProviderEmail() {
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockUtility.convertProviderModelToEntity(Mockito.any(ProviderModel.class)))
		       .thenReturn(provider);
		Role role = TestResponseGenerator.generateRole(UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString()))
		       .thenReturn(role);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		Mockito.when(mockProviderRepository.save(Mockito.any(Provider.class)))
		       .thenReturn(provider);
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);
		Mockito.when(mockUtility.getUserPreferedLang(Mockito.any()))
		       .thenReturn("abcd");
		providerModel.setPharmacist(true);
		ProviderModel actualProviderModel = providerService.addProvider(providerModel);
		assertEquals(actualProviderModel.getId(), providerModel.getId());
	}

	@Test
	public void fetchProviderByUserId() throws DataValidationException {
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		UserOptions userOptions = TestResponseGenerator.generateUserOptions(1, provider.getUserDetails());
		userOptions.setGsPharmacistEnable(1);
		provider.getUserDetails()
		        .setUserOptions(userOptions);
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(provider);
		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);
		ProviderModel actuaProviderModel = providerService.fetchProviderByUserId(1L);
		assertNotNull(providerModel);
	}

	@Test
	public void fetchProviderByUserIdNull() throws Exception {
		try {
			Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
			       .thenReturn(null);
			providerService.fetchProviderByUserId(1L);
		} catch (DataValidationException e) {
			assertEquals(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB, e.getErrorKey());
		}
	}

	@Test
	public void fetchProvidersByPatientId() {
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		List<Provider> providerList = new ArrayList<>();
		providerList.add(provider);
		UserOptions userOptions = TestResponseGenerator.generateUserOptions(1, provider.getUserDetails());
		provider.getUserDetails()
		        .setUserOptions(userOptions);
		Mockito.when(mockProviderRepository.findByPatients_id(Mockito.anyLong()))
		       .thenReturn(providerList);
		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);
		List<ProviderModel> providerModelList = providerService.fetchProvidersByPatientId(1L);
		assertTrue(providerModelList.size() > 0);
	}

	@Test
	public void fetchProviderById() throws Exception {
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		UserOptions userOptions = TestResponseGenerator.generateUserOptions(1, provider.getUserDetails());
		userOptions.setGsPharmacistEnable(1);
		provider.getUserDetails()
		        .setUserOptions(userOptions);
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Mockito.when(mockProviderRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(provider));
		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);

		ProviderModel actualProviderModel = providerService.fetchProviderById(1L);
		assertNotNull(actualProviderModel);
	}

	@Test
	public void directFetchProviderByUserId() {
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(provider);
		Provider actualProvider = providerService.directFetchProviderByUserId(1L);
		assertNotNull(actualProvider);
	}

	@Mock
	UserDetailsService mockUserDetailsService;

	@Test
	public void validateAddProvider() throws IOException, DataValidationException {
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		providerModel.getUserDetails().setImageUrl("test");
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);

		UserDetailsModel userDetailsModel = TestResponseGenerator.generateUserDetailsModel("Male", "Asian", "Patient");
		Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetailsModel);

		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockUtility.convertProviderModelToEntity(Mockito.any(ProviderModel.class)))
		       .thenReturn(provider);
		Role role = TestResponseGenerator.generateRole(UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString()))
		       .thenReturn(role);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		Mockito.when(mockProviderRepository.save(Mockito.any(Provider.class)))
		       .thenReturn(provider);

		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);
		Mockito.when(mockUtility.getUserPreferedLang(Mockito.any()))
		       .thenReturn("abcd");
		final ProviderModel providerResponse = providerService.validateAddProvider(providerModel, profileImage);
		assertNotNull(providerResponse);
		assertEquals(providerModel.getUserDetails().getDateOfBirth(), providerResponse.getUserDetails().getDateOfBirth());
	}

	@Test
	public void validateAddProviderDateOfBirthAfterCurrentDate() {
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		providerModel.getUserDetails().setDateOfBirth(LocalDate.MAX);
		try {
			providerService.validateAddProvider(providerModel, null);
		} catch (DataValidationException exception) {
			assertEquals(APIErrorKeys.ERROR_INPUT_BADREQUEST, exception.getErrorKey());
		}
	}

	@Test
	public void validateAddProviderUserNotFound() throws IOException {
		try {
			ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
			providerModel.setId(0L);
			InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
			MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
			                                                       inputFile);
			Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString()))
			       .thenReturn(providerModel.getUserDetails());
			providerModel = providerService.validateAddProvider(providerModel, profileImage);
			assertNotNull(providerModel);
		} catch (DataValidationException e) {
			assertEquals(APIErrorKeys.ERROR_EMAIL_CONFLICT, e.getErrorKey());
		}

	}

	@Test
	public void validateUserDetailsNull() throws IOException {
		try {
			ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
			providerModel.setUserDetails(null);
			InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
			MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
			                                                       inputFile);
			providerModel = providerService.validateAddProvider(providerModel, profileImage);
		} catch (DataValidationException de) {
			assertEquals(APIErrorKeys.ERROR_INPUT_BADREQUEST, de.getErrorKey());
		}
	}

	@Test
	public void validateUserDetailsMandateChk() throws IOException {
		try {
			ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
			providerModel.getUserDetails()
			             .setFirstName(null);
			InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
			MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
			                                                       inputFile);
			providerService.validateAddProvider(providerModel, profileImage);
		} catch (DataValidationException de) {
			assertEquals(APIErrorKeys.ERROR_INPUT_BADREQUEST, de.getErrorKey());
		}
	}

	@Test
	public void fetchProviderByIdWithNull() throws Exception {
		try {
			Mockito.when(mockProviderRepository.findById(Mockito.anyLong()))
			       .thenReturn(Optional.empty());
			providerService.fetchProviderById(1L);
		} catch (DataValidationException e) {
			assertEquals(APIErrorKeys.ERROR_PROVIDED_ID_NOT_IN_DB, e.getErrorKey());
		}
	}

	@Test
	public void fetchProviders() {
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		List<Provider> providerList = new ArrayList<>();
		UserOptions userOptions = TestResponseGenerator.generateUserOptions(1, provider.getUserDetails());
		userOptions.setGsPharmacistEnable(1);
		provider.getUserDetails()
		        .setUserOptions(userOptions);
		providerList.add(provider);
		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Mockito.when(mockProviderRepository.findAll())
		       .thenReturn(providerList);
		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);
		List<ProviderModel> providerModelList = providerService.fetchProviders();
		assertTrue(providerModelList.size() > 0);
	}
	
	@Test
	public void deleteProviderById() throws DataValidationException {
		long providerId = 1;
		providerService.deleteProviderById(providerId);
		Mockito.verify(mockProviderRepository, Mockito.times(1))
		       .deleteProvider(Mockito.eq(providerId));
	}

	@Test
	public void deleteProviderByIdthrowsDatavalidationExcp() throws DataValidationException {
		try {
			PatientModel patientModel = TestResponseGenerator.generatePatientModel();
			List<PatientModel> patientModelList = new ArrayList<>();
			patientModelList.add(patientModel);

			Mockito.when(mockPatientService.fetchPatientsByProviderId(Mockito.anyLong()))
			       .thenReturn(patientModelList);
			providerService.deleteProviderById(1L);
		} catch (DataValidationException de) {
			assertEquals(APIErrorKeys.ERROR_PROVIDER_ASSOCIATED_WITH_PATIENTS, de.getErrorKey());
		}
	}

	@Test
	public void directFetchProviderById() {
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		Mockito.when(mockProviderRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(provider));

		Provider actualProvider = providerService.directFetchProviderById(1L);
		assertNotNull(actualProvider);
	}

	@Test
	public void directFetchProviderByIdNull() {

		Mockito.when(mockProviderRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());

		Provider actualProvider = providerService.directFetchProviderById(1L);
		assertNull(actualProvider);

	}

}

