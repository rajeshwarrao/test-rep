package com.sg.dayamed.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.pojo.ConsumptionFrequencyResponseModel;
import com.sg.dayamed.pojo.FirstConsumptionResponseModel;
import com.sg.dayamed.repository.ConsumptionFrequencyRepository;
import com.sg.dayamed.service.impl.ConsumptionTemplateServiceImpl;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class ConsumptionTemplateServiceTest {

	@InjectMocks
	private ConsumptionTemplateService consumptionTemplateService = new ConsumptionTemplateServiceImpl();

	@Mock
	private ConsumptionFrequencyRepository mockConsumptionFrequencyRepository;

	@Test
	public void testFetchConsumptionTemplateByConsumptionsKey() {
		ConsumptionTemplate expectedTemplate = TestResponseGenerator.generateConsumptionTemplate();
		Mockito.when(mockConsumptionFrequencyRepository.findByconsumptionsKey(Mockito.anyString()))
		       .thenReturn(expectedTemplate);
		ConsumptionTemplate actualTemplate =
				consumptionTemplateService.fetchConsumptionTemplateByConsumptionsKey("test");
		assertNotNull(actualTemplate);
		assertEquals(expectedTemplate, actualTemplate);
	}

	@Test
	public void testGetFrequencyList(){
		List<ConsumptionTemplate> listExpectedTemplate = Stream.of(TestResponseGenerator.generateConsumptionTemplate()).collect(
				Collectors.toList());
		Mockito.when(mockConsumptionFrequencyRepository.findAll())
		       .thenReturn(listExpectedTemplate);
		ConsumptionFrequencyResponseModel consumptionFrequencyResponseModel=consumptionTemplateService.getFrequencyList();
		assertNotNull(consumptionFrequencyResponseModel);
		assertNotNull(consumptionFrequencyResponseModel.getFrequencies());
		assertTrue(consumptionFrequencyResponseModel.getFrequencies().size() >0);
	}

	@Test
	public void testGetFirstConsumption(){
		List<ConsumptionTemplate> listExpectedTemplate = Stream.of(TestResponseGenerator.generateConsumptionTemplate()).collect(
				Collectors.toList());
		Mockito.when(mockConsumptionFrequencyRepository.findByfrequencyId(Mockito.anyLong()))
		       .thenReturn(listExpectedTemplate);
		FirstConsumptionResponseModel firstConsumptionResponseModel =consumptionTemplateService.getFirstConsumption(1l);
		assertNotNull(firstConsumptionResponseModel);
		assertNotNull(firstConsumptionResponseModel.getConsumptions());
		assertTrue(firstConsumptionResponseModel.getConsumptions().size() >0);
	}
}
