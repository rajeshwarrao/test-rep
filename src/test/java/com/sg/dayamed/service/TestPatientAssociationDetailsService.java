package com.sg.dayamed.service;

import com.sg.dayamed.adapter.patients.PatientLoadingProcessor;
import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.view.PatientAssociationDetailsV;
import com.sg.dayamed.repository.view.PatientAssociationDetailsVRepository;
import com.sg.dayamed.service.v1.PatientAssociationDetailsService;
import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.impl.PatientAssociationDetailsServiceImpl;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientAssociationsVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.PatientSortByEnum;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import ma.glasnost.orika.MapperFacade;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.jpa.domain.Specification;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 17/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPatientAssociationDetailsService extends BaseTestCase {

	@InjectMocks
	private PatientAssociationDetailsService associationDetailsService = new PatientAssociationDetailsServiceImpl();

	@Mock
	private PatientAssociationDetailsVRepository mockPatientAssociationDetailsVRepository;

	@Mock
	private MapperFacade mockMapperFacade;

	@Mock
	private UserPreferencesVService mockUserPreferencesVService;

	@Mock
	private UserValidator mockUserValidator;

	@Mock
	private PatientLoadingProcessor mockPatientLoadingProcessor;

	@Before
	public void setUp() throws Exception {
		Mockito.doNothing()
		       .when(mockUserValidator)
		       .validateGetPatients(Mockito.any(), Mockito.any(LoadPatientRequestVO.class));
		Mockito.doNothing()
		       .when(mockUserValidator)
		       .validateSearchPatients(Mockito.any(), Mockito.any(), Mockito.anyString());
		Mockito.doNothing()
		       .when(mockUserValidator)
		       .validateGetPatientAssociations(Mockito.any(), Mockito.any());
	}

	@Test
	public void testGetPatients() throws Exception {

		/**
		 * Case 1 : Success Response
		 */
		List<PatientAssociationDetailsV> associationDetailsVS = IntStream.range(0, 15)
		                                                                 .mapToObj(
				                                                                 index -> TestResponseGenerator.generatePatientAssociationDetailsV())
		                                                                 .collect(Collectors.toList());
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(associationDetailsVS);
		List<PatientDetailsVO> patientDetailsVOS = associationDetailsVS.stream()
		                                                               .limit(10)
		                                                               .map(detail -> toPatientDetailsVO(detail))
		                                                               .collect(Collectors.toList());
		PatientDetailsResponseVO expectedResponseVO = new PatientDetailsResponseVO();
		expectedResponseVO.setTotalRecords(20);
		expectedResponseVO.setPatientDetails(patientDetailsVOS);
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PROVIDER);
		Mockito.when(mockPatientLoadingProcessor.getPatients(Mockito.any(UserRoleEnum.class),
		                                                     Mockito.any(LoadPatientRequestVO.class)))
		       .thenReturn(expectedResponseVO);
		PatientDetailsResponseVO actualResponse = associationDetailsService.getPatients(requestVO);
		assertNotNull(actualResponse);
		assertEquals(expectedResponseVO.getTotalRecords(), actualResponse.getTotalRecords());
		assertEquals(expectedResponseVO.getPatientDetails(), actualResponse.getPatientDetails());

		/**
		 * Case 2 : Error case
		 */
		Mockito.reset(mockPatientLoadingProcessor);
		Mockito.when(mockPatientLoadingProcessor.getPatients(Mockito.any(UserRoleEnum.class),
		                                                     Mockito.any(LoadPatientRequestVO.class)))
		       .thenThrow(new RuntimeException());
		try {
			associationDetailsService.getPatients(requestVO);
			Assert.fail();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetPatientAssociations() throws Exception {
		/**
		 * Case 1 : Positive cases.
		 */
		PatientAssociationsRequestVO requestVO =
				TestResponseGenerator.generatePatientAssociationsRequest(UserRoleEnum.PROVIDER);
		List<PatientAssociationDetailsV> associationDetailsVS = IntStream.range(0, 15)
		                                                                 .mapToObj(
				                                                                 index -> TestResponseGenerator.generatePatientAssociationDetailsV())
		                                                                 .collect(Collectors.toList());
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(associationDetailsVS);
		UserPreferenceVO preferenceVO = TestResponseGenerator.generateUserPreferenceVO();
		Mockito.when(mockUserPreferencesVService.getUserPreferences(Mockito.anyList()))
		       .thenReturn(Stream.of(preferenceVO)
		                         .collect(Collectors.toList()));
		for (PatientAssociationDetailsV detailsV : associationDetailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.any(PatientAssociationDetailsV.class), Mockito.any()))
			       .thenReturn(toPatientDetailsVO(detailsV));
		}
		PatientAssociationsVO patientAssociationsVO = associationDetailsService.getPatientAssociations(requestVO);
		assertNotNull(patientAssociationsVO);
		assertNotNull(patientAssociationsVO.getPatient());

		/**
		 * Case 2 : Error case scenario
		 */
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenThrow(new RuntimeException());
		try {
			associationDetailsService.getPatientAssociations(requestVO);
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testSearchPatients() throws Exception {
		/**
		 * Case 1 : Positive case search by first name sort by last active time ascending order
		 */
		Mockito.reset(mockMapperFacade);
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PROVIDER);
		requestVO.setSortByEnum(PatientSortByEnum.ACTIVE_TIME);
		List<PatientAssociationDetailsV> associationDetailsVS = IntStream.range(0, 15)
		                                                                 .mapToObj(
				                                                                 index -> TestResponseGenerator.generatePatientAssociationDetailsV())
		                                                                 .collect(Collectors.toList());
		associationDetailsVS = associationDetailsVS.stream()
		                                           .sorted(Comparator.comparing(
				                                           PatientAssociationDetailsV::getLastActiveTime))
		                                           .collect(Collectors.toList());
		;
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(associationDetailsVS);
		for (PatientAssociationDetailsV detailsV : associationDetailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(detailsV), Mockito.any()))
			       .thenReturn(toPatientDetailsVO(detailsV));
		}
		//Mockito.when(mockMapperFacade.map(Mockito.any(PatientAssociationDetailsV.class), Mockito.any())).thenReturn
		// (toPatientDetailsVO(associationDetailsVS.get(0)));
		PatientDetailsResponseVO responseVO = associationDetailsService.searchPatients(requestVO,
		                                                                               associationDetailsVS.get(0)
		                                                                                                   .getFirstName());
		assertNotNull(responseVO);
		assertEquals(1, responseVO.getTotalRecords()
		                          .intValue());
		assertEquals(associationDetailsVS.get(0)
		                                 .getPatientId(), responseVO.getPatientDetails()
		                                                            .get(0)
		                                                            .getId());

		/**
		 * Case 2 : Positive case search by last name sort by last active time ascending order
		 */
		responseVO = associationDetailsService.searchPatients(requestVO, associationDetailsVS.get(10)
		                                                                                     .getLastName());
		assertNotNull(responseVO);
		assertEquals(1, responseVO.getTotalRecords()
		                          .intValue());
		assertEquals(associationDetailsVS.get(10)
		                                 .getPatientId(), responseVO.getPatientDetails()
		                                                            .get(0)
		                                                            .getId());

		/**
		 * Case 3 : Positive case multiple results by firstName and sort by first name ascending order
		 */
		associationDetailsVS = associationDetailsVS.stream()
		                                           .sorted(Comparator.comparing(
				                                           PatientAssociationDetailsV::getFirstName))
		                                           .collect(Collectors.toList());
		String searchText = "Eresh";
		associationDetailsVS.get(4)
		                    .setFirstName("Eresh Gorantla");
		associationDetailsVS.get(10)
		                    .setFirstName("Gorantla Eresh");
		associationDetailsVS.get(13)
		                    .setFirstName(searchText);
		requestVO.setSortByEnum(PatientSortByEnum.FIRST_NAME);
		for (PatientAssociationDetailsV detailsV : associationDetailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(detailsV), Mockito.any()))
			       .thenReturn(toPatientDetailsVO(detailsV));
		}
		responseVO = associationDetailsService.searchPatients(requestVO, searchText);
		assertNotNull(responseVO);
		assertEquals(3, responseVO.getTotalRecords()
		                          .intValue());
		PatientAssociationDetailsV detailsV = associationDetailsVS.stream()
		                                                          .filter(detail -> searchText.equals(
				                                                          detail.getFirstName()))
		                                                          .findFirst()
		                                                          .orElse(null);
		assertNotNull(detailsV);
		assertEquals(detailsV.getPatientId(), responseVO.getPatientDetails()
		                                                .get(0)
		                                                .getId());
		detailsV = associationDetailsVS.stream()
		                               .filter(detail -> "Eresh Gorantla".equals(detail.getFirstName()))
		                               .findFirst()
		                               .orElse(null);
		assertNotNull(detailsV);
		assertEquals(detailsV.getPatientId(), responseVO.getPatientDetails()
		                                                .get(1)
		                                                .getId());
		detailsV = associationDetailsVS.stream()
		                               .filter(detail -> "Gorantla Eresh".equals(detail.getFirstName()))
		                               .findFirst()
		                               .orElse(null);
		assertNotNull(detailsV);
		assertEquals(detailsV.getPatientId(), responseVO.getPatientDetails()
		                                                .get(2)
		                                                .getId());

		/**
		 * Case 3 : Positive case multiple results by last name and sort by first name descending order
		 */
		associationDetailsVS.get(4)
		                    .setFirstName(RandomStringUtils.randomAlphabetic(7));
		associationDetailsVS.get(10)
		                    .setFirstName(RandomStringUtils.randomAlphabetic(7));
		associationDetailsVS.get(13)
		                    .setFirstName(RandomStringUtils.randomAlphabetic(7));
		associationDetailsVS.get(2)
		                    .setLastName("Eresh Gorantla");
		associationDetailsVS.get(8)
		                    .setLastName("Gorantla Eresh");
		associationDetailsVS.get(11)
		                    .setLastName(searchText);
		associationDetailsVS = associationDetailsVS.stream()
		                                           .sorted(Comparator.comparing(
				                                           PatientAssociationDetailsV::getFirstName)
		                                                             .reversed())
		                                           .collect(Collectors.toList());
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		for (PatientAssociationDetailsV v : associationDetailsVS) {
			Mockito.when(mockMapperFacade.map(Mockito.eq(v), Mockito.any()))
			       .thenReturn(toPatientDetailsVO(v));
		}
		responseVO = associationDetailsService.searchPatients(requestVO, searchText);
		assertNotNull(responseVO);
		assertEquals(3, responseVO.getTotalRecords()
		                          .intValue());

		/**
		 * Case 3 : Exception while retrieving data
		 */
		Mockito.when(mockPatientAssociationDetailsVRepository.findAll(Mockito.any(Specification.class)))
		       .thenThrow(new RuntimeException());
		try {
			associationDetailsService.searchPatients(requestVO, "text");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	private PatientDetailsVO toPatientDetailsVO(PatientAssociationDetailsV detailsV) {
		PatientDetailsVO patientDetailsVO = new PatientDetailsVO();
		patientDetailsVO.setCurrentAdherence(detailsV.getCurrentAdherence());
		patientDetailsVO.setEmoji(detailsV.getEmoji());
		patientDetailsVO.setId(detailsV.getPatientId());
		UserDetailsVO userDetailsVO = new UserDetailsVO();
		userDetailsVO.setAge(detailsV.getAge());
		userDetailsVO.setCity(detailsV.getCity());
		userDetailsVO.setCountry(detailsV.getCountry());
		userDetailsVO.setEmailFlag(detailsV.getEmailFlag());
		userDetailsVO.setEmailId(detailsV.getEmailId());
		userDetailsVO.setFirstName(detailsV.getFirstName());
		userDetailsVO.setLastActiveTime(detailsV.getLastActiveTime());
		userDetailsVO.setUserCreatedOn(detailsV.getUserCreatedOn());
		userDetailsVO.setLastName(detailsV.getLastName());
		userDetailsVO.setLocation(detailsV.getLocation());
		userDetailsVO.setId(detailsV.getUserId());
		userDetailsVO.setMobileNumber(detailsV.getMobileNumber());
		userDetailsVO.setRace(detailsV.getRace());
		patientDetailsVO.setUserDetails(userDetailsVO);
		return patientDetailsVO;
	}
}
