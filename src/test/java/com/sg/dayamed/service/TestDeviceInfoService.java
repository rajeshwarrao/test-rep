package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.DeviceInfo;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.service.impl.DeviceInfoServiceImpl;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestDeviceInfoService extends BaseTestCase {

	@InjectMocks
	private DeviceInfoService deviceInfoService = new DeviceInfoServiceImpl();

	@Mock
	private DeviceInfoRepository mockDeviceInfoRepository;

	@Test
	public void testFetchDeviceInfoByID() {
		/**
		 * Case 1 : Non Null response from repository
		 */
		DeviceInfo info = TestResponseGenerator.generateDeviceInfo(100, "Active", false);
		Mockito.when(mockDeviceInfoRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(info));
		DeviceInfo actualDeviceInfo = deviceInfoService.fetctDeviceInfoByID(1);
		assertNotNull(actualDeviceInfo);
		assertEquals(info, actualDeviceInfo);

		/**
		 * Case 2 : Null response from repository
		 */
		Mockito.when(mockDeviceInfoRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualDeviceInfo = deviceInfoService.fetctDeviceInfoByID(1);
		assertNull(actualDeviceInfo);
	}
}
