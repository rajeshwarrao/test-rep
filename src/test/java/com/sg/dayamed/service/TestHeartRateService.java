package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.service.impl.HeartRateServiceImpl;
import com.sg.dayamed.util.TestResponseGenerator;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestHeartRateService extends BaseTestCase {

	@InjectMocks
	private HeartRateService heartRateService = new HeartRateServiceImpl();

	@Mock
	private HeartRateRepository mockHeartRateRepository;

	private HeartRate heartRate;

	@Before
	public void setUp() {
		heartRate = TestResponseGenerator.generHeartRate();
	}

	@Test
	public void testFindByPrescribedTime() {
		List<HeartRate> expectedHeartRates = Stream.of(heartRate)
		                                           .collect(Collectors.toList());
		Mockito.when(mockHeartRateRepository.findByPrescribedTime(Mockito.any()))
		       .thenReturn(expectedHeartRates);
		List<HeartRate> actualHeartRates = heartRateService.findByPrescribedTime(LocalDateTime.now());
		assertTrue(CollectionUtils.isNotEmpty(actualHeartRates));
		assertEquals(expectedHeartRates, actualHeartRates);
	}

	@Test
	public void testFetchHeartRateByPrescribedTimeAndDeviceInfoId() {
		Mockito.when(mockHeartRateRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(heartRate);
		HeartRate actualHeartRate =
				heartRateService.fetchHeartRateByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "info");
		assertNotNull(actualHeartRate);
		assertEquals(heartRate, actualHeartRate);
	}
}
