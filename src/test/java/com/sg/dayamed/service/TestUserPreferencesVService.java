package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.view.UserPreferencesV;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.repository.view.UserPreferencesVRepository;
import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.impl.UserPreferencesVServiceImpl;
import com.sg.dayamed.service.v1.vo.users.CaregiverVO;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PharmacistVO;
import com.sg.dayamed.service.v1.vo.users.ProviderVO;
import com.sg.dayamed.service.v1.vo.users.UserDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.CommonAsserts;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.SortOrderEnum;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestUserPreferencesVService extends BaseTestCase {

	@InjectMocks
	private UserPreferencesVService userPreferencesVService = new UserPreferencesVServiceImpl();

	@Mock
	private UserPreferencesVRepository mockUserPreferencesVRepository;

	@Test
	public void testGetUserPreferences() throws Exception {
		/**
		 * Case 1 : No exception
		 */
		List<UserPreferencesV> userPreferencesVS = Stream.of(TestResponseGenerator.generateUserPreferencesV())
		                                                 .collect(Collectors.toList());
		Mockito.when(mockUserPreferencesVRepository.findByUserIdIn(Mockito.anyList()))
		       .thenReturn(userPreferencesVS);
		List<UserPreferenceVO> userPreferenceVOS = userPreferencesVService.getUserPreferences(Stream.of(1L)
		                                                                                            .collect(
				                                                                                            Collectors.toList()));
		assertNotNull(userPreferenceVOS);
		assertEquals(userPreferencesVS.size(), userPreferenceVOS.size());
		assertEquals(userPreferencesVS.get(0)
		                              .getUserId(), userPreferenceVOS.get(0)
		                                                             .getUserId());

		/**
		 * Case 2 : Exception occurred.
		 */
		Mockito.when(mockUserPreferencesVRepository.findByUserIdIn(Mockito.anyList()))
		       .thenThrow(new RuntimeException());
		try {
			userPreferencesVService.getUserPreferences(Stream.of(1L)
			                                                 .collect(Collectors.toList()));
			Assert.fail();
		} catch (ApplicationException e) {
			assertTrue(true);
		}
	}

	/**
	 * sort By firstName ascending Order -- user type -> Provider
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetUserPreferencesForProvider() throws Exception {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PROVIDER);
		List<UserPreferencesV> userPreferencesVS = IntStream.range(0, 15)
		                                                    .mapToObj(
				                                                    index -> TestResponseGenerator.generateUserPreferencesV())
		                                                    .collect(Collectors.toList());
		Mockito.when(mockUserPreferencesVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(userPreferencesVS);
		UserDetailsResponseVO responseVO = userPreferencesVService.getUserPreferences(UserRoleEnum.PROVIDER,
		                                                                              requestVO);
		assertNotNull(responseVO);
		assertEquals(15, responseVO.getTotalRecords()
		                           .intValue());
		assertTrue(CollectionUtils.isNotEmpty(responseVO.getProviders()));
		assertEquals(10, responseVO.getProviders()
		                           .size());
		userPreferencesVS = userPreferencesVS.stream()
		                                     .sorted(Comparator.comparing(UserPreferencesV::getFirstName,
		                                                                  String::compareToIgnoreCase))
		                                     .limit(10)
		                                     .collect(Collectors.toList());
		for (Integer index = 0; index < userPreferencesVS.size(); index++) {
			UserPreferencesV preferencesV = userPreferencesVS.get(index);
			ProviderVO providerVO = responseVO.getProviders()
			                                  .get(index);
			assertNotNull(preferencesV);
			assertNotNull(providerVO);
			assertEquals(preferencesV.getProviderId(), providerVO.getId());
		}
	}

	/**
	 * sort By firstName descending Order -- user type -> Caregiver
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetUserPreferencesForCaregiver() throws Exception {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.CAREGIVER);
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		requestVO.setOffset(1);
		List<UserPreferencesV> userPreferencesVS = IntStream.range(0, 15)
		                                                    .mapToObj(
				                                                    index -> TestResponseGenerator.generateUserPreferencesV())
		                                                    .collect(Collectors.toList());
		Mockito.when(mockUserPreferencesVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(userPreferencesVS);
		UserDetailsResponseVO responseVO =
				userPreferencesVService.getUserPreferences(UserRoleEnum.CAREGIVER, requestVO);
		assertNotNull(responseVO);
		assertEquals(15, responseVO.getTotalRecords()
		                           .intValue());
		assertTrue(CollectionUtils.isNotEmpty(responseVO.getCaregivers()));
		assertEquals(5, responseVO.getCaregivers()
		                          .size());
		userPreferencesVS = userPreferencesVS.stream()
		                                     .sorted(Comparator.comparing(UserPreferencesV::getFirstName,
		                                                                  String::compareToIgnoreCase)
		                                                       .reversed())
		                                     .collect(Collectors.toList());
		userPreferencesVS = userPreferencesVS.subList(10, 15);
		for (Integer index = 0; index < userPreferencesVS.size(); index++) {
			UserPreferencesV preferencesV = userPreferencesVS.get(index);
			CaregiverVO caregiverVO = responseVO.getCaregivers()
			                                    .get(index);
			assertNotNull(preferencesV);
			assertNotNull(caregiverVO);
			assertEquals(preferencesV.getCaregiverId(), caregiverVO.getId());
		}
	}

	/**
	 * sort By firstName descending Order -- user type -> Caregiver
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetUserPreferencesForPharmacist() throws Exception {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PHARMACIST);
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		requestVO.setOffset(1);
		List<UserPreferencesV> userPreferencesVS = IntStream.range(0, 15)
		                                                    .mapToObj(
				                                                    index -> TestResponseGenerator.generateUserPreferencesV())
		                                                    .collect(Collectors.toList());
		Mockito.when(mockUserPreferencesVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(userPreferencesVS);
		UserDetailsResponseVO responseVO =
				userPreferencesVService.getUserPreferences(UserRoleEnum.PHARMACIST, requestVO);
		assertNotNull(responseVO);
		assertEquals(15, responseVO.getTotalRecords()
		                           .intValue());
		assertTrue(CollectionUtils.isNotEmpty(responseVO.getPharmacists()));
		assertEquals(5, responseVO.getPharmacists()
		                          .size());
		userPreferencesVS = userPreferencesVS.stream()
		                                     .sorted(Comparator.comparing(UserPreferencesV::getFirstName,
		                                                                  String::compareToIgnoreCase)
		                                                       .reversed())
		                                     .collect(Collectors.toList());
		userPreferencesVS = userPreferencesVS.subList(10, 15);
		for (Integer index = 0; index < userPreferencesVS.size(); index++) {
			UserPreferencesV preferencesV = userPreferencesVS.get(index);
			PharmacistVO pharmacistVO = responseVO.getPharmacists()
			                                      .get(index);
			assertNotNull(preferencesV);
			assertNotNull(pharmacistVO);
			assertEquals(preferencesV.getPharmacistId(), pharmacistVO.getId());
		}
	}

	/**
	 * sort By firstName descending Order -- user type -> Caregiver --> search term
	 *
	 * @throws Exception
	 */
	@Test
	public void testGetUserPreferencesWithSearch() throws Exception {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PHARMACIST);
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		List<UserPreferencesV> userPreferencesVS = IntStream.range(0, 15)
		                                                    .mapToObj(
				                                                    index -> TestResponseGenerator.generateUserPreferencesV())
		                                                    .collect(Collectors.toList());
		requestVO.setQuery(userPreferencesVS.get(5)
		                                    .getFirstName());
		Mockito.when(mockUserPreferencesVRepository.findAll(Mockito.any(Specification.class)))
		       .thenReturn(userPreferencesVS);
		UserDetailsResponseVO responseVO =
				userPreferencesVService.getUserPreferences(UserRoleEnum.PHARMACIST, requestVO);
		assertNotNull(responseVO);
		assertEquals(1, responseVO.getTotalRecords()
		                          .intValue());
		assertTrue(CollectionUtils.isNotEmpty(responseVO.getPharmacists()));
		assertEquals(1, responseVO.getPharmacists()
		                          .size());
		assertEquals(userPreferencesVS.get(5)
		                              .getPharmacistId(), responseVO.getPharmacists()
		                                                            .get(0)
		                                                            .getId());
	}

	/**
	 * Error scenario for user preference
	 */
	@Test
	public void testGetUserPreferencesErrorCase() {
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PHARMACIST);
		requestVO.setSortOrderEnum(SortOrderEnum.DESC);
		Mockito.when(mockUserPreferencesVRepository.findAll(Mockito.any(Specification.class)))
		       .thenThrow(new RuntimeException());
		try {
			userPreferencesVService.getUserPreferences(UserRoleEnum.PHARMACIST, requestVO);
			Assert.fail();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetAllUserPreferences() {
		List<UserPreferencesV> userPreferencesVS = IntStream.range(0, 10)
		                                                    .mapToObj(index -> TestResponseGenerator.generateUserPreferencesV())
		                                                    .collect(Collectors.toList());
		Mockito.when(mockUserPreferencesVRepository.findAll())
		       .thenReturn(userPreferencesVS);
		List<UserPreferenceVO> preferenceVOS = userPreferencesVService.getAllUserPreferences();
		assertTrue(CollectionUtils.isNotEmpty(preferenceVOS));
		assertEquals(userPreferencesVS.size(), preferenceVOS.size());
		IntStream.range(0, userPreferencesVS.size())
		         .forEach(index -> CommonAsserts.assertUserPreferenceVO(userPreferencesVS.get(index), preferenceVOS.get(index)));
	}
}
