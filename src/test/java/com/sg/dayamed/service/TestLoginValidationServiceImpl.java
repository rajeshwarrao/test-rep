package com.sg.dayamed.service;

import com.sg.dayamed.adapter.users.UserInfoLoadingProcessor;
import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.controller.ws.LoginRequestModel;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.UserDetailsWithJwtModel;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.impl.LoginValidationServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestLoginValidationServiceImpl extends BaseTestCase {

	@InjectMocks
	private LoginValidationService loginValidationService = new LoginValidationServiceImpl();

	@Mock
	private UserDetailsRepository userDetailsRepository;

	@Mock
	private RediseService rediseService;

	@Mock
	private UserInfoLoadingProcessor userInfoLoadingProcessor;

	@Mock
	HttpServletRequest httpRequest;

	@Test
	public void testLoginValidationwithIncorrectCreditials() {
		/*
		 * if username or password incorrect throw exception.
		 */
		try {
			LoginRequestModel loginRequestModel = TestResponseGenerator.getLoginRequestModel();
			Mockito.when(userDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
			       .thenReturn(null)
			       .thenReturn(
					       TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT));
			Mockito.when(rediseService.getDataInRedisByKey(Mockito.any()))
			       .thenReturn(String.valueOf(Boolean.TRUE));
			loginValidationService.loginValidation(loginRequestModel, null, null);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
	}

	@Test
	public void testLoginValidationwithSuccess() throws Exception {
		/*
		 * login success case;
		 */
		LoginRequestModel loginRequestModel = TestResponseGenerator.getLoginRequestModel();
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
		                                                                    ApplicationConstants.PATIENT);
		String userCrent = RandomStringUtils.randomAlphanumeric(15);
		loginRequestModel.setPassword(userCrent);
		userDetails.setPassword(userCrent);
		Mockito.when(userDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(rediseService.getDataInRedisByKey(Mockito.any()))
		       .thenReturn(String.valueOf(Boolean.TRUE));
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		Mockito.when(userInfoLoadingProcessor.getUserDetailsWithModel(Mockito.any(), Mockito.any(), Mockito.any(),
		                                                              Mockito.any()))
		       .thenReturn(userDetailsWithJwtModel);
		userDetailsWithJwtModel = loginValidationService.loginValidation(loginRequestModel, httpRequest, null);
		assertNotNull(userDetailsWithJwtModel);
	}

	@Test
	public void testLoginValidationwithSuccessRedisEncypFalse() throws Exception {
		/*
		 * if encryption disabled  from redis, login success case;
		 */
		LoginRequestModel loginRequestModel = TestResponseGenerator.getLoginRequestModel();
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
		                                                                    ApplicationConstants.PATIENT);
		String userCrent = RandomStringUtils.randomAlphanumeric(15);
		loginRequestModel.setPassword(userCrent);
		userDetails.setPassword(userCrent);
		Mockito.when(userDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(rediseService.getDataInRedisByKey(Mockito.any()))
		       .thenReturn(String.valueOf(Boolean.FALSE));
		UserDetailsWithJwtModel userDetailsWithJwtModel = new UserDetailsWithJwtModel();
		Mockito.when(userInfoLoadingProcessor.getUserDetailsWithModel(Mockito.any(), Mockito.any(), Mockito.any(),
		                                                              Mockito.any()))
		       .thenReturn(userDetailsWithJwtModel);
		userDetailsWithJwtModel = loginValidationService.loginValidation(loginRequestModel, httpRequest, null);
		assertNotNull(userDetailsWithJwtModel);
	}
}
