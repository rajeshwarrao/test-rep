package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.email.EmailService;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.repository.AdherenceRepository;
import com.sg.dayamed.service.impl.AdherenceServiceImpl;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 03/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestAdherenceService extends BaseTestCase {

	@InjectMocks
	private AdherenceService adherenceService = new AdherenceServiceImpl();

	@Mock
	private AdherenceRepository mockAdherenceRepository;

	@Mock
	private ImanticService mockImanticService;

	@Mock
	private EmailService mocEmailService;

	@Mock
	private PrescriptionService mockPrescriptionService;

	@After
	public void tearDown() throws Exception {
		Mockito.reset(mockImanticService, mocEmailService, mockAdherenceRepository, mockPrescriptionService);
	}

	@Test
	public void testAddAdherence() throws Exception {
		AdherenceDataPoints adherenceDataPoints =
				generateRequestObject("adherenceDataPoints-1", AdherenceDataPoints.class, ADHERENCE_DATA_POINTS_FILE);
		adherenceDataPoints.setId(10);

		/**
		 * Case 1 : Consumed Status
		 */
		Mockito.when(mockAdherenceRepository.save(Mockito.any(AdherenceDataPoints.class)))
		       .thenReturn(adherenceDataPoints);
		AdherenceDataPoints persistedData = adherenceService.addAdherence(adherenceDataPoints, "10:00");

		Mockito.verify(mockImanticService, Mockito.times(1))
		       .medicationTaken(adherenceDataPoints, 1, "10:00");
		Mockito.verify(mockImanticService, Mockito.times(0))
		       .medicationTaken(adherenceDataPoints, 0, "10:00");
		assertNotNull(persistedData);
		assertEquals(adherenceDataPoints.getId(), persistedData.getId());

		/**
		 * Case 2 : Skipped Status
		 */
		Mockito.reset(mockImanticService);
		adherenceDataPoints.setConsumptionStatus(Stream.of("skipped")
		                                               .collect(Collectors.toList()));
		persistedData = adherenceService.addAdherence(adherenceDataPoints, "10:00");
		Mockito.verify(mockImanticService, Mockito.times(0))
		       .medicationTaken(adherenceDataPoints, 1, "10:00");
		Mockito.verify(mockImanticService, Mockito.times(1))
		       .medicationTaken(adherenceDataPoints, 0, "10:00");
		assertNotNull(persistedData);
		assertEquals(adherenceDataPoints.getId(), persistedData.getId());

		/**
		 * Case 3 : No_Action
		 */
		Mockito.reset(mockImanticService);
		adherenceDataPoints.setConsumptionStatus(Stream.of("No_Action")
		                                               .collect(Collectors.toList()));
		persistedData = adherenceService.addAdherence(adherenceDataPoints, "10:00");
		Mockito.verify(mockImanticService, Mockito.times(0))
		       .medicationTaken(adherenceDataPoints, 1, "10:00");
		Mockito.verify(mockImanticService, Mockito.times(1))
		       .medicationTaken(adherenceDataPoints, 0, "10:00");
		assertNotNull(persistedData);
		assertEquals(adherenceDataPoints.getId(), persistedData.getId());
	}

	@Test
	public void testFetchAdherenceById() throws Exception {
		AdherenceDataPoints expectedAdherenceDataPoints =
				generateRequestObject("adherenceDataPoints-1", AdherenceDataPoints.class, ADHERENCE_DATA_POINTS_FILE);

		/**
		 * Case 1 : AdherenceDataPoints exists.
		 */
		Mockito.when(mockAdherenceRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(expectedAdherenceDataPoints));
		AdherenceDataPoints actualAdherenceDataPoints = adherenceService.fetchAdherenceById(1);
		assertEquals(expectedAdherenceDataPoints, actualAdherenceDataPoints);

		/**
		 * Case 2 : AdherenceDataPoints does not exist
		 */
		Mockito.when(mockAdherenceRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualAdherenceDataPoints = adherenceService.fetchAdherenceById(1);
		assertEquals(null, actualAdherenceDataPoints);
	}

	@Test
	public void testFindByPrescriptionID() throws Exception {
		AdherenceDataPoints adherenceDataPoints =
				generateRequestObject("adherenceDataPoints-1", AdherenceDataPoints.class, ADHERENCE_DATA_POINTS_FILE);
		List<AdherenceDataPoints> expectedAdherenceDataPoints = Stream.of(adherenceDataPoints)
		                                                              .collect(Collectors.toList());
		Mockito.when(mockAdherenceRepository.findByPrescriptionID(Mockito.anyLong()))
		       .thenReturn(expectedAdherenceDataPoints);
		List<AdherenceDataPoints> actualAdherenceDataPoints = adherenceService.findByprescriptionID(1);
		assertEquals(expectedAdherenceDataPoints, actualAdherenceDataPoints);
	}

	@Test
	public void testFindByPrescribedTimeAndPrescriptionID() throws Exception {
		AdherenceDataPoints expectedAdherenceDataPoints =
				generateRequestObject("adherenceDataPoints-1", AdherenceDataPoints.class, ADHERENCE_DATA_POINTS_FILE);
		Mockito.when(mockAdherenceRepository.findByPrescribedTimeAndPrescriptionID(Mockito.any(), Mockito.anyLong()))
		       .thenReturn(expectedAdherenceDataPoints);
		AdherenceDataPoints actualAdherenceDataPoints =
				adherenceService.findByPrescribedTimeAndprescriptionID(LocalDateTime.now(), 1);
		assertEquals(expectedAdherenceDataPoints, actualAdherenceDataPoints);
	}

	@Test
	public void testFindByPrescribedTimeAndBetweenTimes() throws Exception {
		AdherenceDataPoints adherenceDataPoints =
				generateRequestObject("adherenceDataPoints-1", AdherenceDataPoints.class, ADHERENCE_DATA_POINTS_FILE);
		List<AdherenceDataPoints> expectedAdherenceDataPoints = Stream.of(adherenceDataPoints)
		                                                              .collect(Collectors.toList());
		Mockito.when(mockAdherenceRepository.findByPrescribedTimeBetweenAndPrescriptionID(Mockito.any(), Mockito.any(),
		                                                                                  Mockito.anyLong()))
		       .thenReturn(expectedAdherenceDataPoints);
		List<AdherenceDataPoints> actualAdherenceDataPoints =
				adherenceService.findByPrescribedTimeAndBetweenTimes(LocalDateTime.now(), LocalDateTime.now(), 1);
		assertEquals(expectedAdherenceDataPoints, actualAdherenceDataPoints);
	}

	@Test
	public void testFindAdherencesBetweenPrescribedTime() throws Exception {
		AdherenceDataPoints adherenceDataPoints =
				generateRequestObject("adherenceDataPoints-1", AdherenceDataPoints.class, ADHERENCE_DATA_POINTS_FILE);
		List<AdherenceDataPoints> expectedAdherenceDataPoints = Stream.of(adherenceDataPoints)
		                                                              .collect(Collectors.toList());
		Mockito.when(
				mockAdherenceRepository.findByPrescribedTimeBetweenAndPrescriptionIDIn(Mockito.any(), Mockito.any(),
				                                                                       Mockito.anyList()))
		       .thenReturn(expectedAdherenceDataPoints);
		List<AdherenceDataPoints> actualAdherenceDataPoints =
				adherenceService.findAdherencesBetweenPrescribedTime(LocalDateTime.now(), LocalDateTime.now(),
				                                                     Collections.emptyList());
		assertEquals(expectedAdherenceDataPoints, actualAdherenceDataPoints);
	}

}
