package com.sg.dayamed.service;

import com.sg.dayamed.dosage.frequency.DailyDosageFrequency;
import com.sg.dayamed.dosage.frequency.FrequencyAbstractFactory;
import com.sg.dayamed.entity.NotificationSchedule;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.service.impl.NotificationScheduleServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestNotificationScheduleServiceImpl {

	@Mock
	NotificationScheduleRepository notificationScheduleRepository;

	@Mock
	DateUtility dateUtility;

	@Mock
	private DataSource dataSource;

	@InjectMocks
	private NotificationScheduleServiceImpl notificationScheduleService;

	@Mock
	FrequencyAbstractFactory frequencyAbstractFactory;

	@Test
	public void testAddSchedulerJobWithNoPrescriptionAndZoneId() {
		notificationScheduleService.addSchedulerJob(null, null);
		verify(dateUtility, never()).getDateTimeBasedOnZoneId(anyString());
		verify(notificationScheduleRepository, never()).findByUserIdAndPrescriptionId(anyLong(), anyLong());
	}

	@Test
	public void testAddSchedulerJob(){
		Mockito.when(dateUtility.getDateTimeBasedOnZoneId(anyString())).thenReturn("2019-09-10 10:10");
		Mockito.when(frequencyAbstractFactory.getFrequencyFactory(Mockito.anyString())).thenReturn(new DailyDosageFrequency());
		//for custom template.
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(notificationScheduleRepository.save(Mockito.any())).thenReturn(TestResponseGenerator.generateEmailNotificationSchedule());
		//Mockito.when(dateUtility.convertUTCLocalDateTimeStringToObject(Mockito.any())).thenReturn(LocalDateTime.now());
		boolean responseValue = notificationScheduleService.addSchedulerJob(prescription, "Asia/Kolkata");
		assertTrue(responseValue);
	}

	@Test
	public void testAddSchedulerJobWithPrescriptionAndZoneId() {
		when(dateUtility.getDateTimeBasedOnZoneId(anyString())).thenReturn("2019-10-10 10:10");
		when(notificationScheduleRepository.findByUserIdAndPrescriptionId(anyLong(), anyLong())).thenReturn(
				Collections.emptyList());

		Prescription prescription = new Prescription();
		Patient patient = new Patient();
		prescription.setPatient(patient);
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT);
		patient.setUserDetails(userDetails);

		notificationScheduleService.addSchedulerJob(prescription, "ABC");
		verify(dateUtility).getDateTimeBasedOnZoneId(anyString());
		verify(notificationScheduleRepository).findByUserIdAndPrescriptionId(anyLong(), anyLong());
	}

	@Test
	public void testFetchJobsByDosageInfoIds() {
		List<NotificationSchedule> listNotificationSchedule =
				Stream.of(TestResponseGenerator.generateEmailNotificationSchedule())
				      .collect(
						      Collectors.toList());
		Mockito.when(notificationScheduleRepository.findByDosageInfo_idIn(Mockito.any()))
		       .thenReturn(listNotificationSchedule);
		List<Long> listDosageIds = new ArrayList<>();
		listDosageIds.add(1l);
		List<NotificationSchedule> listActualNotifications =
				notificationScheduleService.fetchJobsByDosageInfoIds(listDosageIds);
		assertNotNull(listActualNotifications);
		assertEquals(listActualNotifications, listNotificationSchedule);
	}

	@Test
	public void testFetchJobsByUserIdAndPrescriptionId(){
		List<NotificationSchedule> listNotificationSchedule =
				Stream.of(TestResponseGenerator.generateEmailNotificationSchedule())
				      .collect(
						      Collectors.toList());
		Mockito.when(notificationScheduleRepository.findByUserIdAndPrescriptionId(Mockito.anyLong(),Mockito.anyLong()))
		       .thenReturn(listNotificationSchedule);
		List<NotificationSchedule> listActualNotifications =
				notificationScheduleService.fetchJobsByUserIdAndPrescriptionId(1l,1l);
		assertNotNull(listActualNotifications);
		assertEquals(listActualNotifications, listNotificationSchedule);
	}

	@Test
	public void testUpdateNotificationSchedulerJobByDosageInfo(){
		List<NotificationSchedule> listNotificationSchedule =
				Stream.of(TestResponseGenerator.generateEmailNotificationSchedule())
				      .collect(
						      Collectors.toList());
		Mockito.when(notificationScheduleRepository.findByActualDateAndDosageInfo_id(Mockito.any(),Mockito.anyLong())).thenReturn(listNotificationSchedule);
		Mockito.when(notificationScheduleRepository.save(Mockito.any())).thenReturn(TestResponseGenerator.generateEmailNotificationSchedule());
		boolean responseVAlue = notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(LocalDateTime.now(),1l, "");
		assertTrue(responseVAlue);
	}

	@Test
	public void testUpdateSchedulerJobByDeviceInfo(){
		List<NotificationSchedule> listNotificationSchedule =
				Stream.of(TestResponseGenerator.generateEmailNotificationSchedule())
				      .collect(
						      Collectors.toList());
		Mockito.when(notificationScheduleRepository.findByActualDateAndDosageInfo_id(Mockito.any(),Mockito.anyLong())).thenReturn(listNotificationSchedule);
		Mockito.when(notificationScheduleRepository.save(Mockito.any())).thenReturn(TestResponseGenerator.generateEmailNotificationSchedule());
		boolean responseVAlue = notificationScheduleService.updateNotificationSchedulerJobByDosageInfo(LocalDateTime.now(),1l, "");
		assertTrue(responseVAlue);
	}
}
