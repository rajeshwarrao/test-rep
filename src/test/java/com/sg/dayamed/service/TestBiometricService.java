package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.entity.Glucometer;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.PulseOximeter;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.BpMonitorModel;
import com.sg.dayamed.pojo.GlucometerModel;
import com.sg.dayamed.pojo.HeartRateModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PulseOximeterModel;
import com.sg.dayamed.pojo.ScaleModel;
import com.sg.dayamed.pojo.StepsModel;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.repository.GlucoMeterRepository;
import com.sg.dayamed.repository.HeartRateRepository;
import com.sg.dayamed.repository.PulseOximeterRepository;
import com.sg.dayamed.repository.ScaleRepository;
import com.sg.dayamed.repository.StepsRepository;
import com.sg.dayamed.service.impl.BiometricServiceImpl;
import com.sg.dayamed.util.Utility;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 04/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestBiometricService extends BaseTestCase {

	@InjectMocks
	BiometricService biometricService = new BiometricServiceImpl();

	@Mock
	HeartRateRepository mockHeartRateRepository;

	@Mock
	ImanticService mockImanticService;

	@Mock
	PulseOximeterRepository mockPulseOximeterRepository;

	@Mock
	Utility mockUtility;

	@Mock
	StepsRepository mockStepsRepository;

	@Mock
	ScaleRepository mockScaleRepository;

	@Mock
	GlucoMeterRepository mockGlucoMeterRepository;

	@Mock
	BpMonitorRepository mockBpMonitorRepository;

	DateTimeFormatter dayamedGeneralFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	@Before
	public void setUp() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		patientModel.setPharmacists(new HashSet<>());
		patientModel.setCaregivers(new HashSet<>());
		patientModel.setProviders(new HashSet<>());
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
	}

	@Test
	public void testAddHeartRate() throws Exception {
		HeartRateModel heartRateModel =
				generateRequestObject("HeartRateModel-1", HeartRateModel.class, BIOMETRIC_SERVICE_FILE);
		heartRateModel.setId(1);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		heartRateModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		heartRateModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		HeartRate heartRate = new HeartRate();
		heartRate.setId(heartRateModel.getId());
		heartRate.setConsumptionStatus(heartRateModel.getConsumptionStatus());
		heartRate.setObservedTime(obsDateTime);
		heartRate.setPrescribedTime(presDateTime);
		Mockito.when(mockHeartRateRepository.save(Mockito.any()))
		       .thenReturn(heartRate);
		Mockito.when(mockImanticService.biometricEvent(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		HeartRateModel actualHeartRateModel = biometricService.addHeartRate(heartRateModel);
		assertNotNull(actualHeartRateModel);
		assertEquals(heartRateModel.getId(), actualHeartRateModel.getId());
		assertEquals(heartRateModel.getConsumptionStatus(), actualHeartRateModel.getConsumptionStatus());
		assertEquals(heartRateModel.getObservedTime(), actualHeartRateModel.getObservedTime());
		assertEquals(heartRateModel.getPrescribedTime(), actualHeartRateModel.getPrescribedTime());
	}

	@Test
	public void testFetchHeartRatesByPatientId() throws Exception {
		HeartRateModel heartRateModel =
				generateRequestObject("HeartRateModel-1", HeartRateModel.class, BIOMETRIC_SERVICE_FILE);
		heartRateModel.setId(1);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		heartRateModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		heartRateModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		HeartRate heartRate = new HeartRate();
		heartRate.setId(heartRateModel.getId());
		heartRate.setConsumptionStatus(heartRateModel.getConsumptionStatus());
		heartRate.setObservedTime(obsDateTime);
		heartRate.setPrescribedTime(presDateTime);
		List<HeartRate> heartRates = Stream.of(heartRate)
		                                   .collect(Collectors.toList());
		Mockito.when(mockHeartRateRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(heartRates);
		List<HeartRateModel> heartRateModels = biometricService.fetchHeartRatesByPatientId(1);
		assertNotNull(heartRateModels);
		assertEquals(1, heartRateModels.size());
		assertEquals(dayamedGeneralFormatter.format(heartRates.get(0)
		                                                      .getObservedTime()), heartRateModels.get(0)
		                                                                                          .getObservedTime());
		assertEquals(dayamedGeneralFormatter.format(heartRates.get(0)
		                                                      .getPrescribedTime()), heartRateModels.get(0)
		                                                                                            .getPrescribedTime());
		assertEquals(heartRates.get(0)
		                       .getId(), heartRateModels.get(0)
		                                                .getId());
	}

	@Test
	public void testAddPulseOximeter() throws Exception {
		PulseOximeterModel pulseOximeterModel =
				generateRequestObject("PulseOximeterModel-1", PulseOximeterModel.class, BIOMETRIC_SERVICE_FILE);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		LocalDateTime obsDateTime = LocalDateTime.now();
		pulseOximeterModel.setTakenTime(obsDateTime);
		pulseOximeterModel.setPatient(patientModel);
		Patient patient = new Patient();
		patient.setId(1);
		Role role = new Role();
		role.setName("Patient");
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		userDetails.setRole(role);
		PulseOximeter pulseOximeter = new PulseOximeter();
		pulseOximeter.setPatient(patient);
		pulseOximeter.setId(1);
		pulseOximeter.setPulse(40);
		pulseOximeter.setSp02("100");
		pulseOximeter.setTakenTime(obsDateTime);
		Mockito.when(mockPulseOximeterRepository.save(Mockito.any()))
		       .thenReturn(pulseOximeter);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Mockito.when(mockImanticService.biometricEvent(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		PulseOximeterModel oximeterModel = biometricService.addPulseOximeter(pulseOximeterModel);
		assertNotNull(oximeterModel);
		assertEquals(pulseOximeterModel.getId(), oximeterModel.getId());
		assertEquals(pulseOximeterModel.getPulse(), oximeterModel.getPulse());
		assertEquals(pulseOximeterModel.getSp02(), oximeterModel.getSp02());
		assertEquals(pulseOximeterModel.getTakenTime(), oximeterModel.getTakenTime());
	}

	@Test
	public void testAddSteps() throws Exception {
		StepsModel stepsModel = generateRequestObject("StepsModel-1", StepsModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		stepsModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		stepsModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Steps steps = new Steps();
		steps.setId(1);
		steps.setPatient(patient);
		steps.setConsumptionStatus(Stream.of("Consumed")
		                                 .collect(Collectors.toList()));
		steps.setObservedTime(obsDateTime);
		steps.setPrescribedTime(presDateTime);
		steps.setPrescriptionID(1);
		Mockito.when(mockStepsRepository.save(Mockito.any()))
		       .thenReturn(steps);
		Mockito.when(mockImanticService.biometricEvent(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		StepsModel actualStepsModel = biometricService.addSteps(stepsModel);
		assertNotNull(actualStepsModel);
		assertEquals(stepsModel.getId(), actualStepsModel.getId());
		assertEquals(stepsModel.getPrescriptionID(), actualStepsModel.getPrescriptionID());
		assertEquals(stepsModel.getPrescribedTime(), actualStepsModel.getPrescribedTime());
		assertEquals(stepsModel.getObservedTime(), actualStepsModel.getObservedTime());
	}

	@Test
	public void testAddScale() throws Exception {
		ScaleModel scaleModel = generateRequestObject("ScaleModel-1", ScaleModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		scaleModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		scaleModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Scale scale = new Scale();
		scale.setReading(scaleModel.getReading());
		scale.setObservedTime(obsDateTime);
		scale.setPatient(patient);
		scale.setPrescribedTime(presDateTime);
		scale.setConsumptionStatus(scaleModel.getConsumptionStatus());
		scale.setDeviceInfoId(scaleModel.getDeviceInfoId());
		scale.setId(scaleModel.getId());
		Mockito.when(mockScaleRepository.save(Mockito.any()))
		       .thenReturn(scale);
		Mockito.when(mockImanticService.biometricEvent(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		ScaleModel model = biometricService.addScale(scaleModel);
		assertNotNull(model);
		assertEquals(scaleModel.getObservedTime(), model.getObservedTime());
		assertEquals(scaleModel.getPrescribedTime(), model.getPrescribedTime());
		assertEquals(scaleModel.getReading(), model.getReading());
		assertEquals(scaleModel.getId(), model.getId());
		assertEquals(scaleModel.getDeviceInfoId(), model.getDeviceInfoId());
		assertEquals(scaleModel.getConsumptionStatus(), model.getConsumptionStatus());
	}

	@Test
	public void testAddGlucometer() throws Exception {
		GlucometerModel glucometerModel =
				generateRequestObject("GlucometerModel-1", GlucometerModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		glucometerModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		glucometerModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Glucometer glucometer = new Glucometer();
		glucometer.setObservedTime(obsDateTime);
		glucometer.setPatient(patient);
		glucometer.setPrescribedTime(presDateTime);
		glucometer.setConsumptionStatus(glucometerModel.getConsumptionStatus());
		glucometer.setId(glucometerModel.getId());
		glucometer.setPrescriptionID(glucometerModel.getPrescriptionID());
		glucometer.setDeviceInfoId(glucometerModel.getDeviceInfoId());
		glucometer.setReadingValue(glucometerModel.getReadingValue());
		Mockito.when(mockGlucoMeterRepository.save(Mockito.any()))
		       .thenReturn(glucometer);
		Mockito.when(mockImanticService.biometricEvent(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		GlucometerModel model = biometricService.addGlucometer(glucometerModel);
		assertNotNull(model);
		assertEquals(glucometerModel.getObservedTime(), model.getObservedTime());
		assertEquals(glucometerModel.getPrescribedTime(), model.getPrescribedTime());
		assertEquals(glucometerModel.getReadingValue(), model.getReadingValue());
		assertEquals(glucometerModel.getDeviceInfoId(), model.getDeviceInfoId());
		assertEquals(glucometerModel.getId(), model.getId());
		assertEquals(glucometerModel.getPrescriptionID(), model.getPrescriptionID());
		assertEquals(glucometerModel.getConsumptionStatus(), model.getConsumptionStatus());
	}

	@Test
	public void testFetchGlucoReadingsByPatientId() throws Exception {
		GlucometerModel glucometerModel =
				generateRequestObject("GlucometerModel-1", GlucometerModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		glucometerModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		glucometerModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		List<GlucometerModel> expectecGlucometerModels = Stream.of(glucometerModel)
		                                                       .collect(Collectors.toList());
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Glucometer glucometer = new Glucometer();
		glucometer.setObservedTime(obsDateTime);
		glucometer.setPatient(patient);
		glucometer.setPrescribedTime(presDateTime);
		glucometer.setConsumptionStatus(glucometerModel.getConsumptionStatus());
		glucometer.setId(glucometerModel.getId());
		glucometer.setPrescriptionID(glucometerModel.getPrescriptionID());
		glucometer.setDeviceInfoId(glucometerModel.getDeviceInfoId());
		glucometer.setReadingValue(glucometerModel.getReadingValue());
		List<Glucometer> expectedGlucometers = Stream.of(glucometer)
		                                             .collect(Collectors.toList());
		Mockito.when(mockGlucoMeterRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(expectedGlucometers);
		List<GlucometerModel> actualGlucometerModels = biometricService.fetchGlocoReadingsByPatientId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualGlucometerModels));
		assertEquals(expectecGlucometerModels.size(), actualGlucometerModels.size());
		assertEquals(expectecGlucometerModels.get(0)
		                                     .getObservedTime(), actualGlucometerModels.get(0)
		                                                                               .getObservedTime());
		assertEquals(expectecGlucometerModels.get(0)
		                                     .getPrescribedTime(), actualGlucometerModels.get(0)
		                                                                                 .getPrescribedTime());
		assertEquals(expectecGlucometerModels.get(0)
		                                     .getReadingValue(), actualGlucometerModels.get(0)
		                                                                               .getReadingValue());
		assertEquals(expectecGlucometerModels.get(0)
		                                     .getDeviceInfoId(), actualGlucometerModels.get(0)
		                                                                               .getDeviceInfoId());
		assertEquals(expectecGlucometerModels.get(0)
		                                     .getId(), actualGlucometerModels.get(0)
		                                                                     .getId());
		assertEquals(expectecGlucometerModels.get(0)
		                                     .getPrescriptionID(), actualGlucometerModels.get(0)
		                                                                                 .getPrescriptionID());
		assertEquals(expectecGlucometerModels.get(0)
		                                     .getConsumptionStatus(), actualGlucometerModels.get(0)
		                                                                                    .getConsumptionStatus());
	}

	@Test
	public void testAddBpReading() throws Exception {
		BpMonitorModel bpMonitorModel =
				generateRequestObject("BpMonitorModel-1", BpMonitorModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		bpMonitorModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		bpMonitorModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		BpMonitor bpMonitor = new BpMonitor();
		bpMonitor.setId(bpMonitorModel.getId());
		bpMonitor.setObservedTime(obsDateTime);
		bpMonitor.setPatient(patient);
		bpMonitor.setPrescribedTime(presDateTime);
		bpMonitor.setConsumptionStatus(bpMonitorModel.getConsumptionStatus());
		bpMonitor.setDeviceInfoId(bpMonitorModel.getDeviceInfoId());
		bpMonitor.setDiastolicpressureValue(bpMonitorModel.getDiastolicpressureValue());
		bpMonitor.setPrescriptionID(bpMonitorModel.getPrescriptionID());
		bpMonitor.setSystolicpressureValue(bpMonitorModel.getSystolicpressureValue());
		Mockito.when(mockBpMonitorRepository.save(Mockito.any()))
		       .thenReturn(bpMonitor);
		Mockito.when(mockImanticService.biometricEvent(Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		BpMonitorModel actualBpMonitorModel = biometricService.addBpReading(bpMonitorModel);
		assertNotNull(actualBpMonitorModel);
		assertEquals(bpMonitorModel.getConsumptionStatus(), actualBpMonitorModel.getConsumptionStatus());
		assertEquals(bpMonitorModel.getObservedTime(), actualBpMonitorModel.getObservedTime());
		assertEquals(bpMonitorModel.getPrescribedTime(), actualBpMonitorModel.getPrescribedTime());
		assertEquals(bpMonitorModel.getId(), actualBpMonitorModel.getId());
		assertEquals(bpMonitorModel.getPrescriptionID(), actualBpMonitorModel.getPrescriptionID());
		assertEquals(bpMonitorModel.getDiastolicpressureValue(), actualBpMonitorModel.getDiastolicpressureValue());
		assertEquals(bpMonitorModel.getSystolicpressureValue(), actualBpMonitorModel.getSystolicpressureValue());
		assertEquals(bpMonitorModel.getDeviceInfoId(), actualBpMonitorModel.getDeviceInfoId());
	}

	@Test
	public void testFetchBpReadingsByPatientId() throws Exception {
		BpMonitorModel bpMonitorModel =
				generateRequestObject("BpMonitorModel-1", BpMonitorModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		bpMonitorModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		bpMonitorModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		List<BpMonitorModel> expectedBpMonitorModels = Stream.of(bpMonitorModel)
		                                                     .collect(Collectors.toList());
		BpMonitor bpMonitor = new BpMonitor();
		bpMonitor.setId(bpMonitorModel.getId());
		bpMonitor.setObservedTime(obsDateTime);
		bpMonitor.setPrescribedTime(presDateTime);
		bpMonitor.setConsumptionStatus(bpMonitorModel.getConsumptionStatus());
		bpMonitor.setDeviceInfoId(bpMonitorModel.getDeviceInfoId());
		bpMonitor.setDiastolicpressureValue(bpMonitorModel.getDiastolicpressureValue());
		bpMonitor.setPrescriptionID(bpMonitorModel.getPrescriptionID());
		bpMonitor.setSystolicpressureValue(bpMonitorModel.getSystolicpressureValue());
		List<BpMonitor> expectedBpMonitors = Stream.of(bpMonitor)
		                                           .collect(Collectors.toList());
		Mockito.when(mockBpMonitorRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(expectedBpMonitors);
		List<BpMonitorModel> actualBpMonitorModels = biometricService.fetchBpeadingsByPatientId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualBpMonitorModels));
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getConsumptionStatus(), actualBpMonitorModels.get(0)
		                                                                                  .getConsumptionStatus());
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getObservedTime(), actualBpMonitorModels.get(0)
		                                                                             .getObservedTime());
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getPrescribedTime(), actualBpMonitorModels.get(0)
		                                                                               .getPrescribedTime());
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getId(), actualBpMonitorModels.get(0)
		                                                                   .getId());
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getPrescriptionID(), actualBpMonitorModels.get(0)
		                                                                               .getPrescriptionID());
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getDiastolicpressureValue(), actualBpMonitorModels.get(0)
		                                                                                       .getDiastolicpressureValue());
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getSystolicpressureValue(), actualBpMonitorModels.get(0)
		                                                                                      .getSystolicpressureValue());
		assertEquals(expectedBpMonitorModels.get(0)
		                                    .getDeviceInfoId(), actualBpMonitorModels.get(0)
		                                                                             .getDeviceInfoId());
	}

	@Test
	public void testFetchGlucoeterByPrescribedTimeAndDeviceInfoId() throws Exception {
		GlucometerModel glucometerModel =
				generateRequestObject("GlucometerModel-1", GlucometerModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		glucometerModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		glucometerModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Glucometer glucometer = new Glucometer();
		glucometer.setObservedTime(obsDateTime);
		glucometer.setPatient(patient);
		glucometer.setPrescribedTime(presDateTime);
		glucometer.setConsumptionStatus(glucometerModel.getConsumptionStatus());
		glucometer.setId(glucometerModel.getId());
		glucometer.setPrescriptionID(glucometerModel.getPrescriptionID());
		glucometer.setDeviceInfoId(glucometerModel.getDeviceInfoId());
		glucometer.setReadingValue(glucometerModel.getReadingValue());
		Mockito.when(mockGlucoMeterRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(glucometer);
		GlucometerModel model =
				biometricService.fetchGlucometerByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNotNull(model);
		assertEquals(glucometerModel.getObservedTime(), model.getObservedTime());
		assertEquals(glucometerModel.getPrescribedTime(), model.getPrescribedTime());
		assertEquals(glucometerModel.getReadingValue(), model.getReadingValue());
		assertEquals(glucometerModel.getDeviceInfoId(), model.getDeviceInfoId());
		assertEquals(glucometerModel.getId(), model.getId());
		assertEquals(glucometerModel.getPrescriptionID(), model.getPrescriptionID());
		assertEquals(glucometerModel.getConsumptionStatus(), model.getConsumptionStatus());

		// null response
		Mockito.reset(mockGlucoMeterRepository);
		Mockito.when(mockGlucoMeterRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(null);
		model = biometricService.fetchGlucometerByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNull(model);
	}

	@Test
	public void testFetchBpMonitorByPrescribedTimeAndDeviceInfoId() throws Exception {
		BpMonitorModel bpMonitorModel =
				generateRequestObject("BpMonitorModel-1", BpMonitorModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		bpMonitorModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		bpMonitorModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		BpMonitor bpMonitor = new BpMonitor();
		bpMonitor.setId(bpMonitorModel.getId());
		bpMonitor.setObservedTime(obsDateTime);
		bpMonitor.setPatient(patient);
		bpMonitor.setPrescribedTime(presDateTime);
		bpMonitor.setConsumptionStatus(bpMonitorModel.getConsumptionStatus());
		bpMonitor.setDeviceInfoId(bpMonitorModel.getDeviceInfoId());
		bpMonitor.setDiastolicpressureValue(bpMonitorModel.getDiastolicpressureValue());
		bpMonitor.setPrescriptionID(bpMonitorModel.getPrescriptionID());
		bpMonitor.setSystolicpressureValue(bpMonitorModel.getSystolicpressureValue());
		Mockito.when(mockBpMonitorRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(bpMonitor);
		BpMonitorModel actualBpMonitorModel =
				biometricService.fetchBpMonitorByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNotNull(actualBpMonitorModel);
		assertEquals(bpMonitorModel.getConsumptionStatus(), actualBpMonitorModel.getConsumptionStatus());
		assertEquals(bpMonitorModel.getObservedTime(), actualBpMonitorModel.getObservedTime());
		assertEquals(bpMonitorModel.getPrescribedTime(), actualBpMonitorModel.getPrescribedTime());
		assertEquals(bpMonitorModel.getId(), actualBpMonitorModel.getId());
		assertEquals(bpMonitorModel.getPrescriptionID(), actualBpMonitorModel.getPrescriptionID());
		assertEquals(bpMonitorModel.getDiastolicpressureValue(), actualBpMonitorModel.getDiastolicpressureValue());
		assertEquals(bpMonitorModel.getSystolicpressureValue(), actualBpMonitorModel.getSystolicpressureValue());
		assertEquals(bpMonitorModel.getDeviceInfoId(), actualBpMonitorModel.getDeviceInfoId());

		// Null response
		Mockito.reset(mockBpMonitorRepository);
		Mockito.when(mockBpMonitorRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(null);
		actualBpMonitorModel =
				biometricService.fetchBpMonitorByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNull(actualBpMonitorModel);
	}

	@Test
	public void testFetchScaleByPrescribedTimeAndDeviceInfoId() throws Exception {
		ScaleModel scaleModel = generateRequestObject("ScaleModel-1", ScaleModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		scaleModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		scaleModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Scale scale = new Scale();
		scale.setReading(scaleModel.getReading());
		scale.setObservedTime(obsDateTime);
		scale.setPatient(patient);
		scale.setPrescribedTime(presDateTime);
		scale.setConsumptionStatus(scaleModel.getConsumptionStatus());
		scale.setDeviceInfoId(scaleModel.getDeviceInfoId());
		scale.setId(scaleModel.getId());
		Mockito.when(mockScaleRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(scale);
		ScaleModel actualScaleModel =
				biometricService.fetchScaleByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNotNull(actualScaleModel);
		assertEquals(scaleModel.getObservedTime(), actualScaleModel.getObservedTime());
		assertEquals(scaleModel.getPrescribedTime(), actualScaleModel.getPrescribedTime());
		assertEquals(scaleModel.getReading(), actualScaleModel.getReading());
		assertEquals(scaleModel.getId(), actualScaleModel.getId());
		assertEquals(scaleModel.getDeviceInfoId(), actualScaleModel.getDeviceInfoId());
		assertEquals(scaleModel.getConsumptionStatus(), actualScaleModel.getConsumptionStatus());

		// Null response
		Mockito.reset(mockScaleRepository);
		Mockito.when(mockScaleRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(null);
		actualScaleModel = biometricService.fetchScaleByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNull(actualScaleModel);
	}

	@Test
	public void testFetchStepsByPrescribedTimeAndDeviceInfoId() throws Exception {
		StepsModel stepsModel = generateRequestObject("StepsModel-1", StepsModel.class, BIOMETRIC_SERVICE_FILE);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		stepsModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		stepsModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Steps steps = new Steps();
		steps.setId(stepsModel.getId());
		steps.setPatient(patient);
		steps.setConsumptionStatus(stepsModel.getConsumptionStatus());
		steps.setObservedTime(obsDateTime);
		steps.setPrescribedTime(presDateTime);
		steps.setPrescriptionID(stepsModel.getPrescriptionID());
		Mockito.when(mockStepsRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(steps);
		StepsModel actualStepsModel =
				biometricService.fetchStepsByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNotNull(actualStepsModel);
		assertEquals(stepsModel.getId(), actualStepsModel.getId());
		assertEquals(stepsModel.getPrescriptionID(), actualStepsModel.getPrescriptionID());
		assertEquals(stepsModel.getPrescribedTime(), actualStepsModel.getPrescribedTime());
		assertEquals(stepsModel.getObservedTime(), actualStepsModel.getObservedTime());

		// null Response
		Mockito.reset(mockStepsRepository);
		Mockito.when(mockStepsRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(null);
		actualStepsModel = biometricService.fetchStepsByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNull(actualStepsModel);
	}

	@Test
	public void testFetchHeartRateByPrescribedTimeAndDeviceInfoId() throws Exception {
		HeartRateModel heartRateModel =
				generateRequestObject("HeartRateModel-1", HeartRateModel.class, BIOMETRIC_SERVICE_FILE);
		heartRateModel.setId(1);
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		heartRateModel.setPrescribedTime(dayamedGeneralFormatter.format(presDateTime));
		LocalDateTime obsDateTime = LocalDateTime.now();
		heartRateModel.setObservedTime(dayamedGeneralFormatter.format(obsDateTime));
		Role role = new Role();
		role.setName("Patient");
		Patient patient = new Patient();
		patient.setId(1);
		patient.setDeviceId("Junit");
		UserDetails userDetails = new UserDetails();
		userDetails.setRole(role);
		userDetails.setId(1l);
		patient.setUserDetails(userDetails);
		HeartRate heartRate = new HeartRate();
		heartRate.setId(heartRateModel.getId());
		heartRate.setConsumptionStatus(heartRateModel.getConsumptionStatus());
		heartRate.setObservedTime(obsDateTime);
		heartRate.setPrescribedTime(presDateTime);
		Mockito.when(mockHeartRateRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(heartRate);
		HeartRateModel actualHeartRateModel =
				biometricService.fetchHeartRateByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNotNull(actualHeartRateModel);
		assertEquals(heartRateModel.getId(), actualHeartRateModel.getId());
		assertEquals(heartRateModel.getConsumptionStatus(), actualHeartRateModel.getConsumptionStatus());
		assertEquals(heartRateModel.getObservedTime(), actualHeartRateModel.getObservedTime());
		assertEquals(heartRateModel.getPrescribedTime(), actualHeartRateModel.getPrescribedTime());

		// null Response
		Mockito.reset(mockHeartRateRepository);
		Mockito.when(mockHeartRateRepository.findByPrescribedTimeAndDeviceInfoId(Mockito.any(), Mockito.anyString()))
		       .thenReturn(null);
		actualHeartRateModel =
				biometricService.fetchHeartRateByPrescribedTimeAndDeviceInfoId(LocalDateTime.now(), "Junit");
		assertNull(actualHeartRateModel);
	}
}
