package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.repository.CaregiverRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.impl.AuthenticateServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.Utility;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 03/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestAuthenticateService extends BaseTestCase {

	@InjectMocks
	AuthenticateService authenticateService = new AuthenticateServiceImpl();

	@Mock
	HttpServletRequest mockHttpServletRequest;

	@Mock
	HttpServletResponse mockHttpServletResponse;

	@Mock
	UserDetailsRepository mockUserDetailsRepository;

	@Mock
	Utility mockUtility;

	@Mock
	ProviderRepository mockProviderRepository;

	@Mock
	HttpSession mockHttpSession;

	@Mock
	PharmacistService mockPharmacistService;

	@Mock
	CaregiverService mockCaregiverService;

	@Mock
	CaregiverRepository mockCaregiverRepository;

	@Mock
	PatientRepository mockPatientRepository;

	@After
	public void tearDown() throws Exception {
		Mockito.reset(mockHttpServletRequest, mockUtility, mockHttpServletResponse, mockHttpSession,
		              mockProviderRepository, mockUserDetailsRepository, mockPharmacistService);
	}

	@Test
	public void testProviderLogin() throws Exception {
		Mockito.when(mockHttpServletRequest.getSession())
		       .thenReturn(mockHttpSession);
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		Role role = new Role();
		role.setId(1);
		role.setName("Provider");
		userDetails.setPassword("password");
		userDetails.setRole(role);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		UserDetailsModel userDetailsModel =
				generateRequestObject("UserDetailsModel-Provider1", UserDetailsModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		Provider provider = new Provider();
		provider.setId(1);
		provider.setUserDetails(userDetails);
		Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(provider);
		ProviderModel providerModel = generateRequestObject("ProviderModel-1", ProviderModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertProviderEntityToModel(Mockito.any(Provider.class)))
		       .thenReturn(providerModel);
		UserDetailsModel detailsModel =
				authenticateService.login("username", "password", "typeOfDevice", mockHttpServletRequest,
				                          mockHttpServletResponse);
		assertNotNull(detailsModel);
		assertEquals(userDetails.getId(), detailsModel.getId());
		assertEquals(role.getName(), detailsModel.getRole()
		                                         .getName());
	}

	@Test
	public void testPharmacistLogin() throws Exception {
		Mockito.when(mockHttpServletRequest.getSession())
		       .thenReturn(mockHttpSession);
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		Role role = new Role();
		role.setId(1);
		role.setName("Pharmacist");
		userDetails.setPassword("password");
		userDetails.setRole(role);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		UserDetailsModel userDetailsModel =
				generateRequestObject("UserDetailsModel-Pharmacist1", UserDetailsModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		PharmacistModel pharmacistModel =
				generateRequestObject("PharmacistModel-1", PharmacistModel.class, USER_DETAILS_FILE);
		Mockito.when(mockPharmacistService.fetchPharmacistByUserId(Mockito.anyLong()))
		       .thenReturn(pharmacistModel);
		UserDetailsModel detailsModel =
				authenticateService.login("username", "password", "typeOfDevice", mockHttpServletRequest,
				                          mockHttpServletResponse);
		assertNotNull(detailsModel);
		assertEquals(userDetails.getId(), detailsModel.getId());
		assertEquals(role.getName(), detailsModel.getRole()
		                                         .getName());
	}

	@Test
	public void testCaregiverLogin() throws Exception {
		Mockito.when(mockHttpServletRequest.getSession())
		       .thenReturn(mockHttpSession);
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		Role role = new Role();
		role.setId(1);
		role.setName("Caregiver");
		userDetails.setPassword("password");
		userDetails.setRole(role);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		UserDetailsModel userDetailsModel =
				generateRequestObject("UserDetailsModel-Caregiver1", UserDetailsModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		Caregiver caregiver = new Caregiver();
		caregiver.setId(1);
		caregiver.setUserDetails(userDetails);
		Mockito.when(mockCaregiverService.fetchCaregiverByUserId(Mockito.anyLong()))
		       .thenReturn(caregiver);
		Mockito.when(mockCaregiverRepository.save(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiver);
		CaregiverModel caregiverModel =
				generateRequestObject("CaregiverModel-1", CaregiverModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any()))
		       .thenReturn(caregiverModel);
		UserDetailsModel detailsModel =
				authenticateService.login("username", "password", "typeOfDevice", mockHttpServletRequest,
				                          mockHttpServletResponse);
		assertNotNull(detailsModel);
		assertEquals(userDetails.getId(), detailsModel.getId());
		assertEquals(role.getName(), detailsModel.getRole()
		                                         .getName());
	}

	@Test
	public void testPatientLogin() throws Exception {
		Mockito.when(mockHttpServletRequest.getSession())
		       .thenReturn(mockHttpSession);
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		Role role = new Role();
		role.setId(1);
		role.setName(ApplicationConstants.DAYAMED_PATIENT_ROLE);
		userDetails.setPassword("password");
		userDetails.setRole(role);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		UserDetailsModel userDetailsModel =
				generateRequestObject("UserDetailsModel-Patient1", UserDetailsModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		Patient patient = new Patient();
		patient.setId(1);
		patient.setUserDetails(userDetails);
		Mockito.when(mockPatientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		UserDetailsModel detailsModel =
				authenticateService.login("username", "password", "typeOfDevice", mockHttpServletRequest,
				                          mockHttpServletResponse);
		assertNotNull(detailsModel);
		assertEquals(userDetails.getId(), detailsModel.getId());
		assertEquals(role.getName(), detailsModel.getRole()
		                                         .getName());
	}

	@Test
	public void testAdminLogin() throws Exception {
		Mockito.when(mockHttpServletRequest.getSession())
		       .thenReturn(mockHttpSession);
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		Role role = new Role();
		role.setId(1);
		role.setName("Admin");
		userDetails.setPassword("password");
		userDetails.setRole(role);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		UserDetailsModel userDetailsModel =
				generateRequestObject("UserDetailsModel-Patient1", UserDetailsModel.class, USER_DETAILS_FILE);
		userDetailsModel.getRole()
		                .setName("Admin");
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		UserDetailsModel detailsModel =
				authenticateService.login("username", "password", "typeOfDevice", mockHttpServletRequest,
				                          mockHttpServletResponse);
		assertNotNull(detailsModel);
		assertEquals(userDetails.getId(), detailsModel.getId());
		assertEquals(role.getName(), detailsModel.getRole()
		                                         .getName());
	}

	@Test
	public void testEmailIdLogin() throws Exception {
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		Role role = new Role();
		role.setId(1);
		role.setName("Provider");
		userDetails.setPassword("password");
		userDetails.setRole(role);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		UserDetailsModel userDetailsModel =
				generateRequestObject("UserDetailsModel-Provider1", UserDetailsModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		UserDetailsModel detailsModel = authenticateService.login("test@gmail.com");
		assertNotNull(detailsModel);
		assertEquals(userDetails.getId(), detailsModel.getId());
		assertEquals(role.getName(), detailsModel.getRole()
		                                         .getName());
	}

}
