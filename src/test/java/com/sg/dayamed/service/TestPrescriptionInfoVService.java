package com.sg.dayamed.service;

import com.sg.dayamed.entity.view.PrescriptionInfoV;
import com.sg.dayamed.repository.view.PrescriptionInfoVRepository;
import com.sg.dayamed.service.v1.PrescriptionInfoVService;
import com.sg.dayamed.service.v1.impl.PrescriptionInfoVServiceImpl;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 02/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPrescriptionInfoVService {

	@InjectMocks
	private PrescriptionInfoVService prescriptionInfoVService = new PrescriptionInfoVServiceImpl();

	@Mock
	private PrescriptionInfoVRepository mockPrescriptionInfoVRepository;

	@Test
	public void testGetPatientsByPrescriptionStatus() throws Exception {
		/**
		 * Case 1 : with out exception
		 */
		List<PrescriptionInfoV> prescriptionInfoVS = IntStream.range(0, 15)
		                                                      .mapToObj(
				                                                      index -> TestResponseGenerator.generatePrescriptionInfoV())
		                                                      .collect(Collectors.toList());
		Mockito.when(
				mockPrescriptionInfoVRepository.findByStatusIgnoreCaseAndPharmacistIdAndPatientIdIn(Mockito.anyString(),
				                                                                                    Mockito.anyLong(),
				                                                                                    Mockito.anyList()))
		       .thenReturn(prescriptionInfoVS);
		List<Long> expectedIds = prescriptionInfoVS.stream()
		                                           .map(PrescriptionInfoV::getPatientId)
		                                           .distinct()
		                                           .collect(Collectors.toList());
		List<Long> actualIds =
				prescriptionInfoVService.getPatientsByPrescriptionStatus("1", 1L, null, Collections.emptyList());
		assertNotNull(actualIds);
		assertEquals(expectedIds, actualIds);

		/**
		 * Case 2 : Exception occurred
		 */
		Mockito.reset(mockPrescriptionInfoVRepository);
		Mockito.when(
				mockPrescriptionInfoVRepository.findByStatusIgnoreCaseAndPharmacistIdAndPatientIdIn(Mockito.anyString(),
				                                                                                    Mockito.anyLong(),
				                                                                                    Mockito.anyList()))
		       .thenThrow(new RuntimeException());
		try {
			prescriptionInfoVService.getPatientsByPrescriptionStatus("1", 1L, null, Collections.emptyList());
			Assert.fail();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

}
