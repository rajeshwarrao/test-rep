package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.pojo.ConsumerGoodsModel;
import com.sg.dayamed.repository.ConsumerGoodsRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.sg.dayamed.util.TestResponseGenerator.generateConsumerGoods;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class ConsumerGoodsServiceImplTest {

	@InjectMocks
	private ConsumerGoodsServiceImpl consumerGoodsService;

	@Mock
	private ConsumerGoodsRepository consumerGoodsRepository;

	@Test
	public void testGetConsumerGoodsByType() {
		ConsumerGoods chips = generateConsumerGoods(1L, "chips");
		ConsumerGoods fruits = generateConsumerGoods(2L, "fruits");
		Mockito.when(consumerGoodsRepository.findByType(any()))
		       .thenReturn(Stream.of(chips, fruits)
		                         .collect(Collectors.toList()));
		final List<ConsumerGoodsModel> goods = consumerGoodsService.getConsumerGoodsByType("electronic");
		assertEquals(2, goods.size());
		checkConsumerGoodsInResponse(chips, goods.get(0));
		checkConsumerGoodsInResponse(fruits, goods.get(1));
	}

	@Test
	public void testGetConsumerGoodsByTypeWhenNoGoodsFound() {
		Mockito.when(consumerGoodsRepository.findByType(any()))
		       .thenReturn(Collections.emptyList());
		final List<ConsumerGoodsModel> goods = consumerGoodsService.getConsumerGoodsByType("electronic");
		assertTrue(goods.isEmpty());
	}

	private void checkConsumerGoodsInResponse(ConsumerGoods source, ConsumerGoodsModel target) {
		assertEquals(source.getName(), target.getName());
		assertEquals(source.getImageURL(), target.getImageURL());
		assertEquals(source.getId(), target.getId());
		assertEquals(source.getPurpose(), target.getPurpose());
		assertEquals(source.getType(), target.getType());
		assertEquals(source.getDescription(), target.getDescription());
	}
}
