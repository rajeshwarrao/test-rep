package com.sg.dayamed.service.impl;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.service.TokenNotValidationService;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.util.StreamUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 01/Aug/2019
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({FileInputStream.class, TokenNotValidationServiceImpl.class, StreamUtils.class})
public class TokenNotValidationServiceTest {

	@InjectMocks
	private TokenNotValidationService tokenNotValidationService = new TokenNotValidationServiceImpl();

	@Mock
	HttpServletResponse response;

	String imageName = "images.jpg";

	@Test
	public void testFetchNDCImages() throws Exception {
		// Case 1: image is null
		try {
			tokenNotValidationService.fetchNDCImages(null, response);
		} catch (DataValidationException e) {
			assertTrue(true);
		}

		//Case 2 : success

		FileInputStream fileInputStream = PowerMockito.mock(FileInputStream.class);
		PowerMockito.whenNew(FileInputStream.class)
		            .withAnyArguments()
		            .thenReturn(fileInputStream);
		PowerMockito.mock(StreamUtils.class);
		PowerMockito.mockStatic(StreamUtils.class);
		String image = imageName;
		Mockito.when(response.getOutputStream())
		       .thenReturn(TestResponseGenerator.genrateServletOutputStream());
		String responseStatus = tokenNotValidationService.fetchNDCImages(image, response);
		assertNotNull(responseStatus);
		assertEquals(ApplicationConstants.SUCCESS, responseStatus);
	}

	@Test
	public void testGetDeviceImageOrmedicineImageOrProfileImagetestOrImage() throws Exception {
		//Case 1: image is null
		try {
			tokenNotValidationService.getDeviceImageOrmedicineImageOrProfileImagetestOrImage(null, response);
		} catch (ApplicationException e) {
			assertTrue(true);
		}

		//Case 2 : success
		FileInputStream fileInputStream = PowerMockito.mock(FileInputStream.class);
		PowerMockito.whenNew(FileInputStream.class)
		            .withAnyArguments()
		            .thenReturn(fileInputStream);
		PowerMockito.mock(StreamUtils.class);
		PowerMockito.mockStatic(StreamUtils.class);
		String image = imageName;
		Mockito.when(response.getOutputStream())
		       .thenReturn(TestResponseGenerator.genrateServletOutputStream());
		String responseStatus = tokenNotValidationService.getDeviceImageOrmedicineImageOrProfileImagetestOrImage(image,
		                                                                                                         response);
		assertNotNull(responseStatus);
		assertEquals(ApplicationConstants.SUCCESS, responseStatus);
	}
}
