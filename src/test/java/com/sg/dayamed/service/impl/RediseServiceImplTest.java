package com.sg.dayamed.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RediseServiceImplTest {

	@InjectMocks
	private RediseServiceImpl rediseService;

	@Mock
	private RedisTemplate<String, Object> redisUserTemplate;

	@Mock
	private ValueOperations<String, Object> valueOperations;

	@Test
	public void testGetDataInRedisByKeyOrFail() {
		when(valueOperations.get(anyString())).thenReturn("value");
		when(redisUserTemplate.opsForValue()).thenReturn(valueOperations);
		final Object value = rediseService.getDataInRedisByKeyOrFail("key");
		assertEquals("value", value);
	}

	@Test(expected = RuntimeException.class)
	public void testGetDataInRedisByKeyOrFailWhenKeyNotFound() {
		when(redisUserTemplate.opsForValue()).thenReturn(valueOperations);
		when(valueOperations.get(anyString())).thenReturn(null);
		rediseService.getDataInRedisByKeyOrFail("key");
	}
}
