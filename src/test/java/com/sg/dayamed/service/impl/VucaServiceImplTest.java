package com.sg.dayamed.service.impl;

import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.pojo.VucaTokenModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.service.util.APIErrorKeys;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VucaServiceImplTest {

	private static final String MEDS_ONCUE_WIDGET_URL = "http://widget.medsoncue.com/";

	private static final String MEDS_ONCUE_BASE_URL = "http://medsoncue.com/";

	private static final String PARTNER_KEY = RandomStringUtils.randomAlphabetic(10);

	@InjectMocks
	private VucaServiceImpl vucaService;

	@Mock
	private RediseService rediseService;

	@Mock
	private RestTemplate restTemplate;

	private static final String VUCA_TOKEN = RandomStringUtils.randomAlphabetic(30);

	private static final String PHARMACY_ID = RandomStringUtils.randomAlphabetic(10);

	private static final String NDC = "088909001";

	@Test
	public void testVucaFetchVideoURLWhenTokenIsFoundInRedis() throws ApplicationException {
		when(rediseService.getDataInRedisByKey(RedisKeyConstants.vucavediotoken)).thenReturn(VUCA_TOKEN);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL)).thenReturn(
				MEDS_ONCUE_WIDGET_URL);

		final String vucaVideoURL = vucaService.fetchVucaVideoURL(NDC);
		assertEquals(buildExpectedVideoURL(), vucaVideoURL);
		verifyZeroInteractions(restTemplate);
	}

	private String buildExpectedVideoURL() {
		return MEDS_ONCUE_WIDGET_URL + VUCA_TOKEN + "&c=" + PHARMACY_ID + "&idtype=1&drugid=" + NDC + "&l=en";
	}

	@Test
	public void testVucaFetchVideoURLWhenGettingTokenFromMedsOnCue() throws Exception {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucavediopartnerkey)).thenReturn(PARTNER_KEY);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_BASE_URL)).thenReturn(
				MEDS_ONCUE_BASE_URL);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL)).thenReturn(
				MEDS_ONCUE_WIDGET_URL);
		when(restTemplate.getForEntity(anyString(), eq(VucaTokenModel.class))).thenReturn(
				ResponseEntity.ok(buildVucaTokenModel()));

		final String vucaVideoURL = vucaService.fetchVucaVideoURL(NDC);

		assertEquals(buildExpectedVideoURL(), vucaVideoURL);
		ArgumentCaptor<Integer> expiryTimeArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
		verify(rediseService).saveObjectWithExpire(eq(RedisKeyConstants.vucavediotoken), eq(VUCA_TOKEN),
		                                           expiryTimeArgumentCaptor.capture());
		final Integer expiryTime = expiryTimeArgumentCaptor.getValue();
		assertTrue(expiryTime >= 1435 && expiryTime <= 1440);
	}

	private VucaTokenModel buildVucaTokenModel() {
		final VucaTokenModel vucaTokenModel = new VucaTokenModel();
		vucaTokenModel.setExpirationDateTime_UTC(getDateTimeInUTCTomorrowThisTime());
		vucaTokenModel.setBaseUrl(MEDS_ONCUE_BASE_URL + "?t=" + VUCA_TOKEN);
		return vucaTokenModel;
	}

	private String getDateTimeInUTCTomorrowThisTime() {
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("UTC"));
		return localDateTime.plusDays(1)
		                    .toString();
	}

	@Test
	public void testVucaFetchVideoURLWhenUnableToGetTokenFromMedsOnCueWithEmptyBaseUrl() {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucavediopartnerkey)).thenReturn(PARTNER_KEY);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_BASE_URL)).thenReturn(
				MEDS_ONCUE_BASE_URL);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL)).thenReturn(
				MEDS_ONCUE_WIDGET_URL);

		VucaTokenModel vucaTokenModel = new VucaTokenModel();
		final String errorMessage = "Cannot give you token now";
		vucaTokenModel.setStatusMessage(errorMessage);
		when(restTemplate.getForEntity(anyString(), eq(VucaTokenModel.class))).thenReturn(
				ResponseEntity.ok(vucaTokenModel));

		try {
			vucaService.fetchVucaVideoURL(NDC);
			fail();
		} catch (ApplicationException exception) {
			assertEquals(errorMessage, exception.getErrorParams()[0]);
			assertEquals(APIErrorKeys.ERROR_UNABLE_TO_GET_VUCA_TOKEN, exception.getErrorKey());
		}
	}

	@Test
	public void testVucaFetchVideoURLWhenUnableToGetTokenFromMedsOnCueWithStatusNotOk() {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucavediopartnerkey)).thenReturn(PARTNER_KEY);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_BASE_URL)).thenReturn(
				MEDS_ONCUE_BASE_URL);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL)).thenReturn(
				MEDS_ONCUE_WIDGET_URL);

		VucaTokenModel vucaTokenModel = new VucaTokenModel();
		final String errorMessage = "Cannot give you token now";
		vucaTokenModel.setStatusMessage(errorMessage);
		when(restTemplate.getForEntity(anyString(), eq(VucaTokenModel.class))).thenReturn(ResponseEntity.status(500)
		                                                                                                .body(vucaTokenModel));

		try {
			vucaService.fetchVucaVideoURL(NDC);
			fail();
		} catch (ApplicationException exception) {
			assertEquals(errorMessage, exception.getErrorParams()[0]);
			assertEquals(APIErrorKeys.ERROR_UNABLE_TO_GET_VUCA_TOKEN, exception.getErrorKey());
		}
	}

	@Test
	public void testVucaFetchVideoURLWhenUnableToGetTokenFromMedsOnCueWithStatusNotOkAndNoContent() {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucavediopartnerkey)).thenReturn(PARTNER_KEY);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_BASE_URL)).thenReturn(
				MEDS_ONCUE_BASE_URL);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL)).thenReturn(
				MEDS_ONCUE_WIDGET_URL);

		when(restTemplate.getForEntity(anyString(), eq(VucaTokenModel.class))).thenReturn(ResponseEntity.status(500)
		                                                                                                .body(null));

		try {
			vucaService.fetchVucaVideoURL(NDC);
			fail();
		} catch (ApplicationException exception) {
			assertEquals("", exception.getErrorParams()[0]);
			assertEquals(APIErrorKeys.ERROR_UNABLE_TO_GET_VUCA_TOKEN, exception.getErrorKey());
		}
	}

	@Test(expected = RuntimeException.class)
	public void testVucaFetchVideoURLWhenUnableToGetPartnerKeyFromRedis() throws Exception {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucavediopartnerkey)).thenThrow(
				new RuntimeException("Key not found in redis"));
		lenient().when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_BASE_URL))
		         .thenReturn(MEDS_ONCUE_BASE_URL);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);

		vucaService.fetchVucaVideoURL(NDC);
	}

	@Test(expected = RuntimeException.class)
	public void testVucaFetchVideoURLWhenUnableToGetBaseURLFromRedis() throws Exception {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucavediopartnerkey)).thenReturn(PARTNER_KEY);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_BASE_URL)).thenThrow(
				new RuntimeException("Key not found in redis"));
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL)).thenReturn(
				MEDS_ONCUE_WIDGET_URL);
		vucaService.fetchVucaVideoURL(NDC);
	}

	@Test(expected = RuntimeException.class)
	public void testVucaFetchVideoURLWhenUnableToGetWidgetURLFromRedis() throws Exception {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenReturn(PHARMACY_ID);
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL)).thenThrow(
				new RuntimeException("Key not found in redis"));
		vucaService.fetchVucaVideoURL(NDC);
	}

	@Test(expected = RuntimeException.class)
	public void testVucaFetchVideoURLWhenUnableToGetPharmacyIdFromRedis() throws Exception {
		when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.vucapharmacyID)).thenThrow(
				new RuntimeException("Key not found in redis"));
		lenient().when(rediseService.getDataInRedisByKeyOrFail(RedisKeyConstants.MEDS_ONCUE_WIDGET_URL))
		         .thenReturn(MEDS_ONCUE_WIDGET_URL);
		vucaService.fetchVucaVideoURL(NDC);
	}
}
