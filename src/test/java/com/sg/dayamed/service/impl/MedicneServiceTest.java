package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.NDCImageModel;
import com.sg.dayamed.repository.MedicineRepository;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created By Gorantla, Eresh on 01/Aug/2019
 **/
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.security.*")
@PrepareForTest({MedicineServiceImpl.class, MedicineServiceImplTest.class})
public class MedicneServiceTest {

	private static final String PATH_IN_DB = "http://192.168.1.86:8085/images/NDC/";

	private String absolutePath;

	@InjectMocks
	private MedicineServiceImpl medicineService;

	@Mock
	private MedicineRepository medicineRepository;

	private Medicine alrex;

	private Medicine aricept;

	private List<Medicine> medicines;

	@Before
	public void init() throws Exception {
		File file = ResourceUtils.getFile("classpath:images/NDCImages/");
		absolutePath = file.getAbsolutePath();
		alrex = TestResponseGenerator.generateMedicine();
		aricept = TestResponseGenerator.generateMedicine();
		medicines = new ArrayList<>();
		medicines.add(alrex);
		medicines.add(aricept);
	}

	@Test
	public void testUpdateMedicineImageURLWhenNoImageExistsInThePath() throws IOException {
		mockStatic(Files.class);
		PowerMockito.when(Files.list(any(Path.class)))
		            .thenReturn(Stream.empty());

		NDCImageModel nDCImageModel = new NDCImageModel();
		nDCImageModel.setImagePath("/tmp/some_path");
		nDCImageModel.setUrlPathInDB(PATH_IN_DB);
		try {
			medicineService.updateImages(nDCImageModel);
			fail();
		} catch (DataValidationException exception) {
			assertEquals(APIErrorFields.IMAGE_PATH, exception.getFieldName());
			assertEquals(APIErrorKeys.ERROR_NO_IMAGE_IN_IMAGE_DIRECTORY, exception.getErrorKey());
		}
	}
}
