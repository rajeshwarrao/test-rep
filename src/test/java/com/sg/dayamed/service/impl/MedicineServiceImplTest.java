package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.NDCImageModel;
import com.sg.dayamed.repository.MedicineRepository;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@RunWith(MockitoJUnitRunner.class)
@PowerMockIgnore("javax.security.*")
@PrepareForTest({MedicineServiceImpl.class, MedicineServiceImplTest.class})
public class MedicineServiceImplTest {

	private static final String PATH_IN_DB = "http://192.168.1.86:8085/images/NDC/";

	private String absolutePath;

	@InjectMocks
	private MedicineServiceImpl medicineService;

	@Mock
	private MedicineRepository medicineRepository;

	private Medicine alrex;

	private Medicine aricept;

	private List<Medicine> medicines;

	@Before
	public void init() throws Exception {
		File file = ResourceUtils.getFile("classpath:images/NDCImages/");
		absolutePath = file.getAbsolutePath();
		alrex = TestResponseGenerator.generateMedicine();
		aricept = TestResponseGenerator.generateMedicine();
		medicines = new ArrayList<>();
		medicines.add(alrex);
		medicines.add(aricept);
	}

	@Test
	public void testUpdateImages() throws Exception {
		NDCImageModel nDCImageModel = new NDCImageModel();
		nDCImageModel.setImagePath(absolutePath);
		nDCImageModel.setUrlPathInDB(PATH_IN_DB);

		Mockito.when(medicineRepository.findByNdcCodeIn(any()))
		       .thenReturn(medicines);
		Mockito.when(medicineRepository.saveAll(any()))
		       .thenReturn(medicines);
		medicineService.updateImages(nDCImageModel);

		ArgumentCaptor<List> savedMedicinesCaptor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(medicineRepository)
		       .saveAll(savedMedicinesCaptor.capture());
		final Medicine savedFirstMedicine = (Medicine) savedMedicinesCaptor.getValue()
		                                                                   .get(0);
		assertEquals(buildExpectedImagePath(alrex), savedFirstMedicine.getImageURL());
		final Medicine savedSecondMedicine = (Medicine) savedMedicinesCaptor.getValue()
		                                                                    .get(1);
		assertEquals(buildExpectedImagePath(aricept), savedSecondMedicine.getImageURL());
	}

	private String buildExpectedImagePath(Medicine med) {
		return PATH_IN_DB + med.getNdcCode() + ".jpeg";
	}

	@Test
	public void testUpdateImagesWhenImagePathDoesNotExist() throws Exception {
		String absolutePathDummy = absolutePath + "dummy";
		NDCImageModel nDCImageModel = new NDCImageModel();
		nDCImageModel.setImagePath(absolutePathDummy);
		nDCImageModel.setUrlPathInDB(PATH_IN_DB);

		try {
			medicineService.updateImages(nDCImageModel);
		} catch (DataValidationException exception) {
			assertEquals(APIErrorFields.IMAGE_PATH, exception.getFieldName());
			assertEquals(APIErrorKeys.ERROR_IMAGE_DIRECTORY_NOT_ACCESSIBLE, exception.getErrorKey());
		}
	}

	@Test
	public void testGetMedicinesWhenNameFilterIsNull() {
		final Page<Medicine> medicines = medicineService.getMedicines(null, 0, 10);
		assertEquals(0, medicines.getTotalElements());
	}

	@Test
	public void testGetMedicinesWhenNameFilterIsEmpty() {
		final Page<Medicine> medicines = medicineService.getMedicines("", 0, 10);
		assertEquals(0, medicines.getTotalElements());
	}

	@Test
	public void testGetMedicinesWhenNameFilterLengthLessThan3Characters() {
		final Page<Medicine> medicines = medicineService.getMedicines("AB", 0, 10);
		assertEquals(0, medicines.getTotalElements());
	}

	@Test
	public void testGetMedicinesWithValidFilter() {
		final Medicine medicine = TestResponseGenerator.generateMedicine();
		final Page<Medicine> resultPage = new PageImpl<>(Stream.of(medicine)
		                                                       .collect(Collectors.toList()));
		Mockito.when(medicineRepository.findByNameContainingOrderByName(eq(medicine.getName()), any()))
		       .thenReturn(resultPage);
		final Page<Medicine> medicines = medicineService.getMedicines(medicine.getName(), 0, 10);
		assertEquals(1, medicines.getTotalElements());
		assertEquals(medicine.getName(), medicines.getContent()
		                                          .get(0)
		                                          .getName());
	}

}
