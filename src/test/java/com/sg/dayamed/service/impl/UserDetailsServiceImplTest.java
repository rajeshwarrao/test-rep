package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PasswordChangeReqModel;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyLong;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

	@InjectMocks
	private UserDetailsServiceImpl userDetailsService;

	@Mock
	private UserDetailsRepository userDetailsRepository;

	@Test
	public void testChangePasswordWhenUserDetailsNotFound() {
		Mockito.when(userDetailsRepository.findById(anyLong()))
		       .thenReturn(Optional.empty());

		try {
			userDetailsService.changePassword(new PasswordChangeReqModel(), 1L);
			fail();
		} catch (DataValidationException validationException) {
			assertEquals(APIErrorFields.USER_ID, validationException.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_USER_ID, validationException.getErrorKey());
		}
	}

	@Test
	public void testChangePasswordWhenCurrentPasswordIsInvalid() {
		//Current password does not match with existing password
		final UserDetails userDetails = TestResponseGenerator.generateUserDetails("male", "Black", "provider");
		userDetails.setPassword("some_password");
		Mockito.when(userDetailsRepository.findById(anyLong()))
		       .thenReturn(Optional.of(userDetails));

		PasswordChangeReqModel passwordChangeReqModel = buildPasswordRequestModel("some_other_password", "");
		try {
			userDetailsService.changePassword(passwordChangeReqModel, 1L);
			fail();
		} catch (DataValidationException validationException) {
			assertEquals(APIErrorFields.CURRENT_CREDENTIAL, validationException.getFieldName());
			assertEquals(APIErrorKeys.ERRORS_INVALID_CURRENT_CREDENTIALS, validationException.getErrorKey());
		}
	}

	@Test
	public void testChangePasswordSuccess() throws DataValidationException {
		final UserDetails userDetails = TestResponseGenerator.generateUserDetails("male", "Black", "provider");
		userDetails.setPassword("password");
		Mockito.when(userDetailsRepository.findById(anyLong()))
		       .thenReturn(Optional.of(userDetails));

		PasswordChangeReqModel passwordChangeReqModel = buildPasswordRequestModel("password", "new_password");
		userDetailsService.changePassword(passwordChangeReqModel, 1L);

		ArgumentCaptor<UserDetails> userDetailsArgumentCaptor = ArgumentCaptor.forClass(UserDetails.class);
		Mockito.verify(userDetailsRepository)
		       .save(userDetailsArgumentCaptor.capture());
		assertEquals("new_password", userDetailsArgumentCaptor.getValue()
		                                                      .getPassword());
	}

	private PasswordChangeReqModel buildPasswordRequestModel(String currentPassword, String newPassword) {
		PasswordChangeReqModel passwordChangeReqModel = new PasswordChangeReqModel();
		passwordChangeReqModel.setCurrentPassword(currentPassword);
		passwordChangeReqModel.setNewPassword(newPassword);
		return passwordChangeReqModel;
	}
}
