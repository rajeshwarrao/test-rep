package com.sg.dayamed.service.impl;

import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.service.UserDetailsService;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserPreferenceServiceImplTest {

	@InjectMocks
	private UserPreferenceServiceImpl userPreferenceService;

	@Mock
	private UserPreferenceRepository userPreferenceRepository;

	@Spy
	private ObjectMapper objectMapper;

	@Mock
	private UserDetailsService userDetailsService;

	@Mock
	private Utility utility;

	@Test
	public void testUpdatePreferenceWhenPreferenceExistsAndAllValuesProvided() throws Exception {
		final UserPreference userPreference = TestResponseGenerator.generateUserPreference();
		when(userPreferenceRepository.findByUserDetailsId(anyLong())).thenReturn(userPreference);
		final UserPreferenceModel settingUpdateRequest = UserPreferenceModel.builder()
		                                                                    .emailFlag(false)
		                                                                    .languagePreference(
				                                                                    Locale.FRENCH.getDisplayLanguage())
		                                                                    .firstConsumption("MID")
		                                                                    .smsFlag(false)
		                                                                    .pushNotificationFlag(false)
		                                                                    .build();
		userPreferenceService.updateUserPreference(NumberUtils.LONG_ONE, settingUpdateRequest);

		ArgumentCaptor<UserPreference> userPreferenceArgumentCaptor = ArgumentCaptor.forClass(UserPreference.class);
		verify(userPreferenceRepository).save(userPreferenceArgumentCaptor.capture());
		final UserPreference savedUserPreference = userPreferenceArgumentCaptor.getValue();
		final UserPreferenceModel savedSettings =
				objectMapper.readValue(savedUserPreference.getUserPrefSetting(), UserPreferenceModel.class);
		assertFalse(savedSettings.getSmsFlag());
		assertFalse(savedSettings.getEmailFlag());
		assertEquals(Locale.FRENCH.getDisplayLanguage(), savedSettings.getLanguagePreference());
		assertFalse(savedSettings.getPushNotificationFlag());
		verify(utility).updateDosageInfoOfprescriptions(NumberUtils.LONG_ONE, "MID");
	}

	@Test
	public void testUpdatePreferenceWhenNoPreferenceExists() throws Exception {
		/**
		 * When preference does not exist, user details should be fetched from user details service
		 */
		when(userPreferenceRepository.findByUserDetailsId(anyLong())).thenReturn(null);

		userPreferenceService.updateUserPreference(NumberUtils.LONG_ONE, new UserPreferenceModel());

		verify(userDetailsService).fetchDecryptedUserDetailsById(anyLong());
		verify(userPreferenceRepository).save(any(UserPreference.class));
	}

	@Test
	public void testUpdatePreferenceWhenSomeValuesAreNull() throws Exception {
		final UserPreference userPreference = TestResponseGenerator.generateUserPreference();
		when(userPreferenceRepository.findByUserDetailsId(anyLong())).thenReturn(userPreference);
		final UserPreferenceModel settingUpdateRequest = UserPreferenceModel.builder()
		                                                                    .emailFlag(null)
		                                                                    .languagePreference(null)
		                                                                    .firstConsumption("MID")
		                                                                    .smsFlag(false)
		                                                                    .pushNotificationFlag(false)
		                                                                    .build();
		userPreferenceService.updateUserPreference(NumberUtils.LONG_ONE, settingUpdateRequest);

		ArgumentCaptor<UserPreference> userPreferenceArgumentCaptor = ArgumentCaptor.forClass(UserPreference.class);
		verify(userPreferenceRepository).save(userPreferenceArgumentCaptor.capture());
		final UserPreference savedUserPreference = userPreferenceArgumentCaptor.getValue();
		final UserPreferenceModel savedSettings =
				objectMapper.readValue(savedUserPreference.getUserPrefSetting(), UserPreferenceModel.class);
		assertFalse(savedSettings.getSmsFlag());
		assertTrue(savedSettings.getEmailFlag());
		assertEquals(Locale.ENGLISH.getLanguage(), savedSettings.getLanguagePreference());
		assertFalse(savedSettings.getPushNotificationFlag());
		verify(utility).updateDosageInfoOfprescriptions(NumberUtils.LONG_ONE, "MID");
	}

	@Test
	public void testUpdatePreferenceWhenAllValuesAreNull() throws Exception {
		/**
		 * In this case, the current settings should not be modified.
		 */
		final UserPreference userPreference = TestResponseGenerator.generateUserPreference();
		when(userPreferenceRepository.findByUserDetailsId(anyLong())).thenReturn(userPreference);
		final UserPreferenceModel settingUpdateRequest = UserPreferenceModel.builder()
		                                                                    .emailFlag(null)
		                                                                    .languagePreference(null)
		                                                                    .firstConsumption(null)
		                                                                    .smsFlag(null)
		                                                                    .pushNotificationFlag(null)
		                                                                    .build();
		userPreferenceService.updateUserPreference(NumberUtils.LONG_ONE, settingUpdateRequest);

		verify(userPreferenceRepository, never()).save(any());
		verify(utility, never()).updateDosageInfoOfprescriptions(anyLong(), anyString());
	}

	@Test
	public void testFindUserSettingsByUserIdWhenNoPreferenceExists() throws IOException {
		when(userPreferenceRepository.findByUserDetailsId(anyLong())).thenReturn(null);

		final UserPreferenceModel userSettingsByUserId =
				userPreferenceService.findUserSettingsByUserId(NumberUtils.LONG_ONE);

		assertNull(userSettingsByUserId);
		verify(objectMapper, never()).readValue(anyString(), eq(UserPreferenceModel.class));
	}

	@Test
	public void testFindUserSettingsByUserIdWhenPreferenceExists() throws IOException {
		when(userPreferenceRepository.findByUserDetailsId(anyLong())).thenReturn(
				TestResponseGenerator.generateUserPreference());

		final UserPreferenceModel savedSettings = userPreferenceService.findUserSettingsByUserId(NumberUtils.LONG_ONE);

		assertNotNull(savedSettings);
		assertTrue(savedSettings.getSmsFlag());
		assertTrue(savedSettings.getEmailFlag());
		assertEquals(Locale.ENGLISH.getLanguage(), savedSettings.getLanguagePreference());
		assertTrue(savedSettings.getPushNotificationFlag());
	}

}
