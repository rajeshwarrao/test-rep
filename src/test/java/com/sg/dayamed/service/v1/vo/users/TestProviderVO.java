package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.util.TestUtil;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 21/Jun/2019
 **/

public class TestProviderVO {

	@Test
	public void testProviderVO() {
		ProviderVO providerVO = new ProviderVO();
		providerVO.setId(TestUtil.generateId());
		providerVO.setLicense(RandomStringUtils.randomAlphanumeric(10));
		providerVO.setSpecialization(RandomStringUtils.randomAlphanumeric(10));
		providerVO.setUserDetails(new UserDetailsVO());
		assertNotNull(providerVO);
		assertNotNull(providerVO.getId());
		assertNotNull(providerVO.getLicense());
		assertNotNull(providerVO.getSpecialization());
		assertNotNull(providerVO.getUserDetails());
		new ProviderVO(new UserPreferenceVO());
		providerVO.toWsProvider();
	}
}
