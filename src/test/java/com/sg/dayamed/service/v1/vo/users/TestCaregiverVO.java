package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.TestUtil;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/

public class TestCaregiverVO {

	@Test
	public void testCaregiver() {
		CaregiverVO caregiverVO = new CaregiverVO();
		caregiverVO.setAdminFlag(true);
		caregiverVO.setId(TestUtil.generateId());
		caregiverVO.setUserDetails(new UserDetailsVO());
		caregiverVO.toWsCaregiver();
		new CaregiverVO(TestResponseGenerator.generateUserPreferenceVO());
		assertNotNull(caregiverVO.getAdminFlag());
		assertNotNull(caregiverVO.getId());
		assertNotNull(caregiverVO.getUserDetails());
	}
}
