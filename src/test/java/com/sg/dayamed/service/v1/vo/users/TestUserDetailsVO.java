package com.sg.dayamed.service.v1.vo.users;

import com.sg.dayamed.util.TestUtil;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Eresh Gorantla on 21/Jun/2019
 **/

public class TestUserDetailsVO {

	@Test
	public void testUserDetails() {
		UserDetailsVO userDetails = new UserDetailsVO();

		userDetails.setAge(TestUtil.generateAge());
		assertNotNull(userDetails.getAge());

		userDetails.setAppVersion(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getAppVersion());

		userDetails.setCountryCode(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getCountryCode());

		userDetails.setDeviceId(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getDeviceId());

		userDetails.setEmailFlag(false);
		assertNotNull(userDetails.getEmailFlag());

		userDetails.setEmailId(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getEmailId());

		userDetails.setFirstName(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getFirstName());

		userDetails.setGender(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getGender());

		userDetails.setId(TestUtil.generateId());
		assertNotNull(userDetails.getId());

		userDetails.setImanticUserId(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getImanticUserId());

		userDetails.setLastActiveTime(LocalDateTime.now());
		assertNotNull(userDetails.getLastActiveTime());

		userDetails.setLastLoginTime(LocalDateTime.now());
		assertNotNull(userDetails.getLastLoginTime());

		userDetails.setLastName(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getLastName());

		userDetails.setMiddleName(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getMiddleName());

		userDetails.setMobileNumber(TestUtil.generateMobileNumber());
		assertNotNull(userDetails.getMobileNumber());

		userDetails.setOsVersion(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getOsVersion());

		userDetails.setPwdChangeToken(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getPwdChangeToken());

		userDetails.setPwdTokenExpireDate(LocalDateTime.now());
		assertNotNull(userDetails.getPwdTokenExpireDate());

		userDetails.setRace(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getRace());

		userDetails.setSmsFlag(false);
		assertNotNull(userDetails.getSmsFlag());

		userDetails.setToken(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getToken());

		userDetails.setUserCreatedOn(LocalDateTime.now());
		assertNotNull(userDetails.getUserCreatedOn());

		userDetails.setUserDeviceType(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getUserDeviceType());

		userDetails.setUserImageUrl(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getUserImageUrl());

		userDetails.setUserUpdatedOn(LocalDateTime.now());
		assertNotNull(userDetails.getUserUpdatedOn());

		userDetails.setUserZoneId(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getUserZoneId());

		userDetails.setVoipToken(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getVoipToken());

		userDetails.setCity(RandomStringUtils.randomAlphabetic(5));
		assertNotNull(userDetails.getCity());

		userDetails.setZipCode(RandomStringUtils.randomNumeric(6));
		assertNotNull(userDetails.getZipCode());

		userDetails.setState(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getState());

		userDetails.setLocation(RandomStringUtils.randomAlphabetic(10));
		assertNotNull(userDetails.getLocation());

		userDetails.setRole(UserRoleEnum.PROVIDER.getRole());
		assertNotNull(userDetails.getRole());

		userDetails.setRoleId(NumberUtils.LONG_ONE);
		assertNotNull(userDetails.getRoleId());

		new UserDetailsVO(new UserPreferenceVO());

		userDetails.toWsUserDetails();

	}
}
