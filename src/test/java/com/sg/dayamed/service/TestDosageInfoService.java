package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.service.impl.DosageInfoServiceImpl;
import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestDosageInfoService extends BaseTestCase {

	@InjectMocks
	private DosageInfoService dosageInfoService = new DosageInfoServiceImpl();

	@Mock
	private DosageInfoRepository mockDosageInfoRepository;

	private DosageInfo dosageInfo = null;

	@Before
	public void setUp() {
		dosageInfo = TestResponseGenerator.generateDosageInfo(false);
	}

	@Test
	public void testFetchDosageInfoByID() {
		/**
		 * Case 1 : Non null response from repository
		 */
		Mockito.when(mockDosageInfoRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(dosageInfo));
		DosageInfo actualDosageInfo = dosageInfoService.fetctDosageInfoByID(1);
		assertNotNull(actualDosageInfo);
		assertEquals(dosageInfo, actualDosageInfo);

		/**
		 * Case 2 : null response from repository
		 */
		Mockito.when(mockDosageInfoRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualDosageInfo = dosageInfoService.fetctDosageInfoByID(1);
		assertNull(actualDosageInfo);
	}

	@Test
	public void testFetchMaxDurationDosageByPrescription_id() {
		Mockito.when(
				mockDosageInfoRepository.findTop1ByPrescription_idAndDeleteflagOrderByDurationDesc(Mockito.anyLong(),
				                                                                                   Mockito.anyBoolean()))
		       .thenReturn(dosageInfo);
		DosageInfo actualDosageInfo = dosageInfoService.fetchMaxDurationDosageByPrescription_id(1);
		assertNotNull(actualDosageInfo);
		assertEquals(dosageInfo, actualDosageInfo);
	}
}
