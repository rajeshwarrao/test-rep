package com.sg.dayamed.service;

import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.helper.pojo.ResultVO;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.service.impl.UserDetailsServiceImpl;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Created by Naresh Kamisetti on 17/July/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestUserDetailsService {

	@InjectMocks
	UserDetailsServiceImpl mockUserDetailsService;

	@Mock
	UserDetailsRepository mockUserDetailsRepository;

	@Mock
	Utility mockUtility;

	@Mock
	PasswordEncryption mockPwdEncryption;

	@Mock
	UserPreferenceRepository mockUserPreferenceRepository;

	@Mock
	UserDetailsService mockuserDetailsServiceInterface;

	@Mock
	ObjectMapper mockObjectMapper;

	@Mock
	SendResetPwdNotification mockSendForgotPwdEmail;

	UserDetailsModel userDetailsModel = null;

	UserDetails userDetails = null;

	UserPreference userPreference = null;

	UserPreferenceModel userPreferenceModel = null;

	@Before
	public void setUp() throws Exception {
		/*Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any(UserDetails.class)))
				.thenReturn(userDetailsModel);
		Mockito.when(mockUtility.convertUserDetailsModelToEntity(Mockito.any(UserDetailsModel.class)))
				.thenReturn(userDetails);*/
		userDetailsModel = TestResponseGenerator.generateUserDetailsModel("Male", "Asian", "Patient");
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		userPreference = TestResponseGenerator.generateUserPreference();
		userPreferenceModel = TestResponseGenerator.generateUserPreferenceModel();
	}

	@Test
	public void testFindByEmailIdIgnoreCase() {
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(any()))
		       .thenReturn(userDetails);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		UserDetailsModel actualUserDetailsModel = mockUserDetailsService.findByEmailIdIgnoreCase("asd@test.com");
		assertNotNull(actualUserDetailsModel);

		// case2 : If userdetails not existed with provided emailId
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(any()))
		       .thenReturn(null);
		UserDetailsModel nullUserDetailsModel = mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString());
		assertNull(nullUserDetailsModel);
	}

	@Test
	public void updateUserDetails() {
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(any()))
		       .thenReturn(userDetails);
		Mockito.when(mockUtility.convertUserDetailsModelToEntity(any(UserDetailsModel.class)))
		       .thenReturn(userDetails);
		Mockito.when(mockUserDetailsRepository.save(any(UserDetails.class)))
		       .thenReturn(userDetails);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		UserDetailsModel actualUserDetailsModel = mockUserDetailsService.updateUserDetails(userDetailsModel);
		assertEquals(userDetailsModel.getId(), actualUserDetailsModel.getId());
	}

	@Test
	public void fetchUserDetailsById() {
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(any(UserDetails.class)))
		       .thenReturn(userDetailsModel);
		UserDetailsModel userDetailsModel = mockUserDetailsService.fetchUserDetailsById(Mockito.anyLong());
		assertNotNull(userDetailsModel);

		// case2: optonal empty
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		UserDetails nactualUserDetails = mockUserDetailsService.fetchUserDetailEntitiysById(1L);
		assertNull(nactualUserDetails);
	}

	@Test
	public void fetchUserDetailEntitiysById() {
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		UserDetails actualUserDetails = mockUserDetailsService.fetchUserDetailEntitiysById(1L);
		assertNotNull(actualUserDetails);

		// case2: optonal empty
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		UserDetails nactualUserDetails = mockUserDetailsService.fetchUserDetailEntitiysById(1L);
		assertNull(nactualUserDetails);
	}

	@Test
	public void fetchDecryptedUserDetailsById() {
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(userDetails));
		UserDetails actualUserDetails = mockUserDetailsService.fetchDecryptedUserDetailsById(1L);
		assertNotNull(actualUserDetails);

		//case :2   : null expects
		Mockito.when(mockUserDetailsRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		UserDetails actualNullUserDetails = mockUserDetailsService.fetchDecryptedUserDetailsById(1L);
		assertNull(actualNullUserDetails);
	}

	@Test
	public void resetPassword() {
		Mockito.when(mockUserDetailsRepository.findByPwdChangeToken(Mockito.anyString()))
		       .thenReturn(userDetails);
		String result = mockUserDetailsService.resetPassword("abc", "bcd");
		assertEquals("success", "success", result);

		//case2 : expired token
		userDetails.setPwdTokenExpireDate(new Date(System.currentTimeMillis() - 3600 * 1000));
		Mockito.when(mockUserDetailsRepository.findByPwdChangeToken(Mockito.anyString()))
		       .thenReturn(userDetails);
		String expiredResult = mockUserDetailsService.resetPassword("abc", "bcd");
		assertEquals("expired token", "expired token", expiredResult);

		//case3 : invalid_token
		Mockito.when(mockUserDetailsRepository.findByPwdChangeToken(Mockito.anyString()))
		       .thenReturn(null);
		String invalid_token = mockUserDetailsService.resetPassword("abc", "bcd");
		assertEquals("invalid token", "invalid token", invalid_token);
	}

	@Test
	public void resetPasswordThrowsException() {

		//case3 : invalid_token
		Mockito.when(mockUserDetailsRepository.findByPwdChangeToken(Mockito.anyString()))
		       .thenReturn(userDetails);
		// userDetailsRepository.save(userDetails);
		Mockito.when(mockUserDetailsRepository.save(any(UserDetails.class)))
		       .thenThrow(RuntimeException.class);
		String invalid_token = mockUserDetailsService.resetPassword("abc", "bcd");
		assertEquals("invalid token", "invalid token", invalid_token);
	}

	@Test
	public void forgotPassword() {
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(mockPwdEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("abc");
		mockUserDetailsRepository.save(any(UserDetails.class));
		mockSendForgotPwdEmail.sendEmailNotification(any(UserDetails.class), Mockito.anyString(),
		                                             Mockito.anyString(), Mockito.anyString());
		String result = mockUserDetailsService.forgotPassword("abc@daya.com");
		assertEquals("success", "success", result);

		// case2 : Email id does not exist // need to check
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(null);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		String secresult = mockUserDetailsService.forgotPassword("abc@daya.com");
		assertEquals("success", "success", secresult);

		// case3 : Email id does not exist
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(null);
		String notexistResult = mockUserDetailsService.forgotPassword("abc@daya.com");
		assertEquals("Email id does not exist", "Email id does not exist", notexistResult);
	}

	@Test
	public void save() {
		// return userDetailsRepository.save(userDetails);
		Mockito.when(mockUserDetailsRepository.save(any(UserDetails.class)))
		       .thenReturn(userDetails);
		UserDetails actualUserDetails = mockUserDetailsService.save(userDetails);
		assertNotNull(actualUserDetails);
	}

	@Test
	public void findByEmailId() throws Exception {
		//case 1: +ve
		//UserPreference  userPreference  = TestResponseGenerator.generateUserPreference();
		//UserPreferenceModel userPreferenceModel = TestResponseGenerator.generateUserPreferenceModel();

		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
		       .thenReturn(userPreference);

		//Mockito.when(mockObjectMapper.readValue("abc",  UserPreferenceModel.class)).thenReturn(userPreferenceModel);
		UserPreferenceModel actualUserPreferenceModel = mockUserDetailsService.findByEmailId("abc@daya.com");

		//assertNotNull(actualUserPreferenceModel); //need to check
		//Case2 : null
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(null);
		UserPreferenceModel nullUserPreferenceModel = mockUserDetailsService.findByEmailId("abc@daya.com");
		assertNull(nullUserPreferenceModel);

	}

	@Test
	public void findByEmailIdIn() throws Exception {
		List<UserDetails> userDetailslist = new LinkedList<UserDetails>();
		userDetailslist.add(userDetails);

		List<UserPreference> userPreferencelist = new ArrayList<UserPreference>();
		userPreference.getUserDetails()
		              .setEmailId("abc@daya.com");
		userPreferencelist.add(userPreference);

		List<String> emailIds = new ArrayList<String>();
		emailIds.add("abc@daya.com");
		emailIds.add("abd@daya.com");

		userPreferenceModel.setEmailFlag(true);
		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(UserPreferenceModel.class)))
		       .thenReturn(userPreferenceModel);
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCaseIn(any()))
		       .thenReturn(userDetailslist);
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsIdIn(any()))
		       .thenReturn(userPreferencelist);

		//(pHIDataDBEncryption.filedLevelDecryption(pHIDataDBEncryption.getDBEncryKey(), details.getEmailId(),
		// ApplicationConstants.PATIENT));

		List<String> resultEmails = mockUserDetailsService.findByEmailIdIn(emailIds);
		assertTrue(CollectionUtils.isNotEmpty(resultEmails));

	}

	@Test
	public void findByEmailIdInResultVo() throws Exception {

		List<UserDetails> userDetailslist = new LinkedList<UserDetails>();
		userDetailslist.add(userDetails);

		List<UserPreference> userPreferencelist = new ArrayList<UserPreference>();
		userPreferencelist.add(userPreference);

		List<String> emailIds = new ArrayList<String>();
		emailIds.add("abc@daya.com");
		emailIds.add("abd@daya.com");
		Mockito.when(mockUserDetailsRepository.findByEmailIdIgnoreCaseIn(any()))
		       .thenReturn(userDetailslist);
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsIdIn(any()))
		       .thenReturn(userPreferencelist);

		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(UserPreferenceModel.class)))
		       .thenReturn(userPreferenceModel);

		Mockito.when(mockUserPreferenceRepository.findByUserDetailsIdIn(any()))
		       .thenReturn(userPreferencelist);
		List<ResultVO> resultResultVO = mockUserDetailsService.findByEmailIdInResultVo(emailIds);
		assertTrue(CollectionUtils.isNotEmpty(resultResultVO));
	}

	//private metod
	@Test
	public void findByPhoneNumber() throws Exception {
		Mockito.when(mockUserDetailsRepository.findTopByMobileNumber(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
		       .thenReturn(userPreference);
		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(UserPreferenceModel.class)))
		       .thenReturn(userPreferenceModel);
		Boolean flag = mockUserDetailsService.findByPhoneNumber("12345");
		assertTrue(flag);
	}

	@Test
	public void findByPhoneNumberthrowsException() throws Exception {
		Mockito.when(mockUserDetailsRepository.findTopByMobileNumber(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
		       .thenReturn(userPreference);
		Mockito.when(mockObjectMapper.readValue(Mockito.anyString(), Mockito.eq(UserPreferenceModel.class)))
		       .thenThrow(RuntimeException.class);
		Boolean flag = mockUserDetailsService.findByPhoneNumber("12345");
		assertFalse(flag);
	}

	@Test
	public void findByToken() throws Exception {
		Mockito.when(mockUserDetailsRepository.findByToken(Mockito.anyString()))
		       .thenReturn(userDetails);
		Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
		       .thenReturn(userPreference);
		UserPreferenceModel userPreferenceModel = TestResponseGenerator.generateUserPreferenceModel();
		//Mockito.when(mockObjectMapper.readValue("abc",  UserPreferenceModel.class)).thenReturn(userPreferenceModel);
		Boolean result = mockUserDetailsService.findByToken("abc");
		assertTrue(result);
	}

	@Test
	public void fetchUserDetailsByToken() {

		List<UserDetails> userDetailslist = new LinkedList<UserDetails>();
		userDetailslist.add(userDetails);

		Mockito.when(mockUserDetailsRepository.findByTokenIgnoreCase(Mockito.anyString()))
		       .thenReturn(userDetailslist);
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(any(UserDetails.class)))
		       .thenReturn(userDetailsModel);

		List<UserDetailsModel> userDetailsModelList = mockUserDetailsService.fetchUserDetailsByToken("abc");
		assertTrue(CollectionUtils.isNotEmpty(userDetailsModelList));
	}

}
