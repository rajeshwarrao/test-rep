package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.CaregiverRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.impl.CaregiverServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 04/May/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestCaregiverService extends BaseTestCase {

	@InjectMocks
	CaregiverService caregiverService = new CaregiverServiceImpl();

	@Mock
	CaregiverRepository mockCaregiverRepository;

	@Mock
	private Utility mockUtility;

	@Mock
	private PasswordEncryption mockPasswordEncryption;

	@Mock
	private RoleRepository mockRoleRepository;

	@Mock
	private SendResetPwdNotification mockSendResetPwdNotification;

	@Mock
	private PatientService mockPatientService;

	@Mock
	private EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	private Authentication mockAuthentication;

	@Mock
	private ProviderService mockProviderService;

	@Mock
	UserPreferenceService mockUserPreferenceService;

	@Mock
	MessageNotificationConstants mockMessageNotificationConstants;

	@Mock
	UserDetailsService mockUserDetailsService;

	@Mock
	JwtUserDetails jwtUserDetails;
	
	@Mock
	ProviderRepository mockProviderRepository;
	
	@Mock
	PatientRepository mockPatientRepository;

	@Before
	public void setUp() {
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockUtility.convertCaregiverModelToEntity(Mockito.any(CaregiverModel.class)))
		       .thenReturn(caregiver);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
	}

	@Test
	public void testFetchCaregivers() throws Exception {
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", true);
		CaregiverModel caregiverModel =
				generateRequestObject("CaregiverModel-1", CaregiverModel.class, USER_DETAILS_FILE);
		List<CaregiverModel> expectedCaregiverModels = Stream.of(caregiverModel)
		                                                     .collect(Collectors.toList());
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any()))
		       .thenReturn(caregiverModel);
		Mockito.when(mockCaregiverRepository.findAll())
		       .thenReturn(Stream.of(caregiver)
		                         .collect(Collectors.toList()));
		List<CaregiverModel> actualCaregiverModels = caregiverService.fetchCaregivers();
		assertTrue(CollectionUtils.isNotEmpty(actualCaregiverModels));
		assertEquals(expectedCaregiverModels.get(0)
		                                    .getId(), actualCaregiverModels.get(0)
		                                                                   .getId());
	}

	@Test
	public void testFetchCaregiverById() throws DataValidationException {
		/**
		 * Case 1 : Non null response
		 */
		Caregiver expectedCaregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", true);
		Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(expectedCaregiver));
		Caregiver actualCaregiver = caregiverService.fetchCaregiverById(1);
		assertNotNull(actualCaregiver);
		assertEquals(expectedCaregiver, actualCaregiver);

		/**
		 * Case 2 : null response
		 */
		try {
			Mockito.reset(mockCaregiverRepository);
			Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
			       .thenReturn(Optional.empty());
			actualCaregiver = caregiverService.fetchCaregiverById(1);
		} catch (Exception e) {
			assertTrue(e instanceof DataValidationException);
		}
	}


	@Test
	public void testFetchAdminAndProviderCaregivers() {
		List<Caregiver> adminCaregivers =
				Stream.of(TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", true))
				      .collect(Collectors.toList());
		List<Caregiver> providerCaregivers =
				Stream.of(TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false))
				      .collect(Collectors.toList());
		Mockito.when(mockCaregiverRepository.findByAdminflag(Mockito.eq(true)))
		       .thenReturn(adminCaregivers);
		Mockito.when(mockCaregiverRepository.findByProviders_id(Mockito.anyLong()))
		       .thenReturn(providerCaregivers);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.eq(adminCaregivers.get(0))))
		       .thenReturn(TestResponseGenerator.toCaregiverModel(adminCaregivers.get(0)));
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.eq(providerCaregivers.get(0))))
		       .thenReturn(TestResponseGenerator.toCaregiverModel(providerCaregivers.get(0)));
		List<CaregiverModel> actualCaregiverModels = caregiverService.fetchAdminAndProviderCargivers(1);
		assertTrue(CollectionUtils.isNotEmpty(actualCaregiverModels));
		assertEquals(2, actualCaregiverModels.size());
		assertEquals(adminCaregivers.get(0)
		                            .getId(), actualCaregiverModels.get(0)
		                                                           .getId());
		assertEquals(providerCaregivers.get(0)
		                               .getId(), actualCaregiverModels.get(1)
		                                                              .getId());
	}

	@Test
	public void testDeleteCaregiver() throws DataValidationException {
		/**
		 * Case 1 : Success
		 */
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(caregiver));
		String response = caregiverService.deleteCaregiver(1);
		Mockito.verify(mockCaregiverRepository, Mockito.times(1))
		       .deletepatientsBycaregiverId(Mockito.anyLong());
		Mockito.verify(mockCaregiverRepository, Mockito.times(1))
		       .delete(Mockito.any());
		assertEquals(StringUtils.upperCase("success"), StringUtils.upperCase(response));

	}

	@Test
	public void testFetchCaregiverByUserId() throws DataValidationException {
		/**
		 * Case 1 : Non null caregiver
		 */
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockCaregiverRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(caregiver);
		Caregiver actualCaregiver = caregiverService.fetchCaregiverByUserId(1);
		assertNotNull(actualCaregiver);
		assertEquals(caregiver.getId(), actualCaregiver.getId());
		assertEquals(caregiver.getUserDetails()
		                      .getId(), actualCaregiver.getUserDetails()
		                                               .getId());

		/**
		 * Case 2 : Null caregiver
		 */
		try {
			Mockito.when(mockCaregiverRepository.findByUserDetails_id(Mockito.anyLong()))
			       .thenReturn(null);
			actualCaregiver = caregiverService.fetchCaregiverByUserId(1);
		} catch (Exception e) {
			assertTrue(e instanceof DataValidationException);
		}
	}

	@Test
	public void testFetchCaregiversByPatientId() {
		List<Caregiver> expectedCaregivers =
				Stream.of(TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false))
				      .collect(Collectors.toList());
		Mockito.when(mockCaregiverRepository.findByPatients_id(Mockito.anyLong()))
		       .thenReturn(expectedCaregivers);
		CaregiverModel expectedCaregiverModel = TestResponseGenerator.toCaregiverModel(expectedCaregivers.get(0));
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any()))
		       .thenReturn(expectedCaregiverModel);
		List<CaregiverModel> actualCaregiverModels = caregiverService.fetchCaregiversByPatientId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualCaregiverModels));
		assertEquals(expectedCaregiverModel.getId(), actualCaregiverModels.get(0)
		                                                                  .getId());
		assertEquals(expectedCaregiverModel.getUserDetails()
		                                   .getId(), actualCaregiverModels.get(0)
		                                                                  .getUserDetails()
		                                                                  .getId());
	}

	@Test
	public void testDirectFetchCaregiverById() {
		/**
		 * Case 1 : Non null
		 */
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(caregiver));
		Caregiver actualCaregiver = caregiverService.directFetchCaregiverById(1);
		assertNotNull(actualCaregiver);
		assertEquals(caregiver, actualCaregiver);

		/**
		 * Case 2 : Null
		 */
		Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualCaregiver = caregiverService.directFetchCaregiverById(1);
		assertNull(actualCaregiver);
	}


	@Test
	public void testAddCaregiver() {
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockUtility.convertCaregiverModelToEntity(Mockito.any(CaregiverModel.class)))
		       .thenReturn(caregiver);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Set<Caregiver> caregiverSet = new HashSet<Caregiver>();
		caregiverSet.add(caregiver);
		patient.setCaregivers(caregiverSet);

		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Set<CaregiverModel> caregiverModelSet = new HashSet<CaregiverModel>();
		caregiverModelSet.add(caregiverModel);
		patientModel.setCaregivers(caregiverModelSet);
		jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(patient.getUserDetails(), true,
		                                                              ApplicationConstants.CAREGIVER);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any()))
	       .thenReturn(caregiverModel);
		CaregiverModel actualCaregiverModel =
				caregiverService.addCaregiver(caregiverModel, jwtUserDetails);
		assertTrue(actualCaregiverModel!= null); //null pointer
	}

	@Test
	public void testAddCaregiverPatientZero() {
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Set<Caregiver> caregiverSet = new HashSet<Caregiver>();
		caregiverSet.add(caregiver);
		patient.setCaregivers(caregiverSet);

		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));

		Mockito.when(mockCaregiverRepository.save(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiver);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiverModel);
		jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(patient.getUserDetails(), true,
		                                                              ApplicationConstants.CAREGIVER);
		CaregiverModel actualCaregiverModel = caregiverService.addCaregiver(caregiverModel, jwtUserDetails);
		assertTrue(actualCaregiverModel != null);
	}

	@Test
	public void addCaregiverWithAuthentication() {

		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Caregiver caregiver =
				TestResponseGenerator.generateCaregiver("Male", "Asian", UserRoleEnum.CAREGIVER.getRole(), false);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiverModel);
		Mockito.when(mockCaregiverRepository.save(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiver);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		CaregiverModel actualCaregiverModel = caregiverService.addCaregiver(caregiverModel, jwtUserDetails);
		assertEquals(actualCaregiverModel.getUserDetails()
		                                 .getFirstName(), caregiverModel.getUserDetails()
		                                                                .getFirstName());
	}

	@Test  //case2
	public void addCaregiverWithAuthentication2() {

		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Caregiver caregiver =
				TestResponseGenerator.generateCaregiver("Male", "Asian", UserRoleEnum.CAREGIVER.getRole(), false);
		caregiver.setId(0L); // this is the case
		Mockito.when(mockUtility.convertCaregiverModelToEntity(Mockito.any(CaregiverModel.class)))
		       .thenReturn(caregiver);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiverModel);
		Mockito.when(mockCaregiverRepository.save(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiver);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		CaregiverModel actualCaregiverModel = caregiverService.addCaregiver(caregiverModel, jwtUserDetails);
		assertEquals(actualCaregiverModel.getUserDetails()
		                                 .getFirstName(), caregiverModel.getUserDetails()
		                                                                .getFirstName());
	}

	@Test
	public void testAddCaregiverFromApp() throws Exception {
		/**
		 * case 1: Fail
		 */
		try {
			CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
			UserDetailsModel userDetailsModel = TestResponseGenerator.generateUserDetailsModel("Male", "Asian",
			                                                                                   "Caregiver");
			Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString()))
			       .thenReturn(userDetailsModel);
			caregiverService.addCaregiverFromApp(caregiverModel, null);
		} catch (Exception e) {
			assertTrue(true);
		}

		/*
		 * case 2: success
		 */

		Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(null);
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		Mockito.when(mockUtility.uploadProfilepic(Mockito.any(), Mockito.anyString(), Mockito.anyString()))
		       .thenReturn("imagepath");
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockUtility.convertCaregiverModelToEntity(Mockito.any(CaregiverModel.class)))
		       .thenReturn(caregiver);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
		// String pwdChangeToken =
		// pwdEncryption.encrypt(caregiver.getUserDetails().getEmailId() +
		// System.currentTimeMillis());
		Role role = TestResponseGenerator.generateRole("Caregiver");
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString()))
		       .thenReturn(role);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Set<Caregiver> caregiverSet = new HashSet<Caregiver>();
		caregiverSet.add(caregiver);
		patient.setCaregivers(caregiverSet);
		Mockito.when(mockCaregiverRepository.save(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiver);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any(Caregiver.class)))
		       .thenReturn(caregiverModel);
		// Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong())).thenReturn(patient);
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Set<CaregiverModel> caregiverModelSet = new HashSet<CaregiverModel>();
		caregiverModelSet.add(caregiverModel);
		patientModel.setCaregivers(caregiverModelSet);
		Set<CaregiverModel> caregiverModelSetPresis = caregiverService.addCaregiverFromApp(caregiverModel,
		                                                                                   profileImage);
		assertNotNull(caregiverModelSetPresis);
		assertTrue(caregiverModelSetPresis.contains(caregiverModel));
	}
	
	@Test
	public void testFetchAdminAndSpecificProviderCargivers() throws Exception {
		Provider provider = TestResponseGenerator.generateProvider("male", "Asian", ApplicationConstants.PROVIDER);
		Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong())).thenReturn(provider);
		List<Caregiver> adminCaregivers =
				Stream.of(TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", true))
				      .collect(Collectors.toList());
		List<Caregiver> providerCaregivers =
				Stream.of(TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false))
				      .collect(Collectors.toList());
		Mockito.when(mockCaregiverRepository.findByAdminflag(Mockito.eq(true)))
		       .thenReturn(adminCaregivers);
		Mockito.when(mockCaregiverRepository.findByProviders_id(Mockito.anyLong()))
		       .thenReturn(providerCaregivers);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.eq(adminCaregivers.get(0))))
		       .thenReturn(TestResponseGenerator.toCaregiverModel(adminCaregivers.get(0)));
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.eq(providerCaregivers.get(0))))
		       .thenReturn(TestResponseGenerator.toCaregiverModel(providerCaregivers.get(0)));
		List<CaregiverModel> actualCaregiverModels = caregiverService.fetchAdminAndSpecificProviderCargivers(1L, null);
		assertTrue(CollectionUtils.isNotEmpty(actualCaregiverModels));
		assertEquals(2, actualCaregiverModels.size());
		assertEquals(adminCaregivers.get(0)
		                            .getId(), actualCaregiverModels.get(0)
		                                                           .getId());
		assertEquals(providerCaregivers.get(0)
		                               .getId(), actualCaregiverModels.get(1)
		                                                              .getId());
		
	}
	
	@Mock
	PrescriptionRepository mockPrescriptionRepository;
	
	@Test
	public void testFetchPrescriptionsByCaregiverId() throws Exception {
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockPatientRepository.findByCaregivers_id(Mockito.anyLong())).thenReturn(Stream.of(patient).collect(Collectors.toList()));
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockPrescriptionRepository.findByPatient_id(Mockito.anyLong())).thenReturn(Stream.of(prescription).collect(Collectors.toList()));
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any())).thenReturn(prescriptionModel);
		List<PrescriptionModel> listPrescriptionModel =caregiverService.fetchPrescriptionsByCaregiverId(1L);
		assertTrue(CollectionUtils.isNotEmpty(listPrescriptionModel));
		assertEquals(listPrescriptionModel.get(0) , prescriptionModel);
	}

	
	@Test
	public void testFetchPatientsByCaregiverId() throws Exception{
		Caregiver expectedCaregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", true);
		Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(expectedCaregiver));
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockPatientService.directFetchPatientsByCaregiverId(Mockito.anyLong())).thenReturn(Stream.of(patient).collect(Collectors.toList()));
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any())).thenReturn(patientModel);
		List<PatientModel> listPatientModel=caregiverService.fetchPatientsByCaregiverId(1L, null);
		assertTrue(CollectionUtils.isNotEmpty(listPatientModel));
		assertEquals(listPatientModel.get(0) , patientModel);
	}
	
	@Test
	public void testCreateCaregiverAssociatedWithPatient()throws Exception{
		Mockito.when(mockUtility.uploadProfilepic(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn("/testimage");
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("male", "Asian",  ApplicationConstants.CAREGIVER, true);
		Mockito.when(mockUtility.convertCaregiverModelToEntity(Mockito.any())).thenReturn(caregiver);
		Role role = TestResponseGenerator.generateRole(ApplicationConstants.CAREGIVER);
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString())).thenReturn(role);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong())).thenReturn(patient);
		PatientModel persistedPatientModel = TestResponseGenerator.generatePatientModel();
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("male", "Asian");
		Set<CaregiverModel> setCaregiverModels= new HashSet<>();
		setCaregiverModels.add(caregiverModel);
		persistedPatientModel.setCaregivers(setCaregiverModels);
		Mockito.when(mockPatientService.updatePatientAlone(Mockito.any())).thenReturn(persistedPatientModel);
		Set<CaregiverModel> setCaregiverModel = caregiverService.createCaregiverAssociatedwithPatient(caregiverModel, 1L,profileImage);
		assertTrue(CollectionUtils.isNotEmpty(setCaregiverModel));
		assertEquals(setCaregiverModels , setCaregiverModel);
	}
	
	
	@Test
	public void testAddCaregiverToPatient() throws Exception {
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("male", "Asian",  ApplicationConstants.CAREGIVER, true);
		Mockito.when(mockUtility.convertCaregiverModelToEntity(Mockito.any())).thenReturn(caregiver);
		Role role = TestResponseGenerator.generateRole(ApplicationConstants.CAREGIVER);
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString())).thenReturn(role);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong())).thenReturn(patient);
		PatientModel persistedPatientModel = TestResponseGenerator.generatePatientModel();
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("male", "Asian");
		Set<CaregiverModel> setCaregiverModels= new HashSet<>();
		setCaregiverModels.add(caregiverModel);
		persistedPatientModel.setCaregivers(setCaregiverModels);
		Mockito.when(mockPatientService.updatePatientAlone(Mockito.any())).thenReturn(persistedPatientModel);
		Set<CaregiverModel> setCaregiverModel = caregiverService.addCaregiverToPatient(caregiverModel, 1L);
		assertTrue(CollectionUtils.isNotEmpty(setCaregiverModel));
		assertEquals(setCaregiverModels , setCaregiverModel);
	}
	
	
	@Test
	public void testUpdateCaregiverFromApp() throws Exception {
		Caregiver expectedCaregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", true);
		Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(expectedCaregiver));
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		Mockito.when(mockUtility.uploadProfilepic(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn("/testimage");
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.CAREGIVER);
		Mockito.when(mockUtility.convertUserDetailsModelToEntity(Mockito.any())).thenReturn(userDetails);
		Mockito.when(mockCaregiverRepository.save(Mockito.any())).thenReturn(expectedCaregiver);
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any())).thenReturn(caregiverModel);
		CaregiverModel resCaregiverModel = caregiverService.updateCaregiverFromApp(caregiverModel, profileImage);
		assertTrue(resCaregiverModel != null);
		assertEquals(resCaregiverModel , caregiverModel);
	}
	
	@Test
	public void testMapPatientToCaegiver() throws Exception {
		UserDetailsModel userDetailsModel = TestResponseGenerator.generateUserDetailsModel("male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString())).thenReturn(userDetailsModel);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Caregiver expectedCaregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", true);
		Mockito.when(mockCaregiverRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(expectedCaregiver));
		Set<Patient> setPatient= new HashSet<>();
		setPatient.add(patient);
		expectedCaregiver.setPatients(setPatient);
		Mockito.when(mockPatientService.findByUserDetails_id(Mockito.anyLong())).thenReturn(patient);
		String status = caregiverService.mapPatientToCaegiver("Test@t.com",1L);
		assertTrue(status != null);
		assertEquals(status , "success");
	}
	
	@Test
	public void testAddCaregiverFromWebValidate() throws Exception {
		CaregiverModel caregiverModel = TestResponseGenerator.generateCaregiverModel("Male", "Asian");
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		//Mockito.when(mockUtility.uploadProfilepic(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn("/testimage");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Mockito.when(mockUtility.convertCaregiverModelToEntity(Mockito.any(CaregiverModel.class)))
		       .thenReturn(caregiver);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Set<Caregiver> caregiverSet = new HashSet<Caregiver>();
		caregiverSet.add(caregiver);
		patient.setCaregivers(caregiverSet);

		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Set<CaregiverModel> caregiverModelSet = new HashSet<CaregiverModel>();
		caregiverModelSet.add(caregiverModel);
		patientModel.setCaregivers(caregiverModelSet);
		jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(patient.getUserDetails(), true,
		                                                              ApplicationConstants.CAREGIVER);
		Mockito.when(mockUtility.convertCaregiverEntityToModel(Mockito.any()))
	       .thenReturn(caregiverModel);
		CaregiverModel actualCaregiverModel =
				caregiverService.addCaregiverFromWebValidate(caregiverModel, profileImage,jwtUserDetails);
		assertTrue(actualCaregiverModel!= null);
		assertEquals(actualCaregiverModel , caregiverModel);
	}

	
	@Test
	public void testFetchPatietnsAloneByCaregiver() throws Exception {
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		Mockito.when(mockAuthentication.getPrincipal()).thenReturn(jwtUserDetails);
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", UserRoleEnum.CAREGIVER.getRole(), true);
		Mockito.when(mockCaregiverRepository.findByUserDetails_id(Mockito.anyLong())).thenReturn(caregiver);
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockPatientService.fetchPatientsByCaregiverId(Mockito.anyLong())).thenReturn(Stream.of(patientModel).collect(Collectors.toList()));
		List<PatientModel> listPatientModel=caregiverService.fetchPatietnsAloneByCaregiver(mockAuthentication);
		assertTrue(CollectionUtils.isNotEmpty(listPatientModel));
	}
	
	@Test
	public void testFetchPatietnsByCaregiverIdFromJwtToken() throws Exception {
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		Mockito.when(mockAuthentication.getPrincipal()).thenReturn(jwtUserDetails);
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", UserRoleEnum.CAREGIVER.getRole(), true);
		Mockito.when(mockCaregiverRepository.findByUserDetails_id(Mockito.anyLong())).thenReturn(caregiver);
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockPatientService.fetchPatientsByCaregiverId(Mockito.anyLong())).thenReturn(Stream.of(patientModel).collect(Collectors.toList()));
		List<PatientModel> listPatientModel=caregiverService.fetchPatietnsByCaregiverIdFromJwtToken(mockAuthentication);
		assertTrue(CollectionUtils.isNotEmpty(listPatientModel));
		assertEquals(listPatientModel.get(0) , patientModel);
	}
}
