package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.MyEntityRepositoryCustom;
import com.sg.dayamed.service.impl.GoodShapredAlertProtocalImpl;
import com.sg.dayamed.util.SendResetPwdNotification;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNull;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestGoodShepherdAlertProtocol extends BaseTestCase {

	@InjectMocks
	private GoodShapredAlertProtocal goodShapredAlertProtocal = new GoodShapredAlertProtocalImpl();

	@Mock
	private RediseService mockRediseService;

	@Mock
	private MyEntityRepositoryCustom mockMyEntityRepositoryCustom;

	@Mock
	private SendResetPwdNotification mockSendResetPwdNotification;

	@Before
	public void setUp() {

	}

	@Test
	public void testSendMissedAdherenceNotification() {
		/**
		 * Case 1 : All Possible values..
		 */
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.DayamedAlertProtocalNotification)))
		       .thenReturn("Alert Protocol");
		List<Map<String, String>> listUserDetailswithDosage = new ArrayList<>();
		Map<String, String> map = new HashMap<>();
		map.put("patientId", "1");
		map.put("gsNotificationStatus", "1");
		map.put("adharenceId", "1");
		map.put("userName", "randomUser");
		map.put("actionStatus", "1");
		map.put("prescribedTime", "10:00");
		map.put("medicineName", "M1");
		listUserDetailswithDosage.add(map);
		Mockito.when(mockMyEntityRepositoryCustom.getColmNameAndColumValueAsMap(Mockito.eq("Alert Protocol")))
		       .thenReturn(listUserDetailswithDosage);
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.DayamedAlertProtocalNotificationRecepientEmailqry)))
		       .thenReturn("Email Query");
		List<Map<String, String>> recepientMap = new ArrayList<>();
		Map<String, String> receipients = new HashMap<>();
		receipients.put("email", "test@gmail.com");
		recepientMap.add(receipients);
		Mockito.when(mockMyEntityRepositoryCustom.getColmNameAndColumValueAsMap(Mockito.eq("Email Query")))
		       .thenReturn(recepientMap);
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.DayamedAlertProtocalNotificationRecepientEmailBody)))
		       .thenReturn("Email Body");
		Mockito.when(
				mockSendResetPwdNotification.sendEmailNotificationByRecepients(Mockito.anyList(), Mockito.anyString(),
				                                                               Mockito.anyString()))
		       .thenReturn(true);
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.DayamedAlertProtocalNotificationRecepientSMSEnabled)))
		       .thenReturn("true");
		Mockito.when(mockSendResetPwdNotification.sendSMSNotification(Mockito.anyList(), Mockito.anyString()))
		       .thenReturn(true);
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.DayamedAlertProtocalNotifAdharenceUpdatestatsQry)))
		       .thenReturn("Updates adherence");
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.DayamedAlertProtocalNotificationRecepientSMSBody)))
		       .thenReturn("Alert SMS Body");
		String response = goodShapredAlertProtocal.sendMissedAdherenceNotification();
		assertNull(response);
		Mockito.verify(mockSendResetPwdNotification, Mockito.times(1))
		       .sendEmailNotificationByRecepients(Mockito.anyList(), Mockito.anyString(), Mockito.anyString());
		Mockito.verify(mockSendResetPwdNotification, Mockito.times(1))
		       .sendSMSNotification(Mockito.anyList(), Mockito.anyString());
		Mockito.verify(mockMyEntityRepositoryCustom, Mockito.times(1))
		       .updateQueryPrepareStamt(Mockito.anyString(), Mockito.anyMap());

		/**
		 * Case 2 : Null Dosage information
		 */
		Mockito.reset(mockSendResetPwdNotification, mockMyEntityRepositoryCustom);
		Mockito.when(mockMyEntityRepositoryCustom.getColmNameAndColumValueAsMap(Mockito.eq("Alert Protocol")))
		       .thenReturn(null);
		response = goodShapredAlertProtocal.sendMissedAdherenceNotification();
		assertNull(response);
		Mockito.verify(mockSendResetPwdNotification, Mockito.times(0))
		       .sendEmailNotificationByRecepients(Mockito.anyList(), Mockito.anyString(), Mockito.anyString());
		Mockito.verify(mockSendResetPwdNotification, Mockito.times(0))
		       .sendSMSNotification(Mockito.anyList(), Mockito.anyString());
		Mockito.verify(mockMyEntityRepositoryCustom, Mockito.times(0))
		       .updateQueryPrepareStamt(Mockito.anyString(), Mockito.anyMap());

		/**
		 * Case 3 : Exception occurred while processing
		 */
		Mockito.reset(mockSendResetPwdNotification, mockMyEntityRepositoryCustom);
		Mockito.when(mockMyEntityRepositoryCustom.getColmNameAndColumValueAsMap(Mockito.eq("Alert Protocol")))
		       .thenThrow(new RuntimeException());
		response = goodShapredAlertProtocal.sendMissedAdherenceNotification();
		assertNull(response);
		Mockito.verify(mockSendResetPwdNotification, Mockito.times(0))
		       .sendEmailNotificationByRecepients(Mockito.anyList(), Mockito.anyString(), Mockito.anyString());
		Mockito.verify(mockSendResetPwdNotification, Mockito.times(0))
		       .sendSMSNotification(Mockito.anyList(), Mockito.anyString());
		Mockito.verify(mockMyEntityRepositoryCustom, Mockito.times(0))
		       .updateQueryPrepareStamt(Mockito.anyString(), Mockito.anyMap());
	}
}
