package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.repository.CartRepository;
import com.sg.dayamed.repository.ConsumerGoodsRepository;
import com.sg.dayamed.repository.MedicineRepository;
import com.sg.dayamed.service.impl.OrderOrCartServiceImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by Eresh Gorantla on 06/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestOrderOrCartService extends BaseTestCase {

	@InjectMocks
	private OrderOrCartService orderOrCartService = new OrderOrCartServiceImpl();

	@Mock
	private CartRepository mockCartRepository;

	@Mock
	private MedicineRepository mockMedicineRepository;

	@Mock
	private ConsumerGoodsRepository mockConsumerGoodsRepository;

	@Test
	public void testAddOrUpdateCartItems() {

	}

}
