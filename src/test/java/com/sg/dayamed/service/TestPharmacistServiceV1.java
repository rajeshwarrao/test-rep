package com.sg.dayamed.service;

import com.sg.dayamed.adapter.patients.PatientLoadingProcessor;
import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.exceptions.IWSGlobalApiErrorKeys;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.util.APIErrorFields;
import com.sg.dayamed.service.v1.PharmacistService;
import com.sg.dayamed.service.v1.PrescriptionInfoVService;
import com.sg.dayamed.service.v1.impl.PharmacistServiceImpl;
import com.sg.dayamed.service.v1.vo.users.LoadPatientRequestVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsResponseVO;
import com.sg.dayamed.service.v1.vo.users.PatientDetailsVO;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;
import com.sg.dayamed.validators.UserValidator;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 02/Jul/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPharmacistServiceV1 extends BaseTestCase {

	@InjectMocks
	private PharmacistService pharmacistService = new PharmacistServiceImpl();

	@Mock
	private PharmacistRepository mockPharmacistRepository;

	@Mock
	private Utility mockUtility;

	@Mock
	private UserValidator mockUserValidator;

	@Mock
	private PatientLoadingProcessor mockLoadingProcessor;

	@Mock
	PrescriptionInfoVService mockPrescriptionInfoVService;

	private UserDetails userDetails;

	private JwtUserDetails jwtUserDetails;

	@Before
	public void setup() {
		userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PHARMACIST.getRole());
	}

	@Test
	public void testGetPatientsForGSEnabled() throws Exception {
		/**
		 * Case 1 : Returns data with prescription status is new
		 */
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PHARMACIST);
		PatientDetailsResponseVO responseVO = new PatientDetailsResponseVO();
		responseVO.setTotalRecords(10);
		List<PatientDetailsVO> patientDetailsVOS = IntStream.range(0, 10)
		                                                    .mapToObj(
				                                                    index -> TestResponseGenerator.generatePatientDetailsVO())
		                                                    .collect(Collectors.toList());
		responseVO.setPatientDetails(patientDetailsVOS);
		Mockito.when(mockLoadingProcessor.getPatients(Mockito.any(), Mockito.any()))
		       .thenReturn(responseVO);
		Pharmacist pharmacist =
				TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		Mockito.when(mockPharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		PharmacistModel pharmacistModel =
				generateRequestObject("PharmacistModel-1", PharmacistModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPharmacistEntityToModel(Mockito.any()))
		       .thenReturn(pharmacistModel);
		List<Long> patientIds = patientDetailsVOS.stream()
		                                         .map(PatientDetailsVO::getId)
		                                         .distinct()
		                                         .collect(Collectors.toList());
		Mockito.when(
				mockPrescriptionInfoVService.getPatientsByPrescriptionStatus(Mockito.anyString(), Mockito.anyLong(),
				                                                             Mockito.any(), Mockito.anyList()))
		       .thenReturn(patientIds);
		PatientDetailsResponseVO patientDetailsResponseVO =
				pharmacistService.getPatientsForGSEnabled(jwtUserDetails, "new", requestVO);
		assertNotNull(patientDetailsResponseVO);
		assertEquals(10, patientDetailsResponseVO.getTotalRecords()
		                                         .intValue());
		assertTrue(CollectionUtils.isNotEmpty(patientDetailsResponseVO.getPatientDetails()));
		assertEquals(patientIds.size(), patientDetailsResponseVO.getPatientDetails()
		                                                        .size());
		assertEquals(patientDetailsVOS, patientDetailsResponseVO.getPatientDetails());

		/**
		 * Case 2 : Does not return patients with status hold.
		 */
		Mockito.reset(mockPrescriptionInfoVService);
		Mockito.when(
				mockPrescriptionInfoVService.getPatientsByPrescriptionStatus(Mockito.anyString(), Mockito.anyLong(),
				                                                             Mockito.any(), Mockito.anyList()))
		       .thenReturn(Collections.emptyList());
		patientDetailsResponseVO = pharmacistService.getPatientsForGSEnabled(jwtUserDetails, "hold", requestVO);
		assertNotNull(patientDetailsResponseVO);
		assertEquals(0, patientDetailsResponseVO.getTotalRecords()
		                                        .intValue());
		assertTrue(CollectionUtils.isEmpty(patientDetailsResponseVO.getPatientDetails()));
		assertEquals(0, patientDetailsResponseVO.getPatientDetails()
		                                        .size());
	}

	@Test
	public void testGetPatientsForGSEnabledErrorCases() throws Exception {
		/**
		 * Case 1 : privileges exception.
		 */
		LoadPatientRequestVO requestVO = TestResponseGenerator.generateLoadPatientRequest(UserRoleEnum.PHARMACIST);
		PatientDetailsResponseVO responseVO = new PatientDetailsResponseVO();
		responseVO.setTotalRecords(10);
		List<PatientDetailsVO> patientDetailsVOS = IntStream.range(0, 10)
		                                                    .mapToObj(
				                                                    index -> TestResponseGenerator.generatePatientDetailsVO())
		                                                    .collect(Collectors.toList());
		responseVO.setPatientDetails(patientDetailsVOS);
		Mockito.when(mockLoadingProcessor.getPatients(Mockito.any(), Mockito.any()))
		       .thenReturn(responseVO);
		Pharmacist pharmacist =
				TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole());
		Mockito.when(mockPharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		Mockito.when(mockPharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		Mockito.when(mockUtility.convertPharmacistEntityToModel(Mockito.any()))
		       .thenReturn(null);
		try {
			pharmacistService.getPatientsForGSEnabled(jwtUserDetails, "new", requestVO);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof DataValidationException);
			DataValidationException exception = (DataValidationException) e.getCause();
			assertEquals(APIErrorFields.AUTHORIZATION, exception.getFieldName());
			assertEquals(IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES, exception.getErrorKey());
		}

		/**
		 * Case 2 : Exception from validating user.
		 */
		DataValidationException dataValidationException =
				new DataValidationException(APIErrorFields.AUTHORIZATION, IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES);
		Mockito.when(mockUserValidator.validatePharmacistPatients(Mockito.any()))
		       .thenThrow(dataValidationException);
		try {
			pharmacistService.getPatientsForGSEnabled(jwtUserDetails, "new", requestVO);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof DataValidationException);
			DataValidationException exception = (DataValidationException) e.getCause();
			assertEquals(APIErrorFields.AUTHORIZATION, exception.getFieldName());
			assertEquals(IWSGlobalApiErrorKeys.ERRORS_NO_PREVIEGES, exception.getErrorKey());
		}
	}
}
