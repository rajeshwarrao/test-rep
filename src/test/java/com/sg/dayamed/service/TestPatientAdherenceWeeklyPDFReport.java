package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.repository.MyEntityRepositoryCustom;
import com.sg.dayamed.service.impl.PaitentAdherenceWeeklyPDFReportImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Eresh Gorantla on 06/May/2019
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({InternetAddress.class, Transport.class})
public class TestPatientAdherenceWeeklyPDFReport extends BaseTestCase {

	@InjectMocks
	private PaitentAdherenceWeeklyPDFReport paitentAdherenceWeeklyPDFReport =
			new PaitentAdherenceWeeklyPDFReportImpl();

	@Mock
	private RediseService mockRediseService;

	@Mock
	private MyEntityRepositoryCustom mockMyEntityRepositoryCustom;

	@Before
	public void setUp() throws Exception {
		PowerMockito.suppress(PowerMockito.constructor(InternetAddress.class, String.class));
		PowerMockito.mock(Transport.class);
		PowerMockito.mockStatic(Transport.class);
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.patientNoOfDaysAdherenceDataForWeeklyReportFooter)))
		       .thenReturn("10");
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.patientDetailsQueryForRecipienEamils)))
		       .thenReturn("Receipient Emails");
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.patientAdherenceDataQueryForWeeklyReport)))
		       .thenReturn("Adherence Data Query");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.emailHost)))
		       .thenReturn("Mock Host");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.emailPort)))
		       .thenReturn("22");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.emailUsername)))
		       .thenReturn("Mock User");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.emailCredentials)))
		       .thenReturn("Mock Password");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.weeklyReportEmailContent)))
		       .thenReturn("Message");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.weeklyReportEmailContent)))
		       .thenReturn("Mock Sender");
		Mockito.when(mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.emailSender)))
		       .thenReturn("test@dayamed.com");
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.patientAdherenceDataForWeeklyReportImagePath)))
		       .thenReturn("Image path");
		Mockito.when(mockRediseService.getDataInRedisByKey(
				Mockito.eq(RedisKeyConstants.patientAdherenceDataForWeeklyReportFooterLinks)))
		       .thenReturn("Image path");
	}

	@Test
	public void testGetPatientRecordsWithNUllData() {
		Mockito.when(
				mockRediseService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.patientDetailsQueryForWeeklyReoprt)))
		       .thenReturn("Detail Weekly Query");
		List<Map<String, String>> userDetailsResult = new ArrayList<>();
		Map<String, String> mapData = new HashMap<>();
		mapData.put("patientId", "1");
		mapData.put("patientPrescrId", "1");
		userDetailsResult.add(mapData);
		Mockito.when(mockMyEntityRepositoryCustom.getColmNameAndColumValueAsMap(Mockito.anyString()))
		       .thenReturn(userDetailsResult);
		List<Map<String, String>> mailIDResult = new ArrayList<>();
		Map<String, String> mailMap = new HashMap<>();
		mailMap.put("1", "test@dayamed.com");
		mailIDResult.add(mailMap);
		Mockito.when(mockMyEntityRepositoryCustom.getColmNameAndColumValueAsMapWithInParams(Mockito.anyString(),
		                                                                                    Mockito.anyMap()))
		       .thenReturn(mailIDResult);
		paitentAdherenceWeeklyPDFReport.getPatientRecoreds();
	}
}
