package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.controller.ws.ImanticNotificationReqModel;
import com.sg.dayamed.entity.GSVUCAToken;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.helper.pojo.ZoomEndMeetingReqModel;
import com.sg.dayamed.repository.ImanticNotificationRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.UserDetailsRepository;
import com.sg.dayamed.rest.commons.VUCAURestResponse;
import com.sg.dayamed.service.impl.TokenNotValidationServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DataForImantics;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.TestResponseGenerator;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.http.HttpStatus;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestTokenNotValidationService extends BaseTestCase {

	@InjectMocks
	private TokenNotValidationService tokenNotValidationService = new TokenNotValidationServiceImpl();

	@Mock
	RediseService rediseService;

	@Mock
	NotificationService notificationService;

	@Mock
	WeeklyEmailNotificationToPatient weeklyEmailNotificationToPatient;

	@Mock
	VucaService vucaService;

	String ndcImagesFolderPath = "D:/test";

	@Mock
	PrescriptionRepository prescriptionRepository;

	@Mock
	UserDetailsRepository userDetailsRepository;

	@Mock
	PatientService patientService;

	@Mock
	EmailAndSmsUtility emailAndSmsUtility;

	@Mock
	ImanticNotificationRepository imanticNotificationRepository;

	@Test
	public void testEndMeeting() throws Exception {
		String zoomNotificationToken = RandomStringUtils.randomAlphanumeric(10);
		Mockito.when(rediseService.getDataInRedisByKey(Mockito.anyString()))
		       .thenReturn(zoomNotificationToken);
		ZoomEndMeetingReqModel zoomEndMeetingReqModel = TestResponseGenerator.generateZoomEndMeetingReqModel();
		Notification notification = TestResponseGenerator.generateNotification();
		Mockito.when(notificationService.getNotificationByRoomName(Mockito.anyString()))
		       .thenReturn(notification);
		Mockito.when(notificationService.saveNotification(Mockito.any()))
		       .thenReturn(notification);
		Integer responseValue = tokenNotValidationService.endMeeting(zoomEndMeetingReqModel, zoomNotificationToken);
		assertNotNull(responseValue);
		assertEquals(HttpStatus.OK.value(), responseValue.intValue());
	}

	@Test
	public void testGetVUCAUrlbyGSToken() throws Exception {
		/*
		 * Case 1 : success case;
		 */
		GSVUCAToken gSVUCAToken = TestResponseGenerator.genrateGSVUCAToken();
		String vucaVideo = RandomStringUtils.randomAlphanumeric(20);
		Mockito.when(weeklyEmailNotificationToPatient.getNdcCodeByGSToken(Mockito.anyString()))
		       .thenReturn(gSVUCAToken);
		Mockito.when(vucaService.fetchVucaVideoURL(Mockito.anyString()))
		       .thenReturn(vucaVideo);
		VUCAURestResponse vucaRestResponse = (VUCAURestResponse) tokenNotValidationService
				.getVUCAUrlbyGSToken(RandomStringUtils.randomAlphanumeric(7));
		assertNotNull(vucaRestResponse);
		assertEquals(ApplicationConstants.SUCCESS, vucaRestResponse.getStatus());

		/*
		 * Case 1 : Fail case, if GSVUCAToken is null;
		 */
		Mockito.when(weeklyEmailNotificationToPatient.getNdcCodeByGSToken(Mockito.anyString()))
		       .thenReturn(null);
		VUCAURestResponse vucaRestResponseFail = (VUCAURestResponse) tokenNotValidationService
				.getVUCAUrlbyGSToken(RandomStringUtils.randomAlphanumeric(7));
		assertNotNull(vucaRestResponseFail);
		assertEquals(ApplicationConstants.FAIL, vucaRestResponseFail.getStatus());
		assertEquals("Invalid Token", vucaRestResponseFail.getMessage());

		/*
		 * Case 1 : Fail case, if vucaVideo is null;
		 */
		Mockito.when(weeklyEmailNotificationToPatient.getNdcCodeByGSToken(Mockito.anyString()))
		       .thenReturn(gSVUCAToken);
		Mockito.when(vucaService.fetchVucaVideoURL(Mockito.anyString()))
		       .thenReturn(null);
		VUCAURestResponse vucaResFailVideoNull = (VUCAURestResponse) tokenNotValidationService
				.getVUCAUrlbyGSToken(RandomStringUtils.randomAlphanumeric(7));
		assertNotNull(vucaResFailVideoNull);
		assertEquals(ApplicationConstants.FAIL, vucaResFailVideoNull.getStatus());
	}

	public void testWebErrorLogsSave() throws Exception {
		Mockito.when(rediseService.getDataInRedisByKey(Mockito.anyString()))
		       .thenReturn("");
		FileWriter errorFileWriter = PowerMockito.mock(FileWriter.class);
		PowerMockito.whenNew(FileWriter.class)
		            .withAnyArguments()
		            .thenReturn(errorFileWriter);
		VUCAURestResponse vucaResSucess = (VUCAURestResponse) tokenNotValidationService
				.webErrorLogsSave(RandomStringUtils.randomAlphanumeric(7));
		assertNotNull(vucaResSucess);
		assertEquals("Logs saved successfully", vucaResSucess.getStatus());
	}

	@Test
	public void testFetchPatientAttributes() throws Exception {
		/*
		 * Case 1 : Fail prescriptionList size is 0.
		 */
		try {
			List<Prescription> listPrescription = new ArrayList<>();
			Mockito.when(prescriptionRepository.findAll())
			       .thenReturn(listPrescription);
			Optional<String> patientID = Optional.empty();
			tokenNotValidationService.fetchPatientAttributes(patientID);
		} catch (ApplicationException e) {
			assertTrue(true);
		}

		/*
		 * Case 1 : success
		 */
		List<Prescription> listPrescription = new ArrayList<>();
		listPrescription.add(TestResponseGenerator.generatePrescription());
		Mockito.when(prescriptionRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(listPrescription);
		Optional<String> patientID = Optional.of(RandomStringUtils.randomNumeric(2));
		List<DataForImantics> listOfData = tokenNotValidationService.fetchPatientAttributes(patientID);
		assertNotNull(listOfData);
	}

	@Test
	public void testAddImanticNotification() throws Exception {
		/*
		 * case 1: Fail
		 */
		ImanticNotificationReqModel imanticNotificationReqModel = TestResponseGenerator
				.genrateImanticNotificationReqModel();
		try {
			Mockito.when(userDetailsRepository.findByImanticUserid(Mockito.anyString()))
			       .thenReturn(null);
			tokenNotValidationService.addImanticNotification(imanticNotificationReqModel);
		} catch (Exception e) {
			assertTrue(true);
		}

		/*
		 * case 2: success
		 */
		Mockito.when(userDetailsRepository.findByImanticUserid(Mockito.anyString()))
		       .thenReturn(TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT));
		Mockito.when(patientService.fetchPatientByUserId(Mockito.anyLong()))
		       .thenReturn(TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT));
		Mockito.when(imanticNotificationRepository.save(Mockito.any()))
		       .thenReturn(TestResponseGenerator.genrateImanticNotification());
		Mockito.when(notificationService.addWebNotification(Mockito.any()))
		       .thenReturn(TestResponseGenerator.generateNotification());
		Mockito.when(emailAndSmsUtility.sendEmail(Mockito.anyString(), Mockito.anyString(), Mockito.any()))
		       .thenReturn(true);
		Mockito.when(emailAndSmsUtility.sendSms(Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(true);
		String status = tokenNotValidationService.addImanticNotification(imanticNotificationReqModel);
		assertNotNull(status);
		assertEquals(ApplicationConstants.SUCCESS, status);
	}
}
