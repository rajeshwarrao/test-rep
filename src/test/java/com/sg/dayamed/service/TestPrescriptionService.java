package com.sg.dayamed.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;

import com.sg.dayamed.commons.rest.BaseRestApiImpl;
import com.sg.dayamed.dosage.frequency.DailyDosageFrequency;
import com.sg.dayamed.dosage.frequency.FrequencyAbstractFactory;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.ConsumptionTemplate;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.DosageNotification;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.PrescriptionStatus;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.PrescriptionHelper;
import com.sg.dayamed.helper.pojo.DeletePrescriptionVO;
import com.sg.dayamed.helper.pojo.GenericIDAndZoneVO;
import com.sg.dayamed.helper.pojo.UpdatePrescriptionStatusVO;
import com.sg.dayamed.pojo.ConsumerGoodInfoModel;
import com.sg.dayamed.pojo.ConsumerGoodsModel;
import com.sg.dayamed.pojo.DosageInfoModel;
import com.sg.dayamed.pojo.FirstConsumptionTimeModel;
import com.sg.dayamed.pojo.FristConsumptionTypeModel;
import com.sg.dayamed.pojo.MedicineModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.PrescriptionStatusModel;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.PrescriptionStatusRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.impl.PrescriptionServiceImpl;
import com.sg.dayamed.service.util.APIErrorKeys;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.MessageResourceUtility;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

@RunWith(MockitoJUnitRunner.class)
public class TestPrescriptionService {

	@InjectMocks
	PrescriptionServiceImpl prescriptionService;

	@Mock
	PrescriptionRepository mockPrescriptionRepository;

	@Mock
	Utility mockUtility;

	@Mock
	NotificationScheduleService mockJobService;

	@Mock
	PrescriptionScheduleRepository mockPrescriptionScheduleRepository;

	@Mock
	DateUtility mockDateUtility;

	@Mock
	PatientService mockPatientService;

	@Mock
	PHIDataEncryptionService mockPHIDataEncryptionService;

	@Mock
	CaregiverService mockCaregiverService;

	@Mock
	PatientRepository mockPatientRepository;

	@Mock
	PharmaService mockPharmaService;

	@Mock
	NotificationService mockNotificationService;

	@Mock
	PrescriptionStatusRepository mockPrescriptionStatusRepository;

	@Mock
	AdherenceService mockAdherenceService;

	@Mock
	FrequencyAbstractFactory mockFrequencyAbstractFactory;

	@Mock
	BaseRestApiImpl mockBaseRestApiImpl;

	@Mock
	private Authentication mockAuthentication;

	@Mock
	private HttpServletRequest mockHttpRequest;
	
	@Mock
	MessageResourceUtility mockMessageResourceUtility;
	
	/*@Mock
	JwtUserDetails mockJwtUser;*/

	@Mock
	PrescriptionHelper mockPrescriptionHelper;

	/*@Test
	public void testGeneratePrescriptionScheduleWithEmptyZoneId() {
		when(mockUtility.convertPrescriptionModelToEntity(any())).thenReturn(new Prescription());
		boolean prescriptionGenerated = prescriptionService.generatePrescriptionSchedule(1L, "");
		verify(mockDateUtility, never()).getDateTimeBasedOnZoneId(anyString());
		verify(mockPrescriptionScheduleRepository, never()).findByUserIdAndPrescriptionId(anyLong(), anyLong());
		//TODO: The method should return false when zoneId is null
		assertTrue(prescriptionGenerated);
	}
*/
	@Test
	public void testGeneratePrescriptionScheduleWithPrescriptionAndZoneId() throws DataValidationException {
		final Prescription prescription = TestResponseGenerator.generatePrescription();
		AtomicLong position = new AtomicLong();
		prescription.getDosageInfoList()
		            .forEach(dosageInfo -> {
			            dosageInfo.setFrequency(
					            "{\"type\":\"daily\",\"startDate\":\"10/16/2019\",\"value\":2,\"recurrence\":null," +
							            "\"endAfterOccurrences\":null}");
			            ConsumptionTemplate consumptionTemplate = new ConsumptionTemplate();
			            consumptionTemplate.setConsumptionsId(position.incrementAndGet());
			            consumptionTemplate.setConsumptionsKey("CUSTOM");
			            dosageInfo.setConsumptionTemplate(consumptionTemplate);
			            ArrayList<String> timeList = new ArrayList<String>();
			            timeList.add("8:00 am");
			            dosageInfo.setTime(timeList);
		            });
		prescription.getDeviceInfoList()
        .forEach(deviceInfo -> {
        	deviceInfo.setDuration(0);
        });
		
		
		LocalDateTime ldt = LocalDateTime.now();
		/*Mockito.when(mockDateUtility.convertdatetimeStringToLocalDateTime(Mockito.anyString()))
		       .thenReturn(ldt);*/
		/*Mockito.when(mockDateUtility.getFormattedLocalDateTime(Mockito.any(LocalDateTime.class)))
		       .thenReturn("2019-10-10 11:50");
		Mockito.when(mockDateUtility.convertLocalTimeToUTCTimeStringByzoneid(Mockito.anyString(), Mockito.anyString()))
		       .thenReturn("2019-10-10 11:00");*/

		//Optional<Prescription> optionalPrescription = prescriptionRepository.findById(id);
		Mockito.when(mockPrescriptionRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(prescription));
		/*Mockito.when(mockDateUtility.convertUTCLocalDateTimeStringToObject(Mockito.anyString()))
		       .thenReturn(ldt);*/

		Mockito.when(mockFrequencyAbstractFactory.getFrequencyFactory(Mockito.anyString()))
		       .thenReturn(new DailyDosageFrequency());

		//Mockito.when(mockDateUtility.addTimestringToLacalDate(Mockito.anyString(), Mockito.any(LocalDate.class)))
		// .thenReturn(ldt);
		when(mockUtility.convertPrescriptionModelToEntity(any())).thenReturn(prescription);
		when(mockDateUtility.getDateTimeBasedOnZoneId(anyString())).thenReturn("2019-10-10 10:00");
		when(mockPrescriptionScheduleRepository.findByUserIdAndPrescriptionId(anyLong(), anyLong())).thenReturn(
				Collections.emptyList());

		boolean prescriptionGenerated = prescriptionService.generatePrescriptionSchedule(1L, "UTC");
		verify(mockDateUtility).getDateTimeBasedOnZoneId(anyString());
		verify(mockPrescriptionScheduleRepository).findByUserIdAndPrescriptionId(anyLong(), anyLong());
		assertTrue(prescriptionGenerated);
	}

	@Test
	public void testAddPrescription() throws DataValidationException {

		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockUtility.instanceOfPatient(patient, jwtUserDetails))
		       .thenReturn(200);


		Mockito.when(mockPatientService.directFetchPatientByPatientId(Mockito.anyLong()))
		       .thenReturn(patient);

		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any(Patient.class)))
		       .thenReturn(patientModel);

		Mockito.when(
				mockPrescriptionHelper.addPrescription(Mockito.any(PrescriptionModel.class),
				                                       Mockito.any(Patient.class),
				                                       Mockito.anyLong()))
		       .thenReturn(prescriptionModel);
		assertNotNull(prescriptionService.addPrescription(prescriptionModel, 1L, jwtUserDetails));
	}

	@Test
	public void testAddPrescriptionUnAuthorized() {
		try {
			PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
			Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
			UserDetails userDetails =
					TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
			JwtUserDetails jwtUserDetails =
					TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
			Mockito.when(mockPatientService.directFetchPatientByPatientId(Mockito.anyLong()))
			       .thenReturn(patient);
			Mockito.when(mockUtility.instanceOfPatient(patient, jwtUserDetails))
			       .thenReturn(401);
			prescriptionService.addPrescription(prescriptionModel, 1L, jwtUserDetails);
		} catch (DataValidationException dve) {
			assertEquals("errors.authorization.failed", dve.getErrorKey());
		}
	}

	@Test
	public void testAddPrescriptionPatientNotFound() {
		try {
			PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
			UserDetails userDetails =
					TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
			JwtUserDetails jwtUserDetails =
					TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
			prescriptionService.addPrescription(prescriptionModel, 1L, jwtUserDetails);
		} catch (DataValidationException dve) {
			assertEquals("error.provided.id.not.in.db", dve.getErrorKey());
		}
	}

	@Test
	public void testAddPrescriptionByCaregiver() throws DataValidationException {
		Prescription prescription = TestResponseGenerator.generatePrescription();
		DosageInfo dosageInfo = TestResponseGenerator.generateDosageInfo(true);
		DosageNotification dosageNotification = TestResponseGenerator.generateDosageNotification();
		List<DosageNotification> dosageNotificationList = new ArrayList<DosageNotification>();
		dosageNotificationList.add(dosageNotification);

		dosageInfo.setDosageNotificationList(dosageNotificationList);
		List<DosageInfo> dosageInfoList = new ArrayList<DosageInfo>();
		dosageInfoList.add(dosageInfo);
		prescription.setDosageInfoList(dosageInfoList);

		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		ConsumerGoodsModel consumerGoodsModel = TestResponseGenerator.generateConsumerGoodsModel(1L, "Boost");
		List<ConsumerGoodsModel> consumerGoodsModelList = new ArrayList<ConsumerGoodsModel>();
		consumerGoodsModelList.add(consumerGoodsModel);

		ConsumerGoodInfoModel consumerGoodInfoModel = new ConsumerGoodInfoModel();
		consumerGoodInfoModel.setCommodity(consumerGoodsModel);
		consumerGoodInfoModel.setId(1L);
		List<ConsumerGoodInfoModel> consumerGoodInfoModelList = new ArrayList<ConsumerGoodInfoModel>();
		consumerGoodInfoModelList.add(consumerGoodInfoModel);

		prescriptionModel.setCommodityInfoList(consumerGoodInfoModelList);
		DosageInfoModel dosageInfoModel = new DosageInfoModel();
		MedicineModel medicineModel = new MedicineModel();
		medicineModel.setId(1L);
		medicineModel.setName("Asprin");

		dosageInfoModel.setId(1L);
		dosageInfoModel.setMedicine(medicineModel);
		List<DosageInfoModel> dosageInfoModelList = new ArrayList<DosageInfoModel>();
		dosageInfoModelList.add(dosageInfoModel);

		prescriptionModel.setDosageInfoList(dosageInfoModelList);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.CAREGIVER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.CAREGIVER.getRole());
		Mockito.when(mockUtility.instanceOfPatient(patient, jwtUserDetails))
		       .thenReturn(200);

		//Mockito.when(mockAuthentication.getPrincipal()).thenReturn(jwtUserDetails);

		Mockito.when(mockPatientService.directFetchPatientByPatientId(Mockito.anyLong()))
		       .thenReturn(patient);

		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any(Patient.class)))
		       .thenReturn(patientModel);

		Mockito.when(mockPrescriptionRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(prescription));

		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any(Prescription.class)))
		       .thenReturn(prescription);

		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any(Prescription.class)))
		       .thenReturn(prescriptionModel);
		Mockito.when(
				mockPrescriptionHelper.addPrescription(Mockito.any(PrescriptionModel.class),
				                                       Mockito.any(Patient.class),
				                                       Mockito.anyLong()))
		       .thenReturn(prescriptionModel);
		assertNotNull(prescriptionService.addPrescription(prescriptionModel, 1L, jwtUserDetails));
	}

	@Test
	public void validateFetchLatestPrescriptionByPatientId() throws DataValidationException, ParseException {

		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Prescription prescription = TestResponseGenerator.generatePrescription();
		PrescriptionModel PrescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		PrescriptionModel.setDate("28.12.2019 - 11.35.27");
		Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong()))
		       .thenReturn(patient);
		Mockito.when(mockDateUtility.convertDate24hoursFormatTo12hourForamat(Mockito.anyString())).thenReturn("28.12.2019 - 11.35.27 am");
		Mockito.when(mockPrescriptionRepository.findTop1ByPatient_idOrderByUpdatedDateDesc(Mockito.anyLong()))
		       .thenReturn(prescription);
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any(Prescription.class)))
		       .thenReturn(prescription);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any(Prescription.class)))
		       .thenReturn(PrescriptionModel);

		Mockito.when(mockPHIDataEncryptionService.getKeyText(null, mockAuthentication))
		       .thenReturn("key");
		mockPHIDataEncryptionService.encryptPatient(Mockito.any(PatientModel.class), Mockito.anyString());
		assertNotNull(prescriptionService.validateFetchLatestPrescriptionByPatientId(1L, mockAuthentication
		                                                                             ));
	}

	@Test
	public void validateFetchLatestPrescriptionByPatientIdPatintNotFound() throws ParseException {
		try {
			Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong()))
			       .thenReturn(null);
			prescriptionService.validateFetchLatestPrescriptionByPatientId(1L, mockAuthentication);
		} catch (DataValidationException dve) {
			assertEquals("error.provided.id.not.in.db", dve.getErrorKey());
		}
	}

	@Test
	public void fetchPrescriptionListByPatientId() throws DataValidationException {

		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		PrescriptionModel PrescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		List<PrescriptionModel> prescriptionModelList = new ArrayList<PrescriptionModel>();
		prescriptionModelList.add(PrescriptionModel);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(null, mockAuthentication))
		       .thenReturn("key");
		mockPHIDataEncryptionService.encryptPatient(Mockito.any(PatientModel.class), Mockito.anyString());

		Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong()))
		       .thenReturn(patient);
		Mockito.when(mockPatientService.fetchPrescriptionlistByPatientId(Mockito.anyLong()))
		       .thenReturn(prescriptionModelList);
		assertTrue(prescriptionService.fetchPrescriptionListByPatientId(1L, mockAuthentication)
		                              .size() > 0);
	}

	@Test
	public void fetchPrescriptionListByPatientIdPatientNotFound() {
		try {
			Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong()))
			       .thenReturn(null);
			prescriptionService.fetchPrescriptionListByPatientId(1L, mockAuthentication);
		} catch (DataValidationException dve) {
			assertEquals("error.provided.id.not.in.db", dve.getErrorKey());
		}
	}

	@Test // no need exception enuf
	public void fetchPrescriptionByPrescriptionIdException() {

		Prescription prescription = TestResponseGenerator.generatePrescription();
		DosageInfo dosageInfo = TestResponseGenerator.generateDosageInfo(true);
		DosageNotification dosageNotification = TestResponseGenerator.generateDosageNotification();
		List<DosageNotification> dosageNotificationList = new ArrayList<DosageNotification>();
		dosageNotificationList.add(dosageNotification);

		dosageInfo.setDosageNotificationList(dosageNotificationList);
		List<DosageInfo> dosageInfoList = new ArrayList<DosageInfo>();
		dosageInfoList.add(dosageInfo);
		prescription.setDosageInfoList(dosageInfoList);
		Mockito.when(mockPrescriptionRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		try {
			prescriptionService.fetchPrescriptionByPrescriptionId(1L, 1L);
		} catch (DataValidationException de) {
			assertEquals("error.provided.id.not.in.db", de.getErrorKey());
		}
	}

	@Test
	public void createSchedulerJobsByPrescription() throws DataValidationException {

		Prescription prescription = TestResponseGenerator.generatePrescription();
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		DosageInfo dosageInfo = TestResponseGenerator.generateDosageInfo(true);
		DosageNotification dosageNotification = TestResponseGenerator.generateDosageNotification();
		List<DosageNotification> dosageNotificationList = new ArrayList<DosageNotification>();
		dosageNotificationList.add(dosageNotification);

		dosageInfo.setDosageNotificationList(dosageNotificationList);
		List<DosageInfo> dosageInfoList = new ArrayList<DosageInfo>();
		dosageInfoList.add(dosageInfo);
		prescription.setDosageInfoList(dosageInfoList);
		Mockito.when(mockPrescriptionRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(prescription));
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any(Prescription.class)))
		       .thenReturn(prescription);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any(Prescription.class)))
		       .thenReturn(prescriptionModel);
		Mockito.when(mockUtility.convertPrescriptionModelToEntity(Mockito.any(PrescriptionModel.class)))
		       .thenReturn(prescription);
		Mockito.when(mockJobService.addSchedulerJob(Mockito.any(Prescription.class), Mockito.anyString()))
		       .thenReturn(true);
		assertTrue(prescriptionService.createSchedulerJobsByPrescription(1L, "Asia/Kolkata"));
	}

	@Test
	public void fetchPrescriptionEntityByPrescriptionId() {
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockPrescriptionRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(prescription));
		assertNotNull(prescriptionService.fetchPrescriptionEntityByPrescriptionId(1L));
	}

	@Test
	public void addPrescription() {
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockPrescriptionRepository.save(Mockito.any(Prescription.class)))
		       .thenReturn(prescription);
		assertNotNull(prescriptionService.addPrescription(prescription));
	}

	@Test
	public void fetchPrescriptionsByPatientId() {

		Prescription prescription = TestResponseGenerator.generatePrescription();
		List<Prescription> prescriptionList = new ArrayList<Prescription>();
		prescriptionList.add(prescription);

		Mockito.when(mockPrescriptionRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(prescriptionList);
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(prescription))
		       .thenReturn(prescription);
		List<Prescription> actualPrescriptionList = prescriptionService.fetchPrescriptionsByPatientId(1L);
		assertTrue(actualPrescriptionList.size() > 0);
	}

	@Test
	public void fetchActivePrescriptionsByPatientId() {
		Prescription prescription = TestResponseGenerator.generatePrescription();
		List<Prescription> prescriptionList = new ArrayList<Prescription>();
		prescriptionList.add(prescription);
		Mockito.when(mockPrescriptionRepository.findByPatient_idAndEndDateGreaterThanOrEndDateIsNull(Mockito.anyLong(),
		                                                                                             Mockito.any(
				                                                                                             LocalDateTime.class)))
		       .thenReturn(prescriptionList);
		LocalDateTime rightNow = LocalDateTime.now();
		assertTrue(prescriptionService.fetchActivePrescriptionsByPatientId(1L, rightNow)
		                              .size() > 0);
	}

	@Test
	public void fetchActivePrescriptionListByPatientId()
			throws JSONException, ParseException, ApplicationException, DataValidationException {

		Prescription prescription = TestResponseGenerator.generatePrescription();
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		List<Prescription> prescriptionList = new ArrayList<Prescription>();
		prescriptionList.add(prescription);

		Mockito.when(mockPrescriptionRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(prescriptionList);
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(prescription))
		       .thenReturn(prescription);

		DosageInfoModel dosageInfoModel = new DosageInfoModel();
		MedicineModel medicineModel = new MedicineModel();
		medicineModel.setId(1L);
		medicineModel.setName("Asprin");

		dosageInfoModel.setId(1L);
		dosageInfoModel.setMedicine(medicineModel);
		List<DosageInfoModel> dosageInfoModelList = new ArrayList<DosageInfoModel>();
		dosageInfoModelList.add(dosageInfoModel);
		prescriptionModel.setDosageInfoList(dosageInfoModelList);

		//AtomicLong position = new AtomicLong();
		prescriptionModel.getDosageInfoList()
		                 .forEach(dosageInfo -> {
			                 dosageInfo.setFrequency(
					                 "{\"type\":\"daily\",\"startDate\":\"10/16/2019\",\"value\":2," +
							                 "\"recurrence\":null,\"endAfterOccurrences\":null}");

			                 FristConsumptionTypeModel fristConsumptionTypeModel = new FristConsumptionTypeModel();

			                 FirstConsumptionTimeModel firstConsumptionTimeModel = new FirstConsumptionTimeModel();
			                 firstConsumptionTimeModel.setId(1L);
			                 firstConsumptionTimeModel.setName("sampleName");
			                 firstConsumptionTimeModel.setTimes("8:00 am");

			                 fristConsumptionTypeModel.setFirstConsumptionTime(firstConsumptionTimeModel);
			                 fristConsumptionTypeModel.setFrequencyPerDay(null);
			                 ArrayList<String> timeList = new ArrayList<String>();
			                 timeList.add("10:00 am");
			                 dosageInfo.setTime(timeList);
			                 dosageInfo.setFristConsumptionType(fristConsumptionTypeModel);
		                 });

		//FrequencyFactory frequencyFactory = frequencyAbstractFactory.getFrequencyFactory(type);
		Mockito.when(mockFrequencyAbstractFactory.getFrequencyFactory(Mockito.anyString()))
		       .thenReturn(new DailyDosageFrequency());
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any(Prescription.class)))
		       .thenReturn(prescriptionModel);

		GenericIDAndZoneVO genericIDAndZoneVO = new GenericIDAndZoneVO();
		genericIDAndZoneVO.setPatientId("147");
		genericIDAndZoneVO.setZoneId("Asia/Kolkata");

		assertTrue(prescriptionService.fetchActivePrescriptionListByPatientId(genericIDAndZoneVO, mockAuthentication
		                                                                      )
		                              .size() > 0);
	}
	
	@Test
	public void fetchActivePrescriptionListByPatientIdConsumptionTypeNull()
			throws JSONException, ParseException, ApplicationException, DataValidationException {

		Prescription prescription = TestResponseGenerator.generatePrescription();
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		List<Prescription> prescriptionList = new ArrayList<Prescription>();
		prescriptionList.add(prescription);

		Mockito.when(mockPrescriptionRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(prescriptionList);
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(prescription))
		       .thenReturn(prescription);

		DosageInfoModel dosageInfoModel = new DosageInfoModel();
		MedicineModel medicineModel = new MedicineModel();
		medicineModel.setId(1L);
		medicineModel.setName("Asprin");

		dosageInfoModel.setId(1L);
		dosageInfoModel.setMedicine(medicineModel);
		List<DosageInfoModel> dosageInfoModelList = new ArrayList<DosageInfoModel>();
		dosageInfoModelList.add(dosageInfoModel);
		prescriptionModel.setDosageInfoList(dosageInfoModelList);

		//AtomicLong position = new AtomicLong();
		prescriptionModel.getDosageInfoList()
		                 .forEach(dosageInfo -> {
			                 dosageInfo.setFrequency(
					                 "{\"type\":\"daily\",\"startDate\":\"10/16/2019\",\"value\":2," +
							                 "\"recurrence\":null,\"endAfterOccurrences\":null}");

			                 ArrayList<String> timeList = new ArrayList<String>();
			                 timeList.add("10:00 am");
			                 dosageInfo.setTime(timeList);
			                 dosageInfo.setFristConsumptionType(null);
		                 });

		//FrequencyFactory frequencyFactory = frequencyAbstractFactory.getFrequencyFactory(type);
		Mockito.when(mockFrequencyAbstractFactory.getFrequencyFactory(Mockito.anyString()))
		       .thenReturn(new DailyDosageFrequency());
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any(Prescription.class)))
		       .thenReturn(prescriptionModel);

		GenericIDAndZoneVO genericIDAndZoneVO = new GenericIDAndZoneVO();
		genericIDAndZoneVO.setPatientId("147");
		genericIDAndZoneVO.setZoneId("Asia/Kolkata");

		assertTrue(prescriptionService.fetchActivePrescriptionListByPatientId(genericIDAndZoneVO, mockAuthentication)
		                              .size() > 0);
	}
	
	@Test
	public void fetchActivePrescriptionListByPatientIdException() throws ApplicationException{
		try{
			//String keyText = pHIDataEncryptionService.getKeyText(request, authentication);
			GenericIDAndZoneVO genericIDAndZoneVO = new GenericIDAndZoneVO();
			genericIDAndZoneVO.setPatientId("147");
			genericIDAndZoneVO.setZoneId("Asia/Kolkata");
			Mockito.when(mockPHIDataEncryptionService.getKeyText(Mockito.isNull(),Mockito.any(Authentication.class))).thenThrow(new RuntimeException());
			prescriptionService.fetchActivePrescriptionListByPatientId(genericIDAndZoneVO, mockAuthentication);
		}catch(DataValidationException dav){
			assertEquals("error.request.invalid.data", dav.getErrorKey());
		}
		
	}

	@Test
	public void validateFetchPrescriptionByPrescriptionId() throws DataValidationException {

		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
		JwtUserDetails jwtUserDetails =
				TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(jwtUserDetails);

		Prescription prescription = TestResponseGenerator.generatePrescription();
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		DosageInfo dosageInfo = TestResponseGenerator.generateDosageInfo(true);
		DosageNotification dosageNotification = TestResponseGenerator.generateDosageNotification();
		List<DosageNotification> dosageNotificationList = new ArrayList<DosageNotification>();
		dosageNotificationList.add(dosageNotification);

		dosageInfo.setDosageNotificationList(dosageNotificationList);
		List<DosageInfo> dosageInfoList = new ArrayList<DosageInfo>();
		dosageInfoList.add(dosageInfo);
		prescription.setDosageInfoList(dosageInfoList);
		Mockito.when(mockPrescriptionRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(prescription));

		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any(Prescription.class)))
		       .thenReturn(prescription);

		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any(Prescription.class)))
		       .thenReturn(prescriptionModel);

		Mockito.when(mockUtility.instanceOfPrescriptionModel(Mockito.any(PrescriptionModel.class),
		                                                     Mockito.any(JwtUserDetails.class)))
		       .thenReturn(200);

		assertNotNull(
				prescriptionService.validateFetchPrescriptionByPrescriptionId(1L,
				                                                              mockAuthentication));
	}
	
	@Test
	public void validateFetchPrescriptionByPrescriptionIdAsPrescriptionModelNull() {
		try{
			UserDetails userDetails =
					TestResponseGenerator.generateUserDetails("Male", "Asian", UserRoleEnum.PROVIDER.getRole());
			JwtUserDetails jwtUserDetails =
					TestResponseGenerator.generateJwtUserDetails(userDetails, true, UserRoleEnum.PROVIDER.getRole());
			Mockito.when(mockAuthentication.getPrincipal())
			       .thenReturn(jwtUserDetails);
			prescriptionService.validateFetchPrescriptionByPrescriptionId(1L,
                    mockAuthentication);
		}catch(DataValidationException dve){
			assertEquals("error.provided.id.not.in.db", dve.getErrorKey());
		}
	}

	@Test
	public void updatePrescriptionStatus() {

		Prescription prescription = TestResponseGenerator.generatePrescription();
		PrescriptionModel prescriptionModel = TestResponseGenerator.generatePrescriptionModel();
		PrescriptionStatusModel prescriptionStatus = new PrescriptionStatusModel();
		prescriptionStatus.setId(1L);
		prescriptionStatus.setName("INPROCESS");
		prescriptionModel.setPrescriptionStatus(prescriptionStatus);
		Mockito.when(mockUtility.convertPrescriptionModelToEntity(Mockito.any(PrescriptionModel.class)))
		       .thenReturn(prescription);
		Mockito.when(mockPatientService.updatePrescription(Mockito.any(Prescription.class)))
		       .thenReturn(prescriptionModel);
		assertNotNull(
				prescriptionService.updatePrescriptionStatus(prescriptionModel, mockAuthentication));
	}

	@Test
	public void updatePrescriptionStatusByStatusString() {

		PrescriptionStatus prescriptionStatus = new PrescriptionStatus();
		prescriptionStatus.setId(1L);
		prescriptionStatus.setName("INPROCESS");
		Mockito.when(mockPrescriptionStatusRepository.findByName(Mockito.anyString()))
		       .thenReturn(prescriptionStatus);

		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockPrescriptionRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(prescription));

		UpdatePrescriptionStatusVO updatePrescriptionStatusVO = new UpdatePrescriptionStatusVO();
		updatePrescriptionStatusVO.setStatus("inprocess");
		updatePrescriptionStatusVO.setPrescriptionId("1");

		String result = prescriptionService.updatePrescriptionStatusByStatusString(updatePrescriptionStatusVO);
		assertEquals("Status updated", result);
	}
	
	@Test
	public void updatePrescriptionStatusByStatusStringBadRequestAsPrescriptionStatusNull() {
			
		Mockito.when(mockPrescriptionStatusRepository.findByName(Mockito.anyString())).thenReturn(null);
		UpdatePrescriptionStatusVO updatePrescriptionStatusVO = new UpdatePrescriptionStatusVO();
			updatePrescriptionStatusVO.setStatus("inprocess");
			updatePrescriptionStatusVO.setPrescriptionId("1");
			String result = prescriptionService.updatePrescriptionStatusByStatusString(updatePrescriptionStatusVO);
		    assertEquals(result, "Badrequest");
	}
	@Test
	public void updatePrescriptionStatusByStatusStringBadRequestAsPrescriptionNull() {
			UpdatePrescriptionStatusVO updatePrescriptionStatusVO = new UpdatePrescriptionStatusVO();
			updatePrescriptionStatusVO.setStatus("inprocess");
			updatePrescriptionStatusVO.setPrescriptionId("1");
			String result = prescriptionService.updatePrescriptionStatusByStatusString(updatePrescriptionStatusVO);
		    assertEquals(result, "Badrequest");
	}

	@Test
	public void deletePrescriptionById() {
		List<String> consumptionStatusList = new ArrayList<String>();
		consumptionStatusList.add("consumed");

		AdherenceDataPoints adherenceDataPoints =
				TestResponseGenerator.generateAdherenceDataPoints(consumptionStatusList);
		List<AdherenceDataPoints> adherenceDataPointsList = new ArrayList<AdherenceDataPoints>();
		adherenceDataPointsList.add(adherenceDataPoints);
		Mockito.when(mockAdherenceService.findByprescriptionID(Mockito.anyLong()))
		       .thenReturn(adherenceDataPointsList);

		DeletePrescriptionVO deletePrescriptionVO = new DeletePrescriptionVO();
		deletePrescriptionVO.setMessage("sample message");
		deletePrescriptionVO.setStatus("fail");
		assertNotNull(prescriptionService.deletePrescriptionById(1L));
	}

	@Test
	public void deletePrescriptionByIdAsPrescriptionStarted() {
		List<String> consumptionStatusList = new ArrayList<String>();
		consumptionStatusList.add("consumed");
		List<AdherenceDataPoints> adherenceDataPointsList = new ArrayList<AdherenceDataPoints>();
		Mockito.when(mockAdherenceService.findByprescriptionID(Mockito.anyLong()))
		       .thenReturn(adherenceDataPointsList);
		Mockito.when(mockPrescriptionRepository.deletePrescription(Mockito.anyLong()))
		       .thenReturn("success");
		DeletePrescriptionVO deletePrescriptionVO = new DeletePrescriptionVO();
		deletePrescriptionVO.setMessage("sample message");
		deletePrescriptionVO.setStatus("pass");
		assertNotNull(prescriptionService.deletePrescriptionById(1L));
	}

}
