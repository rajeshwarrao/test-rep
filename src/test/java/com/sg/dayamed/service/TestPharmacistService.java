package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.service.impl.PharmacistServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 08/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestPharmacistService extends BaseTestCase {

	@InjectMocks
	private PharmacistService pharmacistService = new PharmacistServiceImpl();

	@Mock
	private PharmacistRepository mockPharmacistRepository;

	@Mock
	private Utility mockUtility;

	@Mock
	private PatientRepository mockPatientRepository;

	private Pharmacist pharmacist = null;

	private PharmacistModel pharmacistModel = null;

	@Mock
	PHIDataEncryptionService mockPHIDataEncryptionService;

	@Before
	public void setUp() throws Exception {
		pharmacist = TestResponseGenerator.generatePharmacist("Male", "Asian", "Pharmacist");
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(pharmacist));
		pharmacistModel = generateRequestObject("PharmacistModel-1", PharmacistModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPharmacistEntityToModel(Mockito.any()))
		       .thenReturn(pharmacistModel);
	}

	@Test
	public void testFetchPharmacistListByPatientId() throws Exception {
		Mockito.when(mockPharmacistRepository.findByPatients_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(pharmacist)
		                         .collect(Collectors.toList()));
		List<PharmacistModel> actualPharmacistModels = pharmacistService.fetchPharmacistListByPatientId(1l);
		assertTrue(CollectionUtils.isNotEmpty(actualPharmacistModels));
		assertEquals(Stream.of(pharmacistModel)
		                   .collect(Collectors.toList()), actualPharmacistModels);
	}

	@Test
	public void testFetchPharmacistByUserId() throws Exception {
		Mockito.when(mockPharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		PharmacistModel actualPharmacistModel = pharmacistService.fetchPharmacistByUserId(1l);
		assertNotNull(actualPharmacistModel);
		assertEquals(pharmacistModel, actualPharmacistModel);
	}

	@Test
	public void testFetchPatientsByPharmacistId() throws Exception {
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Mockito.when(mockPatientRepository.findByPharmacists_idAndStatus(Mockito.anyLong(), Mockito.anyInt()))
		       .thenReturn(Stream.of(patient)
		                         .collect(Collectors.toList()));
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		List<PatientModel> actualPatientModels = pharmacistService.fetchPatientsByPharmacistId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualPatientModels));
		assertEquals(Stream.of(patientModel)
		                   .collect(Collectors.toList()), actualPatientModels);
	}

	@Test
	public void testAddPharmacist() {
		Mockito.when(mockPharmacistRepository.save(Mockito.any()))
		       .thenReturn(pharmacist);
		PharmacistModel actualPharmacistModel = pharmacistService.addPharmacist(pharmacist);
		assertNotNull(actualPharmacistModel);
		assertEquals(pharmacistModel, actualPharmacistModel);
	}

	@Test
	public void testFetchPharmacistById() {
		/**
		 * Case 1 : Non null response
		 */
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(pharmacist));
		Pharmacist actualPharmacist = pharmacistService.fetchPharmacistById(1);
		assertNotNull(actualPharmacist);
		assertEquals(pharmacist, actualPharmacist);

		/**
		 * Case 2 : Null Response
		 */
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPharmacist = pharmacistService.fetchPharmacistById(1);
		assertNull(actualPharmacist);
	}

	@Test
	public void testDirectFetchPharmacistById() {
		/**
		 * Case 1 : Non null response
		 */
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(pharmacist));
		Pharmacist actualPharmacist = pharmacistService.directFetchPharmacistById(1);
		assertNotNull(actualPharmacist);
		assertEquals(pharmacist, actualPharmacist);

		/**
		 * Case 2 : Null Response
		 */
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPharmacist = pharmacistService.directFetchPharmacistById(1);
		assertNull(actualPharmacist);
	}

	@Test
	public void testDirectFetchPharmacistByUserId() {
		Mockito.when(mockPharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		Pharmacist actualPharmacist = pharmacistService.directFetchPharmacistByUserId(1);
		assertNotNull(actualPharmacist);
		assertEquals(pharmacist, actualPharmacist);
	}

	@Test
	public void testFetchPatientsByPharmacistIdFromCookie() throws Exception {
		Mockito.when(mockPharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(pharmacist);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Mockito.when(mockPatientRepository.findByPharmacists_idAndStatus(Mockito.anyLong(), Mockito.anyInt()))
		       .thenReturn(Stream.of(patient)
		                         .collect(Collectors.toList()));
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Mockito.when(mockUtility.convertPharmasistModelToEntity(Mockito.any()))
		       .thenReturn(pharmacist);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(Mockito.any()))
		       .thenReturn("keys");
		Mockito.when(mockPHIDataEncryptionService.encryptPatientList(Mockito.any(), Mockito.anyString()))
		       .thenReturn(Stream.of(patientModel)
		                         .collect(
				                         Collectors.toList()));
		List<PatientModel> listPatientModels = pharmacistService.getPatientsByPharmacistIdentifier(
				TestResponseGenerator.generateJwtUserDetails(pharmacist.getUserDetails(), true,
				                                             ApplicationConstants.PHARMACIST));
		assertNotNull(listPatientModels);
		assertEquals(Stream.of(patientModel)
		                   .collect(
				                   Collectors.toList()), listPatientModels);
	}

}
