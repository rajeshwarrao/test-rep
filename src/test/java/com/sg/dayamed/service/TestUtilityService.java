package com.sg.dayamed.service;

import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.pojo.MultiplePrescriptionSchedulingRequestModel;
import com.sg.dayamed.service.impl.UtilityServiceImpl;

import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestUtilityService {

	@InjectMocks
	UtilityService utilityService = new UtilityServiceImpl();

	@Mock
	PrescriptionService prescriptionService;


	@Test
	public void testStartMultiplePrescriptionScheduling() throws Exception {
		MultiplePrescriptionSchedulingRequestModel multiplePrescriptionSchedulingRequestModel =
				new MultiplePrescriptionSchedulingRequestModel();
		List<Long> prescriptionIds = new ArrayList<>();
		prescriptionIds.add(1l);
		multiplePrescriptionSchedulingRequestModel.setPrescriptionIds(prescriptionIds);
		multiplePrescriptionSchedulingRequestModel.setZoneId("Asia/Kolkata");
		Prescription prescription = TestResponseGenerator.generatePrescription();
		prescription.setCanPatientModify(true);
		Mockito.when(prescriptionService.fetchPrescriptionEntityByPrescriptionId(Mockito.anyLong()))
		       .thenReturn(prescription);
		//Mockito.when(userPreferenceService.fetchUserPreferenceTimeByUserId(Mockito.anyLong()))
		//  .thenReturn("2:00 AM");
		Mockito.when(prescriptionService.generatePrescriptionSchedule(Mockito.anyLong(), Mockito.anyString()))
		       .thenReturn(true);
		Mockito.when(prescriptionService.createSchedulerJobsByPrescription(Mockito.anyLong(), Mockito.anyString()))
		       .thenReturn(true);
		boolean isUpdated =
				utilityService.startMultiplePrescriptionScheduling(multiplePrescriptionSchedulingRequestModel);
		assertTrue(isUpdated);
	}

}
