package com.sg.dayamed.service;

import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.entity.UserPreference;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.UserPreferenceRepository;
import com.sg.dayamed.service.impl.UserPreferenceServiceImpl;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.enums.UserRoleEnum;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestUserPreferenceService {

	@InjectMocks
	UserPreferenceServiceImpl mockUserPreferenceService;

	@Mock
	UserPreferenceRepository mockUserPreferenceRepository;

	@Mock
	UserDetailsService mockUserDetailsService;

	@Mock
	ObjectMapper mockUserPreferenceModelMapper;

	@Test
	public void testAddUserPreference() {

		try {
			UserPreferenceModel userPreferenceModel = TestResponseGenerator.generateUserPreferenceModel();
			UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
			                                                                    UserRoleEnum.PATIENT.getRole());
			Mockito.when(mockUserDetailsService.fetchUserDetailEntitiysById(Mockito.anyLong()))
			       .thenReturn(userDetails);
			UserPreference userPreference = TestResponseGenerator.generateUserPreference();
			Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
			       .thenReturn(userPreference);
			Mockito.when(mockUserPreferenceRepository.save(Mockito.any(UserPreference.class)))
			       .thenReturn(userPreference);
			UserPreference actualUserPreference =
					mockUserPreferenceService.addUserPreference(Mockito.anyLong(), userPreferenceModel);
			assertNotNull(actualUserPreference);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test // case2
	public void testAddUserPreferencewithNull() {
		try {
			UserPreferenceModel userPreferenceModel = TestResponseGenerator.generateUserPreferenceModel();
			UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
			                                                                    UserRoleEnum.PATIENT.getRole());
			Mockito.when(mockUserDetailsService.fetchUserDetailEntitiysById(Mockito.anyLong()))
			       .thenReturn(userDetails);
			Mockito.when(mockUserPreferenceModelMapper.writeValueAsString(Mockito.anyString()))
			       .thenReturn("some error");
			UserPreference userPreference = TestResponseGenerator.generateUserPreference();
			Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
			       .thenReturn(null);
			Mockito.when(mockUserPreferenceRepository.save(Mockito.any(UserPreference.class)))
			       .thenReturn(userPreference);
			UserPreference actualUserPreference =
					mockUserPreferenceService.addUserPreference(Mockito.anyLong(), userPreferenceModel);
			assertNotNull(actualUserPreference);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	@Test  //(expected=JsonProcessingException.class) // case3
	public void testAddUserPreferencethrowError() throws Exception {
		try {
			UserPreferenceModel userPreferenceModel = TestResponseGenerator.generateUserPreferenceModel();
			UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
			                                                                    UserRoleEnum.PATIENT.getRole());
			Mockito.when(mockUserDetailsService.fetchUserDetailEntitiysById(Mockito.anyLong()))
			       .thenReturn(userDetails);
			Mockito.when(mockUserPreferenceModelMapper.writeValueAsString(Mockito.any()))
			       .thenThrow(new JsonProcessingException("") {
			       });

			Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
			       .thenReturn(null);
			UserPreference actualUserPreference =
					mockUserPreferenceService.addUserPreference(Mockito.anyLong(), userPreferenceModel);
			assertNull(actualUserPreference);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	@Test // case4
	public void testAddUserPreferencethrowException() {
		try {
			UserPreferenceModel userPreferenceModel = TestResponseGenerator.generateUserPreferenceModel();
			UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
			                                                                    UserRoleEnum.PATIENT.getRole());
			Mockito.when(mockUserDetailsService.fetchUserDetailEntitiysById(Mockito.anyLong()))
			       .thenReturn(userDetails);
			Mockito.when(mockUserPreferenceModelMapper.writeValueAsString(Mockito.anyString()))
			       .thenReturn("some error");
			Mockito.when(mockUserPreferenceRepository.save(Mockito.any(UserPreference.class)))
			       .thenThrow(RuntimeException.class);
			Mockito.when(mockUserPreferenceRepository.findByUserDetailsId(Mockito.anyLong()))
			       .thenReturn(null);
			UserPreference actualUserPreference =
					mockUserPreferenceService.addUserPreference(Mockito.anyLong(), userPreferenceModel);
			assertNull(actualUserPreference);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
