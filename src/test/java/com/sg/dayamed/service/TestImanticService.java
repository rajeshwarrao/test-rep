package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.Glucometer;
import com.sg.dayamed.entity.HeartRate;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Scale;
import com.sg.dayamed.entity.Steps;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.service.impl.ImanticServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.TestUtil;

import com.imantics.akp.sdk.pid.model.PidDataTable;
import com.imantics.dm.api.DmBiomidApi;
import com.imantics.dm.api.DmDiagnosticDataModel;
import com.imantics.dm.api.DmPatientDataApi;
import com.imantics.dm.api.DmPatientDataModel;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 05/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestImanticService extends BaseTestCase {

	@InjectMocks
	private ImanticService imanticService = new ImanticServiceImpl();

	@Mock
	private PatientService mockPatientService;

	@Mock
	private DateUtility mockDateUtility;

	@Mock
	private DosageInfoRepository mockDosageInfoRepository;

	private DmPatientDataApi mockDmPatientDataApi;

	@Mock
	private DmBiomidApi mockDmBiomidApi;

	@Mock
	private ImanticServiceImpl mockImanticServiceImpl;

	@BeforeClass
	public static void setUp() {
		PowerMockito.suppress(PowerMockito.constructor(ImanticServiceImpl.class));
	}

	@Test
	public void testGetImanticId() {
		String id = imanticService.getImanticId();
		assertNull(id);
	}

	// @Test
	// public void testCreatePatientInImantics() {
	//     mockDmPatientDataApi = Mockito.mock(DmPatientDataApi.class);
	//     ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", mockDmPatientDataApi);
	//     /**
	//      * Case 1 : Patient creation failed in Imantics.
	//      */
	//     Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
	//     Mockito.when(mockPatientService.fetchPatientById(Mockito.anyLong())).thenReturn(null);
	//     Mockito.when(mockImanticServiceImpl.genRandomNumber()).thenReturn(TestUtil.generateId());
	//     Map<String,String> responseMap = imanticService.createPatientInImantics(patient);
	//     assertNull(responseMap);

	//     /**
	//      * Case 2 : Patient creation Passed and returned Imantics id.
	//      */
	//     Mockito.when(mockDmPatientDataApi.createPatient(Mockito.any())).thenReturn(true);
	//     responseMap = imanticService.createPatientInImantics(patient);
	//     assertNotNull(responseMap);
	//     assertTrue(responseMap.containsKey("imanticid"));

	//     /**
	//      * Case 3 : DmPatientDataApi is not set
	//      */
	//     ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", null);
	//     responseMap = imanticService.createPatientInImantics(patient);
	//     assertNotNull(responseMap);
	//     assertFalse(responseMap.containsKey("imanticid"));
	// }

	// @Test
	// public void testUpdateDemographicData() {
	//     /**
	//      * Case 1 : Non null response from imatics.
	//      */
	//     mockDmPatientDataApi = Mockito.mock(DmPatientDataApi.class);
	//     ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", mockDmPatientDataApi);
	//     Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
	//     DmPatientDataModel patientDataModel = new DmPatientDataModel();
	//     Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString())).thenReturn(patientDataModel);
	//     imanticService.updateDemographicData(patient);
	//     Mockito.verify(mockDmPatientDataApi, Mockito.times(1)).updateEntirePatient(Mockito.any());

	//     /**
	//      * Case 2 : null response from imantics
	//      */
	//     Mockito.reset(mockDmPatientDataApi);
	//     Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString())).thenReturn(null);
	//     DmPatientDataApi patientDataApi = imanticService.updateDemographicData(patient);
	//     assertNull(patientDataApi);
	//     Mockito.verify(mockDmPatientDataApi, Mockito.times(0)).updateEntirePatient(Mockito.any());
	// }

	@Test
	public void testCreatePrescription() {
		/**
		 * Case 1 : Success case
		 */
		mockDmPatientDataApi = Mockito.mock(DmPatientDataApi.class);
		ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", mockDmPatientDataApi);
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		DmPatientDataModel patientDataModel = TestResponseGenerator.generateDmPatientDataModel(patient);
		Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString()))
		       .thenReturn(patientDataModel);
		String response = imanticService.createPrescription(prescription);
		assertNotNull(response);
		assertEquals(ApplicationConstants.SUCCESS, response);
		Mockito.verify(mockDmPatientDataApi, Mockito.times(1))
		       .updateEntirePatient(Mockito.any());

		/**
		 * Case 2 : Failure case without userDetails
		 */
		Mockito.reset(mockDmPatientDataApi);
		UserDetails userDetails = patient.getUserDetails();
		patient.setUserDetails(null);
		response = imanticService.createPrescription(prescription);
		assertNotNull(response);
		assertEquals(ApplicationConstants.FAIL, response);
		Mockito.verify(mockDmPatientDataApi, Mockito.times(0))
		       .updateEntirePatient(Mockito.any());

		/**
		 * Case 3 : Exception occurred when saving data.
		 */
		Mockito.reset(mockDmPatientDataApi);
		patient.setUserDetails(userDetails);
		Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString()))
		       .thenThrow(new RuntimeException());
		response = imanticService.createPrescription(prescription);
		assertNotNull(response);
		assertEquals(ApplicationConstants.FAIL, response);
		Mockito.verify(mockDmPatientDataApi, Mockito.times(0))
		       .updateEntirePatient(Mockito.any());
	}

	@Test
	public void testMedicationTaken() {
		/**
		 * Case 1 : DosageInfo returned null
		 */
		AdherenceDataPoints adherenceDataPoints = TestResponseGenerator.generateAdherenceDataPoints(
				Stream.of("Consumed")
				      .collect(Collectors.toList()));
		Boolean result = imanticService.medicationTaken(adherenceDataPoints, 1, "10");
		assertNotNull(result);
		assertFalse(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(0))
		       .postMedicationTakenEvent(Mockito.any());

		/**
		 * Case 2 : DosageInfo returned value
		 */
		Prescription prescription = TestResponseGenerator.generatePrescription();
		DosageInfo dosageInfo = TestResponseGenerator.generateDosageInfo(false);
		dosageInfo.setPrescription(prescription);
		Mockito.when(mockDosageInfoRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(dosageInfo));
		result = imanticService.medicationTaken(adherenceDataPoints, 1, "10");
		assertNotNull(result);
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postMedicationTakenEvent(Mockito.any());
	}

	@Test
	public void testDeletePatientByImanticId() {
		/**
		 * Case 1 : Patient data is not null and deleted successfully
		 */
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		DmPatientDataModel patientDataModel = TestResponseGenerator.generateDmPatientDataModel(patient);
		mockDmPatientDataApi = Mockito.mock(DmPatientDataApi.class);
		ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", mockDmPatientDataApi);
		Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString()))
		       .thenReturn(patientDataModel);
		Mockito.when(mockDmPatientDataApi.deletePatient(Mockito.anyString()))
		       .thenReturn(true);
		Boolean result = imanticService.deletePatientByImanticId("1");
		assertTrue(result);

		/**
		 * Case 2 : Patient data is null and not deleted successfully
		 */
		Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString()))
		       .thenReturn(null);
		result = imanticService.deletePatientByImanticId("1");
		assertFalse(result);

		/**
		 * Case 3 : Exception Occurred while deleting id.
		 */
		Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString()))
		       .thenThrow(new RuntimeException());
		result = imanticService.deletePatientByImanticId("1");
		assertFalse(result);
	}

	// @Test
	// public void testDeletePatientByPatientId() {
	//     /**
	//      * Case 1 : Successfully retrieved data and deleted.
	//      */
	//     Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
	//     DmPatientDataModel patientDataModel = TestResponseGenerator.generateDmPatientDataModel(patient);
	//     mockDmPatientDataApi = Mockito.mock(DmPatientDataApi.class);
	//     ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", mockDmPatientDataApi);
	//     Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString())).thenReturn(patientDataModel);
	//     Mockito.when(mockDmPatientDataApi.getPatientByName(Mockito.anyString())).thenReturn(Stream.of
	//     (patientDataModel).collect(Collectors.toList()));
	//     Mockito.when(mockDmPatientDataApi.deletePatient(Mockito.anyString())).thenReturn(true);
	//     Boolean result = imanticService.deletePatientBypatientId("1");
	//     assertTrue(result);

	//     /**
	//      * Case 1 : Not retrieved data and deletion is Failed.
	//      */
	//     Mockito.when(mockDmPatientDataApi.getPatientByName(Mockito.anyString())).thenReturn(null);
	//     result = imanticService.deletePatientBypatientId("1");
	//     assertFalse(result);
	// }

	@Test
	public void testFetchBiometricData() {
		PidDataTable dataTable = TestResponseGenerator.generatePidDataTable();
		List<PidDataTable> expectedPidDataTables = Stream.of(dataTable)
		                                                 .collect(Collectors.toList());
		Mockito.when(
				mockDmBiomidApi.RetrieveBiometricData(Mockito.anyString(), Mockito.any(), Mockito.any(),
				                                      Mockito.any()))
		       .thenReturn(expectedPidDataTables);
		List<PidDataTable> actualPidDataTables = imanticService.fetchBiometricData("1", new Date(), new Date(), null);
		assertTrue(CollectionUtils.isNotEmpty(actualPidDataTables));
		assertEquals(expectedPidDataTables, actualPidDataTables);
	}

	@Test
	public void testFetchPredictedBiometricData() {
		PidDataTable dataTable = TestResponseGenerator.generatePidDataTable();
		List<PidDataTable> expectedPidDataTables = Stream.of(dataTable)
		                                                 .collect(Collectors.toList());
		Mockito.when(mockDmBiomidApi.RetrievePredictedBiometricData(Mockito.anyString(), Mockito.any(), Mockito.any(),
		                                                            Mockito.any()))
		       .thenReturn(expectedPidDataTables);
		List<PidDataTable> actualPidDataTables =
				imanticService.fetchPredictedBiometricData("1", new Date(), new Date(), null);
		assertTrue(CollectionUtils.isNotEmpty(actualPidDataTables));
		assertEquals(expectedPidDataTables, actualPidDataTables);
	}

	@Test
	public void testFetchAnalyticsMap() {
		PidDataTable dataTable = TestResponseGenerator.generatePidDataTable();
		List<PidDataTable> expectedPidDataTables = Stream.of(dataTable)
		                                                 .collect(Collectors.toList());
		Mockito.when(
				mockDmBiomidApi.RetrieveAnalyticData(Mockito.anyString(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(expectedPidDataTables);
		ArrayList<String> analyticNameList = new ArrayList<>();
		analyticNameList.add(dataTable.getPid());
		Map<String, List<Map<String, String>>> resultMap =
				imanticService.fetchAnalyticsMap("1", new Date(), new Date(), analyticNameList);
		assertNotNull(resultMap);
		assertTrue(resultMap.containsKey(dataTable.getPid()));
		List<Map<String, String>> valueList = resultMap.get(dataTable.getPid());
		assertTrue(valueList.size() == 1);
		Map<String, String> valueMap = valueList.get(0);
		assertTrue(valueMap.containsKey("x"));
		assertTrue(valueMap.containsKey("y"));
	}

	@Test
	public void testFetchAnalyticsMapWithImanticId() {
		/**
		 * Case 1 : Returned response frm imantcs
		 */
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		DmPatientDataModel patientDataModel = TestResponseGenerator.generateDmPatientDataModel(patient);
		DmDiagnosticDataModel diagnosticDataModel = new DmDiagnosticDataModel();
		diagnosticDataModel.setDiagnosticResults(new String[]{"Problem 1"});
		patientDataModel.setDiagnosticMData(Stream.of(diagnosticDataModel)
		                                          .collect(Collectors.toList()));
		mockDmPatientDataApi = Mockito.mock(DmPatientDataApi.class);
		ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", mockDmPatientDataApi);
		Mockito.when(mockDmPatientDataApi.getPatientById(Mockito.anyString()))
		       .thenReturn(patientDataModel);
		Map<String, String> expectedResultsMap = new HashMap<>();
		expectedResultsMap.put("x", "Problem1");
		Mockito.when(mockDmBiomidApi.getAdherence(Mockito.anyString(), Mockito.any(), Mockito.any()))
		       .thenReturn(expectedResultsMap);
		Map<String, String> actualResultsMap = imanticService.fetchAnalyticsMap("1");
		assertNotNull(actualResultsMap);
		assertEquals(expectedResultsMap, actualResultsMap);
		assertTrue(actualResultsMap.containsKey("x"));

		/**
		 * case 2 : Null response from imantics/ unavailable
		 */
		ReflectionTestUtils.setField(imanticService, "dmPatientDataApi", null);
		actualResultsMap = imanticService.fetchAnalyticsMap("1");
		assertNotNull(actualResultsMap);
		assertFalse(actualResultsMap.containsKey("x"));
	}

	@Test
	public void testBiometricEvent_HeartRate() {
		/**
		 * Case 1 : Action => taken
		 */
		Mockito.when(mockDmBiomidApi.postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		HeartRate heartRate = TestResponseGenerator.generHeartRate();
		ArrayList<String> locations = new ArrayList<>();
		locations.add(TestUtil.generateLatitude()
		                      .toString());
		locations.add(TestUtil.generateLongitude()
		                      .toString());
		heartRate.setLocation(locations);
		Boolean result = imanticService.biometricEvent(heartRate, LocalDateTime.now()
		                                                                       .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 2 : Action => No_Action pulse
		 */
		Mockito.reset(mockDmBiomidApi);
		heartRate.setReadingValue(null);
		result = imanticService.biometricEvent(heartRate, LocalDateTime.now()
		                                                               .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 3 : Exception occurred
		 */
		Mockito.reset(mockDmBiomidApi);
		result = imanticService.biometricEvent(heartRate, null, null);
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(0))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void testBiometricEvent_Glucometer() {
		/**
		 * Case 1 : Action => taken
		 */
		Glucometer glucometer = TestResponseGenerator.generateGlucometer();
		Mockito.when(mockDmBiomidApi.postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		Boolean result = imanticService.biometricEvent(glucometer, LocalDateTime.now()
		                                                                        .plusMinutes(-10),
		                                               LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 2 : Action => No_Action pulse
		 */
		Mockito.reset(mockDmBiomidApi);
		glucometer.setReadingValue(null);
		result = imanticService.biometricEvent(glucometer, LocalDateTime.now()
		                                                                .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 3 : Exception occurred
		 */
		Mockito.reset(mockDmBiomidApi);
		result = imanticService.biometricEvent(glucometer, null, null);
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(0))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void testBiometricEvent_Scale() {
		/**
		 * Case 1 : Action => taken
		 */
		Scale scale = TestResponseGenerator.generateScale();
		Mockito.when(mockDmBiomidApi.postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		Boolean result = imanticService.biometricEvent(scale, LocalDateTime.now()
		                                                                   .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 2 : Action => No_Action pulse
		 */
		Mockito.reset(mockDmBiomidApi);
		scale.setReading(null);
		result = imanticService.biometricEvent(scale, LocalDateTime.now()
		                                                           .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 3 : Exception occurred
		 */
		Mockito.reset(mockDmBiomidApi);
		result = imanticService.biometricEvent(scale, null, null);
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(0))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void testBiometricEvent_BpMonitor() {
		/**
		 * Case 1 : Action => taken
		 */
		BpMonitor bpMonitor = TestResponseGenerator.generateBpMonitor();
		Mockito.when(mockDmBiomidApi.postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		Boolean result = imanticService.biometricEvent(bpMonitor, LocalDateTime.now()
		                                                                       .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 2 : Action => No_Action pulse
		 */
		Mockito.reset(mockDmBiomidApi);
		bpMonitor.setSystolicpressureValue(null);
		bpMonitor.setDiastolicpressureValue(null);
		result = imanticService.biometricEvent(bpMonitor, LocalDateTime.now()
		                                                               .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 3 : Exception occurred
		 */
		Mockito.reset(mockDmBiomidApi);
		result = imanticService.biometricEvent(bpMonitor, null, null);
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(0))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void testBiometricEvent_Steps() {
		/**
		 * Case 1 : Action => taken
		 */
		Steps steps = TestResponseGenerator.generateSteps();
		Mockito.when(mockDmBiomidApi.postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
		       .thenReturn(true);
		Boolean result = imanticService.biometricEvent(steps, LocalDateTime.now()
		                                                                   .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 2 : Action => No_Action pulse
		 */
		Mockito.reset(mockDmBiomidApi);
		steps.setStepCount(0);
		result = imanticService.biometricEvent(steps, LocalDateTime.now()
		                                                           .plusMinutes(-10), LocalDateTime.now());
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(1))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());

		/**
		 * Case 3 : Exception occurred
		 */
		Mockito.reset(mockDmBiomidApi);
		result = imanticService.biometricEvent(steps, null, null);
		assertTrue(result);
		Mockito.verify(mockDmBiomidApi, Mockito.times(0))
		       .postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
	}

	// @Test
	// public void testBiometricEvent_PulseOxiMeter() {
	//     /**
	//      * Case 1 : Action => taken
	//      */
	//     PulseOximeter pulseOximeter = TestResponseGenerator.generatePulseOximeter();
	//     Mockito.when(mockDmBiomidApi.postBiometricEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())
	//     ).thenReturn(true);
	//     Boolean result = imanticService.biometricEvent(pulseOximeter, LocalDateTime.now().plusMinutes(-10),
	//     LocalDateTime.now());
	//     assertTrue(result);
	//     Mockito.verify(mockDmBiomidApi, Mockito.times(1)).postBiometricEvent(Mockito.any(), Mockito.any(), Mockito
	//     .any(), Mockito.any());

	//     /**
	//      * Case 3 : Exception occurred
	//      */
	//     Mockito.reset(mockDmBiomidApi);
	//     result = imanticService.biometricEvent(pulseOximeter, null, null);
	//     assertTrue(result);
	//     Mockito.verify(mockDmBiomidApi, Mockito.times(0)).postBiometricEvent(Mockito.any(), Mockito.any(), Mockito
	//     .any(), Mockito.any());
	// }

}
