package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.Notification;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.impl.ZoomVideoAndVoiceServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.entity.Caregiver;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class TestVideoAndVoiceService extends BaseTestCase {

	@InjectMocks
	VideoAndVoiceService mockVideoAndVoiceService = new ZoomVideoAndVoiceServiceImpl();

	@Mock
	RestTemplate restTemplate;

	@Before
	public void setup() {
		ReflectionTestUtils.setField(mockVideoAndVoiceService, "restTemplate", restTemplate);
		ReflectionTestUtils.setField(mockVideoAndVoiceService, "API_SECRET", "test123");
	}

	@Test
	public void testGetZoomVideoCallSignature() throws Exception {
		/*
		 * If Device type is Web.
		 */
		JwtUserDetails jwtUserDetails = TestResponseGenerator.generateJwtUserDetails(
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT), true,
				ApplicationConstants.PATIENT);
		JSONObject createUser = new JSONObject();
		createUser.put("id", "12325");
		ResponseEntity<String> myEntity = new ResponseEntity<String>(createUser.toString(), HttpStatus.CREATED);
		Mockito.when(restTemplate.exchange(
				ArgumentMatchers.anyString(),
				ArgumentMatchers.any(HttpMethod.class),
				ArgumentMatchers.any(),
				ArgumentMatchers.<Class<String>>any()))
		       .thenReturn(myEntity);
		JSONObject zoomMettingResponse = new JSONObject();
		zoomMettingResponse.put("id", "12325");
		zoomMettingResponse.put("start_url", "http://test?zak=xyz1223");
		zoomMettingResponse.put("join_url", "http://test?zak=xyz1223");
		zoomMettingResponse.put("pstn_password", "12234");
		zoomMettingResponse.put("password", "1234");
		zoomMettingResponse.put("h323_password", "1234");
		zoomMettingResponse.put("encrypted_password", "1234");
		zoomMettingResponse.put("host_id", "abc1234");
		Mockito.when(restTemplate.postForObject(
				ArgumentMatchers.anyString(),
				ArgumentMatchers.any(HttpEntity.class),
				ArgumentMatchers.<Class<String>>any(),
				ArgumentMatchers.any(MultiValueMap.class)
		))
		       .thenReturn(zoomMettingResponse.toString());
		VidoeCallSignatureModel vidoeCallSignatureModel =
				mockVideoAndVoiceService.getZoomVideoCallSignature(1, jwtUserDetails);
		assertNotNull(vidoeCallSignatureModel);

		/*
		 * If Device type is IOS.
		 */
		jwtUserDetails.setTypeOfDevice(ApplicationConstants.IOS);
		VidoeCallSignatureModel vidoeCallSignatureModelIOS =
				mockVideoAndVoiceService.getZoomVideoCallSignature(1, jwtUserDetails);
		assertNotNull(vidoeCallSignatureModelIOS);
	}

	@Mock
	UserDetailsService mockUserDetailsService;

	@Mock
	NotificationService mockNotificationService;

	@Mock
	CaregiverService mockCaregiverService;

	@Test
	public void testSendVideoCallNotificationToReciverByUserId() throws Exception  {
		VidoeCallSignatureModel vidoeCallSignatureModel = new VidoeCallSignatureModel();
		vidoeCallSignatureModel.setRecevierUserID("1");
		vidoeCallSignatureModel.setMeetingNumber("123444");
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.CAREGIVER);
		Mockito.when(mockUserDetailsService.fetchDecryptedUserDetailsById(1l))
		       .thenReturn(userDetails);
		Mockito.when(mockNotificationService.sendMobileVidoeCallNotification(Mockito.anyString(), Mockito.anyString(),
		                                                                     Mockito.anyString(), Mockito.anyString(),
		                                                                     Mockito.anyString(), Mockito.any()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		Notification notification = TestResponseGenerator.generateNotification();
		Mockito.when(mockNotificationService.addWebNotification(Mockito.any()))
		       .thenReturn(notification);
		Caregiver caregiver =
				TestResponseGenerator.generateCaregiver("Male", "Asian", ApplicationConstants.CAREGIVER, true);
		Mockito.when(mockCaregiverService.fetchCaregiverByUserId(Mockito.anyLong()))
		       .thenReturn(caregiver);
		String responseString =
				mockVideoAndVoiceService.sendVideoCallNotificationToReciverByUserId(vidoeCallSignatureModel, 1l);
		assertNotNull(responseString);
		assertEquals(responseString, ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION);
	}

	@Test
	public void testSendVideoCallNotificationToReciverByUserIdForPatient() {
		VidoeCallSignatureModel vidoeCallSignatureModel = new VidoeCallSignatureModel();
		vidoeCallSignatureModel.setRecevierUserID("1");
		vidoeCallSignatureModel.setMeetingNumber("123444");
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "Asian", ApplicationConstants.PATIENT);
		Mockito.when(mockUserDetailsService.fetchDecryptedUserDetailsById(1l))
		       .thenReturn(userDetails);
		Mockito.when(mockNotificationService.sendMobileVidoeCallNotification(Mockito.anyString(), Mockito.anyString(),
		                                                                     Mockito.anyString(), Mockito.anyString(),
		                                                                     Mockito.anyString(), Mockito.any()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		Notification notification = TestResponseGenerator.generateNotification();
		Mockito.when(mockNotificationService.addWebNotification(Mockito.any()))
		       .thenReturn(notification);
		String responseString =
				mockVideoAndVoiceService.sendVideoCallNotificationToReciverByUserId(vidoeCallSignatureModel, 1l);
		assertNotNull(responseString);
		assertEquals(responseString, ApplicationConstants.VIDEO_CALL_NOTIIFCATION_MESSAGE_SENT_NOTIFICATION);
	}
}
