package com.sg.dayamed.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Role;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.UserPreferenceModel;
import com.sg.dayamed.repository.PharmacistRepository;
import com.sg.dayamed.repository.RoleRepository;
import com.sg.dayamed.service.impl.PharmaServiceImpl;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import com.sg.dayamed.entity.ConsumerGoods;
import com.sg.dayamed.entity.Device;
import com.sg.dayamed.entity.Disease;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.repository.ConsumerGoodsRepository;
import com.sg.dayamed.repository.DeviceRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.MedicineRepository;
import com.sg.dayamed.util.ApplicationConstants;

/**
 * Created by Naresh Kamisetti on 03/July/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestPharmaService extends BaseTestCase {

	@InjectMocks
	PharmaService mockPharmaService = new PharmaServiceImpl();

	@Mock
	PharmacistRepository mockPharmacistRepository;

	@Mock
	private MedicineRepository mockMedicineRepository;

	@Mock
	private ConsumerGoodsRepository consumerGoodsRepository;

	@Mock
	private DeviceRepository deviceRepository;

	@Mock
	private DiseaseRepository diseaseRepository;

	@Mock
	private Utility mockUtility;

	@Mock
	private PasswordEncryption mockPasswordEncryption;

	@Mock
	UserPreferenceService mockUserPreferenceService;

	@Mock
	UserDetailsService mockUserDetailsService;

	@Mock
	PharmacistService mockPharmacistService;

	@Mock
	RoleRepository mockRoleRepository;

	@Mock
	SendResetPwdNotification mockSendPwdResetNtf;


	@Test
	public void testAddPharmacist() {
		Pharmacist pharmacist = TestResponseGenerator.generatePharmacist("Male", "Asian",
		                                                                 UserRoleEnum.PHARMACIST.getRole());
		Mockito.when(mockUtility.convertPharmasistModelToEntity(Mockito.any(PharmacistModel.class)))
		       .thenReturn(pharmacist);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
		PharmacistModel pharmacistModel = TestResponseGenerator.generatePharmacistModel();
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		Mockito.when(mockPharmacistRepository.save(Mockito.any(Pharmacist.class)))
		       .thenReturn(pharmacist);
		pharmacistModel.getUserDetails()
		               .setId(0);
		Role role = TestResponseGenerator.generateRole(ApplicationConstants.PHARMACIST);
		Mockito.when(mockRoleRepository.findByName(Mockito.anyString()))
		       .thenReturn(role);
		Mockito.when(mockUtility.convertPharmacistEntityToModel(Mockito.any(Pharmacist.class)))
		       .thenReturn(pharmacistModel);
		PharmacistModel actualPharmacistModel = mockPharmaService.addPharmacist(pharmacistModel);
		assertEquals(actualPharmacistModel.getId(), pharmacistModel.getId());
	}

	@Test
	public void testfetchPharmasists() {
		List<Pharmacist> listPharmacist = new ArrayList<Pharmacist>();
		listPharmacist
				.add(TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole()));
		Mockito.when(mockPharmacistRepository.findAll())
		       .thenReturn(listPharmacist);
		Mockito.when(mockUtility.convertPharmacistEntityToModel(Mockito.any(Pharmacist.class)))
		       .thenReturn(TestResponseGenerator.generatePharmacistModel());
		List<PharmacistModel> pharmacistModelList = mockPharmaService.fetchPharmasists();
		assertNotNull(pharmacistModelList);
	}

	@Test
	public void testFetchMedicines() {
		Mockito.when(mockMedicineRepository.findAll())
		       .thenReturn(Arrays.asList(TestResponseGenerator.generateMedicine()));
		List<Medicine> medicinelist = mockPharmaService.fetchMedicines();
		assertNotNull(medicinelist);
	}

	@Test
	public void testFetchConsumergoods() {
		Mockito.when(consumerGoodsRepository.findAll())
		       .thenReturn(Arrays.asList(TestResponseGenerator.generateConsumerGoods()));
		List<ConsumerGoods> listConsumerGoods = mockPharmaService.fetchConsumergoods();
		assertNotNull(listConsumerGoods);
	}

	@Test
	public void testFetchAllDevices() {
		Mockito.when(deviceRepository.findAll())
		       .thenReturn(Arrays.asList(TestResponseGenerator.generateDevice()));
		List<Device> listDevice = mockPharmaService.fetchAllDevices();
		assertNotNull(listDevice);
	}

	@Test
	public void testFetchDiseases() {
		Mockito.when(diseaseRepository.findAll())
		       .thenReturn(Arrays.asList(TestResponseGenerator.generateDisease()));
		List<Disease> listDisease = mockPharmaService.fetchDiseases();
		assertNotNull(listDisease);
	}

	@Test
	public void testFindByUserDetails_id() {
		Mockito.when(mockPharmacistRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(
				       TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole()));
		Pharmacist pharmacist = mockPharmaService.findByUserDetails_id(3l);
		assertNotNull(pharmacist);
	}

	@Test
	public void testDeletePharmacist() {
		/*
		 * Case : Fail
		 */
		Optional<Pharmacist> optionalPharmacistEmpty = Optional.empty();
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalPharmacistEmpty);
		String statusNotExist = mockPharmaService.deletePharmacist(3l);
		assertNotNull(statusNotExist);
		assertEquals(statusNotExist, "notExist");
		/*
		 * Case : success
		 */
		Optional<Pharmacist> optionalPharmacist = Optional
				.of(TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole()));
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalPharmacist);
		String status = mockPharmaService.deletePharmacist(3l);
		assertNotNull(status);
		assertEquals(status, ApplicationConstants.SUCCESS);
	}

	@Test
	public void testFetchPharmacistById() throws Exception {
		Optional<Pharmacist> optionalPharmacist = Optional
				.of(TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole()));
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalPharmacist);
		PharmacistModel pharmacistModel = TestResponseGenerator.generatePharmacistModel();
		Mockito.when(mockUtility.convertPharmacistEntityToModel(Mockito.any(Pharmacist.class)))
		       .thenReturn(pharmacistModel);
		PharmacistModel resPharmacistModel = mockPharmaService.fetchPharmacistById(3l);
		assertNotNull(resPharmacistModel);
		assertEquals(resPharmacistModel, pharmacistModel);
	}

	@Test
	public void testAddPharmacistValidation() throws Exception {
		/*
		 * Case 1: if mobile number is empty. Fail case.
		 */
		try {
			PharmacistModel pharmacistModel = TestResponseGenerator.generatePharmacistModel();
			pharmacistModel.getUserDetails()
			               .setMobileNumber(null);
			mockPharmaService.addPharmacistValidation(pharmacistModel, null);
		} catch (DataValidationException e) {
			assertTrue(true);
		}
		/*
		 * Case 2 : Fail case, Email conflict
		 */
		try {
			PharmacistModel pharmacistModel = TestResponseGenerator.generatePharmacistModel();
			pharmacistModel.setId(0);
			pharmacistModel.getUserDetails()
			               .setImageUrl("test");
			Mockito.when(mockUtility.getLastBitFromUrl(Mockito.anyString()))
			       .thenReturn("testUrl");
			Mockito.when(mockUtility.uploadProfilepic(Mockito.any(), Mockito.anyString(), Mockito.anyString()))
			       .thenReturn("testPath");
			InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
			MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
			                                                       inputFile);
			Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.any()))
			       .thenReturn(TestResponseGenerator.generateUserDetailsModel("Male", "Asian",
			                                                                  ApplicationConstants.PHARMACIST));
			mockPharmaService.addPharmacistValidation(pharmacistModel, profileImage);
		} catch (DataValidationException e) {
			assertTrue(true);
		}
		/*
		 * Case 2 : Success case.
		 */
		PharmacistModel pharmacistModel = TestResponseGenerator.generatePharmacistModel();
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.any()))
		       .thenReturn(null);
		Pharmacist pharmacist = TestResponseGenerator.generatePharmacist("Male", "Asian",
		                                                                 UserRoleEnum.PHARMACIST.getRole());
		Mockito.when(mockUtility.convertPharmasistModelToEntity(Mockito.any(PharmacistModel.class)))
		       .thenReturn(pharmacist);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("ency pwd");
		mockUserPreferenceService.addUserPreference(Mockito.anyLong(), Mockito.any(UserPreferenceModel.class));
		Mockito.when(mockPharmacistRepository.save(Mockito.any(Pharmacist.class)))
		       .thenReturn(pharmacist);
		PharmacistModel resPharmacistModel = mockPharmaService.addPharmacistValidation(pharmacistModel, profileImage);
		assertNotNull(resPharmacistModel);
		assertEquals(resPharmacistModel, pharmacistModel);
	}

	@Test
	public void testDeletePharmacistValidation() throws Exception {
		/*
		 * Case 1 : Fail, pharmacist id is null
		 */
		try {
			mockPharmaService.deletePharmacistValidation(null);
		} catch (DataValidationException e) {
			assertTrue(true);
		}
		/*
		 * Case 1 : Fail, PatientModel list is null
		 */
		try {
			Mockito.when(mockPharmacistService.fetchPatientsByPharmacistId(Mockito.anyLong()))
			       .thenReturn(Stream.of(TestResponseGenerator.generatePatientModel())
			                         .collect(
					                         Collectors.toList()));
			mockPharmaService.deletePharmacistValidation(1l);
		} catch (DataValidationException e) {
			assertTrue(true);
		}
		/*
		 * Case 1 : success
		 */
		Mockito.when(mockPharmacistService.fetchPatientsByPharmacistId(Mockito.anyLong()))
		       .thenReturn(null);
		Optional<Pharmacist> optionalPharmacist = Optional
				.of(TestResponseGenerator.generatePharmacist("Male", "Asian", UserRoleEnum.PHARMACIST.getRole()));
		Mockito.when(mockPharmacistRepository.findById(Mockito.anyLong()))
		       .thenReturn(optionalPharmacist);
		String result = mockPharmaService.deletePharmacistValidation(1l);
		assertNotNull(result);
		assertEquals(result, ApplicationConstants.SUCCESS);
	}
}
