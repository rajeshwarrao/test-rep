package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.encryption.passwordencryption.PasswordEncryption;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.Caregiver;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Pharmacist;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.Provider;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.exceptions.ApplicationException;
import com.sg.dayamed.exceptions.DataValidationException;
import com.sg.dayamed.helper.PatientHelper;
import com.sg.dayamed.helper.pojo.PatientAdherencePojo;
import com.sg.dayamed.helper.pojo.PatientAssociatedActorsPojo;
import com.sg.dayamed.pojo.CaregiverModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.PharmacistModel;
import com.sg.dayamed.pojo.PrescriptionModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.repository.ConsumerGoodsInfoRepository;
import com.sg.dayamed.repository.DeviceInfoRepository;
import com.sg.dayamed.repository.DeviceNotificationRepository;
import com.sg.dayamed.repository.DiseaseInfoRepository;
import com.sg.dayamed.repository.DiseaseRepository;
import com.sg.dayamed.repository.DosageDeviceRepository;
import com.sg.dayamed.repository.DosageInfoRepository;
import com.sg.dayamed.repository.DosageNotificationRepository;
import com.sg.dayamed.repository.NotificationScheduleRepository;
import com.sg.dayamed.repository.PatientRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.repository.PrescriptionScheduleRepository;
import com.sg.dayamed.repository.ProviderRepository;
import com.sg.dayamed.security.jwtsecurity.model.JwtUserDetails;
import com.sg.dayamed.service.impl.PatientInfoServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.EmailAndSmsUtility;
import com.sg.dayamed.util.MessageNotificationConstants;
import com.sg.dayamed.util.SendResetPwdNotification;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
public class TestPatientInfoService extends BaseTestCase {

	@InjectMocks
	private PatientInfoService patientService = new PatientInfoServiceImpl();

	@Mock
	private PatientRepository mockPatientRepository;

	@Mock
	private ProviderRepository mockProviderRepository;

	@Mock
	private ImanticService mockImanticService;

	@Mock
	private PasswordEncryption mockPasswordEncryption;

	@Mock
	private SendResetPwdNotification mockSendResetPwdNotification;

	@Mock
	private NotificationService mockNotificationService;

	@Mock
	private EmailAndSmsUtility mockEmailAndSmsUtility;

	@Mock
	private DiseaseRepository mockDiseaseRepository;

	@Mock
	private DosageInfoRepository mockDosageInfoRepository;

	@Mock
	private DeviceInfoRepository mockDeviceInfoRepository;

	@Mock
	private DiseaseInfoRepository mockDiseaseInfoRepository;

	@Mock
	private ConsumerGoodsInfoRepository mockConsumerGoodsInfoRepository;

	@Mock
	private DosageNotificationRepository mockDosageNotificationRepository;

	@Mock
	private DeviceNotificationRepository mockDeviceNotificationRepository;

	@Mock
	private PrescriptionRepository mockPrescriptionRepository;

	@Mock
	private PrescriptionScheduleService mockPrescriptionScheduleService;

	@Mock
	private NotificationScheduleService mockNotificationScheduleService;

	@Mock
	private PrescriptionScheduleRepository mockPrescriptionScheduleRepository;

	@Mock
	private NotificationScheduleRepository mockNotificationScheduleRepository;

	@Mock
	private DosageDeviceRepository mockDosageDeviceRepository;

	@Mock
	private Utility mockUtility;

	@Mock
	MessageNotificationConstants mockMessageNotificationConstants;

	@Mock
	ProviderService mockProviderService;

	@Mock
	HttpServletRequest mockHttpRequest;

	@Mock
	Authentication mockAuthentication;

	@Mock
	PHIDataEncryptionService mockPHIDataEncryptionService;

	@Mock
	DateUtility mockDateUtility;

	@Mock
	AdherenceService mockAdherenceService;

	@Mock
	PatientHelper mockPatientHelper;

	@Mock
	PharmacistService mockPharmacistService;

	@Mock
	CaregiverService mockCaregiverService;

	@Mock
	UserDetailsService mockUserDetailsService;

	private Patient patient;

	UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");

	@Before
	public void setUp() {
		patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn(UUID.randomUUID()
		                       .toString());
		Mockito.when(mockEmailAndSmsUtility.sendEmail(Mockito.anyString(), Mockito.anyString(), Mockito.anyList()))
		       .thenReturn(true);
		Mockito.when(mockEmailAndSmsUtility.sendSms(Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(true);
		Mockito.doNothing()
		       .when(mockSendResetPwdNotification)
		       .sendPwdResetNotificationWhenRegst(Mockito.any(),
		                                          Mockito.anyString());
		Mockito.when(mockNotificationService.sendMobileNotification(Mockito.any(), Mockito.anyString(),
		                                                            Mockito.anyString(), Mockito.any()))
		       .thenReturn("Success");
	}

	@Test
	public void testDeletePatientByProcedure() {
		/**
		 * Case 1 : Success in deletion of Imantic user.
		 */
		Mockito.when(mockPatientRepository.deletePatient(Mockito.anyLong()))
		       .thenReturn("success");
		Mockito.when(mockImanticService.deletePatientByImanticId(Mockito.anyString()))
		       .thenReturn(true);
		String result = patientService.deletePatientByProcedure(1);
		assertNotNull(result);
		assertEquals("success", result);
	}

	@Test
	public void testFetchPatientsByProviderId() throws Exception {
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findByProviders_idAndStatus(Mockito.anyLong(), Mockito.anyInt()))
		       .thenReturn(patients);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		CaregiverModel caregiverModel = generateRequestObject("CaregiverModel-1", CaregiverModel.class,
		                                                      USER_DETAILS_FILE);
		ProviderModel providerModel = generateRequestObject("ProviderModel-1", ProviderModel.class, USER_DETAILS_FILE);
		PharmacistModel pharmacistModel = generateRequestObject("PharmacistModel-1", PharmacistModel.class,
		                                                        USER_DETAILS_FILE);
		patientModel.setPharmacists(Stream.of(pharmacistModel)
		                                  .collect(Collectors.toSet()));
		patientModel.setProviders(Stream.of(providerModel)
		                                .collect(Collectors.toSet()));
		patientModel.setCaregivers(Stream.of(caregiverModel)
		                                 .collect(Collectors.toSet()));
		patientModel.setCaregiverList(Stream.of(caregiverModel)
		                                    .collect(Collectors.toSet()));
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel = generateRequestObject("PrescriptionModel-1", PrescriptionModel.class,
		                                                            PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		UserDetailsModel userDetailsModel = generateRequestObject("userDetails-Caregiver1", UserDetailsModel.class,
		                                                          USER_DETAILS_FILE);
		List<PatientModel> patientModels = patientService.fetchPatientsByProviderId(1);
		assertTrue(CollectionUtils.isNotEmpty(patientModels));
		assertEquals(patientModels, Stream.of(patientModel)
		                                  .collect(Collectors.toList()));
	}

	@Test
	public void testFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Patient actualPatient = patientService.fetchPatientById(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id does not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.fetchPatientById(1);
		assertNull(actualPatient);
	}

	@Test
	public void testDirectFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		Patient actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNull(actualPatient);
	}

	@Test
	public void testDecryptPatientProviderFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		patient.setCaregivers(Stream.of(caregiver)
		                            .collect(Collectors.toSet()));
		patient.setProviders(Stream.of(provider)
		                           .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Patient actualPatient = patientService.decryptPatientProviderfetchPatientById(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.decryptPatientProviderfetchPatientById(1);
		assertNull(actualPatient);
	}

	@Test
	public void testNoDecryptCargiverFetchPatientById() {
		/**
		 * Case 1 : Id exists
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		patient.setCaregivers(Stream.of(caregiver)
		                            .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Patient actualPatient = patientService.noDecrypCargiverfetchPatientById(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.noDecrypCargiverfetchPatientById(1);
		assertNull(actualPatient);
	}

	@Test
	public void testDirectFetchPatientByPatientId() {
		/**
		 * Case 1 : Id exists
		 */
		Patient actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Id not exists
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.empty());
		actualPatient = patientService.directFetchPatientByPatientId(1);
		assertNull(actualPatient);
	}

	@Test
	public void testFindByUserDetails_id() {
		Mockito.when(mockPatientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		Patient actualPatient = patientService.findByUserDetails_id(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);
	}

	@Test
	public void testFetchPatientsByCaregiverId() throws Exception {
		Mockito.when(mockPatientRepository.findByCaregivers_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(patient)
		                         .collect(Collectors.toList()));
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		List<PatientModel> actualPatientModels = patientService.fetchPatientsByCaregiverId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualPatientModels));
		assertEquals(Stream.of(patientModel)
		                   .collect(Collectors.toList()), actualPatientModels);
	}

	@Test
	public void testDirectFetchPatientsByCaregiverId() {
		Mockito.when(mockPatientRepository.findByCaregivers_id(Mockito.anyLong()))
		       .thenReturn(Stream.of(patient)
		                         .collect(Collectors.toList()));
		List<Patient> actualPatients = patientService.directFetchPatientsByCaregiverId(1);
		assertTrue(CollectionUtils.isNotEmpty(actualPatients));
		assertEquals(Stream.of(patient)
		                   .collect(Collectors.toList()), actualPatients);
	}

	@Test
	public void testUpdatePatientAlone() throws Exception {
		Mockito.when(mockPatientRepository.save(Mockito.any()))
		       .thenReturn(patient);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		UserDetailsModel userDetailsModel = generateRequestObject("userDetails-Caregiver1", UserDetailsModel.class,
		                                                          USER_DETAILS_FILE);
		PatientModel actualPatientModel = patientService.updatePatientAlone(patient);
		assertNotNull(actualPatientModel);
		assertEquals(patientModel, actualPatientModel);
	}

	@Test
	public void testFetchByDeviceId() {
		Mockito.when(mockPatientRepository.findByDeviceId(Mockito.any()))
		       .thenReturn(patient);
		Patient actualPatient = patientService.fetchByDeviceId("JUnit");
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);
	}

	@Test
	public void testFetchAdherenceByPatientId() {
		/**
		 * Case 1 : Null response from repository
		 */
		Mockito.when(mockPatientRepository.findAdherenceById(Mockito.anyLong()))
		       .thenReturn(null);
		PatientAdherencePojo actualPojo = patientService.fetchAdherenceByPatientId(1);
		assertNull(actualPojo);

		/**
		 * Case 2 : non null response from repository
		 */
		PatientAdherencePojo expectedPojo = TestResponseGenerator.generatePatientAdherencePojo();
		Mockito.when(mockPatientRepository.findAdherenceById(Mockito.anyLong()))
		       .thenReturn(expectedPojo);
		actualPojo = patientService.fetchAdherenceByPatientId(1);
		assertNotNull(actualPojo);
		assertEquals(expectedPojo, actualPojo);
	}

	@Test
	public void testFetchPatientByUserId() throws Exception {
		/**
		 * Case 1 : Non null result
		 */
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		patient.setCaregivers(Stream.of(caregiver)
		                            .collect(Collectors.toSet()));
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", "Provider");
		patient.setProviders(Stream.of(provider)
		                           .collect(Collectors.toSet()));
		Pharmacist pharmacist = TestResponseGenerator.generatePharmacist("Male", "Asian", "Pharmacist");
		patient.setPharmacists(Stream.of(pharmacist)
		                             .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(patient);
		UserDetailsModel userDetailsModel = generateRequestObject("userDetails-Caregiver1", UserDetailsModel.class,
		                                                          USER_DETAILS_FILE);
		Patient actualPatient = patientService.fetchPatientByUserId(1);
		assertNotNull(actualPatient);
		assertEquals(patient, actualPatient);

		/**
		 * Case 2 : Null result
		 */
		Mockito.when(mockPatientRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(null);
		actualPatient = patientService.fetchPatientByUserId(1);
		assertNull(actualPatient);
	}

	@Test
	public void testAddPatient() {
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Map<String, String> imanticMap = new HashMap<String, String>();
		imanticMap.put("imanticid", "12345");
		Map<String, String> analyticMap = new HashMap<String, String>();
		analyticMap.put("emoji", "happy");
		Mockito.when(mockImanticService.createPatientInImantics(Mockito.any(Patient.class)))
		       .thenReturn(imanticMap);
		Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString()))
		       .thenReturn(analyticMap);
		Mockito.when(mockPasswordEncryption.encrypt(Mockito.anyString()))
		       .thenReturn("abcd");
		Mockito.when(mockPatientRepository.save(Mockito.any(Patient.class)))
		       .thenReturn(patient);
		mockSendResetPwdNotification.sendPwdResetNotificationWhenRegst(Mockito.any(UserDetails.class),
		                                                               Mockito.anyString());
		mockNotificationService.sendMobileNotification(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
		                                               Mockito.anyString());
		PatientModel patienTModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(patient))
		       .thenReturn(patienTModel);
		Mockito.when(mockUtility.getUserPreferedLang(Mockito.anyLong()))
		       .thenReturn("abcd");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Set<Caregiver> CaregiverSet = new HashSet<Caregiver>();
		CaregiverSet.add(caregiver);
		patient.setCaregivers(CaregiverSet);
		PatientModel patientModel = patientService.addPatient(patient);
		assertNotNull(patientModel);
	}

	@Test
	public void updatePatient() {
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", "Patient");
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Set<Caregiver> CaregiverSet = new HashSet<Caregiver>();
		CaregiverSet.add(caregiver);
		patient.setCaregivers(CaregiverSet);
		PatientModel patienTModel = TestResponseGenerator.generatePatientModel();
		Map<String, String> analyticMap = new HashMap<String, String>();
		analyticMap.put("emoji", "happy");
		Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString()))
		       .thenReturn(analyticMap);

		// pHIDataDBEncryption.encryptUserDetailsEntitiy(patient.getUserDetails(),
		// pHIDataDBEncryption.getDBEncryKey())
		Mockito.when(mockPatientRepository.save(Mockito.any(Patient.class)))
		       .thenReturn(patient);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(patient))
		       .thenReturn(patienTModel);
		Mockito.when(mockUtility.getUserPreferedLang(Mockito.anyLong()))
		       .thenReturn("abcd");

		Mockito.when(mockMessageNotificationConstants.getUserPreferedLangTranslatedMsg(Mockito.any(),
		                                                                               Mockito.anyString(), Mockito.anyString(), Mockito.any(Object[].class)))
		       .thenReturn("abcd");
		PatientModel actualPatientModel = patientService.updatePatient(patient);
		assertNotNull(actualPatientModel);
	}

	@Test
	public void testFetchAllPatients() {
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findAll())
		       .thenReturn(patients);
		List<Patient> listPatients = patientService.fetchAllPatients();
		assertNotNull(listPatients);
		assertEquals(listPatients, patients);
	}

	@Test
	public void testFetchEncryptPatientsByProviderId() {
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findByProviders_id(Mockito.anyLong()))
		       .thenReturn(patients);
		List<Patient> listPatients = patientService.fetchEncryptPatientsByProviderId(1);
		assertNotNull(listPatients);
		assertEquals(listPatients, patients);
	}

	@Test
	public void testFetchPatietnsByProviderId() throws Exception {
		/*
		 * case 1 : if provider is null through exception
		 */
		try {
			Mockito.when(mockProviderService.fetchProviderById(Mockito.anyLong()))
			       .thenReturn(null);
		} catch (Exception e) {
			assertTrue(e instanceof DataValidationException);
			DataValidationException exception = (DataValidationException) e;
			assertEquals("Application Error", exception.getFieldName());
		}
		/**
		 * case 2: success
		 */

		ProviderModel providerModel = TestResponseGenerator.generateProviderModel();
		Mockito.when(mockProviderService.fetchProviderById(Mockito.anyLong()))
		       .thenReturn(providerModel);
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findByProviders_idAndStatus(Mockito.anyLong(), Mockito.anyInt()))
		       .thenReturn(patients);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		CaregiverModel caregiverModel = generateRequestObject("CaregiverModel-1", CaregiverModel.class,
		                                                      USER_DETAILS_FILE);
		PharmacistModel pharmacistModel = generateRequestObject("PharmacistModel-1", PharmacistModel.class,
		                                                        USER_DETAILS_FILE);
		patientModel.setPharmacists(Stream.of(pharmacistModel)
		                                  .collect(Collectors.toSet()));
		patientModel.setProviders(Stream.of(providerModel)
		                                .collect(Collectors.toSet()));
		patientModel.setCaregivers(Stream.of(caregiverModel)
		                                 .collect(Collectors.toSet()));
		patientModel.setCaregiverList(Stream.of(caregiverModel)
		                                    .collect(Collectors.toSet()));
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel = generateRequestObject("PrescriptionModel-1", PrescriptionModel.class,
		                                                            PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		List<PatientModel> listPatientModel = Stream.of(patientModel)
		                                            .collect(Collectors.toList());
		Mockito.when(mockPHIDataEncryptionService.encryptPatientList(Mockito.any(), Mockito.anyString()))
		       .thenReturn(listPatientModel);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(mockHttpRequest, mockAuthentication))
		       .thenReturn("keys");
		List<PatientModel> listPatientModels = patientService.fetchPatietnsByProviderId(mockHttpRequest, "1",
		                                                                                mockAuthentication);
		assertNotNull(listPatientModels);
		assertEquals(listPatientModels, listPatientModel);
	}

	@Test
	public void testFetchMissedAdherencePatietnsByProviderId() throws Exception {
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian",
		                                                                    ApplicationConstants.PATIENT);
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(
				       TestResponseGenerator.generateJwtUserDetails(userDetails, true, ApplicationConstants.PATIENT));
		/*
		 * case 1: provider is null
		 */
		try {
			Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
			       .thenReturn(null);
			patientService.fetchMissedAdherencePatietnsByProviderId(mockAuthentication, mockHttpRequest);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 2: success
		 */
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", ApplicationConstants.PROVIDER);
		Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(provider);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		List<PatientModel> listPatientModel = Stream.of(patientModel)
		                                            .collect(Collectors.toList());
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findByProviders_idAndStatus(Mockito.anyLong(), Mockito.anyInt()))
		       .thenReturn(patients);
		CaregiverModel caregiverModel = generateRequestObject("CaregiverModel-1", CaregiverModel.class,
		                                                      USER_DETAILS_FILE);
		ProviderModel providerModel = generateRequestObject("ProviderModel-1", ProviderModel.class, USER_DETAILS_FILE);
		PharmacistModel pharmacistModel = generateRequestObject("PharmacistModel-1", PharmacistModel.class,
		                                                        USER_DETAILS_FILE);
		patientModel.setPharmacists(Stream.of(pharmacistModel)
		                                  .collect(Collectors.toSet()));
		patientModel.setProviders(Stream.of(providerModel)
		                                .collect(Collectors.toSet()));
		patientModel.setCaregivers(Stream.of(caregiverModel)
		                                 .collect(Collectors.toSet()));
		patientModel.setCaregiverList(Stream.of(caregiverModel)
		                                    .collect(Collectors.toSet()));
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel = generateRequestObject("PrescriptionModel-1", PrescriptionModel.class,
		                                                            PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		java.time.LocalDateTime localDateTime = java.time.LocalDateTime.now();
		Mockito.when(mockDateUtility.convertdatetimeStringToLocalDateTime(Mockito.anyString()))
		       .thenReturn(localDateTime);
		List<AdherenceDataPoints> adherenceList = Stream
				.of(TestResponseGenerator.generateAdherenceDataPoints(
						Stream.of("skipped", "autodelayed", "consumed", "delayed")
						      .collect(Collectors.toList())))
				.collect(Collectors.toList());
		Mockito.when(mockAdherenceService.findByPrescribedTimeAndBetweenTimes(Mockito.any(), Mockito.any(),
		                                                                      Mockito.anyLong()))
		       .thenReturn(adherenceList);
		Mockito.when(mockPHIDataEncryptionService.encryptPatientList(Mockito.any(), Mockito.anyString()))
		       .thenReturn(listPatientModel);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(mockHttpRequest, mockAuthentication))
		       .thenReturn("keys");
		List<PatientModel> patientModellist = patientService
				.fetchMissedAdherencePatietnsByProviderId(mockAuthentication, mockHttpRequest);
		assertNotNull(patientModellist);
		assertEquals(patientModellist, listPatientModel);
	}

	@Test
	public void testFetchPatietnsByProviderIdFromCookie() throws Exception {
		/*
		 * case 1 : if provider null
		 */
		try {
			Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", ApplicationConstants.PROVIDER);
			Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
			       .thenReturn(provider);
			patientService.fetchPatietnsByProviderIdFromCookie(mockAuthentication, mockHttpRequest);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 2 success
		 */
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", ApplicationConstants.PROVIDER);
		Mockito.when(mockProviderRepository.findByUserDetails_id(Mockito.anyLong()))
		       .thenReturn(provider);
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findByProviders_idAndStatus(Mockito.anyLong(), Mockito.anyInt()))
		       .thenReturn(patients);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		CaregiverModel caregiverModel = generateRequestObject("CaregiverModel-1", CaregiverModel.class,
		                                                      USER_DETAILS_FILE);
		ProviderModel providerModel = generateRequestObject("ProviderModel-1", ProviderModel.class, USER_DETAILS_FILE);
		PharmacistModel pharmacistModel = generateRequestObject("PharmacistModel-1", PharmacistModel.class,
		                                                        USER_DETAILS_FILE);
		patientModel.setPharmacists(Stream.of(pharmacistModel)
		                                  .collect(Collectors.toSet()));
		patientModel.setProviders(Stream.of(providerModel)
		                                .collect(Collectors.toSet()));
		patientModel.setCaregivers(Stream.of(caregiverModel)
		                                 .collect(Collectors.toSet()));
		patientModel.setCaregiverList(Stream.of(caregiverModel)
		                                    .collect(Collectors.toSet()));
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Prescription prescription = TestResponseGenerator.generatePrescription();
		Mockito.when(mockUtility.fetchUnsoftDeletedPrescriptionInfo(Mockito.any()))
		       .thenReturn(prescription);
		PrescriptionModel prescriptionModel = generateRequestObject("PrescriptionModel-1", PrescriptionModel.class,
		                                                            PRESCRIPTION_DETAILS_FILE);
		Mockito.when(mockUtility.convertPrescriptionEntityToModel(Mockito.any()))
		       .thenReturn(prescriptionModel);
		UserDetailsModel userDetailsModel = generateRequestObject("userDetails-Caregiver1", UserDetailsModel.class,
		                                                          USER_DETAILS_FILE);
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(
				       TestResponseGenerator.generateJwtUserDetails(userDetails, true, ApplicationConstants.PATIENT));
		List<PatientModel> listPatientModel = Stream.of(patientModel)
		                                            .collect(Collectors.toList());
		Mockito.when(mockPHIDataEncryptionService.encryptPatientList(Mockito.any(), Mockito.anyString()))
		       .thenReturn(listPatientModel);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(mockHttpRequest, mockAuthentication))
		       .thenReturn("keys");
		List<PatientModel> listPatientModels = patientService.fetchPatietnsByProviderIdFromCookie(mockAuthentication,
		                                                                                          mockHttpRequest);
		assertNotNull(listPatientModels);
		assertEquals(listPatientModels, listPatientModel);
	}

	@Test
	public void testFetchAllPatientsWithArgs() throws Exception {
		List<Patient> patients = Stream.of(patient)
		                               .collect(Collectors.toList());
		Mockito.when(mockPatientRepository.findAll())
		       .thenReturn(patients);
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		List<PatientModel> listPatientModel = Stream.of(patientModel)
		                                            .collect(Collectors.toList());
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Mockito.when(mockPHIDataEncryptionService.encryptPatientList(Mockito.any(), Mockito.anyString()))
		       .thenReturn(listPatientModel);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(mockHttpRequest, mockAuthentication))
		       .thenReturn("keys");
		List<PatientModel> listPatientModels = patientService.fetchAllPatients(mockAuthentication, mockHttpRequest);
		assertNotNull(listPatientModels);
		assertEquals(listPatientModels, listPatientModel);
	}

	@Test
	public void testFetchPatientByIdWithArgs() throws Exception {
		/*
		 * case 1: fail if patient is empty
		 */
		try {
			patientService.fetchPatientById(null, mockAuthentication, mockHttpRequest);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 2 : Fail if patient is null
		 */
		try {
			Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
			       .thenReturn(Optional.empty());
			patientService.fetchPatientById(null, mockAuthentication, mockHttpRequest);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 3: success
		 */
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(
				       Optional.of(TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT)));
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(patientModel);
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(
				       TestResponseGenerator.generateJwtUserDetails(userDetails, true, ApplicationConstants.PATIENT));
		Mockito.when(mockPHIDataEncryptionService.getKeyText(mockHttpRequest, mockAuthentication))
		       .thenReturn("keys");
		Mockito.when(mockPHIDataEncryptionService.encryptPatient(Mockito.any(), Mockito.anyString()))
		       .thenReturn(patientModel);
		PatientModel patientModelObj = patientService.fetchPatientById("1", mockAuthentication, mockHttpRequest);
		assertNotNull(patientModel);
		assertEquals(patientModel, patientModelObj);
	}

	@Test
	public void testUpdatePatient() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(mockHttpRequest, mockAuthentication))
		       .thenReturn("keys");

		try {
			Mockito.when(mockPHIDataEncryptionService.decryptPatient(Mockito.any(), Mockito.anyString()))
			       .thenReturn(null);
			patientService.updatePatient(patientModel, null, mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
		}
		Mockito.when(mockPHIDataEncryptionService.decryptPatient(Mockito.any(), Mockito.anyString()))
		       .thenReturn(patientModel);
		/*
		 * case 1: Fail if patient mobile is null
		 */
		try {
			PatientModel patientModelMobileNull = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
			patientModelMobileNull.getUserDetails()
			                      .setMobileNumber(null);
			Mockito.when(mockPHIDataEncryptionService.decryptPatient(Mockito.any(), Mockito.anyString()))
			       .thenReturn(patientModelMobileNull);
			patientService.updatePatient(patientModelMobileNull, null, mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
		}
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);

		/**
		 * case 3: success
		 */
		Mockito.when(mockPHIDataEncryptionService.decryptPatient(Mockito.any(), Mockito.anyString()))
		       .thenReturn(patientModel);
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		com.sg.dayamed.entity.Address address = new com.sg.dayamed.entity.Address();
		address.setCountry("india");
		patient.getUserDetails()
		       .setAddress(address);
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Mockito.when(mockUtility.convertPatientModelTopatientEntity(Mockito.any()))
		       .thenReturn(patient);
		Caregiver caregiver = TestResponseGenerator.generateCaregiver("Male", "Asian", "Caregiver", false);
		Set<Caregiver> CaregiverSet = new HashSet<Caregiver>();
		CaregiverSet.add(caregiver);
		patient.setCaregivers(CaregiverSet);
		Map<String, String> analyticMap = new HashMap<String, String>();
		analyticMap.put("emoji", "happy");
		Mockito.when(mockImanticService.fetchAnalyticsMap(Mockito.anyString()))
		       .thenReturn(analyticMap);

		Mockito.when(mockPatientRepository.save(Mockito.any(Patient.class)))
		       .thenReturn(patient);
		Mockito.when(mockUtility.convertPatientEntityTopatientModel(patient))
		       .thenReturn(patientModel);
		Mockito.when(mockUtility.getUserPreferedLang(Mockito.anyLong()))
		       .thenReturn("abcd");
		Mockito.when(mockMessageNotificationConstants.getUserPreferedLangTranslatedMsg(Mockito.any(),
		                                                                               Mockito.anyString(), Mockito.anyString(), Mockito.any(Object[].class)))
		       .thenReturn("abcd");
		Mockito.when(mockPHIDataEncryptionService.encryptPatient(Mockito.any(), Mockito.anyString()))
		       .thenReturn(patientModel);
		PatientModel actualPatientModel = patientService.updatePatient(patientModel, profileImage, mockHttpRequest,
		                                                               mockAuthentication);
		assertNotNull(actualPatientModel);
		assertEquals(patientModel, actualPatientModel);
	}

	@Test
	public void testDeletePatient() throws Exception {
		JwtUserDetails jwtUser = TestResponseGenerator.generateJwtUserDetails(userDetails, true, ApplicationConstants.PATIENT);
		Mockito.when(mockAuthentication.getPrincipal())
		       .thenReturn(jwtUser);
		;
		/*
		 * case 1: if patient id is null
		 */
		try {
			patientService.deletePatient(null, mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 2: if success
		 */
		UserDetails userDetails = TestResponseGenerator.generateUserDetails("Male", "Asian", "Patient");
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", ApplicationConstants.PROVIDER);
		jwtUser.setUserEmailId("admin@dayamed.com");
		provider.getUserDetails()
		        .setEmailId(jwtUser.getUserEmailId());
		patient.setProviders(Stream.of(provider)
		                           .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		Mockito.when(mockPatientHelper.deletePatient(Mockito.anyString()))
		       .thenReturn(ApplicationConstants.SUCCESS);
		String status = patientService.deletePatient("1", mockHttpRequest, mockAuthentication);
		assertNotNull(status);
		assertEquals(status, ApplicationConstants.SUCCESS);
		/*
		 * case 3: Fail  with UNAUTHORIZED
		 */
		try {
			provider.getUserDetails()
			        .setEmailId("test@dayamed.com");
			patientService.deletePatient("1", mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}

		/*
		 * case 4: if fail
		 */
		try {
			Mockito.when(mockPatientHelper.deletePatient(Mockito.anyString()))
			       .thenReturn(ApplicationConstants.FAIL);
			patientService.deletePatient("1", mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
	}

	@Test
	public void testFetchProvidersByPatientId() throws Exception {
		/**
		 * case 1: Faile if patient id is null.
		 */
		try {
			patientService.fetchProvidersByPatientId(null);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 2:
		 */
		Mockito.when(mockProviderService.fetchProvidersByPatientId(Mockito.anyLong()))
		       .thenReturn(Stream.of(TestResponseGenerator.generateProviderModel())
		                         .collect(Collectors.toList()));
		Mockito.when(mockCaregiverService.fetchCaregiversByPatientId(Mockito.anyLong()))
		       .thenReturn(Stream.of(TestResponseGenerator.generateCaregiverModel("Male", "Asian"))
		                         .collect(Collectors.toList()));
		Mockito.when(mockPharmacistService.fetchPharmacistListByPatientId(Mockito.anyLong()))
		       .thenReturn(Stream.of(TestResponseGenerator.generatePharmacistModel())
		                         .collect(Collectors.toList()));
		Patient patient = TestResponseGenerator.generatePatient("Male", "Asian", ApplicationConstants.PATIENT);
		Provider provider = TestResponseGenerator.generateProvider("Male", "Asian", ApplicationConstants.PROVIDER);
		provider.getUserDetails()
		        .setUserOptions(TestResponseGenerator.generateUserOptions(1, provider.getUserDetails()));
		patient.setProviders(Stream.of(provider)
		                           .collect(Collectors.toSet()));
		Mockito.when(mockPatientRepository.findById(Mockito.anyLong()))
		       .thenReturn(Optional.of(patient));
		PatientAssociatedActorsPojo patientAssociatedActorsPojo = patientService.fetchProvidersByPatientId("1");
		assertNotNull(patientAssociatedActorsPojo);
		assertNotNull(patientAssociatedActorsPojo.getProviderModelList());
	}

	@Test
	public void testAddPatientWithArgs() throws Exception {
		/*
		 * case 1: Fail if patient model is null
		 */
		try {
			patientService.addPatient(null, null, mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}

		/*
		 * case 2: Fail if patient model mobile number blank
		 */
		try {
			PatientModel patientModel = TestResponseGenerator.generatePatientModel();
			patientModel.getUserDetails()
			            .setMobileNumber(null);
			patientService.addPatient(patientModel, null, mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 3: email conflict error.
		 */
		PatientModel patientModel = TestResponseGenerator.generatePatientModel();
		Mockito.when(mockPHIDataEncryptionService.decryptPatient(Mockito.any(), Mockito.anyString()))
		       .thenReturn(patientModel);
		Mockito.when(mockPHIDataEncryptionService.getKeyText(mockHttpRequest, mockAuthentication))
		       .thenReturn("keys");
		InputStream inputFile = new ByteArrayInputStream("test data".getBytes());
		MockMultipartFile profileImage = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data",
		                                                       inputFile);
		Mockito.when(mockUtility.uploadProfilepic(Mockito.any(), Mockito.anyString(), Mockito.anyString()))
		       .thenReturn("path");
		try {
			Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString()))
			       .thenReturn(patientModel.getUserDetails());
			patientService.addPatient(patientModel, profileImage, mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 4 : Fail patient model is null while saving.
		 */
		try {
			Mockito.when(mockPatientHelper.addPatient(Mockito.any(), Mockito.any()))
			       .thenReturn(null);
			patientService.addPatient(patientModel, profileImage, mockHttpRequest, mockAuthentication);
		} catch (Exception e) {
			assertTrue(e instanceof ApplicationException);
			ApplicationException exception = (ApplicationException) e;
			assertEquals("Application Error", exception.getResult());
		}
		/*
		 * case 5: save success
		 */
		Mockito.when(mockUserDetailsService.findByEmailIdIgnoreCase(Mockito.anyString()))
		       .thenReturn(null);
		Mockito.when(mockPatientHelper.addPatient(Mockito.any(), Mockito.any()))
		       .thenReturn(patientModel);
		Mockito.when(mockPHIDataEncryptionService.encryptPatient(Mockito.any(), Mockito.anyString()))
		       .thenReturn(patientModel);
		PatientModel savedEncyPatinetModel = patientService.addPatient(patientModel, profileImage, mockHttpRequest,
		                                                               mockAuthentication);
		assertNotNull(savedEncyPatinetModel);
		assertEquals(savedEncyPatinetModel, patientModel);
	}
}
