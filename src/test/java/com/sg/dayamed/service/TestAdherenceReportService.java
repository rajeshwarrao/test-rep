package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.dosage.frequency.DailyDosageFrequency;
import com.sg.dayamed.dosage.frequency.FrequencyAbstractFactory;
import com.sg.dayamed.entity.AdherenceDataPoints;
import com.sg.dayamed.entity.DosageInfo;
import com.sg.dayamed.entity.Medicine;
import com.sg.dayamed.entity.Patient;
import com.sg.dayamed.entity.Prescription;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.AdherenceReportResModel;
import com.sg.dayamed.pojo.DosageInfoModel;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.ProviderModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.repository.AdherenceRepository;
import com.sg.dayamed.repository.PrescriptionRepository;
import com.sg.dayamed.service.impl.AdherenceReportServiceImpl;
import com.sg.dayamed.util.ApplicationConstants;
import com.sg.dayamed.util.DateUtility;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(PowerMockRunner.class)
public class TestAdherenceReportService extends BaseTestCase {

	@InjectMocks
	private AdherenceReportService adherenceReportService = new AdherenceReportServiceImpl();

	@Mock
	private PatientService patientService;

	@Mock
	private Utility utility;

	@Mock
	private PrescriptionRepository prescriptionRepository;

	@Mock
	private DateUtility dateUtility;

	@Mock
	private AdherenceRepository adherenceRepository;

	@Mock
	private FrequencyAbstractFactory frequencyAbstractFactory;

	@Test
	public void testGetAdherenceReport() {
		/**
		 * Case 1 : With Consumed.
		 */
		List<Long> providerIds = new ArrayList<>();
		providerIds.add(0l);
		List<Patient> listPatient = getPatient();
		Mockito.when(patientService.fetchEncryptPatientsByProviderId(Mockito.anyLong()))
		       .thenReturn(listPatient);
		Mockito.when(patientService.fetchAllPatients())
		       .thenReturn(listPatient);
		Mockito.when(utility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(getPatientModel().get(0));
		Mockito.when(prescriptionRepository.findByPatient_id(Mockito.anyLong()))
		       .thenReturn(getPrescription());
		Mockito.when(utility.convertDosageinfoEntityToModel(Mockito.any()))
		       .thenReturn(getDosageInfoModel().get(0));
		Mockito.when(frequencyAbstractFactory.getFrequencyFactory(Mockito.anyString()))
		       .thenReturn(new DailyDosageFrequency());
		Mockito.when(dateUtility.convertLocalTimeToUTCWithZoneId(Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(LocalDateTime.now());
		AdherenceDataPoints dataPoints = TestResponseGenerator.generateAdherenceDataPoints(Stream.of("consumed")
		                                                                                         .collect(
				                                                                                         Collectors.toList()));
		dataPoints.setPrescribedTime(dataPoints.getPrescribedTime()
		                                       .plusMinutes(300));
		Mockito.when(
				adherenceRepository.findByPrescriptionIDAndAdherenceDosageInfoMapList_DosageinfoId(Mockito.anyLong(),
				                                                                                   Mockito.any()))
		       .thenReturn(Stream.of(dataPoints)
		                         .collect(Collectors.toList()));
		AdherenceReportResModel adherenceReportResModel = adherenceReportService.getAdherenceReport(providerIds, null);
		assertNotNull(adherenceReportResModel);
		assertEquals(adherenceReportResModel.getStatus(), ApplicationConstants.SUCCESS);
		assertNotNull(adherenceReportResModel.getAdherenceReportModelList());

		/**
		 * Case 2 : With out consumed.
		 */
		dataPoints = TestResponseGenerator.generateAdherenceDataPoints(Stream.of("consumed")
		                                                                     .collect(Collectors.toList()));
		dataPoints.setPrescribedTime(dataPoints.getPrescribedTime()
		                                       .plusMinutes(330));
		Mockito.when(
				adherenceRepository.findByPrescriptionIDAndAdherenceDosageInfoMapList_DosageinfoId(Mockito.anyLong(),
				                                                                                   Mockito.any()))
		       .thenReturn(Stream.of(dataPoints)
		                         .collect(Collectors.toList()));
		adherenceReportResModel = adherenceReportService.getAdherenceReport(providerIds, null);
		assertNotNull(adherenceReportResModel);
		assertEquals(adherenceReportResModel.getStatus(), ApplicationConstants.SUCCESS);
		assertNotNull(adherenceReportResModel.getAdherenceReportModelList());

		/**
		 * Case 3 : FirstConsumption type
		 */
		Mockito.reset(utility);
		DosageInfoModel dosageInfoModel = getDosageInfoModel().get(0);
		dosageInfoModel.setTime(null);
		dosageInfoModel.setFristConsumptionType(TestResponseGenerator.generateFristConsumptionTypeModel());
		Mockito.when(utility.convertPatientEntityTopatientModel(Mockito.any()))
		       .thenReturn(getPatientModel().get(0));
		Mockito.when(utility.convertDosageinfoEntityToModel(Mockito.any()))
		       .thenReturn(dosageInfoModel);
		dataPoints = TestResponseGenerator.generateAdherenceDataPoints(Stream.of("consumed")
		                                                                     .collect(Collectors.toList()));
		dataPoints.setPrescribedTime(dataPoints.getPrescribedTime()
		                                       .plusMinutes(330));
		Mockito.when(
				adherenceRepository.findByPrescriptionIDAndAdherenceDosageInfoMapList_DosageinfoId(Mockito.anyLong(),
				                                                                                   Mockito.any()))
		       .thenReturn(Stream.of(dataPoints)
		                         .collect(Collectors.toList()));
		adherenceReportResModel = adherenceReportService.getAdherenceReport(providerIds, null);
		assertNotNull(adherenceReportResModel);
		assertEquals(adherenceReportResModel.getStatus(), ApplicationConstants.SUCCESS);
		assertNotNull(adherenceReportResModel.getAdherenceReportModelList());

	}

	@Test
	public void testGetAdherenceReportErrorCases() {
		/**
		 * Case 1 : Patient loading failed
		 */
		List<Long> providerIds = new ArrayList<>();
		providerIds.add(0l);
		Mockito.when(patientService.fetchAllPatients())
		       .thenThrow(new RuntimeException());
		AdherenceReportResModel adherenceReportResModel = adherenceReportService.getAdherenceReport(null, null);
		assertEquals(ApplicationConstants.FAIL, adherenceReportResModel.getStatus());

		/**
		 * Case 2 : Adherence data loading failed
		 */
		Mockito.reset(patientService);
		List<Patient> listPatient = getPatient();
		Mockito.when(patientService.fetchAllPatients())
		       .thenReturn(listPatient);
		Mockito.when(
				adherenceRepository.findByPrescriptionIDAndAdherenceDosageInfoMapList_DosageinfoId(Mockito.anyLong(),
				                                                                                   Mockito.any()))
		       .thenThrow(new RuntimeException());
		adherenceReportResModel = adherenceReportService.getAdherenceReport(null, null);
		assertEquals(ApplicationConstants.FAIL, adherenceReportResModel.getStatus());

		/**
		 * Case 3 : Patient List is empty
		 */
		Mockito.reset(patientService);
		Mockito.when(patientService.fetchAllPatients())
		       .thenReturn(new ArrayList<>());
		adherenceReportResModel = adherenceReportService.getAdherenceReport(providerIds, null);
		assertEquals(ApplicationConstants.FAIL, adherenceReportResModel.getStatus());
		assertEquals("Patients do not have Prescriptions", adherenceReportResModel.getMessage());
	}

	public List<Prescription> getPrescription() {
		List<Prescription> prescriptionList = new ArrayList<>();
		Prescription prescription = new Prescription();
		prescription.setId(12);
		prescription.setDosageInfoList(getDosageInfo());
		prescriptionList.add(prescription);
		return prescriptionList;
	}

	public List<DosageInfo> getDosageInfo() {
		List<DosageInfo> dosageInfoList = new ArrayList<>();
		DosageInfo dosageInfo = new DosageInfo();
		Medicine medicine = new Medicine();
		medicine.setName("Test Mediciene");
		dosageInfo.setMedicine(medicine);
		dosageInfoList.add(dosageInfo);
		return dosageInfoList;
	}

	public List<DosageInfoModel> getDosageInfoModel() {
		List<DosageInfoModel> dosageInfoList = new ArrayList<>();
		DosageInfoModel dosageInfo = new DosageInfoModel();
		dosageInfo.setFrequency(
				"{\"type\":\"daily\",\"startDate\":\"06/25/2019\",\"value\":5,\"recurrence\":null," +
						"\"endAfterOccurrences\":null}");
		List<String> alarmTimes = new ArrayList<>();
		alarmTimes.add("07:20 PM");
		alarmTimes.add("07:25 PM");
		alarmTimes.add("07:30 PM");
		dosageInfo.setTime(alarmTimes);
		dosageInfoList.add(dosageInfo);
		return dosageInfoList;
	}

	public List<Patient> getPatient() {
		List<Patient> patientList = new ArrayList<>();
		Patient patient = new Patient();
		patient.setId(2827);
		UserDetails userDetails = new UserDetails();
		userDetails.setId(1l);
		userDetails.setEmailId("p1@p.com");
		userDetails.setFirstName("Test");
		userDetails.setLastName("Last");
		patient.setUserDetails(userDetails);
		Patient patient2 = new Patient();
		patient2.setId(2827);
		UserDetails userDetails2 = new UserDetails();
		userDetails2.setId(1l);
		userDetails2.setEmailId("p2@p.com");
		userDetails2.setFirstName("Test2");
		userDetails2.setLastName("Last2");
		patient2.setUserDetails(userDetails2);
		patientList.add(patient2);
		return patientList;
	}

	public List<PatientModel> getPatientModel() {
		List<PatientModel> patientList = new ArrayList<>();
		PatientModel patient = new PatientModel();
		patient.setId(2827);
		Set<ProviderModel> listProvider = new HashSet<>();
		ProviderModel providerModel = new ProviderModel();
		listProvider.add(providerModel);
		patient.setProviders(listProvider);
		UserDetailsModel userDetails = new UserDetailsModel();
		userDetails.setId(1);
		userDetails.setEmailId("p1@p.com");
		userDetails.setFirstName("Test");
		userDetails.setLastName("Last");
		patient.setUserDetails(userDetails);
		providerModel.setUserDetails(userDetails);
		PatientModel patient2 = new PatientModel();
		patient2.setId(2827);
		UserDetailsModel userDetails2 = new UserDetailsModel();
		userDetails2.setId(1);
		userDetails2.setEmailId("p2@p.com");
		userDetails2.setFirstName("Test2");
		userDetails2.setLastName("Last2");
		patient2.setUserDetails(userDetails2);
		patient2.setProviders(listProvider);
		patientList.add(patient2);
		return patientList;
	}
}
