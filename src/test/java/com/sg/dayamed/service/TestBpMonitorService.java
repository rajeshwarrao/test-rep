package com.sg.dayamed.service;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.entity.BpMonitor;
import com.sg.dayamed.repository.BpMonitorRepository;
import com.sg.dayamed.service.impl.BpMonitorServiceImpl;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Eresh Gorantla on 04/May/2019
 **/
@RunWith(PowerMockRunner.class)
public class TestBpMonitorService extends BaseTestCase {

	@InjectMocks
	BpMonitorService bpMonitorService = new BpMonitorServiceImpl();

	@Mock
	BpMonitorRepository mockBpMonitorRepository;

	@Test
	public void testFindByPrescribedTime() throws Exception {
		BpMonitor bpMonitor = new BpMonitor();
		bpMonitor.setSystolicpressureValue("100");
		bpMonitor.setPrescriptionID(1);
		bpMonitor.setDiastolicpressureValue("100");
		bpMonitor.setDeviceInfoId("Junit");
		bpMonitor.setConsumptionStatus(Stream.of("Consumed")
		                                     .collect(Collectors.toList()));
		LocalDateTime presDateTime = LocalDateTime.now()
		                                          .plusMinutes(-10);
		LocalDateTime obsDateTime = LocalDateTime.now();
		bpMonitor.setObservedTime(obsDateTime);
		bpMonitor.setPrescribedTime(presDateTime);
		List<BpMonitor> expectedBpMonitors = Stream.of(bpMonitor)
		                                           .collect(Collectors.toList());
		Mockito.when(mockBpMonitorRepository.findByPrescribedTime(Mockito.any()))
		       .thenReturn(expectedBpMonitors);
		List<BpMonitor> actualBpMonitors = bpMonitorService.findByPrescribedTime(LocalDateTime.now());
		assertTrue(CollectionUtils.isNotEmpty(actualBpMonitors));
		assertEquals(expectedBpMonitors.get(0)
		                               .getDiastolicpressureValue(), actualBpMonitors.get(0)
		                                                                             .getDiastolicpressureValue());
		assertEquals(expectedBpMonitors.get(0)
		                               .getSystolicpressureValue(), actualBpMonitors.get(0)
		                                                                            .getSystolicpressureValue());
		assertEquals(expectedBpMonitors.get(0)
		                               .getDeviceInfoId(), actualBpMonitors.get(0)
		                                                                   .getDeviceInfoId());
		assertEquals(expectedBpMonitors.get(0)
		                               .getPrescribedTime(), actualBpMonitors.get(0)
		                                                                     .getPrescribedTime());
		assertEquals(expectedBpMonitors.get(0)
		                               .getObservedTime(), actualBpMonitors.get(0)
		                                                                   .getObservedTime());
		assertEquals(expectedBpMonitors.get(0)
		                               .getId(), actualBpMonitors.get(0)
		                                                         .getId());
		assertEquals(expectedBpMonitors.get(0)
		                               .getConsumptionStatus(), actualBpMonitors.get(0)
		                                                                        .getConsumptionStatus());
	}
}
