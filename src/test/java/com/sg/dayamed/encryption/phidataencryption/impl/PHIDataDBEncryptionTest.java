package com.sg.dayamed.encryption.phidataencryption.impl;

import com.sg.dayamed.base.BaseTestCase;
import com.sg.dayamed.encryption.phidataencryption.PHIDataDBEncryption;
import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;
import com.sg.dayamed.entity.UserDetails;
import com.sg.dayamed.pojo.PatientModel;
import com.sg.dayamed.pojo.UserDetailsModel;
import com.sg.dayamed.redis.RedisKeyConstants;
import com.sg.dayamed.service.PHIDataEncryptionService;
import com.sg.dayamed.service.RediseService;
import com.sg.dayamed.util.TestResponseGenerator;
import com.sg.dayamed.util.Utility;
import com.sg.dayamed.util.enums.UserRoleEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.crypto.SecretKey;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class PHIDataDBEncryptionTest extends BaseTestCase {

	@InjectMocks
	private PHIDataDBEncryption phiDataDBEncryption = new PHIDataDBEncryptionImpl();

	@Mock
	private RediseService mockRedisService;

	@Mock
	private PHIDataEncryptionService mockPhiDataEncryptionService;

	@Mock
	private PHIDataEncryption mockPhiDataEncryption;

	@Mock
	private Utility mockUtility;

	@Mock
	private SecretKey mockSecretKey;

	@Before
	public void setup() throws Exception {
		ReflectionTestUtils.setField(phiDataDBEncryption, "strDBEncryptionKey", "test");
		Mockito.when(mockRedisService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.dbencryptionenable)))
		       .thenReturn("true");
		Mockito.when(mockRedisService.getDataInRedisByKey(Mockito.eq(RedisKeyConstants.dbencryptionFeilds)))
		       .thenReturn("firstName,lastName");
		Mockito.when(mockPhiDataEncryption.getSecretEncryptionKey(Mockito.anyString()))
		       .thenReturn(mockSecretKey);
	}

	@Test
	public void testEncryptPatientList() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(
				mockPhiDataEncryptionService.encryptPatient(Mockito.any(), Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(patientModel);
		List<PatientModel> patientModels = Stream.of(patientModel)
		                                         .collect(Collectors.toList());
		List<PatientModel> actualPatientModels = phiDataDBEncryption.encryptPatientList(patientModels, "test");
		assertTrue(CollectionUtils.isNotEmpty(actualPatientModels));
		assertEquals(patientModels.size(), actualPatientModels.size());
		assertEquals(patientModels, actualPatientModels);
	}

	@Test
	public void testDecryptPatient() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		Mockito.when(
				mockPhiDataEncryptionService.decryptPatient(Mockito.any(), Mockito.anyString(), Mockito.anyString()))
		       .thenReturn(patientModel);
		PatientModel actualPatientModels = phiDataDBEncryption.decryptPatient(patientModel, "test");
		assertNotNull(actualPatientModels);
		assertEquals(patientModel, actualPatientModels);
	}

	@Test
	public void testEncryptUserDetails() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		UserDetailsModel userDetailsModel = patientModel.getUserDetails();
		Mockito.when(mockPhiDataEncryptionService.encryptUserDetails(Mockito.any(), Mockito.anyString(),
		                                                             Mockito.anyString()))
		       .thenReturn(userDetailsModel);
		UserDetailsModel actual = phiDataDBEncryption.encryptUserDetails(userDetailsModel, "test");
		assertNotNull(actual);
		assertEquals(userDetailsModel, actual);
	}

	@Test
	public void testDecryptUserDetailsEntity() throws Exception {
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		UserDetailsModel userDetailsModel = patientModel.getUserDetails();
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "asian", UserRoleEnum.PATIENT.getRole());
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any()))
		       .thenReturn(userDetailsModel);
		Mockito.when(mockPhiDataEncryptionService.decryptPatientUserDetails(Mockito.any(), Mockito.anyString(),
		                                                                    Mockito.anyString()))
		       .thenReturn(userDetailsModel);
		Mockito.when(mockUtility.convertUserDetailsModelToEntity(Mockito.any()))
		       .thenReturn(userDetails);
		UserDetails actual = phiDataDBEncryption.decryptUserDetailsEntity(userDetails, "test");
		assertNotNull(actual);
		assertEquals(userDetails, actual);
	}

	@Test
	public void testEncryptUserDetailsEntity() throws Exception {
		UserDetails userDetails =
				TestResponseGenerator.generateUserDetails("Male", "asian", UserRoleEnum.PATIENT.getRole());
		PatientModel patientModel = generateRequestObject("PatientModel-1", PatientModel.class, USER_DETAILS_FILE);
		UserDetailsModel userDetailsModel = patientModel.getUserDetails();
		Mockito.when(mockUtility.convertUserDetailsEntityToModel(Mockito.any()))
		       .thenReturn(userDetailsModel);
		Mockito.when(mockPhiDataEncryptionService.encryptUserDetails(Mockito.any(), Mockito.anyString(),
		                                                             Mockito.anyString()))
		       .thenReturn(userDetailsModel);
		Mockito.when(mockUtility.convertUserDetailsModelToEntity(Mockito.any()))
		       .thenReturn(userDetails);
		UserDetails actual = phiDataDBEncryption.encryptUserDetailsEntitiy(userDetails, "test");
		assertNotNull(actual);
		assertEquals(userDetails, actual);
	}

	@Test
	public void testFieldLevelEncryption() throws Exception {
		String text = "sample";
		String encryptedText = RandomStringUtils.randomAlphabetic(10);
		Mockito.when(mockPhiDataEncryption.encryptText(Mockito.eq(text), Mockito.eq(mockSecretKey)))
		       .thenReturn(encryptedText);
		String actual = phiDataDBEncryption.filedLevelEncryption("123", text, UserRoleEnum.PATIENT.getRole());
		assertNotNull(actual);
		assertEquals(encryptedText, actual);

		/**
		 * With Exception
		 */
		Mockito.when(mockPhiDataEncryption.encryptText(Mockito.anyString(), Mockito.any()))
		       .thenThrow(new RuntimeException());
		actual = phiDataDBEncryption.filedLevelEncryption("123", text, UserRoleEnum.PATIENT.getRole());
		assertNotNull(actual);
		assertNotEquals(encryptedText, actual);
	}

	@Test
	public void testFieldLevelDecryption() throws Exception {
		String text = "sample";
		String encryptedText = RandomStringUtils.randomAlphabetic(10);
		Mockito.when(mockPhiDataEncryption.decryptText(Mockito.eq(encryptedText), Mockito.eq(mockSecretKey)))
		       .thenReturn(text);
		String actual = phiDataDBEncryption.filedLevelDecryption("123", encryptedText, UserRoleEnum.PATIENT.getRole());
		assertNotNull(actual);
		assertEquals(text, actual);

		/**
		 * Error case
		 */
		Mockito.reset(mockPhiDataEncryption);
		Mockito.when(mockPhiDataEncryption.decryptText(Mockito.anyString(), Mockito.any()))
		       .thenThrow(new RuntimeException());
		actual = phiDataDBEncryption.filedLevelDecryption("123", encryptedText, UserRoleEnum.PATIENT.getRole());
		assertNotNull(actual);
		assertNotEquals(text, actual);
	}

	@Test
	public void testGetDBEntryKey() {
		assertNotNull(phiDataDBEncryption.getDBEncryKey());
		assertEquals("test", phiDataDBEncryption.getDBEncryKey());
	}
}


