package com.sg.dayamed.encryption.phidataencryption.impl;

import com.sg.dayamed.encryption.phidataencryption.PHIDataEncryption;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import javax.crypto.SecretKey;

import static org.junit.Assert.*;

/**
 * Created By Gorantla, Eresh on 04/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class PHIDataEncryptionTest {

	@InjectMocks
	private PHIDataEncryption phiDataEncryption = new PHIDataEncryptionImpl();

	@Test
	public void testEncryptText() throws Exception {
		/**
		 * Case 1 : With out exception
		 */
		String text = "Junit";
		SecretKey secretKey = phiDataEncryption.getSecretEncryptionKey("test");
		String encryptedText = phiDataEncryption.encryptText(text, secretKey);
		assertNotNull(encryptedText);
		assertNotEquals(text, encryptedText);

		/**
		 * Case 2 : With Exception
		 */
		secretKey = phiDataEncryption.getSecretEncryptionKey(null);
		encryptedText = phiDataEncryption.encryptText(text, secretKey);
		assertNotNull(encryptedText);
		assertEquals(text, encryptedText);
	}

	@Test(expected = Exception.class)
	public void testDecryptTextByte() throws Exception {
		String text = "Junit";
		SecretKey secretKey = phiDataEncryption.getSecretEncryptionKey("1234567890ABCD");
		String encryptedText = phiDataEncryption.encryptText(text, secretKey);
		phiDataEncryption.decryptText(encryptedText.getBytes(), secretKey);
	}

	@Test
	public void testDecryptText() throws Exception {
		/**
		 * Case 1 : With out exception
		 */
		String text = "Junit";
		SecretKey secretKey = phiDataEncryption.getSecretEncryptionKey("test");
		String encryptedText = phiDataEncryption.encryptText(text, secretKey);
		String decryptedText = phiDataEncryption.decryptText(encryptedText, secretKey);
		assertNotNull(decryptedText);
		assertEquals(text, decryptedText);
	}
}
