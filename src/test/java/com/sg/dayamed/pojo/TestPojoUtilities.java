package com.sg.dayamed.pojo;

import com.sg.dayamed.util.TestResponseGenerator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/

public class TestPojoUtilities {

	@Test
	public void testAdherenceReportModel() {
		AdherenceReportModel reportModel = TestResponseGenerator.generateAdherenceReportModel();
		assertNotNull(reportModel);
		assertNotNull(reportModel.getAdherenceAction());
		assertNotNull(reportModel.getAdherenceActionType());
		assertNotNull(reportModel.getDosageOfDay());
		assertNotNull(reportModel.getDosageTimeOfTheDay());
		assertNotNull(reportModel.getEventTime());
		assertNotNull(reportModel.getMedicationName());
		assertNotNull(reportModel.getMedicationQuantity());
		assertNotNull(reportModel.getObservedTime());
		assertNotNull(reportModel.getPatientEmailAddress());
		assertNotNull(reportModel.getPatientId());
		assertNotNull(reportModel.getPatientName());
		assertNotNull(reportModel.getPrescriptionId());
		assertNotNull(reportModel.getProviderName());
		assertNotNull(reportModel.getZone());
	}

	@Test
	public void testDayamedException() {
		DayamedException exception = new DayamedException();
		exception = new DayamedException("Some Exception");
		exception.setError("Some Exception");
		assertNotNull(exception);
		assertEquals("Some Exception", exception.getError());

	}
}
