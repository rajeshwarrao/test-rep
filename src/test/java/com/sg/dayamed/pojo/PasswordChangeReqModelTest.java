package com.sg.dayamed.pojo;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
public class PasswordChangeReqModelTest {

	private Validator validator;

	@Before
	public void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void testCurrentPasswordEmpty() {
		PasswordChangeReqModel passwordChangeReqModel = new PasswordChangeReqModel();
		passwordChangeReqModel.setCurrentPassword(StringUtils.EMPTY);
		passwordChangeReqModel.setNewPassword(RandomStringUtils.randomAlphabetic(10));
		final Set<ConstraintViolation<PasswordChangeReqModel>> constraintViolations =
				validator.validate(passwordChangeReqModel);
		assertFalse(constraintViolations.isEmpty());
		final ConstraintViolation<PasswordChangeReqModel> vi = constraintViolations.iterator()
		                                                                           .next();
		assertEquals("{errors.invalid.current.password}", vi.getMessage());
	}

	@Test
	public void testNewPasswordEmpty() {
		PasswordChangeReqModel passwordChangeReqModel = new PasswordChangeReqModel();
		passwordChangeReqModel.setNewPassword("");
		passwordChangeReqModel.setCurrentPassword(RandomStringUtils.randomAlphabetic(10));
		final Set<ConstraintViolation<PasswordChangeReqModel>> constraintViolations =
				validator.validate(passwordChangeReqModel);
		assertFalse(constraintViolations.isEmpty());
		final ConstraintViolation<PasswordChangeReqModel> vi = constraintViolations.iterator()
		                                                                           .next();
		assertEquals("{errors.invalid.new.password}", vi.getMessage());
	}
}
