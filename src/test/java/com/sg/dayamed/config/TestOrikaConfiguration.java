package com.sg.dayamed.config;

import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;
import java.util.Optional;

/**
 * Created by Eresh Gorantla on 20/Jun/2019
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({DefaultMapperFactory.MapperFactoryBuilder.class})
public class TestOrikaConfiguration {

	@InjectMocks
	private OrikaConfiguration orikaConfiguration;

	@Mock
	private Optional<List<OrikaMapperFactoryBuilderConfigurer>> orikaMapperFactoryBuilderConfigurers;

	@Mock
	OrikaProperties mockOrikaProperties;

	@Test
	public void testOrikaConfiguration() throws Exception {
		orikaConfiguration.orikaMapperFactoryBuilder();
	}
}
