package com.sg.dayamed.dao;

import com.sg.dayamed.service.v1.UserPreferencesVService;
import com.sg.dayamed.service.v1.vo.users.UserPreferenceVO;
import com.sg.dayamed.util.TestResponseGenerator;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created By Gorantla, Eresh on 12/Sep/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class UserDetailsDaoTest {

	@InjectMocks
	private UserDetailsDao userDetailsDao;

	@Mock
	private UserPreferencesVService mockUserPreferencesVService;

	@Test
	public void testLoadUserCacheDetails() {
		List<UserPreferenceVO> preferenceVOS = IntStream.range(0, 10)
		                                                .mapToObj(index -> TestResponseGenerator.generateUserPreferenceVO())
		                                                .collect(Collectors.toList());
		Mockito.when(mockUserPreferencesVService.getAllUserPreferences())
		       .thenReturn(preferenceVOS);
		/**
		 * Case 1 : Data exists
		 */
		List<UserDetailsCacheVO> cacheVOS = userDetailsDao.loadUserCacheDetails();
		assertTrue(CollectionUtils.isNotEmpty(cacheVOS));
		assertEquals(preferenceVOS.size(), cacheVOS.size());
		for (Integer index = 0; index < preferenceVOS.size(); index++) {
			assertEquals(preferenceVOS.get(index)
			                          .getUserId(), cacheVOS.get(index)
			                                                .getId());
			assertEquals(preferenceVOS.get(index)
			                          .getRoleId(), cacheVOS.get(index)
			                                                .getRoleId());
			assertEquals(preferenceVOS.get(index)
			                          .getRole(), cacheVOS.get(index)
			                                              .getRole());
		}

		/**
		 * Case 2 : Data not exists
		 */
		Mockito.when(mockUserPreferencesVService.getAllUserPreferences())
		       .thenReturn(Collections.emptyList());
		Mockito.reset(mockUserPreferencesVService);
		cacheVOS = userDetailsDao.loadUserCacheDetails();
		assertTrue(CollectionUtils.isEmpty(cacheVOS));
	}
}
