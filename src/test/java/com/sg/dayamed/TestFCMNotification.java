package com.sg.dayamed;

import com.sg.dayamed.base.BaseRestApiTest;
import com.sg.dayamed.rest.commons.BaseRestOutboundProcessor;
import com.sg.dayamed.pojo.VidoeCallSignatureModel;
import com.sg.dayamed.service.UserDetailsService;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(MockitoJUnitRunner.class)
public class TestFCMNotification extends BaseRestApiTest {

	@InjectMocks
	FcmNotification fcmNotification = new FcmNotification();

	@Mock
	ObjectMapper objectMapper;

	@Mock
	BaseRestOutboundProcessor baseRestOutboundProcessor;

	@Mock
	UserDetailsService mockUserDetailsService;

	@Test
	public void testPushVideoCallFCMNotification() throws Exception {
		Mockito.when(objectMapper.writeValueAsString(Mockito.any()))
		       .thenReturn("string");
		fcmNotification.pushVideoCallFCMNotification(RandomStringUtils.randomAlphabetic(10),
		                                             RandomStringUtils.randomAlphabetic(10),
		                                             RandomStringUtils.randomAlphabetic(10),
		                                             RandomStringUtils.randomAlphabetic(10),
		                                             RandomStringUtils.randomAlphabetic(10),
		                                             new VidoeCallSignatureModel());
	}

	@Test
	public void testPushFCMNotification() throws Exception {
		Mockito.when(mockUserDetailsService.findByToken(Mockito.anyString()))
		       .thenReturn(true);
		Mockito.when(objectMapper.writeValueAsString(Mockito.any()))
		       .thenReturn("string");
		fcmNotification.pushFCMNotification(RandomStringUtils.randomAlphabetic(10),
		                                    RandomStringUtils.randomAlphabetic(10),
		                                    RandomStringUtils.randomAlphabetic(10));
	}

	@Test
	public void testPushTwilioFCMNotification() throws Exception {
		Mockito.when(objectMapper.writeValueAsString(Mockito.any()))
		       .thenReturn("string");
		fcmNotification.pushTwilioFCMNotification(RandomStringUtils.randomAlphabetic(10),
		                                          RandomStringUtils.randomAlphabetic(10),
		                                          RandomStringUtils.randomAlphabetic(10),
		                                          RandomStringUtils.randomAlphabetic(10),
		                                          RandomStringUtils.randomAlphabetic(10));
	}

}
