package com.sg.dayamed.email;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MessageModelTest {

	private static final String CONTACT = "9866612345";

	@Test
	public void testEmptyCountryCode() {
		MessageModel messageModel = MessageModel.builder()
		                                        .tocontact(CONTACT)
		                                        .build();
		assertEquals("+" + CONTACT, messageModel.contactWithCountryCode());
	}

	@Test
	public void testWithCountryCode() {
		MessageModel messageModel = MessageModel.builder()
		                                        .tocontact(CONTACT)
		                                        .countryPhoneCode("91")
		                                        .build();
		assertEquals("+91" + CONTACT, messageModel.contactWithCountryCode());
	}
}
