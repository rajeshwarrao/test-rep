package com.sg.dayamed.email;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Message.class, Twilio.class})
public class TestSmsService {

	@InjectMocks
	SmsService smsService;

	@Mock
	MessageCreator message;

	@Test
	public void sendSMSNotificationWithEmptyMessageModel() {
		boolean smsNotificationStatus = smsService.sendSMSNotification(null);
		assertFalse(smsNotificationStatus);
	}

	@Test
	public void sendSMSNotificationWithoutContact() {
		boolean smsNotificationStatus = smsService.sendSMSNotification(MessageModel.builder()
		                                                                           .countryPhoneCode("1")
		                                                                           .build());
		assertFalse(smsNotificationStatus);
	}

	@Test
	public void sendSMSNotificationWithContact() {
		PowerMockito.mockStatic(Message.class);
		PowerMockito.mockStatic(Twilio.class);

		PowerMockito.when(Message.creator(any(PhoneNumber.class), any(PhoneNumber.class), anyString()))
		            .thenReturn(message);
		boolean smsNotificationStatus = smsService.sendSMSNotification(MessageModel.builder()
		                                                                           .countryPhoneCode(
				                                                                           RandomStringUtils.randomNumeric(
						                                                                           2))
		                                                                           .tocontact(
				                                                                           RandomStringUtils.randomNumeric(
						                                                                           10))
		                                                                           .message("Some message")
		                                                                           .build());
		assertTrue(smsNotificationStatus);
	}
}
