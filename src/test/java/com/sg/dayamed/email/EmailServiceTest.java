package com.sg.dayamed.email;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created By Gorantla, Eresh on 02/Aug/2019
 **/
@RunWith(PowerMockRunner.class)
@PrepareForTest({Session.class, InternetAddress.class, Transport.class, EmailService.class})
public class EmailServiceTest {

	@InjectMocks
	private EmailService emailService;

	private Session mockSession;

	private InternetAddress mockInternetAddress = null;

	private Transport mockTransport;

	private Email email;

	@Mock
	private MimeMessage mockMimeMessage;

	@Before
	public void init() throws Exception {
		ReflectionTestUtils.setField(emailService, "host", "localhost");
		ReflectionTestUtils.setField(emailService, "port", "9000");
		ReflectionTestUtils.setField(emailService, "userName", "userName");
		ReflectionTestUtils.setField(emailService, "passWord", "passWord");
		ReflectionTestUtils.setField(emailService, "from", "from");
		mockSession = PowerMockito.mock(Session.class);
		PowerMockito.mockStatic(Session.class);
		mockInternetAddress = PowerMockito.mock(InternetAddress.class);
		PowerMockito.mockStatic(InternetAddress.class);
		mockTransport = PowerMockito.mock(Transport.class);
		PowerMockito.mockStatic(Transport.class);
		PowerMockito.when(Session.getInstance(Mockito.any(), Mockito.any()))
		            .thenReturn(mockSession);
		InternetAddress[] internetAddresses = new InternetAddress[1];
		PowerMockito.when(InternetAddress.parse(Mockito.anyString()))
		            .thenReturn(internetAddresses);
		PowerMockito.whenNew(InternetAddress.class)
		            .withAnyArguments()
		            .thenReturn(mockInternetAddress);
		email = new Email();
		email.setBody(RandomStringUtils.randomAlphabetic(20));
		email.setRecipientList(Stream.of(RandomStringUtils.randomAlphabetic(10) + "@gmail.com")
		                             .collect(Collectors.toList()));
		email.setSubject(RandomStringUtils.randomAlphabetic(10));
	}

	@Test(expected = RuntimeException.class)
	public void testEmailMessageException() throws Exception {
		PowerMockito.doThrow(new MessagingException("Some Exception"))
		            .when(Transport.class);
		Transport.send(Mockito.any());
		emailService.sendEmail(email);
	}

	@Test(expected = RuntimeException.class)
	public void testEmailThrowableException() throws Exception {
		PowerMockito.whenNew(MimeMessage.class)
		            .withAnyArguments()
		            .thenThrow(new NullPointerException());
		emailService.sendEmail(email);
	}

	@Test
	public void testSendEmail() {
		emailService.sendEmail(email);
	}
}
